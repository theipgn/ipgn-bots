on *:NOTICE:*:*:{
  if (*I recognize you* iswm $1-) {
    if ($me != [iPGN-DOTA]) {
      msg AuthServ@Services.GameSurge.net ghost [iPGN-DOTA]
      .timer 1 10 nick [iPGN-DOTA]
    }
    .timer 1 10 join %pug.chan
    .timer 1 10 join %pug.killchan
    .timer 1 10 join %pug.adminchan
    .timer 1 10 join #dota2pug
  }
}
