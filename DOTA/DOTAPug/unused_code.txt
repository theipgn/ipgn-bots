on *:TEXT:!*:#ozf-help:{
  if ($nick !isop $chan) { halt }
  if ($1 = !accept) && ($2) {
    if (%ulticomp.team [ $+ [ $2 ] ]) {
      msg $chan $codes(Accepting team registration for $2)
      var %x = $gettok($v1,3,32)
      var %y = $ini(ulticomp.ini,0)
      while (%x < %y) {
        writeini ulticomp.ini %x teammate $readini(ulticomp.ini,$calc(%x + 1),teammate)
        writeini ulticomp.ini %x teamcaptain $readini(ulticomp.ini,$calc(%x + 1),teamcaptain)
        writeini ulticomp.ini %x eliminated $readini(ulticomp.ini,$calc(%x + 1),eliminated)
        writeini ulticomp.ini %x matches $readini(ulticomp.ini,$calc(%x + 1),matches)

        inc %x
      }
      remini ulticomp.ini %y
      addteam $gettok($v1,1,32) $gettok($v1,2,32)
      unset %ulticomp.team [ $+ [ $2 ] ]
    }
    else {
      msg $chan $codes(Team with captain $2 does not need approving)
    }
  }
  if ($1 = !decline) && ($2) {
    if (%ulticomp.team [ $+ [ $2 ] ]) {
      msg $chan $codes(Declining team registration for $2)
      msg %ulticomp.chan $codes(Team registration for $+($clr(name,$gettok($v1,1,32)),/,$clr($gettok($v1,2,32))) has been declined. Please ask $nick for more details)
      var %x = $gettok($v1,3,32)
      var %y = $ini(ulticomp.ini,0)
      while (%x < %y) {
        writeini ulticomp.ini %x teammate $readini(ulticomp.ini,$calc(%x + 1),teammate)
        writeini ulticomp.ini %x teamcaptain $readini(ulticomp.ini,$calc(%x + 1),teamcaptain)
        writeini ulticomp.ini %x eliminated $readini(ulticomp.ini,$calc(%x + 1),eliminated)
        writeini ulticomp.ini %x matches $readini(ulticomp.ini,$calc(%x + 1),matches)

        inc %x
      }
      remini ulticomp.ini %y
      unset %ulticomp.team [ $+ [ $2 ] ]
    }
    else {
      msg $chan $codes(Team with captain $2 does not need approving)
    }
  }
}

    if ($address($nick,2) == $address($2,2)) { 
      msg %ulticomp.chan $codes(You and your teammate have the same hostmask. Waiting for admin approval)
      set %ulticomp.team [ $+ [ $nick ] ] $nick $2 $ini(ulticomp.ini,0)
      msg %ulticomp.adminchan $codes($nick is trying to register a team with $2 $+ . These two players have the same hostmask. $&
        To accept this registration, type !accept $nick (or to decline, !decline $nick))
      writeini ulticomp.ini $ini(ulticomp.ini,0) teamcaptain $nick
      halt
    }

alias upload {
  if (%ulticomp.busy) {
    .timerupload 1 1 upload
    halt
  }
  set %ulticomp.busy 1
  write -c upload
  .copy -o ulticomp.ini ulticomp_upload.ini
  .copy -o ulticomp_matches.ini ulticomp_matches_upload.ini
  .copy -o ulticomp_servers.ini ulticomp_servers_upload.ini
  var %e = write upload

  %e cd /data/websites/ipgn/games/tf2/ultiduo/matches
  %e rm ulticomp*
  %e mput ulticomp_upload.ini ulticomp_matches_upload.ini ulticomp_servers_upload.ini
  %e mv ulticomp_upload.ini ulticomp.ini
  %e mv ulticomp_matches_upload.ini ulticomp_matches.ini
  %e mv ulticomp_servers_upload.ini ulticomp_servers.ini
  %e cd /data/websites/ipgn/games/tf2/ultiduo/teams
  %e rm ulticomp*
  %e mput ulticomp_upload.ini ulticomp_matches_upload.ini
  %e mv ulticomp_upload.ini ulticomp.ini
  %e mv ulticomp_matches_upload.ini ulticomp_matches.ini
  %e exit
  run psftp.exe zoe.ipgn.com.au -l gamesvr -pw iPgNDDm0wZ -b upload
  unset %ulticomp.busy
}