alias tinyurl {
  var %c $iif($isid,return,echo -a)
  if ($1) && (!$com(u)) && (!$sock(tinyurl)) {
    if ($regex($1,/^(?:(?:f|ht)tps?:\/\/|[^<>\/:\s])+[^<>.:\/\s]+(?:\.[^\/:<>.\s]{2,6})+(?:\/.*?)?$/i)) {
      sockopen tinyurl www.tinyurl.com 80
      sockmark tinyurl /create.php?url= $+ $remove($1-,$chr(32))
      .comopen u Wscript.Shell
      .comclose u $com(u,run,1,bstr*,$(%comspec%,) /c ping.exe -n 2 127.0.0.1 >nul,uint,0,bool,true)
      %c $iif(%tinyurl,$v1,No result.) 
    }
    else %c Invalid URL!
  }
  else %c $iif($1,Please be patient...,Input a URL!)
}
on *:sockopen:tinyurl: {
  if ($sockerr) {
    set -u3 %tinyurl Error connecting: $sock($sockname).wsmsg
    sockclose $sockname
  }
  else {
    sockwrite -n $sockname GET $sock($sockname).mark HTTP/1.1
    sockwrite -n $sockname Host: tinyurl.com
    sockwrite -n $sockname User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8)
    sockwrite -n $sockname $crlf
  }
}
on *:sockread:tinyurl: {
  if ($sockerr) {
    set -u3 %tinyurl Error connecting: $sock($sockname).wsmsg
    sockclose $sockname
  }
  else {
    var %x
    sockread %x
    if ($regex(%x,/<blockquote><b>(.*?)<\/b><br>/)) {
      set -u3 %tinyurl $regml(1)
      sockclose $sockname
    }
  }
}
on $*:TEXT:/^[.!@]tiny(url)? .*?$/Si:#: {
  if (!%tinyurl. [ $+ [ $2 ] ]) {
    var %c = $tinyurl($2) 
    msg $chan $iif(error !isin %c && invalid !isin %c,TinyURL converted: %c $+([,From $len($2) to $len(%c) chars,]),%c)
    set -u5 %tinyurl. $+ $2 1
  }
}
