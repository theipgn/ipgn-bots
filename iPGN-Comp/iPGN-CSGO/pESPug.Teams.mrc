on *:LOAD:{
  set %pug.teams.scriptversion 1.0.1
}

alias runteams {
  unset %playerlist %team.*
  setids
  teamnames
  teamscores
  teams %team.scores
  matchback
  sayteams

  set %team.done 1
}
alias setids {
  unset %playerlist
  var %x = $numtok(%players,32)
  var %y = 1
  while (%y <= %x) {
    %playerlist = $addtok(%playerlist,$steamid($gettok(%players,%y,32)),32)
    inc %y
  }
}
alias teamnames {
  tokenize 32 $read(%dir $+ teamnames.txt)
  %team.1.name = $1
  %team.2.name = $2
}
alias teamscores {
  write -c scorematch.txt
  var %x = 1
  %teamscores = $null
  while (%x <= $calc(2 * %gametype)) {
    var %line = $read(%dir $+ statsall.txt,w,$+(*,$gettok(%playerlist,%x,32),$chr(37),*))
    tokenize 37 %line
    ;############ FORMULA HERE LOLZ ###########
    var %score = $iif($calc($2 + $3 + $4) = 0,100,$38)
    %team.scores = %team.scores %score
    write scorematch.txt %score $gettok(%playerlist,%x,32) $statname($gettok(%playerlist,%x,32))
    inc %x
  }
}

alias teams {
  if $0 = 10 {

    write -c combos.txt
    var %x = 1
    var %scores = $sorttok($1-,32,nr)

    ;;Create 250 random pairs of teams
    while (%x <= 250) {
      :gorand
      unset %y %d %e %diff %b %5
      var %numbers = 10 9 8 7 6 5 4 3 2 1
      var %y = 10
      while (%y > 0) {
        var %rand = $gettok(%numbers,$rand(1,$numtok(%numbers,32)),32)
        var %numbers = $remtok(%numbers,%rand,1,32)
        tokenize 32 %scores
        var %b = $+(%b,$chr(32),$ [ $+ [ %rand ] ] )
        dec -z %y
      }
      tokenize 32 %b
      var %diff = $abs($calc($add5($1-5) - $add5($6-10)))
      ;;Write combo to file
      ;;Form: Team1;Team2;Difference
      write combos.txt $+($sorttok($1-5,32,nr),;,$sorttok($6-10,32,nr),;,%diff)
      inc %x
    }

    ;;Find the combo with the lowest difference
    var %c = 1
    var %lowest = 50000, %lowest.number
    while (%c <= 250) {
      if ($gettok($read(combos.txt,%c),3,59) < %lowest) {
        var %lowest = $v1
        var %lowest.number = %c
      }
      inc %c
    }

    ;;Output final teams
    %team.1.scores = $gettok($read(combos.txt,%lowest.number),1,59)
    %team.2.scores = $gettok($read(combos.txt,%lowest.number),2,59)
  }
  if $0 = 8 {

    write -c combos.txt
    var %x = 1
    var %scores = $sorttok($1-,32,nr)

    ;;Create 250 random pairs of teams
    while (%x <= 250) {
      :gorand
      unset %y %d %e %diff %b %5
      var %numbers = 8 7 6 5 4 3 2 1
      var %y = 10
      while (%y > 0) {
        var %rand = $gettok(%numbers,$rand(1,$numtok(%numbers,32)),32)
        var %numbers = $remtok(%numbers,%rand,1,32)
        tokenize 32 %scores
        var %b = $+(%b,$chr(32),$ [ $+ [ %rand ] ] )
        dec -z %y
      }
      tokenize 32 %b
      var %diff = $abs($calc($add4($1-4) - $add4($5-8)))
      ;;Write combo to file
      ;;Form: Team1;Team2;Difference
      write combos.txt $+($sorttok($1-4,32,nr),;,$sorttok($5-8,32,nr),;,%diff)
      inc %x
    }

    ;;Find the combo with the lowest difference
    var %c = 1
    var %lowest = 50000, %lowest.number
    while (%c <= 250) {
      if ($gettok($read(combos.txt,%c),3,59) < %lowest) {
        var %lowest = $v1
        var %lowest.number = %c
      }
      inc %c
    }

    ;;Output final teams
    %team.1.scores = $gettok($read(combos.txt,%lowest.number),1,59)
    %team.2.scores = $gettok($read(combos.txt,%lowest.number),2,59)
  }
  if $0 = 6 {

    write -c combos.txt
    var %x = 1
    var %scores = $sorttok($1-,32,nr)

    ;;Create 250 random pairs of teams
    while (%x <= 250) {
      :gorand
      unset %y %d %e %diff %b %5
      var %numbers = 6 5 4 3 2 1
      var %y = 6
      while (%y > 0) {
        var %rand = $gettok(%numbers,$rand(1,$numtok(%numbers,32)),32)
        var %numbers = $remtok(%numbers,%rand,1,32)
        tokenize 32 %scores
        var %b = $+(%b,$chr(32),$ [ $+ [ %rand ] ] )
        dec -z %y
      }
      tokenize 32 %b
      var %diff = $abs($calc($add3($1-3) - $add3($4-6)))
      ;;Write combo to file
      ;;Form: Team1;Team2;Difference
      write combos.txt $+($sorttok($1-3,32,nr),;,$sorttok($4-6,32,nr),;,%diff)
      inc %x
    }

    ;;Find the combo with the lowest difference
    var %c = 1
    var %lowest = 50000, %lowest.number
    while (%c <= 250) {
      if ($gettok($read(combos.txt,%c),3,59) < %lowest) {
        var %lowest = $v1
        var %lowest.number = %c
      }
      inc %c
    }
    ;;Output final teams
    %team.1.scores = $gettok($read(combos.txt,%lowest.number),1,59)
    %team.2.scores = $gettok($read(combos.txt,%lowest.number),2,59)
  }
}


;;Aliases to prevent clogging up the /teams
alias add3 {
  tokenize 32 $1-
  var %p = $calc($1 + $2 + $3)
  return %p
}
alias add4 {
  tokenize 32 $1-
  var %p = $calc($1 + $2 + $3 + $4)
  return %p
}
alias add5 {
  tokenize 32 $1-
  var %p = $calc($1 + $2 + $3 + $4 + $5)
  return %p
}

alias matchback {
  %team.1.match = $null
  %team.2.match = $null
  %team.1.names = $null
  %team.2.names = $null
  %team.1.irc = $null
  %team.2.irc = $null
  var %x = 1
  while (%x <= %gametype) {
    var %data = $read(scorematch.txt,w,$+($gettok(%team.1.scores,%x,32),$chr(32),*))

    %team.1.match = %team.1.match $gettok(%data,2,32)
    %team.1.names = %team.1.names $gettok(%data,3,32)
    %team.1.irc = %team.1.irc 12 $+ $gettok(%data,3,32) ( $+ $gettok(%data,1,32) $+ )
    write -dl $+ $readn scorematch.txt
    inc %x
  }
  %x = 1
  while (%x <= %gametype) {
    var %data = $read(scorematch.txt,w,$+($gettok(%team.2.scores,%x,32),$chr(32),*))

    %team.2.match = %team.2.match $gettok(%data,2,32)
    %team.2.names = %team.2.names $gettok(%data,3,32)
    %team.2.irc = %team.2.irc 12 $+ $gettok(%data,3,32) ( $+ $gettok(%data,1,32) $+ )
    write -dl $+ $readn scorematch.txt
    inc %x
  }
}

alias sayteams {
  rconsay  %team.1.name $+ : %team.1.names
  rconsay  %team.2.name $+ : %team.2.names
  mnmsg -1 %pug.chan $codes(%team.1.name 12CT: %team.1.irc)
  mnmsg -1 %pug.chan $codes(%team.2.name 04T: %team.2.irc)
}
