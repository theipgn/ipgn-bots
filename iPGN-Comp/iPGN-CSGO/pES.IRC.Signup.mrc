alias msgall {
  var %x = $nick(#ipgn-comp,0)
  var %y = 1
  var %z = 0
  while (%y <= %x) {
    if (*.gamesurge* !iswm $address($nick(#ipgn-comp,%y),3)) && (*.ipgn* !iswm $address($nick(#ipgn-comp,%y),3)) && $address($nick(#ipgn-comp,%y),3) {
      var %n = $nick(#ipgn-comp,%y)
      msg $nick(#ipgn-comp,%y) $codes(You are encouraged to use //mode $nick(#ipgn-comp,%y) +x as it will help reduce bot lag and you will join games faster (please add to your perform))
      inc %z    
    }
    inc %y
  }
  echo -a %z
}

on *:TEXT:!*:?:{
  if ($1 = !steamid) && (STEAM_1:*:* iswm $2) && ($3) && ($nick ison %pug.chan) {
    var %name = $3
    var %tmp = $auth($nick)
    if (!%tmp) { msg $nick $codes(Unknown error. Please try again.) | halt }
    if ($read(%dir $+ ids.txt,w,$+($auth($nick),$chr(37),*))) { 
      halt
    }
    elseif ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$2,$chr(37),*))) { 
      tokenize 37 $v1
      msg $nick $codes(12 $+ $2 is already registered to12 $3 $+ .)
    }
    elseif ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$3))) { 
      tokenize 37 $v1
      msg $nick $codes(12 $+ $3 is already registered to12 $2 $+ .)
    }
    elseif ($read(%dir $+ ids.temp.txt,w,$+(*,$chr(37),$3))) { 
      tokenize 37 $v1
      if ($3 = %name) {
        msg $nick $codes(12 $+ $3 has already been requested by12 $2 $+ .)
      }
    }
    elseif ($read(%dir $+ ids.temp.txt,w,$+($auth($nick),$chr(37),*))) { 
      halt
    }
    elseif ($regex($3,^\w+$)) {
      msg %pug.adminchan $codes(12 $+ $nick (12 $+ $auth($nick) $+ ) is trying to register12 $2 as12 $3 $+ . If this is acceptable, type !accept $3. If it isn't type !reject $3 <reason>.)
      write %dir $+ ids.temp.txt $+(%tmp,$chr(37),$upper($2),$chr(37),$3)
    }
    else {
      msg $nick $codes(The name12 $3 is invalid. It can only consist of letters and/or numbers. Also ensure your SteamID starts with STEAM_1 and not STEAM_0)
    }
  }
  else {
    msg $nick $codes(Invalid SteamID specified. The ID must start with STEAM_1 and not STEAM_0)
  }
}
