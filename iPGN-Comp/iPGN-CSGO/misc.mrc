
alias joinall {
  var %numpugs = 13

  var %x = 2

  join #ipgn-csgo
  join #ipgn-gocontrol %pug.adminchanpass

  while (%x <= %numpugs) {
    join #ipgn-csgo $+ %x
    join #ipgn-gocontrol $+ %x %pug.adminchanpass

    echo -s Joining #ipgn-csgo $+ %x

    inc %x
  }
}

alias resetserver {
  run $readini(compservers.ini,$1,restart) 
}

alias checknameduplicates {
  var %x = 1, %y = $lines(%dir $+ ids.txt)

  while (%x <= %y) {
    var %line = $read(%dir $+ ids.txt,%x)
    var %ln = $readn

    ; now we check if anyone's statname is the same as someone's auth
    if ($read(%dir $+ ids.txt,w,$+($gettok(%line,3,37),$chr(37),*))) {
      if ($v1 != %line) {
        echo -a AUTH DUPLICATE: %line has statname the same as auth of $v1

        ; change this person's statname, as we don't want to change anyone's auths
        var %newline = $puttok(%line,$gettok(%line,3,37) $+ 1,3,37)
        mutex_write -l $+ %ln %dir $+ ids.txt %newline

        echo -a .. UPDATED STATNAME TO %newline
      }
    }

    ; check if statname is the same as anyone else's
    if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$gettok(%line,3,37)))) {
      if ($v1 != %line) {
        echo -a STATNAME DUPLICATE: %line has same statname as $v1
      }
    }

    inc %x
  }
}


alias checkidmm {
  var %x = 1
  var %y = $lines(%dir $+ statsall.txt) 
  fopen statfile %dir $+ statsall.txt
  if ($ferr) { echo -a ERROR OPENING FILE WITH HANDLE STATFILE | halt }
  fopen idfile %dir $+ ids.txt
  if ($ferr) { echo -a ERROR OPENING FILE WITH HANDLE IDFILE | halt }
  while (%x <= %y) {
    var %steamid, %id, %idsteamid, %idid
    if ($fread(statfile)) {
      %steamid = $gettok($v1,1,37)
      %id = %x
    }
    if ($fread(idfile)) {
      var %linedata = $v1
      %idsteamid = $gettok(%linedata,2,37)
      %idid = %x
    }
    if (%steamid != %idsteamid) {
      echo -a MISMATCH BEGINS AT ID %x WITH STEAMID %steamid
      echo -a ID FILE STEAMID AT %x IS %idsteamid
      break
    }

    inc %x
  }
  fclose statfile
  fclose idfile
}

alias update_scores {
  ; used to push new scoring algorithm to errybody!
  acquire_mutex %dir $+ statsall.txt
  fopen statsall %dir $+ statsall.txt
  fopen -o newstats %dir $+ statsall.txt.new

  while (!$fopen(statsall).eof) {
    var %data = $fread(statsall)

    var %newscore = $playerscore(%data)

    echo -a OLD SCORE FOR $statname($gettok(%data,1,37)) $+ : $gettok(%data,38,37) NEW SCORE: %newscore

    %data = $puttok(%data,%newscore,38,37)

    .fwrite -n newstats %data
  }

  fclose statsall
  fclose newstats

  release_mutex %dir $+ statsall.txt
}
