on *:LOAD:{
  set %pug.unityhelper.scriptversion 1.1
}

alias addUnityUser {
  ; addUnityUser $nick

  var %sid = $hget(steamid.cache. $+ $cid,$1)

  if (!%sid) {
    return
  }

  ; prevent duplicate steamids (including ids awaiting approval)
  if ($read(%dir $+ ids.temp.txt,w,$+(*,$chr(37),%sid,$chr(37),*))) {
    hdel steamid.cache. $+ $cid $1
    return
  }

  ; we need to register this user, as there's no corresponding auth
  var %name = $regsubex($remove($1,[A]),[^A-Za-z0-9],$null)

  ; check if this name is already a statname or auth name. if it is, just append a 1
  ; until it's no longer in use
  while ($steamid(%name)) {
    %name = %name $+ 1
  }

  ; name is definitely not taken, now make sure it's not all numbers
  if (%name isnum) {
    %name = %name $+ a
  }

  ; OK, name is good to register as both an auth and statname!
  ; directly add the user, don't worry about accepting/rejecting

  echo -s UNITY USER ADD - AUTH: %name STEAMID: %sid STATNAME: %name

  ; auth, steamid, statname
  adduser %name %sid %name

  ; remove them from the steamid cache because they are now registered
  hdel steamid.cache. $+ $cid $1

  ; now we can properly auth this user
  auth $1
}

; separate chat hook for relaying, so all the commands aren't parsed
on *:TEXT:*:%pug.chan:{
  ; relay this message to all other connected networks
  if (!* !iswm $1) && ($nick != $me) {
    mnmsg_ex $cid $chan $codes(( $+ $network $+ ) <12 $+ $nick $+ > $1-)
  }
}

on ^*:OPEN:?:{
  ; this event triggers when someone PMs the bot. we can intercept it and prevent query windows from opening
  if ($network == %pes.networkname) && ($1 = !stat) { halt }
}

on *:TEXT:!*:?:{
  if ($nick isop %pug.boardchan) {
    if ($istok(%pug.headadmins,$auth($nick),59)) {
      var %target = $nick

      if ($1 = !setpesnetwork) {
        set %pes.networkname $2
        mnmsg %target $codewrap(PES Network name has been set to12 %pes.networkname $+ )
        saveini
      }
      elseif ($1 = !listglobaladmin) {
        var %list
        var %x = $numtok(%pug.headadmins,59)) 

        while (%x > 0) {
          %list = %list - $statname($gettok(%pug.headadmins,%x,59)) (Auth: $gettok(%pug.headadmins,%x,59) $+ )

          dec %x
        }

        mnmsg %target $codewrap(Global admins: %list)
      }
      elseif ($1 = !addglobaladmin) {
        if ($auth($2)) {
          %pug.headadmins = $addtok(%pug.headadmins,$v1,59)
          mnmsg %target $codewrap(12 $+ $2 has been added to the global admin list)
          saveini
        }
      }
      elseif ($1 = !delglobaladmin) {
        if ($istok(%pug.headadmins,$2,59)) {
          %pug.headadmins = $remtok(%pug.headadmins,$2,59)
          mnmsg %target $codewrap(12 $+ $2 has been removed from the global admin list)
          saveini
        }
        elseif ($auth($2)) {
          %pug.headadmins = $remtok(%pug.headadmins,$v1,59)
          mnmsg %target $codewrap(12 $+ $2 has been removed from the global admin list)
          saveini
        }
      }
      elseif ($1 = !status) {
        mnmsg %target $codewrap(%status)
      }
      elseif ($1 = !versions) {
        mnmsg %target $codewrap(IRC: %pug.irc.scriptversion TEAMS: %pug.teams.scriptversion STATS: %pug.stats.scriptversion AUTH: %pug.auth.scriptversion $&
          SERVER: %pug.server.scriptversion UNITYHELPER: %pug.unityhelper.scriptversion)
      }
      elseif ($1 = !exec) && ($nick == _bladez) {
        write -c exec.txt
        write exec.txt $2-
        .play -c %target exec.txt
        mnmsg %target $codewrap(Executed command: $2-)
      }
      elseif ($1 = !addmap) {
        if ($istok(%maps,$2,59)) {
          mnmsg %target $codewrap(Map is already in the map pool. !maps to see maps)
        }
        else {
          %maps = $addtok(%maps,$2,59)
          mnmsg %target $codewrap($2 has been added to the map pool)
          saveini
        }
      }
      elseif ($1 = !delmap) {
        if (!$istok(%maps,$2,59)) {
          mnmsg %target $codewrap($2 is not in the map pool)
        }
        else {
          %maps = $remtok(%maps,$2,1,59)
          mnmsg %target $codewrap($2 has been removed from the map pool)
          saveini
        }
      }
      elseif ($1 = !maps) {
        mnmsg %target $codewrap($maps())
      }
    }
  }
}

alias codewrap {
  return $codes(BOTID %bot.id $+ : $1-)
}
