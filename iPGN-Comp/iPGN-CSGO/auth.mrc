on *:LOAD:{
  set %pug.auth.scriptversion 1.0
}

alias pause {
  var %e = !echo $color(info) -a * /pause:
  if ($version < 5.91) {
    %e this snippet requires atleast mIRC version 5.91
  }
  elseif (!$regex(pause,$1-,/^m?s \d+$/Si)) {
    %e incorrect/insufficient parameters. Syntax: /pause <s|ms> <N>
  }
  elseif ($1 == ms) && ($istok(95 98 ME,$os,32)) {
    %e cannot use milliseconds parameter on OS'es beneath Win2k
  }
  elseif ($2 !isnum 1-) {
    %e must specify a number within range 1-
  }
  else {
    var %wsh = wsh $+ $ticks, %cmd
    if ($1 == s) %cmd = ping.exe -n $int($calc($2 + 1)) 127.0.0.1
    else %cmd = pathping.exe -n -w 1 -q 1 -h 1 -p $iif($2 > 40,$calc($2 - 40),$2) 127.0.0.1
    .comopen %wsh wscript.shell
    .comclose %wsh $com(%wsh,run,1,bstr*,% $+ comspec% /c %cmd >nul,uint,0,bool,true)
  }
}

alias sleep {
  var %a = $ticks $+ .wsf
  write %a <job id="js"><script language="jscript">WScript.Sleep( $+ $$1 $+ );</script></job>
  .comopen %a WScript.Shell
  if !$comerr { .comclose %a $com(%a,Run,3,bstr,%a,uint,0,bool,true) }
  .remove %a
}

alias auth {
  if ($1) && ($1 != ChanServ) && ($1 != System) && ($1 != $me) {
    var %table = $auth.table()

    if (*.gamesurge iswm $address($1,2)) || (*.ipgn iswm $address($1,2)) {
      auth.add $1 $gettok($remove($address($1,2),*!*@),1,46)
    }

    var %auth = $hget(%table,$1)
    if (%auth) {
      ;echo -a Got auth for $1 $+ : $ifmatch
      return %auth
    }

    var %i = $iif($2,$2,20)
    .who $1 n%na

    while (%i > 0) && (!%auth) {
      dec %i
      pause ms 30
      %auth = $hget(%table, $1)
    }

    return %auth
  }
}

raw 330:*:{
  ;echo -s raw 330: $2 logged in as $3
  if !$hget(%table,$2) {
    auth.add $2 $3
  }
}

raw 315:*:{
  haltdef
}
raw 354:*:{
  echo -s raw 354: $1-
  if ($3) {
    auth.add $2 $3
  }

  haltdef
}

raw 352:*:{
  ; this is a WHO response
  echo -s raw 352: $1-
  ;echo -s nick: $6 realname: $9

  check.pantheon.auth $6 $9
}

raw 311:*:{
  ; this is a WHOIS response
  ;echo -s raw 311: $1-

  check.pantheon.auth $2 $6
}

alias check.pantheon.auth {
  ; check.pantheon.auth <name> <steamid>

  ; If we're on the Pantheon (i.e Unity) network, the only identification we can use 
  ; is the user's SteamID, which is provded as the 'realname'
  if ($network == %pes.networkname) {
    ;echo -s On the pantheon network!

    if ($regex(sid,$2,/STEAM_1:([0-1]):(\d+)/i)) {
      ; have the user's steamid. now we can get their auth. this can either be the gamesurge
      ; auth corresponding to this steamid, or a constructed auth
      var %sid = $+(STEAM_1:,$regml(sid,1),:,$regml(sid,2))

      ; check for existing authname with steamid
      if ($authname(%sid)) {
        echo -s $1 has existing auth $v1

        auth.add $1 $v1
      }
      else {
        ; else, user has not registered. we will store the steamid in a hash table for it to be auto
        ; registered. then we can re-update the auth

        echo -s New unity user $1 ( $+ %sid $+ )

        ;hadd -m steamid.cache. $+ $cid $1 %sid

        ;addUnityUser $1
      }
    }
  }
}

alias auth.add {
  var %table = $auth.table()

  hadd -m %table $1 $2
}

alias auth.get {
  if ($1 = ChanServ) || ($network == %pes.networkname) { return }
  auth $1
}

alias auth.remove {
  if (%getauth.on) { return }
  var %table = $auth.table()
  if $hget(%table,$1) {
    hdel %table $1
  }
}
alias auth.whois {
  var %table = $auth.table()
  if !$hget(%table,$1) {
    .whois $1 $1
  }
}
alias auth.table {
  return auth.cache. $+ $cid
}

on *:TEXT:*:%pug.chan:{
  ;echo -a $chan $nick $1-
  var %table = $auth.table()
  if (!$hget(%table,$nick)) { auth.get $nick }
}
on *:JOIN:%pug.chan:{
  auth.get $nick
  ;.timer 1 1 auth.whois $nick
}
on *:ACTION:*:%pug.chan:{
  auth.get $nick
}
on *:OP:%pug.chan:{
  auth.get $nick
}
on *:NICK:{
  ; add the auth under the new nick, then delete the old nick
  var %table = $auth.table()

  if ($hget(%table,$nick)) {
    auth.add $newnick $v1

    hdel %table $nick
  }
}
on !*:PART:%pug.chan:{
  .timer 1 1 auth.remove $nick
}
on *:QUIT:{
  .timer 1 1 auth.remove $nick
}
on !*:KICK:%pug.chan:{
  .timer 1 1 auth.remove $knick
}

alias authcheck {
  if (%getauth.on) { return }
  hfree -w auth.cache. $+ $cid
  var %ipgn.tracker.num = $nick(%pug.chan,0)
  %x = 1 
  while (%x <= %ipgn.tracker.num) {
    authcheck2 $nick(%pug.chan,%x)
    inc %x
  }
}

alias authcheck2 {
  if (!$hget(auth.cache. $+ $cid,$1)) {
    auth $1
  }
}

alias checkauths {
  var %x = $nick(%pug.chan,0)
  var %y = 1
  while (%y <= %x) {
    if (!$hget(auth.cache. $+ $cid,$nick(%pug.chan,%y))) {
      echo -a $nick(%pug.chan,%y)
    }
    inc %y
  }
}
