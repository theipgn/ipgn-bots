menu * {
  Hash Manager: hshviewer
}
alias hshviewer {
  if (!$dialog(hsh)) { dialog -m hsh hsh }
}

dialog hsh {
  title "Hash Manager v.2"
  size -1 -1 423 420
  list 1, 11 17 121 260, vsbar sort
  list 2, 151 17 121 260, vsbar sort
  list 3, 291 17 121 260, vsbar
  box "Table", 4, 4 2 135 275 
  box "Item", 5, 144 2 135 275
  box "Data", 6, 284 2 135 275
  button "New Table", 7, 4 280 60 20,
  button "Delete", 8, 80 280 60 20, 
  button "Add Item", 9, 144 280 60 20, 
  button "Delete", 10, 220 280 60 20, 
  button "Edit Data", 11, 284 280 120 20, 
  edit "", 12, 70 305 350 20, read autohs center
  edit "", 13, 70 330 350 20, read autohs center
  edit "", 14, 70 355 350 20, read autohs center 
  text "Table Name", 15, 5 307 60 20, center
  text "Item", 16, 5 332 60 20, center
  text "Data", 17, 5 357 60 20, center
  button "R", 18, 65 280 15 20, 
  button "R", 19, 205 280 15 20, 
  button "R", 20, 405 280 15 20,
  text "*R buttons resets the list above it and following list where applicable.*", 21, 5 380 418 20, center
  text "*No information is erased from a hash table.*", 22, 5 395 418 20, center
}



alias chsh2 { 
  if (!hsh2) { return }
  else { dialog -x hsh2 hsh2 }
}
alias re.fresh {
  did -r hsh 1,2,3 
  var %x 1
  while ($hget(%x)) {
    did -a hsh 1 $hget(%x)
    inc %x
  }
}
alias re.fresh2 {
  var %x 1
  did -r hsh 2
  while ($hget($did(1).seltext,%x).item) {
    did -a hsh 2 $hget($did(1).seltext,%x).item
    inc %x 
  }
}
alias re.fresh3 {
  did -r hsh 3
  did -a hsh 3 $hget($did(1).seltext,$did(2).seltext)
}
alias re.fresh4 {
  did -h hsh 12,15,13,16,14,17
  did -r hsh 2,3
  did -u hsh 1  
  did -v hsh 12,15 {
    if ($did(1).lines == 1) { did -ra hsh 12 There is one table to choose from. | return }
    else { did -ra hsh 12 There are $did(1).lines tables to choose from. } 
  }
}
on *:dialog:hsh:close:* { chsh2 }


on *:dialog:hsh:init:* {
  did -h hsh 12,13,14,15,16,17
  did -h hsh 18,19,20
  re.fresh
  re.fresh4
}
on *:dialog:hsh:sclick:1 {
  did -h hsh 19,20
  if ($did(1).seltext == $null) { 
    did -h hsh 12,15,13,16,14,17 | did -h hsh 18 | did -r hsh 3 | did -r hsh 2 
    did -v hsh 12,15 {
      if ($did(1).lines == 1) { did -ra hsh 12 There is one table to choose from. halt }
      else { did -ra hsh 12 There are $did(1).lines tables to choose from. | halt } 
    }
  }
  else { 
    var %x 1
    did -r hsh 2,3 
    did -h hsh 13,16,14,17
    did -v hsh 18
    while ($hget($did(1).seltext,%x).item) {
      did -a hsh 2 $hget($did(1).seltext,%x).item
      inc %x
      did -v hsh 12,15
      did -v hsh 18
      did -h hsh 13,16,14,17
    }
    did -ra hsh 12 $did(1).seltext  $qt($hget($did(1).seltext).size)
    if (!hsh2) { set %a $did(1).seltext | hsh2 | dialog -s hsh2 -1 -1 -100 -100 | halt }
  }   
}

on *:dialog:hsh:sclick:2 {
  did -v hsh 19
  did -h hsh 20
  if ($did(2).seltext == $null) { did -h hsh 13,16,14,17 | halt }
  else { 
    did -r hsh 3
    did -ra hsh 3 $hget($did(1).seltext,$did(2).seltext)           
    did -ra hsh 12 $did(1).seltext  $qt($hget($did(1).seltext).size) 
    did -ra hsh 13 $did(2).seltext 
    did -v hsh 13,16
    did -h hsh 14,17
  } 
}

on *:dialog:hsh:sclick:3 { 
  did -v hsh 20    
  if ($did(3).seltext == $null) { did -h hsh 14,17 | halt }
  else {
    did -ra hsh 12 $did(1).seltext  $qt($hget($did(1).seltext).size) 
    did -ra hsh 13 $did(2).seltext 
    did -ra hsh 14 $did(3).seltext
    did -v hsh 14,17 
  }
}
on *:dialog:hsh:sclick:7 { 
  hmake $$?="Table Name Slots:"
  re.fresh
}

on *:dialog:hsh:sclick:8 {
  if ($did(1).sel) {
    if ($input(Are you sure that you want to delete $did(1).seltext,qy,Confirmation)) {
      .hfree $did(1).seltext 
      did -r hsh 1
      re.fresh 
      re.fresh4
    }
  }
}
on *:dialog:hsh:sclick:9 { 
  if ($did(1).sel) {
    if ($$?="+ ( Item with Data ) Ex: /hadd HashTable Item Data $crlf $+ - ( Item without Data ) Ex: /hadd HashTable Item" == -) { set %a $did(1).seltext | hsh2 |  dialog -s hsh2 -1 -1 145 155 | halt }
    else {
      .hadd $did(1).seltext $input(Enter item name,qe,Item Name) $input(Enter Data,qe,Data?)
    }
    re.fresh2
  }
}


on *:dialog:hsh:sclick:10 {
  if ($did(2).lines) {
    if ($input(Are you sure that you want to delete $did(2).seltext,qy,Confirmation)) {
      .hdel $did(1).seltext $did(2).seltext
      did -r hsh 2,3
      did -h hsh 13,16,14,17
      did -h hsh 19,20
      re.fresh2
      did -v hsh 12,15,13,16 {
        if ($did(2).lines == 0) { did -h hsh 13,16 | halt }
        if ($did(2).lines == 1) { did -ra hsh 13 There is one item to choose from. | halt }
        else { did -ra hsh 13 There are $did(2).lines item to choose from. | halt } 
      }
    }
  }
}

on *:dialog:hsh:sclick:11 {
  if ($did(3).lines == 0) { halt }
  else { 
    var %x $input(Current Information $crlf Table: $did(1).seltext $crlf Item: $did(2).seltext $crlf Data: $hget($did(1).seltext,$did(2).seltext),oe,Change Data To?)
    if (%x == $null) { halt }
    if (%x !isalnum) {
      .hdel $did(1).seltext $did(2).seltext 
      .hadd $did(1).seltext $did(2).seltext %x
      did -o hsh 3 1
      did -ra hsh 3,14 %x
      halt
    }
    if ((%x isletter)) { 
      .hdel $did(1).seltext $did(2).seltext 
      .hadd $did(1).seltext $did(2).seltext %x
      did -o hsh 3 1
      did -ra hsh 3,14 %x
      halt
    }
    if ((%x isletter) && || (%x isnum)) {
      .hdel $did(1).seltext $did(2).seltext 
      .hadd $did(1).seltext $did(2).seltext %x
      did -o hsh 3 1
      did -ra hsh 3,14 %x
      halt
    }
    if (%x == 0) {
      .hdel $did(1).seltext $did(2).seltext
      .hadd $did(1).seltext $did(2).seltext
      did -o hsh 3 $calc(1 - %x)
      inc %x
      did -ra hsh 3,14 $calc(1 - %x)
      inc %x
    }
    else {
      if (%x == 1) {   
        .hdel $did(1).seltext $did(2).seltext 
        .hadd $did(1).seltext $did(2).seltext 1
        did -o hsh 3 1
        did -ra hsh 3,14 1
      }
      else { 
        .hdel $did(1).seltext $did(2).seltext 
        .hadd $did(1).seltext $did(2).seltext %x
        inc %x
        did -o hsh 3 $calc(%x - 2)
        inc %x
        did -ra hsh 3,14 $calc(%x - 2)
        inc %x
      }
    }
  }
}

on *:dialog:hsh:sclick:18 {
  did -h hsh 12,15,13,16,14,17
  did -h hsh 18,19,20
  did -r hsh 2,3
  did -u hsh 1  
  did -v hsh 12,15 {
    if ($did(1).lines == 1) { did -ra hsh 12 There is one table to choose from. | halt }
    else { did -ra hsh 12 There are $did(1).lines tables to choose from. } 
  }
}
on *:dialog:hsh:sclick:19 {
  did -h hsh 13,16,14,17
  did -h hsh 20
  did -r hsh 3
  did -u hsh 2 {
    if ($did(2).lines == 0) { halt }
    else {
      did -h hsh 19
      did -v hsh 13,16 {
        if ($did(2).lines == 1) { did -ra hsh 13 There is one item to choose from. | halt }
        else { did -ra hsh 13 There are $did(2).lines item(s) to choose from. }
      }
    }
  }
}
on *:dialog:hsh:sclick:20 {
  if ($did(3).lines == 0) { halt }
  else {
    did -h hsh 14,17
    did -h hsh 19,20
    did -r hsh 3 
    did -u hsh 2
    did -v hsh 13,16 {
      if ($did(2).lines == 1) { did -ra hsh 13 There is one item to choose from. | halt }
      else { did -ra hsh 13 There are $did(2).lines item(s) to choose from. }
    }
  }
}

alias hsh2 {
  if (!$dialog(hsh2)) { dialog -m hsh2 hsh2 |  dialog -s hsh2 -1 -1 -100 -100 }
}

dialog hsh2 {
  title "HM v.2 AddOn"
  size -1 -1 145 155 
  list 1, 10 20 125 75
  box "Item", 2, 2 2 140 80 
  button "List Item", 3, 1 87 143 20
  button "Add Item", 4, 1 110 143 20
  button "Clear List", 5, 1 133 143 20
}

on *:dialog:hsh2:*:*: {
  if ($devent == init) { did -r hsh2 1 }
  if ($devent == sclick) { 
    if ($did == 3) { 
      var %x $input(Enter item to be entered,qe,Item)
      did -a hsh2 1 %x
    }
    if ($did == 4) { 
      if ($did(1).lines == 0) { halt }
      else { 
        if ($did(1).seltext == $null) { halt }
        else { hadd -sc %a $did(1).seltext }
      }
    }
    if ($did == 5) { did -r hsh2 1 }
  }
}
