
alias changeauth {
  if ($read(%dir $+ ids.txt,w,$+($2,$chr(37),*))) {
    mnmsg $chan $codes(12 $+ $2 is already registered to12 $gettok($ifmatch,3,37) (Auth:12 $gettok($ifmatch,1,37) ID:12 $gettok($ifmatch,2,37) $+ ))
    return
  }

  if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$1))) {
    var %line = $v1
    mutex_write -w $+ $+(*%,$gettok(%line,2,37),%,$1) %dir $+ ids.txt $puttok(%line,$2,1,37)
    mnmsg $chan $codes(12 $+ $1 auth name updated to12 $2 (previously12 $gettok(%line,1,37) $+ ))

    who $gnick($gettok(%line,1,37))
  }
}

on *:TEXT:!*:%pug.adminchan:{
  if ($1 = !changeauth) {
    if ($3) && ($steamid($2)) {
      changeauth $2 $3
    }
    else {
      mnmsg $chan $codes(Syntax: !changeauth <statname> <authname>)
    }
  }
  if ($1 = !rename) && ($istok(%pug.headadmins,$auth($nick),59)) && ($3) {
    if (!$regex($3,^\w+$)) {
      mnmsg $chan $codes(Names can only consist of letters/numbers)
      return
    }
    if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$3)) || $read(%dir $+ ids.txt,w,$+($3,$chr(37),*))) {
      ; allow name to be updated if the new name is same as user's auth 
      var %data = $ifmatch
      if ($3 == $gettok(%data,3,37) || $3 != $gettok(%data,1,37)) {
        mnmsg $chan $codes(12 $+ $3 is already registered to12 $gettok(%data,3,37) (Auth:12 $gettok(%data,1,37) ID:12 $gettok(%data,2,37) $+ ))
        return
      }
    }
    if ($read(%dir $+ ids.txt,w,* $+ $chr(37) $+ $2)) {
      mutex_write -l $+ $readn %dir $+ ids.txt $puttok($ifmatch,$3,3,37)
      mnmsg $chan $codes(12 $+ $2 has been renamed to12 $3)
    }
    else {
      mnmsg $chan $codes(12 $+ $2 not found)
    }
  }

  if ($1 = !delbotban) && ($2) && ($istok(%pug.headadmins,$auth($nick),59)) {
    if ($timer(threadwait).secs) {
      mnmsg $chan $codes(Please wait12 $timer(threadwait).secs seconds)
      return
    }
    if ($read(%dir $+ bans.txt,w,$id($steamid($2)) $+ $chr(37) $+ *)) {
      var %line = $v1
      mutex_write -dl $+ $readn %dir $+ bans.txt
      mnmsg $chan $codes(12 $+ $2 is now unbanned.)
      mnmsg -1 %pug.chan $codes(12 $+ $2 is now unbanned.)
      rank
      thread delban $authname($2) $auth($nick) 
    }
    else {
      mnmsg $chan $codes(No ban for12 $2 found.)
    }
  }

  if ($1 = !addbotban) && ($istok(%pug.headadmins,$auth($nick),59)) {
    ;# !addbotban statname time reason
    ;# bans.txt - uid%adminuid%reason%date%unbanctime
    if ($timer(threadwait).secs) {
      mnmsg $chan $codes(Please wait12 $timer(threadwait).secs seconds)
      return 
    }
    if ($0 < 4) {
      mnmsg $chan $codes(Syntax: !addbotban statname (<x>d)(<x>w)(<x>w<x>d) reason eg. !addbotban HeatoN 1w2d Abuse)
      return
    }
    if (!$steamid($2)) {
      mnmsg $chan $codes(Unable to find SteamID for 12 $+ $2 $+ . Did you specify the correct statname?)
      return
    }
    if (!$regex($3,/(\d+w)?(\d+d)?/)) {
      mnmsg $chan $codes(Invalid duration specified. Must be #d or #w or #w#d)
      return
    }
    if ($+(*,$chr(39),*) iswm $4-) || ($+(*,$chr(38),*) iswm $4-) { 
      mnmsg $chan $codes(Can not contain ' or &)
      return
    }
    if (!$authname($2)) { msg $chan $codes(12 $+ $2 not found) | return }
    if ($read(%dir $+ bans.txt,w,$id($steamid($2)) $+ $chr(37) $+ *)) {
      var %line = $v1
      mnmsg $chan $codes(12 $+ $statname($gettok(%line,1,37)) is already banned by12 $statname($gettok(%line,2,37)) $+ . 12Reason: $gettok(%line,3,37) $+ . $&
        12Remaining: $duration($calc($gettok(%line,5,37) - $ctime),2) $+ .)
      return
    }
    else {
      if $regex($3,/^((\d+w)|(\d+d))$/) {
        var %unbantime = $calc($ctime + $duration($regml(1)))

        mutex_write %dir $+ bans.txt $+($id($steamid($2)),$chr(37),$id($steamid($auth($nick))),$chr(37),$strip($4-),$chr(37),$date(dd-mm-yy),$chr(37),%unbantime)
        mnmsg $chan $codes(12 $+ $2 has been banned. 12Reason: $4-  12Time: $duration($calc(%unbantime - $ctime)) $+ .)
        mnmsg -1 %pug.chan $codes(12 $+ $2 has been banned. 12Reason: $4- 12Time: $duration($calc(%unbantime - $ctime)) $+ .)
        rank
        thread addban $authname($2) $auth($nick) $3 $4-
      }
      elseif $regex($3,/^(\d+w)(\d+d)$/) {
        var %unbantime = $calc($ctime + $duration($regml(1)) + $duration($regml(2)))

        mutex_write %dir $+ bans.txt $+($id($steamid($2)),$chr(37),$id($steamid($auth($nick))),$chr(37),$strip($4-),$chr(37),$date(dd-mm-yy),$chr(37),%unbantime)
        mnmsg $chan $codes(12 $+ $2 has been banned. 12Reason: $4-  12Time: $duration($calc(%unbantime - $ctime)) $+ .)
        mnmsg -1 %pug.chan $codes(12 $+ $2 has been banned. 12Reason: $4- 12Time: $duration($calc(%unbantime - $ctime)) $+ .)
        rank
        thread addban $authname($2) $auth($nick) $3 $4-
      }
      else {
        mnmsg $chan $codes(Syntax: !addbotban statname (<x>d)(<x>w)(<x>w<x>d) reason eg. !addbotban HeatoN 1w2d Abuse)
      }
    }
  }
}

on *:TEXT:!*:%pug.boardchan:{
  if ($istok(%pug.headadmins,$auth($nick),59)) {
    if ($1 = !listglobaladmin) {
      var %list
      var %x = $numtok(%pug.headadmins,59)) 

      while (%x > 0) {

        %list = %list $statname($gettok(%pug.headadmins,%x,59))

        dec %x
      }

      mnmsg $chan $codes(Global admins: %list)
    }
    elseif ($1 = !addglobaladmin) {
      if ($auth($2)) {
        %pug.headadmins = $addtok(%pug.headadmins,$v1,59)
        mnmsg $chan $codes(12 $+ $2 has been added to the global admin list)
        saveini
      }
    }
    elseif ($1 = !delglobaladmin) {
      if ($istok(%pug.headadmins,$2,59)) {
        %pug.headadmins = $remtok(%pug.headadmins,$v1,59)
        mnmsg $chan $codes(12 $+ $2 has been removed from the global admin list)
        saveini
      }
      elseif ($auth($2)) {
        %pug.headadmins = $remtok(%pug.headadmins,$v1,59)
        mnmsg $chan $codes(12 $+ $2 has been removed from the global admin list)
        saveini
      }
    }
  }
}

on *:TEXT:!*:#info:{
  if ($1 = !admininfo) {
    ; call the filter command, which will pass each line to `admininfo` after sorting
    filter -ffkeut 2 37 %dir $+ admin.txt admininfo

    return
  }
}

alias admininfo {
  var %admindata = $1

  var %admin.count = $gettok(%admindata,2,37)
  var %admin.id = $gettok(%admindata,1,37)

  mnmsg #info $statname(%admin.id) (ID: $steamid(%admin.id) Auth: $authname(%admin.id) $+ ) has completed  %admin.count  games
}

alias restore { 
  upload uploadbans %dir $+ bans.txt
  upload upload %dir $+ statsall.txt
  upload uploadgames %dir $+ games.txt
  upload ids %dir $+ ids.txt

  var %numfiles = $findfile(%dir $+ games,*.txt,0) 
  var %x = 1
  mnmsg %pug.adminchan Restoring website game stats

  while (%x <= %numfiles) {
    .timery $+ %x -m 1 $calc(%x * 50) upload uploadendgame %dir $+ games/ $+ %x $+ .txt &game= $+ %x
    .timery $+ %x $+ -2 -m 1 $calc(%x * 50) upload uploadgame %dir $+ gamestats/ $+ %x $+ .txt &game= $+ %x

    inc %x
  }

  .timery0 -m 1 $calc(%numfiles * 50 + 100) mnmsg %pug.adminchan Restore complete
}

alias newstats {
  .copy -o %dir $+ statsall.txt %dir $+ statsall.txt.bak. $+ $time(dd-mm-yyyy-hh-nn-ss)
  write -c %dir $+ statsall.txt
  var %a = 1
  var %b = $lines(%dir $+ ids.txt)
  while (%a <= %b) {
    tokenize 37 $read(%dir $+ ids.txt,%a)
    write %dir $+ statsall.txt $+($2,$str($chr(37) $+ 0, $calc($numtok(%allstats,37) - 2)),$chr(37),0.0.0.0)
    inc %a
  }
}
