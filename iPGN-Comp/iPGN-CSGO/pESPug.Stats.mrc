on *:LOAD:{
  set %pug.stats.scriptversion 1.0
}

alias setstats {
  set %allstats STEAMID%Won%Lost%Drew%K%D%DMG%DMRec%Rounds%RoundsWCT%RoundsWT%RoundsLCT%RoundsLT%Suic%TKs%Defuse%Plants%K-CT%K-T%D-CT%D-T%5kr%4kr%3kr%2kr%1kr%5v1%4v1%3v1%2v1%1v1%streak%highstreak%lowstreak%MVPs%highdpr%lowdpr%SCORE%ak47%aug%awp%bizon%deagle%elite%famas%fiveseven%flashbang%g3sg1%galilar%glock%hegrenade%hkp2000%incgrenade%knife%m4a1%m249%mac10%mag7%moltov%mp7%mp9%negev%nova%p90%p250%sawedoff%scar20%sg556%ssg08%taser%tec9%ump45%xm1014%IP  
  ;STEAMID%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0.0.0.0
}



alias filecheck {
  if (!$read(stats.txt,w,$1 $+ $chr(37) $+ *)) {
    write stats.txt $+($1,$str($chr(37) $+ 0,$calc($numtok(%allstats,37) - 2)),$chr(37),0.0.0.0)
  }
}
alias addwin {
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) {
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),2,37) + 1),2,37)
    tokenize 37 $read(%dir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)
    if ($32 <= 0) {
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),1,32,37)
      echo -a $statname($1) $+ : Upped Current streak to 1
      if (1 > $33) {
        write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),1,33,37)
      }    
    }
    else {
      echo -a $1 $+ : STREAK > 0 - $32 New streak: $calc($33 + 1)
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($32 + 1),32,37)
      if ($calc($32 + 1) > $33) { 
        write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($32 + 1),33,37)
      }
    }
  }
}

alias addloss {
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) {
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),3,37) + 1),3,37)
    tokenize 37 $read(%dir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)
    if ($32 >= 0) {
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),-1,32,37)

      if (-1 < $34) {
        write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),-1,34,37)

      }    
    }
    else {

      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($32 - 1),32,37)

      if ($calc($32 - 1) < $34) {
        write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($32 - 1),34,37)

      }
    }
  }
}

alias adddraw {
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) {
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),4,37) + 1),4,37)
  }
}

alias addip {
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) && ($2) {
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$2,65,37)
  }
}

alias addroundwin {
  if (!%match.on) { return }
  if (%half.1) { %pattern.1 = %pattern.1 $1 }
  if (%half.2) { %pattern.2 = %pattern.2 $1 }
  if (%othalf.1) { %pattern.ot1 = %pattern.ot1 $1 }
  if (%othalf.2) { %pattern.ot2 = %pattern.ot2 $1 }
  if ($2 = 1) || ($2 = 4) {
    if ($1 = CT) {
      addround 10 %team.1.match
      addclutch %team.1.match
      addround 13 %team.2.match
    }
    else {
      addround 11 %team.2.match
      addclutch %team.2.match
      addround 12 %team.1.match
    }
  }
  if ($2 = 2) || ($2 = 3) {
    if ($1 = CT) {
      addround 10 %team.2.match
      addclutch %team.2.match
      addround 13 %team.1.match
    } 
    else {
      addround 11 %team.1.match
      addclutch %team.1.match
      addround 12 %team.2.match
    }
  }
}

alias addclutch {
  if (%clutch) && ($istok($1-,$gettok(%clutch,1,32),32)) {
    rconsay  $statname($gettok(%clutch,1,32)) clutched $gettok(%clutch,2,32) $+ v1
    var %c.tok = $findtok(%allstats,$+($gettok(%clutch,2,32),v,1),1,37)
    write -w $+ $gettok(%clutch,1,32) $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$gettok(%clutch,1,32) $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$gettok(%clutch,1,32) $+ $chr(37) $+ *),%c.tok,37) + 1),%c.tok,37)
  }
}

alias addround {
  ; $1 = token
  ; $2- = IDs
  var %tok = $1
  var %x = $numtok($2-,32)
  var %y = 1
  while (%y <= %x) {
    var %id = $gettok($2-,%y,32)
    write -w $+ %id $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,%id $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,%id $+ $chr(37) $+ *),9,37) + 1),9,37)
    write -w $+ %id $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,%id $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,%id $+ $chr(37) $+ *),%tok,37) + 1),%tok,37)
    inc %y
  }
}

alias addkill {
  if (!%match.on) { return }
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),5,37) + 1),5,37)
    if ($2 = CT) {
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),18,37) + 1),18,37)
    }
    if ($2 = T) {
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),19,37) + 1),19,37)
    }
    ;Multi-kill rounds

    inc %kills.round. [ $+ [ $1 ] ]

    if ( %kills.round. [ $+ [ $1 ] ] == 1) {
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),26,37) + 1),26,37)
    }
    elseif ( %kills.round. [ $+ [ $1 ] ] == 2) {
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),26,37) - 1),26,37)
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),25,37) + 1),25,37)
    }
    elseif ( %kills.round. [ $+ [ $1 ] ] == 3) {
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),25,37) - 1),25,37)
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),24,37) + 1),24,37)
    }
    elseif ( %kills.round. [ $+ [ $1 ] ] == 4) {
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),24,37) - 1),24,37)
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),23,37) + 1),23,37)
    }
    elseif ( %kills.round. [ $+ [ $1 ] ] == 5) {
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),23,37) - 1),23,37)
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),22,37) + 1),22,37)
      rconsay  $statname($1) just got a TD!
    }

    ;Clutches
    ; set clutch up
    if ($istok(%alive.1,$3,32)) {
      %alive.1 = $remtok(%alive.1,$3,1,32)
      if ($numtok(%alive.1,32) = 1) && (!%clutch) {
        set %clutch $gettok(%alive.1,1,32) $numtok(%alive.2,32)
      }
    }
    if ($istok(%alive.2,$3,32)) {
      %alive.2 = $remtok(%alive.2,$3,1,32)
      if ($numtok(%alive.2,32) = 1) && (!%clutch) {
        set %clutch $gettok(%alive.2,1,32) $numtok(%alive.1,32)
      }
    }
  }
}

alias adddeath {
  if (!%match.on) { return }
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),6,37) + 1),6,37)
    if ($2 = CT) {
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),20,37) + 1),20,37)
    }
    if ($2 = T) {
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),21,37) + 1),21,37)
    }
  }
}

alias delkill {
  if (!%match.on) { return }
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) {
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),5,37) - 1),5,37)
  }
}

alias adddamage {
  if (!%match.on) { return }
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),7,37) + $2),7,37)
  }
}


alias adddamager {
  if (!%match.on) { return }
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),8,37) + $2),8,37)
  }
}

alias addplant {
  if (!%match.on) { return }
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),17,37) + 1),17,37)
  }
}

alias addtk {
  if (!%match.on) { return }
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),15,37) + 1),15,37)
  }
  if ($istok(%alive.1,$2,32)) {
    %alive.1 = $remtok(%alive.1,$2,1,32)
    if ($numtok(%alive.1,32) = 1) && (!%clutch) {
      set %clutch $gettok(%alive.1,1,32) $numtok(%alive.2,32)
    }
  }
  if ($istok(%alive.2,$2,32)) {
    %alive.2 = $remtok(%alive.2,$2,1,32)
    if ($numtok(%alive.2,32) = 1) && (!%clutch) {
      set %clutch $gettok(%alive.2,1,32) $numtok(%alive.1,32)
    }
  }
}

alias addsuicide {
  if (!%match.on) { return }
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),14,37) + 1),14,37)
  }
  if ($istok(%alive.1,$1,32)) {
    %alive.1 = $remtok(%alive.1,$1,1,32)
  }
  if ($istok(%alive.2,$1,32)) {
    %alive.2 = $remtok(%alive.2,$1,1,32)
  }
}

alias adddefuse {
  if (!%match.on) { return }
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),16,37) + 1),16,37)
  }
}

;find the place to put it ram
alias addassist {
  if (!%match.on) { return }
  filecheck $1
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),16,37) + 1),16,37)
  }
}


alias addweapon {
  var %gun = $2
  if (%gun  == usp_silencer || %gun == usp_silencer_off) { %gun  = p250 }
  if (%gun == m4a1_silencer || %gun == m4a1_silencer_off) { %gun  = m4a1 }
  if (!%match.on) { return }
  filecheck $1
  var %weap = ak47 aug awp bizon deagle elite famas fiveseven flashbang g3sg1 galilar glock hegrenade hkp2000 incgrenade knife m4a1 m249 mac10 mag7 moltov mp7 mp9 negev nova p90 p250 sawedoff scar20 sg556 ssg08 taser tec9 ump45 xm1014
  var %numtok = $calc($findtok(%weap,%gun ,1,32) + 38)
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
    write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,$1 $+ $chr(37) $+ *),%numtok,37) + 1),%numtok,37)
  }
}
