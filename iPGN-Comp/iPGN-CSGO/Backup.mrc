on *:CONNECT:{
  .timer 07:00 1 1 timerbackup 0 86400 backup
}

alias backup {
  var %date = $date(dd-mm-yy)

  var %backupdir = D:\iPGN-Bots\iPGN-Comp\backup\

  var %botfile = %backupdir $+ mIRC-CSGO-Comp- $+ %date $+ .zip
  var %statfile = %backupdir $+ stats-csgo-s5- $+ %date $+ .zip

  write -l1 %dir $+ batch\backup.bat "C:\Program Files (x86)\WinZip\wzzip.exe" -rP -x*.log %botfile $mircdir
  write -l2 %dir $+ batch\backup.bat "C:\Program Files (x86)\WinZip\wzzip.exe" -rP %statfile %dir
  write -l3 %dir $+ batch\backup.bat cd %dir $+ batch

  write -l2 %dir $+ batch\backup.commands lcd %backupdir
  write -l3 %dir $+ batch\backup.commands mput %botfile %statfile

  run %dir $+ batch\backup.bat
}
