alias upload {

  ;-- Set these variables --
  %hostofphpfile = cs-comp.ipgn.com.au
  %locationofphpfile = /s5/ $+ $1 $+ .php?password=cs-c0mp45 $+ $iif($3,$3,$null)
  ;-------------------------

  %dest = $$2
  if ((!$file(%dest)) || ($isdir(%dest))) return
  .remove upload.tmp
  echo -a Uploading file %dest $+ ....
  sockopen upload %hostofphpfile 80
}
On *:sockopen:upload:{

  var %b = $md5() | write -c upload.tmp $+(--,%b,$lf,$&
    Content-Disposition: form-data; name="uploaded";,$&
    filename=",$nopath(%dest),",$lf,$lf)
  .copy -a " $+ %dest $+ " upload.tmp
  write upload.tmp -- $+ %b

  var %write = sockwrite -tn upload
  %write POST %locationofphpfile HTTP/1.1
  %write Host: %hostofphpfile
  %write Connection: close
  %write Content-Type: multipart/form-data; boundary= $+ %b
  %write Content-Length: $file(upload.tmp)
  %write
  .fopen upload upload.tmp
}

On *:sockwrite:upload:{
  if $sock(upload).sq > 8192 || !$fopen(upload) { return }
  if $fread(upload,8192,&a) {
    sockwrite upload &a
  }
  else .fclose upload
}

On *:sockread:upload:{
  var %temp
  sockread %temp
  ; echo -a %temp
  if & 2* iswm %temp {
    echo 3 -a SUCCESS!
  }
  elseif & 4* iswm & temp {
    echo 4 -a FAILED!
  }
}

On *:sockclose:upload:cleanupload
alias cleanupload {
  .sockclose upload
  if $fopen(upload) { .fclose upload }
  unset %dest
  .remove upload.tmp
}
