on *:CONNECT:{
  .timer 07:00 1 1 timerbackup 0 432000 backup
}

alias msgall {
  var %x = $nick(#ipgn-comp,0)
  var %y = 1
  var %z = 0
  while (%y <= %x) {
    if (*.gamesurge* !iswm $address($nick(#ipgn-comp,%y),3)) && (*.ipgn* !iswm $address($nick(#ipgn-comp,%y),3)) && $address($nick(#ipgn-comp,%y),3) {
      var %n = $nick(#ipgn-comp,%y)
      msg $nick(#ipgn-comp,%y) $codes(You are encouraged to use //mode $nick(#ipgn-comp,%y) +x as it will help reduce bot lag and you will join games faster (please add to your perform))
      inc %z    
    }
    inc %y
  }
  echo -a %z
}

alias newstats {
  write -c statsall.txt
  var %a = 1
  var %b = $lines(%dir $+ ids.txt)
  while (%a <= %b) {
    tokenize 37 $read(%dir $+ ids.txt,%a)
    write statsall.txt $2 $+ $str($chr(37) $+ 0,63)
    inc %a
  }

}
