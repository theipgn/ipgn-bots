alias pause {
  var %e = !echo $color(info) -a * /pause:
  if ($version < 5.91) {
    %e this snippet requires atleast mIRC version 5.91
  }
  elseif (!$regex(pause,$1-,/^m?s \d+$/Si)) {
    %e incorrect/insufficient parameters. Syntax: /pause <s|ms> <N>
  }
  elseif ($1 == ms) && ($istok(95 98 ME,$os,32)) {
    %e cannot use milliseconds parameter on OS'es beneath Win2k
  }
  elseif ($2 !isnum 1-) {
    %e must specify a number within range 1-
  }
  else {
    var %wsh = wsh $+ $ticks, %cmd
    if ($1 == s) %cmd = ping.exe -n $int($calc($2 + 1)) 127.0.0.1
    else %cmd = pathping.exe -n -w 1 -q 1 -h 1 -p $iif($2 > 40,$calc($2 - 40),$2) 127.0.0.1
    .comopen %wsh wscript.shell
    .comclose %wsh $com(%wsh,run,1,bstr*,% $+ comspec% /c %cmd >nul,uint,0,bool,true)
  }
}

alias sleep {
  var %a = $ticks $+ .wsf
  write %a <job id="js"><script language="jscript">WScript.Sleep( $+ $$1 $+ );</script></job>
  .comopen %a WScript.Shell
  if !$comerr { .comclose %a $com(%a,Run,3,bstr,%a,uint,0,bool,true) }
  .remove %a
}

alias auth {
  if ($1) && ($1 != ChanServ) {
    var %table = auth.cache. $+ $cid

    if (*.gamesurge iswm $address($1,2)) || (*.ipgn iswm $address($1,2)) {
      hadd -m %table $1 $gettok($remove($address($1,2),*!*@),1,46)
    }
    if ($hget(%table,$1)) && ($hget(%table,$1) != 0) {
      ;  echo -a Got auth for $1 $+ : $ifmatch
      return $ifmatch
    }

    set %authreturn 0
    var %i = $iif($2,$2,20)
    .who $1 n%na
    while (%i > 0) && (%authreturn == 0) {
      dec %i
      ;sleep 50
      pause ms 50
    }
    var %table = auth.cache. $+ $cid
    if (%authreturn == 0) { unset %authreturn }
    if (%authreturn) && (%authreturn != 0) {
      hadd -m %table %authnick %authreturn
      ;  echo -a Got auth for %authnick $+ : %authreturn
    }
    return $iif(%authreturn != $null,%authreturn,$null)
  }
}

raw 330:*:{ 
  var %table = auth.cache. $+ $cid
  if !$hget(%table,$2) {
    hadd -m %table $2 $3
  }
  haltdef 
}

raw 315:*:{
  haltdef
}
raw 354:*:{
  set %authnick $2
  set %authreturn $iif($3,$3,0)
  haltdef
}

alias auth.get {
  if ($1 = ChanServ) { halt }
  if (%getauth.on) { halt }
  auth $1
}

alias auth.remove {
  if (%getauth.on) { halt }
  var %table = auth.cache. $+ $cid
  if $hget(%table,$1) {
    hdel -w %table $1
  }
}
alias auth.whois {
  var %table = auth.cache. $+ $cid
  if !$hget(%table,$1) {
    .whois $1 $1
  }
}
on *:TEXT:*:%pug.chan:{
  var %table = auth.cache. $+ $cid
  if (!$hget(%table,$nick)) { auth.get $nick }
}
on *:JOIN:%pug.chan:{
  .timer 1 1 auth.get $nick
  .timer 1 5 auth.whois $nick
}
on *:ACTION:*:%pug.chan:{
  auth.get $nick
}
on *:OP:%pug.chan:{
  auth.get $nick
}
on *:NICK:{
  auth.remove $nick
  auth.get $newnick
}
on !*:PART:%pug.chan:{
  .timer 1 1 auth.remove $nick
}
on *:QUIT:{
  .timer 1 1 auth.remove $nick
}
on !*:KICK:%pug.chan:{
  .timer 1 1 auth.remove $knick
}
on *:DISCONNECT:{
  ; hfree -w auth.cache. $+ $cid
  ; hfree -w auth.cache. $+ $cid $+ .tmp
}

alias authcheck {
  if (%getauth.on) { halt }
  hfree -w auth.cache. $+ $cid
  var %ipgn.tracker.num = $nick(%pug.chan,0)
  %x = 1 
  while (%x <= %ipgn.tracker.num) {
    authcheck2 $nick(%pug.chan,%x)
    inc %x
  }
}

alias authcheck2 {
  if (!$hget(auth.cache.1,$1)) {
    auth $1
  }
}

alias checkauths {
  var %x = $nick(#ipgn-comp,0)
  var %y = 1
  while (%y <= %x) {
    if (!$hget(auth.cache.1,$nick(#ipgn-comp,%y))) {
      echo -a $nick(#ipgn-comp,%y)
    }
    inc %y
  }
}
