alias backup {
  write -l1 %dir $+ batch\backup.bat "C:\Program Files\WinZip\wzzip.exe" -rP -x@ $+ %dir $+ batch/exclude.txt C:\backup\mIRC-S5-CS-Comp- $+ $date(dd-mm-yy) C:\iPGN-S5 
  write -l2 %dir $+ batch\backup.bat "C:\Program Files\WinZip\wzzip.exe" -rP C:\backup\stats $+ $date(dd-mm-yy) C:\stats
  write -l3 %dir $+ batch\backup.txt put C:\backup\mIRC-S5-CS-Comp- $+ $date(dd-mm-yy) $+ .zip
  run %dir $+ batch\backup.bat
}


menu nicklist {
  .Mass Message {
    var %a $0,%msg $?="msg"
    while %a {
      msg $($+($,%a),2) $codes(%msg)
      dec %a
    }
  }
}
