alias bancheck {

  var %x = 1 
  while (%x <= $lines(%dir $+ batch/banned.cfg)) {
    tokenize 32 $read(%dir $+ batch/banned.cfg,%x)
    ;    echo -a $3
    if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$3,$chr(37),*))) {
      tokenize 37 $ifmatch
      msg %pug.adminchan $1-
    }

    inc %x
  }
}
