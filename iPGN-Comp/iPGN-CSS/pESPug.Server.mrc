on *:start:{
  window -e @rcon
}

on *:INPUT:@rcon:{
  if ($left($1,1) != $chr(47)) { 
    rcon $1-
  }
}

alias warmupcfg {
  set %x $lines(warmup.cfg)
  set %y 1
  while (%y <= %x) {
    rcon $read(warmup.cfg,%y)
    inc %y
  }
  rcon mp_restartgame 1
}

alias rcon.logs {
  if (!$window(@rcon)) { window -e @rcon }
  ; rcon mp_logmessages 1
  ; rcon mp_logfile 1
  ; rcon mp_logdetail 3
  %rcon.myip = $ip
  sockclose iPGNcss
  sockudp -kn iPGNcss %socket.port %rcon.ip %rcon.port 
  rcon logaddress_add %rcon.myip $+ : $+ %socket.port
}

alias rcon {
  if (!$window(@rcon)) { window -e @rcon }
  .run -n $shortfn(C:\iPGN-Comp\iPGN-CSS\SourceRcon\Precompiled Binaries\sourcercon.exe) %rcon.ip %rcon.port %rcon.password " $+ $1- $+ "
  ;  sockudp -kn iPGNcss %socket.port %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " $1-
}

alias logoff {
  rcon log off
  rcon logaddress_del $ip %socket.port
  rcon logaddress_del $ip %socket.port
  sockclose iPGNcss
}

alias logs {
  set %rcon.myip $ip
  rcon logaddress_del %rcon.myip %socket.port 
  rcon logaddress_del %rcon.myip %socket.port 

  sockclose iPGNcss
  .timer -m 1 500 rcon.logs
}

alias scoreupdate {
  if ($regex(%rcon.data,/(Team)(\s)(CT|TERRORIST)(\s)(triggered)(\s)(.*)(\s)(.*)(\s)(\d*)(.*)(\s)(.*)(\s)(\d*)(.*)(.*)/)) {
    if (%match.on = 1) {
      if (%half.1 = 1) {
        rcon say %team.1.name $+ (CT): %CTscore %team.2.name $+ (T): %Tscore
        msg %pug.chan $codes(Progressive 1st-half score:12 %team.1.name $+ (CT): %CTscore 04 %team.2.name $+ (T): %Tscore)
        msg %pug.scorechan $codes(Progressive 1st-half score:12 %team.1.name $+ (CT): %CTscore 04 %team.2.name $+ (T): %Tscore)
        halt
      }
      if (%half.2 = 1) && (!%half.1) {
        rcon say %team.1.name $+ (T): $calc(%Tscore + %CTsh1) %team.2.name $+ (CT): $calc(%CTscore + %Tsh1)
        msg %pug.chan $codes(Progressive 2nd-half score:04 %team.1.name $+ (T): %Tscore 12 $+ %team.2.name $+ (CT): %CTscore $+ . Overall: %team.1.name $+ :12 $calc(%Tscore + %CTsh1) $+  %team.2.name $+ :12 $calc(%CTscore + %Tsh1))
        msg %pug.scorechan $codes(Progressive 2nd-half score:04 %team.1.name $+ (T): %Tscore 12 $+ %team.2.name $+ (CT): %CTscore $+ . Overall: %team.1.name $+ :12 $calc(%Tscore + %CTsh1) $+  %team.2.name $+ :12 $calc(%CTscore + %Tsh1))        
        halt
      }
    }
  }
}

alias hltv.record {
  rcon tv_stoprecord
  .timer 1 1 rcon tv_record CSS-Comp- $+ %team.1.name $+ -vs- $+ %team.2.name $+ - $+ $date(yymmddHHnn) $+ - $+ %win.map
}

alias hltv.stop {
  rcon tv_stoprecord
}

alias sortscores {
  set %rcon.data $strip(%rcon.data)
  if (!%match.on) { 
    halt 
  }
  ;Team TERRORIST triggered Terrorists_Win (CT 3) (T 2)
  if ($regex(%rcon.data,/Team (CT|TERRORIST) triggered (\S*) \x28CT (\d+)\x29 \x28T (\d+)\x29/)) {

    %CTscore = $regml(3)
    %Tscore = $regml(4)
    if (%half.1) {
      addroundwin $regml(1) 1
    }
    if (%half.2) {
      addroundwin $regml(1) 2
    }
  }
  if (%half.1 = 1) {
    if ($calc(%CTscore + %Tscore) = 15) {
      set %CTsh1 %CTscore
      set %Tsh1 %Tscore
      set %half.2 1
      unset %half.1
      msg %pug.chan $codes(The first half is over.12 %team.1.name $+ (CT): %CTscore 04 $+ %team.2.name $+ (T): %Tscore $+ )
      msg %pug.scorechan $codes(The first half is over.12 %team.1.name $+ (CT): %CTscore 04 $+ %team.2.name $+ (T): %Tscore $+ )
      copy -o stats.txt stats.1st.txt
      rcon say The first half is over. %team.1.name $+ : %CTscore %team.2.name $+ : %Tscore 
      .timer 1 1 rcon mp_limitteams 6; mp_restartgame 1
      unset %match.on %CTscore %Tscore
      halt
    }
    else {
      scoreupdate
    }
  }
  if (%half.2 = 1) {
    if ($calc(%CTscore + %Tsh1) = 16) || ($calc(%Tscore + %CTsh1) = 16) || ($calc(%Tscore + %CTscore + %Tsh1 + %CTsh1) = 30) {
      set %CTsh2 %CTscore
      set %Tsh2 %Tscore

      set %CTtotal $calc(%CTsh2 + %Tsh1)
      set %Ttotal $calc(%Tsh2 + %CTsh1)

      rcon say The second half is over. CTs: %CTtotal Ts: %Ttotal

      msg %pug.chan $codes(The second half is over.04 %team.1.name $+ (T): %Ttotal 12 $+ %team.2.name $+ (CT): %CTtotal $+ )
      msg %pug.scorechan $codes(The second half is over.04 %team.1.name $+ (T): %Ttotal 12 $+ %team.2.name $+ (CT): %CTtotal $+ )

      unset %match.on
    }
    else {
      scoreupdate
    }
  }
}

alias rename {
  if ($istok(%team.1.match,$1,32)) {
    if ($3- != %team.1.name $+ . $+ $statname($1)) {
      rcon sm_cexec $chr(35) $+ $2 \"name %team.1.name $+ . $+ $statname($1) $+ \"
    }
  }
  if ($istok(%team.2.match,$1,32)) {
    if ($3- != %team.2.name $+ . $+ $statname($1)) {
      rcon sm_cexec $chr(35) $+ $2 \"name %team.2.name $+ . $+ $statname($1) $+ \"
    }
  }
}
alias statscheck {
  var %x = $0
  var %y = 1
  while (%y <= %x) {
    if ($read(stats.txt,w,$gettok($1-,%y,32) $+ $chr(37) $+ *)) {
      ; done
    }
    else {
      write stats.txt $+($gettok($1-,%y,32),$str($chr(37) $+ 0, $calc($numtok(%allstats,37) - 1)))
    }
    inc %y
  }
}

alias gamereplace {
  var %id = $steamid($1)
  var %id2 = $steamid($2)
  if ($istok(%team.1.names,$1,32)) {
    %team.1.match = $reptok(%team.1.match,%id,%id2,1,32)
    %team.1.names = $reptok(%team.1.names,$1,$2,1,32)
    %team.1.irc = $reptok(%team.1.irc,12 $+ $1 ,12 $+ $2 ,1,32)
  }
  if ($istok(%team.2.names,$1,32)) {
    %team.2.match = $reptok(%team.2.match,%id,%id2,1,32)
    %team.2.names = $reptok(%team.2.names,$1,$2,1,32)
    %team.2.irc = $reptok(%team.2.irc,12 $+ $1 ,12 $+ $2 ,1,32)
  }
  if ($istok(%playerlist,%id,32)) {
    %playerlist = $reptok(%playerlist,%id,%id2,1,32)
  }
  if ($istok(%players,$1,32)) {
    %players = $reptok(%players,$1,$2,1,32)
  }
  rcon say $2 replaced $1 $+ .
  .timer 1 1 rcon mp_freezetime 10
  if ($read(replace.txt,w,* $1)) {
    var %z = $read(replace.txt,$readn)
    write -l $+ $readn replace.txt $reptok($read(replace.txt,1),$gettok(%z,2,32),$2,1,32)
    write -dw $+ $steamid($gettok(%z,2,32)) $+ $chr(37) $+ * stats.txt
  }
  else {
    write replace.txt $1 $2
  }
  if (!$read(stats.txt,w,%id2 $+ $chr(37) $+ *)) {
    write stats.txt $+(%id2,$str($chr(37) $+ 0, $calc($numtok(%allstats,37) - 1)))
  }
  setids
  unset %replace.left
}

alias lo3 {
  if (%lo3) { halt }
  set -u8 %lo3 1
  unset %match.on
  ;renameall
  if (!%half.1) && (!%half.2) {
    set %half.1 1
  }
  if (%half.1 = 1) { 
    hltv.record
    write -c stats.txt
    write -c replace.txt
    ;    statscheck %connected
    unset %pattern.1
    unset %score.*
    msg %pug.chan $codes(The first half of the match is starting)
  }
  if (%half.2 = 1) {
    unset %pattern.2
    copy -o stats.1st.txt stats.txt
    ;    statscheck %connected
    unset %score.*
    msg %pug.chan $codes(The second half of the match is starting) 
  }
  rcon exec lo3cs-comp.cfg
}

on *:udpread:iPGNcss:{
  if ($sockerr > 0) {
    return
  }
  :nextread
  sockread -f %rcon.data
  if ($sockbr == 0) {
    return
  }
  if (%rcon.data == $null) {
    goto nextread
  }
  else {
    if (*" from "*"* iswm %rcon.data) {
      %rcon.data = $null
    }
    if (*"* iswm %rcon.data) {
      %rcon.data = $remove(%rcon.data,")
    }
    if ($gettok(%rcon.data,1,32) == ����challenge) {
      set %rcon.challengenumber $gettok(%rcon.data,3,32)
    }
    if ($left(%rcon.data,6) == ����RL) {
      %rcon.data = $remove(%rcon.data,$left(%rcon.data,30))
      if (%rcon.data == $null) {
        goto nextread
      }
    }
    if (*No*challenge*for*your*address* iswm %rcon.data) {
      logs
    }

    if ($left(%rcon.data,5) == ����l) {
      %rcon.data = $remove(%rcon.data,$left(%rcon.data,5))
      if (%rcon.data == $null) {
        goto nextread
      }
    }
    if (*Rcon:* iswm %rcon.data) {
      %rcon.data = $null
    }
    if (Bad Rcon: rcon* iswm %rcon.data) {
      %rcon.data = $null     
    }
    if ($pos(%rcon.data,$date(yyyy),1) = 9) {
      %rcon.data = $remove(%rcon.data,$left(%rcon.data,24))
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!lo3(?-i))/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        lo3
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!teams(?-i))/)) {
      rcon say %team.1.name $+ $iif(%half.2,(T),(CT)) $+ : %team.1.names ; say %team.2.name $+ $iif(%half.2,(CT),(T)) $+ : %team.2.names
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!password(?-i))(\s)(.*)/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        msg %pug.chan $codes(Server password changed)
        rcon say Password changed to: $gettok($regml(14),1,32) ; sv_password $gettok($regml(14),1,32)    
        set %password $gettok($regml(14),1,32)
        .timer 1 1 rcon sv_password
      }
    }
    if ($regex(%rcon.data,/(\s*)(.*)(<)(\d*)(>)(<)(.*)(>)(<)(TERRORIST)(>)(\s)(triggered Spawned_With_The_Bomb)/)) {
      %bomb.spawn = $regml(2)
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!rs(?-i))/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        rcon mp_restartgame 1
        rcon mp_startmoney 16000
        rcon mp_limitteams 6
        rcon mp_freezetime 0
        unset %match.on
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!rates(?-i))/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        ;        rcon rates_list
        ;        rcon say *** Listing rates in console ***
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!names(?-i))/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        ;       renameall
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!missing(?-i))/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        missing
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!ft(?-i))(\s)(\d+)(.*)/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        rcon mp_freezetime $regml(14)
        rcon say *** mp_freezetime $regml(14) ***
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!kick(?-i))(.*)/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        rcon kick $regml(13)
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!replace(?-i))(.*)/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        if (!%half.2) && (!%match.on) && (!%ctsh1) { 
          rcon say *** Please use !replace in IRC prior to lo3 ***
          halt
        }
        ;rcon status
        pause 1000
        tokenize 32 $regml(13)
        if ($istok(%playerlist,$steamid($1),32)) {
          replace.in $1 $remove($2-,(dead))
          rcon say Replacement requested for $1 $+ .
        }
      }
    }
    if ($regex(%rcon.data,/(.*)<(\d*)><(.*)><(.*)> connected\x2C address(.*)(:)(.*)/)) {
      writeini ips.ini IPS $regml(2) $regml(5)
      %rcon.data = 3 $+ $regml(1) 14has connected
      .timer 1 150 remini ips.ini IPS $regml(3)
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\sdisconnected/)) {

      %rcon.data = 3 $+ $regml(1) 14( $+ $regml(6) $+ )3 disconnected
      rcon say $regml(1) ( $+ $statname($regml(6)) $+ ) disconnected.
      if (%match.on) {
        rcon mp_freezetime 60
        rcon say *** mp_freezetime 60 ***
        %connected = $remtok(%connected,$regml(6),32)
      }
      ;rcon status
    }
    if (*entered the game* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(>)(.*)(entered the game)/)) {
        %rcon.data = 3 $+ $regml(1) 14(# $+ $regml(3) $+ : $regml(6) $+ )3 has entered the game
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(\s)(joined team)(\s)(.*)/)) {
      if ($regml(14) = TERRORIST) {
        %rcon.data = 3 $+ $regml(1) 14joined team4 $regml(14)
      }
      if ($regml(14) = CT) {
        %rcon.data = 3 $+ $regml(1) 14joined team12 $regml(14)
      }
      if ($regml(14) = SPECTATOR) {
        %rcon.data = 3 $+ $regml(1) 14joined team10 $regml(14)
        write -dw $+ $regml(6) $+ * stats.txt
      }
      rename $regml(6) $regml(3) $regml(1)
    }
    if ($regex(%rcon.data,/(.*)<(\d*)><(.*)><> STEAM USERID/)) {
      if ($readini(ips.ini,IPS,$regml(2))) {
        %rcon.data = 3 $+ $regml(1) 14(# $+ $regml(2) $+ : $regml(3) @ $readini(ips.ini,IPS,$regml(2)) $+ ) 3has connected
        write %dir $+ history/ $+ $date(dd-mm-yy) $+ .txt $date(dd-mm-yy hh:nntt) $strip($statname($regml(3)) $chr(40) $+ $regml(3) @ $readini(ips.ini,IPS,$regml(2)) $+ $chr(41) connected.)
        msg %pug.adminchan $codes($statname($regml(3)) $chr(40) $+ $regml(3) @ $readini(ips.ini,IPS,$regml(2)) $+ $chr(41) connected.)
        remini ips.ini IPS $regml(2)
      }
      if (%pug) {
        if (!$istok(%playerlist,$regml(3),32)) && (!%replace.left) && (!$istok(%pes.admins,$regml(3),59)) {
          rcon kick $regml(1) 
          rcon banid 2 $regml(3) kick
          msg %pug.adminchan $codes($statname($regml(3)) $chr(40) $+ $regml(3) $+ $chr(41) was kicked because they're not in the playerlist)
          halt 
        }
        if (%replace.left) && (!$istok(%playerlist,$regml(3),32)) {
          if ($statname($regml(3)) = %replace.left) {
            rcon kick $regml(1)
            halt
          }
          gamereplace %replace.left $statname($regml(3))
        }
        rcon say $regml(1) ( $+ $statname($regml(3)) $+ ) connected.
        %connected = $addtok(%connected,$regml(3),32)
        if (%match.on) { 
          rcon mp_freezetime 10 
          rcon say *** mp_freezetime 10 ***
        }
        ;rcon status
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(TERRORIST)(>)(\s)(triggered Planted_The_Bomb)/)) {
      addplant $regml(6)
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT)(>)(\s)(triggered Defused_The_Bomb)/)) {
      adddefuse $regml(6)
    }
    if ($regex(%rcon.data,/(])(.*)(<)(\d*)(>)(<)(\S*)(>)(<)(>)(\s)(has inconsistent file:)(.*)/)) {
      rcon say custom file disallowed: $regml(13)
      msg %pug.chan $codes(Bad file: $regml(2) ( $+ $regml(7) $+ ) has been kicked for use of custom file: $regml(13))
    }
    if (*attacked* iswm %rcon.data) {
      ;aug (damage 23) (damage_armor 0) (health 48) (armor 0) (hitgroup right leg)

      if ($regex(%rcon.data,/(.*)(<)(\d*)(><)(.*)(><)(TERRORIST)(> attacked)(\s)(.*)(<)(\d*)(><)(.*)(><)(CT)(>)( with )(.*)(damage )(\d*)(\S)(.*)(health)(\s)(-?)(\d*)(\S)/)) {
        if (%hp. [ $+ [ $regml(14) ] ]) {
          if ($regml(27) > 0) {
            set %hp. [ $+ [ $regml(14) ] ] $regml(27)
            var %dmg = $regml(21) 
          }
          elseif ($regml(27) = 0) {
            var %dmg = %hp. [ $+ [ $regml(14) ] ] 
          }
        }
        if (!%hp. [ $+ [ $regml(14) ] ]) {
          set %hp. [ $+ [ $regml(14) ] ] 100
          if ($regml(27) > 0) {
            set %hp. [ $+ [ $regml(14) ] ] $regml(27)
            var %dmg = $regml(21) 
          }
          elseif ($regml(27) = 0) {
            var %dmg = %hp. [ $+ [ $regml(14) ] ]   
          }
        }
        adddamage $regml(5) %dmg
        adddamager $regml(14) %dmg
      }
      if ($regex(%rcon.data,/(.*)(<)(\d*)(><)(.*)(><)(CT)(> attacked)(\s)(.*)(<)(\d*)(><)(.*)(><)(TERRORIST)(>)( with )(.*)(damage )(\d*)(\S)(.*)(health)(\s)(-?)(\d*)(\S)/)) {
        if (%hp. [ $+ [ $regml(14) ] ]) {
          if ($regml(27) > 0) {
            set %hp. [ $+ [ $regml(14) ] ] $regml(27)
            var %dmg = $regml(21) 
          }
          elseif ($regml(27) = 0) {
            var %dmg = %hp. [ $+ [ $regml(14) ] ] 
          }
        }
        if (!%hp. [ $+ [ $regml(14) ] ]) {
          set %hp. [ $+ [ $regml(14) ] ] 100
          if ($regml(27) > 0) {
            set %hp. [ $+ [ $regml(14) ] ] $regml(27)
            var %dmg = $regml(21) 
          }
          elseif ($regml(27) = 0) {
            var %dmg = %hp. [ $+ [ $regml(14) ] ]   
          }
        }
        adddamage $regml(5) %dmg
        adddamager $regml(14) %dmg
      }
      if ($regex(%rcon.data,/(.*)(<)(\d*)(><)(.*)(><)(CT|TERRORIST)(> attacked)(\s)(.*)(<)(\d*)(><)(.*)(><)(CT|TERRORIST)(>)( with )(.*)(damage )(\d*)(\S)(.*)(health)(\s)(-?)(\d*)(\S)/)) {
        if ($regml(7) = $regml(16)) {
          if (%hp. [ $+ [ $regml(14) ] ]) {
            if ($regml(27) > 0) {
              set %hp. [ $+ [ $regml(14) ] ] $regml(27)
            }
          }
          if (!%hp. [ $+ [ $regml(14) ] ]) {
            set %hp. [ $+ [ $regml(14) ] ] 100
            if ($regml(27) > 0) {
              set %hp. [ $+ [ $regml(14) ] ] $regml(27)
            }
          }
        }
      }
      %rcon.data = $null
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(>)\schanged name to\s(.*)/)) {
      ;rename id number oldname
      rename $regml(6) $regml(3) $regml(11)
    }

    if (*killed* iswm %rcon.data) {
      if ($regex(kills,%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(>)(.*)(killed)(\s)(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(> with )(\S*)(.*)/)) {
        rename $regml(kills,19) $regml(kills,16) $regml(kills,14)
        if ($regml(kills,9) = CT) {
          if ($regml(kills,22) = CT) {
            addtk $regml(kills,6) $regml(kills,19)
            adddeath $regml(kills,19) CT
            dec %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 12 $+ $regml(kills,1) $score($regml(kills,3)) teamkilled12 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)
          }
          else {
            inc %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 12 $+ $regml(kills,1) $score($regml(kills,3)) killed4 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)
            adddeath $regml(kills,19) CT
            addkill $regml(kills,6) CT $regml(kills,19)
            addweapon $regml(kills,6) $regml(kills,24)        
          }      
        }
        if ($regml(kills,9) = TERRORIST) {
          if ($regml(kills,22) = TERRORIST) {
            addtk $regml(kills,6) $regml(kills,19)
            adddeath $regml(kills,19) T
            dec %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 4 $+ $regml(kills,1) $score($regml(kills,3)) teamkilled4 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)
          }
          else {
            inc %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 4 $+ $regml(kills,1) $score($regml(kills,3)) killed12 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)
            adddeath $regml(kills,19) T
            addkill $regml(kills,6) T $regml(kills,19)
            addweapon $regml(kills,6) $regml(kills,24)        
          }
        }
        if (%match.on) {
          msg %pug.scorechan $codes(%rcon.data $iif(*headshot* iswm $regml(kills,25),(headshot),$null)) 
        }
      }
    }
    if (*committed suicide* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(>)(\s)(committed suicide with)(\s)(.*)/)) {
        addsuicide $regml(6)
        adddeath $regml(6) $iif($regml(9) = CT,CT,T)
        if ($regml(14) = worldspawn (world)) {
          inc %score. [ $+ [ $regml(3) $+ .deaths ] ]
          if ($regml(9) = CT) {
            %rcon.data = 12 $+ $regml(1) $score($regml(3)) killed self with 10worldspawn
          }
          if ($regml(9) = TERRORIST) {
            %rcon.data = 4 $+ $regml(1) $score($regml(3)) killed self with 10worldspawn
          }
          if (%match.on) {          
            msg %pug.scorechan $codes(%rcon.data)
          }
        }
        if ($regml(14) = grenade) {
          dec %score. [ $+ [ $regml(3) $+ .kills ] ]
          inc %score. [ $+ [ $regml(3) $+ .deaths ] ]
          if ($regml(9) = CT) {
            %rcon.data = 12 $+ $regml(1) $score($regml(3)) killed self with 10grenade
          }
          if ($regml(9) = TERRORIST) {
            %rcon.data = 4 $+ $regml(1) $score($regml(3)) killed self with 10grenade
          }
          if (%match.on) {          
            msg %pug.scorechan $codes(%rcon.data)
          }
        }
        if ($regml(14) = world) {
          dec %score. [ $+ [ $regml(3) $+ .kills ] ]
          inc %score. [ $+ [ $regml(3) $+ .deaths ] ]
          if ($regml(9) = CT) {
            %rcon.data = 12 $+ $regml(1) $score($regml(3)) killed self with 10world
          }
          if ($regml(9) = TERRORIST) {
            %rcon.data = 4 $+ $regml(1) $score($regml(3)) killed self with 10world
          }
        }
      }
    }
    if (*Team CT triggered* iswm %rcon.data) || (*Team TERRORIST triggered* iswm %rcon.data) {
      if (*Team TERRORIST Triggered Target_Bombed* iswm %rcon.data) {
        set %score. [ $+ [ %bomb.planter $+ .kills ] ] $iif(%score. [ $+ [ %bomb.planter $+ .kills ] ],$calc($ifmatch + 3),3)
        unset %bomb.planter
      }
      sortscores
    }
    if (*World triggered Round_Start* iswm %rcon.data) {
      ;rcon status
      if (%match.on) || (%lo3) {
        unset %kills.* alive.* %clutch %hp.*
        set %alive.1 %team.1.match
        set %alive.2 %team.2.match
      }
    }
    if (*Console<0><Console><Console> say * Live! GLHF! * iswm %rcon.data) {
      if (%pug) && (%team.1.match) {
        set %match.on 1
      }
    }
    if (%rcon.data != $null) {
      echo @rcon $asctime(hh:nn:ss) $+ : %rcon.data
      unset %rcon.data
    }
    goto nextread
  }
}

alias missing {
  var %x = %playerlist
  var %z = 1
  while (%z <= $numtok(%connected,32)) {
    %x = $remtok(%x,$gettok(%connected,%z,32),32)
    inc %z
  }
  var %y = 1
  while (%y <= $numtok(%x,32)) {
    %x = $puttok(%x,$statname($gettok(%x,%y,32)),%y,32)
    inc %y
  }
  rcon say *** Players missing: %x ***
}

alias score {
  if (%score. [ $+ [ $1 $+ .kills ] ] = $null) {
    set %score. [ $+ [ $1 $+ .kills ] ] 0
  }
  if (%score. [ $+ [ $1 $+ .deaths ] ] = $null) {
    set %score. [ $+ [ $1 $+ .deaths ] ] 0
  }
  return $chr(91) $+ %score. [ $+ [ $1 $+ .kills ] ] $+ / $+ %score. [ $+ [ $1 $+ .deaths ] ] $+ $chr(93)
}


on *:sockwrite:rcon:{
  if ($sockerr) {
    aline @rcon $sock().wserr
  }
}

on *:close:@rcon:{
  if (%rcon.scorebot == 1) {
    set %rcon.scorebot 0
  }
  set %rcon.myip $ip
}
