on *:start:{
  resetserver 1
  resetserver 2
  resetserver 3
  resetserver 4
  resetserver 5
}

on *:TEXT:!book *:?:{
  ;## SYNTAX: !book <servernumber> <steamid>
  if ($2 <= 5) && ($2 isnum) && (*STEAM_0:*:* iswm $3) {
    if ($readini(bookables.ini,$2,inuse) = 0)  {
      .run rcon6.exe $readini(bookables.ini,$2,ip) $+ , $+ $readini(bookables.ini,$2,port) $+ , $+ $readini(bookables.ini,$2,rcon) $+ ,ipgn_steamid " $+ $strip($3) $+ "
      .run rcon6.exe $readini(bookables.ini,$2,ip) $+ , $+ $readini(bookables.ini,$2,port) $+ , $+ $readini(bookables.ini,$2,rcon) $+ ,ipgn_name " $+ $nick $+ "
      .run rcon6.exe $readini(bookables.ini,$2,ip) $+ , $+ $readini(bookables.ini,$2,port) $+ , $+ $readini(bookables.ini,$2,rcon) $+ ,sv_password ""
      writeini bookables.ini $2 inuse 1
      .timerbookable $+ $2 1 7200 resetserver $2
      .timerbookable $+ $2 $+ check 0 601 servercheck $2
      .msg $nick $codes($readini(bookables.ini,$2,ip) $+ : $+ $readini(bookables.ini,$2,port) $+ , booked by $strip($3) for two hours)
    }
    else {
      .msg $nick $codes(Server # $+ $2 is in use)
    }
  }
  else { 
    .msg $nick $codes(Syntax: !book <servernumber> <steamid>)
  }
}

on *:TEXT:!servers:#scrim,#ipgn-board:{
  var %i = 1 
  while (%i <= 5) {
    .notice $nick $codes(Server $chr(35) $+ %i ( $+ $readini(bookables.ini,%i,ip) $+ : $+ $readini(bookables.ini,%i,port) $&
      - $readini(bookables.ini,%i,loc) $+ ): $iif($readini(bookables.ini,%i,inuse) = 1,In use ( $+ $duration($timer(bookable $+ %i).secs) remaining),Available))
    inc %i
  }
  .notice $nick $codes(To book a server: /msg $me !book <servernumber> <steamid> ie. !book 2 STEAM_0:0:1337)
}

on *:TEXT:!reset *:#ipgn-admins,#ipgn-board:{
  if ($2 isnum) && ($2 <= 5) {
    resetserver $2
    .msg $chan $codes(Bookable server $chr(35) $+ $2 has been reset. Only reset in-use servers that are empty.)
  }
  else {
    .msg $chan $codes(Syntax: !reset <servernumber> ie. !reset 2 - Only reset in-use servers that are empty.)
  }
}

alias resetserver {
  run $readini(bookables.ini,$1,restart) 
  writeini bookables.ini $1 inuse 0
  .timerbookable $+ $1 off
  .timerbookable $+ $1 $+ check off
}

alias servercheck {
  if (%servercheck = 1) { .timerbkservercheck $+ $1 1 5 servercheck $1 }
  else {
    set %servercheck 1
    set %servercheck.number $1
    bk.set $readini(bookables.ini,$1,ip) $readini(bookables.ini,$1,port) $readini(bookables.ini,$1,rcon)
    rcon.bk.challenge
    .timerbkcheck $+ $1 1 1 rcon.bk status
    .timerservercheckclear 1 10 set %servercheck 0
  }
}

alias bk.set {
  ;USAGE: bk.set IP PORT RCON
  %socket.bk.port = 8111
  %rcon.bk.ip = $1
  %rcon.bk.port = $2
  %rcon.bk.password = $3
}

alias rcon.bk.challenge {
  sockclose iPGNrcon.bk
  sockudp -kn iPGNrcon.bk %socket.bk.port %rcon.bk.ip %rcon.bk.port ����challenge rcon
}

alias rcon.bk {
  sockudp -kn iPGNrcon.bk %socket.bk.port %rcon.bk.ip %rcon.bk.port ����rcon %rcon.bk.challengenumber " $+ %rcon.bk.password $+ " $1-
}

on *:udpread:iPGNrcon.bk:{
  if ($sockerr > 0) {
    return
  }
  :nextread
  sockread -f %rcon.bk.data
  if ($sockbr == 0) {
    return
  }
  if (%rcon.bk.data == $null) {
    goto nextread
  }
  else {
    if (*" from "*"* iswm %rcon.bk.data) {
      %rcon.bk.data = $null
    }
    if (*"* iswm %rcon.bk.data) {
      %rcon.bk.data = $remove(%rcon.bk.data,")
    }
    if ($gettok(%rcon.bk.data,1,32) == ����challenge) {
      set %rcon.bk.challengenumber $gettok(%rcon.bk.data,3,32)
    }
    if ($left(%rcon.bk.data,7) == ����log) {
      %rcon.bk.data = $remove(%rcon.bk.data,$left(%rcon.bk.data,32))
      if (%rcon.bk.data == $null) {
        goto nextread
      }
    }
    if ($regex(%rcon.bk.data,/^players\s\x3A\s+(\d+)\s+active \x28\d+ max\x29/)) {
      if ($regml(1) = 0) {
        echo -a Resetting server %servercheck.number
        resetserver %servercheck.number
      }
      set %servercheck 0
    }
    if (%rcon.bk.data != $null) {
      unset %rcon.bk.data
    }
    goto nextread
  }
}
