on *:START:{
  .timercheatyg 0 3600 cheatyg
}

alias cheatyg {
  write -c cheatyg.txt

  ; Set socket name
  var %sock = cheatyg $+ $ticks

  ; Open socket and send data to it
  sockopen %sock cheatyg.net 80
}

on *:SOCKOPEN:cheatyg*:{
  ; Define command
  var %a = sockwrite -n $sockname

  ; First send GET/POST, next send Host
  %a GET /?page_id=46 HTTP/1.1
  %a Host: cheatyg.net

  ; Force connection to close
  %a Connection: close

  ;Just-to-make-sure requests:
  %a Accept: */*
  %a Accept-Charset: *
  %a Accept-Encoding: *
  %a Accept-Language: *
  %a User-Agent: Mozilla/5.0

  %a $crlf

}

on *:SOCKREAD:cheatyg*:{
  sockread %tmp
  if (*banid 0.0* iswm %tmp) {
    write cheatyg.txt $gettok(%tmp,3-,32)
  }
}

on *:TEXT:*:#cheatyg:{
  if ($regex($1-,/(STEAM_\d:\d:\d+)/)) {
    if ($read(cheatyg.txt,w,$regml(1))) {
      msg $chan $codes($regml(1) = CHEAT (match found in cheatyG database))
    }
    else {
      msg $chan $codes($regml(1) = legit (not found in cheatyG database))
    }
  }
}
