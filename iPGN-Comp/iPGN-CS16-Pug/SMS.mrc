on *:TEXT:*:#ipgn-board:{
  if ($1 = !sms) {
    if ($readini(sms.ini,numbers,$2) || $2 isnum) && ($len($3) <= 11) && ($len($4-) <= 160) && ($4) {
      if ($2 isnum) { var %target = $2 }
      else { var %target = $readini(sms.ini,numbers,$2) }
      sms get /httpsend.php $+(usr=admin@ipgn.com.au&pass=caj00n&mess=,$4-,&to=,%target,&from=,$3)
    }
    else { mnmsg $chan $codes(Syntax: !sms <name|number> <alias> <message>) }
  }
}
alias sms {
  write -c sms.txt
  ; Open a new window, where we will output the data
  window -e @sms
  linesep @sms

  ; Set first parameter, which defaults to GET
  var %method = $iif($1,$1,GET)

  ; Set second parameter, which defautls to /
  var %page = $iif($2,$2,/)

  %string = $urlencode_string($3-)

  ; Set socket name
  var %sock = sms $+ $ticks

  ; Open socket and send data to it
  sockopen %sock smsmanager.com.au 80
  sockmark %sock %method %page %string
}

on *:SOCKOPEN:sms*:{
  ; Connection has been made
  aline @sms Connection established...
  aline @sms MODE: $getmark($sockname,1-)

  ; Define command
  var %a = sockwrite -n $sockname


  ; First send GET/POST, next send Host
  %a $getmark($sockname,1-2) $+ $iif($getmark($sockname,1) == GET && %string,$+(?,%string)) HTTP/1.1
  %a Host: smsmanager.com.au

  ; If method is POST, we need to include 2 extra parameters
  if ($getmark($sockname,1) == POST) {
    %a Content-Length: $len(%string)
    %a Content-Type: application/x-www-form-urlencoded
  }

  ; Force connection to close
  %a Connection: close

  ;Just-to-make-sure requests:
  %a Accept: */*
  %a Accept-Charset: *
  %a Accept-Encoding: *
  %a Accept-Language: *
  %a User-Agent: Mozilla/5.0

  %a $crlf

}

on *:SOCKREAD:sms*:{
  sockread %tmp
  if ($regex(%tmp,/^success$/)) {
    msg #ipgn-board $codes(Success!)
  }

  write sms.txt %tmp
}
