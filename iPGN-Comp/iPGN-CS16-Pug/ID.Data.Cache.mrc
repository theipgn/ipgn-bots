; enforcing aggressive caching. all data is cached, and the item is only updated
; if the cache time is less than the last file modification time (meaning cache
; will automatically update on a request-by-request basis)
; data is stored in the form NAME: <uid> DATA: <last mtime>%<auth>%<steamid>%<statname>
alias id.cache.table {
  var %table.name = id.cache

  if (!$hget(%table.name)) {
    hmake %table.name 100
  }

  return %table.name
}

alias id.cache.add {
  var %table = $id.cache.table()

  hadd %table $1 $+($ctime,$chr(37),$2)
}

alias id.cache.get {
  ; searches the cache for the specified data. will try cache first,
  ; and will then attempt to recache if out-of-date/no result
  if ($1 isnum) {
    if ($id.cache.getbyuid($1)) {
      return $v1
    }
    else {
      return $id.cache.getbyuid($1, $true)
    }
  }
  elseif (STEAM_* iswm $1) {
    ; $1 = SteamID
    if ($id.cache.getbysid($1)) { 
      return $v1
    }
    else {
      return $id.cache.getbysid($1, $true)
    }
  }
  elseif ($id.cache.getbyauthname($1) || $id.cache.getbystatname($1)) {
    return $v1
  }
  elseif ($id.cache.getbyauthname($1, $true) || $id.cache.getbystatname($1, $true)) {
    return $v1
  }
}

alias id.cache.profile {
  ;hfree $id.cache.table()

  hdel $id.cache.table 18
  var %ticks = $ticks

  ;var %x = 50
  ;while (%x > 0) {
  ;  id.cache.get %x

  ;  dec %x
  ;}
  id.cache.get 18

  debug_msg timing id.cache.profile %ticks
}

alias id.cache.getid {
  var %table = $id.cache.table()

  var %search
  if ($1 = sid) {
    %search = $+(*,$chr(37),$2,$chr(37),*)
  }
  elseif ($1 = authname) {
    %search = $+(*,$chr(37),$2,$chr(37),*)
  }
  elseif ($1 = statname) {
    %search = $+(*,$chr(37),$2)
  }
  else {
    debug_msg cache INVALID SEARCH TYPE FOR ID.CACHE.GETID
    return
  }

  debug_msg cache Getting ID for $2 using pattern ( $+ $1 $+ ) %search

  return $hfind(%table,%search,1,w).data
}

alias id.cache.refreshitem {
  var %search = $1, %fdata

  debug_msg cache Attempting to refresh cache item using %search

  if (%search = uid) {
    %fdata = $read(%dir $+ ids.txt,$2)
  }
  else {
    %fdata = $read(%dir $+ ids.txt,w,%search)
  }

  if (%fdata) {
    debug_msg cache Data found for search %search $+ : %fdata
    ; recache
    id.cache.add $readn %fdata

    debug_msg cache Returning data %fdata
    return %fdata
  }
}

alias id.cache.getbyuid {
  ; $1 = id, $2 = whether to look at file or not in the case of no result (true/false)
  var %table = $id.cache.table()
  var %mtime = $file(%dir $+ ids.txt).mtime

  ; <last mtime>%<auth>%<steamid>%<statname>
  var %udata = $hget(%table,$1)
  debug_msg cache Search: %search Table: %table Mtime: %mtime Cache data: %udata

  if ((%udata && $gettok(%udata,1,37) < %mtime) || (!%udata && $2)) {
    ; we need to recache this if possible
    debug_msg cache Out of date or non-existant item $1

    return $id.cache.refreshitem(uid, $1)
  }
  elseif (%udata) {
    return $gettok(%udata,2-4,37)
  }
}

alias id.cache.getbysid {
  ; $1 = id, $2 = whether to look at file or not in the case of no result (true/false)
  var %search = $+(*,$chr(37),$1,$chr(37),*), %table = $id.cache.table()
  var %mtime = $file(%dir $+ ids.txt).mtime

  var %udata = $hget(%table,$id.cache.getid(sid,$1))

  debug_msg cache Search: %search Table: %table Mtime: %mtime Cache data: %udata

  if ((%udata && $gettok(%udata,1,37) < %mtime) || (!%udata && $2)) {
    ; we need to recache this if possible
    debug_msg cache Out of date or non-existant item $1

    return $id.cache.refreshitem(%search)
  }
  elseif (%udata) {
    return $gettok(%udata,2-4,37)
  }
}

alias id.cache.getbyauthname {
  ; $1 = id, $2 = whether to look at file or not in the case of no result (true/false)
  var %search = $+($1,$chr(37),*), %table = $id.cache.table()
  var %mtime = $file(%dir $+ ids.txt).mtime

  ; need a special search for authname because we've added another token in the cache
  ; but not the file
  var %udata = $hget(%table,$id.cache.getid(authname,$1))

  debug_msg cache Search: %search Table: %table Mtime: %mtime Cache data: %udata

  if ((%udata && $gettok(%udata,1,37) < %mtime) || (!%udata && $2)) {
    ; we need to recache this if possible
    debug_msg cache Out of date or non-existant item $1

    return $id.cache.refreshitem(%search)
  }
  elseif (%udata) {
    return $gettok(%udata,2-4,37)
  }
}

alias id.cache.getbystatname {
  ; $1 = id, $2 = whether to look at file or not in the case of no result (true/false)
  var %search = $+(*,$chr(37),$1), %table = $id.cache.table()
  var %mtime = $file(%dir $+ ids.txt).mtime

  var %udata = $hget(%table,$id.cache.getid(statname,$1))

  debug_msg cache Search: %search Table: %table Mtime: %mtime Cache data: %udata

  if ((%udata && $gettok(%udata,1,37) < %mtime) || (!%udata && $2)) {
    ; we need to recache this if possible
    debug_msg cache Out of date or non-existant item $1

    return $id.cache.refreshitem(%search)
  }
  elseif (%udata) {
    return $gettok(%udata,2-4,37)
  }
}
