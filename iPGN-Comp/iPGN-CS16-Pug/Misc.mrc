on *:CONNECT:{
  .timer 07:00 1 1 timerbackup 0 86400 backup
}

alias backup {
  write -l1 %dir $+ batch\backup.bat "C:\Program Files\WinZip\wzzip.exe" -rP -s"bumbl3b33" -x@ $+ %dir $+ batch/exclude.txt C:\iPGN-Comp\backup\mIRC-S10-CS-Comp- $+ $date(dd-mm-yy) C:\iPGN-Comp\iPGN-S6
  ; Bot folders still named S6 hence above
  write -l2 %dir $+ batch\backup.bat "C:\Program Files\WinZip\wzzip.exe" -rP -s"bumbleb33" C:\iPGN-Comp\backup\stats-s10- $+ $date(dd-mm-yy) C:\iPGN-Comp\stats-s10
  write -l3 %dir $+ batch\backup.txt put C:\iPGN-Comp\backup\mIRC-s8-CS-Comp- $+ $date(dd-mm-yy) $+ .zip
  write -l4 %dir $+ batch\backup.txt put C:\iPGN-Comp\backup\stats-s8- $+ $date(dd-mm-yy) $+ .zip
  run %dir $+ batch\backup.bat
  .timer 1 300 run %dir $+ batch\backupftp.bat
}

menu nicklist {
  .Mass Message {
    var %a $0,%msg $?="msg"
    while %a {
      msg $($+($,%a),2) $codes(%msg)
      dec %a
    }
  }
}

alias msgall {
  var %x = $nick(#ipgn-comp,0)
  var %y = 1
  var %z = 0
  while (%y <= %x) {
    if (*.gamesurge* !iswm $address($nick(#ipgn-comp,%y),3)) && (*.ipgn* !iswm $address($nick(#ipgn-comp,%y),3)) && $address($nick(#ipgn-comp,%y),3) {
      var %n = $nick(#ipgn-comp,%y)
      msg $nick(#ipgn-comp,%y) $codes(You are encouraged to use //mode $nick(#ipgn-comp,%y) +x as it will help reduce bot lag and you will join games faster (please add to your perform))
      inc %z    
    }
    inc %y
  }
  echo -a %z
}


alias addiptostats {
  write -c statsall.txt
  var %a = 1
  var %b = $lines(%dir $+ statsall.txt)
  while (%a <= %b) {
    write statsall.txt $read(%dir $+ statsall.txt,%a) $+ $chr(37) $+ 0.0.0.0
    inc %a
  }
}

alias idmismatch {
  var %idnum
  if ($read(%dir $+ statsall.txt,w,* $+ $1 $+ * )) {
    %idnum = $readn
    echo -a $v1
  }
  echo -a MATCHES TO ID %idnum $read(%dir $+ ids.txt,%idnum)
  if ($read(%dir $+ ids.txt,w,* $+ $1 $+ * )) {
    echo -a REAL ID NUM FOR $1 : $readn LINE DATA: $v1
  }
  else {
    echo -a NO REAL ID MATCH
  }
}

alias checkidmm {
  var %x = 1
  var %y = $lines(%dir $+ statsall.txt) 
  fopen statfile %dir $+ statsall.txt
  if ($ferr) { echo -a ERROR OPENING FILE WITH HANDLE STATFILE | halt }
  fopen idfile %dir $+ ids.txt
  if ($ferr) { echo -a ERROR OPENING FILE WITH HANDLE IDFILE | halt }
  while (%x <= %y) {
    var %steamid, %id, %idsteamid, %idid
    .fseek -l statfile %x
    .fseek -l idfile %x
    if ($fread(statfile)) {
      %steamid = $gettok($v1,1,37)
      %id = %x
    }
    if ($fread(idfile)) {
      var %linedata = $v1
      %idsteamid = $gettok(%linedata,2,37)
      %idid = %x
    }
    if (%steamid != %idsteamid) {
      echo -a MISMATCH BEGINS AT ID %x WITH STEAMID %steamid
      echo -a ID FILE STEAMID AT %x IS %idsteamid
      break
    }

    inc %x
  }
  fclose statfile
  fclose idfile
}

alias staffmsg {
  var %x = $nick(%pug.adminchan,0)
  var %y = 1
  while (%y <= %x) {
    var %nick = $nick(%pug.adminchan,%y)
    if ($authed($auth(%nick))) {
      msg %nick Staff message from $nick $+ : $codes($1-)
    }
    inc %y
  }
}
