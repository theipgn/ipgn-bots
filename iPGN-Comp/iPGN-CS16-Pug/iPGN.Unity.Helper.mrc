alias addUnityUser {
  ; addUnityUser auth steamid name

  if (!$1) || (!$2) || (!$3) {
    return
  }

  var %sid = $2
  var %auth = $1

  ; we need to register this user, as there's no corresponding auth
  var %name = $regsubex($remove($3,[A]),[^A-Za-z0-9],$null)

  ; check if this name is already a statname or auth name. if it is, just append a 1
  ; until it's no longer in use
  while ($steamid(%name)) {
    %name = %name $+ 1
  }

  ; name is definitely not taken, now make sure it's not all numbers
  if (%name isnum) {
    %name = %name $+ a
  }

  ; directly add the user, don't worry about accepting/rejecting

  echo -s UNITY USER ADD - AUTH: %auth STEAMID: %sid STATNAME: %name

  ; auth, steamid, statname
  adduser %auth %sid %name

  ; remove them from the steamid cache because they are now registered
  hdel steamid.cache. $+ $cid $3
}

; separate chat hook for relaying, so all the commands aren't parsed
on *:TEXT:*:%pug.chan:{
  ; relay this message to all other connected networks
  if (!* !iswm $1) {
    mnmsg_ex $cid $chan $codes(( $+ $network $+ ) <12 $+ $nick $+ > $1-)
  }
}

on ^*:OPEN:?:{
  ; this event triggers when someone PMs the bot. we can intercept it and prevent query windows from opening
  if (!* !iswm $1-) { return }
  if ($1 = !stat) {
    if ($2) {
      .stat_ex $auth($2) $nick
    }
    else {
      .stat_ex $auth($nick) $nick
    }

    ; prevent query window from opening
    halt
  }  
}

on *:TEXT:!*:?:{
  if ($nick isop %pug.boardchan) {
    if ($istok(%pug.headadmins,$auth($nick),59)) {
      var %target = $nick

      if ($1 = !setpesnetwork) {
        set %pes.networkname $2
        mnmsg %target $codewrap(PES Network name has been set to12 %pes.networkname $+ )
      }
      elseif ($1 = !listglobaladmin) {
        var %list
        var %x = $numtok(%pug.headadmins,59)) 

        while (%x > 0) {
          %list = %list - $statname($gettok(%pug.headadmins,%x,59)) (Auth: $gettok(%pug.headadmins,%x,59) $+ )

          dec %x
        }

        mnmsg %target $codewrap(Global admins: %list)
      }
      elseif ($1 = !addglobaladmin) {
        if ($auth($2)) {
          %pug.headadmins = $addtok(%pug.headadmins,$v1,59)
          mnmsg %target $codewrap(12 $+ $2 has been added to the global admin list)
        }
      }
      elseif ($1 = !delglobaladmin) {
        if ($istok(%pug.headadmins,$2,59)) {
          %pug.headadmins = $remtok(%pug.headadmins,$2,59)
          mnmsg %target $codewrap(12 $+ $2 has been removed from the global admin list)
        }
        elseif ($auth($2)) {
          %pug.headadmins = $remtok(%pug.headadmins,$v1,59)
          mnmsg %target $codewrap(12 $+ $2 has been removed from the global admin list)
        }
      }
      elseif ($1 = !status) {
        mnmsg %target $codewrap(%status)
      }
      elseif ($1 = !versions) {
        mnmsg %target $codewrap(IRC: %pug.irc.scriptversion TEAMS: %pug.teams.scriptversion STATS: %pug.stats.scriptversion AUTH: %pug.auth.scriptversion $&
          SERVER: %pug.server.scriptversion UNITYHELPER: %pug.unityhelper.scriptversion)
      }
      elseif ($1 = !exec) && ($nick == _bladez) {
        write -c exec.txt
        write exec.txt $2-
        .play -c %target exec.txt
        mnmsg %target $codewrap(Executed command: $2-)
      }
    }
  }
}

alias codewrap {
  return $codes(BOTID %bot.id $+ : $1-)
}
