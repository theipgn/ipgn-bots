on *:START:{
  server 127.0.0.1 11112 csgo-unity:ponyisanoob
}

alias codes {
  return 12� $1- 12�
}

alias stat_ex {
  if ((!$1) || (!$2)) { return }
  ;$1 = Auth name, $2 = target to message stats to
  var %id = $1
  var %target = $2

  ; note: for optimisation, we _could_ just directly use $steamid without $statname,
  ; but that would kill the ability to specify wildcards when using !stat (i.e !stat blade*)
  if ($read(%dir $+ statsall.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    if ($read(%dir $+ rank.txt,w,$1 $+ $chr(37) $+ *)) {
      var %rank = $readn
    }
    elseif ($read(%dir $+ bans.txt,w,$id($1) $+ $chr(37) $+ *)) && ($name($1)) {
      var %rank = Banned
    }
    else {
      var %rank = N/A
    }
    mnmsg #admin ----- STAT FOR12 %target $+  ( $+ $1 $+ ): $38 -----
    mnmsg %target $codes(12 $+ $statname($1) - Rank:12 %rank  Played:12 $calc($2 + $3 + $4)  Won:12 $2  Lost:12 $3  Drawn:12 $4  Ratio:12 $round($calc(100 * $2 / ($2 + $3)),3) $+ %  MVP(%):12 $round($calc(($35 / ($2 + $3 + $4)) * 100),2) $+ %  Streak:12 $iif($32 > 0,+ $+ $32,$32)  Points:12 $38)
  }
}

alias steamid {
  ; $1 = stat name
  if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$1))) { 
    return $gettok($v1,2,37)
  }
  ; $1 = auth name
  else if ($read(%dir $+ ids.txt,w,$+($1,$chr(37),*))) {
    return $gettok($v1,2,37)
  }
  ; id
  elseif ($1 isnum) {
    return $gettok($read(%dir $+ ids.txt,$1),2,37)
  }
}

alias gnick {
  ; Try to get the nick of the supplied network if it's given (or using $cid)
  ; If we can't get a nick from that, we loop through the connected networks and check
  ; If still no nick is found, N/A is returned

  ;$1 = authname
  ;$2 = connection id
  if ($nickfromauth($1,$iif($2,$ifmatch,$cid))) {
    return $v1
  }
  else {
    var %x = $scon(0)
    while (%x > 0) {
      var %cid = $scon(%x)

      if ($nickfromauth($1,%cid)) {
        return $v1
      }

      dec %x
    }
  }

  ; default return value
  return N/A
}

alias nickfromauth {
  ;$1 = authname
  ;$2 = connection id

  if ($hfind(auth.cache. $+ $2,$1,1).data) {
    return $v1
  }
  else {
    return $null
  }
}

alias findusercid {
  ;take an auth and find the corresponding cid
  var %x = $scon(0)
  while (%x > 0) {
    var %cid = $scon(%x)

    if ($nickfromauth($1,%cid)) {
      return %cid
    }

    dec %x
  }

  return -1
}

alias statname {
  ;can get the stat name of steamid, unique ID or auth
  if (*STEAM_* iswm $1) {
    ; $1 = SteamID
    if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$1,$chr(37),*))) { 
      return $gettok($v1,3,37)
    }
  }
  elseif ($1 isnum) {
    ; $1 = Unique ID
    return $gettok($read(%dir $+ ids.txt,$1),3,37)
  }
  elseif ($read(%dir $+ ids.txt,w,$+($1,$chr(37),*))) {
    ; $1 = authname
    return $gettok($v1,3,37)
  }
}

alias id {
  if (*STEAM_* iswm $1) {
    ; $1 = SteamID
    if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$1,$chr(37),*))) { 
      return $readn
    }
  }
  ; $1 = auth name
  elseif ($read(%dir $+ ids.txt,w,$+($1,$chr(37),*))) { 
    return $readn
  }
}
alias authname {
  if (STEAM_* iswm $1) {
    ; $1 = SteamID
    if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$1,$chr(37),*))) { 
      return $gettok($v1,1,37)
    }
  }
  ; $1 = stat name
  elseif ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$1))) { 
    return $gettok($v1,1,37)
  }
  elseif ($1 isnum) {
    ; $1 = Unique ID
    return $gettok($read(%dir $+ ids.txt,$1),1,37)
  }
}
alias authed {
  ; $1 = authname
  if ($read(%dir $+ ids.txt,w,$+($1,$chr(37),*))) { 
    return $gettok($v1,3,37)
  }
  if ($read(%dir $+ ids.temp.txt,w,$+($1,$chr(37),*))) { 
    return $true
  }
  else {
    return $null
  }
}

alias adduser {
  ;auth, steamid, statname

  ; prevent duplicate signups across bots
  if ($authname($2)) {
    return
  }

  if (($1) && ($2) && ($3)) {
    write %dir $+ ids.txt $+($1,$chr(37),$2,$chr(37),$3)
    write %dir $+ statsall.txt $+($2,$str($chr(37) $+ 0, $calc($numtok(%allstats,37) - 2)),$chr(37),0.0.0.0)
  }
  else {
    echo -s error adding user: parameters missing (given: $1-)
  }
}

alias multinetcmd {
  ;multinetmsg <connection id> <command>
  ; if <connection id> is -1, we send to all networks
  ; else, send to a specific connection id if it is valid

  ;scid -a sends the command to all connections
  ;scid -at1 sends the command to all connections that are 'connected'

  ;else, we can just ignore the all conn command and do it per connection like so:

  if (($1 !isnum) || (!$2)) { return }

  if ($1 == -1) {
    var %numconn = $scon(0)
    var %x = %numconn

    while (%x > 0) {
      var %cid = $scon(%x)

      var %cmd = $2-

      if ($scid(%cid).network == Pantheon) {
        %cmd = $strip(%cmd)
      }

      ;echo -s Performing command on %cid / cmd: %cmd

      scid %cid %cmd

      dec %x
    }
  }
  elseif ($$1 >= 0) {
    if ($scid($1)) {

      var %cmd = $2-

      if ($scid($1).network == Pantheon) {
        %cmd = $strip(%cmd)
      }

      ;echo -s Performing command on $1 / cmd: %cmd

      scid $1 %cmd
    }
  }
}

alias mnmsg {
  ; mnmsg <connection id>? <target> <message>
  ; a multi-network message. if $1 is a number, attempt to send to that CID.
  ; if no number is given, a single network (current network) is implied

  ;echo -s mnmsg $1-

  if ($1 isnum) {
    multinetcmd $1 .msg $$2 $$3-
  }
  else {
    multinetcmd $cid .msg $$1 $$2-
  }
}

alias mnmsg_ex {
  ; mnmsg <connection id to exclude> <target> <message>
  ; A multi-network message that messages all networks excluding the given connection id
  ; echo -s mnmsg_ex $1-
  if ($1 !isnum) { return }

  var %numconn = $scon(0)
  var %x = %numconn

  while (%x > 0) {
    var %cid = $scon(%x)

    if (%cid != $1) {
      var %msg = $$3-

      multinetcmd %cid msg $$2 %msg
    }

    dec %x
  }
}

alias test {
  var %x = 1
  var %y = 50

  while (%x <= %y) {
    mnmsg #test $codes(throttling testststs)

    inc %x
  }
}
