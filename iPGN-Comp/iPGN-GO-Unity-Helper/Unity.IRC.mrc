on *:START:{
  server 127.0.0.1 11112 csgo-unity:ponyisanoob
}

alias debug_msg {
  ; Logs a debug message to @debug
  ; $1 is the function debug is being called from, $2- is the debug message
  if (!$window(debug)) {
    window -e @debug
  }

  var %time = $time([(ddd)HH:nn:ss])

  var %ccode = 9
  var %body = $2-

  if ($1 = end) {
    %ccode = 7
  }
  if ($1 = err) {
    %ccode = 4
  }
  if ($1 = timing) {
    %ccode = 11

    ; timing debug_msg is debug_msg timing <function> <start time>
    var %body = $2 took $calc($ticks - $3) $+ ms to complete
  }

  echo @debug %time %ccode $+ $upper($1) $+ : %body
}

alias codes {
  return 12� $1- 12�
}

alias stat_ex {
  if ((!$1) || (!$2)) { return }
  ;$1 = Auth name, $2 = target to message stats to
  var %id = $1
  var %target = $2

  ; note: for optimisation, we _could_ just directly use $steamid without $statname,
  ; but that would kill the ability to specify wildcards when using !stat (i.e !stat blade*)
  if ($read(%dir $+ statsall.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    if ($read(%dir $+ rank.txt,w,$1 $+ $chr(37) $+ *)) {
      var %rank = $readn
    }
    elseif ($read(%dir $+ bans.txt,w,$id($1) $+ $chr(37) $+ *)) && ($name($1)) {
      var %rank = Banned
    }
    else {
      var %rank = N/A
    }
    mnmsg #admin ----- STAT FOR12 %target $+  ( $+ $1 $+ ): $38 -----
    mnmsg %target $codes(12 $+ $statname($1) - Rank:12 %rank  Played:12 $calc($2 + $3 + $4)  Won:12 $2  Lost:12 $3  Drawn:12 $4  Ratio:12 $round($calc(100 * $2 / ($2 + $3)),3) $+ %  MVP(%):12 $round($calc(($35 / ($2 + $3 + $4)) * 100),2) $+ %  Streak:12 $iif($32 > 0,+ $+ $32,$32)  Points:12 $38)
  }
}

alias gnick {
  ; Try to get the nick of the supplied network if it's given (or using $cid)
  ; If we can't get a nick from that, we loop through the connected networks and check
  ; If still no nick is found, N/A is returned

  ;$1 = authname
  ;$2 = connection id
  if ($nickfromauth($1,$iif($2,$ifmatch,$cid))) {
    return $v1
  }
  else {
    var %x = $scon(0)
    while (%x > 0) {
      var %cid = $scon(%x)

      if ($nickfromauth($1,%cid)) {
        return $v1
      }

      dec %x
    }
  }

  ; default return value
  return N/A
}

alias nickfromauth {
  ;$1 = authname
  ;$2 = connection id

  if ($hfind(auth.cache. $+ $2,$1,1).data) {
    return $v1
  }
  else {
    return $null
  }
}

alias findusercid {
  ;take an auth and find the corresponding cid
  var %x = $scon(0)
  while (%x > 0) {
    var %cid = $scon(%x)

    if ($nickfromauth($1,%cid)) {
      return %cid
    }

    dec %x
  }

  return -1
}

alias id {
  ;implicitly update the cache when doing this shiz
  if (STEAM_* iswm $1) {
    ; $1 = SteamID
    if ($id.cache.getid(sid, $1)) {
      return $v1
    }
    elseif ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$1,$chr(37),*))) {
      id.cache.add $readn $v1

      return $readn
    }
  }
  ; $1 = auth name
  elseif ($id.cache.getid(authname, $1)) {
    return $v1
  }
  elseif ($id.cache.getid(statname, $1)) {
    return $v1
  }
  elseif ($read(%dir $+ ids.txt,w,$+($1,$chr(37),*))) { 
    id.cache.add $readn $v1

    return $readn
  }
}
alias statname {
  if ($id.cache.get($1)) {
    var %v = $gettok($v1,3,37)

    debug_msg id_data Got statname %v for $1

    return %v
  }
}

alias authname {
  if ($id.cache.get($1)) {
    return $gettok($v1,1,37)
  }
}

alias steamid {
  if ($id.cache.get($1)) {
    return $gettok($v1,2,37)
  }
}

; enforcing aggressive caching. all data is cached, and the item is only updated
; if the cache time is less than the last file modification time (meaning cache
; will automatically update on a request-by-request basis)
; data is stored in the form NAME: <uid> DATA: <last mtime>%<auth>%<steamid>%<statname>
alias id.cache.table {
  var %table.name = id.cache

  if (!$hget(%table.name)) {
    hmake %table.name 100
  }

  return %table.name
}

alias id.cache.add {
  var %table = $id.cache.table()

  hadd %table $1 $+($ctime,$chr(37),$2)
}

alias id.cache.get {
  ; searches the cache for the specified data. will try cache first,
  ; and will then attempt to recache if out-of-date/no result
  if ($1 isnum) {
    if ($id.cache.getbyuid($1)) {
      return $v1
    }
    else {
      return $id.cache.getbyuid($1, $true)
    }
  }
  elseif (STEAM_* iswm $1) {
    ; $1 = SteamID
    if ($id.cache.getbysid($1)) { 
      return $v1
    }
    else {
      return $id.cache.getbysid($1, $true)
    }
  }
  elseif ($id.cache.getbyauthname($1) || $id.cache.getbystatname($1)) {
    return $v1
  }
  elseif ($id.cache.getbyauthname($1, $true) || $id.cache.getbystatname($1, $true)) {
    return $v1
  }
}

alias id.cache.profile {
  ;hfree $id.cache.table()

  hdel $id.cache.table 18
  var %ticks = $ticks

  ;var %x = 50
  ;while (%x > 0) {
  ;  id.cache.get %x

  ;  dec %x
  ;}
  id.cache.get 18

  debug_msg timing id.cache.profile %ticks
}

alias id.cache.getid {
  var %table = $id.cache.table()

  var %search
  if ($1 = sid) {
    %search = $+(*,$chr(37),$2,$chr(37),*)
  }
  elseif ($1 = authname) {
    %search = $+(*,$chr(37),$2,$chr(37),*)
  }
  elseif ($1 = statname) {
    %search = $+(*,$chr(37),$2)
  }
  else {
    debug_msg cache INVALID SEARCH TYPE FOR ID.CACHE.GETID
    return
  }

  debug_msg cache Getting ID for $2 using pattern ( $+ $1 $+ ) %search

  return $hfind(%table,%search,1,w).data
}

alias id.cache.refreshitem {
  var %search = $1, %fdata

  debug_msg cache Attempting to refresh cache item using %search

  if (%search = uid) {
    %fdata = $read(%dir $+ ids.txt,$2)
  }
  else {
    %fdata = $read(%dir $+ ids.txt,w,%search)
  }

  if (%fdata) {
    debug_msg cache Data found for search %search $+ : %fdata
    ; recache
    id.cache.add $readn %fdata

    debug_msg cache Returning data %fdata
    return %fdata
  }
}

alias id.cache.getbyuid {
  ; $1 = id, $2 = whether to look at file or not in the case of no result (true/false)
  var %table = $id.cache.table()
  var %mtime = $file(%dir $+ ids.txt).mtime

  ; <last mtime>%<auth>%<steamid>%<statname>
  var %udata = $hget(%table,$1)
  debug_msg cache Search: %search Table: %table Mtime: %mtime Cache data: %udata

  if ((%udata && $gettok(%udata,1,37) < %mtime) || (!%udata && $2)) {
    ; we need to recache this if possible
    debug_msg cache Out of date or non-existant item $1

    return $id.cache.refreshitem(uid, $1)
  }
  elseif (%udata) {
    return $gettok(%udata,2-4,37)
  }
}

alias id.cache.getbysid {
  ; $1 = id, $2 = whether to look at file or not in the case of no result (true/false)
  var %search = $+(*,$chr(37),$1,$chr(37),*), %table = $id.cache.table()
  var %mtime = $file(%dir $+ ids.txt).mtime

  var %udata = $hget(%table,$id.cache.getid(sid,$1))

  debug_msg cache Search: %search Table: %table Mtime: %mtime Cache data: %udata

  if ((%udata && $gettok(%udata,1,37) < %mtime) || (!%udata && $2)) {
    ; we need to recache this if possible
    debug_msg cache Out of date or non-existant item $1

    return $id.cache.refreshitem(%search)
  }
  elseif (%udata) {
    return $gettok(%udata,2-4,37)
  }
}

alias id.cache.getbyauthname {
  ; $1 = id, $2 = whether to look at file or not in the case of no result (true/false)
  var %search = $+($1,$chr(37),*), %table = $id.cache.table()
  var %mtime = $file(%dir $+ ids.txt).mtime

  ; need a special search for authname because we've added another token in the cache
  ; but not the file
  var %udata = $hget(%table,$id.cache.getid(authname,$1))

  debug_msg cache Search: %search Table: %table Mtime: %mtime Cache data: %udata

  if ((%udata && $gettok(%udata,1,37) < %mtime) || (!%udata && $2)) {
    ; we need to recache this if possible
    debug_msg cache Out of date or non-existant item $1

    return $id.cache.refreshitem(%search)
  }
  elseif (%udata) {
    return $gettok(%udata,2-4,37)
  }
}

alias id.cache.getbystatname {
  ; $1 = id, $2 = whether to look at file or not in the case of no result (true/false)
  var %search = $+(*,$chr(37),$1), %table = $id.cache.table()
  var %mtime = $file(%dir $+ ids.txt).mtime

  var %udata = $hget(%table,$id.cache.getid(statname,$1))

  debug_msg cache Search: %search Table: %table Mtime: %mtime Cache data: %udata

  if ((%udata && $gettok(%udata,1,37) < %mtime) || (!%udata && $2)) {
    ; we need to recache this if possible
    debug_msg cache Out of date or non-existant item $1

    return $id.cache.refreshitem(%search)
  }
  elseif (%udata) {
    return $gettok(%udata,2-4,37)
  }
}

alias authed {
  ; $1 = authname
  if ($id.cache.getbyauthname($1)) { 
    return $gettok($v1,3,37)
  }
  elseif ($read(%dir $+ ids.temp.txt,w,$+($1,$chr(37),*))) { 
    return $true
  }
  else {
    return $null
  }
}
alias adduser {
  ;auth, steamid, statname

  ; prevent duplicate signups across bots
  if ($authname($2)) {
    return
  }

  if (($1) && ($2) && ($3)) {
    write %dir $+ ids.txt $+($1,$chr(37),$2,$chr(37),$3)
    write %dir $+ statsall.txt $+($2,$str($chr(37) $+ 0, $calc($numtok(%allstats,37) - 2)),$chr(37),0.0.0.0)
  }
  else {
    echo -s error adding user: parameters missing (given: $1-)
  }
}

alias multinetcmd {
  ;multinetmsg <connection id> <command>
  ; if <connection id> is -1, we send to all networks
  ; else, send to a specific connection id if it is valid

  ;scid -a sends the command to all connections
  ;scid -at1 sends the command to all connections that are 'connected'

  ;else, we can just ignore the all conn command and do it per connection like so:

  if (($1 !isnum) || (!$2)) { return }

  if ($1 == -1) {
    var %numconn = $scon(0)
    var %x = %numconn

    while (%x > 0) {
      var %cid = $scon(%x)

      var %cmd = $2-

      if ($scid(%cid).network == %pes.networkname) {
        %cmd = $strip(%cmd)
      }

      ;echo -s Performing command on %cid / cmd: %cmd

      scid %cid %cmd

      dec %x
    }
  }
  elseif ($$1 >= 0) {
    if ($scid($1)) {

      var %cmd = $2-

      if ($scid($1).network == %pes.networkname) {
        %cmd = $strip(%cmd)
      }

      ;echo -s Performing command on $1 / cmd: %cmd

      scid $1 %cmd
    }
  }
}

alias mnmsg {
  ; mnmsg <connection id>? <target> <message>
  ; a multi-network message. if $1 is a number, attempt to send to that CID.
  ; if no number is given, a single network (current network) is implied

  ;echo -s mnmsg $1-

  if ($1 isnum) {
    multinetcmd $1 .msg $$2 $$3-
  }
  else {
    multinetcmd $cid .msg $$1 $$2-
  }
}

alias mnmsg_ex {
  ; mnmsg <connection id to exclude> <target> <message>
  ; A multi-network message that messages all networks excluding the given connection id
  ; echo -s mnmsg_ex $1-
  if ($1 !isnum) { return }

  var %numconn = $scon(0)
  var %x = %numconn

  while (%x > 0) {
    var %cid = $scon(%x)

    if (%cid != $1) {
      var %msg = $$3-

      multinetcmd %cid msg $$2 %msg
    }

    dec %x
  }
}

alias test {
  var %x = 1
  var %y = 50

  while (%x <= %y) {
    mnmsg #test $codes(throttling testststs)

    inc %x
  }
}
