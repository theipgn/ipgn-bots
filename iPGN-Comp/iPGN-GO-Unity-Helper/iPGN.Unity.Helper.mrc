on *:START:{
  window -e @unitypm
}

alias addUnityUser {
  ; addUnityUser $nick

  var %sid = $hget(steamid.cache. $+ $cid,$1)

  if (!%sid) {
    return
  }

  ; prevent duplicate steamids (including ids awaiting approval)
  if ($read(%dir $+ ids.temp.txt,w,$+(*,$chr(37),%sid,$chr(37),*))) {
    hdel steamid.cache. $+ $cid $1
    return
  }

  ; we need to register this user, as there's no corresponding auth
  var %name = $regsubex($remove($1,[A]),[^A-Za-z0-9],$null)

  ; check if this name is already a statname or auth name. if it is, just append a 1
  ; until it's no longer in use
  while ($steamid(%name)) {
    %name = %name $+ 1
  }

  ; name is definitely not taken, now make sure it's not all numbers
  if (%name isnum) {
    %name = %name $+ a
  }

  ; OK, name is good to register as both an auth and statname!
  ; directly add the user, don't worry about accepting/rejecting

  echo -s UNITY USER ADD - AUTH: %name STEAMID: %sid STATNAME: %name

  ; auth, steamid, statname
  adduser %name %sid %name

  ; remove them from the steamid cache because they are now registered
  hdel steamid.cache. $+ $cid $1

  auth.wait.remove $1

  ; now we can properly auth this user
  auth $1
}

on ^*:OPEN:?:{
  echo @unitypm $time([(ddd)HH:nn:ss]) < $+ $nick $+ >: $1-
  ; intercept commands only
  if (!* !iswm $1-) { return }
  if ($1 = !stat) {
    if ($2) {
      .stat_ex $auth($2) $nick
    }
    else {
      .stat_ex $auth($nick) $nick
    }

    ; prevent query window from opening
    halt
  }
}

on *:TEXT:!*:?:{
  if ($istok(%pug.headadmins,$auth($nick),59)) {
    if ($1 = !setpesnetwork) {
      set %pes.networkname $2
      msg $nick $codes(Bot ID %bot.id $+ : PES Network name has been set to12 %pes.networkname $+ )
    }
  }

  if ($1 = !stat) {
    if ($2) {
      .stat_ex $auth($2) $nick
    }
    else {
      .stat_ex $auth($nick) $nick
    }

    window -c $nick
  }
}
