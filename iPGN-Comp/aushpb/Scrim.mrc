on *:START:{
  write -c player.ini
  write -c scrim.ini
  write -c trial.ini
}

on *:TEXT:*:*:{
  ;Scrims
  if ($chan = #ipgn-admins) { return }
  if ($regex($1-,/(5|5ive|five)\s*\S*\s*\S*\s*(in|and|no|need|\x2B)*\s*(serv|wait|waiting)\s*(1.6|onepointsix|csp|promod|source|cs:s|sauce|css)*/i)) {
    write scrim.txt $strip($1-)
    if ($timer(scrim $+ $nick)) { return }
    else {
      %scrim.nick = $nick
      %scrim.msg = $strip($1-)
      .timerscrim $+ $nick 1 90 echo Removed timer
    }

    ;CS:S
    if ($regex(%scrim.msg,/(source|cs:s|sauce|css)/i)) {
      msg #scrim $codes(Scrim 12(CS:S12):  $+ %scrim.nick $+  12-  $+ %scrim.msg)
      writeini scrim.ini CS:S %scrim.nick %scrim.msg
      .timerscrimCS:S $+ %scrim.nick 1 600 remini scrim.ini CS:S %scrim.nick
    } 

    ;CSPM
    elseif ($regex(%scrim.msg,/(csp|promod|cspro)/i)) {
      msg #scrim $codes(Scrim 12(CSPM12):  $+ %scrim.nick $+  12-  $+ %scrim.msg)
      writeini scrim.ini CSPM %scrim.nick %scrim.msg
      .timerscrimCSPM $+ %scrim.nick 1 600 remini scrim.ini CSPM %scrim.nick
    } 
    ;1.6
    else {
      msg #scrim $codes(Scrim 12(1.612):  $+ %scrim.nick $+  12-  $+ %scrim.msg)
      writeini scrim.ini 1.6 %scrim.nick %scrim.msg
      .timerscrim1.6 $+ %scrim.nick 1 600 remini scrim.ini 1.6 %scrim.nick

    } 
  }

  ;Players
  if ($regex($1-,/(looking|need|seeking|keen)(.*)(slut|player|ringer)/i)) {
    write player.txt $strip($1-)
    if ($timer(player $+ $nick)) { return }
    else {
      %player.nick = $nick
      %player.msg = $1-
      .timerplayer $+ $nick 1 90 echo Removed timer
    }

    ;CS:S
    if ($regex(%player.msg,/(source|cs:s|sauce|css)/i)) {
      msg #scrim $codes(Player 12(CS:S12):  $+ %player.nick $+  12-  $+ %player.msg)
      writeini player.ini CS:S %player.nick %player.msg
      .timerplayerCS:S $+ %player.nick 1 600 remini player.ini CS:S %player.nick
    } 

    ;CSPM
    elseif ($regex(%player.msg,/(csp|promod|cspro)/i)) {
      msg #scrim $codes(Player 12(CSPM12):  $+ %player.nick $+  12-  $+ %player.msg)
      writeini player.ini CSPM %player.nick %player.msg
      .timerplayerCSPM $+ %player.nick 1 600 remini player.ini CSPM %player.nick
    } 

    else {
      msg #scrim $codes(Player 12(1.612):  $+ %player.nick $+  12-  $+ %player.msg)
      writeini player.ini 1.6 %player.nick %player.msg
      .timerplayer1.6 $+ %player.nick 1 600 remini player.ini 1.6 %player.nick

    } 
  }

  ;Trial/recruit
  if ($regex($1-,/(looking|need|seeking|keen)(.*)(trial)/i)) {
    write trial.txt $strip($1-)
    if ($timer(trial $+ $nick)) { return }
    else {
      %trial.nick = $nick
      %trial.msg = $1-
      .timertrial $+ $nick 1 90 echo Removed timer
    }

    ;CS:S
    if ($regex(%trial.msg,/(source|cs:s|sauce|css)/i)) {
      msg #scrim $codes(Trial 12(CS:s12):  $+ %trial.nick $+  12-  $+ %trial.msg)
      writeini trial.ini CS:S %trial.nick %trial.msg
      .timertrialCS:S $+ %trial.nick 1 600 remini trial.ini CS:S %trial.nick
    } 

    ;CSPM
    elseif ($regex(%trial.msg,/(csp|promod|cspro)/i)) {
      msg #scrim $codes(Trial 12(CSPM12):  $+ %trial.nick $+  12-  $+ %trial.msg)
      writeini trial.ini CSPM %trial.nick %trial.msg
      .timertrialCSPM $+ %trial.nick 1 600 remini trial.ini CSPM %trial.nick
    } 

    else {
      msg #scrim $codes(Trial 12(1.612):  $+ %trial.nick $+  12-  $+ %trial.msg)
      writeini trial.ini 1.6 %trial.nick %trial.msg
      .timertrial1.6 $+ %trial.nick 1 600 remini trial.ini 1.6 %trial.nick
    } 
  }

  ;Commands
  if ($chan = #scrim) {
    if ($1 = !scrims) || ($1 = !wars) {
      if ($timer(scrimreq $+ $nick)) { return }
      else { 
        .timerscrimreq $+ $nick 1 30 echo Removed timer 
        scrimmsg $nick $2
      }
    }
    if ($1 = !players) || ($1 = !sluts) {
      if ($timer(playerreq $+ $nick)) { return }
      else { 
        .timerplayerreq $+ $nick 1 30 echo Removed timer 
        playermsg $nick $2
      }
    }
    if ($1 = !trials) {
      if ($timer(trialreq $+ $nick)) { return }
      else { 
        .timertrialreq $+ $nick 1 30 echo Removed timer 
        trialmsg $nick $2
      }
    }
    if ($1 = !dice) {
      var %a = 1
      while (%a <= 5) {
        notice $nick $codes(dicevip news: $readini(dicevip.ini,$chr(35) $+ %a,title) 12( $+ $readini(dicevip.ini,$chr(35) $+ %a,link) 12- $readini(dicevip.ini,$chr(35) $+ %a,pubDate) $+ 12))
        inc %a
      }
    }
  }
}

alias scrimmsg {
  if ($2) {
    if ($2 = cs) { tokenize 32 $1 1.6 }
    elseif ($2 = css) { tokenize 32 $1 CS:S }
    else { tokenize 32 $1 $upper($2) }
    ;One game only
    var %x = 1
    var %y = $ini(scrim.ini,$2,0)
    while (%x <= %y) {
      var %nick = $ini(scrim.ini,$2,%x)
      if (*~* iswm %nick) {
        var %nick = $replace(%nick,~,*)
        var %nick = $gettok($ial(%nick,1),1,$asc(!))
      }

      notice $1 $codes(Scrim 12( $+ $2 $+ 12):  $+ %nick $+  12-  $+ $readini(scrim.ini,$2,$ini(scrim.ini,$2,%x)) $&
        12( $+ $duration($calc(600 - $timer(scrim $+ $2 $+ %nick).secs)) ago12 $+ ))
      inc %x
    }

  } 
  else {
    ;All games
    var %a = 1
    var %b = $ini(scrim.ini,0)
    while (%a <= %b) {
      tokenize 32 $1 $ini(scrim.ini,%a)
      var %x = 1
      var %y = $ini(scrim.ini,$2,0)
      while (%x <= %y) {
        var %nick = $ini(scrim.ini,$2,%x)
        if (*~* iswm %nick) {
          var %nick = $replace(%nick,~,*)
          var %nick = $gettok($ial(%nick,1),1,$asc(!))
        }

        notice $1 $codes(Scrim 12( $+ $2 $+ 12):  $+ %nick $+  12-  $+ $readini(scrim.ini,$2,$ini(scrim.ini,$2,%x)) $&
          12( $+ $duration($calc(600 - $timer(scrim $+ $2 $+ %nick).secs)) ago12 $+ ))
        inc %x
      }
      inc %a
    }
  }
}

alias playermsg {
  if ($2) {
    if ($2 = cs) { tokenize 32 $1 1.6 }
    elseif ($2 = css) { tokenize 32 $1 CS:S }
    else { tokenize 32 $1 $upper($2) }
    ;One game only
    var %x = 1
    var %y = $ini(player.ini,$2,0)
    while (%x <= %y) {
      var %nick = $ini(player.ini,$2,%x)
      if (*~* iswm %nick) {
        var %nick = $replace(%nick,~,*)
        var %nick = $gettok($ial(%nick,1),1,$asc(!))
      }

      notice $1 $codes(Player 12( $+ $2 $+ 12):  $+ %nick $+  12-  $+ $readini(player.ini,$2,$ini(player.ini,$2,%x)) $&
        12( $+ $duration($calc(600 - $timer(player $+ $2 $+ %nick).secs)) ago12 $+ ))
      inc %x
    }

  } 
  else {
    ;All games
    var %a = 1
    var %b = $ini(player.ini,0)
    while (%a <= %b) {
      tokenize 32 $1 $ini(player.ini,%a)
      var %x = 1
      var %y = $ini(Player.ini,$2,0)
      while (%x <= %y) {
        var %nick = $ini(player.ini,$2,%x)
        if (*~* iswm %nick) {
          var %nick = $replace(%nick,~,*)
          var %nick = $gettok($ial(%nick,1),1,$asc(!))
        }

        notice $1 $codes(player 12( $+ $2 $+ 12):  $+ %nick $+  12-  $+ $readini(player.ini,$2,$ini(player.ini,$2,%x)) $&
          12( $+ $duration($calc(600 - $timer(player $+ $2 $+ %nick).secs)) ago12 $+ ))
        inc %x
      }
      inc %a
    }
  }
}

alias trialmsg {
  if ($2) {
    if ($2 = cs) { tokenize 32 $1 1.6 }
    elseif ($2 = css) { tokenize 32 $1 CS:S }
    else { tokenize 32 $1 $upper($2) }
    ;One game only
    var %x = 1
    var %y = $ini(trial.ini,$2,0)
    while (%x <= %y) {
      var %nick = $ini(trial.ini,$2,%x)
      if (*~* iswm %nick) {
        var %nick = $replace(%nick,~,*)
        var %nick = $gettok($ial(%nick,1),1,$asc(!))
      }

      notice $1 $codes(Trial 12( $+ $2 $+ 12):  $+ %nick $+  12-  $+ $readini(trial.ini,$2,$ini(trial.ini,$2,%x)) $&
        12( $+ $duration($calc(600 - $timer(trial $+ $2 $+ %nick).secs)) ago12 $+ ))
      inc %x
    }

  } 
  else {
    ;All games
    var %a = 1
    var %b = $ini(trial.ini,0)
    while (%a <= %b) {
      tokenize 32 $1 $ini(trial.ini,%a)
      var %x = 1
      var %y = $ini(trial.ini,$2,0)
      while (%x <= %y) {
        var %nick = $ini(trial.ini,$2,%x)
        if (*~* iswm %nick) {
          var %nick = $replace(%nick,~,*)
          var %nick = $gettok($ial(%nick,1),1,$asc(!))
        }

        notice $1 $codes(Trial 12( $+ $2 $+ 12):  $+ %nick $+  12-  $+ $readini(trial.ini,$2,$ini(trial.ini,$2,%x)) $&
          12( $+ $duration($calc(600 - $timer(trial $+ $2 $+ %nick).secs)) ago12 $+ ))
        inc %x
      }
      inc %a
    }
  }
}
