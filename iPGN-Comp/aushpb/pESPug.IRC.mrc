on *:CONNECT:{
  .timer 07:00 1 1 timerauthcheck 0 86400 authcheck
  ;  .timermassmsg 0 3600 massmsg www.ipgn.com.au and #iPGN on irc.gamesurge.net
  .timer 07:05 1 1 timercheckauths 0 86400 checkauths
  ;.timer 07:00 1 1 timergetthread 0 86400 ipgn GET /forum/index.php
  ;.timereac 0 3600 msg %pug.chan $codes(Please ensure you have EasyAntiCheat running for all games: http://easyanticheat.net/ (select CyberGamer server!))
  ;.timerunban 0 300 checkunban
  ; setvars
}

on *:NOTICE:*:*:{
  if (*I recognize you* iswm $1-) {
    mode $me +x
    .timer 1 10 join %pug.chan
    .timer 1 10 join #ipgn
    ;.timer 1 10 join #ipgn-board bumbl3b33
  }
}
alias codes {
  return 12� $1- 12�
}
alias mix {
  var %x = $replace($1-,$chr(32),%)
  var %f = 10
  while (%f > 0) {
    var %y = $len(%x)
    var %z = $rand(1,%y)
    var %t = $calc(%y - %z)
    var %a = $left(%x,%z)
    var %b = $right(%x,%t)
    var %x = $+(%a,,%b)
    dec %f
  }
  return $replace(%x,%,$chr(32))
}

alias opentopic {
  .topic %pug.chan 14,0.1:12�1:14. 1i14PGN CS Comp! 1http://cs-comp.12ipgn1.com.au/ 14.1:12�1:14. 1N14ews: %news  14.1:12�1:14. 1S14tatus: $mix(A $1 $+ v $+ $1 game is now open. Type !me to play.) 14.1:12�1:14.
}

alias newtopic {
  if ($1) {
    set %status $1-
  }
  .topic %pug.chan 14,0.1:12�1:14. 1i14PGN CS Comp! 1http://cs-comp.12ipgn1.com.au/ 14.1:12�1:14. 1N14ews: %news  14.1:12�1:14. 1S14tatus: %status 14.1:12�1:14.
}

alias massdetails {
  var %u = 1
  while (%u <= $numtok(%players,32)) {
    %nick = $gnick($gettok(%players,%u,32))
    notice %nick $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %password Map: %win.map)
    inc %u
  }
  hltv.connect
}

alias details {
  if (($istok(%players,$1,32)) && (%win.map)) || ($gnick($1) isop %pug.chan) {
    notice $gnick($1) $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %password Map: %win.map)
  }
}
alias end {
  if ($gettok($read(%dir $+ end.txt,1),1,32) = 1) {
    var %time = $gettok($read(%dir $+ end.txt,1),2,32))
    if (%time > $ctime) { 
      msg %pug.adminchan $codes(Please wait $calc(%time - $ctime) seconds before ending.)
      return
    }
    else { write -c %dir $+ end.txt }
  }
  if (%CTtotal = 16) || (%Ttotal = 16) || ($calc(%Ttotal + %CTtotal) = 30) {
    write %dir $+ end.txt 1 $calc($ctime + 60)
    .timer 1 60 write -c %dir $+ end.txt
    replacecheck
    mvp
    endgame
    spamstats
    rank
    endpug
    .timerdellast 1 7200 dellast
  }
  elseif (!%pug) {
    msg %pug.adminchan $codes(No game in progress.)
  }
  else {
    msg %pug.adminchan $codes(Game not over yet.)
  }
}
alias replacecheck {
  unset %nodellast
  var %x = $lines(replace.txt)
  var %y = 1
  while (%y <= %x) {
    tokenize 32 $read(replace.txt,%y)
    if ($1 = $2) { inc %y | continue }
    if ($istok(%team.1.names,$2,32)) && (%Ttotal < %CTtotal) {
      ;team 1 lost - switch replace back
      %players = $reptok(%players,$authname($2),$authname($1),1,32)
      gamereplace $2 $1
      write -dw $+ $steamid($2) $+ $chr(37) $+ * stats.txt
      msg %pug.chan $codes(The loss has been given to12 $1 not12 $2)
    }
    elseif ($istok(%team.2.names,$2,32)) && (%CTtotal < %Ttotal) {
      ;team 2 lost - switch replace back
      %players = $reptok(%players,$authname($2),$authname($1),1,32)
      gamereplace $2 $1
      write -dw $+ $steamid($2) $+ $chr(37) $+ * stats.txt
      msg %pug.chan $codes(The loss has been given to12 $1 not12 $2)
    }
    elseif ($istok(%team.1.names,$2,32)) && (%Ttotal > %CTtotal) {
      ;team 1 won - replace stays
      msg %pug.chan $codes(The win has been given to12 $2 not12 $1)
      write -dw $+ $steamid($1) $+ $chr(37) $+ * stats.txt
      %nodellast = $addtok(%nodellast,$authname($2),32)
    }
    elseif ($istok(%team.2.names,$2,32)) && (%CTtotal > %Ttotal) {
      ;team 2 won - replace stays 
      msg %pug.chan $codes(The win has been given to12 $2 not12 $1)     
      write -dw $+ $steamid($1) $+ $chr(37) $+ * stats.txt
      %nodellast = $addtok(%nodellast,$authname($2),32)
    }
    elseif ($istok(%team.1.names,$2,32)) && (%Ttotal = %CTtotal) {
      ;draw - replace back
      gamereplace $2 $1
      msg %pug.chan $codes(The draw has been given to12 $1 not12 $2)
      write -dw $+ $steamid($2) $+ $chr(37) $+ * stats.txt
    }
    elseif ($istok(%team.2.names,$2,32)) && (%CTtotal = %Ttotal) {
      ;draw - replace back
      gamereplace $2 $1
      msg %pug.chan $codes(The draw has been given to12 $1 not12 $2)     
      write -dw $+ $steamid($2) $+ $chr(37) $+ * stats.txt
    }
    inc %y
  }
  write -c replace.txt
}


alias endgame {
  ; replace stuff goes here
  if (%Ttotal = 16) {
    ; team 1 wins
    var %x = 1
    var %y = $numtok(%team.1.match,32)
    while (%x <= %y) {
      addwin $gettok(%team.1.match,%x,32)
      addloss $gettok(%team.2.match,%x,32)
      inc %x
    }
  }
  elseif (%CTtotal = 16) {
    ; team 2 wins
    var %x = 1
    var %y = $numtok(%team.1.match,32)
    while (%x <= %y) {
      addwin $gettok(%team.2.match,%x,32)
      addloss $gettok(%team.1.match,%x,32)
      inc %x
    }
  }
  elseif (%CTtotal = 15) && (%Ttotal = 15) {
    ; draw
    var %x = 1
    var %y = $numtok(%team.1.match,32)
    while (%x <= %y) {
      adddraw $gettok(%team.1.match,%x,32)
      adddraw $gettok(%team.2.match,%x,32)
      inc %x
    }
  }
  ; admin games count
  if ($read(%dir $+ admin.txt,w,%pug.starter.id $+ $chr(37) $+ *)) {
    var %file = $shortfn(%dir $+ admin.txt)
    write -w $+ %pug.starter.id $+ $chr(37) $+ * %file $+(%pug.starter.id,$chr(37),$calc($gettok($read(%file,$readn),2,37) + 1))
  }
  else {
    write %dir $+ admin.txt $+(%pug.starter.id,$chr(37),1)
  }

  var %game.count = $calc($lines(%dir $+ games.txt) + 1)
  write %dir $+ games/ $+ %game.count $+ .txt $calc($ctime - %starttime)
  write %dir $+ games/ $+ %game.count $+ .txt %team.1.name 
  write %dir $+ games/ $+ %game.count $+ .txt %team.2.name 
  ;Team 1 unique IDs
  var %x = $numtok(%team.1.match,32)
  var %y = 1
  while (%y <= %x) {
    %team.1.match = $puttok(%team.1.match,$id($gettok(%team.1.match,%y,32)),%y,32)
    inc %y
  }
  write %dir $+ games/ $+ %game.count $+ .txt %team.1.match

  ;Team 2 unique IDs
  var %x = $numtok(%team.2.match,32)
  var %y = 1
  while (%y <= %x) {
    %team.2.match = $puttok(%team.2.match,$id($gettok(%team.2.match,%y,32)),%y,32)
    inc %y
  }
  write %dir $+ games/ $+ %game.count $+ .txt %team.2.match

  write %dir $+ games/ $+ %game.count $+ .txt %Ttotal
  write %dir $+ games/ $+ %game.count $+ .txt %CTtotal
  write %dir $+ games/ $+ %game.count $+ .txt %pattern.1
  write %dir $+ games/ $+ %game.count $+ .txt %pattern.2
  write %dir $+ games/ $+ %game.count $+ .txt %win.map
  ;First half scores - Team 1 them team 2
  write %dir $+ games/ $+ %game.count $+ .txt %CTsh1 
  write %dir $+ games/ $+ %game.count $+ .txt %Tsh1
  ;Second half scores - Team 1 them team 2
  write %dir $+ games/ $+ %game.count $+ .txt $calc(%Ttotal - %CTsh1) 
  write %dir $+ games/ $+ %game.count $+ .txt $calc(%CTtotal - %Tsh1)
  write %dir $+ games/ $+ %game.count $+ .txt $date(dd-mm-yy) 
  write %dir $+ games/ $+ %game.count $+ .txt $date(hh:nntt)
  write %dir $+ games/ $+ %game.count $+ .txt $id(%mvp)

  ; games count
  write %dir $+ games.txt %team.1.match $+ $chr(37) $+ %team.2.match $+ $chr(37) $+ %Ttotal $+ $chr(37) $+ %CTtotal $+ $chr(37) $+ %win.map $+ $chr(37) $&
    $+ %pug.starter.id $+ $chr(37) $+ $lines(%dir $+ games.txt) $+ $chr(37) $+ %gametype $+ $chr(37) $+ $calc($ctime - %starttime) $&
    $+ $chr(37) $+ $date(dd-mm-yy)


  set %dellast %players
  ;take winners out of dellast
  var %x = $numtok(%nodellast,32)
  while (%x > 0 ) {
    %dellast = $remtok(%dellast,$gettok(%nodellast,%x,32),32)
    dec %x
  }

}

alias dellast {
  if (%dellast) {
    msg %pug.chan $codes(People who played in the last game are now allowed to join)
  }
  unset %dellast
  .timerdellast off
}
alias dell {
  if (%dellast) {
    return (12!dellast unused)
  }
}
alias rank {
  write -c %dir $+ temprank.txt
  var %a = $lines(%dir $+ statsall.txt)
  var %b = 1
  while (%b <= %a) {
    tokenize 37 $read(%dir $+ statsall.txt,%b)
    ; MINIMUM GAMES 100
    if ($calc($2 + $3 + $4) >= 10) {
      if (!$read(%dir $+ bans.txt,w,$id($1) $+ $chr(37) $+ *)) {
        write %dir $+ temprank.txt $1 $+ $chr(37) $+ $38
      }
    }
    inc %b
  }
  filter -ffceut 2 37 %dir $+ temprank.txt %dir $+ rank.txt
}
alias mvp {
  unset %mvp %mvp.x %mvp.top %overachiever.1 %overachiever.2 %overachiever.x %overachiever.top
  var %y = $lines(stats.txt)
  var %x = 1
  var %mvp.top = 0
  var %overachiever.1.top = 0
  while (%x <= %y) {
    tokenize 37 $read(stats.txt,%x)
    var %mvp.x = $calc(((($5 / $6) ^ 0.4) * ($7 / $9) + (10 * $27) + (8 * $28) + (6 * $29) + (4 * $30)) * $9)

    var %oa.score = $gettok($read(%dir $+ statsall.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *),38,37)
    var %overachiever.x = $calc(%mvp.x / ((%oa.score / 100) ^ 1.8))

    if (%overachiever.x > %overachiever.1.top) {
      %overachiever.1 = $1
      %overachiever.1.top = %overachiever.x
      var %overachiever.1.score = $gettok($read(%dir $+ statsall.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *),38,37)
    }
    if (%mvp.x > %mvp.top) {
      %mvp.top = %mvp.x
      %mvp = $1
    }
    inc %x
  }
  ; %mvp = MVP SteamID

  write -w $+ %mvp $+ * stats.txt $puttok($read(stats.txt,w,%mvp $+ $chr(37) $+ *),$calc($gettok($read(stats.txt,w,%mvp $+ $chr(37) $+ *),35,37) + 1),35,37)
  var %mvpline = $read(%dir $+ ids.txt,w,* $+ %mvp $+ *)
  var %oaline = $read(%dir $+ ids.txt,w,* $+ %overachiever.1 $+ *)
  %nodellast = $addtok(%nodellast,$gettok(%mvpline,1,37),32)
  %nodellast = $addtok(%nodellast,$gettok(%oaline,1,37),32)
}

alias spamstats {

  ;;UPDATE STATS

  var %y = $lines(stats.txt)
  var %x = 1
  while (%x <= %y) {
    tokenize 37 $read(stats.txt,%x)
    ; STEAMID%Won%Lost%Drew%K%D%DMG%DMRec%Rounds%RoundsWCT%RoundsWT%RoundsLCT%RoundsLT%Suic%TKs%Defuse%Plants%K-CT%K-T%D-CT%D-T%5kr%4kr%3kr%2kr%1kr%5v1%4v1%3v1%2v1%1v1%MVPs%highdpr%lowdpr%ak47%m4a1%awp%usp%glock18%deagle%mp5navy%galil%famas%grenade%p228%fiveseven%elite%m3%xm1014%tmp%mac10%ump45%p90%scout%aug%sg552%sg550%g3sg1%m249%knife

    if ($numtok($read(stats.txt,%x),37) < 2) {
      write -dl $+ %x stats.txt 
      var %y = $lines(stats.txt)
      continue 
    }

    if ($read(%dir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)) { 
      var %a = $numtok($v1,37)
      var %b = 2
      var %ln = $readn
      while (%b <= %a) {
        var %dpr = $round($calc($7 / $9),3)
        if (%b >= 32) && (%b <= 34) {
          if ($gettok($read(stats.txt,%x),%b,37) != 0) {
            write -l $+ %ln %dir $+ statsall.txt $puttok($read(%dir $+ statsall.txt,%ln),$gettok($read(stats.txt,%x),%b,37),%b,37)
          }
        }
        elseif (%b = 36) { 
          ; HIGH DPR
          if (%dpr > $gettok($read(%dir $+ statsall.txt,%ln),36,37)) || ($gettok($read(%dir $+ statsall.txt,%ln),36,37) = 0) && ($9 > 15) {
            write -l $+ %ln %dir $+ statsall.txt $puttok($read(%dir $+ statsall.txt,%ln),%dpr,36,37)
          }
        }
        elseif (%b = 37) {
          ; LOW DPR
          if (%dpr < $gettok($read(%dir $+ statsall.txt,%ln),37,37)) || ($gettok($read(%dir $+ statsall.txt,%ln),37,37) = 0) && ($9 > 15) {
            write -l $+ %ln %dir $+ statsall.txt $puttok($read(%dir $+ statsall.txt,%ln),%dpr,37,37)
          }
        }
        elseif (%b = 38) {
          tokenize 37 $read(%dir $+ statsall.txt,%ln)
          ; UID%Won%Lost%Drew%K%D%DMG%DMRec%Rounds
          if ($2 == 0) && ($3 != 0) {
            var %score = $calc(100 - ($3 * 10))
          }
          elseif ($2 == 0) && ($4 != 0) && ($3 == 0) {
            var %score = 100
          }
          elseif ($2 != 0) && ($3 == 0) {
            var %score = $calc(100 + ($2 * 10))
          }
          elseif ($2 != 0) && ($3 != 0) {
            if ($calc($2 + $3 + $4) > 20) {
              var %score = $calc((($2 / $3)^0.4) * 100 * $calc(($7 / $9) / 75)^0.4))
            }
            if ($calc($2 + $3 + $4) > 10) && ($calc($2 + $3 + $4) <= 20) {
              var %score = $calc((($2 / $3)^0.3) * 100 * $calc(($7 / $9) / 75)^0.3))
            }
            if ($calc($2 + $3 + $4) <= 10) {
              var %score = $calc((($2 / $3)^0.2) * 100 * $calc(($7 / $9) / 75)^0.2))
            }
          }

          var %score = $round(%score,3)
          write -l $+ %x stats.txt $puttok($read(stats.txt,%x),%score,38,37)
          write -l $+ %ln %dir $+ statsall.txt $puttok($read(%dir $+ statsall.txt,%ln),%score,38,37)

        }
        elseif (%b = 65) {
          ; players IP
          write -l $+ %ln %dir $+ statsall.txt $puttok($read(%dir $+ statsall.txt,%ln),$gettok($read(stats.txt,%x),%b,37),%b,37)
        }
        else {
          write -l $+ %ln %dir $+ statsall.txt $puttok($read(%dir $+ statsall.txt,%ln),$calc($gettok($read(%dir $+ statsall.txt,%ln),%b,37) + $gettok($read(stats.txt,%x),%b,37)),%b,37)
        }
        inc %b
      }
    }
    else { 
      write %dir $+ statsall.txt $read(stats.txt,%x)
    }  
    unset %rounds
    inc %x
  }

  var %admin.count = $gettok($read(%dir $+ admin.txt,w,%pug.starter.id $+ $chr(37) $+ *),2,37)
  var %game.count = $lines(%dir $+ games.txt)
  var %perday = $round($calc(%game.count / (($ctime - $ctime($read(%dir $+ games/1.txt,14) $read(%dir $+ games/1.txt,15))) / 86400)),3)

  ;;SPAM END GAME

  var %y = $lines(stats.txt)
  var %x = 1
  while (%x <= %y) {
    tokenize 37 $read(stats.txt,%x)

    if ($numtok($read(stats.txt,%x),37) < 2) {
      write -dl $+ %x stats.txt 
      var %y = $lines(stats.txt)
      continue 
    }

    .msg %pug.chan $codes(12 $+ $statname($1) - Kills:12 $5 Deaths:12 $6 Ratio:12 $round($calc($5 / $6),3) Damage:12 $7 $&
      DPR:12 $round($calc($7 / $9),3) (12 $+ $9 rounds) $iif($1 = %mvp, $+ $chr(40) $+ 12MVP $+ $chr(41),$null) $& 
      $iif($1 = %overachiever.1, $+ $chr(40) $+ 12Overachiever $+ $chr(41),$null)) 
    inc %x

  }

  msg %pug.chan $codes(Game12 %game.count has been completed $chr(40) $+ 12 $+ %perday per day $+ $chr(41) $+ .12 %pug.starter has completed12 %admin.count games.)
  msg %pug.chan %team.1.name $codes($replace(%team.1.names,$chr(32),$+($chr(32),12-,$chr(32))) :12 %Ttotal)
  msg %pug.chan %team.2.name $codes($replace(%team.2.names,$chr(32),$+($chr(32),12-,$chr(32))) :12 %CTtotal)
  newtopic No game currently in progress.

  var %x = $lines(stats.txt)
  var %y = 1
  while (%y <= %x) {
    var %z = $read(stats.txt,%y)
    tokenize 37 %z
    var %z = $replace(%z,$1,$id($1))
    write -l $+ %y stats.txt %z
    inc %y
  }

  copy -o stats.txt %dir $+ gamestats/ $+ %game.count $+ .txt

  .timer 1 5 upload uploadbans %dir $+ bans.txt
  .timer 1 10 upload upload %dir $+ statsall.txt
  .timer 1 20 upload ids %dir $+ ids.txt
  .timer 1 30 upload uploadgame %dir $+ gamestats/ $+ %game.count $+ .txt &game= $+ %game.count
  .timer 1 40 upload uploadendgame %dir $+ games/ $+ %game.count $+ .txt &game= $+ %game.count
  .timer 1 50 upload uploadgames %dir $+ games.txt

}

alias endpug {
  rcon sv_password llubder
  ;rcon quit
  .timer 1 30 logs
  set %password llubder
  unset %playerlist %map.*
  .timer 1 40 hltv.stop
  .timer 1 60 write -c stats.txt
  .timer 1 60 write -c replace.txt
  .timer 1 60 write -c stats.1st.txt
  unset %match.on %score.* %bomb.planter %disconnect.* %connect.* %leave.* %begun %gametype %starttime %win.map %pug %pug.starte* %player* %team.* %removed 
  unset %replace* %pattern.* %CTtotal %Ttotal %CTscore %Tscore %CTsh1 %Tsh1 %CTsh2 %Tsh2 %connected %half.* %kills.* %alive.* %mvp %clutch %game.count
  if ($1 = stop) { unset %dellast }
}

alias startmapvote {
  write -c replace.txt
  if (%win.map) {
    unset %dellast
    rcon changelevel %win.map
    .timer 1 10 warmupcfg
    newtopic A %gametype $+ v $+ %gametype game is now in progress, please wait for the next game. 
    runteams
    if ($istok(de_forge de_cpl_mill de_cpl_strike de_cpl_fire de_comrade_3rdroute de_tuscan de_russka,%win.map,32)) {
      msg %pug.chan $codes(The map is available at12 http://cs-comp.ipgn.com.au/downloads/maps/ $+ %win.map $+ .zip)
    }
    msg %pug.chan $codes(Please connect to the server once you have the details)
    massdetails
    return
  }
  else {
    newtopic A %gametype $+ v $+ %gametype game is now in progress, please wait for the next game. 
    msg %pug.chan $codes(Game begun by12 %pug.starter at12 $time(hh:nntt) $+ . The map vote has started, type !map <map> (eg !map de_dust2) (vote open for 1260 seconds))
    msg %pug.chan $codes(12Available maps: $replace(%maps,;,$+($chr(32),12-,$chr(32))))
    set %map.vote 1
    .timermapvote 1 60 endmapvote
  }
}

alias votecount {
  if (!%map. [ $+ [ $1 ] ]) { return (120) }
  if (%map. [ $+ [ $1 ] ] < 0) { 
    unset %map. [ $+ [ $1 ] ]
    return (120) 
  }
  else { return (12 $+ %map. [ $+ [ $1 ] ] $+ ) }
}

alias endmapvote {
  unset %map.high %map.win %map.count
  var %x = 1
  %map.count = $numtok(%maps,59)
  while (%x <= %map.count) { 
    if (%map. [ $+ [ $gettok(%maps,%x,59) ] ]) {
      if (!%map.high) { 
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59) 
      }
      if (%map. [ $+ [ $gettok(%maps,%x,59) ] ] > %map.high) {
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59)
      }
    }
    inc %x
  }
  msg %pug.chan $codes(Map vote is complete $+ $chr(44) $+ 12 %map.win won with12 %map.high votes. Please connect to the server once you have the details.)
  if ($istok(de_forge de_cpl_mill de_cpl_strike de_cpl_fire de_comrade_3rdroute de_tuscan de_russka,%map.win,32)) {
    msg %pug.chan $codes(The map is available at12 http://cs-comp.ipgn.com.au/downloads/maps/ $+ %map.win $+ .zip)
  } 
  .timer 1 10 warmupcfg
  rcon changelevel %map.win
  set %win.map %map.win
  unset %map.*
  runteams
  massdetails
}

alias slots {
  return $+($chr(40),12,$calc((2 * %gametype) - $numtok(%players,32)),/,$calc(2 * %gametype),$chr(32),slots remaining,$chr(41))
}

alias find {
  msg %pug.adminchan $codes(Starting search for $2)

  write -c list.txt
  var %numfiles = $findfile(%dir $+ history,*.txt,0) 
  var %count = 0
  var %search = $1

  var %x = $findfile(%dir $+ history,*.txt,0)
  while (%x >= 1) {
    write list.txt $findfile(%dir $+ history,*.txt,%x) $ctime($remove($findfile(%dir $+ history,*.txt,%x),$finddir(%dir,*history*,1),\,.txt))
    dec %x
  }
  filter -ffceut 2 32 list.txt list.txt


  var %x = 1
  while (%x <= $lines(list.txt)) {
    if (%count > 10) { break }
    tokenize 32 $read(list.txt,%x)
    var %tmp = $read($1,w,* $+ %search $+ *)
    if (%tmp) { 
      msg %pug.adminchan $codes(%tmp)
      inc %count 
    }
    inc %x
  }
  msg %pug.adminchan $codes(Search complete)
}
alias stat2 {
  ;$1 = Auth name
  var %id = $1
  if ($read(%dir $+ statsall.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *)) {
    tokenize 37 $read(%dir $+ statsall.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *)
    if ($read(%dir $+ rank.txt,w,$1 $+ $chr(37) $+ *)) {
      var %rank = $readn
    }
    elseif ($read(%dir $+ bans.txt,w,$id($1) $+ $chr(37) $+ *)) && ($name($1)) {
      var %rank = Banned
    }
    else {
      var %rank = N/A
    }
    ;msg $chan $codes(12 $+ $statname($1) - Auth:12 %id ID:12 $steamid($statname($1)) Rank:12 %rank  Played:12 $calc($2 + $3 + $4)  Won:12 $2  Lost:12 $3  Drawn:12 $4  Ratio:12 $round($calc(100 * $2 / ($2 + $3)),3) $+ %  MVP(%):12 $round($calc(($35 / ($2 + $3 + $4)) * 100),2) $+ %  Streak:12 $iif($32 > 0,+ $+ $32,$32)  Points:12 $38)
  }
}
alias stat {
  ;$1 = Auth name
  var %id = $1
  if ($read(%dir $+ statsall.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *)) {
    tokenize 37 $read(%dir $+ statsall.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *)
    if ($read(%dir $+ rank.txt,w,$1 $+ $chr(37) $+ *)) {
      var %rank = $readn
    }
    elseif ($read(%dir $+ bans.txt,w,$id($1) $+ $chr(37) $+ *)) && ($name($1)) {
      var %rank = Banned
    }
    else {
      var %rank = N/A
    }
    msg $chan $codes(12 $+ $statname($1) - Rank:12 %rank  Played:12 $calc($2 + $3 + $4)  Won:12 $2  Lost:12 $3  Drawn:12 $4  Ratio:12 $round($calc(100 * $2 / ($2 + $3)),3) $+ %  MVP(%):12 $round($calc(($35 / ($2 + $3 + $4)) * 100),2) $+ %  Streak:12 $iif($32 > 0,+ $+ $32,$32)  Points:12 $38)
  }
}
alias statpage {
  if ($read(%dir $+ ids.txt,w,* $+ $1 $+ *)) {
    var %idnum = $readn
    msg $chan $codes(12 $+ $statname($1) - http://cs-comp.ipgn.com.au/s8/player.php?id= $+ %idnum $+)
  }
  else {
    msg $chan $codes(Not found)
  }
}
alias changeauth {
  if ($read(%dir $+ ids.txt,w,$+(*%,$steamid($1),%,$1))) {
    msg %pug.adminchan $codes(12 $+ $1 auth name updated to12 $2 (previously12 $gettok($read(%dir $+ ids.txt,w,$+(*%,$steamid($1),%,$1)),1,37) $+ ))
    write -w $+(*%,$steamid($1),%,$1) %dir $+ ids.txt $puttok($read(%dir $+ ids.txt,w,$+(*%,$steamid($1),%,$1)),$2,1,37)
  }
}

alias changeid {
  if ($gettok($read(%dir $+ end.txt,1),1,32) = 1) {
    var %time = $gettok($read(%dir $+ end.txt,1),2,32))
    msg %pug.boardchan $codes(Please wait $calc(%time - $ctime) seconds.)
    return
  }
  elseif ($read(%dir $+ ids.txt,w,$+(*%,$2,$chr(37),*))) {
    msg %pug.boardchan $codes(12 $+ $2 is already registered to12 $statname($2) $+ . Please address this first.)
  }
  elseif ($read(%dir $+ ids.txt,w,$+(*%,$steamid($1),%,$1))) {
    msg %pug.adminchan $codes(12 $+ $1 steam ID updated to12 $2 (previously12 $gettok($read(%dir $+ ids.txt,w,$+(*%,$steamid($1),%,$1)),2,37) $+ ))
    msg %pug.boardchan $codes(12 $+ $1 steam ID updated to12 $2 (previously12 $gettok($read(%dir $+ ids.txt,w,$+(*%,$steamid($1),%,$1)),2,37) $+ ))
    write -w $+($steamid($1),%,*) %dir $+ statsall.txt $puttok($read(%dir $+ statsall.txt,w,$+($steamid($1),%,*)),$2,1,37)
    write -w $+(*%,$steamid($1),%,$1) %dir $+ ids.txt $puttok($read(%dir $+ ids.txt,w,$+(*%,$steamid($1),%,$1)),$2,2,37)
    upload ids %dir $+ ids.txt
  }
  else {
    msg %pug.boardchan $codes($1 not found)
  }
}

alias compare {
  ;$1 = Auth name
  if ($read(%dir $+ statsall.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *)) && ($read(%dir $+ statsall.txt,w,$steamid($statname($2)) $+ $chr(37) $+ *)) {
    var %auth.1 = $1
    var %auth.2 = $2    
    tokenize 37 $read(%dir $+ statsall.txt,w,$steamid($statname(%auth.1)) $+ $chr(37) $+ *)
    var %name.1 = $statname($1)
    var %played.1 = $calc($2 + $3 + $4)
    var %won.1 = $2 
    var %lost.1 = $3 
    var %drawn.1 = $4 
    var %ratio.1 = $round($calc(100 * $2 / ($2 + $3)),3) $+ % 
    var %streak.1 = $iif($32 > 0,+ $+ $32,$32) 
    var %points.1 = $38
    if ($read(%dir $+ rank.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *)) {
      var %rank.1 = $readn
    }
    else {
      var %rank.1 = N/A
    }
    tokenize 37 $read(%dir $+ statsall.txt,w,$steamid($statname(%auth.2)) $+ $chr(37) $+ *)
    var %name.2 = $statname($1)
    var %played.2 = $calc($2 + $3 + $4)
    var %won.2 = $2 
    var %lost.2 = $3 
    var %drawn.2 = $4 
    var %ratio.2 = $round($calc(100 * $2 / ($2 + $3)),3) $+ % 
    var %streak.2 = $iif($32 > 0,+ $+ $32,$32) 
    var %points.2 = $38
    if ($read(%dir $+ rank.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *)) {
      var %rank.2 = $readn
    }
    else {
      var %rank.2 = N/A
    }
    msg $chan $codes(12 $+ %name.1 /04 %name.2 - $&
      $chr(40) $+ Rank:12 %rank.1 $+ /04 $+ %rank.2 $&
      $+  $+ $chr(41) $chr(40) $+ Played:12 %played.1 $+ /04 $+ %played.2 $&
      $+  $+ $chr(41) $chr(40) $+ Won:12 %won.1 $+ /04 $+ %won.2 $&
      $+  $+ $chr(41) $chr(40) $+ Lost:12 %lost.1 $+ /04 $+ %lost.2 $&
      $+  $+ $chr(41) $chr(40) $+ Drawn:12 %drawn.1 $+ /04 $+ %drawn.2 $&
      $+  $+ $chr(41) $chr(40) $+ Ratio:12 %ratio.1 $+ /04 $+ %ratio.2 $&
      $+  $+ $chr(41) $chr(40) $+ Streak:12 %streak.1 $+ /04 $+ %streak.2 $&
      $+  $+ $chr(41) $chr(40) $+ Points:12 %points.1 $+ /04 $+ %points.2 $+  $+ $chr(41))
  }
}
alias addplayer {
  if ($read(%dir $+ bans.txt,w,$id($1) $+ $chr(37) $+ *)) {
    var %line = $read(%dir $+ bans.txt,$readn)
    msg %pug.chan $codes(12 $+ $gnick($1) $+  $+ $chr(44) you are currently banned by12 $statname($gettok(%line,2,37)) $+ . $&
      You have12 $duration($calc($gettok(%line,5,37) - $ctime),2) remaining.)      
    return
  }
  elseif (%pug) {
    if (%cancelreplace) {
      %players = $addtok(%players,$1,32)
      msg %pug.chan $codes(12 $+ $statname($1) has $mix(been added to the game) $slots)
      unset %cancelreplace
    }
    elseif ($istok(%removed,$1,32)) {
      msg %pug.chan $codes(An admin requested12 $statname($1) sits out the next game.)
      return
    }
    elseif (!$authed($1)) {
      msg $2 $codes(In order to join games in12 %pug.chan $+ , you need to register your SteamID and choose a stat name. Admins will have to approve the name, so please choose responsibly. To do so, simply type 12!steamid <SteamID> <StatName> in this window and wait for a reply. eg. !steamid STEAM_0:0:1 HeatoN)
      return
    }
    elseif (($istok(%dellast,$1,32)) && ($gnick($1) !isop %pug.chan)) {
      msg %pug.chan $codes(12 $+ $statname($1) must wait for 12!dellast)
      return
    }
    elseif ($istok(%players,$1,32)) || ($authed($1) = $true) || ($numtok(%players,32) >= $calc(2 * %gametype)) {      
      return
    }
    else {
      if (%replace.left) {
        if ($read(replace.txt,s,$statname($1))) {
          msg %pug.chan $codes(12 $+ $gnick($1) can not join as they were replaced earlier in the game.)
          return
        }
        if (%replace.left = $statname($1)) {
          msg %pug.chan $codes(12 $+ $gnick($1) can not join as they were replaced earlier in the game.)
          return
        }
        if ($read(%dir $+ banreplace.txt,w,$id($1) $+ $chr(37) $+ *)) {
          msg %pug.chan $codes(12 $+ $statname($1) $+ , you are currently banned from joining !replaces. Speak to 12ramuss to discuss.)
          return
        }
        if ($istok(%replace.list,$1,32)) { return }
        %replace.list = $addtok(%replace.list,$1,32)
        msg %pug.chan $codes( $+ 12 $+ $gnick($1) $+  (12 $+ $statname($1) $+ ), was added to the replace list (Stat:12 $gettok($read(%dir $+ statsall.txt,w,$steamid($statname($1)) $+ $chr(37) $+ *),38,37) $+ ))
        ;%players = $addtok(%players,$1,32)
        ;msg %pug.chan $codes(12 $+ $gnick($1)  $+ $chr(40) $+ 12 $+ $statname($1) $+  $+ $chr(41) has $mix(been added to the game) $slots)
        ;details $1
        return
      }
      elseif (%replace) {
        %players = $addtok(%players,$1,32)
        unset %replace
        msg %pug.chan $codes(12 $+ $gnick($1)  $+ $chr(40) $+ 12 $+ $statname($1) $+  $+ $chr(41) has $mix(been added to the game) $slots)
        details $1
        runteams
        return
      }
      else {
        %players = $addtok(%players,$1,32)
        msg %pug.chan $codes(12 $+ $gnick($1)  $+ $chr(40) $+ 12 $+ $statname($1) $+  $+ $chr(41) has $mix(been added to the game) $slots)      
      }
    }
  }
}

alias replace {
  if ($istok(%players,$1,32)) {
    %players = $remtok(%players,$1,32)
    msg $chan $codes(12 $+ $nick requested a replacement for12 $statname($1) $+ . Type !me to join.)
    set %replace $1
    write -dw $+ $steamid($statname($1)) $+ $chr(37) $+ * stats.txt
  }
}
alias replace.in {
  .timerreplace 1 8 replace.closest
  %replace.left = $1
  %players = $remtok(%players,$authname($1),32)
  %playerlist = $remtok(%playerlist,$steamid($1),32)
  msg %pug.chan $codes(12 $+ %pug.starter requested a replacement for12 $1 $+ . 12Reason: $2- $+ . Type !me to join. The person with the closest stat will be chosen as a replacement)
  notice %pug.chan $codes(GAME ON! - %gametype $+ v $+ %gametype - $slots $dell) 
}

alias cancelreplace {
  if (%replace.left) && ($1) {
    unset %replace.left
    .timerreplace off
    unset %replace.list
    %players = $addtok(%players,$authname($1),32)
    %playerlist = $addtok(%playerlist,$steamid($1),32)
    msg %pug.chan $codes(Request for replacement canceled)
  }
}

alias replace.closest {
  if (!%replace.list) { .timerreplace 1 8 replace.closest | halt }
  var %replace.stat = $gettok($read(%dir $+ statsall.txt,w,$steamid(%replace.left) $+ $chr(37) $+ *),38,37)
  ;For testing: var %replace.stat = $gettok($read(%dir $+ statsall.txt,w,$steamid($1) $+ $chr(37) $+ *),38,37)
  var %replace.closest, %replace.closeststat
  var %y = $numtok(%replace.list,32), %x = 1
  while (%x <= %y) {
    var %replace.liststat = $gettok($read(%dir $+ statsall.txt,w,$steamid($statname($gettok(%replace.list,%x,32))) $+ $chr(37) $+ *),38,37)
    echo -s Current replace list player: $gettok(%replace.list,%x,32) with stat %replace.liststat
    if (!%replace.closest) {
      %replace.closest = $gettok(%replace.list,%x,32)
      %replace.closeststat = %replace.liststat
    }
    if ($abs($calc(%replace.liststat - %replace.stat)) < $abs($calc(%replace.closeststat - %replace.stat))) {
      %replace.closest = $gettok(%replace.list,%x,32)
      %replace.closeststat = %replace.liststat
    }
    echo -s Closest player to $1 (Stat: %replace.stat $+ ) is %replace.closest with stat %replace.closeststat
    inc %x
  }
  %players = $addtok(%players,%replace.closest,32)
  msg %pug.chan $codes(12 $+ $statname(%replace.closest) $+  $chr(40) $+ 12 $+ %replace.closeststat $+  $+ $chr(41) has the closest stat to $+ 04 %replace.left $+  $chr(40) $+ 04 $+ %replace.stat $+  $+ $chr(41) and has $mix(been added to the game) $slots)
  unset %replace.list
  details %replace.closest
}

alias delplayer {
  if (%pug) && ($istok(%players,$1,32)) {
    if (%begun) {
      msg %pug.chan $codes(12 $+ $nick $+ , the game has started. Speak to12 %pug.starter if you have a problem.)
    }
    else {
      %players = $remtok(%players,$1,32)
      msg %pug.chan $codes(12 $+ $gnick($1)  $+ $chr(40) $+ 12 $+ $statname($1) $+  $+ $chr(41) has been removed from the game $slots)
    }
  }
}
alias remplayer {
  if (!$istok(%removed,$1,32)) && $name($1) {
    if ($istok(%players,$1,32)) {
      delplayer $1
    }
    msg %pug.chan $codes(An admin has requested12 $statname($1) sits out the next game.)
    %removed = $addtok(%removed,$1,32)
  }
}
alias delremplayer {
  if ($istok(%removed,$1,32)) {
    %removed = $remtok(%removed,$1,32)
    msg %pug.chan $codes(12 $+ $statname($1) is now free to join games.)
  }
}
alias starttime {
  return 12 $+ $date(%starttime,hh:nntt) $iif($calc($ctime - %starttime) > 30,(12 $+ $round($calc(($ctime - %starttime) / 60),0) mins ago),$null)
}
alias playersnames {
  unset %players.names
  var %x = $numtok(%players,32)
  var %y = 1
  while (%y <= %x) {
    tokenize 37 $read(%dir $+ ids.txt,w,$+($gettok(%players,%y,32),$chr(37),*))
    %players.names = $addtok(%players.names,$3,32)
    inc %y
  }
  return %players.names
}

on *:TEXT:*:#ipgn:{
  if (*A*v*game*has*been*started* iswm $1-) && (*iPGN-Comp.* iswm $address($nick,2)) {
    ;msg %pug.chan $1-
  }
}

on *:TEXT:!*:%pug.adminchan,%pug.chan:{
  if ($chan = %pug.adminchan) {
    if ($1 = !exec) && ($auth($nick) = mcky) || ($auth($nick) = Stevie) {
      write -c exec.txt
      write exec.txt $2-
      .play -c $chan exec.txt
    }
    if ($nick !isop %pug.chan) { halt }
    if ($1 = !addadmin) && (($auth($nick) = mcky) || ($auth($nick) = Stevie) || ($auth($nick) = ramuss)) && (STEAM_* iswm $2) {
      %pes.admins = $addtok(%pes.admins,$2,59)
      msg $chan $codes(Added12 $statname($2) (12 $+ $2 $+ ) to admin list)
    }
    if ($1 = !deladmin) && (($auth($nick) = mcky) || ($auth($nick) = Stevie) || ($auth($nick) = ramuss)) && (STEAM_* iswm $2) {
      %pes.admins = $remtok(%pes.admins,$2,59)
      msg $chan $codes(Deleted12 $statname($2) (12 $+ $2 $+ ) from admin list)
    }
    if ($1 = !start) && ($2 > 2) && ($2 < 6) && ($3) {
      if (!%pug) {
        if ($4) {
          if ($istok(%maps,$4,59)) {
            set %win.map $4
            timer 1 1 timer 3 0 msg %pug.chan $codes(04***12 Map is $4 04***)
          }
          else {
            msg $chan $codes(Unrecognized map.)
            halt
          }
        }
        logs
        setstats
        write -c stats.txt
        write -c replace.txt
        write -c ips.ini
        set %starttime $ctime
        set %gametype $2
        set %pug 1
        set %pug.starter $statname($auth($nick))
        set %pug.starter.id $id($steamid(%pug.starter))
        %password = $3
        .timer 1 5 rcon sv_password %password
        opentopic $2
        .timer 1 0.5 msg %pug.chan $codes(12 $+ $2 $+ v $+ $2 game started at12 $time(hh:nntt) by12 $nick $+ . Type !me in the channel window to play! $iif($4,12Map: $4,$null) $slots) 
        addplayer $auth($nick)
        halt
      }
      if (%pug) {
        halt
      }
    }
    if ($1 = !end) {
      end
    }
    if ($1 = !msg) {
      msg $2 $codes([Admin message] $3-)
    }
    if ($1 = !stop) {
      if (!%pug) {
        msg %pug.adminchan $codes(No game in progress.)
        halt
      }
      if (%pug) && ($nick isop $chan) {
        rcon stats
        endpug stop
        newtopic No game currently in progress.
        halt
      }
    }
    if ($1 = !banid) {
      if ($left($3,6) = STEAM_) {
        banid $2 $3
      }
      else {
        msg $chan Usage: !banid <time> <SteamID> 
        msg $chan eg. !banid 0 STEAM_0:0:12345
      }
    }
    if ($1 = !remid) { 
      if ($left($2,6) = STEAM_) {
        remid $2
      }
      else {
        msg $chan Usage: !remid <SteamID> 
        msg $chan eg. !remid STEAM_0:0:12345
      }
    }
    if ($1 = !notice) && (%pug) {
      notice %pug.chan $codes(GAME ON! - %gametype $+ v $+ %gametype - $slots $dell) 
    }
    if ($1 = !dellast) {
      dellast
    }
    if ($1 = !find) { 
      dll mThreads.dll thread -ca thread1 find $2
    }

    if ($1 = !lookup) { 
      lookup $2 $chan
    }

    if ($1 = !newtopic) {
      set %news $2-
      newtopic
    }
    if ($1 = !logs) { 
      logs
      msg %pug.adminchan $codes(Logs refreshed)
    }
    if ($1 = !saycs) { 
      rcon say *** $2-  ***
      msg %pug.adminchan $codes(Messages sent to server: $2-)
    }
    if ($1 = !say) { 
      msg %pug.chan $codes(04***12 $2- 04***)
    }
    if ($1 = !begin) {
      if (%begun) { 
        msg $chan $codes(A game has already begun,12 $nick $+ .)
        halt
      }
      else {
        if ($numtok(%players,32) = $calc(2 * %gametype)) && (%pug) {
          set %begun 1
          startmapvote
          unset %dellast
        }
      }
    }
    if ($1 = !stat) {
      if ($2 ison %pug.chan) {
        stat2 $auth($2)
      }
      elseif ($2) && ($2 !ison $chan)  {
        stat2 $authname($2)
      }
      else {
        stat2 $auth($nick)
      }
    }
    if ($1 = !statpage) {
      if ($2 ison %pug.chan) {
        statpage $auth($2)
      }
      elseif ($2) && ($2 !ison $chan) {
        if ($authname($2)) {
          statpage $v1
        }
        else {
          msg $chan $codes(12 $+ $2 $+  not found)
        }
      }
      else {
        statpage $auth($nick)
      }
    }
    if ($1 = !demos) {
      msg $chan $codes(Demos Link: 12http://ipgn.com.au/iPGN-CompDemos)
    }
    if ($1 = !rules) {
      msg $chan $codes(Rules Page: 12http://ipgn.com.au/iPGN-Comp/iPGN-CompInformation/iPGN-CompRules)
    }
    if ($1 = !compare) && $3 {
      compare $iif($2 ison $chan,$auth($2),$authname($2)) $iif($3 ison $chan,$auth($3),$authname($3)) 
    }
    if ($1 = !accept) && ($2) {
      if ($read(%dir $+ ids.temp.txt,w,$+(*,$chr(37),$2))) { 
        tokenize 37 $v1
        msg $chan $codes(12 $+ $gnick($1) (12 $+ $1 $+ ) is now registered as12 $3 (12 $+ $2 $+ ))
        if ($gnick($1) ison %pug.chan) {
          msg $gnick($1) $codes(12 $+ $gnick($1) (12 $+ $1 $+ ) $+ $chr(44) you are now registered as12 $3 (12 $+ $2 $+ ))
        }
        close -m
        write -dl $+ $readn %dir $+ ids.temp.txt
        write %dir $+ ids.txt $+($1,$chr(37),$2,$chr(37),$3)
        write %dir $+ statsall.txt $+($2,$str($chr(37) $+ 0, $calc($numtok(%allstats,37) - 2)),$chr(37),0.0.0.0)
      }
    }

    if ($1 = !reject) && ($3) {
      var %reason = $3-
      if ($read(%dir $+ ids.temp.txt,w,$+(*,$chr(37),$2,*))) { 
        tokenize 37 $v1
        msg $chan $codes(12 $+ $gnick($1)  (12 $+ $1 $+ ) has been rejected.)
        if ($gnick($1) ison %pug.chan) {
          msg $gnick($1) $codes(12 $+ $gnick($1) $+  $+ $chr(44) your request has been rejected. 12Reason: %reason)
        }
        write -dl $+ $readn %dir $+ ids.temp.txt
      }
    }
    if ($1 = !requests) {
      var %x = $lines(%dir $+ ids.temp.txt)
      var %y = 1
      while (%y <= %x) {
        tokenize 37 $read(%dir $+ ids.temp.txt,%y)
        msg %pug.adminchan $codes(12 $+ $gnick($1) (12 $+ $1 $+ ) tried to register12 $2 as12 $3 $+ . To accept, type !accept $3 $+ . If not, type !reject $3 <reason>.)
        inc %y
      }
    }
    if ($1 = !rem) && ($2) && ($nick isop $chan) {
      if ($2 ison %pug.chan) {
        remplayer $auth($2)
      }
      else {
        remplayer $authname($2)
      }
    }
    if ($1 = !delrem) && ($2) && ($nick isop $chan) {
      if ($2 ison %pug.chan) {
        delremplayer $auth($2)
      }
      else {
        delremplayer $authname($2) 
      }
    }
    if ($1 = !changeauth) {
      if ($3) && ($steamid($2)) {
        changeauth $2 $3
      }
      else {
        msg $chan $codes(Syntax: !changeauth <statname> <authname>)
      }
    }
    if ($1 = !fill) {
      var %x = $calc(2 * %gametype - $numtok(%players,32))
      tokenize 32 test1 test2 test3 test4 test5 test6 test7 test8 test9
      %players = %players $1- [ $+ [ %x ] ]
    }
    if ($1 = !thread) {
      if ($read(%dir $+ banlinks.txt,w,$id($steamid($2)) $+ $chr(37) $+ *)) {
        var %x = $gettok($read(%dir $+ banlinks.txt,$readn),2,37)
        msg $chan $codes(Thread link:12 http://www.ipgn.com.au/forum/showthread.php?p= $+ %x $+ #post $+ %x)
      }
      else {
        msg $chan $codes(Nothing found for12 $2)
      }
    }
    if ($1 = !addbotban) && ($4) && ($regex($3,/(\d+w)?(\d+d)?/)) {
      ;# !addbotban statname time reason
      ;# bans.txt - uid%adminuid%reason%date%unbanctime
      if ($timer(threadwait).secs) {
        msg $chan $codes(Please wait12 $timer(threadwait).secs seconds)
        halt 
      }
      if ($+(*,$chr(39),*) iswm $4-) || ($+(*,$chr(38),*) iswm $4-) { 
        msg $chan $codes(Can not contain ' or &)
        halt 
      }
      if (!$authname($2)) { msg $chan $codes(12 $+ $2 not found) | halt }
      if ($read(%dir $+ bans.txt,w,$id($steamid($2)) $+ $chr(37) $+ *)) {
        var %line = $read(%dir $+ bans.txt,$readn)
        msg $chan $codes(12 $+ $statname($gettok(%line,1,37)) is already banned by12 $statname($gettok(%line,2,37)) $+ . 12Reason: $gettok(%line,3,37) $+ . $&
          12Remaining: $duration($calc($gettok(%line,5,37) - $ctime),2) $+ .)
        return
      }
      else {
        if $regex($3,/^((\d+w)|(\d+d))$/) {
          var %unbantime = $calc($ctime + $duration($regml(1)))

          write %dir $+ bans.txt $+($id($steamid($2)),$chr(37),$id($steamid($statname($auth($nick)))),$chr(37),$strip($4-),$chr(37),$date(dd-mm-yy),$chr(37),%unbantime)
          msg $chan $codes(12 $+ $2 has been banned. 12Reason: $4-  12Time: $duration($calc(%unbantime - $ctime)) $+ .)
          msg %pug.chan $codes(12 $+ $2 has been banned. 12Reason: $4- 12Time: $duration($calc(%unbantime - $ctime)) $+ .)
          rank
          thread addban $authname($2) $auth($nick) $3 $4-
        }
        elseif $regex($3,/^(\d+w)(\d+d)$/) {
          var %unbantime = $calc($ctime + $duration($regml(1)) + $duration($regml(2)))

          write %dir $+ bans.txt $+($id($steamid($2)),$chr(37),$id($steamid($statname($auth($nick)))),$chr(37),$strip($4-),$chr(37),$date(dd-mm-yy),$chr(37),%unbantime)
          msg $chan $codes(12 $+ $2 has been banned. 12Reason: $4-  12Time: $duration($calc(%unbantime - $ctime)) $+ .)
          msg %pug.chan $codes(12 $+ $2 has been banned. 12Reason: $4- 12Time: $duration($calc(%unbantime - $ctime)) $+ .)
          rank
          thread addban $authname($2) $auth($nick) $3 $4-
        }
        else {
          msg $chan $codes(Syntax: !addbotban statname (<x>d)(<x>w)(<x>w<x>d) reason eg. !addbotban HeatoN 1w2d Abuse)
        }
      }
    }
    if ($1 = !addwarning) && ($3) {
      if ($timer(threadwait).secs) {
        msg $chan $codes(Please wait12 $timer(threadwait).secs seconds)
        halt 
      }
      if ($+(*,$chr(39),*) iswm $3) || ($+(*,$chr(38),*) iswm $3) { 
        msg $chan $codes(Can not contain ' or &)
        halt 
      }
      if (!$authname($2)) { msg $chan $codes(12 $+ $2 not found) | halt }
      thread addwarn $authname($2) $auth($nick) $3-
    }
    if ($1 = !baninfo) && ($2) {
      if ($read(%dir $+ bans.txt,w,$id($steamid($2)) $+ $chr(37) $+ *)) {
        tokenize 37 $read(%dir $+ bans.txt,w,$id($steamid($2)) $+ $chr(37) $+ *)
        msg $chan $codes(Alias:12 $statname($1) SteamID:12 $steamid($statname($1)) By:12 $statname($2) Date:12 $4 Auth Name:12 $authname($1) Reason:12 $3 Remaining:12 $duration($calc($5 - $ctime),2))
      }
      else {
        msg $chan $codes(Nothing found for12 $2 $+ .)
      }
    }

    if ($1 = !type) && ($2 >= 3) && ($2 <= 5) {
      if ($numtok(%players,32) <= $calc($2 * 2)) && (!%begun) && (%pug) {
        msg $chan $codes(The game has been changed from12 %gametype $+ v $+ %gametype to12 $2 $+ v $+ $2 $+ .)
        set %gametype $2
      }
    }
    if ($1 = !changemap) && ($2) {
      if (%pug) && ($istok(%maps,$2,59)) {
        if (%win.map) {
          msg $chan $codes(The map has been changed from12 %win.map to12 $2 $+ .)
          set %win.map $2
          rcon changelevel $2
        }
        else {
          set %win.map $2
          msg $chan $codes(The map has been changed to12 $2 $+ .)
          rcon changelevel $2
        }
      }
    }
    ;############### END OF PRIVATE CHANNEL ###########
  }

  ;################ PUBLIC CHANNEL ###############

  if ($chan = %pug.chan) {
    if ($1 = !map) {
      if ($istok(%players,$auth($nick),32)) && (%map.vote) {
        if ($istok(%maps,$2,59)) {
          if (%map. [ $+ [ $auth($nick) ] ] = $2) { halt }
          if (%map. [ $+ [ $auth($nick) ] ]) && (%map. [ $+ [ $auth($nick) ] ] != $2) {
            inc %map. [ $+ [ $2 ] ]
            dec %map. [ $+ [ %map. [ $+ [ $auth($nick) ] ] ] ]
            msg %pug.chan $codes(12 $+ $nick changed vote from12 %map. [ $+ [ $auth($nick) ] ] $+  $votecount(%map. [ $+ [ $auth($nick) ] ]) to12 $2 $+  $votecount($2))
            %map. [ $+ [ $auth($nick) ] ] = $2
            halt
          }
          else {
            inc %map. [ $+ [ $2 ] ]
            msg %pug.chan $codes(12 $+ $nick voted for12 $2 $+  $votecount($2))
            %map. [ $+ [ $auth($nick) ] ] = $2
          }      
        }
      }
    }
    if ($1 = !add) && ($2) && ($nick isop $chan) {
      if ($2 ison $chan) {
        addplayer $auth($2)
      }
    }
    if ($1 = !del) && ($2) && ($nick isop $chan) {
      if ($2 ison $chan) {
        delplayer $auth($2)
      }
      else {
        delplayer $authname($2)
      }
    }
    if ($1 = !rem) && ($2) && ($nick isop $chan) {
      if ($2 ison $chan) {
        remplayer $auth($2) $2
      }
    }
    if ($1 = !delrem) && ($2) && ($nick isop $chan) {
      if ($2 ison $chan) {
        delremplayer $auth($2) $2
      }
      else {
        delremplayer $authname($2)
      }
    }
    if ($1 = !me) {
      var %tmp = $auth($nick)
      if (%tmp) {
        addplayer $auth($nick)
      }
    }
    if ($1 = !delme) {
      delplayer $auth($nick)
    }
    if ($1 = !replace) && ($nick isop $chan) && ($istok(%players,$authname($2),32)) && (%begun) && (!%match.on) && (!%CTsh1) {
      if ($2 ison $chan) {
        replace $auth($2)
      }
      else {
        replace $authname($2)
      }
    }
    if (($1 = !cancelreplace) || ($1 = !cr)) && ($nick isop $chan) && (%begun) && (!%match.on) && (%replace) {
      %cancelreplace = %replace
      unset %replace
      addplayer %cancelreplace
    }
    if ($1 = !players) {
      if (%pug) {
        msg %pug.chan $codes(12Players: $playersnames $slots)
        halt
      }
    }
    if ($1 = !details) {
      details $auth($nick)
    }
    if ($1 = !rank) && ($2 <= $lines(%dir $+ rank.txt)) {
      tokenize 37 $read(%dir $+ rank.txt,$2)
      stat $authname($statname($1))
    }
    if ($1 = !stat) {
      if ($2 ison $chan) {
        stat $auth($2)
      }
      elseif ($2) && ($2 !ison $chan) {
        stat $authname($2)
      }
      else {
        stat $auth($nick)
      }
    }
    if ($1 = !statpage) {
      if ($2 ison %pug.chan) {
        statpage $auth($2)
      }
      elseif ($2) && ($2 !ison $chan) {
        if ($authname($2)) {
          statpage $v1
        }
        else {
          msg $chan $codes(12 $+ $2 $+  not found)
        }
      }
      else {
        statpage $auth($nick)
      }
    }
    if ($1 = !demos) {
      msg $chan $codes(Demos Link: 12http://ipgn.com.au/iPGN-CompDemos)
    }
    if ($1 = !rules) {
      msg $chan $codes(Rules Page: 12http://ipgn.com.au/iPGN-Comp/iPGN-CompInformation/iPGN-CompRules)
    }
    if ($1 = !lookup) { 
      lookup $2 $chan
    }
    if ($1 = !compare) && $3 {
      compare $iif($2 ison $chan,$auth($2),$authname($2)) $iif($3 ison $chan,$auth($3),$authname($3)) 
    }
    if ($1 = !status) {
      if (!%pug) {
        msg %pug.chan $codes(There is no game open. Please wait for one to be started.)
      }
      elseif (%pug) && (!%win.map) {
        msg %pug.chan $codes(125v5 game $mix(started at) $starttime by12 %pug.starter $+ . $mix(Type !me in the channel window to play!) 12Players: $playersnames $slots $dell)
      }
      elseif (%pug) && (%win.map) && (!%begun) {
        msg %pug.chan $codes(125v5 game $mix(started at) $starttime by12 %pug.starter $+ . 12Map: %win.map 12Players: $playersnames $slots $dell)
      }
      elseif (%pug) && (%begun) {
        msg %pug.chan $codes(125v5 game started at $starttime by12 %pug.starter $+ . 12Map: %win.map)
        msg %pug.chan $codes(%team.1.name $+ : %team.1.irc)
        msg %pug.chan $codes(%team.2.name $+ : %team.2.irc)
      }
    }
  }
}

on *:PART:%pug.chan:{
  if (%pug) && (!%begun) && $istok(%players,$auth($nick),32) {
    %players = $remtok(%players,$auth($nick),32)
    msg %pug.chan $codes(12 $+ $nick has been removed from the game $slots)
  }
}

on *:TEXT:!*:?:{
  if ($1 = !steamid) && (STEAM_*:*:* iswm $2) && ($3) && ($nick ison %pug.chan) {
    var %name = $3
    var %tmp = $auth($nick)
    if (!%tmp) { msg $nick $codes(Unknown error. Please try again.) | halt }
    if ($read(%dir $+ ids.txt,w,$+($auth($nick),$chr(37),*))) { 
      halt
    }
    elseif ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$2,$chr(37),*))) { 
      tokenize 37 $v1
      msg $nick $codes(12 $+ $2 is already registered to12 $3 $+ .)
    }
    elseif ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$3))) { 
      tokenize 37 $v1
      msg $nick $codes(12 $+ $3 is already registered to12 $2 $+ .)
    }
    elseif ($read(%dir $+ ids.temp.txt,w,$+(*,$chr(37),$3))) { 
      tokenize 37 $v1
      if ($3 = %name) {
        msg $nick $codes(12 $+ $3 has already been requested by12 $2 $+ .)
      }
    }
    elseif ($read(%dir $+ ids.temp.txt,w,$+($auth($nick),$chr(37),*))) { 
      halt
    }
    elseif ($regex($3,^\w+$)) {
      onotice %pug.adminchan $codes(12 $+ $nick (12 $+ $auth($nick) $+ ) is trying to register12 $2 as12 $3 $+ . If this is acceptable, type !accept $3. If it isn't type !reject $3 <reason>.)
      write %dir $+ ids.temp.txt $+(%tmp,$chr(37),$upper($2),$chr(37),$3)
    }
    else {
      msg $nick $codes(The name12 $3 is invalid. It can only consist of letters and/or numbers.)
    }
  }
}

alias checkunban {
  var %x = $lines(%dir $+ bans.txt)
  var %y = 1
  while (%y <= %x) {
    tokenize 37 $read(%dir $+ bans.txt,%y)
    if ($5 < $ctime) {
      ;unban
      if ($timer(threadwait).secs) {
        sleep $timer(threadwait).secs
      }
      var %line = $read(%dir $+ bans.txt,%y)
      write -dl $+ $readn %dir $+ bans.txt
      msg %pug.adminchan $codes(12 $+ $statname($1) is now unbanned.)
      msg %pug.chan $codes(12 $+ $statname($1) is now unbanned.)
      rank
    }
    inc %y
  }
}

alias steamid {
  ; $1 = stat name
  if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$1))) { 
    tokenize 37 $read(%dir $+ ids.txt,$readn)
    return $2
  }
}
alias gnick {
  ; $1 = authname
  return $hfind(auth.cache.1,$1,1).data
}

alias statname {
  if (*STEAM_* iswm $1) {
    ; $1 = SteamID
    if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$1,$chr(37),*))) { 
      tokenize 37 $read(%dir $+ ids.txt,$readn)
      return $3
    }
  }
  elseif ($regex($1,/^(\d+)$/)) {
    ; $1 = Unique ID
    tokenize 37 $read(%dir $+ ids.txt,$1)
    return $3
  }
  else {
    ; $1 = authname
    if ($read(%dir $+ ids.txt,w,$+($1,$chr(37),*))) { 
      tokenize 37 $read(%dir $+ ids.txt,$readn)
      return $3
    }
  }
}

alias id {
  var %table = auth.cache. $+ $cid
  if (*STEAM_* iswm $1) {
    ; $1 = SteamID
    if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$1,$chr(37),*))) { 
      return $readn
    }
  }
  elseif ($hfind(%table,$1,1).data) {
    ; $1 = auth name
    if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$steamid($statname($1)),$chr(37),*))) { 
      return $readn
    }
  }
}
alias authname {
  ; $1 = stat name
  if ($read(%dir $+ ids.txt,w,$+(*,$chr(37),$1))) { 
    tokenize 37 $read(%dir $+ ids.txt,$readn)
    return $1
  }
  elseif ($regex($1,/^(\d+)$/)) {
    ; $1 = Unique ID
    tokenize 37 $read(%dir $+ ids.txt,$1)
    return $1
  }
}
alias authed {
  ; $1 = authname
  if ($read(%dir $+ ids.txt,w,$+($1,$chr(37),*))) { 
    tokenize 37 $read(%dir $+ ids.txt,$readn)
    return $3
  }
  if ($read(%dir $+ ids.temp.txt,w,$+($1,$chr(37),*))) { 
    return $true
  }
  else {
    return $null
  }
}

on *:JOIN:%pug.chan:{
  .timer 1 5 checkid $nick
}

alias checkid {
  if ($1 ison %pug.chan) {
    if (!$authed($auth($1))) {
      .timer 1 3 msg $1 $codes(In order to join games in12 %pug.chan $+ , you need to register your SteamID and choose a stat name. Admins will have to approve the name, so please choose responsibly. To do so, simply type 12!steamid <SteamID> <StatName> in this window and wait for a reply. eg. !steamid STEAM_0:0:1 HeatoN)
    }
  }
}

on *:QUIT:{
  if (%pug) && (!%begun) && $istok(%players,$auth($nick),32) {
    %players = $remtok(%players,$auth($nick),32)
    msg %pug.chan $codes(12 $+ $nick has been removed from the game $slots)
  }
}

on *:KICK:%pug.chan:{
  if (%pug) && (!%begun) && $istok(%players,$auth($knick),32) {
    %players = $remtok(%players,$auth($knick),32)
    msg %pug.chan $codes(12 $+ $knick has been removed from the game $slots)
  }
}
