alias limit {
  mode %pug.chan +l $calc($nick(%pug.chan,0) + 5)
}
on *:JOIN:%pug.chan:{
  if ($nick = $me) { halt }
  .timerlimit 1 3 limit 
  if (%channel.flood) { halt }
  inc %channel.count
  .timer 1 10 dec %channel.count
  if (%channel.count > 8) {
    unset %channel.count
    set -u300 %channel.flood 1
    mode $chan +r
    .msg $chan $codes(Join flood detected.)
    .timer 1 300 mode $chan -r
  }
}

on *:PART:%pug.chan:{
  .timerlimit 1 3 limit 
}

on *:QUIT:{
  .timerlimit 1 3 limit 
}
