on *:start:{
  window -e @rcon
  .timer 1 2 logs
  .timer 0 300 logs
}

on *:INPUT:@rcon:{
  if ($left($1,1) != $chr(47)) { 
    rcon $1-
    set %rcon.input $1-
  }
}

menu * {
  -
  pESpug
  .RCON Console:{
    if (!$window(@rcon)) {
      window -e @rcon
      aline @rcon PES Pug Bot
      aline @rcon Usage:
      aline @rcon 1: type /rcon.password RCONPASSWORDHERE
      aline @rcon 2: type /rcon.challenge IPHERE PORTHERE
      aline @rcon 3: If you want to see the logs, type /rcon.logs
      aline @rcon 4: type /rcon COMMANDHERE
    }
    else {
      echo -a The RCON window is already open!
    }
  }
}

alias warcfg {
  set %x $lines(war.cfg)
  set %y 1
  while (%y <= %x) {
    rcon $read(war.cfg,%y)
    inc %y
  }
}
alias warmupcfg {
  set %x $lines(warmup.cfg)
  set %y 1
  while (%y <= %x) {
    rcon $read(warmup.cfg,%y)
    inc %y
  }
  rcon sv_restart 1
}

alias bl {
  if ($read(bans.txt,w,$1 *)) {
    tokenize 32 $v1
    write -l $+ $readn bans.txt $1 $calc($2 + 2)
    rcon banid $calc($2 + 2) $1
    rcon say $1 banned for $calc($2 + 2) days.
    rcon writeid
  } 
  else {
    write bans.txt $1 2
    rcon banid $calc(1440 * 2) $1
    rcon say $1 banned for two days.
    rcon writeid
  }
}
alias logoff {
  sockudp -kn dice 9001 %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " logaddress_del %rcon.myip 9001
  sockudp -kn dice 9001 %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " logaddress_del %rcon.myip 9001
  sockclose dice
  sockclose dice.cmd
}

alias rcon.challenge {
  if ($window(@rcon)) {
    if (!$1) {
      aline @rcon You need to enter an ip address
      aline @rcon The format is: /rcon.challenge 123.456.789.012 27015
      aline @rcon the first set of numbers is the ip, the second set of numbers is the port
      halt
    }
    if (!$2) {
      set %rcon.ip $1
      set %rcon.port 27015
    }
    else {
      set %rcon.ip $1
      set %rcon.port $2
    }
    sockudp -kn dice.cmd %rcon.ip %rcon.port ����challenge rcon
  }
  else {
    echo -a The RCON window is not open! Select it from the popup menu.
  }
}

alias rcon.logs {
  if ($window(@rcon)) {
    if ((!%rcon.ip) || (!%rcon.port) || (!%rcon.challengenumber) || (!%rcon.password)) {
      aline @rcon some vital components are missing, please make sure the ip, port, challenegenumber, and rcon password all exist.
    }
    else {
      sockudp -kn dice.cmd %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " mp_logmessages 1
      sockudp -kn dice.cmd %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " mp_logfile 1
      sockudp -kn dice.cmd %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " mp_logdetail 3
      %rcon.myip = $ip
      sockudp -kn dice 9001 %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " logaddress_add %rcon.myip 9001
    }
  }
  else {
    echo -a The RCON window is not open! Select it from the popup menu.
  }
}

alias rcon {
  if ($window(@rcon)) {
    if ((!%rcon.ip) || (!%rcon.port) || (!%rcon.challengenumber) || (!%rcon.password)) {
      aline @rcon some vital components are missing, please make sure the ip, port, challenegenumber, and rcon password all exist.
    }
    else {
      sockudp -kn dice.cmd %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " $1-
      set %rcon.input $1
    }
  }
  else {
    echo -a The RCON window is not open! Select it from the popup menu.
  }
}

on *:udpread:dice.cmd:{
  if ($sockerr > 0) {
    return
  }
  :nextread
  sockread -f %rcon.cmd
  if ($sockbr == 0) {
    return
  }
  if (%rcon.cmd == $null) {
    goto nextread
  }
  else {
    if ($gettok(%rcon.cmd,1,32) == ����challenge) {
      set %rcon.challengenumber $gettok(%rcon.cmd,3,32)
    }
    if ($left(%rcon.cmd,5) == ����l) {
      %rcon.cmd = $remove(%rcon.cmd,$left(%rcon.cmd,5))
    }
  }  
  if (%rcon.cmd == $null) {
    goto nextread
  }
  if (*"* iswm %rcon.cmd) {
    %rcon.cmd = $remove(%rcon.cmd,")
  }
  if ($pos(%rcon.cmd,$date(yyyy),1) = 9) {
    %rcon.cmd = $remove(%rcon.cmd,$left(%rcon.cmd,24))
  } 
  if (*server cvar* iswm %rcon.cmd) {
    if ($left(%rcon.input,3) = sv_) || ($left(%rcon.input,3) = mp_) {
      %rcon.cmd = $remove(%rcon.cmd,Server cvar)
      %rcon.cmd = 10RCON cvar14: %rcon.cmd
    } 
    else {
      %rcon.cmd = $null
    }
  }
  if (*Log file* iswm %rcon.cmd) || (*Server logging data to file* iswm %rcon.cmd) {
    %rcon.cmd = $null
  }
  if (*Rcon:* iswm %rcon.cmd) {
    %rcon.cmd = $null
  }
  if (*����9Bad challenge* iswm %rcon.cmd) {
    unset %rcon.cmd
    halt
  }
  if (*CPU*In*Out* iswm %rcon.cmd) {
    set -u2 %stats 1
    %rcon.cmd = $null
  }
  if (%stats) {
    if ($regex(%rcon.cmd,/((\d+)(.)(\d+))(\s*)((\d+)(.)(\d+))(\s*)((\d+)(.)(\d+))(\s*)(\d+)(\s*)(\d+)(\s*)((\d+)(.)(\d+))(\s*)(\s*)(\d+)/)) {
      %rcon.cmd = 10CPU:14 $regml(1) 10In:14 $regml(6) 10Out:14 $regml(11) 10Uptime:14 $regml(16) 10Users:14 $regml(18) 10FPS:14 $regml(20) 10Players:14 $regml(26)
      unset %stats    
    }
  }
  if (Bad Rcon: rcon* iswm %rcon.cmd) {
    %rcon.cmd = $null     
  }
  if (*No*challenge*for*your*address* iswm %rcon.cmd) {
    logs
  }
  if (*steam_* iswm %rcon.cmd) {
    if ($regex(%rcon.cmd,/(#)(\s*)(\d+)(\s*)(.*)(\s+)(\d+)(\s+)((STEAM_0:)(\S*))(\s+)(.*)(\s+)((\S+))(\s+)(\d+)(\s+)(\d+)(\s+)((\S+)(:)(\d+))/)) {
      if (%end.pug) {
        rcon kick $+($chr(35),$regml(9))
      }
      %rcon.cmd = 14Name:10 $regml(5) 14#:10 $regml(7) 14ID:10 $regml(9) 14Score:10 $+($regml(13),/,$iif(%score. [ $+ [ $regml(7) $+ .deaths ] ],$ifmatch,0)) 14Time:10 $regml(15) 14Address:10 $regml(22)
      set %score. [ $+ [ $regml(7) $+ .kills ] ] $regml(13)
      if (*STEAM_0* iswm $regml(9)) {
        writename $regml(9) $regml(5)
      }
    } 
  }
  if (*hltv:* iswm %rcon.cmd) {
    if ($regex(%rcon.cmd,/(#)(\s*)(\d*)(\s)(.*)(\s)(\d*)(\s)(HLTV)(\s)(hltv:)(.*)(\s)(delay:)(.*)(\s)(.*)(\s)(.*)/)) {
      %rcon.cmd = 14HLTV:10 $regml(5) 14#:10 $regml(7) 14Slots:10 $regml(12) 14Delay:10 $regml(15) 14Time:10 $regml(17) 14Address:10 $regml(19)
    }
  }
  if (*map*at: 0 x, 0 y, 0 z* iswm %rcon.cmd) {
    tokenize 32 %rcon.cmd
    set %map.current $3
    %rcon.cmd = 14Map:10 $3
  }
  if (*tcp/ip*:*:* iswm %rcon.cmd) {
    tokenize 32 %rcon.cmd
    %rcon.cmd = 14IP:10 $3
  }
  if (*players*active*max* iswm %rcon.cmd) {
    tokenize 32 %rcon.cmd
    %rcon.cmd = 14Players:10 $3-
  }
  if (*#*name*userid*uniqueid*frag*time*ping*loss*adr* iswm %rcon.cmd) {
    %rcon.cmd = $null
  }
  if (*version*:*secure* iswm %rcon.cmd) {
    tokenize 32 %rcon.cmd
    %rcon.cmd = 14Version:10 $3-
  }
  if (*hostname*:* iswm %rcon.cmd) {
    tokenize 32 %rcon.cmd
    %rcon.cmd = 14Hostname:10 $2-
  }
  if ($regex(%rcon.cmd,/(\d*)(\s)(users)/)) {
    %rcon.cmd = 14Users:10 $regml(1)
    if (%round.start = 1) {
      unset %round.start
    }
  }
  if (%rcon.cmd != $null) && (%rcon.cmd != ") {
    echo @rcon ( $+ $asctime(hh:nn:ss) $+ ) Rcon.cmd: %rcon.cmd
    if (*World triggered* iswm %rcon.cmd) { halt }
    if (*round_* iswm %rcon.cmd) { halt }
    if (*meta* iswm %rcon.cmd) { halt }
    if (*sv_password* iswm %rcon.cmd) && (*PROTECTED* !iswm %rcon.cmd) {
      set %current.pass %rcon.cmd
      %current.pass = $remove(%current.pass,sv_password is)
      %current.pass = $remove(%current.pass,$chr(32))
      halt
    }
    if (*PROTECTED* iswm %rcon.cmd) {
      getpass
      halt
    }
    if (*RCON cvar* iswm %rcon.cmd) {
      write rcon.info.txt %rcon.cmd
      goto nextread
    }
    if (*server say* iswm %rcon.cmd) {
      %rcon.cmd = $remove(%rcon.cmd,server say)
      write rcon.info.txt 10RCON say14: %rcon.cmd
    }
    else {
      write rcon.info.txt %rcon.cmd
      goto nextread
    }  
    goto nextread
  }
}

alias logoff {
  sockudp -kn dice.cmd %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " log off
  sockudp -kn dice 9001 %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " logaddress_del $ip 9001
  sockudp -kn dice 9001 %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " logaddress_del $ip 9001
  sockclose dice
  sockclose dice.cmd
}

alias logs {
  set %rcon.myip $ip
  sockudp -kn dice 9001 %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " logaddress_del %rcon.myip 9001 
  sockudp -kn dice 9001 %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " logaddress_del %rcon.myip 9001 

  sockclose dice
  sockclose dice.cmd

  .timer -m 1 200 rcon.challenge %rcon.ip %rcon.port
  .timer -m 1 500 rcon.logs

}

on *:Text:!rcon *:*:{
  if ($nick isop %pug.chan) {
    if ($2 = quit) || ($2 = rcon_password) { .msg $nick Disabled command | halt }
    if ($2 = logs) {
      logs
      .msg $nick Logs restarted
      logs
      halt
    }
    if (*pass* iswm $2-) && (!$3) {
      getpass
      .timer 1 1 msg $nick 10Pass:14 %current.pass
      halt
    }
    write -c rcon.info.txt

    rcon $2-
    .timer 1 2 play $nick rcon.info.txt 
  }
}

on *:TEXT:!logs:#:{
  if ($nick isop %pug.chan) {
    logs
    msg $chan Logs restarted
  }
}

alias scoreupdate {
  if ($regex(%rcon.data,/(Team)(\s)(CT|TERRORIST)(\s)(triggered)(\s)(.*)(\s)(.*)(\s)(\d*)(.*)(\s)(.*)(\s)(\d*)(.*)(.*)/)) {
    if (%match.on = 1) {
      if (%half.1 = 1) {
        rcon say Progressive score: (Ts: %Tscore $+ ) (CTs: %CTscore $+ )
        msg %pug.chan $codes(Progressive 1st-half score:4 Terrorist: %Tscore 12CT: %CTscore)
        halt
      }
      if (%half.2 = 1) && (!%half.1) {
        rcon say Progressive score: (Ts: $calc(%Tscore + %CTsh1) $+ ) (CTs: $calc(%CTscore + %Tsh1) $+ )
        msg %pug.chan $codes(0Progressive 2nd-half score:12 CT: %CTscore 4Terrorist: %Tscore $+ . Overall: 12CT: $calc(%CTscore + %Tsh1) 4Terrorist: $calc(%Tscore + %CTsh1))
        halt
      }
    }
    if (!%match.on) {
      if ($regml(3) = TERRORIST) {
        %rcon.data = 4 $+ $team($regml(3)) $regml(3) 14triggered4 $wintype($regml(7)) 12(CT: %CTscore $+ )  4(T: %Tscore $+ )
      }
      if ($regml(3) = CT) {
        %rcon.data = 12 $+ $team($regml(3)) 14triggered12 $wintype($regml(7)) 12(CT: %CTscore $+ )  4(T: %Tscore $+ )
      }
    }
  }
}

alias team {
  if ($1 = TERRORIST) {
    return Terrorists
  }
  if ($1 = CT) {
    return CTs
  }
}

alias wintype {
  return $replace($1,$chr(95),$chr(32)) $+ !
}

alias manscoreupdate {
  if (%match.on = 1) {
    if (%half.1) {
      rcon say Progressive score: (Ts: %Tscore $+ ) (CTs: %CTscore $+ )
      halt
    }
    if (%half.2) {
      rcon say Progressive score: (Ts: $calc(%Tscore + %CTsh1) $+ ) (CTs: $calc(%CTscore + %Tsh1) $+ )
    }
  }
}

alias stophalf {
  if (%half.1) {
    rcon say First half stopped. !lo3 to restart. 
    msg %pug.chan $codes(First half stopped)
  }
  if (%half.2) {
    rcon say Second half stopped. !lo3 to restart.
    msg %pug.chan $codes(Second half stopped)
  }
  unset %match.on
}

alias hltv.connect {
  .run rcon6.exe 210.50.4.55,27020, $+ %rcon.password $+ ,serverpassword %password ; connect %rcon.ip $+ : $+ %rcon.port
}


alias hltv.record {
  .run rcon6.exe 210.50.4.55,27020, $+ %rcon.password $+ ,record pug
  set %demo.name pug- $+ $date(yymmddHHnn) $+ - $+ %map.current $+ .dem
}

alias hltv.stop {
  .run rcon6.exe 210.50.4.55,27020, $+ %rcon.password $+ ,stoprecording
}

alias sortscores {
  set %rcon.data $strip(%rcon.data)
  if ($regex(%rcon.data,/(Team)(\s)(CT|TERRORIST)(\s)(triggered)(\s)(.*)(\s)(.*)(\s)(\d*)(.*)(\s)(.*)(\s)(\d*)(.*)(.*)/)) {
    %CTscore = $regml(11)
    %Tscore = $regml(16)
  }
  if (!%match.on) { scoreupdate | halt }
  if ($calc(%CTscore + %Tscore) = 15) && (%half.1 = 1) {
    set %CTsh1 %CTscore
    set %Tsh1 %Tscore
    set %half.2 1
    unset %half.1
    msg %pug.chan $codes(First Half Over. 12(CT: %CTscore $+ ) 4(T: %Tscore $+ ))
    copy -o stats.txt stats.1st.txt
    rcon say First Half Over. CTs: %CTscore Ts: %Tscore 
    rcon say Round restarting in 5 seconds. Please change teams.
    rcon say To commence second half type !lo3
    rcon mp_limiteams 6
    rcon sv_restartround 5
    unset %CTscore
    unset %Tscore
    unset %match.on
    halt
  }
  if (%half.2 = 1) {
    if ($calc(%CTscore + %Tsh1) = 16) || ($calc(%Tscore + %CTsh1) = 16) {
      set %CTsh2 %CTscore
      set %Tsh2 %Tscore
      unset %scorebot 
      unset %leave*
      .timersteam* off
      set %CTtotal $calc(%CTsh2 + %Tsh1)
      set %Ttotal $calc(%Tsh2 + %CTsh1)
      if (%CTtotal > %Ttotal) {
        rcon say Match Completed. Counter Terrorists Win! CTs: %CTtotal Ts: %Ttotal
        msg %pug.chan $codes(Match Completed. 12Counter Terrorists Win! 12(CT: %CTtotal $+ ) 4(T: %Ttotal $+ ))
        unset %match.on
        spamstats
        unset %half.* %match.on %scores %CTscore %Tscore %CTsh1 %Tsh1 %CTsh2 %Tsh2
        halt
      }
      if (%CTtotal < %Ttotal) {
        msg %pug.chan $codes(Match Completed. 4Terrorists Win! 4(T: %Ttotal $+ ) 12(CT: %CTtotal $+ ))
        unset %match.on
        spamstats
        rcon say Match Completed. Terrorists Win! Ts: %Ttotal CTs: %CTtotal
        unset %half.* %match.on %scores %CTscore %Tscore %CTsh1 %Tsh1 %CTsh2 %Tsh2
        halt
      }      
    }
    if ($calc(%CTscore + %Tscore) = 15) {
      unset %leave*
      .timersteam* off
      set %CTsh2 %CTscore
      set %Tsh2 %Tscore
      unset %scorebot 
      set %CTtotal $calc(%CTsh2 + %Tsh1)
      set %Ttotal $calc(%Tsh2 + %CTsh1)
      unset %match.on
      spamstats
      msg %pug.chan $codes(Match Completed. 10It was a tie! 4Ts: %Ttotal 12CTs: %CTtotal)
      rcon say Match Completed. It was a tie! CTs: %CTtotal Ts: %Ttotal
      unset %half.* %match.on %scores %CTscore %Tscore %CTsh1 %Tsh1 %CTsh2 %Tsh2 
      halt
    }
  }
  scoreupdate
}

alias getpass {
  rcon sv_password
  rcon sv_password
}

alias lo3 {
  if (%lo3) { halt }
  set -u10 %lo3 1
  rcon status
  rcon exec war.cfg
  ;  hltv.record
  unset %match.on
  if (!%half.1) && (!%half.2) {
    set %half.1 1
  }
  if (%half.1 = 1) { 
    write -c stats.txt
    unset %score.*
    msg %pug.chan $codes(First half commencing)
  }
  if (%half.2 = 1) {
    copy -o stats.1st.txt stats.txt
    unset %score.*
    msg %pug.chan $codes(Second half commencing) 
  }
  rcon say Live in 3 restarts
  rcon sv_restart 1
  .timer 1 2 rcon sv_restart 1
  .timer 1 4 rcon sv_restart 3
  .timer 1 6 rcon say LIVE
  .timer 1 8 set %match.on 1
}

on *:udpread:dice:{
  if ($sockerr > 0) {
    return
  }
  :nextread
  sockread -f %rcon.data


  if ($sockbr == 0) {
    return
  }
  if (%rcon.data == $null) {
    goto nextread
  }
  else {
    if (*"*from*"*"* iswm %rcon.data) {
      %rcon.data = $null
    }
    if (*"* iswm %rcon.data) {
      %rcon.data = $remove(%rcon.data,")
    }
    if ($gettok(%rcon.data,1,32) == ����challenge) {
      set %rcon.challengenumber $gettok(%rcon.data,3,32)
    }
    if ($left(%rcon.data,7) == ����log) {
      %rcon.data = $remove(%rcon.data,$left(%rcon.data,32))
      if (%rcon.data == $null) {
        goto nextread
      }
    }
    if ($left(%rcon.data,5) == ����l) {
      %rcon.data = $remove(%rcon.data,$left(%rcon.data,5))
      if (%rcon.data == $null) {
        goto nextread
      }
    }
    :continue
    if (*Rcon:* iswm %rcon.data) {
      %rcon.data = $null
    }
    if (Bad Rcon: rcon* iswm %rcon.data) {
      %rcon.data = $null     
    }
    if ($pos(%rcon.data,$date(yyyy),1) = 12) {
      %rcon.data = $remove(%rcon.data,$left(%rcon.data,27))
    }
    if (*!lo3* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!lo3(?-i))(.*)/)) {
        if ($istok(%pes.admins,$regml(6),59)) || ($regml(6) = %pug.admin) {
          lo3
        }
      }
    }
    if (*!score* iswm %rcon.data) {
      manscoreupdate
    }
    if (*!stophalf iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!stophalf(?-i))(.*)/)) {
        if ($istok(%pes.admins,$regml(6),59)) || ($regml(6) = %pug.admin) {
          stophalf
        }
      }
    }
    if (*!endpug* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!endpug(?-i))(.*)/)) {
        if ($istok(%pes.admins,$regml(6),59)) || ($regml(6) = %pug.admin) {
          endpug
        }
      }
    }
    if (*!password* iswm %rcon.data) {
      %rcon.data = $remove(%rcon.data,(dead))
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!password(?-i))(\s)(.*)/)) {
        if ($istok(%pes.admins,$regml(6),59)) || ($regml(6) = %pug.admin) {
          msg %pug.chan $codes(Server password changed)
          rcon say Password changed to: $gettok($regml(14),1,32)
          rcon sv_password $gettok($regml(14),1,32)    
          set %password $gettok($regml(14),1,32)
          .timer 1 1 rcon sv_password
        }
      }
    }
    if (*!players* iswm %rcon.data) {
      tokenize 32 %players
      rcon say $1-5
      rcon say $6-10
    }
    if (*Spawned_With* iswm %rcon.data) {
      if ($regex(%rcon.data,/(\s*)(.*)(<)(\d*)(>)(<)(.*)(>)(<)(TERRORIST)(>)(\s)(triggered Spawned_With_The_Bomb)/)) {
        %bomb.spawn = $regml(2)
        addplantatt $regml(7)
      }
    }
    if (*!ff* iswm %rcon.data) {
      %rcon.data = $remove(%rcon.data,(dead))
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!ff(?-i))(\s)(0|1)/)) {
        if ($istok(%pes.admins,$regml(6),59)) || ($regml(6) = %pug.admin) {
          rcon mp_friendlyfire $regml(14)
          rcon say mp_friendlyfire = $regml(14)
        }
      }   
    }
    if (*!kick* iswm %rcon.data) {
      %rcon.data = $remove(%rcon.data,(dead))
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!kick(?-i))(\s)(.*)/)) {
        if ($istok(%pes.admins,$regml(6),59)) || ($regml(6) = %pug.admin) {
          rcon kick $regml(14)        
          set -u5 %kicked 1        
        }
      }   
    }
    if (*!irc* iswm %rcon.data) {
      %rcon.data = $remove(%rcon.data,(dead))
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!irc(?-i))(\s)(.*)/)) {
        if ($istok(%pes.admins,$regml(6),59)) || ($regml(6) = %pug.admin) {
          msg %pug.chan $codes(In-game: $regml(14))          
        }
      }   
    }
    if (*!get* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!get(?-i))(.*)/)) {
        if ($istok(%pes.admins,$regml(6),59)) || ($regml(6) = %pug.admin) {
          msg %pug.chan $codes(Requesting player! Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %password)
          if (%admin.get) { halt }
          set -u30 %admin.get 1
        }
      }
    }
    if (*!restart* iswm %rcon.data) || (*!rs* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!restart|!rs(?-i))(.*)/)) {
        if ($istok(%pes.admins,$regml(6),59)) || ($regml(6) = %pug.admin) {
          rcon mp_startmoney 16000
          rcon mp_freezetime 0
          rcon sv_restart 2
        }
      }
    }
    if (*!teams* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!teams(?-i))(.*)/)) {
        rcon say $iif(%half.2,CT,T) $+ : %team1
        rcon say $iif(%half.2,T,CT) $+ : %team2
      }
    }
    if (*!login* iswm %rcon.data) {
      %rcon.data = $remove(%rcon.data,(dead))
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!login(?-i))(\s*)(\w*)/)) {
        if ($regml(6) = %pug.admin) {
          rcon say You are already logged in
          halt
        }
        if (!%pug.admin) {
          if ($regml(14) = %rcon.adminpass) {
            msg %pug.chan $codes(Admin logged in: $regml(1) ( $+ $regml(6) $+ ))
            set %pug.admin $regml(6)
            rcon say $regml(1) ( $+ %pug.admin $+ ) has logged in.
            rcon say Commands: !lo3 !stophalf !rs !kick !get !password
            echo @rcon ( $+ $asctime(hh:nn:ss) $+ ) Rcon.data: $regml(1) ( $+ %pug.admin $+ ) has logged in.
          }
        }
      }
    }
    if (*connected* iswm %rcon.data) && (*disconnected* !iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(.*)(address)(.*)(:)(.*)/)) {
        if ($regml(6) = HLTV) { halt }
        writeini ips.ini IPS $regml(3) $regml(10)
        %rcon.data = 3 $+ $regml(1) 14has connected
        .timer 1 30 remini ips.ini IPS $regml(3)
      }
    }
    if (*disconnected* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)(disconnected)/)) {
        %rcon.data = 3 $+ $regml(1) 14( $+ $regml(6) $+ )3 disconnected
        if ($regml(6) = HLTV) { halt }
        if ($regml(9) = SPECTATOR) {
          write -dw $+ $regml(6) $+ * stats.txt
          halt 
        }
        if (*STEAM_0* iswm $regml(6)) {
          writename $regml(6) $regml(1)
        }
        if (%half.1) {
          set %disconnect. [ $+ [ $regml(6) ] ] $calc(%Tscore + %CTscore)
          unset %connect. [ $+ [ $regml(6) ] ]
        }
        if (%half.2) {
          set %disconnect. [ $+ [ $regml(6) ] ] $calc(%Tscore + %CTscore + 15)
          unset %connect. [ $+ [ $regml(6) ] ]
        }
        if ($regml(6) = %pug.admin) {
          rcon say Admin has left. Admin pass is %rcon.adminpass
          unset %pug.admin
        }
        else { 
          if (!%kicked) && (%match.on) {
            rcon say $regml(1) has disconnected, !bl enabled for 30 seconds.
            set -u30 %leaver $regml(6)
          }
        }
      }
    }
    if (*!bl* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)((?i)!bl(?-i))(.*)/)) {
        if ($istok(%pes.admins,$regml(6),59)) || ($regml(6) = %pug.admin) {
          if (%leaver) {
            bl %leaver
          }
          else {
            rcon say No leaver found
          }
        }
      }
    }
    if (*entered the game* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(>)(.*)(entered the game)/)) {
        %rcon.data = 3 $+ $regml(1) 14(# $+ $regml(3) $+ : $regml(6) $+ )3 has entered the game
        if (%leaver = $regml(6)) { unset %leaver }
        if (%half.1) {
          set %connect. [ $+ [ $regml(6) ] ] $calc(%Tscore + %CTscore)
        }
        if (%half.2) {
          set %connect. [ $+ [ $regml(6) ] ] $calc(%Tscore + %CTscore + 15)
        }
        if (*STEAM_0* iswm $regml(6)) {
          writename $regml(6) $regml(1)
        }
      }
    }
    if (*joined team* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)(joined team)(\s)(.*)/)) {
        if ($regml(14) = TERRORIST) {
          %rcon.data = 3 $+ $regml(1) 14joined team4 $regml(14)
        }
        if ($regml(14) = CT) {
          %rcon.data = 3 $+ $regml(1) 14joined team12 $regml(14)
        }
        if ($regml(14) = SPECTATOR) {
          %rcon.data = 3 $+ $regml(1) 14joined team10 $regml(14)
          write -dw $+ $regml(6) $+ * stats.txt
        }
      }
    }
    if (*STEAM USERID validated* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<>)(.*)(STEAM USERID)/)) {
        if ($readini(ips.ini,IPS,$regml(3))) {
          %rcon.data = 3 $+ $regml(1) 14(# $+ $regml(3) $+ : $regml(6) @ $readini(ips.ini,IPS,$regml(3)) $+ ) 3has connected
          if (%pug) {
            msg %pug.chan $codes(3 $+ $regml(1) 14(# $+ $regml(3) $+ : $regml(6) $+ ) 3has connected)
          }
        }
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(TERRORIST)(>)(\s)(triggered Planted_The_Bomb)/)) {
      addplant $regml(6)
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(TERRORIST)(>)(\s)(triggered Got_The_Bomb)/)) {
      addplantatt $regml(6)
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT)(>)(\s)(triggered Begin_Bomb_Defuse_)(With|Without)(_Kit)/)) {
      adddefatt $regml(6)
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT)(>)(\s)(triggered Defused_The_Bomb)/)) {
      adddefuse $regml(6)
    }
    if (*has inconsistent* iswm %rcon.data) { 
      $regex(%rcon.data,/(])(.*)(<)(\d*)(>)(<)(\S*)(>)(<)(>)(\s)(has inconsistent file:)(.*)/)
      rcon say custom file disallowed: $regml(13)
      msg %pug.chan $codes(Bad file: $regml(2) ( $+ $regml(7) $+ ) has been kicked for use of custom file: $regml(13))
    }
    if (*attacked* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(><)(.*)(><)(TERRORIST)(> attacked)(\s)(.*)(<)(\d*)(><)(.*)(><)(CT)(>)( with )(.*)(damage )(\d*)(\S)(.*)(health)(\s)(-?)(\d*)(\S)/)) {
        if ($regml(26)) {
          adddamage $regml(5) $calc($regml(21) - $regml(27))
          adddamager $regml(14) $calc($regml(21) - $regml(27))
        }
        if (!$regml(26)) {
          adddamage $regml(5) $regml(21)
          adddamager $regml(14) $regml(21) 
        }
      }
      if ($regex(%rcon.data,/(.*)(<)(\d*)(><)(.*)(><)(CT)(> attacked)(\s)(.*)(<)(\d*)(><)(.*)(><)(TERRORIST)(>)( with )(.*)(damage )(\d*)(\S)(.*)(health)(\s)(-?)(\d*)(\S)/)) {
        if ($regml(26)) {
          adddamage $regml(5) $calc($regml(21) - $regml(27))
          adddamager $regml(14) $calc($regml(21) - $regml(27))
        }
        if (!$regml(26)) {
          adddamage $regml(5) $regml(21)
          adddamager $regml(14) $regml(21) 
        }
      }
      %rcon.data = $null
    }
    if (*killed* iswm %rcon.data) {
      if ($regex(kills,%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(>)(.*)(killed)(\s)(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(> with )(.*)/)) {
        if ($regml(kills,9) = CT) {
          if ($regml(kills,22) = CT) {
            addtk $regml(kills,6)
            dec %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 12 $+ $regml(kills,1) $score($regml(kills,3)) teamkilled12 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)
          }
          else {
            inc %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 12 $+ $regml(kills,1) $score($regml(kills,3)) killed4 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)
            addkill $regml(kills,6) 
            adddeath $regml(kills,19)
            addweapon $regml(kills,6) $regml(kills,24)        
          }      
        }
        if ($regml(kills,9) = TERRORIST) {
          if ($regml(kills,22) = TERRORIST) {
            dec %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 4 $+ $regml(kills,1) $score($regml(kills,3)) teamkilled4 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)
            )   
          }
          else {
            inc %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 4 $+ $regml(kills,1) $score($regml(kills,3)) killed12 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)
            addkill $regml(kills,6) 
            adddeath $regml(kills,19)   
            addweapon $regml(kills,6) $regml(kills,24)        
          }
        }
        if (%match.on) {
          msg %pug.chan $codes(%rcon.data)
        }
      }
    }
    if (*committed suicide* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(>)(\s)(committed suicide with)(\s)(.*)/)) {
        addsuicide $regml(6)
        if ($regml(14) = worldspawn (world)) {
          inc %score. [ $+ [ $regml(3) $+ .deaths ] ]
          if ($regml(9) = CT) {
            %rcon.data = 12 $+ $regml(1) $score($regml(3)) killed self with 10worldspawn
          }
          if ($regml(9) = TERRORIST) {
            %rcon.data = 4 $+ $regml(1) $score($regml(3)) killed self with 10worldspawn
          }
          if (%match.on) {          
            msg %pug.chan $code1 %rcon.data
          }
        }
        if ($regml(14) = grenade) {
          dec %score. [ $+ [ $regml(3) $+ .kills ] ]
          inc %score. [ $+ [ $regml(3) $+ .deaths ] ]
          if ($regml(9) = CT) {
            %rcon.data = 12 $+ $regml(1) $score($regml(3)) killed self with 10grenade
          }
          if ($regml(9) = TERRORIST) {
            %rcon.data = 4 $+ $regml(1) $score($regml(3)) killed self with 10grenade
          }
          if (%match.on) {          
            msg %pug.chan $code1 %rcon.data
          }
        }
        if ($regml(14) = world) {
          dec %score. [ $+ [ $regml(3) $+ .kills ] ]
          inc %score. [ $+ [ $regml(3) $+ .deaths ] ]
          if ($regml(9) = CT) {
            %rcon.data = 12 $+ $regml(1) $score($regml(3)) killed self with 10world
          }
          if ($regml(9) = TERRORIST) {
            %rcon.data = 4 $+ $regml(1) $score($regml(3)) killed self with 10world
          }
        }
      }
    }
    if (*Team CT triggered* iswm %rcon.data) || (*Team TERRORIST triggered* iswm %rcon.data) {
      if (*Team TERRORIST Triggered Target_Bombed* iswm %rcon.data) {
        set %score. [ $+ [ %bomb.planter $+ .kills ] ] $iif(%score. [ $+ [ %bomb.planter $+ .kills ] ],$calc($ifmatch + 3),3)
        unset %bomb.planter
      }
      sortscores
    }
    if (*loading map* iswm %rcon.data) {
      tokenize 32 %rcon.data
      if ($0 = 3) {
        unset %score.*
      }
    }
    if (*started map de_* iswm %rcon.data) {
      tokenize 32 %rcon.data
      if (%pug) {
        msg %pug.chan $codes(Map change: $3)
      }
    }
    if (*Planted_The_Bomb* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(>)(\s)(triggered)(\s)(Planted_The_Bomb)/)) {
        set %bomb.planter $regml(3)
      }
    }
    if (*Defused_The_Bomb* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(>)(\s)(triggered)(\s)(Defused_The_Bomb)/)) {
        set %score. [ $+ [ $regml(3) $+ .kills ] ] $iif(%score. [ $+ [ $regml(3) $+ .kills ] ],$calc($ifmatch + 3),3)
      }
    }
    if (*Server*cvar*sv_restart*=* iswm %rcon.data) {
      tokenize 32 %rcon.data
      if ($0 = 5) {
        .timer 1 $iif($5 != 0,$5,1) unset %score.* %kills.round.*
        if (%match.on = 1) && ($5 != 0) {
          msg %pug.chan $codes(*** Round restarting in  $+ $5 $+  seconds ***)
        }
      }
    }
    if (*Server shutdown* iswm %rcon.data) {
      tokenize 32 %rcon.data
      if ($0 = 2) {
        unset %score.*
      }
    }
    if (*World triggered* iswm %rcon.data) {
      if (*world triggered Game_Commencing* iswm %rcon.data) {
        unset %score.*
      }
      if (*World triggered Round_Start* iswm %rcon.data) {
        if (%match.on) {
          msg %pug.chan $codes(14*** World triggered Round Start 4( $+ %bomb.spawn spawned with the bomb)14 ***)
          unset %kills.round.*
        }
      }
    }
    if (*kick:* iswm %rcon.data) {
      if ($regex(%rcon.data,/(Kick:)(\s)(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)(was kicked by Console)/)) {
        if (%match.on) {
          msg %pug.chan $codes(3 $+ $regml(3) 14( $+ $regml(8) $+ ) 3was kicked!)
        }
        set -u5 %kicked 1
      }
    }
    if (*changed name to* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(\s)(changed name to)(\s)(.*)/)) {
        %rcon.data = 10 $+ $regml(1) changed name to10 $regml(14)
        writename $regml(6) $regml(14)
      }
    }
    if (*server say* iswm %rcon.data) {
      ;      if (*live* iswm %rcon.data) && (*[* iswm %rcon.data) || (*-----* iswm %rcon.data) { halt }
    }
    if (*say* iswm %rcon.data) && (*server say* !iswm %rcon.data) {
      %rcon.data = $remove(%rcon.data,(dead))
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST|SPECTATOR)(>)(\s)(say|say_team)(\s)(.*)/)) {
        if ($regml(9) = TERRORIST) {
          %rcon.data = 4 $+ $regml(1)  $+ $regml(12) $+ : $regml(14)
        }
        if ($regml(9) = CT) {
          %rcon.data = 12 $+ $regml(1)  $+  $regml(12) $+ : $regml(14)
        }
        if ($regml(9) = SPECTATOR) {
          %rcon.data = 10 $+ $regml(1)  $+ $regml(12) $+ : $regml(14)
        }
      }    
    }
    if (%rcon.data != $null) {
      echo @rcon ( $+ $asctime(hh:nn:ss) $+ ) Rcon.data: %rcon.data
      .timerlogs off
      .timerlogs 1 1200 logs
      unset %rcon.data
    }
    goto nextread
  }
}

alias score {
  if (%score. [ $+ [ $1 $+ .kills ] ] = $null) {
    set %score. [ $+ [ $1 $+ .kills ] ] 0
  }
  if (%score. [ $+ [ $1 $+ .deaths ] ] = $null) {
    set %score. [ $+ [ $1 $+ .deaths ] ] 0
  }
  return $chr(91) $+ %score. [ $+ [ $1 $+ .kills ] ] $+ / $+ %score. [ $+ [ $1 $+ .deaths ] ] $+ $chr(93)
}


on *:sockwrite:rcon:{
  if ($sockerr) {
    aline @rcon $sock().wserr
  }
}

on *:close:@rcon:{
  if (%rcon.scorebot == 1) {
    set %rcon.scorebot 0
  }
  set %rcon.myip $ip
}
