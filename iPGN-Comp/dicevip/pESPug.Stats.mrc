alias addkill {
  if (!%match.on) { halt }
  if ($read(stats.txt,w,$1 $+ *)) { 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),3,37) + 1),3,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),3,37) + 1),3,37)
  }

  ;Multi-kill rounds

  inc %kills.round. [ $+ [ $1 ] ]

  if ( %kills.round. [ $+ [ $1 ] ] == 2) {
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),38,37) + 1),38,37)
  }
  elseif ( %kills.round. [ $+ [ $1 ] ] == 3) {
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),38,37) - 1),38,37)
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),39,37) + 1),39,37)
  }
  elseif ( %kills.round. [ $+ [ $1 ] ] == 4) {
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),39,37) - 1),39,37)
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),40,37) + 1),40,37)
  }
  elseif ( %kills.round. [ $+ [ $1 ] ] == 5) {
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),40,37) - 1),40,37)
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),41,37) + 1),41,37)
  }
}

alias adddeath {
  if (!%match.on) { halt }
  if ($read(stats.txt,w,$1 $+ *)) { 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),4,37) + 1),4,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),4,37) + 1),4,37)
  }
}

alias delkill {
  if (!%match.on) { halt }
  if ($read(stats.txt,w,$1 $+ *)) {
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),3,37) - 1),3,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),3,37) - 1),3,37)
  }
}

alias adddamage {
  if (!%match.on) { halt }
  if ($read(stats.txt,w,$1 $+ *)) { 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),6,37) + $2),6,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),6,37) + $2),6,37)
  }
}


alias adddamager {
  if (!%match.on) { halt }
  if ($read(stats.txt,w,$1 $+ *)) { 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),7,37) + $2),7,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),7,37) + $2),7,37)
  }
}

alias writename {
  if ($read(stats.txt,w,$1 $+ *)) {
    if (* [ $+ [ $chr(39) ] $+ ] * iswm $2-) { 
      %temp.name = $remove($2-,$chr(39))
      write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),%temp.name,2,37),2,37)
      unset %temp.name
    }
    else {
      write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$2-,2,37),2,37)
    }
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    if (* [ $+ [ $chr(39) ] $+ ] * iswm $2-) { 
      %temp.name = $remove($2-,$chr(39))
      write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),%temp.name,2,37),2,37)
      unset %temp.name
    }
    else {
      write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$2-,2,37),2,37)
    }
  }
}

alias addplantatt {
  if (!%match.on) { halt }
  if ($read(stats.txt,w,$1 $+ *)) { 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),8,37) + 1),8,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),8,37) + 1),8,37)
  }
}

alias addplant {
  if (!%match.on) { halt }
  if ($read(stats.txt,w,$1 $+ *)) { 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),9,37) + 1),9,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),9,37) + 1),9,37)
  }
}

alias addtk {
  if (!%match.on) { halt }
  if ($read(stats.txt,w,$1 $+ *)) { 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),42,37) + 1),42,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),42,37) + 1),42,37)
  }
}

alias addsuicide {
  if (!%match.on) { halt }
  if ($read(stats.txt,w,$1 $+ *)) { 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),43,37) + 1),43,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),43,37) + 1),43,37)
  }
}

alias adddefatt {
  if (!%match.on) { halt }
  if ($read(stats.txt,w,$1 $+ *)) { 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),10,37) + 1),10,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),10,37) + 1),10,37)
  }
}

alias adddefuse {
  if (!%match.on) { halt }
  if ($read(stats.txt,w,$1 $+ *)) { 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),11,37) + 1),11,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),11,37) + 1),11,37)
  }
}

alias addweapon {
  if (!%match.on) { halt }
  var %weap = ak47 m4a1 awp usp glock18 deagle mp5navy galil famas grenade p228 fiveseven elite m3 xm1014 tmp mac10 ump45 p90 scout aug sg552 sg550 g3sg1 m249 knife
  var %numtok = $calc($findtok(%weap,$2,1,32) + 11)
  if ($read(stats.txt,w,$1 $+ *)) { 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),%numtok,37) + 1),%numtok,37)
  }
  else { 
    write stats.txt $+($1,$chr(37),name%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0%0) 
    write -w $+ $1 $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),%numtok,37) + 1),%numtok,37)
  }
}


;steamid%name%kills%deaths%rounds%damage%damager%plantatt%plant%defatt%defuse%ak47%m4a1%awp%usp%glock18%deagle%mp5navy%galil%famas%grenade%p228%fiveseven%elite%m3%xm1014%tmp%mac10%ump45%p90%scout%aug%sg552%sg550%g3sg1%m249%knife%2kr%3kr%4kr%5kr%tks%suicides
