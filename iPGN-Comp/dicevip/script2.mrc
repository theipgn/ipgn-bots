alias test {
  ; mode %pug.chan +m
  var %y = $lines(stats.txt)
  var %x = 1
  while (%x <= %y) {
    tokenize 37 $read(stats.txt,%x)
    if ( %disconnect. [ $+ [ $1 ] ] ) && ( !%connect. [ $+ [ $1 ] ] ) {
      %rounds = %disconnect. [ $+ [ $1 ] ]
    }
    elseif ( %disconnect. [ $+ [ $1 ] ] ) && ( %connect. [ $+ [ $1 ] ] ) {
      %rounds = $calc(%CTtotal + %Ttotal - $calc( %connect. [ $+ [ $1 ] ] - %disconnect. [ $+ [ $1 ] ] ))
    }
    elseif ( %connect. [ $+ [ $1 ] ] ) && ( !%disconnect. [ $+ [ $1 ] ] ) {
      %rounds = $calc(%CTtotal + %Ttotal - %connect. [ $+ [ $1 ] ] )
    }
    else {
      %rounds = $calc(%CTtotal + %Ttotal)
    }
    %rounds = 30
    write -l $+ $readn stats.txt $puttok($read(stats.txt,$readn),%rounds,5,37)

    ;  echo -a   .msg %pug.chan $codes(12 $+ $2 (12 $+ $1 $+ ) - Kills:12 $3 Deaths:12 $4 Ratio:12 $round($calc($3 / $4),3) Damage:12 $6 DPR:12 $round($calc($6 / %rounds),3) DamageRec:12 $7 DRPR:12 $round($calc($7 / %rounds),3) (12 $+ %rounds rounds)) 

    if ($read(statsall.txt,w,$1 $+ $chr(37) $+ *)) { 
      echo -a $v1
      var %a = $numtok($v1,37)
      var %b = 2
      %ln = $readn
      while (%b <= %a) {
        if (%b = 2) {
          write -l $+ %ln statsall.txt $puttok($read(statsall.txt,%ln),$gettok($read(stats.txt,%x),%b,37),2,37)
        } 
        else {
          write -l $+ %ln statsall.txt $puttok($read(statsall.txt,%ln),$calc($gettok($read(statsall.txt,%ln),%b,37) + $gettok($read(stats.txt,%x),%b,37)),%b,37)
        }
        inc %b
      }
    }
    else { 
      write statsall.txt $read(stats.txt,%x)
    }   
    unset %rounds
    inc %x
  }
  ;steamid%name%kills%deaths%rounds%damage%damager
}
