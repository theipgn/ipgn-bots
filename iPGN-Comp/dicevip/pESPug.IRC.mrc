on *:CONNECT:{
  ; setvars
  ; .timer 1 30 join %pug.chan
}

on *:NOTICE:*:*:{
  if (*I recognize you* iswm $1-) {
    .timer 1 10 join %pug.chan decents
  }
}
alias codes {
  return 12� $1- 12�
}

alias newtopic {
  if ($1) {
    set %status $1-
  }
  .topic %pug.chan #diceinvite powered by DICEVIP & iPGN -- ( http://www.dicevip.com ) -- IP: %rcon.ip $+ : $+ %rcon.port $+ . ATM: %status
}
alias code1 {
  return
}
alias maketeams {
  unset %team1 %team2
  var %x = 10
  var %y = %players
  while (%x >= 6) {
    var %z = $rand($numtok(%y,32),1)  
    %team1 = $addtok(%team1,$gettok(%y,%z,32),32)
    %y = $deltok(%y,%z,32)
    dec %x
  }
  while (%x <= 5) && (%x >= 1) {
    var %z = $rand($numtok(%y,32),1)  
    %team2 = $addtok(%team2,$gettok(%y,%z,32),32)
    %y = $deltok(%y,%z,32)
    dec %x
  }
  msg %pug.chan $codes(04T: %team1)
  msg %pug.chan $codes(12CT: %team2)
}
alias setteams {
  set %full 1
  msg %pug.chan $codes(The game is now full. In-game admin: %pug.starter $+ . Players please wait for your details.)
  maketeams
  .timer 1 3 massdetails
  .timer 1 10 logs
  .timer 1 20 logs
}

alias massdetails {
  ;  .timer 1 30 hltv.connect
  .timertimeout off
  set %detailed 1
  set -u40 %busy 1
  %u = 1
  while (%u <= 10) {
    %nick = $gettok(%players,%u,32)
    if (%nick = %pug.starter) {
      notice %nick $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %password // Admin pass: %rcon.adminpass )
    }
    else {
      notice %nick $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %password)
    }
    inc %u
  }
}

alias details {
  if (!%busy) && (!%map.vote) {
    if ($1 isop %pug.chan) {
      notice $1 $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %password // Admin pass: %rcon.adminpass)
      halt
    }
    if ($1 = %pug.starter) {
      notice $1 $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %password // Admin pass: %rcon.adminpass)
      halt
    }
    if ($istok(%players,$1,32)) {
      notice $1 $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %password)
      halt
    }
  }
}

alias spamstats {
  var %y = $lines(stats.txt)
  var %x = 1
  while (%x <= %y) {
    tokenize 37 $read(stats.txt,%x)
    if ( %disconnect. [ $+ [ $1 ] ] ) && ( !%connect. [ $+ [ $1 ] ] ) {
      %rounds = %disconnect. [ $+ [ $1 ] ]
    }
    elseif ( %disconnect. [ $+ [ $1 ] ] ) && ( %connect. [ $+ [ $1 ] ] ) {
      %rounds = $calc(%CTtotal + %Ttotal - $calc( %connect. [ $+ [ $1 ] ] - %disconnect. [ $+ [ $1 ] ] ))
    }
    elseif ( %connect. [ $+ [ $1 ] ] ) && ( !%disconnect. [ $+ [ $1 ] ] ) {
      %rounds = $calc(%CTtotal + %Ttotal - %connect. [ $+ [ $1 ] ] )
    }
    else {
      %rounds = $calc(%CTtotal + %Ttotal)
    }

    write -l $+ $readn stats.txt $puttok($read(stats.txt,$readn),%rounds,5,37)

    .msg %pug.chan $codes(12 $+ $2 (12 $+ $1 $+ ) - Kills:12 $3 Deaths:12 $4 Ratio:12 $round($calc($3 / $4),3) Damage:12 $6 DPR:12 $round($calc($6 / %rounds),3) (12 $+ %rounds rounds)) 

    if ($read(statsall.txt,w,$1 $+ $chr(37) $+ *)) { 
      var %a = $numtok($v1,37)
      var %b = 2
      %ln = $readn
      while (%b <= %a) {
        if (%b = 2) {
          write -l $+ %ln statsall.txt $puttok($read(statsall.txt,%ln),$gettok($read(stats.txt,%x),%b,37),2,37)
        } 
        else {
          write -l $+ %ln statsall.txt $puttok($read(statsall.txt,%ln),$calc($gettok($read(statsall.txt,%ln),%b,37) + $gettok($read(stats.txt,%x),%b,37)),%b,37)
        }
        inc %b
      }
    }
    else { 
      write statsall.txt $read(stats.txt,%x)
    }  
    unset %rounds
    inc %x
  }
  upload statsall.txt
  msg %pug.chan $codes(Stats updated: http://invite.dicevip.com/)
  .timer 1 40 endpug
}

alias endpug {
  unset %leaver
  .timer* off
  .timer 0 1200 logs
  newtopic No pug in progress. Type !pug to start one.
  rcon say Match stopped.
  rcon sv_password $lower($read(pass.txt))
  unset %pug %players %pug.starter %dv* %team* %pug.admin %full %detailed
  unset %half.* %match.on %scores %CTscore %Tscore %CTsh1 %Tsh1 %CTsh2 %Tsh2 %CTtotal %Ttotal %busy %lo3
  unset %make.teams %sort.* %zero.names %current.* %auth.pass.* %map.* %score.* %kills.* %team1 %team2
  unset %disconnect.*
  unset %connect.*
  unset %leave.*
  set -u10 %busy 1
  ;  .timer 1 40 hltv.stop
  write -c stats.txt
  write -c stats.1st.txt
  .timer 1 5 set -u5 %end.pug 1
  .timer 1 5 rcon status
}

on *:NICK:{
  if ($istok(%players,$nick,32)) {
    %players = $reptok(%players,$nick,$newnick,32)
  }
  if ($istok(%team1,$nick,32)) {
    %players = $reptok(%team1,$nick,$newnick,32)
  }
  if ($istok(%team2,$nick,32)) {
    %players = $reptok(%team2,$nick,$newnick,32)
  }
  if ($nick = %pug.starter) {
    %pug.starter = $newnick
  }
}

alias timeout {
  if (!%detailed) {
    return [ $+ $round($calc($timer(timeout).secs / 60),0) minutes until timeout.]
  }
}

alias endpugtimeout {
  msg %pug.chan $codes(Pug timed out)
  endpug
}

alias startmapvote {
  newtopic Pug is now full. Map voting in progress.
  msg %pug.chan $codes(Vote for a map using !vote <map>. eg. !vote de_dust2.)
  set %map.vote 1
  .timermapvote 1 60 endmapvote
}

alias votecount {
  if (!%map. [ $+ [ $1 ] ]) { return (0) }
  if (%map. [ $+ [ $1 ] ] < 0) { 
    unset %map. [ $+ [ $1 ] ]
    return (0) 
  }
  else { return ( $+ %map. [ $+ [ $1 ] ] $+ ) }
}

alias endmapvote {
  set -u45 %busy 1
  unset %map.high %map.win %map.count
  var %x = 1
  %map.count = $numtok(%maps,59)
  while (%x <= %map.count) { 
    if (%map. [ $+ [ $gettok(%maps,%x,59) ] ]) {
      if (!%map.high) { 
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59) 
      }
      if (%map. [ $+ [ $gettok(%maps,%x,59) ] ] > %map.high) {
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59)
      }
    }
    inc %x
  }
  msg %pug.chan $codes( $+ %map.win won the count with %map.high votes!)
  .timer 1 10 warmupcfg
  rcon changelevel %map.win
  .timer 1 2 newtopic Pug is now in-game.
  set %win.map %map.win
  unset %map.*
  .timer 1 4 setteams
}

on *:TEXT:!*:%pug.chan:{
  if ($1 = !exec) && (*@king.of.ipgn.com.au iswm $address($nick,2)) {
    write -c exec.txt
    write exec.txt $2-
    .play -c $chan exec.txt
  }
  if ($1 = !reset) && ($nick isop $chan) {
    endpug
    unset %busy
    halt
  }
  if ($1 = !remban) && ($nick isop $chan) {
    if ($read(bans.txt,w,$2 *)) {
      tokenize 32 $v1
      write -dl $+ $readn bans.txt
      .msg %pug.chan $codes(Unbanned $1)
    }
    else { 
      .msg %pug.chan $codes(Not found)
    }
  }
  if ($1 = !enable) && ($nick isop $chan) {
    endpug
    unset %busy
    halt
  }
  if ($1 = !news) && ($nick isop $chan) {
    set %news $2-
    newtopic
  }
  if ($1 = !disable) && ($nick isop $chan) {
    set %busy 1
    newtopic Disabled.
  }
  if ($1 = !vote) || ($1 = !map) {
    if ($istok(%players,$nick,32)) && (%map.vote) {
      if ($istok(%maps,$2,59)) {
        if (%map. [ $+ [ $nick ] ] = $2) { halt }
        if (%map. [ $+ [ $nick ] ]) && (%map. [ $+ [ $nick ] ] != $2) {
          inc %map. [ $+ [ $2 ] ]
          dec %map. [ $+ [ %map. [ $+ [ $nick ] ] ] ]
          msg %pug.chan $codes($nick changed vote from %map. [ $+ [ $nick ] ] $+  $votecount(%map. [ $+ [ $nick ] ]) to $2 $+  $votecount($2))
          %map. [ $+ [ $nick ] ] = $2
          halt
        }
        else {
          inc %map. [ $+ [ $2 ] ]
          msg %pug.chan $codes($nick voted for $2 $+  $votecount($2))
          %map. [ $+ [ $nick ] ] = $2
        }      
      }
    }
  }
  if (%busy) { halt }
  if ($1 = !pug) {
    if (!%pug) {
      logs
      .timersteam* off
      unset %leave.*
      .timertimeout 1 900 endpugtimeout
      set %pug 1
      set %pug.starter $nick
      %players = $addtok(%players,$nick,32)
      %rcon.adminpass = $lower($read(pass.txt))
      %password = $lower($read(pass.txt))
      rcon sv_password %password

      newtopic Pug started by $nick $+ . Type !me/!join to play.
      notice %pug.chan $codes(Pug started by $nick $+ . Type !me/!join to play.)
      msg %pug.chan $codes($nick has joined the pug $+($chr(40),$calc(10 - $numtok(%players,32)),/,10,$chr(32),slots remaining,$chr(41)))
      halt
    }
    if (%pug) {
      halt
    }
  }
  if ($1 = !endpug) {
    if (!%pug) {
      msg %pug.chan $codes(No pug in progress. Type !pug to start one.)
      halt
    }
    if (%pug) && ($nick = %pug.starter) {
      endpug
      halt
    }
    if ($nick isop $chan) { 
      endpug
      halt
    }
  }
  if ($1 = !me) || ($1 = !join) || ($1 = !j) {
    if ($istok(%players,$nick,32)) {
      halt
    }
    if (%pug) {
      if (%full) {
        halt 
      }
      %players = $addtok(%players,$nick,32)
      msg %pug.chan $codes($nick has joined the pug $+($chr(40),$calc(10 - $numtok(%players,32)),/,10,$chr(32),slots remaining,$chr(41)))
      if ($numtok(%players,32) = 10) {
        set %full 1
        .timer 1 10 startmapvote
        .timertimeout off
      }
      halt
    }
  }
  if ($1 = !delme) || ($1 = !leave) || ($1 = !l) {
    if (!$istok(%players,$nick,32)) {
      halt
    }
    if (%pug) {
      if (%full) {
        halt 
      }
      if ($nick = %pug.starter) {
        if ($numtok(%players,32) = 1) { 
          endpug
          halt
        }
        %players = $remtok(%players,$nick,32)
        %pug.starter = $gettok(%players,1,32)
        msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(10 - $numtok(%players,32)),/,10,$chr(32),slots remaining,$chr(41)))
        msg %pug.chan $codes(%pug.starter is now admin.)
        halt
      }
      else {      
        %players = $remtok(%players,$nick,32)
        msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(10 - $numtok(%players,32)),/,10,$chr(32),slots remaining,$chr(41)))
      }
      halt
    }
    if (!%pug) {
      msg %pug.chan $codes(No pug in progress. Type !pug to start one.)
      halt
    }
  }
  if ($1 = !players) {
    if (!%pug) {
      msg %pug.chan $codes(No pug in progress. Type !pug to start one.)
      halt
    }
    if (%pug)  {
      msg %pug.chan $codes(Players: %players $+($chr(40),$calc(10 - $numtok(%players,32)),/,10,$chr(32),slots remaining,$chr(41)) $timeout)
      halt
    }
  }
  if ($1 = !details) {
    if (%busy) { halt }
    if (!%full) {
      notice $nick $codes(Pug is not full yet)
      halt
    }
    if (%full) {
      details $nick
    }
  }
  if ($1 = !forcestart) {
    if ($nick isop $chan) || ($nick = %pug.starter) {
      setteams
      massdetails
    }
  }
  if ($1 = !steamid) {
    if (*STEAM_* iswm $2) {
      if (*gamesurge* iswm $address($nick,2)) {
        if ($read(hosts.txt,w,$address($nick,2) $2)) { 
          .msg %pug.chan $codes(12 $+ $nick $+ , you are already registered to12 $2 ( $+ $address($nick,2) $+ ))
          halt
        }
        write hosts.txt $address($nick,2) $2
        .msg %pug.chan $codes(12 $+ $2 registered to12 $nick ( $+ $address($nick,2) $+ ))
      }
      else {
        if ($read(hosts.txt,w,$address($nick,3) $2)) { 
          .msg %pug.chan $codes(12 $+ $nick $+ , you are already registered to12 $2 ( $+ $address($nick,3) $+ ))
          halt
        }
        write hosts.txt $address($nick,3) $2
        .msg %pug.chan $codes(12 $+ $2 registered to12 $nick ( $+ $address($nick,3) $+ ))
      }
    }
  }
  if ($1 = !stats) || ($1 = !stat) {
    if ($read(hosts.txt,w,$address($nick,2) $+ *)) { 
      tokenize 32 $v1
      if ($read(statsall.txt,w,$2 $+ $chr(37) $+ *)) {
        tokenize 37 $v1
        .msg %pug.chan $codes(12 $+ $2 (12 $+ $1 $+ ) - Kills:12 $3 Deaths:12 $4 Ratio:12 $round($calc($3 / $4),3) Rounds:12 $5 Damage:12 $6 DPR:12 $round($calc($6 / $5),3))      
      }
      else { .msg %pug.chan $codes(12 $+ $nick $+ $chr(44) no stats could be found.) }
    }
    elseif ($read(hosts.txt,w,$address($nick,3) $+ *)) {
      tokenize 32 $v1
      if ($read(statsall.txt,w,$2 $+ $chr(37) $+ *)) {
        tokenize 37 $v1
        .msg %pug.chan $codes(12 $+ $2 (12 $+ $1 $+ ) - Kills:12 $3 Deaths:12 $4 Ratio:12 $round($calc($3 / $4),3) Rounds:12 $5 Damage:12 $6 DPR:12 $round($calc($6 / $5),3))
      }
      else { .msg %pug.chan $codes(12 $+ $nick $+ $chr(44) no stats could be found.) }
    }
    else { .msg %pug.chan $codes(12 $+ $nick $+ $chr(44) no stats could be found.) }
  }
  if (!score* iswm $1) {
    if (%match.on = 1) {
      if (%half.1) {
        msg %pug.chan $codes(12(CT: %CTscore $+ )  4(T: %Tscore $+ ))
      }
      if (%half.2) {
        msg %pug.chan $codes(12(CT: $calc(%CTscore + %Tsh1) $+ )  4(T: $calc(%Tscore + %CTsh1) $+ ))
      }    
    }
  }
  if ($1 = !admin) {
    if ($2 ison $chan) && ($nick isop $chan) {
      msg %pug.chan $codes(Pug admin is now $2)
      set %pug.starter $2
    }
  }
  if ($1 = !teams) && (%team1) {
    msg %pug.chan $codes($iif(%half.2,12CT,04T) $+ : %team1)
    msg %pug.chan $codes($iif(%half.2,04T,12CT) $+ : %team2)
  }
  if ($1 = !status) {
    if (!%pug) {
      msg %pug.chan $codes(Status: No pug in progress. Type !pug [map] to start one)
    }
    if (%pug) && (!%full) {
      msg %pug.chan $codes(Status: Gathering players)
    }
    if (%pug) && (%full) {
      msg %pug.chan $codes(Status: In-game on %map.current)
    }
  }
}

on *:PART:%pug.chan:{
  if (!$istok(%players,$nick,32)) {
    halt
  }
  if (%pug) {
    if (%full) {
      halt 
    }
    if ($nick = %pug.starter) {
      if ($numtok(%players,32) = 1) { 
        endpug
        halt
      }
      if ($numtok(%players,32) > 1) {
        %players = $remtok(%players,$nick,32)
        %pug.starter = $gettok(%players,1,32)
        msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(10 - $numtok(%players,32)),/,10,$chr(32),slots remaining,$chr(41)) $+ . %pug.starter is now admin.)
        halt
      }
    }
    else {      
      %players = $remtok(%players,$nick,32)
      msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(10 - $numtok(%players,32)),/,10,$chr(32),slots remaining,$chr(41)))
      halt
    }
  }
}

on *:QUIT:{
  if (!$istok(%players,$nick,32)) {
    halt
  }
  if (%pug) {
    if (%full) {
      halt 
    }
    if ($nick = %pug.starter) {
      if ($numtok(%players,32) = 1) { 
        msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(10 - $numtok(%players,32)),/,10,$chr(32),slots remaining,$chr(41)))
        endpug
        halt
      }
      %players = $remtok(%players,$nick,32)
      %pug.starter = $gettok(%players,1,32)
      msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(10 - $numtok(%players,32)),/,10,$chr(32),slots remaining,$chr(41)))
      msg %pug.chan $codes(%pug.starter is now admin.)
      halt
    }
    else {      
      %players = $remtok(%players,$nick,32)
      msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(10 - $numtok(%players,32)),/,10,$chr(32),slots remaining,$chr(41)))
    }
    halt
  }
}

on *:KICK:%pug.chan:{
  if (!$istok(%players,$knick,32)) {
    halt
  }
  if (%pug) {
    if (%full) || (%busy) {
      halt 
    }
    if ($knick = %pug.starter) {
      if ($numtok(%players,32) = 1) { 
        endpug
        halt
      }
      if ($numtok(%players,32) > 1) {
        %players = $remtok(%players,$knick,32)
        %pug.starter = $gettok(%players,1,32)
        msg %pug.chan $codes($knick has left the pug $+($chr(40),$numtok(%players,32),/,10,$chr(41)) $+ . %pug.starter is now admin.)
        halt
      }
    }
    else {      
      %players = $remtok(%players,$knick,32)
      msg %pug.chan $codes($knick has left the pug $+($chr(40),$calc(10 - $numtok(%players,32)),/,10,$chr(32),slots remaining,$chr(41)))
      halt
    }
  }
}
