on *:start:{
  window -e @rcon
}

on *:INPUT:@rcon:{
  if ($left($1,1) != $chr(47)) { 
    rcon $1-
  }
}

alias warcfg {
  var %x $lines(war.cfg)
  var %y 1
  while (%y <= %x) {
    rcon $read(war.cfg,%y)
    inc %y
  }
}
alias warmupcfg {
  set %x $lines(warmup.cfg)
  set %y 1
  while (%y <= %x) {
    rcon $read(warmup.cfg,%y)
    inc %y
  }
  rcon sv_restart 1
}

alias logoff {
  sockudp -kn iPGNrcon %socket.port %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " logaddress_del %rcon.myip %socket.port
  sockudp -kn iPGNrcon %socket.port %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " logaddress_del %rcon.myip %socket.port
  sockclose iPGNrcon
}

alias rcon.challenge {
  if (!$window(@rcon)) { window -e @rcon }
  sockudp -kn iPGNrcon %socket.port %rcon.ip %rcon.port ����challenge rcon
}

alias rcon.logs {
  if (!$window(@rcon)) { window -e @rcon }
  ; rcon mp_logmessages 1
  ; rcon mp_logfile 1
  ; rcon mp_logdetail 3
  %rcon.myip = $ip
  rcon logaddress_add %rcon.myip %socket.port
}

alias rcon {
  if (!$window(@rcon)) { window -e @rcon }
  sockudp -kn iPGNrcon %socket.port %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " $1-
}



alias logoff {
  rcon log off
  rcon logaddress_del $ip %socket.port
  rcon logaddress_del $ip %socket.port
  sockclose iPGNrcon
}

alias logs {
  set %rcon.myip $ip
  sockudp -kn iPGNrcon %socket.port %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " logaddress_del %rcon.myip %socket.port 
  sockudp -kn iPGNrcon %socket.port %rcon.ip %rcon.port ����rcon %rcon.challengenumber " $+ %rcon.password $+ " logaddress_del %rcon.myip %socket.port 

  sockclose iPGNrcon

  .timer -m 1 200 rcon.challenge %rcon.ip %rcon.port
  .timer -m 1 500 rcon.logs

}

alias scoreupdate {
  if ($regex(%rcon.data,/(Team)(\s)(CT|TERRORIST)(\s)(triggered)(\s)(.*)(\s)(.*)(\s)(\d*)(.*)(\s)(.*)(\s)(\d*)(.*)(.*)/)) {
    if (%match.on = 1) {
      if (%half.1 = 1) {
        rcon say %team.1.name $+ (CT): %CTscore %team.2.name $+ (T): %Tscore
        mnmsg %pug.chan $codes(Progressive 1st-half score:12 %team.1.name $+ (CT): %CTscore 04 %team.2.name $+ (T): %Tscore)
        msg %pug.scorechan $codes(Progressive 1st-half score:12 %team.1.name $+ (CT): %CTscore 04 %team.2.name $+ (T): %Tscore)
        halt
      }
      if (%half.2 = 1) && (!%half.1) {
        rcon say %team.1.name $+ (T): $calc(%Tscore + %CTsh1) %team.2.name $+ (CT): $calc(%CTscore + %Tsh1)
        mnmsg %pug.chan $codes(Progressive 2nd-half score:04 %team.1.name $+ (T): %Tscore 12 $+ %team.2.name $+ (CT): %CTscore $+ . Overall: %team.1.name $+ :12 $calc(%Tscore + %CTsh1) $+  %team.2.name $+ :12 $calc(%CTscore + %Tsh1))
        msg %pug.scorechan $codes(Progressive 2nd-half score:04 %team.1.name $+ (T): %Tscore 12 $+ %team.2.name $+ (CT): %CTscore $+ . Overall: %team.1.name $+ :12 $calc(%Tscore + %CTsh1) $+  %team.2.name $+ :12 $calc(%CTscore + %Tsh1))        
        halt
      }
      if (%othalf.1) && (!%half.2) {
        var %team.1.currenttotal = $calc(%CTsh1 + %Tsh2 + %Tscore), %team.2.currenttotal = $calc(%Tsh1 + %CTsh2 + %CTscore)
        rcon say %team.1.name $+ (T): %team.1.currenttotal %team.2.name $+ (CT): %team.2.currenttotal
        mnmsg %pug.chan $codes(Progressive 1st-half overtime score:04 %team.1.name $+ (T): %Tscore 12 $+ %team.2.name $+ (CT): %CTscore $+ . Overall: %team.1.name $+ :12 %team.1.currenttotal $+  %team.2.name $+ :12 %team.2.currenttotal)
        msg %pug.scorechan $codes(Progressive 1st-half overtime score:04 %team.1.name $+ (T): %Tscore 12 $+ %team.2.name $+ (CT): %CTscore $+ . Overall: %team.1.name $+ :12 %team.1.currenttotal $+  %team.2.name $+ :12 %team.2.currenttotal)
        return
      }
      if (!%othalf.1) && (%othalf.2) {
        var %team.1.currenttotal = $calc(%CTsh1 + %Tsh2 + %Totsh1 + %CTscore), %team.2.currenttotal = $calc(%Tsh1 + %CTsh2 + %CTotsh1 + %Tscore)
        rcon say %team.1.name $+ (CT): %team.1.currenttotal %team.2.name $+ (T): %team.2.currenttotal
        mnmsg %pug.chan $codes(Progressive 2nd-half overtime score:12 %team.1.name $+ (CT): $calc(%Totsh1 + %CTscore) 04 $+ %team.2.name $+ (T): $calc(%CTotsh1 + %Tscore) $+ . Overall: %team.1.name $+ :12 %team.1.currenttotal $+  %team.2.name $+ :12 %team.2.currenttotal)
        msg %pug.scorechan $codes(Progressive 2nd-half overtime score:12 %team.1.name $+ (CT): $calc(%Totsh1 + %CTscore) 04 $+ %team.2.name $+ (T): $calc(%CTotsh1 + %Tscore) $+ . Overall: %team.1.name $+ :12 %team.1.currenttotal $+  %team.2.name $+ :12 %team.2.currenttotal)
        return
      }
    }
  }
}

alias hltv.connect {
  .run rcon6.exe %hltv.ip $+ , $+ %hltv.port $+ , $+ %hltv.rcon $+ ,serverpassword %password ; connect %rcon.ip $+ : $+ %rcon.port
}

alias hltv.record {
  .run rcon6.exe %hltv.ip $+ , $+ %hltv.port $+ , $+ %hltv.rcon $+ ,stoprecording;record CS-Comp- $+ %team.1.name $+ -vs- $+ %team.2.name
}

alias hltv.stop {
  .run rcon6.exe %hltv.ip $+ , $+ %hltv.port $+ , $+ %hltv.rcon $+ ,stoprecording
}

alias sortscores {
  set %rcon.data $strip(%rcon.data)
  if (!%match.on) { 
    halt 
  }
  if ($regex(%rcon.data,/(Team)(\s)(CT|TERRORIST)(\s)(triggered)(\s)(.*)(\s)(.*)(\s)(\d*)(.*)(\s)(.*)(\s)(\d*)(.*)(.*)/)) {
    %CTscore = $regml(11)
    %Tscore = $regml(16)
    if (%half.1) {
      addroundwin $regml(3) 1
    }
    if (%half.2) {
      addroundwin $regml(3) 2
    }
    if (%othalf.1) {
      addroundwin $regml(3) 3
    }
    if (%othalf.2) {
      addroundwin $regml(3) 4
    }
  }
  if (%half.1 = 1) {
    if ($calc(%CTscore + %Tscore) = 15) {
      set %CTsh1 %CTscore
      set %Tsh1 %Tscore

      set %half.2 1
      unset %half.1

      mnmsg %pug.chan $codes(The first half is over.12 %team.1.name $+ (CT): %CTscore 04 $+ %team.2.name $+ (T): %Tscore $+ )
      msg %pug.scorechan $codes(The first half is over.12 %team.1.name $+ (CT): %CTscore 04 $+ %team.2.name $+ (T): %Tscore $+ )

      .copy -o stats.txt stats.1st.txt

      rcon say The first half is over. %team.1.name $+ (CT): %CTscore %team.2.name $+ (T): %Tscore 

      rcon mp_limitteams 6; sv_restartround 1

      unset %match.on %CTscore %Tscore
      halt
    }
    else {
      scoreupdate
    }
  }
  if (%half.2 = 1) {
    if (($calc(%CTscore + %Tsh1) = 15) && ($calc(%Tscore + %CTsh1) = 15)) && (%overtime) {
      set %CTsh2 %CTscore
      set %Tsh2 %Tscore

      set %team.1.total $calc(%CTsh1 + %Tsh2)
      set %team.2.total $calc(%Tsh1 + %CTsh2)

      unset %half.2
      set %othalf.1 1

      .copy -o stats.txt stats.2nd.txt

      rcon say The second half is over. %team.1.name $+ (T): %team.1.total %team.2.name (CT): %team.2.total $+ . Going into overtime
      rcon mp_limitteams 6; sv_restartround 1

      mnmsg %pug.chan $codes(The second half is over.04 %team.1.name $+ (T): %team.1.total 12 $+ %team.2.name $+ (CT): %team.2.total $+ . The game is now going into overtime)
      msg %pug.scorechan $codes(The second half is over.04 %team.1.name $+ (T): %team.1.total 12 $+ %team.2.name $+ (CT): %team.2.total $+ . The game is now going into overtime)

      unset %match.on %CTscore %Tscore
      halt
    }
    elseif ($calc(%CTscore + %Tsh1) = 16) || ($calc(%Tscore + %CTsh1) = 16) || ($calc(%Tscore + %CTscore + %Tsh1 + %CTsh1) = 30) {
      set %CTsh2 %CTscore
      set %Tsh2 %Tscore

      set %team.1.total $calc(%CTsh1 + %Tsh2)
      set %team.2.total $calc(%Tsh1 + %CTsh2)

      rcon say The second half is over. %team.1.name $+ (T): %team.1.total %team.2.name (CT): %team.2.total

      mnmsg %pug.chan $codes(The second half is over.04 %team.1.name $+ (T): %team.1.total 12 $+ %team.2.name $+ (CT): %team.2.total $+ )
      msg %pug.scorechan $codes(The second half is over.04 %team.1.name $+ (T): %team.1.total 12 $+ %team.2.name $+ (CT): %team.2.total $+ )

      unset %match.on
    }
    else {
      scoreupdate
    }
  }
  if (%overtime) && ((%othalf.1) || (%othalf.2)) {
    if (%othalf.1) {
      var %team1.ots = $calc(%CTsh1 + %Tsh2 + %Tscore)
      var %team2.ots = $calc(%Tsh1 + %CTsh2 + %CTscore)
      if ($calc(%Tscore + %CTscore) >= 3) {
        set %CTotsh1 %CTscore
        set %Totsh1 %Tscore

        unset %othalf.1
        set %othalf.2 1

        .copy -o stats.txt stats.ot1st.txt

        rcon say The first half of overtime is over. %team.1.name $+ (T): %Tscore %team.2.name $+ (CT): %CTscore
        rcon say Overall: %team.1.name $+ : %team1.ots %team.2.name $+ : %team2.ots

        rcon mp_limitteams 6; sv_restartround 1

        mnmsg %pug.chan $codes(The first half of overtime is over.04 %team.1.name $+ (T): %Tscore 12 $+ %team.2.name $+ (CT): %CTscore $+ . $&
          Overall: %team.1.name $+ :12 %team1.ots $+  %team.2.name $+ :12 %team2.ots)

        msg %pug.scorechan $codes(The first half of overtime is over.04 %team.1.name $+ (T): %Tscore 12 $+ %team.2.name $+ (CT): %CTscore $+ . $&
          Overall: %team.1.name $+ :12 %team1.ots $+  %team.2.name $+ :12 %team2.ots)

        unset %match.on %CTscore %Tscore
        halt
      }
      else {
        scoreupdate
      }
    }
    if (%othalf.2) {
      var %team1.ots = $calc(%CTsh1 + %Tsh2 + %Totsh1 + %CTscore)
      var %team2.ots = $calc(%Tsh1 + %CTsh2 + %CTotsh1 + %Tscore)
      echo debug: team1 score: %team1.ots team2: %team2.ots
      if ((%team1.ots = 19) || (%team2.ots = 19) || ($calc(%team1.ots + %team2.ots) = 36)) {
        set %CTotsh2 %CTscore
        set %Totsh2 %Tscore

        set %team.1.total %team1.ots
        set %team.2.total %team2.ots

        unset %othalf.2

        var %T.ot.total = $calc(%CTotsh1 + %Totsh2)
        var %CT.ot.total = $calc(%Totsh1 + %CTotsh2)

        rcon say The second half of overtime is over. %team.1.name $+ (CT): %CT.ot.total %team.2.name $+ (T): %T.ot.total
        rcon say Overall: %team.1.name $+ : %team.1.total %team.2.name $+ : %team.2.total

        mnmsg %pug.chan $codes(The second half of overtime is over.12 %team.1.name $+ (CT): %CT.ot.total 04 $+ %team.2.name $+ (T): %T.ot.total)
        mnmsg %pug.chan $codes(Overall scores:12 %team.1.name $+ : %team.1.total 04 $+ %team.2.name $+ : %team.2.total)

        msg %pug.scorechan $codes(The second half of overtime is over.12 %team.1.name $+ (CT): %CT.ot.total 04 $+ %team.2.name $+ (T): %T.ot.total)
        msg %pug.scorechan $codes(Overall scores:12 %team.1.name $+ : %team.1.total 04 $+ %team.2.name $+ : %team.2.total)

        unset %match.on
      }
      else {
        scoreupdate
      }
    }
  }
}
alias renameall {
  var %x = 1
  var %y = $numtok(%connected,32)
  while (%x <= %y) {
    if ($istok(%team.1.match,$gettok(%connected,%x,32),32)) {
      rcon amx_exec " $+ $gettok(%connected,%x,32) $+ " "name %team.1.name $+ . $+ $statname($gettok(%connected,%x,32)) $+ "
    }
    if ($istok(%team.2.match,$gettok(%connected,%x,32),32)) {
      .timer -m 1 500 rcon amx_exec " $+ $gettok(%connected,%x,32) $+ " "name %team.2.name $+ . $+ $statname($gettok(%connected,%x,32)) $+ "
    }
    inc %x
  }
}

alias statscheck {
  var %x = $0
  var %y = 1
  while (%y <= %x) {
    if ($read(stats.txt,w,$gettok($1-,%y,32) $+ $chr(37) $+ *)) {
      ; done
    }
    else {
      write stats.txt $+($gettok($1-,%y,32),$str($chr(37) $+ 0, $calc($numtok(%allstats,37) - 2)),$chr(37),0.0.0.0)
    }
    inc %y
  }
}

alias gamereplace {
  ;gamereplace name.of.person.being.replaced name.of.person.replacing

  ;%replace.left is now a list of players are being replaced - for a couple of reasons
  ;sometimes a player would join while there is another replacement going on and replace.left would then be unset
  ;replace.closest would then be trying to replace using an empty var, fucking shit up
  ;therefore, we need to have replace.left as a list of statnames and work from that so shit doesn't go wrong and so everything works a bit better

  var %replace.name = $1
  var %id = $steamid(%replace.name)
  var %id2 = $steamid($2)
  if ($istok(%team.1.names,%replace.name,32)) {
    %team.1.match = $reptok(%team.1.match,%id,%id2,1,32)
    %team.1.names = $reptok(%team.1.names,%replace.name,$2,1,32)
    %team.1.irc = $reptok(%team.1.irc,12 $+ %replace.name ,12 $+ $2 ,1,32)
  }
  if ($istok(%team.2.names,%replace.name,32)) {
    %team.2.match = $reptok(%team.2.match,%id,%id2,1,32)
    %team.2.names = $reptok(%team.2.names,%replace.name,$2,1,32)
    %team.2.irc = $reptok(%team.2.irc,12 $+ %replace.name ,12 $+ $2 ,1,32)
  }
  if ($istok(%playerlist,%id,32)) {
    %playerlist = $reptok(%playerlist,%id,%id2,1,32)
  }
  if ($istok(%players,$authname(%replace.name),32)) {
    %players = $reptok(%players,$authname(%replace.name),$authname($2),1,32)
  }
  rcon say $2 replaced %replace.name $+ .
  rcon mp_freezetime 10
  if ($read(replace.txt,w,* $+ %replace.name $+ *)) {
    var %z = $read(replace.txt,$readn)
    write -l $+ $readn replace.txt $reptok($read(replace.txt,1),$gettok(%z,2,32),$2,1,32)
    write -dw $+ $steamid($gettok(%z,2,32)) $+ $chr(37) $+ * stats.txt
  }
  else {
    write replace.txt %replace.name $2
  }
  if (!$read(stats.txt,w,%id2 $+ $chr(37) $+ *)) {
    write stats.txt $+(%id2,$str($chr(37) $+ 0, $calc($numtok(%allstats,37) - 2)),$chr(37),0.0.0.0)
  }
  setids
  %replace.left = $remtok(%replace.left,%replace.name,32)
  %replacer.list = $remtok(%replacer.list,$2,32)
}

alias lo3 {
  if (%lo3) { halt }
  if ($numtok(%connected,32) != $calc(%gametype * 2)) {
    rcon say *** Incorrect number of players connected ***
    rcon sv_restart 1
    halt
  }
  set -u8 %lo3 1
  rcon exec war.cfg
  unset %match.on
  renameall
  if ((%team.1.total = 19) || (%team.2.total = 19) || ($calc(%team.1.total + %team.2.total) = 36)) {
    rcon say The game is over, you can't !lo3 again
    return
  }
  if (!%half.1) && (!%half.2) && (!%othalf.1) && (!%othalf.2) {
    set %half.1 1
  }
  if (%half.1 = 1) { 
    hltv.record
    write -c stats.txt
    write -c replace.txt
    ;    statscheck %connected
    unset %pattern.1
    unset %score.*
    mnmsg %pug.chan $codes(The first half of the match is starting)
    rcon mp_startmoney 800
  }
  if (%half.2 = 1) {
    unset %pattern.2
    copy -o stats.1st.txt stats.txt
    ;    statscheck %connected
    unset %score.*
    mnmsg %pug.chan $codes(The second half of the match is starting) 
    rcon mp_startmoney 800
  }
  if (%othalf.1) {
    unset %pattern.ot1
    .copy -o stats.2nd.txt stats.txt
    unset %score.*
    mnmsg %pug.chan $codes(The first half of overtime is starting)
    rcon mp_startmoney 10000
  }
  if (%othalf.2) {
    unset %pattern.ot2
    .copy -o stats.ot1st.txt stats.txt
    unset %score.*
    mnmsg %pug.chan $codes(The second half of overtime is starting)
    rcon mp_startmoney 10000
  }
  rcon exec lo3cs-comp.cfg
}

on 1:DNS:{
  if ($dns(0) == 0) { return }
  else {
    var %n = $dns(0)
    while (%n > 0) {
      write $shortfn(%dir $+ dns.txt) $dns(%n).ip $dns(%n).addr

      dec %n
    }
  }
}


on *:udpread:iPGNrcon:{
  if ($sockerr > 0) {
    return
  }
  :nextread
  sockread -f %rcon.data
  if ($sockbr == 0) {
    return
  }
  if (%rcon.data == $null) {
    goto nextread
  }
  else {
    if (*" from "*"* iswm %rcon.data) {
      %rcon.data = $null
    }
    if (*"* iswm %rcon.data) {
      %rcon.data = $remove(%rcon.data,")
    }
    if ($gettok(%rcon.data,1,32) == ����challenge) {
      set %rcon.challengenumber $gettok(%rcon.data,3,32)
    }
    if ($left(%rcon.data,7) == ����log) {
      %rcon.data = $remove(%rcon.data,$left(%rcon.data,32))
      if (%rcon.data == $null) {
        goto nextread
      }
    }
    if (*No*challenge*for*your*address* iswm %rcon.data) {
      logs
    }
    if ($regex(%rcon.data,/name userid uniqueid frag time ping loss adr/)) {
      unset %connected
    }
    if ($regex(%rcon.data,/(#)(\s*)(\d+)(\s*)(.*)(\s+)(\d+)(\s+)((STEAM_0:)(\S*))(\s+)(.*)(\s+)((\S+))(\s+)(\d+)(\s+)(\d+)(\s+)((\S+)(:)(\d+))/)) {
      addip $regml(9) $regml(23)
      if ($istok(%playerlist,$regml(9),32)) { 
        %connected = $addtok(%connected,$regml(9),32))
      } 
      else {
        if (!$istok(%pes.admins,$regml(9),59)) {
          rcon kick $chr(35) $+ $regml(7)  "Not in playerlist"
          rcon say *** Kicked $regml(5) ( $+ $statname($regml(9)) $+ ) as they were not in the player list ***
        }
      }
    }
    if ($regex(%rcon.data,^(\d+) users$)) {
      if ($numtok(%connected,32) = $calc(2 * %gametype)) {
        renameall
      }
      if ($numtok(%connected,32) = $regml(1)) { 
        hltv.connect
      }
    }
    if ($left(%rcon.data,5) == ����l) {
      %rcon.data = $remove(%rcon.data,$left(%rcon.data,5))
      if (%rcon.data == $null) {
        goto nextread
      }
    }
    if (*Rcon:* iswm %rcon.data) {
      %rcon.data = $null
    }
    if (Bad Rcon: rcon* iswm %rcon.data) {
      %rcon.data = $null     
    }
    if ($pos(%rcon.data,$date(yyyy),1) = 9) {
      %rcon.data = $remove(%rcon.data,$left(%rcon.data,24))
    }
    if ($regex(%rcon.data,/\d+\x2E\d+(.+)\d+\x2E\d+(.+)\d+\x2E\d+(.+)\d+(.+)\d+(.+)\d+\x2E\d+(.+)\d+$/)) {
      mnmsg %pug.adminchan $codes(Server uptime: $gettok(%rcon.data,4,32))
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!lo3(?-i))/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        rcon status
        .timer 1 1 lo3
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!teams(?-i))/)) {
      rcon say %team.1.name $+ $iif(%half.2,(T),(CT)) $+ : %team.1.names
      rcon say %team.2.name $+ $iif(%half.2,(CT),(T)) $+ : %team.2.names
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!password(?-i))(\s)(.*)/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        mnmsg %pug.chan $codes(Server password changed)
        rcon say Password changed to: $gettok($regml(14),1,32)
        rcon sv_password $gettok($regml(14),1,32)    
        set %password $gettok($regml(14),1,32)
        .timer 1 1 rcon sv_password
      }
    }
    if ($regex(%rcon.data,/(\s*)(.*)(<)(\d*)(>)(<)(.*)(>)(<)(TERRORIST)(>)(\s)(triggered Spawned_With_The_Bomb)/)) {
      %bomb.spawn = $regml(2)
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!rs(?-i))/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        rcon sv_restartround 1
        rcon mp_startmoney 16000
        rcon mp_limitteams 6
        rcon mp_freezetime 0
        rcon mp_friendlyfire 0
        unset %match.on
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!rates(?-i))/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        rcon rates_list
        rcon say *** Listing rates in console ***
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!names(?-i))/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        renameall
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!missing(?-i))/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        missing
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!ft(?-i))(\s)(\d+)(.*)/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        rcon mp_freezetime $regml(14)
        rcon say *** mp_freezetime $regml(14) ***
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!kick(?-i))(.*)/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        rcon kick $regml(13)
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!cr(?-i))(.*)/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        tokenize 32 $regml(13)
        echo -s $1-
        if ($istok(%replace.left,$1,32)) {
          cancelreplace $1
        }
        else {
          rcon say Specify who you're cancelling for
        }
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)\s(say_team|say)\s((?i)!replace(?-i))(.*)/)) {
      if ($istok(%pes.admins,$regml(6),59)) {
        if (!%half.2) && (!%match.on) && (!%ctsh1) { 
          rcon say *** Please use !replace in IRC prior to lo3 ***
          return
        }
        elseif ($calc(%CTscore + %Tscore) < 4) && (!%ctsh1) {
          rcon say *** In-game !replace is disabled in the first 4 rounds ***
          return
        }
        else {
          tokenize 32 $regml(13)
          if ($istok(%playerlist,$steamid($1),32)) {
            replace.in $1 $remove($2-,(dead))
            rcon say Replacement requested for $1 $+ .
          }
        }
      }
    }
    ;"bladez<107><STEAM_0:1:10325827><>" connected, address "124.170.23.34:27005"
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(><>)(.*)(address)(.*)(:)(.*)/)) {
      if ($regml(6) = HLTV) { 
        goto nextread
      }
      if (STEAM_*:*:* !iswm $regml(6)) {
        rcon kick $chr(35) $+ $regml(3) "Bad SteamID, please retry"
        goto nextread
      }
      if ($regml(3) == $null) || ($regml(6) == $null) || ($regml(10) == $null) { goto nextread }
      if (!$read(%dir $+ dns.txt,w,$remove($regml(10),$chr(32)) $+ *)) {
        dns $remove($regml(10),")
        sleep 1
      }
      %rcon.data = 3 $+ $regml(1) 14(# $+ $regml(3) $+ : $regml(6) @ $regml(10) $+ ) 3has connected

      var %addr = $iif($read(%dir $+ dns.txt,w,$remove($regml(10),$chr(32)) $+ *),$gettok($ifmatch,2,32),not found)
      if (%addr = not found) { .timer 1 600 dns $regml(10) }
      write %dir $+ history/ $+ $date(dd-mm-yy) $+ .txt $date(dd-mm-yy hh:nntt) $strip($statname($regml(6)) $chr(40) $+ $regml(6) @ $regml(10) - %addr $+ $chr(41) connected.)
      mnmsg %pug.adminchan $codes($statname($regml(6)) $chr(40) $+ $regml(6) @ $regml(10) - %addr $+ $chr(41) connected.)
      if (%pug) {
        if (($istok(%pes.admins,$regml(6),59)) && (!$istok(%playerlist,$regml(6),32)) && (!%replace.left)) {
          msg #ipgn-board $codes($statname($regml(6)) ( $+ $regml(6) $+ ) connected to the server without being in the playerlist)
          goto nextread
        }

        if (!$istok(%playerlist,$regml(6),32)) && (!%replace.left) {
          rcon kick $chr(35) $+ $regml(3) "Not in playerlist"
          rcon banid 2 $regml(6) kick
          mnmsg %pug.adminchan $codes($statname($regml(6)) $chr(40) $+ $regml(6) $+ $chr(41) was kicked because they're not in the playerlist)
          goto nextread
        }

        ;writes currently connected players for late checking
        ;write pc.txt $statname($regml(6))  

        if (%replace.left) && (!$istok(%playerlist,$regml(6),32)) {
          if ($istok(%replace.left,$statname($regml(6)),32)) {
            rcon kick $chr(35) $+ $regml(3) "You have been replaced"
            goto nextread
          }
          var %replacer.statname = $statname($regml(6))
          if ($istok(%replacer.list,%replacer.statname,32)) {
            gamereplace $gettok(%replace.left,$findtok(%replacer.list,%replacer.statname,1,32),32) %replacer.statname
          }
          else {
            rcon kick $chr(35) $+ $regml(3) "Not in playerlist"
            rcon banid 2 $regml(6) kick
          }
        }
        rcon say $regml(1) ( $+ $statname($regml(6)) $+ ) connected.
        %connected = $addtok(%connected,$regml(6),32)
        if (%match.on) { 
          rcon mp_freezetime 10 
          rcon say *** mp_freezetime 10 ***
        }
        rcon status
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)(disconnected)/)) {
      %rcon.data = 3 $+ $regml(1) 14( $+ $regml(6) $+ )3 disconnected
      if ($regml(6) = HLTV) { halt }
      rcon say $regml(1) ( $+ $statname($regml(6)) $+ ) disconnected.
      if (%match.on) {
        rcon mp_freezetime 60
        rcon say *** mp_freezetime 60 ***
      }
      rcon status
    }
    if (*entered the game* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(>)(.*)(entered the game)/)) {
        %rcon.data = 3 $+ $regml(1) 14(# $+ $regml(3) $+ : $regml(6) $+ )3 has entered the game
      }
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(.*)(>)(.*)(joined team)(\s)(.*)/)) {
      if ($regml(14) = TERRORIST) {
        %rcon.data = 3 $+ $regml(1) 14joined team4 $regml(14)
      }
      if ($regml(14) = CT) {
        %rcon.data = 3 $+ $regml(1) 14joined team12 $regml(14)
      }
      if ($regml(14) = SPECTATOR) {
        %rcon.data = 3 $+ $regml(1) 14joined team10 $regml(14)
        write -dw $+ $regml(6) $+ * stats.txt
      }
    }

    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(TERRORIST)(>)(\s)(triggered Planted_The_Bomb)/)) {
      addplant $regml(6)
    }
    if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT)(>)(\s)(triggered Defused_The_Bomb)/)) {
      adddefuse $regml(6)
    }
    if ($regex(%rcon.data,/(])(.*)(<)(\d*)(>)(<)(\S*)(>)(<)(>)(\s)(has inconsistent file:)(.*)/)) {
      rcon say custom file disallowed: $regml(13)
      mnmsg %pug.chan $codes(Bad file: $regml(2) ( $+ $regml(7) $+ ) has been kicked for use of custom file: $regml(13))
    }
    if (*attacked* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(><)(.*)(><)(TERRORIST)(> attacked)(\s)(.*)(<)(\d*)(><)(.*)(><)(CT)(>)( with )(.*)(damage )(\d*)(\S)(.*)(health)(\s)(-?)(\d*)(\S)/)) {
        if ($regml(26)) {
          adddamage $regml(5) $calc($regml(21) - $regml(27))
          adddamager $regml(14) $calc($regml(21) - $regml(27))
        }
        if (!$regml(26)) {
          adddamage $regml(5) $regml(21)
          adddamager $regml(14) $regml(21) 
        }
      }
      if ($regex(%rcon.data,/(.*)(<)(\d*)(><)(.*)(><)(CT)(> attacked)(\s)(.*)(<)(\d*)(><)(.*)(><)(TERRORIST)(>)( with )(.*)(damage )(\d*)(\S)(.*)(health)(\s)(-?)(\d*)(\S)/)) {
        if ($regml(26)) {
          adddamage $regml(5) $calc($regml(21) - $regml(27))
          adddamager $regml(14) $calc($regml(21) - $regml(27))
        }
        if (!$regml(26)) {
          adddamage $regml(5) $regml(21)
          adddamager $regml(14) $regml(21) 
        }
      }
      %rcon.data = $null
    }
    if (*killed* iswm %rcon.data) {
      if ($regex(kills,%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(>)(.*)(killed)(\s)(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(> with )(.*)/)) {
        if ($regml(kills,9) = CT) {
          if ($regml(kills,22) = CT) {
            addtk $regml(kills,6) $regml(kills,19)
            adddeath $regml(kills,19) CT
            dec %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 12 $+ $regml(kills,1) $score($regml(kills,3)) teamkilled12 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)
          }
          else {
            inc %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 12 $+ $regml(kills,1) $score($regml(kills,3)) killed4 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)
            adddeath $regml(kills,19) CT
            addkill $regml(kills,6) CT $regml(kills,19)
            addweapon $regml(kills,6) $regml(kills,24)        
          }      
        }
        if ($regml(kills,9) = TERRORIST) {
          if ($regml(kills,22) = TERRORIST) {
            addtk $regml(kills,6) $regml(kills,19)
            adddeath $regml(kills,19) T
            dec %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 4 $+ $regml(kills,1) $score($regml(kills,3)) teamkilled4 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)

          }
          else {
            inc %score. [ $+ [ $regml(kills,3) $+ .kills ] ]
            inc %score. [ $+ [ $regml(kills,16) $+ .deaths ] ]
            %rcon.data = 4 $+ $regml(kills,1) $score($regml(kills,3)) killed12 $regml(kills,14) $score($regml(kills,16)) with10 $regml(kills,24)
            adddeath $regml(kills,19) T
            addkill $regml(kills,6) T $regml(kills,19)
            addweapon $regml(kills,6) $regml(kills,24)        
          }
        }
        if (%match.on) {
          msg %pug.scorechan $codes(%rcon.data)
        }
      }
    }
    if (*committed suicide* iswm %rcon.data) {
      if ($regex(%rcon.data,/(.*)(<)(\d*)(>)(<)(.*)(>)(<)(CT|TERRORIST)(>)(\s)(committed suicide with)(\s)(.*)/)) {
        addsuicide $regml(6)
        adddeath $regml(6) $iif($regml(9) = CT,CT,T)
        if ($regml(14) = worldspawn (world)) {
          inc %score. [ $+ [ $regml(3) $+ .deaths ] ]
          if ($regml(9) = CT) {
            %rcon.data = 12 $+ $regml(1) $score($regml(3)) killed self with 10worldspawn
          }
          if ($regml(9) = TERRORIST) {
            %rcon.data = 4 $+ $regml(1) $score($regml(3)) killed self with 10worldspawn
          }
          if (%match.on) {          
            msg %pug.scorechan $codes(%rcon.data)
          }
        }
        if ($regml(14) = grenade) {
          dec %score. [ $+ [ $regml(3) $+ .kills ] ]
          inc %score. [ $+ [ $regml(3) $+ .deaths ] ]
          if ($regml(9) = CT) {
            %rcon.data = 12 $+ $regml(1) $score($regml(3)) killed self with 10grenade
          }
          if ($regml(9) = TERRORIST) {
            %rcon.data = 4 $+ $regml(1) $score($regml(3)) killed self with 10grenade
          }
          if (%match.on) {          
            msg %pug.scorechan $codes(%rcon.data)
          }
        }
        if ($regml(14) = world) {
          dec %score. [ $+ [ $regml(3) $+ .kills ] ]
          inc %score. [ $+ [ $regml(3) $+ .deaths ] ]
          if ($regml(9) = CT) {
            %rcon.data = 12 $+ $regml(1) $score($regml(3)) killed self with 10world
          }
          if ($regml(9) = TERRORIST) {
            %rcon.data = 4 $+ $regml(1) $score($regml(3)) killed self with 10world
          }
        }
      }
    }
    if (*Team CT triggered* iswm %rcon.data) || (*Team TERRORIST triggered* iswm %rcon.data) {
      if (*Team TERRORIST Triggered Target_Bombed* iswm %rcon.data) {
        set %score. [ $+ [ %bomb.planter $+ .kills ] ] $iif(%score. [ $+ [ %bomb.planter $+ .kills ] ],$calc($ifmatch + 3),3)
        unset %bomb.planter
      }
      sortscores
    }
    if (*World triggered Round_Start* iswm %rcon.data) {
      rcon status
      if (%match.on) || (%lo3) {
        unset %kills.* alive.* %clutch
        set %alive.1 %team.1.match
        set %alive.2 %team.2.match
      }
    }
    if (*Server say * Live! GLHF! * iswm %rcon.data) {
      if (%pug) && (%team.1.match) {
        set %match.on 1
      }
    }
    if (*Server say custom file* iswm %rcon.data) {
      if ($regex(%rcon.data,/Server say custom file: .* <(.*)>/)) {
        rcon banid 1 $regml(1) kick
        %connected = $remtok(%connected,$regml(1),32)
        mnmsg %pug.adminchan $codes($regml(1) kicked and banned for 1 minute for having a custom file)
      }
    }
    if (%rcon.data) { echo @rcon $asctime(hh:nn:ss) $+ : %rcon.data }
    goto nextread
  }
}

alias missing {
  var %x = %playerlist
  var %z = 1
  while (%z <= $numtok(%connected,32)) {
    %x = $remtok(%x,$gettok(%connected,%z,32),32)
    inc %z
  }
  var %y = 1
  while (%y <= $numtok(%x,32)) {
    %x = $puttok(%x,$statname($gettok(%x,%y,32)),%y,32)
    inc %y
  }
  if ($numtok(%x,32) > 0) {
    mnmsg %pug.chan $codes(Players missing: %x - HURRY UP)
  }
  rcon say *** Players missing: $iif(%x,$ifmatch,None) ***

}

alias score {
  if (%score. [ $+ [ $1 $+ .kills ] ] = $null) {
    set %score. [ $+ [ $1 $+ .kills ] ] 0
  }
  if (%score. [ $+ [ $1 $+ .deaths ] ] = $null) {
    set %score. [ $+ [ $1 $+ .deaths ] ] 0
  }
  return $chr(91) $+ %score. [ $+ [ $1 $+ .kills ] ] $+ / $+ %score. [ $+ [ $1 $+ .deaths ] ] $+ $chr(93)
}


on *:sockwrite:rcon:{
  if ($sockerr) {
    aline @rcon $sock().wserrf
  }
}

on *:close:@rcon:{
  if (%rcon.scorebot == 1) {
    set %rcon.scorebot 0
  }
  set %rcon.myip $ip
}
