alias banid {
  if ($read(%dir $+ batch/banned.cfg, w, * $+ $2)) {
    write -l $+ $readn %dir $+ batch/banned.cfg banid $1-
    mnmsg $chan $codes(Ban found: $read(%dir $+ batch/banned.cfg,$readn) $+ . Ban files updated.)
  }
  else {
    write %dir $+ batch/banned.cfg banid $1-
  }
  copy -o %dir $+ batch/banned.cfg %dir $+ batch/banned_user.cfg
  run %dir $+ batch/bancfg.bat
  mnmsg $chan $codes(Banned.cfg updated for $2 across all servers.)
  .timer 1 10 execban ban $2
}

alias remid {
  if ($read(%dir $+ batch/banned.cfg, w, * $+ $1)) {
    mnmsg $chan $codes(Ban removed: $read(%dir $+ batch/banned.cfg,$readn) $+ . Ban files updated.)
    mnmsg $chan $codes(Banned.cfg updated for $1 across all servers.)
    write -dl $+ $readn %dir $+ batch/banned.cfg
  }
  else {
    mnmsg $chan %ipgntagl Ban not found. %ipgntagr
  }
  copy -o %dir $+ batch/banned.cfg %dir $+ batch/banned_user.cfg
  run %dir $+ batch/bancfg.bat
  .timer 1 10 execban unban $1
}

alias execban {
  var %y = $lines(serverlist.txt)
  var %x = 1
  var %ban.mode = $1
  var %banid = $2
  while (%x <= %y) {
    tokenize 32 $read(serverlist.txt,%x)
    if (%ban.mode = ban) {
      .timer 1 $calc(%x * 4) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),banid 0 %banid kick,$chr(44),writeid)
    }
    if (%ban.mode = unban) {
      .timer 1 $calc(%x * 4) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),removeid %banid,$chr(44),writeid)
    }
    inc %x
  }
  var %w = $lines(cssserverlist.txt), %v = 1
  while (%v <= %w) {
    tokenize 32 $read(cssserverlist.txt,%v)
    if (%ban.mode = ban) {
      ;.timer 1 $calc(%b * 2) .run C:\TF2\SourceRcon.exe $1 $2 $3 " $+ banid 0 %banid kick $+ ; writeid $+ "
    }
    if (%ban.mode = unban) {
      ;.timer 1 $calc(%b * 2) .run C:\TF2\SourceRcon.exe $1 $2 $3 " $+ removeid %banid $+ ; writeid $+ "
    }
    inc %v
  }
}

alias massrs {
  var %y = $lines(serverlist.txt)
  var %x = 1
  mnmsg %pug.adminchan $codes(Running mass restart script)
  while (%x <= %y) {
    tokenize 32 $read(serverlist.txt,%x)
    .timer 1 $calc(%x * 30) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),say Scheduled restart in 3 seconds)
    .timer 1 $calc(%x * 30 + 3) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),quit)
    .timer 1 $calc(%x * 30) echo -s Restarting $4-
    inc %x
  }
}

alias massmsg {
  var %y = $lines(serverlist.txt)
  var %x = 1
  while (%x <= %y) {
    tokenize 32 $read(serverlist.txt,%x)
    .timer 1 $calc(%x * 30) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),say Check out iPGN's new site at www.ipgn.com.au)
    .timer 1 $calc((%x * 30) + 1800) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),say You can join us on #iPGN on irc.gamesurge.net)
    inc %x
  }
}
