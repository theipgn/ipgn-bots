alias addUnityUser {
  ; addUnityUser auth steamid name

  if (!$1) || (!$2) || (!$3) {
    return
  }

  var %sid = $2
  var %auth = $1

  ; we need to register this user, as there's no corresponding auth
  var %name = $regsubex($remove($3,[A]),[^A-Za-z0-9],$null)

  ; check if this name is already a statname or auth name. if it is, just append a 1
  ; until it's no longer in use
  while ($steamid(%name)) {
    %name = %name $+ 1
  }

  ; name is definitely not taken, now make sure it's not all numbers
  if (%name isnum) {
    %name = %name $+ a
  }

  ; directly add the user, don't worry about accepting/rejecting

  echo -s UNITY USER ADD - AUTH: %auth STEAMID: %sid STATNAME: %name

  ; auth, steamid, statname
  adduser %auth %sid %name

  ; remove them from the steamid cache because they are now registered
  hdel steamid.cache. $+ $cid $3
}

; separate chat hook for relaying, so all the commands aren't parsed
on *:TEXT:*:%pug.chan:{
  ; relay this message to all other connected networks
  if (!* !iswm $1) {
    mnmsg_ex $cid $chan $codes(( $+ $network $+ ) <12 $+ $nick $+ > $1-)
  }
}

on *:TEXT:!*:?:{
  if ($1 = !stat) {
    if ($2) {
      stat_ex $auth($2) $nick
    }
    else {
      stat_ex $auth($nick) $nick
    }
  }
  window -c $nick
}
