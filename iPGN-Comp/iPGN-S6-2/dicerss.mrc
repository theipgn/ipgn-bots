;
; Author:        FiberOPtics -- mirc.fiberoptics@gmail.com
;
; Usage:         /rss <-fhi> <url> <file|hash table>   or  $rss(-fhi, url, file|hash table)
;
; Purpose:    -  Reads the information that is stored in the <item> tags in RSS feeds.
;
;             -  If the alias is called as an identifier it returns $true if data was retrieved,
;                else it returns $false. If the alias is called as a command, it will echo if data
;                was retrieved or not, but only if you didn't silence the alias with a dot like /.rss
;
; Parameters:    Switches:
;
;         -f --> Means that you specified an export file where the retrieved data will be stored.
;                The exportfile should either be a full filepath like c:\program files\my files\file.txt
;                or a single file like file.txt. In the latter case, this file will be saved in your
;                main mIRC folder. You don't need to specify the quotes " " for filepaths with spaces,
;                as the snippet already takes care of this.
;
;                Structure -> title: ...$chr(127)link= ...$chr(127)description=...
;
;                In other words, the title, link, and description tags are written on one single line,
;                delimited by $chr(127) which is a backspace character. You can parse this then with
;                $gettok like $gettok($read(<file>,N),M,127) etc. In this format, only these 3 mentioned
;                tags are written.
;                When saving the data in this form, only the first 920 characters are written on a single line,
;                to prevent you from errors in mIRC regarding "string too long", and to prevent from data being
;                on multiple lines. Therefore, when saving in non-ini form, each line will always be a different
;                news item.
;
;         -i --> Means that the data should be stored to the exportfile in INI format with
;                the topic being [#<number], the items being the tags of the news item,
;                and the values being the corresponding data with each tag. If you specify the
;                i flag, then you should not specify the f flag, as the script will automatically
;                assume it's a file you're writing to.
;
;                [#number]
;                title= ...
;                link= ...
;                description= ...
;                pubdate= ...
;
;         -h --> Means that you specified a hash table where the data will be loaded in.
;                The data is structured in the same way like it is when specifying a file
;                with the f flag. The hash table item is the number, and the data is the one
;                single line delimited by $chr(127). Only title, link, and description are stored.
;                If the hash table already exists, it is freed, else it is created, before loading
;                the data into it. Data is cut at 920 characters. Looping is easy like this:
;
;                //var %i = 1 | while ($hget(<name>,%i)) { ... | inc %i }
;
; Notes:      -  During the download (which shouldn't take longer than a second or two depending on your
;                connection) mIRC will be paused on the current thread where you called /rss or $rss.
;                This means that any code after the call of the rss alias, will only be executed when
;                all the data has been retrieved. Note that mIRC is not frozen in the mean time though.
;
;                Suppose I have the following code in my mIRC:
;
;                alias msnnews {
;                   var %url = http://rss.msnbc.msn.com/id/3032091/device/rss/rss.xml
;                   if ($rss(i,%url,msn.ini)) run msn.ini
;                   else echo -a MSN News: an error has occured.
;                }
;
;                The /run or /echo commands in the above code, will only be called once the data has been
;                fully retrieved from the RSS Feed. This let's you put all code in one small alias, whereas
;                with sockets you have to deal with multiple events. If you want to use this alias from
;                an event like on *:text:!rss &:#mychan:, then you should never directly call the rss
;                snippet, because as said it will pause the current thread. Instead use the simple solution
;                of putting the command in a timer like this:
;
;                on *:text:!rss &:#news: set %rss.url $2 | .timer 1 0 somealias
;                alias somealias ...
;
;                Note: Don't pass $2 to the timer, because it will be evaluated twice, once
;                      when calling the .timer command, and once when the timer triggers.
;                      People who know you are hosting a script like this could exploit you
;                      by saying something like: !rss <identifier that does something bad to your pc>
;                      This is not related to the snippet, but I thought I should mention it anyway.
;
;                You can also use a signal event, thus not worrying about /timer evaluating twice.
;
;                on *:text:!rss &:#news: signal getnews $2
;                on *:signal:getnews: ...
;
;                Don't forget to put the url in a variable if it contains commas.
;
; Examples:      /rss -i http://online.wsj.com/xml/rss/0,,3_7011,00.xml wallstreet.ini
;                /rss -f http://www.foxnews.com/xmlfeed/rss/0,4313,1,00.rss foxnews.txt
;                /rss -f http://rss.cnn.com/rss/edition.rss cnn.txt
;                /rss -h http://www.iht.com/rss/frontpage.xml iht
;                /rss -h http://newsrss.bbc.co.uk/rss/newsonline_world_edition/front_page/rss.xml bbc
;                /rss -h http://www.nytimes.com/services/xml/rss/nyt/HomePage.xml nytimes
;                /rss -i http://www.guardian.co.uk/rssfeed/0,,1,00.xml guardian.ini
;                /rss -f http://www.washingtontimes.com/feeds/rss/feedhome/twt_world.rss wshtimes.txt
;                /rss -i http://news.ft.com/rss/home/europe fntimes.ini
;                /rss -f http://rss.msnbc.msn.com/id/3032091/device/rss/rss.xml msn.txt
;                /rss -f http://www.spiegel.de/schlagzeilen/rss/0,5291,,00.xml spiegel.txt
;                /rss -h http://www.heise.de/newsticker/heise.rdf heise
;                /rss -i http://rss.slashdot.org/Slashdot/slashdot slash.ini
;                /rss -h http://www.theinquirer.net/inquirer.rss inquirer
;                /rss -i http://thesuperficial.com/index.xm superficial.ini
;                /rss -i http://www.GamersDatabase.com/rss.php gamers.ini
;                /rss -h http://www.mirc.net/rss.php?type=news mirc.net
;                 ...
;
;                All of the above RSS feeds were tested at the time of release and were all working.
;                Of course, if these sites decide to change the location of their RSS feeds, then
;                obviously some of the examples won't work anymore.
;
; Notes:         You should be able to use this snippet on any rss feed, as long as it
;                has the <item> tags.
;
; Requirements:  mIRC 5.91
;                Internetexplorer 5 or higher
;                Windows ME or higher
;
; Warning:       This snippet has been modified from its original version, which used to be implemented
;                solely with the use of COM objects. However the current version uses VBScript because
;                of unstable COM support in the current mIRC version (6.16). With each run of this alias,
;                a .vbs file is being executed. Advantage of this is that we have this excellent feature
;                where calling /rss or $rss will pause the current thread, but a downside is that
;                some programs like MS Anti-spyware will complain about the execution of a scripting
;                file like a .vbs (visualbasicscript) file. There is nothing I can do about this.
;
; FYI:           This snippet does what it's supposed to do: it retrieves data from an RSS feed,
;                and stores it in a file or hash table, in a certain structure.
;                It is not meant to be some kind of RSS reader/displayer, I leave that to other coders
;                who wish to incorporate my snippet into a bigger addon.
;


alias rss {
  var %e = !echo $color(info) -a * Rss:, %r = return $false
  if ($os isin 9598) { %e this snippet requires Windows ME or higher | %r }
  if ($version < 5.91) { %e this snippet requires atleast mIRC 5.91 or higher | %r }
  if (!$regex(%e,$1-,/^-[fhi] \S+ \S+$/i)) {
    %e Syntax: /rss <-fhi> <url> <file|htb> or $!rss(-fhi, url, file|htb) | %r
  }
  var %flag = $mid($1,2), %filepath
  if (%flag == h) %filepath = " $+ $mircdirtmprss"
  else {
    var %dir = $nofile($3-), %file = $nopath($3-)
    if (%file != $mkfn(%file)) { %e file %file contains illegal characters. | %r }
    if (* !iswm %dir) %dir = $mircdir
    elseif (!$isdir(%dir)) { %e no such folder %dir | %r }
    %filepath = $+(",%dir,%file,")
  }
  var %w = $+(@rss,$ticks,$r(1111,9999),.vbs), %a = aline %w, %size, %n = $lf
  write -c %filepath
  window -h %w
  %a Function nohtml(byref string)
  %a set regex = new regexp
  %a regex.global = true : regex.pattern = "<[^>]*>|[\r\n\177]+" : nohtml = regex.replace(string,"")
  %a set regex = nothing
  %a End Function
  %a set xml = createobject("msxml.domdocument") : set ado = createobject("adodb.stream")
  %a ado.open : ado.type = 2 : ado.charset = "ascii"
  %a xml.validateonparse = false : xml.async = false : xml.load " $+ $2"
  %a for each item in xml.getelementsbytagname("item")
  if (%flag == i) {
    %a i = i + 1
    %a ado.writetext "[#" & i & "]",1
    %a for each node in item.childnodes
    %a if node.text <> "" then ado.writetext node.nodename & "=" & nohtml(node.text),1
    %a next %n ado.writetext vbcrlf
  }
  else {
    %a for each node in item.childnodes
    %a name = node.nodename
    %a if name = "description" then %n desc = name & ": " & nohtml(node.text)
    %a elseif name = "link" or name = "title" then %n tmp = tmp & name & ": " & nohtml(node.text) & chr(127)
    %a end if %n next
    if (%flag == h) %a i = i + 1 %n ado.writetext i & chr(10) & left(tmp & desc,920) & chr(10)
    else %a ado.writetext left(tmp & desc,920) & vbcrlf
    %a tmp = null
  }
  %a next %n ado.savetofile %filepath ,2 : ado.close %n set ado = nothing : set xml = nothing
  savebuf %w %w | close -@ %w
  .comopen %w wscript.shell
  if (!$comerr) .comclose %w $com(%w,run,1,bstr*,%w,uint,0,bool,true)
  .remove %w
  if (%flag == h) {
    if ($hget($3)) hfree $3
    hmake $3
    hload $3 %filepath
    .remove %filepath
    %size = $hget($3,0).item
  }
  else %size = $file(%filepath)
  if ($isid) return $iif(%size,$true,$false)
  if ($show) {
    if (!%size) %e no data could be retrieved from $2
    ;else %e Finished retrieving info from $2 (saved to $iif(%flag == h,hash table:,file:) $3-)
  }
  return
  :error
  if ($com(%w)) .comclose %w
  if ($isfile(%w)) .remove %w
  if ($window(%w)) close -@ %w
}

on *:START:{
  ;.timerdicerss 0 120 getdicerss
}

alias getdicerss {
  rss -i http://www.dicevip.com/feed/index.php dicevip.ini
  if (%dicelast != $readini(dicevip.ini,#1,title)) {
    msg %pug.chan $codes(dicevip news update: $readini(dicevip.ini,#1,title))
    msg %pug.chan $codes(Description: $iif($len($readini(dicevip.ini,#1,description)) < 130,$ifmatch,$left($readini(dicevip.ini,#1,description),130) $+ ...))
    msg %pug.chan $codes(Link: $readini(dicevip.ini,#1,link))
    set %dicelast $readini(dicevip.ini,#1,title)
  } 
}
