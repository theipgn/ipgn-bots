alias banid {
  if ($read(%dir $+ batch/banned.cfg, w, * $+ $2)) {
    write -l $+ $readn %dir $+ batch/banned.cfg banid $1-
    msg $chan $codes(Ban found: $read(%dir $+ batch/banned.cfg,$readn) $+ . Ban files updated.)
  }
  else {
    write %dir $+ batch/banned.cfg banid $1-
  }
  copy -o %dir $+ batch/banned.cfg %dir $+ batch/banned_user.cfg
  run %dir $+ batch/bancfg.bat
  msg $chan $codes(Banned.cfg updated for $2 across all servers.)
  .timer 1 10 execban $2
}

alias remid {
  if ($read(%dir $+ batch/banned.cfg, w, * $+ $1)) {
    msg $chan $codes(Ban removed: $read(%dir $+ batch/banned.cfg,$readn) $+ . Ban files updated.)
    msg $chan $codes(Banned.cfg updated for $1 across all servers.)
    write -dl $+ $readn %dir $+ batch/banned.cfg
  }
  else {
    msg $chan %ipgntagl Ban not found. %ipgntagr
  }
  copy -o %dir $+ batch/banned.cfg %dir $+ batch/banned_user.cfg
  run %dir $+ batch/bancfg.bat
  .timer 1 10 execban $2
}

alias execban {
  var %y = $lines(serverlist.txt)
  var %x = 1
  var %banid = $1
  while (%x <= %y) {
    tokenize 32 $read(serverlist.txt,%x)
    .timer 1 $calc(%x * 4) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),banid 0 %banid kick)
    .timer 1 $calc((%x * 4) + 2) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),writeid)
    inc %x
  }
}

alias massrs {
  var %y = $lines(serverlist.txt)
  var %x = 1
  msg %pug.adminchan $codes(Running mass restart script)
  while (%x <= %y) {
    tokenize 32 $read(serverlist.txt,%x)
    .timer 1 $calc(%x * 30) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),say Scheduled restart in 3 seconds)
    .timer 1 $calc(%x * 30 + 3) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),quit)
    .timer 1 $calc(%x * 30) echo -s Restarting $4-
    inc %x
  }
}

alias massmsg {
  var %y = $lines(serverlist.txt)
  var %x = 1
  while (%x <= %y) {
    tokenize 32 $read(serverlist.txt,%x)
    .timer 1 $calc(%x * 30) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),say Check out iPGN's new site at www.ipgn.com.au)
    .timer 1 $calc((%x * 30) + 1800) .run rcon6.exe $+($1,$chr(44),$2,$chr(44),$3,$chr(44),say You can join us on #iPGN on irc.gamesurge.net)
    inc %x
  }
}
