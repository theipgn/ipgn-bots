on *:CONNECT:{
  .timer 07:00 1 1 timerbackup 0 432000 backup
}

alias backup {
  write -l1 %dir $+ batch\backup.bat "C:\Program Files\WinZip\wzzip.exe" -rP -x@ $+ %dir $+ batch/exclude.txt C:\backup\mIRC-S5-CS-Comp- $+ $date(dd-mm-yy) C:\iPGN-S5 
  write -l2 %dir $+ batch\backup.bat "C:\Program Files\WinZip\wzzip.exe" -rP C:\backup\stats $+ $date(dd-mm-yy) C:\stats
  write -l3 %dir $+ batch\backup.txt put C:\backup\mIRC-S5-CS-Comp- $+ $date(dd-mm-yy) $+ .zip
  run %dir $+ batch\backup.bat
}

menu nicklist {
  .Mass Message {
    var %a $0,%msg $?="msg"
    while %a {
      msg $($+($,%a),2) $codes(%msg)
      dec %a
    }
  }
}

alias msgall {
  var %x = $nick(#ipgn-comp,0)
  var %y = 1
  var %z = 0
  while (%y <= %x) {
    if (*.gamesurge* !iswm $address($nick(#ipgn-comp,%y),3)) && (*.ipgn* !iswm $address($nick(#ipgn-comp,%y),3)) && $address($nick(#ipgn-comp,%y),3) {
      var %n = $nick(#ipgn-comp,%y)
      msg $nick(#ipgn-comp,%y) $codes(You are encouraged to use //mode $nick(#ipgn-comp,%y) +x as it will help reduce bot lag and you will join games faster (please add to your perform))
      inc %z    
    }
    inc %y
  }
  echo -a %z
}

alias newstats {
  write -c statsall.txt
  var %a = 1
  var %b = $lines(%dir $+ ids.txt)
  while (%a <= %b) {
    tokenize 37 $read(%dir $+ ids.txt,%a)
    write statsall.txt $2 $+ $str($chr(37) $+ 0,63)
    inc %a
  }

}

alias restore { 

  upload uploadbans %dir $+ bans.txt
  .timer 1 10 upload upload %dir $+ statsall.txt
  .timer 1 20 upload uploadgames %dir $+ games.txt
  .timer 1 30 upload ids %dir $+ ids.txt


  var %x = $findfile(%dir $+ games,*.txt,0) 


  msg %pug.adminchan Running restore
  part %pug.adminchan
  .timer 1 $calc(100 + (%x * 6)) join %pug.adminchan caj00n
  .timer 1 $calc(101 + (%x * 6)) msg %pug.adminchan Games now ok to run

  while (%x > 0) {
    .timery $+ $calc(40 + (%x * 6)) 1 $calc(40 + (%x * 6)) upload uploadendgame %dir $+ games/ $+ %x $+ .txt &game= $+ %x
    .timery $+ $calc(43 + (%x * 6)) 1 $calc(43 + (%x * 6)) upload uploadgame %dir $+ gamestats/ $+ %x $+ .txt &game= $+ %x
    dec %x
  }

}
