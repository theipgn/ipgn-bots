on *:TEXT:*:#ipgn-board:{
  if ($1 = !sms) {
    if ($istok(Jax mcky Stevie reamo,$2,32) || $2 isnum) && ($len($3) <= 11) && ($len($4-) <= 160) && ($4) {
      if ($2 isnum) { var %target = $2 }
      else { var %target = $readini(sms.ini,numbers,$2) }
      sms get /httpsend.php $+(usr=admin@ipgn.com.au&pass=caj00n&mess=,$4-,&to=,%target,&from=,$3)
    }
    else { msg $chan $codes(Syntax: !sms <name|number> <alias> <message>) }
  }
  if ($1 = !changeid) {
    if ($3) && ($steamid($2)) {
      changeid $2 $3
    }
    else {
      msg $chan $codes(Syntax: !changeid <statname> <steamid>)
    }
  }
  if ($1 = !delbotban) && ($2) {
    if ($timer(threadwait).secs) {
      msg $chan $codes(Please wait12 $timer(threadwait).secs seconds)
      halt 
    }
    if ($read(%dir $+ bans.txt,w,$id($steamid($2)) $+ $chr(37) $+ *)) {
      var %line = $read(%dir $+ bans.txt,$readn)
      write -dl $+ $readn %dir $+ bans.txt
      msg $chan $codes(12 $+ $2 is now unbanned.)
      msg %pug.chan $codes(12 $+ $2 is now unbanned.)
      rank
      thread delban $authname($2) $auth($nick) 
    }
    else {
      msg $chan $codes(No ban for12 $2 found.)
    }
  }
  if ($1 = !baninfo) && ($2) {
    if ($gettok($read(%dir $+ bans.txt,w,$id($steamid($2)) $+ $chr(37) $+ *),3,37) = $2) {
      tokenize 37 $read(%dir $+ bans.txt,w,$id($steamid($2)) $+ $chr(37) $+ *)
      msg $chan $codes(Alias:12 $statname($1) SteamID:12 $steamid($statname($1)) By:12 $statname($2) Date:12 $4 Auth Name:12 $authname($1) Reason:12 $3)
    }
    else {
      msg $chan $codes(Nothing found for12 $2 $+ .)
    }
  }
}
alias sms {
  write -c sms.txt
  ; Open a new window, where we will output the data
  window -e @sms
  linesep @sms

  ; Set first parameter, which defaults to GET
  var %method = $iif($1,$1,GET)

  ; Set second parameter, which defautls to /
  var %page = $iif($2,$2,/)

  %string = $urlencode_string($3-)

  ; Set socket name
  var %sock = sms $+ $ticks

  ; Open socket and send data to it
  sockopen %sock smsmanager.com.au 80
  sockmark %sock %method %page %string
}

on *:SOCKOPEN:sms*:{
  ; Connection has been made
  aline @sms Connection established...
  aline @sms MODE: $getmark($sockname,1-)

  ; Define command
  var %a = sockwrite -n $sockname


  ; First send GET/POST, next send Host
  %a $getmark($sockname,1-2) $+ $iif($getmark($sockname,1) == GET && %string,$+(?,%string)) HTTP/1.1
  %a Host: smsmanager.com.au

  ; If method is POST, we need to include 2 extra parameters
  if ($getmark($sockname,1) == POST) {
    %a Content-Length: $len(%string)
    %a Content-Type: application/x-www-form-urlencoded
  }

  ; Force connection to close
  %a Connection: close

  ;Just-to-make-sure requests:
  %a Accept: */*
  %a Accept-Charset: *
  %a Accept-Encoding: *
  %a Accept-Language: *
  %a User-Agent: Mozilla/5.0

  %a $crlf

}

on *:SOCKREAD:sms*:{
  sockread %tmp
  if ($regex(%tmp,/^success$/)) {
    msg #ipgn-board $codes(Success!)
  }

  write sms.txt %tmp
}
