alias checkunban {
  var %x = $lines(%dir $+ bans.txt)
  var %y = 1
  while (%y <= %x) {
    tokenize 37 $read(%dir $+ bans.txt,%y)
    if ($5 > $ctime) {
      ;unban
      if ($timer(threadwait).secs) {
        sleep $timer(threadwait).secs
      }
      var %line = $read(%dir $+ bans.txt,%y)
      write -dl $+ $readn %dir $+ bans.txt
      msg $chan $codes(12 $+ $statname($1) is now unbanned.)
      msg %pug.chan $codes(12 $+ $statname($1) is now unbanned.)
      rank
      thread delban $authname($1) iPGN-Bot 
    }
    inc %y
  }
}
