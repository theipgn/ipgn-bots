alias lookup {
  unset %s.*
  %s.chan = $2
  if ($regex($1,^STEAM_\d:(\d):(\d+)$)) {
    var %n = $right(76561197960265728,$calc($len($regml(2) + 1)))
    var %id = $calc(($regml(2) * 2) + $regml(1) + %n)
    var %conv = $+(/profiles/,$left(76561197960265728,$calc(13 - $len($regml(2)))),%id)
    vac get %conv $1 %conv
  }
  elseif ($regex($1,^7656119\d+$)) {
    var %n = $right($1,10)
    var %n = $calc(%n - 7960265728)
    if (. isin $calc(%n / 2)) { var %one = 1 | dec %n }
    var %n = $calc(%n / 2)
    vac get /profiles/ $+ $1 $+(STEAM_0:,$iif(%one,1,0),:,%n) /profiles/ $+ $1
  }
  else { msg %s.chan $codes($1 does not appear to be a valid profile or Steam ID.) }
}


alias vac {
  write -c vac.txt
  ; Open a new window, where we will output the data
  window -e @vac
  linesep @vac

  ; Set first parameter, which defaults to GET
  var %method = $iif($1,$1,GET)

  ; Set second parameter, which defautls to /
  var %page = $iif($2,$2,/)
  %s.id = $3
  %s.link = $4
  ; Set socket name
  var %sock = vac $+ $ticks

  ; Open socket and send data to it
  sockopen %sock steamcommunity.com 80
  sockmark %sock %method %page $3-
}

on *:SOCKOPEN:vac*:{
  ; Connection has been made
  aline @vac Connection established...
  aline @vac MODE: $getmark($sockname,1-)

  ; Define command
  var %a = sockwrite -n $sockname


  ; First send GET/POST, next send Host
  %a $getmark($sockname,1-2) HTTP/1.1
  %a Host: steamcommunity.com

  ; Force connection to close
  %a Connection: close

  ;Just-to-make-sure requests:
  %a Accept: */*
  %a Accept-Charset: *
  %a Accept-Encoding: *
  %a Accept-Language: *
  %a User-Agent: Mozilla/5.0

  %a $crlf

}

on *:SOCKREAD:vac*:{
  sockread %tmp
  if ($regex(%tmp,/Location: http://steamcommunity.com(\S+)/)) {
    %s.loc = $regml(1)
    echo -a $regml(1)
    vac get $regml(1) %s.id %s.link
  }
  if ($regex(%tmp,/<span class="vacText">ban\(s\) on record</span>/)) {
    msg %s.chan $codes(%s.id $chr(61) http://steamcommunity.com $+ $iif(%s.loc,%s.loc,%s.link) (Ban(s) on record))
    unset %s.id
  }
  if ($regex(%tmp,/</html>/)) && (%s.id) {
    msg %s.chan $codes(%s.id $chr(61) http://steamcommunity.com $+ $iif(%s.loc,%s.loc,%s.link) (No bans found))
  }
  write vac.txt %tmp
}
