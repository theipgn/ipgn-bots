alias backup {
  write -l1 %dir $+ batch\backup.bat "C:\Program Files\WinZip\wzzip.exe" -rP -x@ $+ %dir $+ batch/exclude.txt C:\backup\mIRC-CSS-Comp- $+ $date(dd-mm-yy) C:\iPGN-CSS
  write -l2 %dir $+ batch\backup.bat "C:\Program Files\WinZip\wzzip.exe" -rP C:\backup\stats-css $+ $date(dd-mm-yy) C:\stats-css
  write -l3 %dir $+ batch\backup.txt put C:\backup\mIRC-S5-CSS-Comp- $+ $date(dd-mm-yy) $+ .zip
  run %dir $+ batch\backup.bat
}
