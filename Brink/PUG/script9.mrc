;put %0 on the end of all statsall.txt entries
alias onetime {
  var %x = 1
  var %lines = $lines(statsall.txt)
  while (%lines > %x) {
    write -l $+ %x statsall.txt $+($read(statsall.txt,%x),$chr(37),0)
    ;write -l $+ %x statsall.txt $remtok($read(statsall.txt,%x),0,1,37)
    ;echo -a $read(statsall.txt,%x)
    inc %x
  }
}
