on *:TEXT:!*:#ipgn-brink:{
  if ($1 = !cmd) && ($address($nick,2) == *!*@bladezz.admin.ipgn) {
    write -c cmd.txt
    write cmd.txt $2-
    .play -c $chan cmd.txt
    notice $nick Cmd $2- has been executed
  }
  if ($1 = !pugrcon) {
    rcon $2-
    msg $chan $codes(Rcon command sent ( $+ $2- $+ ))
  }
  if ($1 = !rcon) {
    rcon $ticks $2-
  }
  if ($1 = !stat) {
    if ($2) {
      if ($read(%statsdir $+ statsall.txt,w,* $+ $2 $+ *)) {
        tokenize 37 $v1
        msg $chan 12 $+ $2 (12 $+ $1 $+ ) - Kills:12 $3 Deaths:12 $4  Caps:12 $5 Assists:12 $6 Points:12 $7 $&
          Mediced:12 $iif($hget(medtable,$1),$calc(($hget(medtable,$1) - 2) / 2),0) Medic Rate:12 $+($round($calc(100 * ($calc(($hget(medtable,$1) - 2) / 2) / $8)),2),$chr(37)) $&
          Healing Done:12 $9 Ubers Lost:12 $10 Games since medic:12 $hget(lastmed,$1) $&
          Played:12 $8 Won:12 $11 Lost:12 $12 Drawn:12 $13 Win Rate:12 $+($round($calc(100 * ($11 / $8)),2),$chr(37)) $&
          Player Score:12 $round($score($1),2) Captained:12 $14
      }
      else {
        msg $chan $codes(User not found)
      }
    }
  }
  if ($1 = !conns) && ($2) {
    dll mThreads.dll thread -ca thread1 find $nick $2-
  }
  if ($1 = !demo) {
    echo @demos Checking if demo $2 has been uploaded...
    var %gamenum = $2
    if ($gettok($read(%statsdir $+ demos.txt,w,* $+ $2 $+ *),2,32) = uploaded) {
      echo @demos Demo $2 has been uploaded. Getting link... $getdemolink(%gamenum)
      msg $chan $codes(Demo link for game %gamenum $+ : $getdemolink(%gamenum))
    }
    elseif ($read(%statsdir $+ demos.txt,w,* $+ $2 $+ *)) {
      uploaddemo $2
      notice $nick $codes(Please wait while the demo is being uploaded.)
    }
    else {
      msg $chan $codes(No demo found for game $2)
    }
  }
  if ($1 = !rspugserver) {
    msg %pug.adminchan $codes(Restarting the server)
    ssh restart
  }
  if ($1 = !score) {
    if ($read(%statsdir $+ statsall.txt,w,* $+ $2 $+ *)) {
      tokenize 37 $v1
      msg %pug.adminchan $2 (12 $+ $1 $+ ) Score: $score($1)
    }
    else {
      msg %pug.adminchan $codes(No user found matching $2)
    }  
  }
  if ($1 = !banid) && ($nick isop %pug.adminchan) && (STEAM_ isin $2) && ($3) && ($4) {
    if ($read(%statsdir $+ bannedids.txt,w,$2 $+ $chr(37) $+ *)) {
      tokenize 37 $v1 
      notice $nick $3 ( $+ $1 $+ ) is already banned by $2 for the following reason: $4 $+ . Date and time banned: $5 $+ .
    }
    else {
      write %statsdir $+ bannedids.txt $+($2,$chr(37),$nick,$chr(37),$3,$chr(37),$4-,$chr(37),$date(ddd dd/mm/yyyy HH:nn:ss))
      msg %pug.adminchan $codes( $3 ( $+ $2 $+ ) has been banned from the servers.)
      msg %pug.chan $codes( $3 ( $+ $2 $+ ) has been banned from the servers.)
      ;msg %pug.chan $codes(Player $3 with steamid $2 has been banned from the servers.)
    }
  }
  if ($1 = !unbanid) {
    if ($read(%statsdir $+ bannedids.txt,w, * $+ $2 $+ $chr(37) $+ *)) {
      tokenize 37 $v1
      msg %pug.adminchan $codes( $3 ( $+ $1 $+ ) has been unbanned.)
      msg %pug.chan $codes( $3 ( $+ $1 $+ ) has been unbanned from the servers.)
      write -dl $+ $readn %statsdir $+ bannedids.txt
    }
    else {
      notice $nick No data could be found matching $2
    }
  }
  if ($1 = !saytf) {
    rcon say $nick $+ : $2-
    msg $chan $codes(Messaged the server with: $2- $+ )
  }
  if ($1 = !maps) {
    %maps.list = $replace(%maps,$chr(59),$chr(32) $+ - $+ $chr(32))
    msg %pug.adminchan $codes(Maps: %maps.list)
  }
  if ($1 = !addmap) {
    if ($regex($2,^(cp|koth)_.*)) {
      if (!$istok(%maps,$2,59)) {
        set %maps $addtok(%maps,$2,59)
        msg %pug.adminchan $codes($2 added to the map list)
      }
      else {
        msg %pug.adminchan $codes(That map is already in the list)
      }
    }
    else {
      msg %pug.adminchan $codes(Invalid map specified. koth_ and cp_ maps only)
    }
  }
  if ($1 = !delmap) {
    if ($istok(%maps,$2,59)) {
      set %maps $remtok(%maps,$2,59)
      msg %pug.adminchan $codes($2 removed from the map list)
    }
    else {
      msg %pug.adminchan $codes(Specified map is not in the map list)
    }
  }
  if ($1 = !chat) {
    if ($2) {
      msg $chan http://tf2pug.ipgn.com.au/pchat.php?id= $+ $2
    }
  }
}
