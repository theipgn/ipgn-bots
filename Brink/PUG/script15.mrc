;
; Q3 RCON for MIRC v1.00
; writte by >V<Callista
; callista@void-jumper.de
;
; Feel free to redistribute, modify, copy and the rest.
; But if you do: please give me credit. Thanx.
; For comments, feedback, bug reports contact me.
;
; Have fun :)
;

alias q3rcon_binstr {
  var %i
  unset %str
  %i = 1
  %nextSpace = 0
  while (%i <= $bvar(&bin,0)) {
    if ($bvar(&bin, %i) == 32) %str = %str $+ $chr(160)
    else %str = %str $+ $chr($bvar(&bin, %i))
    inc %i
  }
}

alias q3rcon_udpread {
  sockread &temp
  if ($sockbr == 0) return
  if ($bvar(&temp,1,1) != 255) {
    echo -a 9*1q3rcon9*1 Received invalid server response.
    return
  }
  %q3pos = $bfind(&temp, 1, 10)
  %q3oldpos = %q3pos
  inc %q3oldpos
  %q3done = 1
  while (%q3done != 0) {
    %q3pos = $bfind(&temp, %q3oldpos, 10)
    if (%q3pos >= $bvar(&temp,0)) {
      %q3done = 0
    }
    bunset &bin
    bcopy &bin 1 &temp %q3oldpos $calc(%q3pos - %q3oldpos)
    q3rcon_binstr
    echo -a 9*1q3rcon9*1 9<--1 %str
    %q3oldpos = %q3pos
    inc %q3oldpos
  }
}

alias q3rconsettings {
  if ($3 == $null) {
    echo -a 9*1q3rcon9*1 You must provide three parameters: the IP, the portnumber and the password.
    return
  }
  %q3rcon_ip = $1
  %q3rcon_port = $2
  %q3rcon_password = $3
  echo -a 9*1q3rcon9*1 Settings updated.
  sockclose q3rconsock
  %q3rcon_sockopen=0
  echo -a 9*1q3rcon9*1 %q3rcon_ip

}

alias q3rcon {
  if (%q3rcon_ip == $null) {
    echo -a 9*1q3rcon9*1 You must use /q3rconsettings first.
    return
  }
  if ($1 == $null) {
    echo -a 9*1q3rcon9*1 Q3Rcon script by >V<Callista - callista@void-jumper.de
    echo -a 9*1q3rcon9*1 Use the following command first: /q3rconsettings <server ip> <server port> <rcon password>
    echo -a 9*1q3rcon9*1 Now you can send commands to this server: /q3rcon <command>
    echo -a 9*1q3rcon9*1 Example: /q3rcon serverinfo
    return
  }
  echo -a 9*1q3rcon9*1 9-->1 $1-
  bunset &bin
  bset &bin 1 255 255 255 255 $asc(r) $asc(c) $asc(o) $asc(n) 32
  %pos = 10
  %i = 1
  while (%i <= $len(%q3rcon_password)) {
    bset &bin %pos $asc($mid(%q3rcon_password,%i,1))
    inc %pos
    inc %i
  }
  bset &bin %pos 32
  inc %pos
  %i = 1
  while (%i <= $len($1-)) {
    bset &bin %pos $asc($mid($1-,%i,1))
    inc %pos
    inc %i
  }
  if (%q3rcon_sockopen != 1) {
    sockudp -k q3rconsock %q3rcon_ip %q3rcon_port &bin
    %q3rcon_sockopen=1
  }
  else sockudp -k q3rconsock %q3rcon_ip %q3rcon_port &bin
}

on 1:udpread:q3rconsock:q3rcon_udpread
on 1:START:echo 9*1q3rcon9*1 Q3Rcon script v1.00 by >V<Callista
