alias add {
  var %keys = id;name;kill;death;cap;assist;points;games;healing;uberlost;win;loss;draw;captain
  var %key = $findtok(%keys,$2,1,59)
  ;echo -s %keys %key
  if (!$2) {
    if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { return }
    if ($read(%statsdir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)) { 
      var %oldname = $gettok($read(%statsdir $+ statsall.txt,w,$1 $+ $chr(37) $+ *),2,37)
    }
    else { 
      var %oldname = $sid($1)
    }
    write stats.txt $+($1,$chr(37),%oldname,$chr(37),0%0%0%0%0%0%0%0%0%0%0%0) 
  } 
  if ($2 = captain) {
    if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),%key,37) + $3),%key,37)
    }
    else { 
      write stats.txt $+($1,$chr(37),$sid($1),$chr(37),0%0%0%0%0%0%0%0%0%0%0%0) 
      if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
        write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),%key,37) + $3),%key,37)
      }
    }
  }
  elseif ($2 = name) {
    if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$3-,%key,37)
    }
    else {
      write stats.txt $+($1,$chr(37),$3-,$chr(37),0%0%0%0%0%0%0%0%0%0%0%0) 
    }
  }
  else {
    if (%match.on) {
      if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
        write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),%key,37) + $3),%key,37)
      }
      else { 
        write stats.txt $+($1,$chr(37),$sid($1),$chr(37),0%0%0%0%0%0%0%0%0%0%0%0) 
        if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { 
          write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok($read(stats.txt,w,$1 $+ *),$calc($gettok($read(stats.txt,w,$1 $+ *),%key,37) + $3),%key,37)
        }
      }
    }
  }
}

alias addweapon {
  var %weapons = tf_projectile_rocket rocketlauncher_directhit shotgun_soldier pickaxe shovel scattergun pistol_scout bat force_a_nature flamethrower shotgun_pyro fireaxe backburner axtinguisher flaregun deflect_flare deflect_promode deflect_rocket deflect_sticky taunt_pyro minigun shotgun_hwg fists natascha gloves taunt_heavy shotgun_primary pistol wrench obj_sentrygun obj_sentrygun2 obj_sentrygun3 sniperrifle smg club tf_projectile_arrow compound_bow taunt_sniper tf_projectile_pipe tf_projectile_pipe_remote sword bottle revolver ambassador knife syringegun_medic bonesaw blutsauger ubersaw world player
  if ($2) && (!$istok(%weapons,$2,32)) { echo @weapons $clr(orange) $+ Weapon $2 detected... ignoring | return }
  var %weap = $calc($findtok(%weapons,$2,1,32) + 1)
  if (!$2) {
    var %numweapons = $numtok(%weapons,32)
    var %x = 1
    %weaponline = $1 $+ %
    while (%numweapons >= %x) {
      %weaponline = $instok(%weaponline,0,2,37)
      inc %x
    }
    write weapons.txt %weaponline
  }
  if (%match.on) && ($2) {
    if ($read(weapons.txt,w,$1 $+ *)) { 
      ;echo @weapons Current weapon line for $1 - $read(weapons.txt,w,$1 $+ *)
      ;echo @weapons New weapon line for $1 - $puttok($read(weapons.txt,w,$1 $+ *),$calc($gettok($read(weapons.txt,w,$1 $+ *),%weap,37) + 1),%weap,37)
      write -w $+ $1 $+ * weapons.txt $puttok($read(weapons.txt,w,$1 $+ *),$calc($gettok($read(weapons.txt,w,$1 $+ *),%weap,37) + 1),%weap,37)
    }
    else {
      var %numweapons = $numtok(%weapons,32)
      var %x = 1
      %weaponline = $1 $+ %
      while (%numweapons >= %x) {
        %weaponline = $instok(%weaponline,0,2,37)
        inc %x
      }
      ;echo @weapons Player was not here at start - $puttok(%weaponline,$calc($gettok(%weaponline,%weap,37) + 1),%weap,37)
      write weapons.txt $puttok(%weaponline,$calc($gettok(%weaponline,%weap,37) + 1),%weap,37)
    }
  }
}
