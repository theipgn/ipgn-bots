alias resetstats {
  var %lines = $lines(%statsdir $+ statsall.txt)
  var %x = 1
  ;id;name;kill;death;cap;assist;points;games;healing;uberlost;won;lost;draw
  while (%lines >= %x) {
    tokenize 37 $read(%statsdir $+ statsall.txt,%x)
    write -l $+ %x %statsdir $+ statsall.txt $+($1,$chr(37),$2,$chr(37),0%0%0%0%0%0%0%0%0%0%0)
    inc %x
  }
  msg %pug.chan $codes(All statistics have been reset)
  msg %pug.adminchan $codes(All statistics have been reset)
}

alias clearstats {
  var %lines = $lines(%statsdir $+ statsall.txt)
  var %x = 1
  while (%lines >= %x) {
    tokenize 37 $read(%statsdir $+ statsall.txt,%x)
    if (($3 = 0) && ($4 = 0) && ($5 = 0)) {
      echo @upload $read(%statsdir $+ statsall.txt,%x)
      write -dl $+ %x %statsdir $+ statsall.txt
    }
    inc %x
  }

}

alias modstats {
  var %lines = $lines(%statsdir $+ statsall.txt)
  var %x = 1
  while (%lines >= %x) {
    tokenize 37 $read(%statsdir $+ statsall.txt,%x)
    write -l $+ %x %statsdir $+ statsall.txt $+($1,$chr(37),$2,$chr(37),$3,$chr(37),$4,$chr(37),$5,$chr(37),$6,$chr(37),$7,$chr(37),$8,$chr(37),$9,$chr(37),$10,$chr(37),$11,$chr(37),$12,$chr(37),$13,$chr(37),0)
    inc %x
  }
}

;tf_projectile_rocket rocketlauncher_directhit shotgun_soldier pickaxe shovel scattergun pistol_scout bat force_a_nature flamethrower shotgun_pyro fireaxe backburner axtinguisher flaregun deflect_flare deflect_promode deflect_rocket deflect_sticky taunt_pyro minigun shotgun_hwg fists natascha gloves taunt_heavy shotgun_primary pistol wrench obj_sentrygun obj_sentrygun2 obj_sentrygun3 sniperrifle smg club tf_projectile_arrow compound_bow taunt_sniper tf_projectile_pipe tf_projectile_pipe_remote sword bottle revolver ambassador knife syringegun_medic bonesaw blutsauger ubersaw world player
alias resetweapons {
  var %weapons = tf_projectile_rocket rocketlauncher_directhit shotgun_soldier pickaxe shovel scattergun pistol_scout bat force_a_nature flamethrower shotgun_pyro fireaxe backburner axtinguisher flaregun deflect_flare deflect_promode deflect_rocket deflect_sticky taunt_pyro minigun shotgun_hwg fists natascha gloves taunt_heavy shotgun_primary pistol wrench obj_sentrygun obj_sentrygun2 obj_sentrygun3 sniperrifle smg club tf_projectile_arrow compound_bow taunt_sniper tf_projectile_pipe tf_projectile_pipe_remote sword bottle revolver ambassador knife syringegun_medic bonesaw blutsauger ubersaw world player
  var %lines = $lines(%statsdir $+ weaponsall.txt)
  var %numweapons = $numtok(%weapons,32)
  ;echo -s Number of weapons: %numweapons
  var %y = 2
  var %weaponline = 0%
  while (%numweapons >= %y) {
    %weaponline = $instok(%weaponline,0,2,37)
    inc %y
  }
  ;echo -s Weapon line: %weaponline
  ;echo -s Number of weapons in weaponline: $numtok(%weaponline,37)
  var %x = 1
  while (%lines >= %x) {
    tokenize 37 $read(%statsdir $+ weaponsall.txt,%x)
    write -l $+ %x %statsdir $+ weaponsall.txt $+($1,$chr(37),%weaponline)
    inc %x
  }
  msg %pug.chan $codes(All weapon stats have been reset)
  msg %pug.adminchan $codes(All weapon stats have been reset)
}

alias weaponsfordb {
  var %weapons = tf_projectile_rocket rocketlauncher_directhit shotgun_soldier pickaxe shovel scattergun pistol_scout bat force_a_nature flamethrower shotgun_pyro fireaxe backburner axtinguisher flaregun deflect_flare deflect_promode deflect_rocket deflect_sticky taunt_pyro minigun shotgun_hwg fists natascha gloves taunt_heavy shotgun_primary pistol wrench obj_sentrygun obj_sentrygun2 obj_sentrygun3 sniperrifle smg club tf_projectile_arrow compound_bow taunt_sniper tf_projectile_pipe tf_projectile_pipe_remote sword bottle revolver ambassador knife syringegun_medic bonesaw blutsauger ubersaw world player
  var %x = 1
  var %numweapons = $numtok(%weapons,32)
  while (%numweapons >= %x) {
    write temp.txt $gettok(%weapons,%x,32) int,
    inc %x
  }
}

alias weaponsfordb2 {
  var %weapons = tf_projectile_rocket rocketlauncher_directhit shotgun_soldier pickaxe shovel scattergun pistol_scout bat force_a_nature flamethrower shotgun_pyro fireaxe backburner axtinguisher flaregun deflect_flare deflect_promode deflect_rocket deflect_sticky taunt_pyro minigun shotgun_hwg fists natascha gloves taunt_heavy shotgun_primary pistol wrench obj_sentrygun obj_sentrygun2 obj_sentrygun3 sniperrifle smg club tf_projectile_arrow compound_bow taunt_sniper tf_projectile_pipe tf_projectile_pipe_remote sword bottle revolver ambassador knife syringegun_medic bonesaw blutsauger ubersaw world player
  var %x = 1
  var %numweapons = $numtok(%weapons,32)
  unset %insline
  while (%numweapons >= %x) {
    %insline = $addtok(%insline,$chr(39) $+ $chr(123) $+ $chr(36) $+ words $+ $chr(91) $+ %x $+ $chr(93) $+ $chr(125) $+ $chr(39) $+ $chr(44),32)
    inc %x
  }
  echo %insline
}

alias weaponsfordb3 {
  var %weapons = tf_projectile_rocket rocketlauncher_directhit shotgun_soldier pickaxe shovel scattergun pistol_scout bat force_a_nature flamethrower shotgun_pyro fireaxe backburner axtinguisher flaregun deflect_flare deflect_promode deflect_rocket deflect_sticky taunt_pyro minigun shotgun_hwg fists natascha gloves taunt_heavy shotgun_primary pistol wrench obj_sentrygun obj_sentrygun2 obj_sentrygun3 sniperrifle smg club tf_projectile_arrow compound_bow taunt_sniper tf_projectile_pipe tf_projectile_pipe_remote sword bottle revolver ambassador knife syringegun_medic bonesaw blutsauger ubersaw world player
  echo $replace(%weapons,$chr(32),$chr(44) $+ $chr(32))
}

alias rigmed {
  var %x = $lines(rigids.txt)
  var %y = 1
  while (%x >= %y) {
    hadd -s medtable $read(rigids.txt,%y) 3500
    inc %y
  }
  hsave -s medtable %statsdir $+ pugmedtable
  msg %pug.adminchan $codes(Rigged med stats inserted)
}

alias fixconlog {
  ;var %x = $findfile(connections,*.txt,0)
  ;while (%x >= 1) {
  ;  write list.txt $findfile(connections,*.txt,%x) $ctime($remove($findfile(connections,*.txt,%x),connections,.txt))
  ;  dec %x
  ;}
  var %lines = $lines(list.txt)
  var %y = 1
  while (%y <= %lines) {
    .copy -a $read(list.txt,%y) %statsdir $+ connections_log.txt
    .remove $read(list.txt,%y)
    write -dl $+ %y list.txt
    inc %y
  }
  echo -s Log files merged to %statsdir $+ connections_log.txt
}

alias winloss {
  var %fakescore.red = 2
  var %fakescore.blue = 3
  var %ppt = 6
  var %playernum = 1
  if (%fakescore.red > %fakescore.blue) {
    echo -s %fakescore.red > %fakescore.blue
    while (%playernum <= %ppt) {
      ;add %player.red [ $+ [ %playernum ] ] won 1
      ;add %player.blue [ $+ [ %playernum ] ] lost 1
      echo -s write -w $+ %player.red [ $+ [ %playernum ] ] fakestats.txt $puttok($read(fakestats.txt,w,%player.red [ $+ [ %playernum ] ] $+ *),1,11,37)
      echo -s write -w $+ %player.blue [ $+ [ %playernum ] ] fakestats.txt $puttok($read(fakestats.txt,w,%player.red [ $+ [ %playernum ] ] $+ *),1,12,37)
      inc %playernum
    }
  }
  if (%fakescore.blue > %fakescore.red) {
    echo -s %fakescore.red < %fakescore.blue
    while (%playernum <= %ppt) {
      ;add %player.red [ $+ [ %playernum ] ] won 1
      ;add %player.blue [ $+ [ %playernum ] ] lost 1
      echo -s write -w $+ %player.red [ $+ [ %playernum ] ] fakestats.txt $puttok($read(fakestats.txt,w,%player.red [ $+ [ %playernum ] ] $+ *),1,12,37)
      echo -s write -w $+ %player.blue [ $+ [ %playernum ] ] fakestats.txt $puttok($read(fakestats.txt,w,%player.red [ $+ [ %playernum ] ] $+ *),1,11,37)
      inc %playernum
    }
  }
  if (%fakescore.red == %fakescore.blue) {
    while (%playernum <= %ppt) {
      ;add %player.red [ $+ [ %playernum ] ] won 1
      ;add %player.blue [ $+ [ %playernum ] ] lost 1
      echo -s write -w $+ %player.red [ $+ [ %playernum ] ] fakestats.txt $puttok($read(fakestats.txt,w,%player.red [ $+ [ %playernum ] ] $+ *),1,13,37)
      echo -s write -w $+ %player.blue [ $+ [ %playernum ] ] fakestats.txt $puttok($read(fakestats.txt,w,%player.red [ $+ [ %playernum ] ] $+ *),1,13,37)
      inc %playernum
    }
  }
}

alias setupfakefile {
  write -c fakestats.txt
  var %x = 1
  var %y = 6
  while (%x <= %y) {
    write fakestats.txt $+(%player.red [ $+ [ %x ] ],$chr(37),bob,$chr(37),0%0%0%0%0%0%0%0%0%0%0)
    write fakestats.txt $+(%player.blue [ $+ [ %x ] ],$chr(37),bob,$chr(37),0%0%0%0%0%0%0%0%0%0%0)
    inc %x
  }
}

alias regtest {
  noop $regex($1,^(cp|koth)_.*)
  echo -s $regml(1)
}

alias picktesting {
  %team1captain = STEAM_0:1:10325827
  %team2captain = STEAM_0:0:6127703
  %team1ids = %team1captain
  %team2ids = %team2captain
  %player.blue1 = %team1captain
  %player.red1 = %team2captain
  add %team1captain captain 1
  add %team2captain captain 1
  rcon say Blue Captain: $getnamebyid(%team1captain)
  rcon say Red Captain: $getnamebyid(%team2captain)
  %picklist = %lastgameids
  %picklist = $remtok(%picklist,%team1captain,44)
  %picklist = $remtok(%picklist,%team2captain,44)
  set %pick $rand(1,2)
  picktesting2
  var %x = 1, %y = $lines(picks.txt)
  var %availablePicks
  while (%x <= %y) {
    %availablePicks = $addtok(%availablePicks,say $gettok($read(picks.txt,%x),2,37),59)
    inc %x
  }
  rcon say Players available for picking:; %availablePicks
}

alias mumblesim {
  mumbleServer password gettheFUCKoUT
  .timer 1 8 mumbleServer restart
}

alias picktesting2 {
  var %x = 1, %y = $numtok(%picklist,44)
  while (%x <= %y) {
    write picks.txt $+($gettok(%picklist,%x,44),$chr(37),$getnamebyid($gettok(%picklist,%x,44)))
    inc %x
  }
}

alias chatupfix {
  var %x = 1
  var %y = $lines(%statsdir $+ chat\ $+ 8652.txt)
  while (%x <= %y) {
    write -l $+ %x %statsdir $+ chat\ $+ 8652.txt $replace($read(%statsdir $+ chat\ $+ 8647.txt,%x),$chr(44),\ $+ $chr(44))
    inc %x
  }
}
