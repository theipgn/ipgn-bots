alias uploaddemo {
  set % [ $+ demo. $+ [ $1 ] ] $read(%statsdir $+ demos.txt,w,* $+ $1 $+ *)
  ssh demo % [ $+ demo. $+ [ $1 ] ]
  echo @demos write -w* $+ % [ $+ demo. $+ [ $1 ] ] $+ * %statsdir $+ demos.txt $+($read(%statsdir $+ demos.txt,w,% [ $+ demo. $+ [ $1 ] ] $+ *),$chr(32),uploaded)
  write -w* $+ % [ $+ demo. $+ [ $1 ] ] $+ * %statsdir $+ demos.txt $+($read(%statsdir $+ demos.txt,w,% [ $+ demo. $+ [ $1 ] ] $+ *),$chr(32),uploaded)
  set % [ $+ demo. $+ [ $1 ] $+ .link ] http://games.ipgn.com.au/downloads/tf2/demos/pug/ $+ % [ $+ demo. $+ [ $1 ] ]
  shortenurl $1 % [ $+ demo. $+ [ $1 ] $+ .link ]
}

alias shortenurl {
  set % [ $+ shorten.url. $+ [ $1 ] $+ .link ] $2 
  echo @shorten Demo $1 - Link $2
  sockopen shorten.url. [ $+ [ $1 ] ] tinyurl.com 80
}

on *:sockopen:shorten.*:{
  if ($sockerr) {
    echo -a Connection failed
    sockclose shorten
    return
  }
  sockwrite -tn $sockname GET /create.php?url= $+ % [ $+ [ $sockname ] $+ .link ] HTTP/1.1
  sockwrite -tn $sockname Host: tinyurl.com
  sockwrite -tn $sockname Connection: Keep-Alive
  sockwrite -tn $sockname Content-Type: text/html
  sockwrite -tn $sockname
}

on *:sockread:shorten.*:{
  sockread % [ $+ [ $sockname ] $+ .sockdata ]
  tokenize 62 $left(% [ $+ [ $sockname ] $+ .sockdata ],255)
  if (http://tinyurl.com/*</b iswm $3) {
    echo @shorten $remove($gettok($3-,1,32),</b)
    set % [ $+ [ $sockname ] $+ .rec ] $remove($gettok($3-,1,32),</b)
    if ($read(%statsdir $+ demos.txt,w,% [ $+ demo. $+ [ $gettok($sockname,3,46) ] ] $+ *)) {
      tokenize 32 $v1
      write -w* $+ % [ $+ demo. $+ [ $gettok($sockname,3,46) ] ] $+ * %statsdir $+ demos.txt $+($1,$chr(32),$2,$chr(32),% [ $+ [ $sockname ] $+ .rec ])
      echo @shorten $readn %statsdir $+ demos.txt $+($1,$chr(32),$2,$chr(32),% [ $+ [ $sockname ] $+ .rec ])
      msg %pug.chan $codes(Link for demo $chr(35) $+ $gettok($sockname,3,46) $+ : % [ $+ [ $sockname ] $+ .rec ])
    }
  }
}

on *:sockclose:shorten.*: {
  if ($sockerr) {
    echo -a SOCKET CLOSED DUE TO ERROR
  }
  unset % [ $+ [ $sockname ] ] $+ *
  unset % [ $+ demo. $+ [ $gettok($sockname,3,46) ] ] $+ *
}

alias getdemolink {
  if ($read(%statsdir $+ demos.txt,w,* $+ $1 $+ .zip $+ *)) {
    ;echo -a $v1
    return $gettok($v1,3,32)
  }
  else {
    return ERROR: No link found
  }
}

alias demolinkall {
  var %x = $lines(%statsdir $+ demos.txt)
  var %y = 1
  while (%x >= %y) {
    tokenize 32 $read(%statsdir $+ demos.txt,%y)
    var %demo_line = $read(%statsdir $+ demos.txt,%y)
    if (!$2) || (!$3) {
      var %demo_num_zip = $gettok(%demo_line,9,$asc(-))
      var %demo_num = $gettok(%demo_num_zip,1,46)
      uploaddemo %demo_num
    }
    inc %y
  }
}

alias cleanupdemo {
  var %x = $lines(%statsdir $+ demos.txt)
  var %y = 1
  while (%x >= %y) {
    tokenize 32 $read(%statsdir $+ demos.txt,%y)
    if ($2 = uploaded) && (!$3) {
      write -l $+ %y %statsdir $+ demos.txt $1
      inc %y
    }
    else {
      inc %y
    }
  }
}
