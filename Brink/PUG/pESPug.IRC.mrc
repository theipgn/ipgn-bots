on *:NOTICE:*:*:{
  if (*I recognize you* iswm $1-) {
    if ($me != [iPGN-Brink]) {
      msg AuthServ@Services.GameSurge.net ghost [iPGN-Brink]
      .timer 1 10 nick [iPGN-Brink]
    }
    .timer 1 10 join %pug.chan
    .timer 1 10 join %pug.adminchan
  }
}
alias codes {
  return 12� $1- 12�
}

alias pinkcodes {
  return 13� $1- 13�
}

alias autonotify {
  if (!$timer(autonotify) && (!%full) && (%pug)) {
    .timerautonotify 4 240 autonotify
  }
  elseif (!%full) && ($timer(autonotify)) && (%pug) {
    msg %pug.chan $codes($calc((%pug.limit - $numtok(%players,32))) more players needed!)
  }
  elseif (!%pug) {
    timerautonotify off
  }
  else {
    halt
  }
}

alias newtopic {
  if ($1) {
    set %status $1-
  }
  .topic %pug.chan 12,0� 1i14,0PGN TF2 Pug! 1,0http://tf2pug.12ipgn1.com.au/ 12�1 N14ews: %news 12�1 S14tatus: %status 12�1 G14ame 1I14nfo: %pug.killchan 12�'
  .topic %pug.killchan 12,0� 1i14,0PGN TF2 Pug! 1,0http://tf2pug.12ipgn1.com.au/ 12�1 S14tatus: %status 12�
}

alias setteams {
  set %full 1
  msg %pug.chan $codes(Game $chr(35) $+ $calc(%gamesplayed + 1) is now in-game. In-game admin: %pug.starter $+ . Players will receive details shortly.)
  massdetails
  set %noget 1
  .timerget 1 180 unset %noget
}

alias massdetails {
  .timertimeout off
  set %detailed 1
  set -u3 %busy 1
  var %v = %pug.limit
  var %u = 1
  while (%u <= %v) {
    %nick = $gettok(%players,%u,32)
    if (%nick = %pug.starter) || (%nick isop %pug.chan) {
      notice %nick $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass // Admin pass: %adminpass )
      notice %nick $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
      notice %nick $codes(Mumble: /run $+(mumble://,%nick,:,%serverpass,@,%mumble.ip,:,%mumble.port,/?version=1.2.2))
    }
    else {
      notice %nick $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass)
      notice %nick $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
      notice %nick $codes(Mumble: /run $+(mumble://,%nick,:,%serverpass,@,%mumble.ip,:,%mumble.port,/?version=1.2.2))
    }
    inc %u
  }
}

alias details {
  if (!%busy) && (!%map.vote) {
    if ($1 isop %pug.chan) || ($1 = %pug.starter) {
      notice $1 $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass // Admin pass: %adminpass)
      notice $1 $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
      notice $1 $codes(Mumble: /run $+(mumble://,$1,:,%serverpass,@,%mumble.ip,:,%mumble.port,/TF2Pug?version=1.2.0))
      return
    }
    if ($istok(%players,$1,32)) {
      notice $1 $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass)
      notice $1 $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
      notice $1 $codes(Mumble: /run $+(mumble://,$1,:,%serverpass,@,%mumble.ip,:,%mumble.port,/TF2Pug?version=1.2.0))
      return
    }
  }
  if (!%busy) && ($1 isop $chan) {
    notice $1 $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass // Admin pass: %adminpass)
    notice $1 $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
    notice $1 $codes(Mumble: /run $+(mumble://,$1,:,%serverpass,@,%mumble.ip,:,%mumble.port,/TF2Pug?version=1.2.0))
  }
}

alias spamstats {
  unset %match.on
  if (!$read(stats.txt)) { endpug | halt }
  if (%endpug) {
    halt
  }
  rcon tv_stoprecord
  .timer 1 2 ssh restart
  set %endpug 1
  mode %pug.chan +m
  var %y = $lines(stats.txt)
  var %x = 1
  while (%x <= %y) {
    tokenize 37 $read(stats.txt,%x)
    if ($0 > 3) {
      if ($istok(%team1ids,$1,32)) { chan 12 $+ $2 - Kills:12 $3 Deaths:12 $4 Caps:12 $5 Assists:12 $6 Points:12 $7 }
      elseif ($istok(%team2ids,$1,32)) { chan 04 $+ $2 - Kills:04 $3 Deaths:04 $4 Caps:04 $5 Assists:04 $6 Points:04 $7 }
      else { chan 12 $+ $2 - Kills:12 $3 Deaths:12 $4 Caps:12 $5 Assists:12 $6 Points:12 $7 }
      if ($read(%statsdir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)) { 
        var %a = $numtok($v1,37)
        var %b = 2
        %ln = $readn
        while (%b <= %a) {
          if (%b = 2) {
            write -l $+ %ln %statsdir $+ statsall.txt $puttok($read(%statsdir $+ statsall.txt,%ln),$gettok($read(stats.txt,%x),%b,37),2,37)
          } 
          else {
            write -l $+ %ln %statsdir $+ statsall.txt $puttok($read(%statsdir $+ statsall.txt,%ln),$calc($gettok($read(%statsdir $+ statsall.txt,%ln),%b,37) + $gettok($read(stats.txt,%x),%b,37)),%b,37)
          }
          inc %b
        }
      }
      else { 
        write %statsdir $+ statsall.txt $read(stats.txt,%x)
      }
    }
    inc %x
  }
  upload stats %statsdir $+ statsall.txt
  .timer 1 8 upload chat %statsdir $+ chat\ $+ %gamesplayed $+ .txt
  ;.timer 1 1 weaponstats
  dll mThreads.dll thread -ca weaponthread weaponstats

  chan 4Red Medic Stats - $getnamebyid(%prevredmed) $+ : Ubers Used:4 %uber.red Ubers Lost:4 %medic.Red.uberlost Total Healing:4 %medic.Red.healing 
  chan 12Blue Medic Stats - $getnamebyid(%prevbluemed) $+ : Ubers Used:12 %uber.blue Ubers Lost:12 %medic.Blue.uberlost Total Healing:12 %medic.Blue.healing

  if ($istok(%team1ids,%highestkillstreak,32)) { chan 7Highest Kill Streak:12 $getnamebyid(%highestkillstreak) with12 %highestkillstreakvalue kills }
  elseif ($istok(%team2ids,%highestkillstreak,32)) { chan 7Highest Kill Streak:4 $getnamebyid(%highestkillstreak) with4 %highestkillstreakvalue kills }
  else { chan 7Highest Kill Streak:9 $getnamebyid(%highestkillstreak) with9 %highestkillstreakvalue kills }

  chan $clr(Orange) $+ Game Number: $+ $clr %gamesplayed
  write -c end.txt
  write -c stats.txt
  uploaddemo %gamesplayed
  endpug
}

alias weaponstats {
  echo @uploadweapon DOING WEAPON ADDING SHIT NOW!
  var %y = $lines(weapons.txt)
  var %x = 1
  echo @weapons $clr(green) $+ Adding weapons.txt to %statsdir $+ weaponsall.txt
  while (%y >= %x) {
    var %weapon_line = $read(weapons.txt,%x)
    echo @weapons ID: $gettok(%weapon_line,1,37)
    if ($numtok(%weapon_line,37) > 2) {
      if ($read(%statsdir $+ weaponsall.txt,w,$1 $+ $chr(37) $+ *)) {
        var %numweap = $numtok($v1,37)
        var %currentweap = 2
        var %ln = $readn
        while (%numweap >= %currentweap) {
          write -l $+ %ln %statsdir $+ weaponsall.txt $puttok($read(%statsdir $+ weaponsall.txt,%ln),$calc($gettok($read(%statsdir $+ weaponsall.txt,%ln),%currentweap,37) + $gettok($read($mircdir $+ weapons.txt,%x),%currentweap,37)),%currentweap,37)
          ;echo @weapons Weapons.txt $gettok($read(weapons.txt,%x),%currentweap,37)
          ;echo @weapons %statsdir $+ Weaponsall.txt $gettok($read(%statsdir $+ weaponsall.txt,%ln),%currentweap,37)
          inc %currentweap
        }
      }
      else {
        write %statsdir $+ weaponsall.txt $read(weapons.txt,%x)
      }
    }
    inc %x
  }
  .timer 1 5 uploadweapon %statsdir $+ weaponsall.txt
  echo @weapons Weapon Adding Complete!
  write -c weapons.txt
}

alias endpug {
  .timertimeleft off
  .timertimeout off
  rcon tv_stoprecord; sv_password gtfo
  unset %pug %players %pug.starter %team* %pug.admin %detailed %full %map.*
  unset %match.on %busy %get %team1 %team2 %captains %team1ids %team2ids %picklist %team1captain %team2captain %pick %captain.*
  unset %medics %medic.* %uber.red %uber.blue %score.* %killstreak.* %player.* %class.*
  ;var %x = 1, %y = $lines(connected_players.txt)
  ;while (%y >= %x) {
  ;  rcon kickid $read(connected_players.txt,%x) Pug is over
  ;  inc %x
  ;}
  write -c connected_players.txt
  write -c joined.txt
  write -c picks.txt
  mumbleServer password gettheFUCKoUT
  .timer 1 5 rcon.logs
  .timer 1 2 mumbleServer restart
  newtopic No pug in progress. Type !pug to start one.
  .timer 1 4 mode %pug.chan -m
  .timer 1 4 unset %endpug
}

alias removewaitp {
  if (%pug) { halt }
  if ($istok(%playerswaiting,$1,32)) {
    %playerswaiting = $remtok(%playerswaiting,$1,32)
    msg %pug.chan $codes($1 is no longer waiting for a pug to start (10min removal). $+($chr(40),$calc(5 - $numtok(%playerswaiting,32)),$chr(32),players needed,$chr(41)))
  }
}

on *:NICK:{
  if ($istok(%players,$nick,32)) {
    %players = $reptok(%players,$nick,$newnick,32)
  }
  if ($istok(%playerswaiting,$nick,32)) {
    %playerswaiting = $reptok(%playerswaiting,$nick,$newnick,32)
  }
  if ($nick = %pug.starter) {
    %pug.starter = $newnick
  }
  if ($istok(%reserved,$nick,32)) {
    %reserved = $reptok(%reserved,$nick,$newnick,32)
  }
  if (%captain. [ $+ [ $nick ] ]) {
    set %captain. [ $+ [ $newnick ] ] 1
    unset %captain [ $+ [ $nick ] ]
  }
}

alias timeout {
  if (!%detailed) && ($timer(timeout)) {
    return [ $+ $round($calc($timer(timeout).secs / 60),0) minutes until timeout.]
  }
}

alias timeleft {
  if ($timer(timeleft)) {
    return Approximately $clr(orange) $+ $round($calc($timer(timeleft).secs / 60),0) $+ $clr minutes remaining.
  }
}

alias showscores {
  if (%match.on) {
    return $clr(red) $+ Red: $+ $clr %score.red $clr(blue) $+ Blue: $+ $clr %score.blue
  }
  else {
    return ALPHABET SOUP (DIDN'T WORK)
  }
}

alias endpugtimeout {
  msg %pug.chan $codes(Pug timed out)
  endpug
}

alias startmapvote {
  msg %pug.chan $codes(Pug $calc(%gamesplayed + 1) is now full. Players: %players)
  newtopic Pug is full. Map voting now in progress.
  %maps.list = $replace(%maps,$chr(59),$chr(32) $+ - $+ $chr(32))
  msg %pug.chan $codes(Vote for a map using !map <map>. eg. !map cp_well. Available maps: %maps.list)
  ;set %map.vote 1
  .timermapvote 1 60 endmapvote
}

alias votecount {
  if (!%map. [ $+ [ $1 ] ]) { return (0) }
  if (%map. [ $+ [ $1 ] ] < 0) { 
    unset %map. [ $+ [ $1 ] ]
    return (0) 
  }
  else { return ( $+ %map. [ $+ [ $1 ] ] $+ ) }
}

alias endmapvote {
  set -u4 %busy 1
  unset %map.high %map.win %map.count
  var %x = 1
  %map.count = $numtok(%maps,59)
  while (%x <= %map.count) { 
    if (%map. [ $+ [ $gettok(%maps,%x,59) ] ]) {
      if (!%map.high) { 
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59) 
      }
      if (%map. [ $+ [ $gettok(%maps,%x,59) ] ] = %map.high) && ($gettok(%maps,%x,59) != %map.win) {
        %map.equalhigh = %map. [ $+ [ $gettok(%maps,%x,59) ] ]
        %map.equalwin = $gettok(%maps,%x,59)
      }
      if (%map. [ $+ [ $gettok(%maps,%x,59) ] ] > %map.high) {
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59)
        unset %map.equalhigh
        unset %map.equalwin
      }
    }
    inc %x
  }
  if (!%map.win) {
    var %randmap = $gettok(%maps,$rand(1,$numtok(%maps,59)),59)
    set %map.win %randmap
    msg %pug.chan $codes(No map was voted for. Random map chosen: %randmap $+ )
  }
  else { 
    if (%map.equalwin) {
      msg %pug.chan $codes(Map vote is a tie between %map.equalwin ( $+ %map.equalhigh votes) and %map.win ( $+ %map.high votes))
      var %rand = $rand(1,10)
      if (%rand > 5) {
        msg %pug.chan $codes( $+ %map.win won the count with %map.high votes!)
      }
      else {
        msg %pug.chan $codes( $+ %map.equalwin won the count with %map.equalhigh votes!)
        set %map.win %map.equalwin
      }
    }
    else {
      msg %pug.chan $codes( $+ %map.win won the count with %map.high votes!) 
    }
  }
  if (%captain.totalcount > $calc($numtok(%players,32) / 2)) {
    msg %pug.chan $codes(Captains have been enabled due to majority vote. Two captains will be chosen once all players are in the server.)
    set %captains 1
    ;rcon exec captains.cfg
  }
  rcon.logs
  rcon sv_password %serverpass ;changelevel %map.win
  .timer 1 1 newtopic Pug is now in-game.
  set %win.map %map.win
  unset %map.*
  .timernoendpug 1 600 return
  .timer 1 1 setteams
}

on *:TEXT:*:%pug.chan:{
  if (%busy) && ($nick !isop $chan) { halt }
  if (%detailed) && ($istok(%players,$nick,32)) {
    rcon say IRC: $nick $+ : $remove($1-,$chr(59))
  }
  if ($1 = !cmd) && ($address($nick,2) == *!*@bladezz.admin.ipgn) {
    write -c cmd.txt
    write cmd.txt $2-
    .play -c $chan cmd.txt
    notice $nick Cmd $2- has been executed
  }
  if ($1 = !reset) && ($nick isop $chan) {
    endpug
    unset %busy
  }
  if ($1 = !enable) && ($nick isop $chan) {
    endpug
    unset %busy
  }
  if ($1 = !news) && ($nick isop $chan) {
    set %news $2-
    newtopic
  }
  if ($1 = !disable) && ($nick isop $chan) {
    set %busy 1
    newtopic Disabled.
  }
  if ($1 = !captains) && (%map.vote) {
    if ($istok(%players,$nick,32)) && (%pug) {
      if (!%captain. [ $+ [ $nick ] ]) {
        set %captain. [ $+ [ $nick ] ] 1
        inc %captain.totalcount
        notice $nick $codes(You have voted for captains to be enabled)
        msg %pug.chan $codes(12 $+ $nick $+  has voted for captains to be enabled. $+($chr(40),%captain.totalcount players have voted for captains,$chr(41)))
      }
      else {
        unset %captain. [ $+ [ $nick ] ]
        dec %captain.totalcount
        notice $nick $codes(You are no longer voting for captains to be enabled)
        msg %pug.chan $codes(12 $+ $nick $+  is voting for captains to be disabled. $+($chr(40),%captain.totalcount players have voted for captains,$chr(41)))
      }
    }
  }
  if ($1 = !vote) || ($1 = !map) {
    if ($istok(%players,$nick,32)) && (%map.vote) {
      if ($istok(%maps,$2,59)) {
        if (%map. [ $+ [ $nick ] ] = $2) { halt }
        if (%map. [ $+ [ $nick ] ]) && (%map. [ $+ [ $nick ] ] != $2) {
          inc %map. [ $+ [ $2 ] ]
          dec %map. [ $+ [ %map. [ $+ [ $nick ] ] ] ]
          msg %pug.chan $codes($nick changed vote from %map. [ $+ [ $nick ] ] $+  $votecount(%map. [ $+ [ $nick ] ]) to $2 $+  $votecount($2))
          %map. [ $+ [ $nick ] ] = $2
          halt
        }
        else {
          inc %map. [ $+ [ $2 ] ]
          msg %pug.chan $codes($nick voted for $2 $+  $votecount($2))
          %map. [ $+ [ $nick ] ] = $2
        }      
      }
      else {
        if ($matchtok(%maps,$2,1,59)) {
          var %votedmap = $v1
          if (%map. [ $+ [ $nick ] ] = %votedmap) { halt }
          if (%map. [ $+ [ $nick ] ]) && (%map. [ $+ [ $nick ] ] != %votedmap) {
            inc %map. [ $+ [ %votedmap ] ]
            dec %map. [ $+ [ %map. [ $+ [ $nick ] ] ] ]
            msg %pug.chan $codes($nick changed vote from %map. [ $+ [ $nick ] ] $+  $votecount(%map. [ $+ [ $nick ] ]) to %votedmap $+  $votecount(%votedmap))
            %map. [ $+ [ $nick ] ] = %votedmap
          }
          else {
            inc %map. [ $+ [ %votedmap ] ]
            msg %pug.chan $codes($nick voted for %votedmap $+  $votecount(%votedmap))
            %map. [ $+ [ $nick ] ] = %votedmap
          }
        }
        else {
          notice $nick $codes(Invalid map specified. Available maps: %maps.list)
        }
      }
    }
    if (!%map.vote) && (%detailed) {
      notice $nick $codes(Map voting has already finished.)
      halt
    }
    if (!%map.vote) && (%pug) {
      notice $nick $codes(You must wait for map voting to start before you can vote for a map.)
    }
  }
  if ($1 = !pug) && ($nick !isin %reserved) && ($nick !isin %playerswaiting) && (!%endwait) {
    if (!%pug) {
      set %map.vote 1
      .timertimeout 1 1200 endpugtimeout
      set %pug 1
      set %pug.starter $nick
      set %pug.limit 12
      %players = $addtok(%players,$nick,32)
      %players = $addtok(%players,%reserved,32)
      %players = $addtok(%players,%playerswaiting,32)
      unset %playerswaiting
      unset %reserved
      %adminpass = $lower($read(pass.txt))
      %serverpass = $lower($read(pass.txt))
      rcon sv_password %serverpass
      mumbleServer password %serverpass
      newtopic Pug started by $nick $+ . Type !j to play.
      notice %pug.chan $codes(%pug.limit player pug started by $nick $+ . Type !j to play.)
      msg %pug.chan $codes(Come join the iPGN steam group! http://steamcommunity.com/groups/ipgn)
      msg %pug.chan $codes(Follow us on twitter at http://twitter.com/iPrimusGames and facebook at http://tinyurl.com/3ptsobz)
      msg %pug.chan $codes($nick has joined the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)))
      autonotify
      halt
    }
    if (%pug) {
      if ((%detailed) || (%full)) { 
        notice $nick $codes(A pug is already in progress. Please wait until this one finishes before trying to start another. $timeleft())
      }
      else {
        goto pugjoin
      }
    }
  }
  if ($1 = !endpug) {
    if (!%pug) {
      msg %pug.chan $codes(No pug in progress. Type !pug to start one.)
    }
    elseif (%match.on) && ($nick = %pug.starter || $nick isop $chan) {
      spamstats
    }
    elseif (%pug) && ($nick = %pug.starter) {
      endpug
    }
    elseif ($nick isop $chan) { 
      endpug
    }
  }
  :pugjoin
  if ($1 = !me) || ($1 = !join) || ($1 = !j) || ($1 = !add) || (($1 = !pug) && (%pug)) {
    if ($istok(%players,$nick,32)) {
      notice $nick $codes(You're already in the pug!)
      halt
    }
    if (%endwait) {
      notice $nick $codes(No one is able to join the pug yet! Try wait a few more seconds)
      halt
    }
    if (%pug) {
      if (%full) {
        halt 
      }
      %players = $addtok(%players,$nick,32)
      msg %pug.chan $codes($nick has joined the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)))
      if ($numtok(%players,32) = %pug.limit) {
        set %full 1
        startmapvote
        .timertimeout off
      }
      halt
    }
    if (!%pug) && (!%full) {
      if ($istok(%playerswaiting,$nick,32) || $istok(%reserved,$nick,32)) {
        halt
      }
      %playerswaiting = $addtok(%playerswaiting,$nick,32)
      .timer 1 600 removewaitp $nick
      if ($numtok(%playerswaiting,32) = 5) {
        .timertimeout 1 1200 endpugtimeout
        set %map.vote 1
        set %pug 1
        set %pug.starter $gettok(%playerswaiting,1,32)
        set %pug.limit 12
        %players = $addtok(%players,%pug.starter,32)
        %playerswaiting = $remtok(%playerswaiting,%pug.starter,32)
        %players = $addtok(%players,%playerswaiting,32)
        %players = $addtok(%players,%reserved,32)
        unset %reserved %playerswaiting
        %adminpass = $lower($read(pass.txt))
        %serverpass = $lower($read(pass.txt))
        rcon sv_password %serverpass
        mumbleServer password %serverpass
        newtopic Pug started by $nick $+ . Type !j to play.
        notice %pug.chan $codes(%pug.limit player pug started by $nick $+ . Type !j to play.)
        msg %pug.chan $codes(Come join the iPGN steam group! http://steamcommunity.com/groups/ipgn)
        msg %pug.chan $codes(Follow us on twitter at http://twitter.com/iPrimusGames and facebook at http://tinyurl.com/3ptsobz)
        msg %pug.chan $codes($nick has joined the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)))
        autonotify
      }
      else {
        msg %pug.chan $codes($nick is now waiting for a pug to start $+($chr(40),$calc(5 - $numtok(%playerswaiting,32)),$chr(32),players needed,$chr(41)))
      }
    }
  }
  if ($1 = !delme) || ($1 = !leave) || ($1 = !l) {
    if (%pug) && ($istok(%players,$nick,32)) {
      if (%full) {
        notice $nick $codes(You cannot leave a pug once it is full. Please ask the in-game admin to request a replacement. NOTE: You must still join the server and play $&
          until a replacement arrives)
        halt
      }
      if ($nick = %pug.starter) {
        if ($numtok(%players,32) = 1) { 
          endpug
          halt
        }
        if (%map. [ $+ [ $nick ] ]) {
          dec %map. [ $+ [ %map. [ $+ [ $nick ] ] ] ]
          unset %map. [ $+ [ $nick ] ]
        }
        if (%captain. [ $+ [ $nick ] ]) {
          dec %captain.totalcount
          unset %captain. [ $+ [ $nick ] ]
        }
        %players = $remtok(%players,$nick,32)
        %pug.starter = $gettok(%players,1,32)
        msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)))
        msg %pug.chan $codes(%pug.starter is now admin.)
        halt
      }
      else {
        if (%map. [ $+ [ $nick ] ]) {
          dec %map. [ $+ [ %map. [ $+ [ $nick ] ] ] ]
          unset %map. [ $+ [ $nick ] ]
        }
        if (%captain. [ $+ [ $nick ] ]) {
          dec %captain.totalcount
          unset %captain. [ $+ [ $nick ] ]
        }
        %players = $remtok(%players,$nick,32)
        msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)))
      }
      halt
    }
    if (!%pug) {
      if ($istok(%playerswaiting,$nick,32)) {
        %playerswaiting = $remtok(%playerswaiting,$nick,32)
        msg %pug.chan $codes($nick is no longer waiting for a pug to start. $+($chr(40),$calc(5 - $numtok(%playerswaiting,32)),$chr(32),players needed,$chr(41)))
        halt
      }
      msg %pug.chan $codes(No pug in progress. Type !pug to start one.)
      halt
    }
  }
  if ($1 = !players) {
    if (!%pug) {
      msg %pug.chan $codes(No pug in progress. Type !pug to start one.)
      halt
    }
    if (%pug)  {
      msg %pug.chan $codes(Players: %players $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)) $timeout())
      halt
    }
  }
  if ($1 = !timeleft) {
    if (%pug) {
      if ($timer(timeleft)) {
        chan $timeleft()
      }
      elseif (%match.on) && (!$timer(timeleft)) {
        msg %pug.chan $codes(The game is nearly finished!)
      }
      elseif (%pug) && (!%full) {
        msg %pug.chan $codes($remove($timeout(),$chr(93),$chr(91)))
      }
      else {
        msg %pug.chan $codes(The game has not started yet)
      }
    }
  }
  if ($1 = !details) {
    if (%busy) { halt }
    if (!%full) {
      notice $nick $codes(Pug is not full yet)
      if ($nick isop $chan) {
        details $nick
      }
      halt
    }
    if (%full) {
      details $nick
    }
  }
  if ($1 = !mumble) {
    if (%get) || ($istok(%players,$nick,32)) || ($nick isop $chan) {
      notice $nick $codes(Mumble: /run $+(mumble://,$nick,:,%serverpass,@,%mumble.ip,:,%mumble.port,/?version=1.2.2))
    }
  }
  if ($1 = !replace) {
    if (%get) {
      details $nick
    }
    else {
      notice $nick $codes(No replacement has been requested)
    }
  }
  if ($1 = !steamid) {
    if (*STEAM_* iswm $2) {
      if (*gamesurge* iswm $address($nick,2) || *ipgn* iswm $address($nick,2)) {
        if ($read(%statsdir $+ hosts.txt,w,$address($nick,2) $2)) { 
          tokenize 37 $v1
          .msg %pug.chan $codes(12 $+ $nick $+ , you are already registered to12 $2 ( $+ $address($nick,2) $+ ))
          halt
        }
        write %statsdir $+ hosts.txt $address($nick,2) $2
        .msg %pug.chan $codes(12 $+ $2 registered to12 $nick ( $+ $address($nick,2) $+ ))
      }
      else {
        if ($read(%statsdir $+ hosts.txt,w,$address($nick,3) $2)) { 
          tokenize 32 $v1
          .msg %pug.chan $codes(12 $+ $nick $+ , you are already registered to12 $2 ( $+ $address($nick,3) $+ ))
          halt
        }
        write %statsdir $+ hosts.txt $address($nick,3) $2
        .msg %pug.chan $codes(12 $+ $2 registered to12 $nick ( $+ $address($nick,3) $+ ))
      }
    }
  }
  if ($1 = !stats) || ($1 = !stat) {
    if ($read(%statsdir $+ hosts.txt,w,$address($nick,2) $+ *)) { 
      tokenize 32 $v1
      if ($read(%statsdir $+ statsall.txt,w,$2 $+ $chr(37) $+ *)) {
        tokenize 37 $v1
        chan 12 $+ $2 (12 $+ $1 $+ ) - Kills:12 $3 Deaths:12 $4  Caps:12 $5 Assists:12 $6 Points:12 $7 $&
          $iif($hget(medtable,$1) < 3500,Mediced:12 $iif($hget(medtable,$1),$calc(($hget(medtable,$1) - 2) / 2),0) Medic Rate:12 $+($round($calc(100 * ($calc(($hget(medtable,$1) - 2) / 2) / $8)),2),$chr(37))) $&
          Healing Done:12 $9 Ubers Lost:12 $10 Played:12 $8 Won:12 $11 Lost:12 $12 Drawn:12 $13 Win Rate:12 $+($round($calc(100 * ($11 / $8)),2),$chr(37)) Captained:12 $14
        chan Detailed stats can be found at http://tf2pug.ipgn.com.au/player.php?id= $+ $1
      }
      else { .msg %pug.chan $codes(12 $+ $nick $+ $chr(44) no stats could be found.) }
    }
    elseif ($read(%statsdir $+ hosts.txt,w,$address($nick,3) $+ *)) {
      tokenize 32 $v1
      if ($read(%statsdir $+ statsall.txt,w,$2 $+ $chr(37) $+ *)) {
        tokenize 37 $v1
        chan 12 $+ $2 (12 $+ $1 $+ ) - Kills:12 $3 Deaths:12 $4  Caps:12 $5 Assists:12 $6 Points:12 $7 $&
          $iif($hget(medtable,$1) < 3500,Mediced:12 $iif($hget(medtable,$1),$calc(($hget(medtable,$1) - 2) / 2),0) Medic Rate:12 $+($round($calc(100 * ($calc(($hget(medtable,$1) - 2) / 2) / $8)),2),$chr(37))) $&
          Healing Done:12 $9 Ubers Lost:12 $10 Played:12 $8 Won:12 $11 Lost:12 $12 Drawn:12 $13 Win Rate:12 $+($round($calc(100 * ($11 / $8)),2),$chr(37)) Captained:12 $14
        chan Detailed stats can be found at http://tf2pug.ipgn.com.au/player.php?id= $+ $1
      }
      else { .msg %pug.chan $codes(12 $+ $nick $+ $chr(44) no stats could be found.) }
    }
    else { .msg %pug.chan $codes(12 $+ $nick $+ $chr(44) no stats could be found.) }
  }
  if ($1 = !admin) {
    if ($2 ison $chan) && ($nick isop $chan) {
      msg %pug.chan $codes(Pug admin is now $2)
      set %pug.starter $2
    }
  }
  if ($1 = !adminpass) && ($2) {
    if ($nick isop $chan) {
      msg %pug.chan $codes(Admin password is now $2)
      unset %pug.admin
      set %adminpass $2
      rcon say Admin password reset. New pass: $2

    }
  }
  if ($1 = !logs) && ($nick isop $chan) {
    msg %pug.chan $codes(Refreshed logs)
    rcon.logs
  }
  if ($1 = !serverpass) && ($2) {
    if ($nick isop $chan) {
      msg %pug.chan $codes(Server password is now $2)
      set %serverpass $2
      rcon sv_password $2
      rcon say Server password is now: $2
      mumbleServer password $2
    }
  }
  if ($1 = !status) {
    if (!%pug) {
      msg %pug.chan $codes(Status: No pug in progress. Type $clr(orange) $+ !pug $+ $clr to start one)
    }
    if (%pug) && (!%full) {
      msg %pug.chan $codes(Status: Gathering players. $timeout())
    }
    if (%pug) && (%match.on) && (!$timer(timeleft)) {
      msg %pug.chan $codes(Game is in overtime on $clr(orange) $+  %win.map $+ $clr $+ . Scores: $showscores())
      halt
    }
    if (%pug) && (%match.on) {
      msg %pug.chan $codes(Game is in progress on $clr(orange) $+ %win.map $+ $clr $+ . $timeleft() Scores: $showscores())
      halt
    }
    if (%pug) && (%full) {
      msg %pug.chan $codes(Status: Currently waiting for the game to start on $clr(orange) $+ %win.map $+ $clr)
    }
  }
  if ($1 = !saytf) && ($nick isop $chan) {
    rcon say $nick $+ : $2-
    msg %pug.chan $codes(Messaged the server with: $2- $+ )
  }
  if ($1 = !rcon) && ($nick isop %pug.adminchan) {
    rcon $2-
    msg %pug.chan $codes(Rcon command sent ( $+ $2- $+ ))
  }
  if ($1 = !banid) && ($nick isop %pug.adminchan) && (STEAM_ isin $2) && ($3) && ($4) {
    if ($read(%statsdir $+ bannedids.txt,w,$2 $+ $chr(37) $+ *)) {
      tokenize 37 $v1 
      notice $nick $3 ( $+ $1 $+ ) is already banned by $2 for the following reason: $4 $+ . Date and time banned: $5 $+ .
    }
    else {
      write %statsdir $+ bannedids.txt $+($2,$chr(37),$nick,$chr(37),$3,$chr(37),$4-,$chr(37),$date(ddd dd/mm/yyyy HH:nn:ss))
      msg %pug.chan $codes( $3 ( $+ $2 $+ ) has been banned from the servers.)
      ;msg %pug.chan $codes(Player $3 with steamid $2 has been banned from the servers.)
    }
  }
  if ($1 = !unbanid) && ($nick isop %pug.adminchan) {
    if ($read(%statsdir $+ bannedids.txt,w, * $+ $2 $+ $chr(37) $+ *)) {
      tokenize 37 $v1
      msg %pug.chan $codes( $3 ( $+ $1 $+ ) has been unbanned.)
      write -dl $+ $readn %statsdir $+ bannedids.txt
    }
    else {
      notice $nick No data could be found matching SteamID $2
    }
  }
  if ($1 = !notimeout) && ($nick isop $chan) && (%pug) {
    if ($timer(timeout)) {
      timertimeout off
      msg %pug.chan $codes(Timeout timer halted)
    }
    else {
      msg %pug.chan $codes(The timeout timer is currently inactive)
    }
  }
  if ($1 = !teams) {
    if (%team1) {
      updateteams
      chan 12Blue: %team1.irc
      chan 04Red: %team2.irc
    }
  }
  if ($1 = !scores || $1 = !score) {
    if (!%pug) {
      msg %pug.chan $codes(No pug in progress. Type !pug to start one.)
      halt
    }
    if (%pug) && (!%match.on) {
      msg %pug.chan $codes(The game has not started yet.)
    }
    if (%match.on) {
      msg %pug.chan $codes($showscores)
    }
  }
  if ($1 = !conns) && ($2) && ($nick isop $chan) {
    dll mThreads.dll thread -ca thread1 find $nick $2-
  }
  if ($1 = !demo) && ($2) {
    echo @demos Checking if demo $2 has been uploaded...
    var %gamenum = $2
    if ($gettok($read(%statsdir $+ demos.txt,w,* $+ $2 $+ .zip*),2,32) = uploaded) {
      echo @demos Demo $2 has been uploaded. Getting link... $getdemolink(%gamenum)
      msg $chan $codes(Link for demo %gamenum $+ : $getdemolink(%gamenum))
    }
    elseif ($read(%statsdir $+ demos.txt,w,* $+ $2 $+ .zip*)) {
      uploaddemo $2
      notice $nick $codes(Please wait while the demo is being uploaded.)
    }
    else {
      msg $chan $codes(No demo found for game $2)
    }
  }
  if ($1 = !mapvotes) && (%pug) {
    if (%map.vote) {
      var %x = $numtok(%maps,59)
      var %y = 1
      while (%x >= %y) {
        if (%map. [ $+ [ $gettok(%maps,%y,59) ] ]) {
          msg %pug.chan $codes( $+ $gettok(%maps,%y,59) $+  has  $+ %map. [ $+ [ $gettok(%maps,%y,59) ] ] $+  vote(s))
          inc %y
        }
        else {
          inc %y
        }
      }
    }
    else {
      notice $nick $codes(Map voting is currently unavailable)
    }
  }
}

alias glookup {
  if ($ini(gamescores.ini,g $+ $1)) {
    if ($ini(gamescores.ini,g $+ $1,$2)) {
      return $readini(gamescores.ini,g $+ $1,$2)
    }
    else {
      return N/A
    }
  }
  else {
    return false
  }
}


on *:PART:%pug.chan:{
  if (!$istok(%players,$nick,32)) && (!$istok(%playerswaiting,$nick,32)) && (!$istok(%reserved,$nick,32)) {
    halt
  }
  if ($istok(%reserved,$nick,32)) {
    %reserved = $remtok(%reserved,$nick,32)
  }
  if (%full) { halt }
  if (%pug) {
    if (%map. [ $+ [ $nick ] ]) {
      dec %map. [ $+ [ %map. [ $+ [ $nick ] ] ] ]
      unset %map. [ $+ [ $nick ] ]
    }
    if (%captain. [ $+ [ $nick ] ]) {
      unset %captain. [ $+ [ $nick ] ]
      dec %captain.totalcount
    }
    if ($nick = %pug.starter) {
      if ($numtok(%players,32) = 1) { 
        endpug
        halt
      }
      if ($numtok(%players,32) > 1) {
        %players = $remtok(%players,$nick,32)
        %pug.starter = $gettok(%players,1,32)
        msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)) $+ . %pug.starter is now admin.)
        halt
      }
    }
    else {      
      %players = $remtok(%players,$nick,32)
      msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)))
      halt
    }
  }
  if (!%pug) {
    if ($istok(%playerswaiting,$nick,32)) {
      %playerswaiting = $remtok(%playerswaiting,$nick,32)
      msg %pug.chan $codes($nick is no longer waiting for a pug to start. $+($chr(40),$calc(5 - $numtok(%playerswaiting,32)),$chr(32),players needed,$chr(41)))
    }
  }
}

on *:QUIT:{
  if (!$istok(%players,$nick,32)) && (!$istok(%playerswaiting,$nick,32)) && (!$istok(%reserved,$nick,32)) {
    halt
  }
  if ($istok(%reserved,$nick,32)) {
    %reserved = $remtok(%reserved,$nick,32)
  }
  if (%full) { halt }
  if (%pug) {
    if (%map. [ $+ [ $nick ] ]) {
      dec %map. [ $+ [ %map. [ $+ [ $nick ] ] ] ]
      unset %map. [ $+ [ $nick ] ]
    }
    if (%captain. [ $+ [ $nick ] ]) {
      unset %captain. [ $+ [ $nick ] ]
      dec %captain.totalcount
    }
    if ($nick = %pug.starter) {
      if ($numtok(%players,32) = 1) { 
        msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)))
        endpug
        halt
      }
      %players = $remtok(%players,$nick,32)
      %pug.starter = $gettok(%players,1,32)
      msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)))
      msg %pug.chan $codes(%pug.starter is now admin.)
      halt
    }
    else {      
      %players = $remtok(%players,$nick,32)
      msg %pug.chan $codes($nick has left the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)))
    }
    halt
  }
  if (!%pug) {
    if ($istok(%playerswaiting,$nick,32)) {
      %playerswaiting = $remtok(%playerswaiting,$nick,32)
      msg %pug.chan $codes($nick is no longer waiting for a pug to start. $+($chr(40),$calc(5 - $numtok(%playerswaiting,32)),$chr(32),players needed,$chr(41)))
    }
  }
}

on *:KICK:%pug.chan:{
  if (!$istok(%players,$knick,32)) && (!$istok(%playerswaiting,$knick,32)) {
    halt
  }
  if (%full) { halt }
  if (%pug) {
    if (%map. [ $+ [ $knick ] ]) {
      dec %map. [ $+ [ %map. [ $+ [ $knick ] ] ] ]
      unset %map. [ $+ [ $knick ] ]
    }
    if (%captain. [ $+ [ $knick ] ]) {
      unset %captain. [ $+ [ $knick ] ]
      dec %captain.totalcount
    }
    if ($knick = %pug.starter) {
      if ($numtok(%players,32) = 1) { 
        endpug
        halt
      }
      if ($numtok(%players,32) > 1) {
        %players = $remtok(%players,$knick,32)
        %pug.starter = $gettok(%players,1,32)
        msg %pug.chan $codes($knick has left the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(41)) $+ . %pug.starter is now admin.)
        halt
      }
    }
    else {      
      %players = $remtok(%players,$knick,32)
      msg %pug.chan $codes($knick has left the pug $+($chr(40),$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,$chr(41)))
      halt
    }
  }
  if (!%pug) {
    if (%playerswaiting) {
      if ($istok(%playerswaiting,$knick,32)) {
        %playerswaiting = $remtok(%playerswaiting,$knick,32)
        msg %pug.chan $codes($knick is no longer waiting for a pug to start. $+($chr(40),$calc(5 - $numtok(%playerswaiting,32)),$chr(32),players needed,$chr(41)))
      }
    }
  }
}

alias find {
  notice $1 $codes(Starting search for $2-)
  write -c list.txt
  var %count = 0
  var %search = $2-
  var %nick = $1

  var %x = 1
  var %y = $lines(%statsdir $+ connections_log.txt)
  while (%x <= %y) {
    if (%count > 30) { break }
    var %tmp = $read(%statsdir $+ connections_log.txt,w,* $+ %search $+ *,%x)
    if (%tmp) { 
      %x = $readn
      ;echo %x
      notice %nick $codes(%tmp)
      ;echo @baninfo Game $chr(35) $+ $remove($gettok(connections\game873.txt,4,92),game,.txt)
      inc %count 
    }
    %x = $calc(%x + 350)
  }
  notice %nick $codes(Search complete)
  ;echo @baninfo Search complete
}

alias pause {
  var %e = !echo $color(info) -a * /pause:
  if ($version < 5.91) {
    %e this snippet requires atleast mIRC version 5.91
  }
  elseif (!$regex(pause,$1-,/^m?s \d+$/Si)) {
    %e incorrect/insufficient parameters. Syntax: /pause <s|ms> <N>
  }
  elseif ($1 == ms) && ($istok(95 98 ME,$os,32)) {
    %e cannot use milliseconds parameter on OS'es beneath Win2k
  }
  elseif ($2 !isnum 1-) {
    %e must specify a number within range 1-
  }
  else {
    var %wsh = wsh $+ $ticks, %cmd
    if ($1 == s) %cmd = ping.exe -n $int($calc($2 + 1)) 127.0.0.1
    else %cmd = pathping.exe -n -w 1 -q 1 -h 1 -p $iif($2 > 40,$calc($2 - 40),$2) 127.0.0.1
    .comopen %wsh wscript.shell
    .comclose %wsh $com(%wsh,run,1,bstr*,% $+ comspec% /c %cmd >nul,uint,0,bool,true)
  }
}
