on *:NOTICE:*:*:{
  if (*I recognize you* iswm $1-) {
    if ($me != [iPGN-Brink]) {
      msg AuthServ@Services.GameSurge.net ghost [iPGN-Brink]
      .timer 1 10 nick [iPGN-Brink]
    }
    .timer 1 10 join %pug.chan
    ;.timer 1 10 join %pug.adminchan
    .timer 1 10 join #ausbrink
    .timer 1 10 join #tf2pug
    ;.timer 1 10 join %pug.killchan
    .timer 0 2400 msg %pug.chan $codes(Come join the iPGN steam group! http://steamcommunity.com/groups/ipgn)
  }
}

on *:START:{
  window -e @rcon
  window -e @upload
  window -e @debug
}

alias codes {
  return 12� $1- 12�
}

alias pinkcodes {
  return 13� $1- 13�
}

alias autonotify {
  if (!$1) { return }
  if (!$timer(autonotify [ $+ [ $1 ] ])) {
    .timerautonotify $+ $1 5 240 autonotify $1
  }
  elseif ($istok(%pug.fullids,$1,32)) {
    .timerautonotify $+ $1 off
  }
  else {
    msg %pug.chan $codes(Pug $chr(35) $+ $1 needs more players! Type !j to join. $slotsremaining($1))
    msg #ausbrink $strip($codes(Pug $chr(35) $+ $1 needs more players! Type !j in 7#brinkpug.au to join. $slotsremaining($1)))
    msg #tf2pug $codes(iPGN Brink Pug $chr(35) $+ $1 needs more players! Type !j in 7#brinkpug.au to join. $slotsremaining($1))
  }
}

alias timeleft {
  return $iif($timer(timeleft [ $+ [ $1 ] ]),Approximately $round($calc($timer(timeleft [ $+ [ $1 ] ]).secs / 60),0) minutes left,The game will be finished soon)
}

alias votes {
  if (!%map. [ $+ [ $1 ] ]) { return (0) }
  if (%map. [ $+ [ $1 ] ] < 0) { 
    unset %map. [ $+ [ $1 ] ]
    return (0) 
  }
  else { return ( $+ %map. [ $+ [ $1 ] ] $+ ) }
}

alias newtopic {
  if ($1) {
    set %status $1-
  }
  .topic %pug.chan 12,0� 1i14,0PGN Brink Pug! 1,0http://brinkweb.12ipgn1.com.au/ 12�1 N14ews: %news 12�1 S14tatus: %status 12�'
  ;.topic %pug.killchan 12,0� 1i14,0PGN Brink Pug! 1,0http://brinkweb.12ipgn1.com.au/ 12�1 S14tatus: %status 12�
}

alias dopugtopic {
  newtopic Total pugs: $np() $iif(%pug.openids,LFM: $numtok(%pug.openids,32)) $iif(%pug.fullids,In-progress: $numtok(%pug.fullids,32))
}

alias massdetails {
  .timertimeout off
  set %pug. [ $+ [ $1 ] $+ .detailed ] 1
  msg %pug.chan $codes(Players in pug $chr(35) $+ $1 are now receiving details)
  msg %pug.chan $codes(Pug admin is %pug. [ $+ [ $1 ] $+ .starter ])
  set -u3 %busy 1
  var %v = $numtok(%pug. [ $+ [ $1 ] $+ .players ],32)
  var %u = 1
  while (%u <= %v) {
    var %nick = $gettok(%pug. [ $+ [ $1 ] $+ .players ],%u,32)
    if (%nick = %pug.starter) || (%nick isop %pug.chan) {
      notice %nick $codes(Server details: connect $+(%pug. [ $+ [ $1 ] $+ .ip ],:,%pug. [ $+ [ $1 ] $+ .port ],;) password %pug. [ $+ [ $1 ] $+ .pass ])
      ;notice %nick $codes(Mumble: /run $+(mumble://,%nick,:,%serverpass,@,%mumble.ip,:,%mumble.port,/?version=1.2.2))
    }
    else {
      notice %nick $codes(Server details: connect $+(%pug. [ $+ [ $1 ] $+ .ip ],:,%pug. [ $+ [ $1 ] $+ .port ],;) password %pug. [ $+ [ $1 ] $+ .pass ])
      ;notice %nick $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
      ;notice %nick $codes(Mumble: /run $+(mumble://,%nick,:,%serverpass,@,%mumble.ip,:,%mumble.port,/?version=1.2.2))
    }
    inc %u
  }
}

alias details {
  if (!%busy) && (!%map.vote) {
    if ($2 isop %pug.chan) || ($2 = %pug.starter) {
      notice $2 $codes(Server details: connect $+(%pug. [ $+ [ $1 ] $+ .ip ],:,%pug. [ $+ [ $1 ] $+ .port ],;) password %pug. [ $+ [ $1 ] $+ .pass ])
      ;notice $1 $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
      ;notice $1 $codes(Mumble: /run $+(mumble://,$1,:,%serverpass,@,%mumble.ip,:,%mumble.port,/TF2Pug?version=1.2.0))
      return
    }
    if ($istok(%pug.allplayers,$2,32)) {
      notice $2 $codes(Server details: connect $+(%pug. [ $+ [ $1 ] $+ .ip ],:,%pug. [ $+ [ $1 ] $+ .port ],;) password %pug. [ $+ [ $1 ] $+ .pass ])
      ;notice $1 $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
      ;notice $1 $codes(Mumble: /run $+(mumble://,$1,:,%serverpass,@,%mumble.ip,:,%mumble.port,/TF2Pug?version=1.2.0))
      return
    }
  }
  if (!%busy) && ($2 isop $chan) {
    notice $2 $codes(Server details: connect $+(%pug. [ $+ [ $1 ] $+ .ip ],:,%pug. [ $+ [ $1 ] $+ .port ],;) password %pug. [ $+ [ $1 ] $+ .pass ])
    ;notice $1 $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
    ;notice $1 $codes(Mumble: /run $+(mumble://,$1,:,%serverpass,@,%mumble.ip,:,%mumble.port,/TF2Pug?version=1.2.0))
  }
}

on *:NICK:{
  if ($istok(%pug.allplayers,$nick,32)) {
    %pug.allplayers = $reptok(%pug.allplayers,$nick,$newnick,32)
    if ($istok(%pug. [ $+ [ $gettok(%pug.openids,1,32) ] $+ .players ],$nick,32)) {
      %pug. [ $+ [ $gettok(%pug.openids,1,32) ] $+ .players ] = $reptok(%pug. [ $+ [ $gettok(%pug.openids,1,32) ] $+ .players ],$nick,$newnick,32)
    }
  }
}

on *:JOIN:%pug.chan:{
  if (%pug.openids) {
    notice $nick $codes(There's currently a pug open $slotsremaining(%pug.openids) Type 7!j to join)
  }
}

on *:PART:%pug.chan:{
  if ($istok(%pug.allplayers,$nick,32)) {
    if ($istok(%pug. [ $+ [ $gettok(%pug.openids,1,32) ] $+ .players ],$nick,32)) {
      delplayer $nick
    }
  }
}

on *:QUIT:{
  if ($istok(%pug.allplayers,$nick,32)) {
    if ($istok(%pug. [ $+ [ $gettok(%pug.openids,1,32) ] $+ .players ],$nick,32)) {
      delplayer $nick
    }
  }
}

on *:KICK:%pug.chan:{
  if ($istok(%pug.allplayers,$knick,32)) {
    if ($istok(%pug. [ $+ [ $gettok(%pug.openids,1,32) ] $+ .players ],$knick,32)) {
      delplayer $knick
    }
  }
}

on *:TEXT:!*:%pug.chan:{
  if ($nick isop $chan) {
    if ($1 = !rcon) && ($address($nick,2) == *!*@bladezz.admin.ipgn) {
      rcon $2 $3 $4 $5-
    }
    if ($1 = !news) {
      set %news $2-
      newtopic
    }
    if ($1 = !cmd) && ($address($nick,2) == *!*@bladezz.admin.ipgn) {
      write -c cmd.txt
      write cmd.txt $2-
      .play -c $chan cmd.txt
      notice $nick Cmd $2- has been executed
    }
    if ($1 = !endpug) {
      if ($2) && (($istok(%pug.openids,$2,32)) || ($istok(%pug.fullids,$2,32))) {
        endpug $2
      }
      else {
        notice $nick $codes(You need to specify a valid pug ID to end. eg !endpug 3)
      }
    }
    if ($1 = !serverpass) {
      if ($2) && ($3) && ($istok(%pug.fullids,$2,32)) {
        msg %pug.chan $codes(Password for pug $chr(35) $+ $2 has been changed.)
      }
      else {
        notice $nick $codes(You must specify the pug $chr(35) of which you want to change the password. eg, !serverpass 2 suckpenis)
      }
    }
  }
  if ($1 = !shuffle) {
    var %pugid, %x = 1
    if (!$2) {
      while (%x <= $numtok(%pug.fullids,32)) {
        if (%pug. [ $+ [ $gettok(%pug.fullids,%x,32) ] $+ .detailed ]) {
          if ($nick = %pug. [ $+ [ $gettok(%pug.fullids,%x,32) ] $+ .starter ]) {
            %pugid = $gettok(%pug.fullids,%x,32)
            rcon %pugid %pug. [ $+ [ %pugid ] $+ .ip ] %pug. [ $+ [ %pugid ] $+ .port ] admin shuffleTeams xp
            msg %pug.chan $codes(Shuffled teams in pug %pugid by XP)
            break
          }
        }
        inc %x
      }
    }
    elseif ($nick isop $chan) && ($2) {
      if (!$istok(%pug.fullids,$2,32)) { notice $nick $codes(Invalid pug ID specified) | halt }
      if ($3) {
        %pugid = $2
        if (!$istok(xp random swap,$3,32)) { notice $nick $codes(Available shuffle types: xp random swap) | halt }
        rcon %pugid %pug. [ $+ [ %pugid ] $+ .ip ] %pug. [ $+ [ %pugid ] $+ .port ] admin shuffleTeams $3
        msg %pug.chan $codes(Shuffled teams in pug $2 by $3)
      }
      else {
        %pugid = $2
        rcon %pugid %pug. [ $+ [ %pugid ] $+ .ip ] %pug. [ $+ [ %pugid ] $+ .port ] admin shuffleTeams xp
        msg %pug.chan $codes(Shuffled teams in pug $2 by XP)
      }
    }
  }
  if ($1 = !restart) {
    var %pugid, %x = 1
    if (!$2) {
      while (%x <= $numtok(%pug.fullids,32)) {
        if (%pug. [ $+ [ $gettok(%pug.fullids,%x,32) ] $+ .detailed ]) {
          if ($nick = %pug. [ $+ [ $gettok(%pug.fullids,%x,32) ] $+ .starter ]) {
            %pugid = $gettok(%pug.fullids,%x,32)
            rcon %pugid %pug. [ $+ [ %pugid ] $+ .ip ] %pug. [ $+ [ %pugid ] $+ .port ] admin restartMap
            msg %pug.chan $codes(Restarting map in %pugid)
            break
          }
        }
        inc %x
      }
    }
    elseif ($nick isop $chan) && ($2) {
      if (!$istok(%pug.fullids,$2,32)) { notice $nick $codes(Invalid pug ID specified) | halt }
      %pugid = $2
      rcon %pugid %pug. [ $+ [ %pugid ] $+ .ip ] %pug. [ $+ [ %pugid ] $+ .port ] admin restartMap
      msg %pug.chan $codes(Restarting map in $chr(35) $+ $2)
    }
  }
  if ($1 = !start) {
    var %pugid, %x = 1
    if (!$2) {
      while (%x <= $numtok(%pug.fullids,32)) {
        if (%pug. [ $+ [ $gettok(%pug.fullids,%x,32) ] $+ .detailed ]) {
          if ($nick = %pug. [ $+ [ $gettok(%pug.fullids,%x,32) ] $+ .starter ]) {
            %pugid = $gettok(%pug.fullids,%x,32)
            startMatch %pugid
            msg %pug.chan $codes(Starting match (pug $chr(35) $+ %pugid $+ ) in 10 seconds)
            break
          }
        }
        inc %x
      }
    }
    elseif ($nick isop $chan) && ($2) {
      if (!$istok(%pug.fullids,$2,32)) { notice $nick $codes(Invalid pug ID specified) | halt }
      %pugid = $2
      startMatch %pugid
      msg %pug.chan $codes(Starting match (pug $chr(35) $+ %pugid $+ ) in 10 seconds)
    }
  }
  if ($1 = !pug) {
    if ($istok(%pug.allplayers,$nick,32)) { notice $nick $codes(You're already in a pug!) | halt }
    if (!%pug.openids) {
      createPug
    }
    else {
      notice $nick $codes(There is already a pug open that requires players! Adding you to this pug instead)
      addplayer $nick
    }
  }
  if (($1 = !j) || ($1 = !me) || ($1 = !add) || ($1 = !join)) {
    if ($istok(%pug.allplayers,$nick,32)) { notice $nick $codes(You're already in a pug!) | halt }
    if (%pug.openids) {
      if ($2) {
        if ($istok(%pug.openids,$2,32)) {
          addplayer $2 $nick
        }
        else {
          notice $nick $codes(The pug ID you specified is already full or does not exist)
        }
      }
      else {
        addplayer $nick
      }
    }
    else {
      createPug
    }
  }
  if (($1 = !l) || ($1 = !delme) || ($1 = !leave)) {
    if (%pug.openids) {
      if (!$istok(%pug.allplayers,$nick,32)) { notice $nick $codes(You're not in a pug) | halt }
      if ($2) && ($istok(%pug.openids,$2,32)) {
        delplayer $2 $nick
      }
      else {
        delplayer $nick
      }
    }
  }
  if ($1 = !players) {
    if (%pug.openids) || (%pug.fullids) {
      if ($2) && (($istok(%pug.openids,$2,32) || ($istok(%pug.fullids,$2,32)) {
        msg %pug.chan $codes(Players in $2 $+ : %pug. [ $+ [ $2 ] $+ .players ])
      }
      else {
        var %a = 1, %x = 1, %b = $numtok(%pug.fullids,32), %y = $numtok(%pug.openids,32)
        while (%x <= %y) {
          msg %pug.chan $codes(Players in $gettok(%pug.openids,%x,32) $+ : %pug. [ $+ [ $gettok(%pug.openids,%x,32) ] $+ .players ])
          inc %x
        }
        while (%a <= %b) {
          msg %pug.chan $codes(Players in $gettok(%pug.fullids,%a,32) $+ : %pug. [ $+ [ $gettok(%pug.fullids,%a,32) ] $+ .players ])
          inc %a
        }
      }
    }
    else {
      msg %pug.chan $codes(No running pugs)
    }
  }
  if ($1 = !status) {
    msg %pug.chan $codes(There are a total of $np() pugs running)
    var %a = 1, %x = 1, %b = $numtok(%pug.fullids,32), %y = $numtok(%pug.openids,32)
    while (%x <= %y) {
      msg %pug.chan $codes(Pug $chr(35) $+ %x is currently looking for more players. Type 7!j to play)
      inc %x
    }
    while (%a <= %b) {
      msg %pug.chan $codes($iif(%pug. [ $+ [ $gettok(%pug.fullids,%a,32) ] $+ .detailed ],Pug $chr(35) $+ $gettok(%pug.fullids,%a,32) is in-game. $timeleft(gettok(%pug.fullids,%a,32)), $&
        Pug $chr(35) $+ $gettok(%pug.fullids,%a,32) is currently voting for a map))
      inc %a
    }
  }
  if ($1 = !details) {
    if ($istok(%pug. [ $+ [ $gettok(%pug.openids,1,32) ] $+ .players ],$nick,32)) {
      details $gettok(%pug.openids,1,32) $nick
    }
    else {
      var %x = 1
      while (%x <= $numtok(%pug.fullids,32)) {
        if ($istok(%pug. [ $+ [ $gettok(%pug.fullids,%x,32) ] $+ .players ],$nick,32)) {
          details $gettok(%pug.fullids,%x,32) $nick
        }
        inc %x
      }
    }
  }
  if (($1 = !vote) || ($1 = !map)) {
    if (%pug.mapvote) {
      if ($istok(%pug. [ $+ [ %pug.mapvote ] $+ .players ],$nick,32)) {
        if ($matchtok(%maps,$2,1,59)) {   
          var %voted = $v1  
          if (%map. [ $+ [ $nick ] ] = %voted) { halt }
          if (%map. [ $+ [ $nick ] ]) && (%map. [ $+ [ $nick ] ] != %voted) {
            inc %map. [ $+ [ %voted ] ]
            dec %map. [ $+ [ %map. [ $+ [ $nick ] ] ] ]
            msg %pug.chan $codes($nick changed vote from %map. [ $+ [ $nick ] ] $+  $votes(%map. [ $+ [ $nick ] ]) to %voted $+  $votes(%voted))
            %map. [ $+ [ $nick ] ] = %voted
          }
          else {
            inc %map. [ $+ [ %voted ] ]
            %map. [ $+ [ $nick ] ] = %voted
            msg %pug.chan $codes($nick voted for %voted $+  $votes(%voted))
          }
        }
        else {
          notice $nick $codes(Invalid map specified. Maps: %maps.list)
        }
      }
      else {
        notice $nick $codes(You're not in the pug that is currently voting for a map)
      }
    }
    else {
      notice $nick $codes(There is no map vote currently in progress)
    }
  }
}

alias startMatch {
  ;startMatch <id>
  var %pugid = $1
  var %x = 10
  while (%x >= 1) {
    .timer 1 $calc(10 - %x) rcon %pugid %pug. [ $+ [ %pugid ] $+ .ip ] %pug. [ $+ [ %pugid ] $+ .port ] say * * * Match starts in %x * * *
    dec %x
  }
  .timer 1 11 rcon %pugid %pug. [ $+ [ %pugid ] $+ .ip ] %pug. [ $+ [ %pugid ] $+ .port ] admin startMatch
  .timertimeleft $+ $1 1 $calc(40 * 60) return
}

alias choosePugServer {
  ;$choosePugServer($id)
  var %y = $ini(pugservers.ini,0), %x = 1, %served
  while (%x <= %y) {
    if (!$readini(pugservers.ini,%x,inuse)) {
      writeini pugservers.ini %x inuse $1
      %served = 1
      return $readini(pugservers.ini,%x,ip) $readini(pugservers.ini,%x,port)
    }
    inc %x
  }
  if (!%served) {
    return $null
  }
}

alias resetPugServer {
  ;resetPugServer $id
  var %y = $ini(pugservers.ini,0), %x = 1
  while (%x <= %y) {
    if ($readini(pugservers.ini,%x,inuse) = $1) {
      writeini pugservers.ini %x inuse 0
      break
    }
    inc %x
  }
}

alias slotsremaining {
  ;slotsremaining(id)
  if (!$1) { echo @debug slotsremaining called without id | return }
  return $+($chr(40),12,$calc(%pug.limit - $numtok(%pug. [ $+ [ $1 ] $+ .players ],32)),,/,%pug.limit,$chr(32),slots remaining,$chr(41))
}

alias startmapvote {
  ;startmapvote <id>
  if (!$1) { echo @debug startmapvote called without id | return }
  if (%pug.mapvote) { .timer 1 10 startmapvote $1 | return }
  %pug.mapvote = $1
  dopugtopic
  msg %pug.chan $codes(Map voting is now in progress. Vote for a map using !map or !vote. Eg, !map terminal)
  %maps.list = $replace(%maps,$chr(59),$chr(32) $+ - $+ $chr(32))
  msg %pug.chan $codes(Available maps: %maps.list)
  .timerautonotify $+ $1 off
  .timerendmapvote $+ $1 1 30 endmapvote $1
}

alias endmapvote {
  ;endmapvote <id>
  if (!$1) { echo @debug endmapvote called without id | return }
  if (%pug.mapvote != $1) { echo @debug 7A4L8ER4T endmapvote called with ID that is not the same as the mapvote ID | return }
  var %x = 1
  while (%x <= $numtok(%maps,59)) {
    if (%map. [ $+ [ $gettok(%maps,%x,59) ] ]) {
      if (!%map.high) { 
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59) 
      }
      if (%map. [ $+ [ $gettok(%maps,%x,59) ] ] = %map.high) && ($gettok(%maps,%x,59) != %map.win) {
        %map.equalhigh = %map. [ $+ [ $gettok(%maps,%x,59) ] ]
        %map.equalwin = $gettok(%maps,%x,59)
      }
      if (%map. [ $+ [ $gettok(%maps,%x,59) ] ] > %map.high) {
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59)
        unset %map.equalhigh
        unset %map.equalwin
      }
    }
    inc %x
  }
  if (!%map.win) {
    var %randmap = $gettok(%maps,$rand(1,$numtok(%maps,59)),59)
    set %map.win %randmap
    msg %pug.chan $codes(No map was voted for. Random map chosen: %randmap $+ )
  }
  else { 
    if (%map.equalwin) {
      msg %pug.chan $codes(Map vote is a tie between %map.equalwin ( $+ %map.equalhigh votes) and %map.win ( $+ %map.high votes))
      var %rand = $rand(1,10)
      if (%rand > 5) {
        msg %pug.chan $codes( $+ %map.win won the count with %map.high votes!)
      }
      else {
        msg %pug.chan $codes( $+ %map.equalwin won the count with %map.equalhigh votes!)
        set %map.win %map.equalwin
      }
    }
    else {
      msg %pug.chan $codes( $+ %map.win won the count with %map.high votes!) 
    }
  }
  rcon $1 %pug. [ $+ [ $1 ] $+ .ip ] %pug. [ $+ [ $1 ] $+ .port ] g_password %pug. [ $+ [ $1 ] $+ .pass ] ; admin changeMap %map.win
  set %pug. [ $+ [ $1 ] $+ .map ] %map.win
  dopugtopic
  unset %map.*
  unset %pug.mapvote
  .timerforceend $+ $1 1 3600 endpug $1
  .timernoendpug $+ $1 1 600 return
  massdetails $1
}

alias endpug {
  if ($istok(%pug.fullids,$1,32)) {
    rcon $1 %pug. [ $+ [ $1 ] $+ .ip ] %pug. [ $+ [ $1 ] $+ .port ] g_password getout
    var %x = 1, %y = $lines(%pug. [ $+ [ $1 ] $+ .dir ] $+ connections.txt)
    ;while (%x <= %y) {
    ;  rcon kickid $read(%pug. [ $+ [ $1 ] $+ .dir ] $+ connections.txt,%x) Pug is over
    ;  inc %x
    ;}
    %x = 1
    while (%x <= $numtok(%pug. [ $+ [ $1 ] $+ .players ],32)) {
      if ($istok(%pug.allplayers,$gettok(%pug. [ $+ [ $1 ] $+ .players ],%x,32),32)) {
        %pug.allplayers = $remtok(%pug.allplayers,$gettok(%pug. [ $+ [ $1 ] $+ .players ],%x,32),32)
      }
      inc %x
    }
    if (%pug.mapvote = $1) { unset %map.* %pug.mapvote }
    %pug.fullids = $remtok(%pug.fullids,$1,32)
    msg %pug.chan $codes(Pug $1 has ended)
    resetPugServer $1
  }
  elseif ($istok(%pug.openids,$1,32)) {
    write -dw* $+ $1 $+ * %dir $+ pugs.txt
    %pug.openids = $remtok(%pug.openids,$1,32)
    var %x = 1
    while (%x <= $numtok(%pug. [ $+ [ $1 ] $+ .players ],32)) {
      if ($istok(%pug.allplayers,$gettok(%pug. [ $+ [ $1 ] $+ .players ],%x,32),32)) {
        %pug.allplayers = $remtok(%pug.allplayers,$gettok(%pug. [ $+ [ $1 ] $+ .players ],%x,32),32)
      }
      inc %x
    }
    msg %pug.chan $codes(Closed pug $chr(35) $+ $1)
    resetPugServer $1
  }
  else {
    echo @debug 8W7A4R8NING - An endpug was called with an invalid ID... ooooh dear
  }
  dopugtopic
  .timerforceend $+ $1 off
  .timerautonotify $+ $1 off
  unset %pug. [ $+ [ $1 ] $+ * ]
}

alias pugtimeout {
  ;pugtimeout <id>
  msg %pug.chan $codes(Pug timed out)
  endpug $1
}

alias createPug {
  ;createPug <nick>
  echo @debug 4No running pugs. Creating new one:
  var %newpugid = $calc($read(%dir $+ pugs.txt,$calc($lines(%dir $+ pugs.txt) - 1)) + 1)
  var %serverdetails = $choosePugServer(%newpugid)
  if (!%serverdetails) { msg %pug.chan $codes(No more servers are available) | return }
  write %dir $+ pugs.txt %newpugid
  %pug.openids = $addtok(%pug.openids,%newpugid,32)
  var %pugdir = %dir $+ pug. $+ %newpugid $+ \
  mkdir %pugdir
  write %pugdir $+ players.txt $nick
  %pug. [ $+ [ %newpugid ] $+ .dir ] = %pugdir
  %pug. [ $+ [ %newpugid ] $+ .players ] = $nick
  %pug. [ $+ [ %newpugid ] $+ .starter ] = $nick
  %pug.allplayers = $addtok(%pug.allplayers,$nick,32)
  %pug. [ $+ [ %newpugid ] $+ .pass ] = $lower($read(pass.txt))
  %pug. [ $+ [ %newpugid ] $+ .adminpass ] = $lower($read(pass.txt))
  %pug. [ $+ [ %newpugid ] $+ .ip ] = $gettok(%serverdetails,1,32)
  %pug. [ $+ [ %newpugid ] $+ .port ] = $gettok(%serverdetails,2,32)

  rcon %newpugid %pug. [ $+ [ %newpugid ] $+ .ip ] %pug. [ $+ [ %newpugid ] $+ .port ] g_password %pug. [ $+ [ %newpugid ] $+ .pass ]

  dopugtopic
  autonotify %newpugid
  .timertimeout $+ %newpugid 1 1200 pugtimeout %newpugid
  notice %pug.chan $codes(5v5 pug started by $nick $+ . Type 7!j to join)
  msg %pug.chan $codes(New pug started by $nick $+ . Type 7!j to join)
  msg %pug.chan $codes($nick has joined pug %newpugid $slotsremaining(%newpugid))
}

alias addplayer {
  ;addplayer <id> <name>
  if ($istok(%pug.openids,$1,32)) {
    var %pugid = $1
    var %nick = $2
    var %pugdir = %pug. [ $+ [ %pugid ] $+ .dir ]
    write %pugdir $+ players.txt %nick
    %pug. [ $+ [ %pugid ] $+ .players ] = $addtok(%pug. [ $+ [ %pugid ] $+ .players ],%nick,32)
    %pug.allplayers = $addtok(%pug.allplayers,%nick,32)
    msg %pug.chan $codes(%nick has joined pug %pugid $slotsremaining(%pugid))
    if ($numtok(%pug. [ $+ [ %pugid ] $+ .players ],32) = %pug.limit) {
      set %pug. [ $+ [ %pugid ] $+ .full ] 1
      %pug.fullids = $addtok(%pug.fullids,%pugid,32)
      %pug.openids = $remtok(%pug.openids,%pugid,32)
      startmapvote %pugid
    }
  }
  else {
    var %pugid = $gettok(%pug.openids,1,32)
    var %nick = $1
    var %pugdir = %pug. [ $+ [ %pugid ] $+ .dir ]
    write %pugdir $+ players.txt %nick
    %pug. [ $+ [ %pugid ] $+ .players ] = $addtok(%pug. [ $+ [ %pugid ] $+ .players ],%nick,32)
    %pug.allplayers = $addtok(%pug.allplayers,%nick,32)
    msg %pug.chan $codes(%nick has joined pug %pugid $slotsremaining(%pugid))
    if ($numtok(%pug. [ $+ [ %pugid ] $+ .players ],32) = %pug.limit) {
      set %pug. [ $+ [ %pugid ] $+ .full ] 1
      %pug.fullids = $addtok(%pug.fullids,%pugid,32)
      %pug.openids = $remtok(%pug.openids,%pugid,32)
      startmapvote %pugid
    }
  }
}

alias delplayer {
  if ($istok(%pug.openids,$1,32)) {
    var %pugid = $1
    var %nick = $2
    if ((!$istok(%pug.allplayers,%nick,32)) || (!$istok(%pug. [ $+ [ %pugid ] $+ .players ],%nick,32))) { echo @debug DELPLAYER IS MISSING FROM $v1 or $v2 | return }
    var %pugdir = %pug. [ $+ [ %pugid ] $+ .dir ]
    write -dw* $+ %nick $+ * %pugdir $+ players.txt
    %pug. [ $+ [ %pugid ] $+ .players ] = $remtok(%pug. [ $+ [ %pugid ] $+ .players ],%nick,32)
    %pug.allplayers = $remtok(%pug.allplayers,%nick,32)
    if ($numtok(%pug. [ $+ [ %pugid ] $+ .players ],32) = 0) {
      endpug %pugid
      return
    }
    msg %pug.chan $codes(%nick has left pug %pugid $slotsremaining(%pugid))
    if ($nick = %pug. [ $+ [ %pugid ] $+ .starter ]) {
      %pug. [ $+ [ %pugid ] $+ .starter ] = $gettok(%pug. [ $+ [ %pugid ] $+ .players ],1,32)
      msg %pug.chan $codes(%pug. [ $+ [ %pugid ] $+ .starter ] is now admin)
    }
  }
  else {
    var %pugid = $gettok(%pug.openids,1,32)
    var %nick = $1
    if ((!$istok(%pug.allplayers,%nick,32)) || (!$istok(%pug. [ $+ [ %pugid ] $+ .players ],%nick,32))) { echo @debug DELPLAYER IS MISSING FROM $v1 or $v2 | return }
    var %pugdir = %pug. [ $+ [ %pugid ] $+ .dir ]
    write -dw* $+ %nick $+ * %pugdir $+ players.txt
    %pug. [ $+ [ %pugid ] $+ .players ] = $remtok(%pug. [ $+ [ %pugid ] $+ .players ],%nick,32)
    %pug.allplayers = $remtok(%pug.allplayers,%nick,32)
    if ($numtok(%pug. [ $+ [ %pugid ] $+ .players ],32) = 0) {
      endpug %pugid
      return
    }
    msg %pug.chan $codes(%nick has left pug %pugid $slotsremaining(%pugid))
    if ($nick = %pug. [ $+ [ %pugid ] $+ .starter ]) {
      %pug. [ $+ [ %pugid ] $+ .starter ] = $gettok(%pug. [ $+ [ %pugid ] $+ .players ],1,32)
      msg %pug.chan $codes(%pug. [ $+ [ %pugid ] $+ .starter ] is now admin)
    }
  }
}

alias np {
  return $calc($numtok(%pug.fullids,32) + $numtok(%pug.openids,32))
}
