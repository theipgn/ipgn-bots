alias ssh {
  var %f C:\TF2\ssh.commands
  write -c %f
  var %e = write %f
  if ($1 = demo) {
    %e ssh -t -t inara.ipgn.com.au /games/ipgn/bin/zipdemos2
    %e /usr/bin/rsync --remove-sent-files inara:/games/tf2_pug/orangebox/tf/ $+ $2 /data/gamefiles/tf2/demos/pug/
    %e /data/scripts/games/indexgames /data/gamefiles/tf2/demos/pug/
    sshrun putty zoe.ipgn.com.au 22 -l gamesvr -i C:\TF2\key_priv.ppk -m %f
  }
  if ($1 = restart) {
    %e /games/ipgn/bin/tf2pug.sh restart
    sshrun putty inara.ipgn.com.au -l steam -i C:\TF2\key_priv.ppk -m %f
    ;msg %pug.adminchan $codes(Server has been restarted)
  }
  if ($1 = mumble) {
    %e python /games/ipgn/bin/mmctl.py $2-
    sshrun putty jayne.ipgn.com.au -l gamesvr -i C:\TF2\key_priv.ppk -m %f
  }
  if ($1 = bookabledemos) {
    %e ssh -t -t steam@inara /games/ipgn/bin/zipdemos
    %e ssh -t -t steam@mal /games/ipgn/bin/zipdemos
    %e ssh -t -t steam@calypso /games/ipgn/bin/zipdemos
    %e /usr/bin/rsync --remove-sent-files --bwlimit=1000 steam@calypso:/games/tf2/orangebox/tf/*.zip /data/gamefiles/tf2/demos/
    %e /usr/bin/rsync --remove-sent-files --bwlimit=8000 steam@mal:/games/tf2/orangebox/tf/*.zip /data/gamefiles/tf2/demos/
    %e /usr/bin/rsync --remove-sent-files --bwlimit=8000 steam@inara:/games/tf2/orangebox/tf/*.zip /data/gamefiles/tf2/demos/
    %e mv -f /data/gamefiles/tf2/demos/*ozfl*zip /data/gamefiles/tf2/demos/ozfl/
    %e mv -f /data/gamefiles/tf2/demos/auto-*zip /data/gamefiles/tf2/demos/autorecord
    %e mv -f /data/gamefiles/tf2/demos/*zip /data/gamefiles/tf2/demos/scrim/
    %e /data/scripts/games/indexgames /data/gamefiles/tf2/demos/scrim/ > /dev/null 2>&1 &
    %e /data/scripts/games/indexgames /data/gamefiles/tf2/demos/autorecord/ > /dev/null 2>&1 &
    %e /data/scripts/games/indexgames /data/gamefiles/tf2/demos/ozfl/ > /dev/null 2>&1 &
    ;%e /data/scripts/games/botupload
    sshrun putty zoe.ipgn.com.au 22 -l gamesvr -i C:\TF2\key_priv.ppk -m %f
  }
  ;More to come...
}

alias sshrun {
  .run C:\TF2\ $+ $1 $+ .exe $2-
}

on *:sockread:demoupload:{
  var %upload.data
  sockread -f %upload.data
  echo -s %upload.data
}
