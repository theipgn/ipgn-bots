alias rcon {
  echo @debug rcon params: $1-
  if ($chan != %pug.chan) { set % [ $+ rcon. $+ [ $1 ] $+ .chan ] $chan }
  bset &rcon_cmd 1 255 255 $asc(r) $asc(c) $asc(o) $asc(n) 0
  var %x = 1
  while (%x <= $len(%rcon.password)) {
    bset &rcon_cmd $calc($bvar(&rcon_cmd,0) + 1) $asc($mid(%rcon.password,%x,1))
    inc %x
  }
  bset &rcon_cmd $calc($bvar(&rcon_cmd,0) + 1) 0
  %x = 1
  while (%x <= $len($4-)) {
    bset &rcon_cmd $calc($bvar(&rcon_cmd,0) + 1) $asc($mid($4-,%x,1))
    inc %x
  }
  bset &rcon_cmd $calc($bvar(&rcon_cmd,0) + 1) 0
  sockudp -k rcon. [ $+ [ $1 ] ] $calc(%rcon.sockport + $1) $2 $3 &rcon_cmd
}

on *:udpread:rcon.*:{
  var &rcon.data
  sockread &rcon.data
  ;echo @rcon $bvar(&rcon.data,1-)
  var %x = 13, %y = $bvar(&rcon.data,0)
  echo @rcon OVERALL LENGTH OF RECEIVED TRANSMISSION: %y
  while (%x <= %y) {
    var %data = $bvar(&rcon.data,%x,100).text
    echo @rcon DATA: %data
    echo @rcon LENGTH OF DATA: $len(%data)
    var %a = 1, %b = $numtok(%data,10)
    while (%a <= %b) {
      ;client 0: o, ping = 23, rate = 35000
      ;msg %pug.chan $gettok(%data,%a,10)
      if (client isin $gettok(%data,%a,10)) {
        var %clientdata = $gettok(%data,%a,10)
        var %z = 1
        echo @debug 4CLIENT DETECTED
      }
      echo @rcon $gettok(%data,%a,10)
      msg % [ $+ [ $sockname ] $+ .chan ] Response %a $+ : $remove($gettok(%data,%a,10),^7)
      inc %a
    }
    %x = $calc(%x + $len(%data) + 1)
    echo @rcon Value of x: %x
  }
  sockclose $sockname
}
