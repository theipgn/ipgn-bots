on *:CONNECT:{
  .timerunbancheck 0 600 unbancheck
}

on *:TEXT:!*:#ipgn-tf2:{
  if ($1 = !pugrcon) {
    rcon $2-
    msg $chan $codes(Rcon command sent ( $+ $2- $+ ))
  }
  if ($1 = !allowid) {
    if (!$regex($2,STEAM_(\d):(\d):(\d+))) || (!$3) { msg $chan $codes(Syntax: !allowid <steamid> <name>) | return }
    if (!$read(%statsdir $+ allowedids.txt,w,$2 $+ *)) {
      write %statsdir $+ allowedids.txt $2 $3 $nick $time(dd/mm/yyyy)
      rcon removeid $2 ; writeid
      msg $chan $codes(12 $+ $3 ( $+ $2 $+ ) is now able to connect to the server)
      if ($3 ison %pug.chan) { msg $3 $codes($nick has approved your ID ( $+ $2 $+ ) and you are now able to connect to the server) }
      unbanid $2
    }
    else {
      tokenize 32 $read(%statsdir $+ allowedids.txt,w,* $+ $2 $+ *)
      msg $chan $codes(12 $+ $2 ( $+ $1 $+ ) has already been allowed to connect by $3 on $4)
    }
  }
  if ($1 = !disallowid) {
    if (!$regex($2,STEAM_(\d):(\d):(\d+))) { msg $chan $codes(Syntax: !disallowid <steamid>) | return }
    if ($read(%statsdir $+ bannedids.txt,w,$2 $+ $chr(37) $+ *)) {
      tokenize 37 $v1 
      msg $chan $codes($3 ( $+ $1 $+ ) is already banned by $2 for the following reason: $4 $+ . Date and time banned: $5 $+ .)
      return
    }
    if ($read(%statsdir $+ allowedids.txt,w,* $+ $2 $+ *)) {
      tokenize 32 $v1
      write -dl $+ $readn %statsdir $+ allowedids.txt
      msg $chan $codes(12 $+ $2 ( $+ $1 $+ ) allowed by $3 on $4 is no longer allowed to connect to the server)
      if ($2 ison %pug.chan) { msg $2 $codes($nick has disallowed your ID ( $+ $1 $+ ) from connecting to the server) }
    }
    else {
      msg $chan $codes(SteamID $2 has not been allowed to connect to the server)
      write %statsdir $+ bannedids.txt $+($2,$chr(37),$nick,$chr(37),unknown,$chr(37),new steam account; connection disallowed,$chr(37),$date(ddd dd/mm/yyyy HH:nn:ss))
    }
  }
  if ($1 = !allowedids) {
    var %x = 1, %y = $lines(%statsdir $+ allowedids.txt)
    msg $chan $codes(Allowed SteamIDs of newly registered accounts:)
    while (%x <= %y) {
      tokenize 32 $read(%statsdir $+ allowedids.txt,%x)
      msg $chan $codes(12 $+ $2 ( $+ $1 $+ ) allowed by $3 on $4)
      inc %x
    }
  }
  if ($1 = !stat) {
    if ($2) {
      if ($read(%statsdir $+ statsall.txt,w,* $+ $2 $+ *)) {
        ;id;name;kill;death;cap;assist;points;games;healing;uberlost;win;loss;draw;captain;damage;ammopack_small;ammopack_medium;tf_ammo_pack;medkit_small;medkit_medium
        tokenize 37 $v1
        msg $chan 12 $+ $2 (12 $+ $1 $+ ) - Kills:12 $3 Deaths:12 $4  Caps:12 $5 Assists:12 $6 Points:12 $7 $&
          Mediced:12 $iif($hget(medtable,$1),$calc(($hget(medtable,$1) - 2) / 2),0) Medic Rate:12 $+($round($calc(100 * ($calc(($hget(medtable,$1) - 2) / 2) / $8)),2),$chr(37)) Med Value:12 $calc($hget(medtable,$1) / $8 / $hget(lastmed,$1)) $&
          Healing Done:12 $9 Ubers Lost:12 $10 Games since medic:12 $hget(lastmed,$1) $&
          Played:12 $8 Won:12 $11 Lost:12 $12 Drawn:12 $13 Win Rate:12 $+($round($calc(100 * ($11 / $8)),2),$chr(37)) $&
          Player Score:12 $round($score($1),2) Captained:12 $14 Damage:12 $15 Ammo Packs:12 $calc($16 + $17 + $18) Med Packs:12 $calc($19 + $20)
      }
      else {
        msg $chan $codes(User not found)
      }
    }
  }
  if ($1 = !conns) && ($2) {
    if (%conns.search) { 
      notice $nick $codes(Someone is already searching. Please wait a while)
      return
    }
    dll mThreads.dll thread -ca thread1 find $nick $2-
  }
  if ($1 = !demo) {
    echo @demos Checking if demo $2 has been uploaded...
    var %gamenum = $2
    if ($gettok($read(%statsdir $+ demos.txt,w,* $+ $2 $+ *),2,32) = uploaded) {
      echo @demos Demo $2 has been uploaded. Getting link... $getdemolink(%gamenum)
      msg $chan $codes(Demo link for game %gamenum $+ : $getdemolink(%gamenum))
    }
    elseif ($read(%statsdir $+ demos.txt,w,* $+ $2 $+ *)) {
      uploaddemo $2
      notice $nick $codes(Please wait while the demo is being uploaded.)
    }
    else {
      msg $chan $codes(No demo found for game $2)
    }
  }
  if ($1 = !rspugserver) {
    msg %pug.adminchan $codes(Restarting the server)
    ssh restart
  }
  if ($1 = !gdetails) && ($2) {
    if ($glookup($2)) {
      var %redscore = $glookup($2,fsred)
      var %bluescore = $glookup($2,fsblue)
      var %gplayers = $glookup($2,players)
      var %blueplayers = $glookup($2,playersblue)
      var %redplayers = $glookup($2,playersred)
      var %redteam = $glookup($2,redteam)
      var %blueteam = $glookup($2,blueteam)
      var %blueplayerscore = $glookup($2,blueplayerscore)
      var %redplayerscore = $glookup($2,redplayerscore)
      var %winreason = $glookup($2,winreason)
      var %mapplayed = $glookup($2,mapplayed)
      msg %pug.adminchan $codes(Details for game $chr(35) $+ $2 are:)
      msg %pug.adminchan $codes($clr(orange) $+ Map Played: $+ $clr %mapplayed)
      msg %pug.adminchan $codes($clr(orange) $+ Game Ended: $+ $clr %winreason)
      msg %pug.adminchan $codes(Players: %gplayers)
      msg %pug.adminchan $codes($clr(red) $+ Red $+ $clr final score: $clr(red) $+ %redscore with $clr(red) $+ %redplayers players)
      msg %pug.adminchan $codes($clr(blue) $+ Blue $+ $clr final score: $clr(blue) $+ %bluescore with $clr(blue) $+ %blueplayers players)
      msg %pug.adminchan $codes($clr(blue) $+ Blue: $+ $clr %blueteam)
      msg %pug.adminchan $codes($clr(red) $+ Red: $+ $clr %redteam)
      msg %pug.adminchan $codes(Red player score: $clr(red) $+ %redplayerscore)
      msg %pug.adminchan $codes(Blue player score: $clr(blue) $+ %blueplayerscore)
      msg %pug.adminchan $codes(Total player score: $calc(%redplayerscore + %blueplayerscore))
    }
    else { msg %pug.chan $codes(No scores for game $2 available) }
  }
  if ($1 = !score) {
    if ($read(%statsdir $+ statsall.txt,w,* $+ $2 $+ *)) {
      tokenize 37 $v1
      msg %pug.adminchan $2 (12 $+ $1 $+ ) Score: $score($1)
    }
    else {
      msg %pug.adminchan $codes(No user found matching $2)
    }  
  }
  if ($1 = !banid) && ($nick isop %pug.adminchan) && (STEAM_ isin $2) && ($3) && ($4) {
    banid $2-
  }
  if ($1 = !unbanid) {
    unbanid $2-
  }
  if ($1 = !saytf) {
    rcon say $nick $+ : $2-
    msg $chan $codes(Messaged the server with: $2- $+ )
  }
  if ($1 = !maps) {
    %maps.list = $replace(%maps,$chr(59),$chr(32) $+ - $+ $chr(32))
    msg $chan $codes(Maps: %maps.list)
  }
  if ($1 = !addmap) {
    if ($regex($2,^(cp|koth)_.*)) {
      if (!$istok(%maps,$2,59)) {
        set %maps $addtok(%maps,$2,59)
        msg %pug.adminchan $codes($2 added to the map list)
      }
      else {
        msg %pug.adminchan $codes(That map is already in the list)
      }
    }
    else {
      msg %pug.adminchan $codes(Invalid map specified. koth_ and cp_ maps only)
    }
  }
  if ($1 = !delmap) {
    if ($istok(%maps,$2,59)) {
      set %maps $remtok(%maps,$2,59)
      msg %pug.adminchan $codes($2 removed from the map list)
    }
    else {
      msg %pug.adminchan $codes(Specified map is not in the map list)
    }
  }
  if ($1 = !addmaplink) {
    if ((!$2) || (!$3)) { 
      msg %pug.adminchan $codes(Syntax: !addmaplink <mapname> <url>)
    }
    elseif (!$istok(%maps,$2,59)) {
      msg %pug.adminchan $codes(Invalid map specified. Maps: $replace(%maps,$chr(59),$chr(32) $+ - $+ $chr(32)))
    }
    else {
      if ($read(%statsdir $+ maplinks.txt,w,* $+ $2 $+ *)) {
        var %l = $v1, %readn = $readn
        var %currentlink = $gettok(%l,2,32)
        write -l $+ %readn %statsdir $+ maplinks.txt $puttok(%l,$3,2,32)
        msg %pug.adminchan $codes(Changed link for12 $2 to $3 from %currentlink)
      }
      else {
        write %statsdir $+ maplinks.txt $2 $replace($3-,$chr(32),% $+ 20)
        msg %pug.adminchan $codes(Added link for12 $2 ( $+ $3- $+ ))
      }
    }
  }
  if ($1 = !chat) {
    if ($2) {
      msg $chan http://tf2pug.ipgn.com.au/pchat.php?id= $+ $2
    }
  }
  if ($1 = !nomed) {
    var %x = 1
    msg $chan $codes(Players who will not get medic:)
    while (%x <= $lines(rigids.txt)) {
      var %id = $read(rigids.txt,%x)
      msg $chan $codes(12 $+ $getnamebyid(%id) ( $+ %id $+ ) Rigged value:12 $hget(medtable,%id) Med Value:12 $calc($hget(medtable,%id) / $hget(lastmed,%id) / $gettok($read(%statsdir $+ statsall.txt,w,%id $+ *),8,37)) Last Med:12 $hget(lastmed,%id))
      inc %x
    }
  }
  if ($1 = !addnomed) {
    ;STEAM_0:0:11560062
    if ($regex($2,STEAM_(\d):(\d):(\d+))) {
      if (!$read(rigids.txt,w,$2)) {
        write rigids.txt $2
        hload medtable %statsdir $+ pugmedtable
        hadd medtable $2 $calc($hget(medtable,$2) + 100000)
        hsave medtable %statsdir $+ pugmedtable
        msg $chan $codes($getnamebyid($2) ( $+ $2 $+ ) has been rigged)
      }
      else {
        msg $chan $codes($2 is already rigged)
      }
    }
    else {
      msg $chan $codes(Invalid SteamID specified)
    }
  }
  if ($1 = !delnomed) {
    if ($regex($2,STEAM_(\d):(\d):(\d+))) {
      if ($read(rigids.txt,w,$2)) {
        write -dw $+ $2 rigids.txt $2
        hload medtable %statsdir $+ pugmedtable
        hload lastmed %statsdir $+ lastmedtable.med
        hadd medtable $2 $calc($hget(medtable,$2) - 99925)
        hdel lastmed $2
        hsave lastmed %statsdir $+ lastmedtable.med
        hsave medtable %statsdir $+ pugmedtable
        msg $chan $codes($getnamebyid($2) ( $+ $2 $+ ) has been unrigged)
      }
      else {
        msg $chan $codes($2 not rigged)
      }
    }
    else {
      msg $chan $codes(Invalid SteamID specified)
    }
  }
  if ($1 = !mapstats) && (1 == 2) {
    var %i = 1, %ng = $ini(gamescores.ini,0), %ln = $lines(%statsdir $+ maps.txt)
    while (%i <= %ln) {
      var %l = $read(%statsdir $+ maps.txt,%i)
      var %m = $gettok(%l,1,37)
      var %tp = $gettok(%l,2,37)
      var %pct = $calc(($gettok(%l,2,37) / %ng) * 100)
      msg $chan $codes(Map:12 %m Played:12 %tp Overall % $+ :12 %pct)
      inc %i
    }
  }
  if ($1 = !redomapstats) && ($address($nick,2) == *!*@bladezz.admin.ipgn) {
    dll mThreads.dll thread -ca thread1 mapstats
  }
  if ($1 = !gamestats) && ($2 isnum) {
    var %game = $2
    var %gamefile = %statsdir $+ games\ $+ stats. $+ %game $+ .txt
    if ($exists(%gamefile)) {
      parseStats %gamefile $chan
    }
    else {
      msg $chan $codes(No stats for game %game)
    }
  }
  if ($1 = !rename) {
    if ($0 < 2) { msg $chan $codes(Syntax: !rename <steamid> <name>) | return }
    if (!$regex($2,^STEAM_0:[0-1]:([0-9]+)$)) { msg $chan $codes(Invalid steamid specified) | return }
    if ($3 !isalnum) { msg $chan $codes(Invalid name specified) | return }
    var %id = $2, %name = $3-
    if ($read(%statsdir $+ rename.txt,w,%id $+ $chr(32) $+ *)) {
      var %nl = $v1
      if ($gettok(%nl,2-,32) = %name) {
        msg $chan $codes(%id already has name %name)
      }
      else {
        write -w* $+ %id $+ * %statsdir $+ rename.txt %id %name
        rcon cw_rename " $+ %name $+ " " $+ %id $+ "
        msg $chan $codes(12 $+ %id will now be renamed to12 %name)
      }
    }
    else {
      write %statsdir $+ rename.txt %id %name
      rcon rcon cw_rename " $+ %name $+ " " $+ %id $+ "
      msg $chan $codes(12 $+ %id will now be renamed to12 %name)
    }
  }
  if ($1 = !playernames) {
    var %x = 1, %y = $lines(%statsdir $+ rename.txt)
    while (%x <= %y) {
      var %l = $read(%statsdir $+ rename.txt,%x)
      msg $chan $codes(12 $+ $gettok(%l,1,32) has name12 $gettok(%l,2-,32))
      inc %x
    }
  }
  if ($1 = !tempbanid) {
    if ((!$2) || (!$3) || (!$4)) {
      msg $chan $codes(Syntax: !tempbanid <steamid> <duration (xWxD or xW or xD or xH)> <reason>)
      return
    }
    if ($regex($3,/((\d+)w)?((\d+)d)?|(\d+)h)/i) {
      ;write %statsdir $+ bannedids.txt $+(STEAMID,$chr(37),BANNER,$chr(37),BANNED NAME,$chr(37),REASON,$chr(37),$date(ddd dd/mm/yyyy HH:nn:ss),$chr(37),DURATION)
      if ($read(%statsdir $+ bannedids.txt, w, $2 $+ $chr(37) $+ *)) {
        tokenize 37 $v1 
        msg $chan $codes($3 (12 $+ $1 $+ ) is already banned by12 $2 for the following reason: $4 $+ . Date and time banned: $5 $+ . Duration: $6)
      }
      else {
        var %playernick = $getnamebyid($2)
        write %statsdir $+ bannedids.txt $+($2,$chr(37),$nick,$chr(37),%playernick,$chr(37),$4-,$chr(37),$date(ddd dd/mm/yyyy HH:nn:ss),$chr(37),$calc($ctime + $duration($3)))
        msg $chan $codes(%playernick (12 $+ $2 $+ ) has been temporarily banned for $3 $+ . Reason: $4-)
        upload bans %statsdir $+ bannedids.txt
      }
    }
    else {
      msg $chan $codes(Invalid duration specified. Syntax: !tempbanid <steamid> <duration (xW or xD or xH)> <reason>)
    }
  }
  if (($1 = !bp) || ($1 = !banplayer)) {
    if (($2 !isnum) && ($3 isnum)) {
      autobanplayercheck $2-
    }
  }
}

alias unbancheck {
  var %x = 1, %y = $lines(%statsdir $+ bannedids.txt)
  while (%x <= %y) {
    if ($read(%statsdir $+ bannedids.txt,%x)) {
      var %line = $v1, %readn = $readn

      if ($gettok(%line,6,37)) {
        var %unbantime = $v1

        if ($ctime >= %unbantime) {
          tokenize 37 %line

          msg %pug.adminchan $codes(Ban for $3 ( $+ $1 $+ ) has expired)
          msg %pug.chan $codes($3 ( $+ $1 $+ ) has been unbanned from the server)
          write -dl $+ %readn %statsdir $+ bannedids.txt
          rcon removeid $1 ; writeid
          upload bans %statsdir $+ bannedids.txt
        }
      }
    }

    inc %x
  }
}

alias autobanplayercheck {
  if ($len($1) < 6) {
    return
  }

  if ($read(%statsdir $+ statsall.txt,w,* $+ $1 $+ *)) {
    var %steamid = $gettok($v1,1,37), %ignick = $gettok($v1,2,37)

    if ($read(%statsdir $+ bannedids.txt,w,%steamid $+ $chr(37) $+ *)) { return }

    var %banlength = $calc($ctime + $duration($2 $+ h))

    write %statsdir $+ bannedids.txt $+(%steamid,$chr(37),$nick,$chr(37),$1,$chr(37),$4-,$chr(37),$date(ddd dd/mm/yyyy HH:nn:ss),$chr(37),%banlength))
    msg %pug.adminchan $codes($1 07/ %ignick (12 $+ %steamid $+ ) has been temporarily banned for $2 hour(s). Reason: $4-)
    msg %pug.chan $codes($1 07/ %ignick (12 $+ %steamid $+ ) has been temporarily banned for $2 hour(s). Reason: $4-)

    upload bans %statsdir $+ bannedids.txt
  }
}

alias banid {
  ;!banid steami_id name reason
  if ($read(%statsdir $+ bannedids.txt,w,$1 $+ $chr(37) $+ *)) {
    tokenize 37 $v1 
    if ($nick) {
      notice $nick $3 ( $+ $1 $+ ) is already banned by $2 for the following reason: $4 $+ . Date and time banned: $5 $+ .
    }
  }
  else {
    write %statsdir $+ bannedids.txt $+($1,$chr(37),$iif($nick,$ifmatch,$me),$chr(37),$2,$chr(37),$3-,$chr(37),$date(ddd dd/mm/yyyy HH:nn:ss))
    msg %pug.adminchan $codes($2 ( $+ $1 $+ ) has been banned from the server.)
    msg %pug.chan $codes($2 ( $+ $1 $+ ) has been banned from the server.)

    write -dw* $+ $1 $+ $chr(37) $+ * %statsdir $+ allowedids.txt

    upload bans %statsdir $+ bannedids.txt
  }
}

alias unbanid {
  if ($read(%statsdir $+ bannedids.txt,w, * $+ $1 $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    msg %pug.adminchan $codes( $3 ( $+ $1 $+ ) has been unbanned.)
    msg %pug.chan $codes( $3 ( $+ $1 $+ ) has been unbanned from the server.)
    write -dl $+ $readn %statsdir $+ bannedids.txt
    rcon removeid $1 ; writeid
    upload bans %statsdir $+ bannedids.txt
  }
  else {
    notice $nick No data could be found matching $1
  }
}
