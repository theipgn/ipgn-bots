on *:start:{
  window -e @rcon
  window -e @upload
  window -e @country
  window -e @medic
  window -e @weapons
  window -e @shorten
  window -e @demos
  set %gamesplayed $readini(gamescores.ini,global,gamesplayed)
  hmake medtable 100
  hmake lastmed 100
  .timer 1 10 hload medtable %statsdir $+ pugmedtable
  .timer 1 10 hload lastmed %statsdir $+ lastmedtable.med
  .timer 1 2 rcon.logs
  endpug
}

alias resetup {
  endpug
  window -e @rcon
  window -e @upload
  window -e @country
  window -e @medic
  window -e @weapons
  window -e @shorten
  window -e @demos
  set %gamesplayed $readini(gamescores.ini,global,gamesplayed)
  hmake medtable 100
  hmake lastmed 100
  .timer 1 10 hload medtable %statsdir $+ pugmedtable
  .timer 1 10 hload lastmed %statsdir $+ lastmedtable.med
  .timer 1 2 rcon.logs
}

on *:INPUT:@rcon:{
  if ($left($1,1) != $chr(47)) { 
    rcon $1-
    set %rcon.input $1-
  }
}

alias logoff {
  rcon logaddress_delall
}

alias rcon.logs {
  sockclose rcon
  set %rcon.myip 210.50.4.5
  sockudp -kn rcon %rcon.sockport %rcon.ip %rcon.port 
  rcon logaddress_del %rcon.myip $+ : $+ %rcon.sockport ; logaddress_add %rcon.myip $+ : $+ %rcon.sockport
}

alias rcon {
  echo @rcon ---------------------------- CMD $1-
  ;.run -n $shortfn(C:\TF2\SourceRcon.exe) %rcon.ip %rcon.port %rcon.password " $+ $1- $+ "
  sourceRcon %rcon.ip %rcon.port %rcon.password $1-
}

alias chan {
  msg %pug.chan $codes($1-)
}

alias killchan {
  msg %pug.killchan $pinkcodes($1-)
}
alias clr {
  if ($1 = Blue) {
    return 12
  }
  elseif ($1 = Red) {
    return 04
  }
  elseif ($1 = Green) {
    return 09
  }
  elseif ($1 = yellow) {
    return 08
  }
  elseif ($1 = darkgreen) {
    return 03
  }
  elseif ($1 = orange) {
    return 07
  }
  elseif ($1 = purple) {
    return 06
  }
  elseif ($1 = cyan) {
    return 11
  }
  elseif ($1 = pink) {
    return 13
  }
  else {
    return 
  }
}

alias sid {
  return $remove($1,STEAM_,:)
}

alias chatreplace {
  var %msg = $1-
  %msg = $replace(%msg,',\')
  %msg = $replace(%msg,$chr(44),\ $+ $chr(44))
  return %msg
}

alias replacenames {
  if ($1 = tf_projectile_rocket) {
    return rocket launcher
  }
  elseif ($1 = tf_projectile_pipe) {
    return pipe bomb
  }
  elseif ($1 = tf_projectile_pipe_remote) {
    return sticky bomb
  }
  elseif ($1 = tf_projectile_arrow) {
    return bow and arrow
  }
  elseif ($1 = OBJ_SENTRYGUN) {
    return L1 sentry gun
  }
  elseif ($1 = OBJ_TELEPORTER_EXIT) {
    return teleporter exit
  }
  elseif ($1 = OBJ_TELEPORTER_ENTRANCE) {
    return teleporter entrance
  }
  elseif ($1 = OBJ_DISPENSER) {
    return dispenser
  }
  elseif ($1 = obj_sentrygun2) {
    return L2 sentry gun
  }
  elseif ($1 = obj_sentrygun3) {
    return L3 sentry gun
  }
  elseif ($1 = Granary_cap_red_cp2) {
    ;return Control Point: 2 (Red). CP Name: Granary Red CP2 (Red House)
    return Granary Red CP2 ( $+ $clr(red) $+ Red House $+ $clr $+ )
  }
  elseif ($1 = Granary_cap_red_cp1) {
    ;return Control Point: 1 (Red). CP Name: Granary Red CP1 ( $+ $clr(red) $+ Red Base $+ $clr $+ )
    return Granary Red CP1 ( $+ $clr(red) $+ Red Base $+ $clr $+ )
  }
  elseif ($1 = Granary_cap_cp3) {
    ;return Control Point: 3 (CENTER). CP Name: Granary CP3 (Center)
    return Granary CP3 ( $+ $clr(purple) $+ Middle $+ $clr $+ )
  }
  elseif ($1 = Granary_cap_blue_cp2) {
    ;return Control Point: 2 (Blue). CP Name: Granary Blue CP2 ( $+ $clr(blue) $+ Blue House $+ $clr $+ )
    return Granary Blue CP2 ( $+ $clr(blue) $+ Blue House $+ $clr $+ )
  }
  elseif ($1 = Granary_cap_blue_cp1) {
    ;return Control Point: 1 (Blue). CP Name: Granary Blue CP1 ( $+ $clr(blue) Blue Base $+ $clr $+ )
    return Granary Blue CP1 ( $+ $clr(blue) $+ Blue Base $+ $clr $+ )
  }
  elseif ($1 = #Well_cap_center) {
    return Well CP3 ( $+ $clr(purple) $+ Middle $+ $clr $+ )
  }
  elseif ($1 = #Well_cap_blue_two) {
    return Well Blue CP2 ( $+ $clr(blue) $+ Blue Warehouse $+ $clr $+ )
  }
  elseif ($1 = #Well_cap_blue_rocket) {
    return Well Blue CP1 ( $+ $clr(blue) $+ Blue Base $+ $clr $+ )
  }
  elseif ($1 = #Well_cap_red_two) {
    return Well Red CP2 ( $+ $clr(red) $+ Red Warehouse $+ $clr $+ )
  }
  elseif ($1 = #Well_cap_red_rocket) {
    return Well Red CP1 ( $+ $clr(red) $+ Red Base $+ $clr $+ )
  }
  elseif ($1 = #Badlands_cap_cp3) {
    return Badlands CP3 ( $+ $clr(purple) $+ Middle $+ $clr $+ )
  }
  elseif ($1 = #Badlands_cap_blue_cp2) {
    return Badlands Blue CP2 ( $+ $clr(blue) $+ Blue Spire $+ $clr $+ )
  }
  elseif ($1 = #Badlands_cap_blue_cp1) {
    return Badlands Blue CP1 ( $+ $clr(blue) $+ Blue Warehouse $+ $clr $+ )
  }
  elseif ($1 = #Badlands_cap_red_cp2) {
    return Badlands Red CP2 ( $+ $clr(red) $+ Red Spire $+ $clr $+ )
  }
  elseif ($1 = #Badlands_cap_red_cp1) {
    return Badlands Red CP1 ( $+ $clr(red) $+ Red Warehouse $+ $clr $+ )
  }
  else {
    return $replace($1-,_,$chr(32))
  }
}

alias gswrite {
  writeini -n gamescores.ini $+(g,$1) $2 $3-
}

alias remnonalpha {
  ;removes all non alphanumeric characters from player's names

  var %x = 1, %y = $len($1-), %line = $1-

  while (%x <= %y) {
    var %char = $mid($1-,%x,1)

    if (%char !isalpha) {
      %line = $remove(%line,%char)
    }

    inc %x
  }

  return %line
}

on *:udpread:rcon:{
  if ($sockerr > 0) {
    return
  }
  :nextread
  sockread -f %rcon.data
  if ($sockbr == 0) {
    return
  }
  if (%rcon.data == $null) {
    goto nextread
  }
  else {

    if ($regex(%rcon.data,rcon from "(.*)": command "(.*)")) {
      if (tf_tournament_classlimit* iswm $regml(2)) {
        rcon exec classlimit.cfg
      }
      if ($istok($regml(2),sv_password,32)) && ($ip !isin $regml(1)) {
        rcon sv_password %serverpass
      }
    }

    if ($regex(%rcon.data,.*Your server will be restarted on map change*)) {
      msg %pug.adminchan $codes(Master server issuing restart for update message. Updating)
      ssh restart
    }

    if ($regex(%rcon.data,Log file started \x28file "(.*?)"\x29 \x28game "(.*?)"\x29)) && (%match.on) {
      write %statsdir $+ logs.txt %gamesplayed $regml(2) $+ / $+ $regml(1)
    }

    if ($regex(%rcon.data,"(.+)<(\d+)><(.+)><(Blue|Red)>" disconnected \x28reason "(.*)"\x29)) {
      echo @rcon $regml(3) disconnected.
      echo -s $regml(3) Disconnected
      killchan $regml(1) ( $+ $regml(3) $+ ) disconnected. Reason: $regml(5)
      if (%match.on) && ($timer(timeleft).secs > 240) && ($abs($calc(%score.red - %score.blue)) < 4) {
        chan 4 $+ $regml(1) ( $+ $regml(3) $+ ) disconnected. Reason: $regml(5)
        write %statsdir $+ connection_logs\ $+ %gamesplayed $+ .txt $date(dd-mm-yy) $time(hh:mm:ss) - $regml(1) ( $+ $regml(3) $+ ) disconnected while the pug was in progress. Reason: $regml(5)
      }
      if ($regml(3) == %pug.admin) {
        unset %pug.admin
        %adminpass = new
        rcon say $regml(1) left. Adminpass is now 'new'.
      }
      ;"ch3x<23><STEAM_0:0:20730909><Blue>" disconnected (reason "ch3x timed out")
      left $regml(3)
    }

    if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><>" connected. address "(.*)")) {
      echo @country Checking country for $regml(1) - $regml(3) ( $+ $regml(4) $+ )

      if ($regml(1) != SourceTV) && (!%match.on) { write %statsdir $+ game $+ $calc(%gamesplayed + 1) $+ .txt $date(dd-mm-yy) $time(hh:mm:ss) - $regml(1) ( $+ $regml(3) $+ ) connected, address: $gettok($regml(4),1,58) }

      if ($gettok($regml(3),3,58) > 70000000) && (!$read(%statsdir $+ allowedids.txt,w,* $+ $regml(3) $+ *) && (0)) {
        rcon banid 0 $regml(3) ; kickid $regml(3) New steam account - speak to an admin ; say $regml(1) is a new steam account - automatically banned until approved; writeid

        msg %pug.adminchan $codes($regml(1) ( $+ $regml(3) $+ ) is a newly registered steam account (ID is > 70000000). Automatically banned. IP: $regml(4))

        write %statsdir $+ bannedids.txt $+($regml(3),$chr(37),$me,$chr(37),$regml(1),$chr(37),New account autoban,$chr(37),$date(ddd dd/mm/yyyy HH:nn:ss))
        upload bans %statsdir $+ bannedids.txt

        goto serversay
      }

      %playerip = $gettok($regml(4),1,58)
      %playername = $regml(1)
      %playerid = $regml(3)
      %playerlocation = $geoip.country(%playerip)

      echo @country %playername ( $+ %playerid $+ ) is from %playerlocation
      rcon say %playername ( $+ %playerlocation $+ ) connected
    }

    ;"(.+)<(\d+)><(.+)><>" STEAM USERID validated
    if ($regex(%rcon.data,"(.+)<(\d+)><(.+)><>" STEAM USERID validated)) {
      chan 12 $+ $regml(1) ( $+ $regml(3) $+ ) validated ( $+ %playerlocation $+ ). Total connected: $calc($lines(joined.txt) + 1)
      if (%match.on) { write %statsdir $+ connection_logs\ $+ %gamesplayed $+ .txt $date(dd-mm-yy) $time(hh:mm:ss) - $regml(1) ( $+ $regml(3) $+ ) connected while the pug was in progress. }
      killchan $regml(1) ( $+ $regml(3) $+ ) validated ( $+ %playerlocation $+ ). Total connected: $calc($lines(joined.txt) + 1)
      if ($read(%statsdir $+ bannedids.txt,w,$regml(3) $+ $chr(37) $+ *)) {
        tokenize 37 $v1
        rcon banid 10 $regml(3) ; kickid $regml(3) Banned. #tf2pug; say $regml(1) has been kicked from the server - banned from pugs.
        msg %pug.chan $codes($regml(1) ( $+ $regml(3) $+ ) has been removed from the server (banned player). Reason: $4 $+ )
        msg %pug.adminchan $codes($regml(1) ( $+ $regml(3) $+ ) has been removed from the server (banned player). Reason: $4 $+ )
      }
      elseif (%playerlocation != AU) && (%playerlocation != NZ) && (%playername != SourceTV) && (%kickos) && (%playerlocation != $null) {
        rcon kickid $regml(3) AU/NZ only
        rcon say $regml(1) has been kicked from the server - AU/NZ only. $regml(1) is from %playerlocation
        echo @country 4 $+ $regml(1) is NOT OK! Deporting him...
        echo @country $regml(1) has been kicked from the server ( $+ %playerlocation $+ )
        msg %pug.chan $codes($regml(1) ( $+ $regml(3) $+ ) has been removed from the server, AU/NZ only)
      }
      else {
        write connected_players.txt $regml(3)
        echo @country 9 $+ $regml(1) is OK!
        joined $regml(3) $regml(1)

        add $regml(3) name $left($remnonalpha($regml(1)),16)
        addweapon $regml(3)
      }
    }

    if ($regex(%rcon.data,"(.+)<(\d+)><(.+)><(Red|Blue|Spectator)>" (say|say_team) "(.+)")) {
      if ((lemonparty isin $regml(6-)) || (meatspin isin $regml(6-)) || (tubgirl isin $regml(6-)) || ((lemon isin $regml(6-)) && (party isin $regml(6-))) || (jizz isin $regml(6-)) || (http isin $regml(6-))) { 
        goto serversay 
      }

      if ($regml(5) = say) { killchan $clr($regml(4)) $+ $regml(1) says: $+ $clr $regml(6) }
      if ($regml(5) = say_team) { killchan $clr($regml(4)) $+ $regml(1) says $clr(yellow) $+ (team) $+ $clr(blue) $+ : $+ $clr $regml(6) }
      tokenize 32 $regml(6)
      ;log in-game chat
      if (%match.on) {
        write $+(%statsdir,chat\,%gamesplayed,.txt) $+(%gamesplayed,$chr(37),$regml(4),$chr(37),$regml(5),$chr(37),$regml(3),$chr(37),$chatreplace($regml(6)))
      }
      if (%pug) && (!%match.on) {
        write $+(%statsdir,chat\,$calc(%gamesplayed + 1),.txt) $+($calc(%gamesplayed + 1),$chr(37),$regml(4),$chr(37),$regml(5),$chr(37),$regml(3),$chr(37),$chatreplace($regml(6)))
      }
      if ($1 = !teams) {
        if (%team1) {
          updateteams
          rcon say Blue: %team1
          rcon say Red: %team2
        }
      }
      if ($1 == !players) {
        rcon say Players: %players
      }
      if ($1 = !medic) {
        if (%medics) {
          if ($timer(timeleft).secs <= 1200) && (!%get) {
            rcon say It is too late to volunteer to medic. It must be done before 10 minutes have passed
          }
          else {
            if (($regml(3) == %prevredmed) || ($regml(3) == %prevbluemed)) {
              rcon say You're already medic!
              goto serversay
            }
            if ($istok(%team1ids,$regml(3),32)) {
              hload medtable %statsdir $+ pugmedtable
              hload lastmed %statsdir $+ lastmedtable.med

              if (%volunteermed. [ $+ [ $regml(4) ] ]) {
                var %volunteerid = $v1
                ;because we inc %prevbluemed later, we need to take it down by two so in net we're only taking it down by 1
                hdec lastmed %volunteerid 2
                hadd medtable %volunteerid $calc($hget(medtable,%volunteerid) - 4)
              }

              var %prevmedname = $getnamebyid(%prevbluemed)

              echo @medic $clr($regml(4)) $+ $regml(1) $+ $clr has volunteered to be $clr($regml(4)) $+ medic $+ $clr $+ . $clr(green) $+ %prevmedname $+ $clr no longer needs to medic.
              rcon say $regml(1) has volunteered to be medic. %prevmedname no longer needs to medic
              killchan $clr($regml(4)) $+ $regml(1) $+ $clr has volunteered to be $clr($regml(4)) $+ medic $+ $clr $+ . $clr($regml(4)) $+ %prevmedname $+ $clr no longer needs to medic.

              hadd lastmed $regml(3) 1
              hinc lastmed %prevbluemed 1

              var %newmed.playernum = $findtok(%team1ids,$regml(3),1,32)
              set %player.blue1 $regml(3)
              set %player.blue [ $+ [ %newmed.playernum ] ] %prevbluemed

              updateteams

              set %prevbluemed $regml(3)
              hadd medtable $regml(3) $calc($hget(medtable,$regml(3)) + 4)

              set % [ $+ volunteermed. $+ [ $regml(4) ] ] $regml(3)

              hsave medtable %statsdir $+ pugmedtable
              hsave lastmed %statsdir $+ lastmedtable.med              
            }
            elseif ($istok(%team2ids,$regml(3),32)) {
              hload medtable %statsdir $+ pugmedtable
              hload lastmed %statsdir $+ lastmedtable.med

              if (%volunteermed. [ $+ [ $regml(4) ] ]) {
                var %volunteerid = $v1
                ;because we inc %prevredmed later, we need to take it down by two so in net we're only taking it down by 1
                hdec lastmed %volunteerid 2
                hadd medtable %volunteerid $calc($hget(medtable,%volunteerid) - 4)
              }

              var %prevmedname = $getnamebyid(%prevredmed)

              echo @medic $clr($regml(4)) $+ $regml(1) $+ $clr has volunteered to be $clr($regml(4)) $+ medic $+ $clr $+ . $clr(green) $+ %prevmedname $+ $clr no longer needs to medic.
              rcon say $regml(1) has volunteered to be medic. %prevmedname no longer needs to medic.
              killchan $clr($regml(4)) $+ $regml(1) $+ $clr has volunteered to be $clr($regml(4)) $+ medic $+ $clr $+ . $clr($regml(4)) $+ %prevmedname $+ $clr no longer needs to medic.

              hadd lastmed $regml(3) 1
              hinc lastmed %prevredmed 1

              var %newmed.playernum = $findtok(%team2ids,$regml(3),1,32)
              set %player.red1 $regml(3)
              set %player.red [ $+ [ %newmed.playernum ] ] %prevredmed

              updateteams

              set %prevredmed $regml(3)
              hadd medtable $regml(3) $calc($hget(medtable,$regml(3)) + 4)

              set %volunteermed. [ $+ [ $regml(4) ] ] $regml(3)

              hsave medtable %statsdir $+ pugmedtable
              hsave lastmed %statsdir $+ lastmedtable.med
            }
            else {
              hload medtable %statsdir $+ pugmedtable
              hload lastmed %statsdir $+ lastmedtable.med

              if (%volunteermed. [ $+ [ $regml(4) ] ]) {
                var %volunteerid = $v1
                ;because we inc %prevmed later, we need to take it down by two so in net we're only taking it down by 1
                hdec lastmed %volunteerid 2
                ;subtract our 2 game med value
                hadd medtable %volunteerid $calc($hget(medtable,%volunteerid) - 4)
              }

              var %team = $lower($regml(4))

              var %prevmed = %prev [ $+ [ %team ] $+ med ]
              var %prevmedname = $getnamebyid(%prevmed)

              echo @medic $clr($regml(4)) $+ $regml(1) $+ $clr has volunteered to be $clr($regml(4)) $+ medic $+ $clr $+ . $clr(green) $+ %prevmedname $+ $clr no longer needs to medic.
              rcon say $regml(1) has volunteered to be medic. %prevmedname no longer needs to medic.
              killchan $clr($regml(4)) $+ $regml(1) $+ $clr has volunteered to be $clr($regml(4)) $+ medic $+ $clr $+ . $clr($regml(4)) $+ %prevmedname $+ $clr no longer needs to medic.

              hadd lastmed $regml(3) 1
              hinc lastmed %prevmed 1

              set %player. [ $+ [ %team ] $+ 1 ] $regml(3)

              updateteams

              set %prev [ $+ [ %team ] $+ med ] $regml(3)
              if ($hget(medtable,$regml(3))) {
                hadd medtable $regml(3) $calc($v1 + 4)
              }
              else {
                hadd medtable $regml(3) 4
              }

              set %volunteermed. [ $+ [ $regml(4) ] ] $regml(3)

              hsave medtable %statsdir $+ pugmedtable
              hsave lastmed %statsdir $+ lastmedtable.med

              unset %get
            }
          }
        }
        else {
          rcon say Please wait until teams have been made before volunteering to be medic
        }
      }

      if ($1 = !newadmin) {
        if (%newadmin) { return }

        if (!%vote. [ $+ [ $regml(3) ] ]) {
          set %vote. [ $+ [ $regml(3) ] ] 1
          if (%vote.newadmin) {
            inc %vote.newadmin
            if (%vote.newadmin >= 5) {
              set %newadmin 1
              unset %vote.*
              unset %pug.admin
              %adminpass = $lower($read(pass.txt))
              rcon say New admin password is %adminpass
              chan New admin password is %adminpass
            }
            else {
              rcon say $regml(1) has voted for a new admin password. $+($chr(40),$calc(5 - %vote.newadmin),$chr(32),more votes required,$chr(41))
              chan $regml(1) has voted for a new admin password. $+($chr(40),$calc(5 - %vote.newadmin),$chr(32),more votes required,$chr(41))
            }
          }
          else {
            set %vote.newadmin 1
            rcon say $regml(1) has voted for a new admin password. (4 more votes required)
            chan $regml(1) has voted for a new admin password. (4 more votes required)
          }
        }
      }

      if ($1 = !login) {
        if ($2 = %adminpass) {
          if (%pug.admin) { goto serversay }
          else {
            rcon say $regml(1) ( $+ $regml(3) $+ ) logged in.; pug_admin $regml(3)
            chan $regml(1) ( $+ $regml(3) $+ ) logged in.
            %pug.admin = $regml(3)
          }
        }
      }
      if ($1 = !pick) {
        if (%captains) && ($2) {
          if (!$read(picks.txt,w,* $+ $chr(37) $+ * $+ $2 $+ *)) { rcon say Invalid pick specified. Please use !pick <name> or partial name shown in the picklist | goto serversay }
          if ($regml(3) == %team1captain) || ($regml(3) == %team2captain) {
            var %captainpick
            if ($calc(%pick % 2)) { %captainpick = %team2captain }
            else { %captainpick = %team1captain }
            if ($regml(3) == %captainpick) {
              pickplayer $regml(3) $2
            }
            else {
              rcon say It is currently $getnamebyid(%captainpick) $+ 's turn to pick
            }
          }
          else {
            rcon say Only captains can pick teams!
          }
        }
      }
      if ($1 = !picklist) {
        if (%captains) {
          if ($regml(3) == %team1captain) || ($regml(3) == %team2captain) {
            var %x = 1, %y = $lines(picks.txt)
            var %availablePicks
            while (%x <= %y) {
              %availablePicks = $addtok(%availablePicks,say $gettok($read(picks.txt,%x),2,37),59)
              inc %x
            }
            rcon say Players available for picking:; %availablePicks
          }
        }
      }
      if (($regml(3) = %pug.admin) || ($istok(%adminids,$regml(3),32))) {
        if ($1 == !irc) && ($2) {
          rcon say Message sent to IRC: $2-
          chan $regml(1) (In-game): $2-
          msg %pug.adminchan $codes($regml(1) (In-Game PUG $chr(35) $+ %gamesplayed $+ ): $2-)
        }
        if ($1 == !get) {
          if (%noget) {
            rcon say You must wait $timer(get).secs seconds before you can use !get
          }
          else {
            chan Requesting player! The server details are: connect %rcon.ip $+ : $+ %rcon.port $+ ;password %serverpass
            msg #ozfortress $codes(#tf2pug requires another player! The server details are: connect %rcon.ip $+ : $+ %rcon.port $+ ;password %serverpass)
            set %get 1
            rcon say Requesting player
          }
        }
        if ($1 == !start) {
          if (!%team1) { rcon say Cannot start without teams | goto serversay }
          if (%match.on) { 
            .timertimeleft 1 1800 timeleft
            rcon exec start.cfg

            set %score.red 0
            set %score.blue 0

            .copy -o stats_start.txt stats.txt
          }
          else {
            newtopic Pug in progress on %win.map

            set %match.on 1

            inctimesplayed
            %gamesplayed = $readini(gamescores.ini,global,gamesplayed)
            inc %gamesplayed

            %pug.demo = pug- $+ $date(HH-nn-ddd-dd-m-yyyy) $+ - $+ %win.map $+ - $+ %gamesplayed
            rcon tv_record %pug.demo ; exec start.cfg; mp_friendlyfire 0; log on; livelogs_name #tf2pug %gamesplayed

            write %statsdir $+ demos.txt $+(%pug.demo,.zip)
            writeini -n gamescores.ini global gamesplayed %gamesplayed
            write %statsdir $+ gamesplayed.txt %gamesplayed

            var %mn = $iif($read(%statsdir $+ maps.txt,w,%win.map $+ $chr(37) $+ *),$ifmatch,$+(%win.map,$chr(37),0))
            write -w* $+ %win.map $+ * %statsdir $+ maps.txt $puttok(%mn,$calc($gettok(%mn,2,37) + 1),2,37)

            .timertimeleft 1 1800 timeleft
            .copy -a %statsdir $+ game $+ %gamesplayed $+ .txt %statsdir $+ connections_log.txt
            .remove %statsdir $+ game $+ %gamesplayed $+ .txt

            .copy -o stats.txt stats_start.txt

            set %score.red 0
            set %score.blue 0
          }

          chan The game is going live!
          killchan The game is starting!
          killchan The game is starting!
          killchan The game is starting!
        }
        if ($1 == !endpug) {
          if (%endpug) { return }
          if (!$timer(noendpug)) {
            if (%match.on) && (($timer(timeleft).secs < 1200) || (!$timer(timeleft))) {
              .timer 1 5 spamstats
            }
            elseif (%pug) && ($timer(timeleft).secs > 1200) {
              .timer 1 5 spamstats
            }
            elseif (%pug) && (!%match.on) {
              endpug
            }
            else {
              msg %pug.adminchan $codes(Something weird happened and neither endpug cases are valid... match.on: %match.on pug: %pug timeleft: $timer(timeleft).secs)
            }
          }
          else {
            rcon say You must wait $round($timer(noendpug).secs,0) seconds before you can end the pug
          }
        }
      }
      if ($istok(%adminids,$regml(3),32)) {
        if ($1 == !forcestart) && (!%match.on) && ($regml(3) == STEAM_0:1:10325827) {
          set %match.on 1
          ;inctimesplayed
          inc %gamesplayed
          %pug.demo = pug- $+ $date(HH-nn-ddd-dd-m-yyyy) $+ - $+ %win.map $+ - $+ %gamesplayed
          rcon tv_record %pug.demo ; exec start.cfg
          write %statsdir $+ demos.txt $+(%pug.demo,.zip)
          .timertimeleft 1 1800 timeleft
          .copy -a %statsdir $+ game $+ %gamesplayed $+ .txt %statsdir $+ connections_log.txt
          .remove %statsdir $+ game $+ %gamesplayed $+ .txt
        }
        if ($1 = !password) && ($2) {
          rcon sv_password $2 $+ ; say Password changed to $2
          %serverpass = $2
        }
        if ($1 = !cmd) && ($regml(3) == STEAM_0:1:10325827) {
          write -c cmd.txt
          write cmd.txt $2-
          .play -c %pug.chan cmd.txt
        }
        if ($1 == !k || $1 == !kick) {
          rcon kick $2-
        }
        if (($1 == !kickid) || ($1 = !kid)) {
          rcon kickid $2-
        }
      }
    }
    if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" changed role to "(.*)")) {
      killchan $clr($regml(4)) $+ $regml(1) $+ $clr changed class to $clr($regml(4)) $+ $regml(5) $+ $clr
      playerclass $regml(3) $regml(5)
    }
    if (%match.on == 1) {
      if ($regex(%rcon.data,World triggered "Round_Win".+winner.+"(Blue|Red)")) {
        chan Round win: $clr($regml(1)) $+ $regml(1) $+ $clr won the round!
        killstreak endround
      }
      if ($regex(%rcon.data,World triggered "Round_Overtime")) {
        chan Round overtime!
      }
      if ($regex(%rcon.data,World triggered "Round_Length".+seconds.+"(\d+.\d+))) {
        chan Round length: $clr(orange) $+ $floor($calc($regml(1) / 60)) $+ $clr minutes and $clr(orange) $+ $calc($regml(1) % 60) $+ $clr seconds
      }
      if ($regex(%rcon.data,World triggered "Round_Start")) {
        chan Round start
      }
      if ($regex(%rcon.data,World triggered "Round_Setup_Begin")) {
        chan Round setup begin
      }
      if ($regex(%rcon.data,World triggered "Mini_Round_Win" \x28winner "(Blue|Red)"\x29 \x28round "round_(\d+)"\x29)) {
        chan Mini round $chr(35) $+ $regml(2) win: $clr($regml(1)) $+ $regml(1) $+ $clr
      }
      if ($regex(%rcon.data,World triggered "Mini_Round_Length" \x28seconds "(\d+.\d+)"\x29)) {
        chan Mini round length: $regml(1)
      }
      if ($regex(%rcon.data,World triggered "Round_Setup_End")) {
        chan Round setup end
      }
      ;Format: "%s<%i><%s><%s>" triggered "medic_death" against "%s<%i><%s><%s>" (healing "%d") (ubercharge "%s")
      ;healing is the amount the Medic healed in that life
      ;ubercharge (1/0) is whether they died with a full charge
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "medic_death" against "(.*)<(\d+)><(.*)><(Red|Blue)>" \x28healing "(.*)"\x29 \x28ubercharge "(.*)"\x29)) {
        ;echo @medic %rcon.data
        ;add $regml(7) healing $regml(9)
        ;add $regml(7) points $round($calc($regml(9) / 500),2)
        medcalc $regml(8) 0 $regml(10)
        if ($regml(10) = 1) {
          killchan $clr($regml(8)) $+ $regml(5) $+ $clr(orange) $kdratio($regml(7)) $+ $clr healed a total of $clr($regml(8)) $+ $regml(9) $+ $clr and died with uber! $read(medphrase.txt)
          add $regml(7) uberlost 1
          rcon say $remove($regml(5),$chr(59),$chr(124)) lost uber! $read(medphrase.txt)
        }
        if ($regml(10) = 0) {
          killchan $clr($regml(8)) $+ $regml(5) $+ $clr(orange) $kdratio($regml(7)) $+ $clr died and did NOT lose uber after healing a total of $clr($regml(8)) $+ $regml(9)
        }
      }
      ;"vsn.RynoCerus<6><STEAM_0:0:23192637><Blue>" triggered "healed" against "Hyperbrole<3><STEAM_0:1:22674758><Blue>" (healing "26")
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "healed" against "(.*)<(\d+)><(.*)><(Red|Blue)>" \x28healing "(.*)"\x29)) {
        ;medcalc colour healing uber
        medcalc $regml(4) $regml(9)
        add $regml(3) healing $regml(9)
        add $regml(7) healing_received $regml(9)
        add $regml(3) points $round($calc($regml(9) / 500),2)
      }
      ;Format: "%s<%i><%s><%s>" committed suicide with "world" (customkill "%s") (attacker_position "%d %d %d")
      ;07/03/2010 - 14:04:16: "Hypnos<20><STEAM_0:0:24915059><Red>" committed suicide with "world" (customkill "train") (attacker_position "568 397 -511")
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" committed suicide with "(.*)" \x28customkill "(.*?)"\x29)) {
        killchan $clr($regml(4)) $+ $regml(1) $clr(orange) $+ $kdratio($regml(3)) $+ $clr was killed by a $clr(purple) $+ $regml(6) $+ $clr
        add $regml(3) deaths 1
      }
      ;"oxide<7><STEAM_0:0:3622998><Blue>" triggered "damage" (damage "22")
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "damage" against "(.*)<(\d+)><(.*)><(Red|Blue)>" \x28damage "(\d+)"\x29)) {
        if ($regml(9)) {
          add $regml(3) damage $regml(9)
        }
      }
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" killed "(.*)<(\d+)><(.*)><(Red|Blue)>" with "(.*)" \x28customkill "(\w+)"\x29)) {
        killstreak $regml(3) $regml(7)
        set -u5 %killer $regml(1)
        set -u5 %killerid $regml(3)
        if ($regml(10) = backstab) {
          add $regml(7) death 1
          add $regml(3) kill 1
          addweapon $regml(3) $regml(9)
          add $regml(3) points 2
          killchan $clr($regml(4)) $+ $regml(1) $clr(orange) $+ $kdratio($regml(3)) $+ $clr killed $clr($regml(8)) $+ $regml(5) $clr(orange) $+ $kdratio($regml(7)) $+ $clr with $clr(purple) $+ $replacenames($regml(9)) $+ $clr ( $+ $clr(green) $+ $regml(10) $+ $clr $+ )
        }
        if ($regml(10) = headshot) {
          add $regml(7) death 1
          add $regml(3) kill 1
          addweapon $regml(3) $regml(9)
          add $regml(3) points 2
          killchan $clr($regml(4)) $+ $regml(1) $clr(orange) $+ $kdratio($regml(3)) $+ $clr killed $clr($regml(8)) $+ $regml(5) $clr(orange) $+ $kdratio($regml(7)) $+ $clr with $clr(purple) $+ $replacenames($regml(9)) $+ $clr ( $+ $clr(green) $+ $regml(10) $+ $clr $+ )
        }
        if ($regml(10) = feign_death) {
          ;add $regml(3) kill 1
          ;add $regml(3) points 1
          killchan $clr($regml(4)) $+ $regml(1) $clr(orange) $+ $kdratio($regml(3)) $+ $clr killed $clr($regml(8)) $+ $regml(5) $clr(orange) $+ $kdratio($regml(7)) $+ $clr with $clr(purple) $+ $replacenames($regml(9)) $+ $clr $+ ... Wait! ( $+ $clr(green) $+ $regml(10) $+ $clr $+ )
        }
      }
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" killed "(.*)<(\d+)><(.*)><(Red|Blue)>" with "(.*)" \x28att.+)) {
        ;inc %killstreak. [ $+ [ $regml(3) ] ]
        ;if (%killstreak. [ $+ [ $regml(7) ] ]) { unset %killstreak. [ $+ [ $regml(7) ] ] }
        killstreak $regml(3) $regml(7)
        set -u5 %killer $regml(1)
        set -u5 %killerid $regml(3)
        add $regml(3) kill 1
        add $regml(3) points 1
        add $regml(7) death 1
        addweapon $regml(3) $regml(9)
        killchan $clr($regml(4)) $+ $regml(1) $clr(orange) $+ $kdratio($regml(3)) $+ $clr killed $clr($regml(8)) $+ $regml(5) $clr(orange) $+ $kdratio($regml(7)) $+ $clr with $clr(purple) $+ $replacenames($regml(9)) $+ $clr
      }
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "kill assist" against "(.*)<(\d+)><(.*)><(Red|Blue)>")) {
        killchan $clr($regml(4)) $+ $regml(1) $+ $clr(orange) $kdratio($regml(3)) $+ $clr assisted $clr($regml(4)) $+ %killer $+ $clr(orange) $kdratio(%killerid) $+ $clr in killing $clr($regml(8)) $+ $regml(5) $+ $clr(orange) $kdratio($regml(7)) $+ $clr
        add $regml(3) assist 1
        add $regml(3) points 0.5
      }
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "flagevent" .event "(.*)". .posi)) {
        if ($regml(5) = captured) {
          if ($regml(4) = blue) {
            chan $clr($regml(4)) $+ $regml(1) $+ $clr captured the $clr(red) $+ Red $+ $clr intelligence.
          }
          elseif ($regml(4) = red) {
            chan $clr($regml(4)) $+ $regml(1) $+ $clr captured the $clr(blue) $+ Blue $+ $clr intelligence.
          }
          add $regml(3) cap 1
          add $regml(3) points 2
        }
        if ($regml(5) = defended) {
          chan $clr($regml(4)) $+ $regml(1) $+ $clr defended the $clr($regml(4)) $+ $regml(4) $+ $clr intelligence.
          add $regml(3) points 1
        }
      }
      if ($regex(%rcon.data,/Team "(Blue|Red)" triggered "pointcaptured" \x28cp "(\d+)"\x29 \x28cpname "(.+)"\x29 \x28numcappers "(\d+)".+/)) {
        chan $clr($regml(1)) $+ $regml(1) $+ $clr captured $replacenames($regml(3)) $+ .
        var %numcappers = $regml(4)
        chan Number of cappers: %numcappers
        var %str1 = \x28player(\d) "(.+)<(\d+)><(\S+)><(Blue|Red)>"\x29.+
        var %x = 0
        var %str = $str(%str1,%numcappers)
        if ($regex(%rcon.data,%str)) {
          while (%x < %numcappers) {
            chan Capper $chr(35) $+ $regml($calc(1 + (5 * %x))) $+ : $clr($regml($calc(5 + (5 * %x)))) $+ $regml($calc(2 + (%x * 5))) $+ $clr $kdratio($regml($calc(4 + (%x * 5))))
            add $regml($calc(4 + (5 * %x))) cap 1
            add $regml($calc(4 + (5 * %x))) points 2
            inc %x   
          }
        }
      }
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "domination" against "(.*)<(\d+)><(.*)><(Red|Blue)>")) {
        killchan $clr($regml(4)) $+ $regml(1) $+ $clr(orange) $kdratio($regml(3)) $+ $clr is dominating $clr($regml(8)) $+ $regml(5) $+ $clr(orange) $kdratio($regml(7)) $+ $clr
      }
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "revenge" against "(.*)<(\d+)><(.*)><(Red|Blue)>")) {
        killchan $clr($regml(4)) $+ $regml(1) $+ $clr(orange) $kdratio($regml(3)) $+ $clr got revenge against $clr($regml(8)) $+ $regml(5) $+ $clr(orange) $kdratio($regml(7)) $+ $clr
        add $regml(3) points 1
      }
      ;"dcup<109><STEAM_0:0:15236776><Red>" triggered "killedobject" (object "OBJ_SENTRYGUN") (weapon "tf_projectile_pipe") (objectowner "NsS. oLiVz<101><STEAM_0:1:15674014><Blue>") (attacker_position "551 2559 216")
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "killedobject" .object "(.*)". .weapon "(.*)". .objectowner "(.*)<(\d+)><(.*)><(Blue|Red)>)") {
        killchan $clr($regml(4)) $+ $regml(1) $+ $clr destroyed a $clr(purple) $+ $replacenames($regml(5)) $+ $clr built by $clr($regml(10)) $+ $regml(7) $+ $clr
        add $regml(3) points 1
      }
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "chargedeployed")) {
        killchan $clr($regml(4)) $+ $regml(1) $+ $clr used $clr($regml(4)) $+ ubercharge $+ $clr
        inc %uber. [ $+ [ $regml(4) ] ]
        add $regml(3) points 1
      }
      ;"skae<14><STEAM_0:1:31647857><Red>" picked up item "ammopack_medium"
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" picked up item "(.*)")) {
        add $regml(3) $regml(5) 1
        killchan $clr($regml(4)) $+ $regml(1) $+ $clr picked up $clr(green) $+ $replacenames($regml(5)) $+ $clr
      }
      ;"pvtx<103><STEAM_0:1:7540588><Red>" triggered "captureblocked" (cp "1") (cpname "Control Point B") (position "-2143 2284 156")
      if ($regex(%rcon.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "captureblocked".+cp "(\d+)".+.+cpname "#?(\w*)".+)) {
        killchan $clr($regml(4)) $+ $regml(1) $+ $clr blocked CP $+ $regml(5) $replacenames($regml(6))
        add $regml(3) points 1
      }
      if ($regex(%rcon.data,Team "(Blue|Red)" current score "(\d+)" with "(\d+)" players)) {
        chan $clr($regml(1)) $+ $regml(1) $+ $clr current score: $clr($regml(1)) $+ $regml(2) $+ $clr with $clr($regml(1)) $+ $regml(3) $+ $clr players
        killchan $clr($regml(1)) $+ $regml(1) $+ $clr current score: $clr($regml(1)) $+ $regml(2) $+ $clr with $clr($regml(1)) $+ $regml(3) $+ $clr players
        set %score. [ $+ [ $regml(1) ] ] $regml(2)
        if ($regml(1) = Blue) && ($timeleft()) { killchan $timeleft() | chan $timeleft() }
      }
      if ($regex(%rcon.data,Team "(Blue|Red)" final score "(\d+)" with "(\d+)" players)) {
        chan $clr($regml(1)) $+ $regml(1) $+ $clr final score: $clr($regml(1)) $+ $regml(2) $+ $clr with $clr($regml(1)) $+ $regml(3) $+ $clr players
        killchan $clr($regml(1)) $+ $regml(1) $+ $clr final score: $clr($regml(1)) $+ $regml(2) $+ $clr with $clr($regml(1)) $+ $regml(3) $+ $clr players
        gswrite %gamesplayed fs [ $+ [ $lower($regml(1)) ] ] $regml(2)
        gswrite %gamesplayed players [ $+ [ $lower($regml(1)) ] ] $regml(3)
      }
      if ($regex(%rcon.data,World triggered "Game_Over" reason "(.*)")) {
        chan Game over: $clr(green) $+ $regml(1) $+ $clr
        winlossdraw
        .timer 1 2 unset %match.on
        gswrite %gamesplayed winreason $regml(1)
        if (%team1) { gswrite %gamesplayed blueteam %team1 }
        if (%team2) { gswrite %gamesplayed redteam %team2 }
        if (%players) { gswrite %gamesplayed players %players }
        if (%player.red.score) { gswrite %gamesplayed redplayerscore %player.red.score }
        if (%player.blue.score) { gswrite %gamesplayed blueplayerscore %player.blue.score }
        if (%win.map) { gswrite %gamesplayed mapplayed %win.map }
        killchan Game over: $clr(green) $+ $regml(1) $+ $clr
        killchan Waiting for the next pug to start
        killchan Waiting for the next pug to start
        killchan Waiting for the next pug to start
        rcon tv_stoprecord
        .timer 1 2 spamstats
      }
    }
    :serversay
    if (%rcon.data != $null) {
      echo @rcon %rcon.data
      .timerlogs off
      .timerlogs 1 1200 rcon.logs
      unset %rcon.data
    }
    goto nextread
  }
}

alias playerclass {
  if ($2) {
    set %class. [ $+ [ $1 ] ] $2
  }
  else {
    if (%class. [ $+ [ $1 ] ]) {
      return ( $+ $upper($left(%class. [ $+ [ $1 ] ],1)) $+ $mid(%class. [ $+ [ $1 ] ],2,$len(%class. [ $+ [ $1 ] ])) $+ )
    }
    else {
      return ( $+ N/A $+ )
    }
  }
}

alias winlossdraw {
  var %ppt = 6
  var %x = 1
  if (%score.red > %score.blue) {
    while (%x <= %ppt) {
      add %player.red [ $+ [ %x ] ] win 1
      add %player.blue [ $+ [ %x ] ] loss 1
      inc %x
    }
  }
  if (%score.red < %score.blue) {
    while (%x <= %ppt) {
      add %player.red [ $+ [ %x ] ] loss 1
      add %player.blue [ $+ [ %x ] ] win 1
      inc %x
    }
  }
  if (%score.red = %score.blue) {
    while (%x <= %ppt) {
      add %player.red [ $+ [ %x ] ] draw 1
      add %player.blue [ $+ [ %x ] ] draw 1
      inc %x
    }
  }
}

alias medcalc {
  ;medcalc colour healing uber
  if (!%medic. [ $+ [ $1 $+ .healing ] ]) && (%medic. [ $+ [ $1 $+ .healing ] ] != 0) {
    %medic. [ $+ [ $1 $+ .healing ] ] = $2
  }
  else {
    %medic. [ $+ [ $1 $+ .healing ] ] = $calc(%medic. [ $+ [ $1 $+ .healing ] ] + $2)
  }
  if ($3) {
    if (!%medic. [ $+ [ $1 $+ .uberlost ] ]) && (%medic. [ $+ [ $1 $+ .uberlost ] ] != 0) {
      %medic. [ $+ [ $1 $+ .uberlost ] ] = $3
    }
    else {
      %medic. [ $+ [ $1 $+ .uberlost ] ] = $calc(%medic. [ $+ [ $1 $+ .uberlost ] ] + $3)
    }
  }
}

alias killstreak {
  ;Clear the killstreaks at the end of a round, so it isn't considered a streak by not dying over multiple rounds
  if ($1 = endround) {
    var %x 1
    var %numplayers = $numtok(%gameids,44)
    while (%numplayers >= %x) {
      if (%killstreak. [ $+ [ $gettok(%gameids,%x,44) ] ]) {
        if (!%killstreak.highest) {
          %killstreak.highestvalue = %killstreak. [ $+ [ $gettok(%gameids,%x,44) ] ]
          %killstreak.highest = $gettok(%gameids,%x,44)
        }
        if (%killstreak. [ $+ [ $gettok(%gameids,%x,44) ] ] > %killstreak.highestvalue) {
          %killstreak.highestvalue = %killstreak. [ $+ [ $gettok(%gameids,%x,44) ] ]
          %killstreak.highest = $gettok(%gameids,%x,44)
        }
      }
      inc %x
    }
    set %highestkillstreak %killstreak.highest
    set %highestkillstreakvalue %killstreak.highestvalue
    unset %killstreak.*
    return
  }
  if (%killstreak. [ $+ [ $1 ] ]) {
    inc %killstreak. [ $+ [ $1 ] ]
  }
  else {
    set %killstreak. [ $+ [ $1 ] ] 1
  }
  if (%killstreak. [ $+ [ $2 ] ]) {
    var %x 1
    var %numplayers = $numtok(%gameids,44)
    while (%numplayers >= %x) {
      if (%killstreak. [ $+ [ $gettok(%gameids,%x,44) ] ]) {
        if (!%killstreak.highest) {
          %killstreak.highestvalue = %killstreak. [ $+ [ $gettok(%gameids,%x,44) ] ]
          %killstreak.highest = $gettok(%gameids,%x,44)
        }
        if (%killstreak. [ $+ [ $gettok(%gameids,%x,44) ] ] > %killstreak.highestvalue) {
          %killstreak.highestvalue = %killstreak. [ $+ [ $gettok(%gameids,%x,44) ] ]
          %killstreak.highest = $gettok(%gameids,%x,44)
        }
      }
      inc %x
    }
    set %highestkillstreak %killstreak.highest
    set %highestkillstreakvalue %killstreak.highestvalue
    unset %killstreak. [ $+ [ $2 ] ]
  }
}

alias incmedtimes {
  if ($hfind(medtable,$1)) {
    hadd medtable $1 $calc($hget(medtable,$1) + 2)
  }
  else {
    ;Here we add the player to the table because he doesn't exist. 2 is the initial value (because we can't have a null (0) integer in a hash table)
    hadd medtable $1 2
  }
}

alias incgamesincemed {
  hload lastmed %statsdir $+ lastmedtable.med
  var %x = 1, %y = $numtok(%team1ids,32)
  while (%x <= %y) {
    if ($gettok(%team1ids,%x,32) != %prevredmed) && ($gettok(%team2ids,%x,32) != %prevbluemed) {
      if (!$hfind(lastmed,$gettok(%team1ids,%x,32))) {
        hadd lastmed $gettok(%team1ids,%x,32) 1
      }
      else {
        hadd lastmed $gettok(%team1ids,%x,32) $calc($hget(lastmed,$gettok(%team1ids,%x,32)) + 1)
      }
      if (!$hfind(lastmed,$gettok(%team2ids,%x,32))) {
        hadd lastmed $gettok(%team2ids,%x,32) 1
      }
      else {
        hadd lastmed $gettok(%team2ids,%x,32) $calc($hget(lastmed,$gettok(%team2ids,%x,32)) + 1)
      }
    }
    inc %x
  }

  hadd lastmed %prevredmed 0
  hadd lastmed %prevbluemed 0

  hsave lastmed %statsdir $+ lastmedtable.med
}

alias medtimes {
  hload medtable %statsdir $+ pugmedtable
  hload lastmed %statsdir $+ lastmedtable.med
  var %numplayers = $numtok(%playerids,44)
  var %currentplayer 1
  if (!%playerids) { msg %pug.adminchan $codes(holy shit playerids isn't set what is going on) | return }
  echo @medic Game $calc(%gamesplayed + 1) playerids: %playerids
  %playerids = $iif(%prevbluemed3,$remtok(%playerids,%prevbluemed3,44),%playerids)
  %playerids = $iif(%prevbluemed2,$remtok(%playerids,%prevbluemed2,44),%playerids)
  %playerids = $iif(%prevbluemed,$remtok(%playerids,%prevbluemed,44),%playerids)
  %playerids = $iif(%prevredmed3,$remtok(%playerids,%prevredmed3,44),%playerids)
  %playerids = $iif(%prevredmed2,$remtok(%playerids,%prevredmed2,44),%playerids)
  %playerids = $iif(%prevredmed,$remtok(%playerids,%prevredmed,44),%playerids)
  while (%numplayers >= %currentplayer) {
    if ($hfind(medtable,$gettok(%playerids,%currentplayer,44))) {
      echo @medic Found $gettok(%playerids,%currentplayer,44) in the med table. Value: $calc($hget(medtable,$gettok(%playerids,%currentplayer,44)) / $iif($gettok($read(%statsdir $+ statsall.txt,w,$gettok(%playerids,%currentplayer,44) $+ *),8,37),$gettok($read(%statsdir $+ statsall.txt,w,$gettok(%playerids,%currentplayer,44) $+ *),8,37),1))
      set %med. [ $+ [ $gettok(%playerids,%currentplayer,44) ] ] $hget(medtable,$gettok(%playerids,%currentplayer,44))
    }
    else {
      set %med. [ $+ [ $gettok(%playerids,%currentplayer,44) ] ] 1
      hadd medtable $gettok(%playerids,%currentplayer,44) 2
    }
    if ($hfind(lastmed,$gettok(%playerids,%currentplayer,44))) {
      echo @medic GAMES SINCE $gettok(%playerids,%currentplayer,44) LAST PLAYED MEDIC: $hget(lastmed,$gettok(%playerids,%currentplayer,44))
      set %lastmed. [ $+ [ $gettok(%playerids,%currentplayer,44) ] ] $hget(lastmed,$gettok(%playerids,%currentplayer,44))
    }
    else {
      set %lastmed. [ $+ [ $gettok(%playerids,%currentplayer,44) ] ] 1
      hadd lastmed $gettok(%playerids,%currentplayer,44) 1
    }
    inc %currentplayer
  }
  return $+($choosebluemed,$chr(59),$chooseredmed)
}

alias choosebluemed {
  var %z 1
  var %numplayers $numtok(%playerids,44)
  echo @medic $clr(Blue) $+ Choosing blue med...
  while (%numplayers >= %z) {
    echo @medic Blue loop $clr(blue) $+ $chr(35) $+ %z $+ $clr
    if (%med. [ $+ [ $gettok(%playerids,%z,44) ] ]) {
      var %medlast = $iif(%lastmed. [ $+ [ $gettok(%playerids,%z,44) ] ],$ifmatch,1)
      var %medscore = $calc(%med. [ $+ [ $gettok(%playerids,%z,44) ] ] / $iif($gettok($read(%statsdir $+ statsall.txt,w,$gettok(%playerids,%z,44) $+ *),8,37),$gettok($read(%statsdir $+ statsall.txt,w,$gettok(%playerids,%z,44) $+ *),8,37),1) / %medlast)
      if (!%med.blue) {
        %med.lastblue = %medlast
        %med.lowblue = %medscore
        %med.blue = $gettok(%playerids,%z,44))
        ;echo @medic Current blue med selected: %med.blue with value %med.lowblue
      }
      if (%medscore < %med.lowblue) {
        %med.lastblue = %medlast
        %med.lowblue = %medscore
        %med.blue = $gettok(%playerids,%z,44)
        ;echo @medic Current blue med selected: %med.blue with value %med.lowblue
      }
      echo @medic Current blue med selected: %med.blue with value %med.lowblue $+ ; Games since last: %med.lastblue
    }
    inc %z
  }
  write -dw $+ %med.blue $+ * joined.txt
  %playerids = $remtok(%playerids,%med.blue,44)
  return %med.blue
}

alias chooseredmed {
  var %x 1
  var %numplayers $numtok(%playerids,44)
  echo @medic $clr(Red) $+ Choosing red med...
  while (%numplayers >= %x) {
    echo @medic Red loop $clr(red) $+ $chr(35) $+ %x $+ $clr
    if (%med. [ $+ [ $gettok(%playerids,%x,44) ] ]) {
      var %medlast = $iif(%lastmed. [ $+ [ $gettok(%playerids,%x,44) ] ],$ifmatch,1)
      var %medscore = $calc(%med. [ $+ [ $gettok(%playerids,%x,44) ] ] / $iif($gettok($read(%statsdir $+ statsall.txt,w,$gettok(%playerids,%x,44) $+ *),8,37),$gettok($read(%statsdir $+ statsall.txt,w,$gettok(%playerids,%x,44) $+ *),8,37),1) / %medlast)

      if (!%med.red) {
        ;echo @medic First red loop
        %med.lastred = %medlast
        %med.lowred = %medscore
        %med.red = $gettok(%playerids,%x,44))
        ;echo @medic Current red med selected: %med.red with value %med.lowred $+ ; Games since last: %medlast
      }
      if (%medscore < %med.lowred) {
        ;echo @medic Red loop $chr(35) $+ %x
        %med.lastred = %medlast
        %med.lowred = %medscore
        %med.red = $gettok(%playerids,%x,44)
        ;echo @medic Current red med selected: %med.red with value %med.lowred
      }
      echo @medic Current red med selected: %med.red with value %med.lowred $+ ; Games since last: %med.lastred
    }
    inc %x
  }
  write -dw $+ %med.red $+ * joined.txt
  %playerids = $remtok(%playerids,%med.red,44)
  return %med.red
}

alias kdratio {
  return $+($clr(orange),$chr(91),$gettok($read(stats.txt,w,$1 $+ *),3,37),$chr(47),$gettok($read(stats.txt,w,$1 $+ *),4,37),$chr(93),$clr)
}

alias inctimesplayed {
  var %numplayers = $numtok(%gameids,44)
  var %x = 1
  while (%numplayers >= %x) {
    add $gettok(%gameids,%x,44) games 1
    inc %x
  }
}

alias name {
  var %x = $1
  if $read(joinedsort.txt,$1) {
    tokenize 61 $read(joinedsort.txt,$1)
    if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) {
      tokenize 37 $v1
      return $1
    }
    else {
      return $1
    }
  }
  else {
    return 
  }
}

alias getnamebyid {
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    return $2
  }
  elseif ($read(%statsdir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    return $2
  }
  else {
    return $sid($1)
  }
}

alias pickplayer {
  if ($1 = %team1captain) {
    if ($read(picks.txt,w,* $+ $chr(37) $+ * $+ $2 $+ *)) {
      var %readline = $v1
      var %line = $readn
      var %pickname = $gettok(%readline,2,37)
      var %pickid = $gettok(%readline,1,37)
      %team1ids = $addtok(%team1ids,%pickid,32)
      rcon say %pickname has been chosen by $getnamebyid($1) for the Blue team
      set %player.blue [ $+ [ $numtok(%team1ids,32) ] ] %pickid
      %picklist = $remtok(%picklist,%pickid,44)
      write -dl $+ %line picks.txt
      if (!$read(picks.txt,1)) { goto allpicked }
      var %x = 1, %y = $lines(picks.txt)
      var %availablePicks
      while (%x <= %y) {
        %availablePicks = $addtok(%availablePicks,say $gettok($read(picks.txt,%x),2,37),59)
        inc %x
      }
      rcon say Players available for picking:; %availablePicks ; say It is now $getnamebyid(%team2captain) $+ 's turn to pick
    }
  }
  if ($1 = %team2captain) {
    if ($read(picks.txt,w,* $+ $chr(37) $+ * $+ $2 $+ *)) {
      var %readline = $v1
      var %line = $readn
      var %pickname = $gettok(%readline,2,37)
      var %pickid = $gettok(%readline,1,37)
      %team2ids = $addtok(%team2ids,%pickid,32)
      rcon say %pickname has been chosen by $getnamebyid($1) for the Red team
      set %player.red [ $+ [ $numtok(%team2ids,32) ] ] %pickid
      %picklist = $remtok(%picklist,%pickid,44)
      write -dl $+ %line picks.txt
      if (!$read(picks.txt,1)) { goto allpicked }
      var %x = 1, %y = $lines(picks.txt)
      var %availablePicks
      while (%x <= %y) {
        %availablePicks = $addtok(%availablePicks,say $gettok($read(picks.txt,%x),2,37),59)
        inc %x
      }
      rcon say Players available for picking:; %availablePicks ; say It is now $getnamebyid(%team1captain) $+ 's turn to pick
    }
  }
  inc %pick
  :allpicked
  if (!$read(picks.txt,1)) {
    rcon say All players have been picked
    ;choose medics here... how 2 do? probs add something to do with captains to $chooseredmed and $choosebluemed
    ;For NOW - CAPTAINS ARE MEDICS (SO EASY TO DO ;))

    %team1.irc = $getnamebyid(%player.blue1) (Medic) - $playerinfo(%player.blue2) - $playerinfo(%player.blue3) - $playerinfo(%player.blue4) - $playerinfo(%player.blue5) - $playerinfo(%player.blue6)
    %team2.irc = $getnamebyid(%player.red1) (Medic) - $playerinfo(%player.red2) - $playerinfo(%player.red3) - $playerinfo(%player.red4) - $playerinfo(%player.red5) - $playerinfo(%player.red6)
    doteams
    chan All players have been picked
    chan 12Blue: %team1.irc
    chan 04Red: %team2.irc
    rcon say Blue: %team1 ; say Red: %team2
  }
}

alias maketeams  {
  set %lastgameids %playerids
  set %gameids %playerids
  if (%captains) {
    ;var %randplayer = $rand(1,$numtok(%gameids,44))
    ;var %randplayer2 = $rand(1,$numtok(%gameids,44))
    ;if (%randplayer == %randplayer2) {
    ;  while (%randplayer == %randplayer2) {
    ;    %randplayer2 = $rand(1,$numtok(%gameids,44))
    ;  }
    ;}
    %medics = $medtimes(%playerids)
    set %prevbluemed3 %prevbluemed2
    set %prevbluemed2 %prevbluemed
    set %prevbluemed $gettok(%medics,1,59)
    set %prevredmed3 %prevredmed2
    set %prevredmed2 %prevredmed
    set %prevredmed $gettok(%medics,2,59)
    incmedtimes %prevbluemed
    incmedtimes %prevredmed
    %team1captain = %prevbluemed
    %team2captain = %prevredmed
    %team1ids = %team1captain
    %team2ids = %team2captain
    %player.blue1 = %team1captain
    %player.red1 = %team2captain
    add %team1captain captain 1
    add %team2captain captain 1
    rcon say Blue Captain: $getnamebyid(%team1captain) ; say Red Captain: $getnamebyid(%team2captain)
    %picklist = %gameids
    %picklist = $remtok(%picklist,%team1captain,44)
    %picklist = $remtok(%picklist,%team2captain,44)
    set %pick $rand(1,2)
    var %x = 1, %y = $numtok(%picklist,44)
    while (%x <= %y) {
      write picks.txt $+($gettok(%picklist,%x,44),$chr(37),$getnamebyid($gettok(%picklist,%x,44)))
      inc %x
    }
    var %x = 1, %y = $lines(picks.txt), %availablePicks
    while (%x <= %y) {
      %availablePicks = $addtok(%availablePicks,say $gettok($read(picks.txt,%x),2,37),59)
      inc %x
    }
    if ($calc(%pick % 2)) { %captainpick = %team2captain }
    else { %captainpick = %team1captain }
    rcon say Players available for picking:; %availablePicks ; say -------------------; say $getnamebyid(%captainpick) ( $+ $iif(%pick == 1,Red,Blue) $+ ) gets first pick; say Use !picklist at any time to see available picks

    unset %med.*
    unset %lastmed.*
    unset %playerids
    hsave medtable %statsdir $+ pugmedtable
  }
  else {
    %medics = $medtimes(%playerids)
    filter -cfftue 2 61 joined.txt joinedsort.txt
    set %prevbluemed3 %prevbluemed2
    set %prevbluemed2 %prevbluemed
    set %prevbluemed $gettok(%medics,1,59)
    set %prevredmed3 %prevredmed2
    set %prevredmed2 %prevredmed
    set %prevredmed $gettok(%medics,2,59)
    chooseteams
    incmedtimes %prevbluemed
    incmedtimes %prevredmed
    %team1ids = %player.blue1 %player.blue2 %player.blue3 %player.blue4 %player.blue5 %player.blue6
    %team2ids = %player.red1 %player.red2 %player.red3 %player.red4 %player.red5 %player.red6
    %team1.irc = $getnamebyid(%player.blue1) (Medic) - $playerinfo(%player.blue2) - $playerinfo(%player.blue3) - $playerinfo(%player.blue4) - $playerinfo(%player.blue5) - $playerinfo(%player.blue6)
    %team2.irc = $getnamebyid(%player.red1) (Medic) - $playerinfo(%player.red2) - $playerinfo(%player.red3) - $playerinfo(%player.red4) - $playerinfo(%player.red5) - $playerinfo(%player.red6)
    doteams
    rcon say Blue: %team1 ; say Red: %team2
    .timer 1 15 forceteams
    chan 12Blue: %team1.irc
    chan 04Red: %team2.irc
    incgamesincemed
    unset %med.*
    unset %lastmed.*
    unset %playerids
    hsave medtable %statsdir $+ pugmedtable
  }
}

alias updateteams {
  if (%team1) {
    %team1.irc = $getnamebyid(%player.blue1) (Medic) - $playerinfo(%player.blue2) - $playerinfo(%player.blue3) - $playerinfo(%player.blue4) - $playerinfo(%player.blue5) - $playerinfo(%player.blue6)
    %team2.irc = $getnamebyid(%player.red1) (Medic) - $playerinfo(%player.red2) - $playerinfo(%player.red3) - $playerinfo(%player.red4) - $playerinfo(%player.red5) - $playerinfo(%player.red6)
    doteams
  }
}

alias doteams {
  %team1 = $getnamebyid(%player.blue1) (Medic)
  %team2 = $getnamebyid(%player.red1) (Medic)
  var %x = 2
  while (%x <= $numtok(%team1ids,32)) {
    %team1 = %team1 - $getnamebyid($gettok(%team1ids,%x,32))
    %team2 = %team2 - $getnamebyid($gettok(%team2ids,%x,32))
    inc %x
  }
}

alias forceteams {
  var %x = 2, %teamforce.red = cw_forceteam %rcon.redteam " $+ %player.red1 $+ ", %teamforce.blue = cw_forceteam %rcon.blueteam " $+ %player.blue1 $+ "

  while (%x <= $numtok(%team1ids,32)) {
    ;echo -a Player idx: %x
    %teamforce.blue = $addtok(%teamforce.blue,cw_forceteam %rcon.blueteam " $+ $gettok(%team1ids,%x,32) $+ ",59)
    %teamforce.red = $addtok(%teamforce.red,cw_forceteam %rcon.redteam " $+ $gettok(%team2ids,%x,32) $+ ",59)
    inc %x
  }
  ;echo -a %teamforce.blue
  ;echo -a %teamforce.red
  rcon %teamforce.blue
  rcon %teamforce.red

}

alias playerinfo {
  return $getnamebyid($1) $playerclass($1)
}

alias chooseteams {
  set %player.blue1 %prevbluemed
  set %player.blue2 $name(1)
  set %player.blue3 $name(4)
  set %player.blue4 $name(5)
  set %player.blue5 $name(8)
  set %player.blue6 $name(10)
  set %player.blue.score $teamscore(blue)

  set %player.red1 %prevredmed
  set %player.red2 $name(2)
  set %player.red3 $name(3)
  set %player.red4 $name(6)
  set %player.red5 $name(7)
  set %player.red6 $name(9)
  set %player.red.score $teamscore(red)
}

alias teamscore {
  var %team = $1, %x = 1, %score = 0
  while (%x <= 6) {
    %score = $calc(%score + $score(%player. [ $+ [ $1 ] $+ [ %x ] ]))
    inc %x
  }
  return %score
}

alias score {
  ;id;name;kill;death;cap;assist;points;games;healing;uberlost;win;loss;draw
  ;((win/game) ^ 0.5 + 200 * ((kills + assists)/deaths) + (assists/game))^1.2 - (deaths/game) = old
  ;
  ;return $calc($calc($3 / $4) + $calc($7 / $8))
  ;hload medtable %statsdir $+ pugmedtable
  if ($read(%statsdir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    ;echo -s $calc($hget(medtable,$1) * 1.32 / (($11 / $8) ^ 2))
    ;echo -s $calc($6 / $4)
    ;echo -s $calc($3 / $8)
    ;echo -s $calc($6 / $8)
    ;((200 * (wins/games)) + ((kills+assists)/deaths)^1.1 + 200)
    return $calc(((200 * ($11 / $8)) + (($3 + $6) / $4))^1.1 + 200)
  }
  else {
    return $rand(200,400)
  }
}
alias joined {
  if (%get) && (!$istok(%gameids,$1,44)) { 
    %gameids = $addtok(%gameids,$1,44)
    unset %get 
  }

  if (%team1) { return }
  if ($read(joined.txt,w,$1 $+ $chr(61) $+ *)) { 
    return
  }
  else { 
    write joined.txt $+($1,$chr(61),,$score($1))
    %playerids = $addtok(%playerids,$1,44)
  }
  echo -s JOINED $2- ( $+ $1 $+ ) - $score($1) - Total: $lines(joined.txt)
  if ($lines(joined.txt) = %pug.limit) {
    ;msg %pug.chan $codes(12 players are in the server... creating teams...)
    .timer 1 1 maketeams
  }
}

alias left {
  if (%team1) { return }
  if ($read(joined.txt,w,$1 $+ $chr(61) $+ *)) && (!%match.on) { 
    ;write -dw $+ $1 $+ =* joined.txt
    %playerids = $remtok(%playerids,$1,44)
  }
}

on *:sockwrite:rcon:{
  if ($sockerr) {
    aline @rcon $sock().wserr
  }
}

on *:close:@rcon:{
  if (%rcon.scorebot == 1) {
    set %rcon.scorebot 0
  }
  set %rcon.myip $ip
}

alias geoip.country {
  ; Assign the ip to a variable
  var %ip = $1

  ; Make sure we have a valid ip, if not return $null
  if (!$regex(%ip, /\d+\.\d+\.\d+\.\d+/)) {
    return
  }

  ; Make sure DB is open, if it isn't open it
  if (!$sqlite_is_valid_conn(%geoip_db)) {
    set %geoip_db $sqlite_open(geoip.db)
    if (!%geoip_db) {
      echo 4 -a Error: Couldn't open database ( $+ %sqlite_errstr $+ )
      return
    }
  }

  ; Find the country
  var %sql = SELECT ccode FROM ip_to_country WHERE $longip(%ip) BETWEEN begin_num AND end_num
  var %res = $sqlite_query(%geoip_db, %sql)
  if (!%res) {
    echo 4 -a Error: Query failed ( $+ %sqlite_errstr $+ )
    return
  }

  ; Check if there was a result
  if ($sqlite_num_rows(%res) > 0) {
    var %country = $sqlite_fetch_single(%res)
  }
  else {
    var %country = $null
  }

  ; Free result
  sqlite_free %res

  ; Return country ($null if one wasn't found)
  return %country
}
