on *:LOAD:{
  unset %getrelay
}

on *:TOPIC:#tf2pug:{
  ;  if (Pug started isin $chan(#tf2pug).topic)
  if (Pug started by isin $chan(#tf2pug).topic) {
    //    msg $chan 14,0 Don't forget to join us in 1 #tf2pug2
  }
}

on *:NOTICE:*:#tf2pug*:{
  if ($address($nick,2) == *!*@iPGN-TF2.user.gamesurge && 12 player pug started isin $1-) { 
    if ($chan == #tf2pug) {
      msg #ozfortress 12 player pug started in $chan join up!
      msg #tf2pug2 12 player pug started in $chan  join up!
    }
    if ($chan == #tf2pug2) {
      msg #ozfortress 12 player pug started in $chan join up!
      msg #tf2pug 12 player pug started in $chan join up!
    }
  } 
}

on *:TEXT:*:#tf2pug*:{
  if ($address($nick,2) == *!*@iPGN-TF2.user.gamesurge && Requesting player isin $1-) {
    if (!%getrelay) {
      set %getrelay 1
      if ($chan == #tf2pug) {
        msg #ozfortress <<< Requesting player in #tf2pug! The details are: $8 $9 $10 >>>
        msg #tf2pug2 12� Requesting player in #tf2pug! The server details are: $8 $9 $10 12�
        .timer 1 300 unset %getrelay
      }
      if ($chan == #tf2pug2) {
        msg #ozfortress <<< Requesting player in #tf2pug2! The details are: $8 $9 $10 >>>
        msg #tf2pug 12� Requesting player in #tf2pug2! The server details are: $8 $9 $10 12�
        .timer 1 300 unset %getrelay
      }
    }
  }
}
