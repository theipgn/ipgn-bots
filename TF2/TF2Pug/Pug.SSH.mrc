alias ssh {
  var %f = C:\TF2\ssh.commands
  write -c %f
  var %e = write %f
  if ($1 = demo) {
    %e ssh -t -t inara.ipgn.com.au /games/ipgn/bin/zipdemos2
    %e /usr/bin/rsync --remove-sent-files inara:/games/tf2_pug/orangebox/tf/*zip /data/gamefiles/tf2/demos/pug/
    %e /data/scripts/games/indexgames /data/gamefiles/tf2/demos/pug/
    sshrun putty 10.50.4.36 22 -l gamesvr -pw f4tm4x1mu5 -i C:\TF2\key_priv.ppk -m %f
  }
  if ($1 = restart) {
    %e /games/ipgn/bin/tf2pug.sh restart
    sshrun putty inara.ipgn.com.au -l steam -i C:\TF2\key_priv.ppk -m %f
  }
  if ($1 = mumble) {
    %e python /home/murmur/mmctl.py $2-
    sshrun putty river.ipgn.com.au -l murmur -i C:\TF2\key_priv.ppk -m %f
  }
  ;More to come...
}

alias sshrun {
  .run C:\TF2\ $+ $1 $+ .exe $2-
}
