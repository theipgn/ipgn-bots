Provided for convienience here.

These will NOT be updated if you change the code.

Usage:

To use in interactive mode, use no parameters.
Else use parameters in the form: ip port password command
Enclose the command in " marks if it is more than one word
E.g. sourcercon 192.168.0.5 27015 testpass "say Testing!"