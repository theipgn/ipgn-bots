  var %y = $numtok(%players,32), %x = 1, %captaincount = 0
  while (%x <= %y) {
    if (%captain. [ $+ [ $gettok(%players,%x,32) ] ]) {
      inc %captaincount
    }
    if (%captaincount > $calc(%y / 2)) {
      msg %pug.chan $codes(Captains have been enabled due to majority vote. Two captains will be randomly chosen once the server is full.)
      set %captains 1
      rcon exec captains.cfg
      break
    }
    inc %x
  }

      elseif ($read(%statsdir $+ connections_log.txt,w,* $+ %playerip $+ *)) {
        var %ipline = $v1
        var %y = $lines(%statsdir $+ bannedids.txt)
        var %x = 1
        while (%x <= %y) {
          tokenize 37 $read(%statsdir $+ bannedids.txt,%x)
          if (* $+ $1 $+ * iswm %ipline) {
            ;rcon kickid $regml(3) Banned from pugs; say $regml(1) has been kicked from the server - banned from pugs.
            msg %pug.chan $codes($regml(1) ( $+ $regml(3) $+ ) has the same IP as banned player $3 ( $+ $1 $+ ))
            msg %pug.adminchan $codes($regml(1) ( $+ $regml(3) $+ ) has the same IP as banned player $3 ( $+ $1 $+ ))
            msg %pug.helperchan $codes($regml(1) ( $+ $regml(3) $+ ) has the same IP as banned player $3 ( $+ $1 $+ ))
            msg %pug.chan $codes($regml(1) ( $+ $regml(3) $+ ) has been removed from the server (banned player). Reason: $4 $+ )
            msg %pug.adminchan $codes($regml(1) ( $+ $regml(3) $+ ) has been removed from the server (banned player). Reason: $4 $+ )
            msg %pug.helperchan $codes($regml(1) ( $+ $regml(3) $+ ) has been removed from the server (banned player). Reason: $4 $+ )
            goto serversay
          }
          inc %x
        }
        goto allowed
      }