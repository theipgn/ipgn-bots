on *:TEXT:*:#tf2pug*:{
  if ( $nick isop $chan ) {
    ;///////////////////////////////////////////////////////
    if ($1 == !banplayer) {
      if ($2 && $3 isalnum && $4 isnum && $5 && 1 <= $4 <= 8) {
        ;set %ipgn.ban.time $3 * 2
        ;msg ChanServ@Services.gamesurge.net tb #tf2pug $2 %ipgn.ban.time $+ h $2 # $+ $4 $5-
        ;The above is for some automation in the future

        ;Simple ban - msg chanserv with the tokens supplied
        ;msg ChanServ@Services.gamesurge.net tb #tf2pug $2 $3 $+ h $2 $mid(asd#f,4,1) $+ $4 - $5-
        ;msg ChanServ@Services.gamesurge.net tb #tf2pug2 $2 $3 $+ h $2 $mid(asd#f,4,1) $+ $4 - $5-
        ;Depricated, moved to alias

        banplayer $2 $3 $4 $5-
        if ($2 ison #tf2pug) || ($2 ison #tf2pug2 || $2 ison #ozfortress) {
          writeban $nick $gettok($gettok($address($2,11),1,$asc(@)),2,$asc(!)) $2 $3 $4 $5-
        }
        else {
          writeban $nick $gettok($gettok($mask($2,11),1,$asc(@)),2,$asc(!)) $2 $3 $4 $5-
        }
      }

      else {
        notice $nick ERROR: You're either missing a variable or you've got something in the wrong place. Please make sure you've got it set out like this: !banplayer <nick or ip> <time> <number of ban> <reason>.  NOTE: Do NOT include any $mid(asd#f,4,1) $+ s.
        notice $nick EG. !banplayer dumbguy 40 3 being an idiot
      }
    }
    ;////////////////////////////////////////////////////    
    if ($1 == !permban && $nick isop #ipgn-tf2) {
      if ($2) {
        permban $2 $3-
        if ($3) {
          notice $nick Player $2 has been banned permanently for $3-
        }
        else {
          notice $nick Player $2 has been banned permanently, no reason specified.
        }
      }
      else {
        notice $nick You're missing something. Set it out like this: !permban <nick or ip>
      }
    }
    ;////////////////////////////////////////////////////    
    if ($1 == !unbanplayer && $nick isop #ipgn-tf2) {
      if ($2) {
        unbanplayer $2
        notice $nick %unbanreply
      }
      else {
        notice $nick You need to specify the player's ip (mask) or nick
      }
    }
    ;////////////////////////////////////////////////////
    if ($1 == !banid && $nick isop #ipgn-tf2) {
      notice $nick this is a WIP
      ;if ($2 && $3)
      ;banid $2 $3 $nick
      ;notice $nick SteamID $2 has been permanently banned from the servers
      ; }
      ; else {
      ;notice $nick You need to specify a SteamID and a reason! !banid <id> <reason>
      ; }
    }
    ;///////////////////////////////////////////////////
    if ($1 == !cmdlist) {
      notice $nick The available commands are:
      notice $nick !banplayer <nick or ip> <time> <number of ban> <reason>
      notice $nick !permban <nick or ip> <reason>. Reason isn't required, but it's useful :)
      notice $nick !unbanplayer <nick or ip>
      notice $nick !listban <nick or ip>. Shows details on specified nick (if they've been banned before)
      notice $nick !banid <id> // WIP, not working at the moment
      notice $nick More to come
    }
    ;///////////////////////////////////////////////////
    if ($1 == !listban) {
      if ($2) {
        listban $2
        notice $nick %listbanreply
      }
      else {
        notice $nick You need to specify a player to check on. !listban <nick or ip>
      }
    }
    ;//////////////////////////////////////////////////
  }
}

alias banplayer {
  msg ChanServ@Services.gamesurge.net tb #tf2pug $1 $2 $+ h $1 $mid(asd#f,4,1) $+ $3 - $4-
  msg ChanServ@Services.gamesurge.net tb #tf2pug2 $1 $2 $+ h $1 $mid(asd#f,4,1) $+ $3 - $4-
}

alias permban {
  msg ChanServ@Services.gamesurge.net addban #tf2pug $1 $2-
  msg ChanServ@Services.gamesurge.net addban #tf2pug2 $1 $2-
}

alias unbanplayer {
  msg ChanServ@Services.gamesurge.net delban #tf2pug $1
  msg ChanServ@Services.gamesurge.net delban #tf2pug2 $1
  ;//write -w*!*testing@* bannedplayers.txt $+(*!*testing@*,$chr(37),*!*testing@*,$chr(37),2 $+ h,$chr(37),1,$chr(37),for testing purposes,$chr(37),yomum,$chr(37),$date,$chr(37),false)
  set %unbannick $1
  if ($read(bannedplayers.txt,w,$1 $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    ;$8 == banned status
    if ($8 == true) {
      set %unbanreply Player $1 has been unbanned
      echo Set $1 'banned' status to false
      ;name%hostmask%time%number%reason%banner%date%banned(true/false)
      write -l $+ $readn bannedplayers.txt $+($1,$chr(37),$2,$chr(37),$3,$chr(37),$4,$chr(37),$5,$chr(37),$6,$chr(37),$date,$chr(37),false)
    }
    if ($8 == false) {
      set %unbanreply Player $1 is not banned
      echo Player $1 is not banned
    }
  }
  else {
    echo No ban could be found for $1
    set %unbanreply No ban found for $1
  }
}

alias banid {
  set %steam.id $1
  rcon banid 0 %steam.id
  ;Writes it in the format steamid%reason%banner%date
  write bannedids.txt $+($1,$chr(37),$2,$chr(37),$3,$chr(37),$date)
}

alias writeban {
  ;This writes the ban to the file in the format name%hostmask%time%number%reason%banner%date
  ;easily readable (hopefully)
  ;writeban $nick $gettok($gettok($address($2,11),1,$asc(@)),2,$asc(!)) $2 $3 $4 $5-
  write bannedplayers.txt $+($3,$chr(37),*! $+ $2 $+ @*,$chr(37),$4 $+ h,$chr(37),$5,$chr(37),$6-,$chr(37),$1,$chr(37),$date,$chr(37),true)
}

alias listban {
  ;This will read from the file bannedplayers.txt
  if ($read(bannedplayers.txt,w,$1 $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    set %listbanreply Banned player: $1 ( $+ $2 $+ ) $+ . Time banned: $3 $+ . Number of ban: $4 $+ . Banner: $6 $+ . Reason: $5 $+ . Date banned: $7 $+ .
    echo Banned player: $1 ( $+ $2 $+ ) $+ . Time banned: $3 $+ . Number of ban: $4 $+ . Banner: $6 $+ . Reason: $5 $+ . Date banned: $7 $+ .
    return %listbanreply
  }
  else {
    echo No ban could be found for $1
    set %listbanreply No ban found for $1
  }
}

alias autoincreaseban {
  if ($read(bannedplayers.txt,w,$1 $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    write -l $+ $readn bannedplayers.txt $+($3,$chr(37),*! $+ $2 $+ @*,$chr(37),$4 $+ h,$chr(37),$5,$chr(37),$6-,$chr(37),$1,$chr(37),$date)


  }
}
