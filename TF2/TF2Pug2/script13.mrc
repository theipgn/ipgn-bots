alias setupssh {
  set %ssh.cmd mal.ipgn.com.au -l gamesvr -i C:\TF2\key_priv.ppk -m %ssh.script
}

alias ssh {
  setupssh
  set %demo $read(%statsdir $+ demos.txt,w,* $+ $2 $+ *)
  echo -a %demo
  if ($1 = sshftp) {
    sshwrite -c
    sshwrite ssh -t -t inara /games/ipgn/bin/zipdemos
    sshwrite ssh -t -t zoe /usr/bin/rsync --remove-sent-files inara:/games/tf2/orangebox/tf/ [ $+ [ %demo ] ] /data/gamefiles/tf2/demos/pug/
    sshwrite ssh -t -t zoe /data/scripts/games/indexgames /data/gamefiles/tf2/demos/pug/
    write -w* $+ %demo $+ * %statsdir $+ demos.txt $+($read(%statsdir $+ demos.txt,w,%demo $+ *),$chr(32),uploaded)
    %demo.link = http://games.ipgn.com.au/downloads/tf2/demos/pug/ [ $+ [ %demo ] ]
    shortenurl %demo.link
    sshrun putty %ssh.cmd
  }
  ;elseif {$1 = sshmove) {
  ;
  ;}
  elseif ($1 = sshrestart) {
    sshwrite -c
    sshwrite ssh inara@ipgn.com.au
    sshwrite sudo su steam
    sshwrite /games/ipgn/bin/tf2pug.sh restart
    sshrun putty
    set %ssh.reply $codes(The pug server has been restarted)
  }
  else {
    ssh.comm $1-
  }
  return %ssh.reply
}

alias sshwrite {
  if ($1 = -c) {
    write -c %ssh.script
  }
  else {
    write %ssh.script $1-
  }
}

alias sshrun {
  .run C:\TF2\ $+ $1 $+ .exe $2- 
}

alias getdemolink {
  if ($read(%statsdir $+ demos.txt,w,* $+ $1 $+ .zip $+ *)) {
    echo -a $v1
    return $gettok($v1,3,32)
  }
  else {
    return ERROR: No link found
  }
}

alias autonotify {
  if (!$timer(autonotify) && (!%full) && (%pug)) {
    .timer 4 300 autonotify
  }
  elseif (!%full) && ($timer(autonotify)) && (%pug) {
    msg %pug.chan $codes($calc((%pug.limit - $numtok(%players,32))) morep)
  }
  else {
    halt
  }
}

alias shortenurl {
  sockclose shorten
  sockopen shorten tinyurl.com 80
  ;set %shorten.demolink http://games.ipgn.com.au/downloads/tf2/demos/pug/ [ $+ [ $1 ] ]
  set %shorten.url GET $+(/create.php?url=,$1-) HTTP/1.1
  echo @shorten %shorten.url
}

on *:sockopen:shorten:{
  if ($sockerr) {
    echo -a Connection failed
    sockclose shorten
    return
  }
  sockwrite -tn $sockname %shorten.url
  sockwrite -tn $sockname Host: tinyurl.com
  sockwrite -tn $sockname Connection: Keep-Alive
  sockwrite -tn $sockname Content-Type: text/html
  sockwrite -tn $sockname
}

on *:sockread:shorten:{
  sockread %shorten.read
  tokenize 62 $left(%shorten.read,255)
  if (http://tinyurl.com/*</b iswm $3) {
    echo @shorten $remove($gettok($3-,1,32),</b)
    set %shorten.urlrec $remove($gettok($3-,1,32),</b)
    if ($read(%statsdir $+ demos.txt,w,%demo $+ *)) {
      tokenize 32 $v1
      write -l $+ $readn %statsdir $+ demos.txt $+($1,$chr(32),$2,$chr(32),%shorten.urlrec)
    }
  }
}

on *:sockclose:shorten {
  if ($sockerr) {
    echo -a SOCKET CLOSED DUE TO ERROR
  }
}
