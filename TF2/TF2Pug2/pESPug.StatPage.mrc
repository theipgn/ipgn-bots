alias uploadstats {
  .timerstats 1 15 mode %pug.chan -m
  upload $+(%stats.dir,stats.txt)
}

alias upload {
  ;-- Set these variables --
  %hostofphpfile = vel.kgbfh.com
  %locationofphpfile = /lol/upload.php
  ;-------------------------

  %dest = $$1-
  if ((!$file(%dest)) || ($isdir(%dest))) return
  .remove upload.tmp
  echo -a Uploading file %dest $+ ....
  sockopen upload %hostofphpfile 80
}
On *:sockopen:upload:{
  var %b = $md5() 
  write -c upload.tmp $+(--,%b,$lf,$&
    Content-Disposition: form-data; name="uploadfile";,$&
    filename=",$nopath(%dest),",$lf,$lf)
  .copy -a " $+ %dest $+ " upload.tmp
  write upload.tmp -- $+ %b

  var %write = sockwrite -tn upload
  %write POST %locationofphpfile HTTP/1.1
  %write Host: %hostofphpfile
  %write Connection: close
  %write Content-Type: multipart/form-data; boundary= $+ %b
  %write Content-Length: $file(upload.tmp)
  %write
  .fopen upload upload.tmp
}

On *:sockwrite:upload:{
  if $sock(upload).sq > 8192 || !$fopen(upload) { return }
  if $fread(upload,8192,&a) {
    sockwrite upload &a
  }
  else .fclose upload
}

On *:sockread:upload:{
  var %temp
  sockread %temp
  ;echo -a %temp
  if (*incorrect* iswm %temp) {
    echo 4 -a Failed!
  }
  elseif (*Successful* iswm %temp) {
    echo 3 -a Success!
  }
}

On *:sockclose:upload:cleanupload
alias cleanupload {
  .sockclose upload
  if $fopen(upload) { .fclose upload }
  unset %dest
  msg %pug.chan $codes(Stats updated: http://esau.kgbfh.com/)
  .remove upload.tmp
}
