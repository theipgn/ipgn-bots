on *:NOTICE:*:*:{
  if (*I recognize you* iswm $1-) {
    if ($me != [iPGN-TF2][2]) {
      msg AuthServ@Services.GameSurge.net ghost [iPGN-TF2][2]
      .timer 1 8 nick [iPGN-TF2][2]
    }
    .timer 1 10 join %pug.chan
    ;.timer 1 15 msg irpg login ipgntf2 h1tl3r
    .timer 1 10 join %pug.killchan
    .timer 1 10 join %pug.adminchan
  }
}

alias codes {
  return 12� $1- 12�
}

alias pinkcodes {
  return 13� $1- 13�
}

alias autonotify {
  if (!$timer(autonotify) && (!%full) && (%pug)) {
    .timerautonotify 0 240 autonotify
  }
  elseif (!%full) && ($timer(autonotify)) && (%pug) {
    msg %pug.chan $codes($calc((%pug.limit - $numtok(%players,32))) more players needed!)
  }
  elseif (%full) || (!%pug) {
    timerautonotify off
  }
  else {
    halt
  }
}

alias nextpugreminder {
  var %x = 1, %ln = $lines(reminders.txt)
  while (%x <= %ln) {
    var %l = $read(reminders.txt,%x)
    notice %l $codes(You'll be able to join the next pug shortly. Best get your join ready!)
    msg %l $codes(You'll be able to join the next pug shortly. Best get your join ready!)
    inc %x
  }
}

alias newtopic {
  if ($1) {
    set %status $1-
  }
  .topic %pug.chan 12,0� 1i14,0PGN TF2 Pug! 1,0http://tf2pug.12ipgn1.com.au/ 12�1 N14ews: %news 12�1 S14tatus: %status 12�1 G14ame 1I14nfo: %pug.killchan 12�
  .topic %pug.killchan 12,0� 1i14,0PGN TF2 Pug! 1,0http://tf2pug.12ipgn1.com.au/ 12�1 S14tatus: %status 12�
}

alias setteams {
  msg %pug.chan $codes(Game $chr(35) $+ $calc(%gamesplayed + 1) is now in-game. Admin: %pug.starter $+ . Please join the server promptly)
  massdetails
  set %noget 1
  .timerget 1 180 unset %noget
}

alias massdetails {
  .timertimeout off
  set %detailed 1
  set -u3 %busy 1
  var %v = $numtok(%players,32)
  var %u = 1
  while (%u <= %v) {
    var %nick = $gettok(%players,%u,32), %admin = 0
    if (%nick = %pug.starter) || (%nick isop %pug.chan) {
      %admin = 1
    }
    notice %nick $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass $+ $iif(%admin,; alias puglogin "say !login %adminpass $+ " // Admin pass: %adminpass))
    notice %nick $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
    notice %nick $codes(Mumble: /run $+(mumble://,%nick,:,%serverpass,@,%mumble.ip,:,%mumble.port,/?version=,%mumble.version))

    inc %u
  }
}

alias details {
  if (((!%busy) && (!%map.vote) && ($istok(%players,$1,32))) || ($1 isop %pug.chan)) {
    notice $1 $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass $+ $iif(($1 isop %pug.chan || $1 = %pug.starter),; alias puglogin "say !login %adminpass $+ " // Admin pass: %adminpass))
    notice $1 $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
    notice $1 $codes(Mumble: /run $+(mumble://,$1,:,%serverpass,@,%mumble.ip,:,%mumble.port,/TF2Pug?version=,%mumble.version))
  }
}

alias spamstats {
  unset %match.on
  .copy -o stats.txt stats_last.txt
  filter -cfftue 3 37 stats.txt stats.txt
  .copy -o stats.txt $+(%statsdir,games\,stats.,%gamesplayed,.txt)

  if ($read(%statsdir $+ end.txt)) {
    .timerspamstats 1 3 spamstats
    return
  }

  if (!$read(stats.txt)) { 
    msg %pug.adminchan $codes(Error in stat calc: No stats found in stats file)
    endpug
    return 
  }

  if (%endpug) {
    return
  }

  write %statsdir $+ end.txt 2

  set %endpug 1

  mode %pug.chan +m
  nextpugreminder
  var %y = $lines(stats.txt)
  var %x = 1, %highdmg.red = 0, %highdmg.redvalue = 0, %highdmg.blue = 0, %highdmg.bluevalue = 0
  var %highmedpack = 0, %highmedpack.value = 0, %highmedpack.small = 0, %highmedpack.medium = 0, %mkgc = pink
  var %highammopack = 0, %highammopack.value = 0, %highammopack.small = 0, %highammopack.medium = 0, %highammopack.large = 0, %apgc = pink
  while (%x <= %y) {
    var %gamestat = $read(stats.txt,%x)
    tokenize 37 %gamestat
    if ($0 > 3) {
      ;damage;ammopack_small;ammopack_medium;tf_ammo_pack;medkit_small;medkit_medium
      var %ap = $calc($16 + $17 + $18)
      if (%ap > %highammopack.value) {
        %highammopack.value = %ap
        %highammopack = $2
        %highammopack.small = $16
        %highammopack.medium = $17
        %highammopack.large = $18
        ;echo -s AMMO PACK VALUES: %highammopack.small (S) %highammopack.medium (M) %highammopack.large (L)
        %apgc = $iif($istok(%team1ids,$1,32),blue,red)
      }
      var %mp = $calc($19 + $20)
      if (%mp > %highmedpack.value) {
        %highmedpack.value = %mp
        %highmedpack = $2
        %highmedpack.small = $19
        %highmedpack.medium = $20
        ;echo -s MED PACK VALUES: %highmedpack.small (S) %highmedpack.medium (M)
        %mkgc = $iif($istok(%team1ids,$1,32),blue,red)
      }
      if ($istok(%team1ids,$1,32)) { 
        chan 12 $+ $2 - K:12 $3 D:12 $4 A:12 $6 Dmg:12 $15 APs:12 %ap MPs:12 %mp Heal Rcvd:12 $21 Points:12 $7
        if ($15 > %highdmg.bluevalue) {
          %highdmg.blue = $2
          %highdmg.bluevalue = $15
        }
      }
      elseif ($istok(%team2ids,$1,32)) { 
        chan 04 $+ $2 - K:04 $3 D:04 $4 A:04 $6 Dmg:4 $15 APs:4 %ap MPs:4 %mp Heal Rcvd:4 $21 Points:04 $7
        if ($15 > %highdmg.redvalue) {
          %highdmg.red = $2
          %highdmg.redvalue = $15
        }
      }
      else { 
        chan 13 $+ $2 - K:13 $3 D:13 $4 A:13 $6 Dmg:13 $15 APs:13 %ap MPs:13 %mp Heal Rcvd:13 $21 Points:13 $7 
      }

      if ($read(%statsdir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)) { 
        var %sl = $v1
        var %b = 2, %a = $numtok(%sl,37)
        var %ln = $readn
        while (%b <= %a) {
          if (%b = 2) {
            ;write -l $+ %ln %statsdir $+ statsall.txt $puttok($read(%statsdir $+ statsall.txt,%ln),$gettok($read(stats.txt,%x),%b,37),2,37)
            %sl = $puttok(%sl,$2,2,37)
          } 
          else {
            ;write -l $+ %ln %statsdir $+ statsall.txt $puttok($read(%statsdir $+ statsall.txt,%ln),$calc($gettok($read(%statsdir $+ statsall.txt,%ln),%b,37) + $gettok($read(stats.txt,%x),%b,37)),%b,37)
            %sl = $puttok(%sl,$calc($gettok(%sl,%b,37) + $gettok(%gamestat,%b,37)),%b,37)
          }
          inc %b
        }
        write -l $+ %ln %statsdir $+ statsall.txt %sl
      }
      else { 
        write %statsdir $+ statsall.txt %gamestat
      }
    }
    inc %x
  }
  upload stats %statsdir $+ statsall.txt
  .timer 1 1 upload chat %statsdir $+ chat\ $+ %gamesplayed $+ .txt
  dll mThreads.dll thread -ca weaponthread weaponstats

  chan 4Red Medic Stats - $getnamebyid(%prevredmed) $+ : Ubers Used:4 %uber.red Ubers Lost:4 %medic.Red.uberlost Total Healing:4 %medic.Red.healing 
  chan 12Blue Medic Stats - $getnamebyid(%prevbluemed) $+ : Ubers Used:12 %uber.blue Ubers Lost:12 %medic.Blue.uberlost Total Healing:12 %medic.Blue.healing

  if ($istok(%team1ids,%highestkillstreak,32)) { chan 7Highest Kill Streak:12 $getnamebyid(%highestkillstreak) with12 %highestkillstreakvalue kills }
  elseif ($istok(%team2ids,%highestkillstreak,32)) { chan 7Highest Kill Streak:4 $getnamebyid(%highestkillstreak) with4 %highestkillstreakvalue kills }
  else { chan 7Highest Kill Streak:9 $getnamebyid(%highestkillstreak) with9 %highestkillstreakvalue kills }

  chan $clr(orange) $+ Highest Damage per Team:4 %highdmg.red (04 $+ %highdmg.redvalue $+ ) 7/12 %highdmg.blue (12 $+ %highdmg.bluevalue $+ )

  %mkgc = $iif(%mkgc,$ifmatch,pink)
  chan $clr(orange) $+ Medkit Guzzler: $+ $clr(%mkgc) %highmedpack $+  $+($chr(40),$clr(%mkgc),%highmedpack.small,S,$chr(32),$clr(%mkgc),%highmedpack.medium,M,$chr(32),Total:,$chr(32),$clr(%mkgc),%highmedpack.value,,$chr(41))
  %apgc = $iif(%apgc,$ifmatch,pink)
  chan $clr(orange) $+ Ammo Pack Guzzler: $+ $clr(%apgc) %highammopack $+  $+($chr(40),$clr(%apgc),%highammopack.small,S,$chr(32),$clr(%apgc),%highammopack.medium,M,$chr(32),$clr(%apgc),%highammopack.large,L,$chr(32),Total:,$chr(32),$clr(%apgc),%highammopack.value,,$chr(41))

  chan $clr(Orange) $+ Game Number: $+ $clr %gamesplayed
  var %ms = $read(%statsdir $+ maps.txt,w,%win.map $+ *), %ng = $ini(gamescores.ini,0)
  chan $clr(orange) $+ %win.map has been played7 $gettok(%ms,2,37) times in7 %ng games ( $+ $clr(orange) $+ $round($calc(($gettok(%ms,2,37) / %ng) * 100),2) $+ % $+ )

  write -c stats.txt
  write -c %statsdir $+ end.txt

  uploaddemo %gamesplayed

  notice %pug.chan $codes(The next pug is about to open! Get your joins ready)

  set %lastplayers %players

  endpug
}

alias weaponstats {
  echo @weapons DOING WEAPON ADDING SHIT NOW!
  var %y = $lines(weapons.txt)
  var %x = 1, %dir = %statsdir
  .copy weapons.txt $+(%statsdir,games\,weapons.,%gamesplayed,.txt)
  echo @weapons $clr(green) $+ Adding weapons.txt to %statsdir $+ weaponsall.txt
  while (%y >= %x) {
    var %weapon_line = $read(weapons.txt,%x), %id = $gettok(%weapon_line,1,37)
    echo @weapons ID: $gettok(%weapon_line,1,37)
    if ($numtok(%weapon_line,37) > 2) {
      if ($read(%statsdir $+ weaponsall.txt,w,%id $+ $chr(37) $+ *)) {
        var %numweap = $numtok($v1,37)
        var %currentweap = 2, %wl = $v1
        var %ln = $readn
        while (%numweap >= %currentweap) {
          write -l $+ %ln %statsdir $+ weaponsall.txt $puttok(%wl,$calc($gettok(%wl,%currentweap,37) + $gettok(%weapon_line,%currentweap,37)),%currentweap,37)
          inc %currentweap
        }
      }
      else {
        echo @weapons Couldn't find ID in %dir $+ weaponsall.txt, writing on a new line
        write %statsdir $+ weaponsall.txt %weapon_line
      }
    }
    inc %x
  }
  .timer 1 1 upload weapon %statsdir $+ weaponsall.txt
  echo @weapons Weapon Adding Complete!
  write -c weapons.txt
}

alias endpug {
  .timerstatus off
  .timertimeleft off
  .timertimeout off
  .timermapvote off

  rcon tv_stoprecord; sv_password gtfo

  unset %pug %players %pug.starter %team* %pug.admin %detailed %full %map.*
  unset %match.on %busy %get %team1 %team2 %captains %team1ids %team2ids %picklist %team1captain %team2captain %pick %captain.* %blueteam %redteam %volunteermed*
  unset %medics %medic.* %uber.red %uber.blue %score.* %killstreak.* %player.* %class.* %paused %classlimit.* %classes.*

  if ($read(connected_players.txt)) {
    ;split kicks into 3 to prevent oversized packet length

    var %x = 1, %y = $floor($calc($lines(connected_players.txt) / 3)), %kickline
    while (%y >= %x) {
      %kickline = $addtok(%kickline,kickid $read(connected_players.txt,%x) #tf2pug,59)
      inc %x
    }
    rcon %kickline

    var %y = $calc($lines(connected_players.txt) - %y), %kickline = $null
    while (%y >= %x) {
      %kickline = $addtok(%kickline,kickid $read(connected_players.txt,%x) #tf2pug,59)
      inc %x
    }
    rcon %kickline

    var %y = $lines(connected_players.txt), %kickline = $null
    while (%y >= %x) {
      %kickline = $addtok(%kickline,kickid $read(connected_players.txt,%x) #tf2pug,59)
      inc %x
    }
    rcon %kickline
  }

  write -c connected_players.txt
  write -c joined.txt
  write -c picks.txt
  write -c reminders.txt
  write -c stats.txt

  mumbleServer password gettheFUCKoUT

  .timer 1 5 rcon.logs
  .timer 1 2 mumbleServer restart

  newtopic No pug in progress. Type !pug to start one.

  .timer 1 4 mode %pug.chan -m

  set %pug.dellastincr 1
  .timerdellast 1 10 unset %lastplayers

  unset %endpug
}

alias removewaitp {
  if (%pug) { return }
  if ($istok(%playerswaiting,$1,32)) {
    leavePug $1
  }
}

on *:NICK:{
  if ($timer($nick).secs) {
    .timer $+ $nick off
    .timer $+ $newnick 1 $v1 joinPug $newnick
  }
  if ($istok(%players,$nick,32)) {
    %players = $reptok(%players,$nick,$newnick,32)
  }
  if ($istok(%playerswaiting,$nick,32)) {
    %playerswaiting = $reptok(%playerswaiting,$nick,$newnick,32)
  }
  if ($nick = %pug.starter) {
    %pug.starter = $newnick
  }
  if ($istok(%reserved,$nick,32)) {
    %reserved = $reptok(%reserved,$nick,$newnick,32)
  }
  if (%captain. [ $+ [ $nick ] ]) {
    set %captain. [ $+ [ $newnick ] ] 1
    unset %captain [ $+ [ $nick ] ]
  }
}

alias timeout {
  if (!%detailed) && ($timer(timeout)) {
    return [ $+ $round($calc($timer(timeout).secs / 60),0) minutes until timeout]
  }
}

alias timeleft {
  if ($timer(timeleft)) {
    return Approximately $clr(orange) $+ $round($calc($timer(timeleft).secs / 60),0) $+ $clr minutes remaining
  }
}

alias showscores {
  if (%match.on) {
    return $clr(red) $+ Red: $+ $clr %score.red $clr(blue) $+ Blue: $+ $clr %score.blue
  }
  else {
    return ALPHABET SOUP (DIDN'T WORK)
  }
}

alias slotsremaining {
  if (%pug) {
    return $+(7,$chr(40),12,$calc(%pug.limit - $numtok(%players,32)),/,%pug.limit,$chr(32),slots remaining,07,$chr(41),)
  }
  else {
    return $+(7,$chr(40),12,$calc(5 - $numtok(%playerswaiting,32)),,$chr(32),players needed,07,$chr(41),)
  }
}

alias endpugtimeout {
  msg %pug.chan $codes(Pug timed out)
  if (!$istok(%maps,%pug.lastmap,59)) && (%pug.lastmap) && (%pug.lastmappos) {
    set %maps $instok(%maps,%pug.lastmap,%pug.lastmappos,59)
  }
  endpug
}

alias startmapvote {
  msg %pug.chan $codes(Pug $calc(%gamesplayed + 1) is now full. Players: %players)
  newtopic Pug is full. Map voting now in progress.
  %maps.list = $replace(%maps,$chr(59),$chr(32) $+ - $+ $chr(32))
  msg %pug.chan $codes(Vote for a map using !map <map>. eg. !map cp_well. Available maps: %maps.list)
  ;set %map.vote 1
  .timermapvote 1 60 endmapvote
}

alias votecount {
  if (!%map. [ $+ [ $1 ] ]) { return (0) }
  if (%map. [ $+ [ $1 ] ] < 0) { 
    unset %map. [ $+ [ $1 ] ]
    return (0) 
  }
  else { 
    return ( $+ %map. [ $+ [ $1 ] ] $+ ) 
  }
}

alias endmapvote {
  set -u4 %busy 1
  unset %map.high %map.win %map.count
  var %x = 1
  %map.count = $numtok(%maps,59)
  while (%x <= %map.count) { 
    if (%map. [ $+ [ $gettok(%maps,%x,59) ] ]) {
      if (!%map.high) { 
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59) 
      }
      if (%map. [ $+ [ $gettok(%maps,%x,59) ] ] = %map.high) && ($gettok(%maps,%x,59) != %map.win) {
        %map.equalhigh = %map. [ $+ [ $gettok(%maps,%x,59) ] ]
        %map.equalwin = $gettok(%maps,%x,59)
      }
      if (%map. [ $+ [ $gettok(%maps,%x,59) ] ] > %map.high) {
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59)
        unset %map.equalhigh
        unset %map.equalwin
      }
    }
    inc %x
  }
  if (!%map.win) {
    var %randmap = $gettok(%maps,$rand(1,$numtok(%maps,59)),59)
    set %map.win %randmap
    msg %pug.chan $codes(No map was voted for. Random map chosen: %randmap $+ )
  }
  else { 
    if (%map.equalwin) {
      msg %pug.chan $codes(Map vote is a tie between %map.equalwin ( $+ %map.equalhigh votes) and %map.win ( $+ %map.high votes))
      var %rand = $rand(1,10)
      if (%rand > 5) {
        msg %pug.chan $codes( $+ %map.win won the vote with %map.high votes!)
      }
      else {
        msg %pug.chan $codes( $+ %map.equalwin won the vote with %map.equalhigh votes!)
        set %map.win %map.equalwin
      }
    }
    else {
      msg %pug.chan $codes( $+ %map.win won the vote with %map.high votes!) 
    }
  }
  if (%captain.totalcount > $calc($numtok(%players,32) / 2)) {
    msg %pug.chan $codes(Captains have been enabled due to majority vote. Two captains will be chosen once all players are in the server.)
    set %captains 1
    ;rcon exec captains.cfg
  }
  rcon.logs
  rcon sv_password %serverpass ;changelevel %map.win
  .timer 1 1 newtopic Pug is now in-game.
  set %win.map %map.win

  if (!$istok(%maps,%pug.lastmap,59)) && (%pug.lastmap) && (%pug.lastmappos) { set %maps $instok(%maps,%pug.lastmap,%pug.lastmappos,59) }

  set %pug.lastmap %map.win
  set %pug.lastmappos $findtok(%maps,%map.win,1,59)
  set %maps $remtok(%maps,%map.win,59)
  unset %map.*

  if ($read(%statsdir $+ maplinks.txt,w,* $+ %win.map $+ *)) {
    var %l = $v1
    msg %pug.chan $codes(12 $+ %win.map can be downloaded at $gettok(%l,2,32))
  }

  .timernoendpug 1 600 return
  .timerstatus 0 30 rcon status
  .timer 1 1 setteams
}

alias startPug {
  ;startPug starternick pugsize class(es)
  if ($istok(%lastplayers,$1,32)) && ($nick !isop $chan) {
    if ($timer($1)) {
      return
    }
    var %dellasttime = $calc($timer(dellast).secs + (0.25 * %pug.dellastincr))
    notice $1 $codes(Since you played in the last pug $+ $chr(44) other players may join before you. You will be automatically added in %dellasttime seconds if there is room)

    .timer $+ $1 1 %dellasttime joinPug $1

    inc %pug.dellastincr

    return
  }

  if ($istok(%playerswaiting,$1,32)) { %playerswaiting = $remtok(%playerswaiting,$1,32) }
  if ($istok(%reserved,$1,32)) { %reserved = $remtok(%reserved,$1,32) }

  set %map.vote 1
  .timertimeout 1 1200 endpugtimeout
  set %pug 1
  set %pug.starter $1
  set %pug.limit $calc($2 * 2)

  if ($2 == 6) {
    set %classlimit.scout 4
    set %classlimit.soldier 4
    set %classlimit.demoman 2
    set %classlimit.medic 2

    ;rest of the classes are disabled in 6v6
    set %classlimit.pyro 0
    set %classlimit.heavy 0
    set %classlimit.engineer 0
    set %classlimit.sniper 0
    set %classlimit.spy 0
  }
  elseif ($2 == 9) {
    set %classlimit.scout 2
    set %classlimit.soldier 2
    set %classlimit.pyro 2
    set %classlimit.demoman 2
    set %classlimit.heavy 2
    set %classlimit.engineer 2
    set %classlimit.medic 2
    set %classlimit.sniper 2
    set %classlimit.spy 2
  }

  %players = $addtok(%players,$1,32)
  %players = $addtok(%players,%playerswaiting,32)
  %players = $addtok(%players,%reserved,32)

  addclass $1 $3-

  unset %playerswaiting
  unset %reserved

  %adminpass = $lower($read(pass.txt))
  %serverpass = $lower($read(pass.txt))

  rcon sv_password %serverpass
  mumbleServer password %serverpass

  newtopic Pug started by $1 $+ . Type !j to play.
  notice %pug.chan $codes(%pug.limit player pug started by $1 $+ . Type !j to play.)

  msg %pug.chan $codes($1 has joined the pug $slotsremaining())

  msg %pug.chan $codes(Come join the iPGN steam group! http://steamcommunity.com/groups/ipgn)
  msg %pug.chan $codes(Follow us on twitter at http://twitter.com/iPrimusGames and facebook at http://tinyurl.com/3ptsobz)

  autonotify
}

alias joinPug {
  ;joinPug $nick class(es)

  if ($istok(%lastplayers,$1,32)) && ($nick !isop $chan) {
    if ($timer($1)) {
      return
    }
    var %dellasttime = $calc($timer(dellast).secs + (0.25 * %pug.dellastincr))
    notice $1 $codes(Since you played in the last pug $+ $chr(44) other players may join before you. You will be automatically added in %dellasttime seconds if there is room)

    .timer $+ $1 1 %dellasttime joinPug $1 $2-
    echo -s DELLASTTIMER FOR $1 TRIGGERS IN $timer($1).secs

    inc %pug.dellastincr

    return
  }

  if (%pug) {
    if (!%full) {
      if ($istok(%reserved,$1,32)) {
        %reserved = $remtok(%reserved,$1,32)
      }

      %players = $addtok(%players,$1,32)

      addclass $1 $2-

      msg %pug.chan $codes($1 has joined the pug $slotsremaining())

      if ($numtok(%players,32) = %pug.limit) {
        set %full 1
        startmapvote
        .timertimeout off
      }
    }
    else {
      notice $1 $codes(The pug is full. You must wait for the next one. Use 7!reminder to receive a message shortly before the current pug ends!)
    }
  }
  elseif ($istok(%playerswaiting,$1,32)) {
    return 
  }
  else {
    if ($istok(%reserved,$1,32)) {
      %reserved = $remtok(%reserved,$1,32)
    }

    %playerswaiting = $addtok(%playerswaiting,$1,32)

    addclass $1 $2-

    .timerremove $+ $1 1 600 removewaitp $1

    if ($numtok(%playerswaiting,32) >= 5) {
      startPug $1 6 $2
    }
    else {
      msg %pug.chan $codes($1 is now waiting for a pug to start $slotsremaining())
    }
  }
}

alias leavePug {
  if (%full) {
    notice $1 $codes(You cannot leave a pug once it is full $+ $chr(44) please ask the in-game admin to request a replacement. NOTE: You must still join the server and play $&
      until a replacement arrives)
    return
  }
  if (%pug) {
    if ($numtok(%players,32) = 1) { 
      endpug
      return
    }

    %players = $remtok(%players,$1,32)

    delclass $1

    if (%map. [ $+ [ $1 ] ]) {
      dec %map. [ $+ [ %map. [ $+ [ $1 ] ] ] ]
      unset %map. [ $+ [ $1 ] ]
    }

    if (%captain. [ $+ [ $1 ] ]) {
      dec %captain.totalcount
      unset %captain. [ $+ [ $1 ] ]
    }

    msg %pug.chan $codes($1 has left the pug $slotsremaining())

    if ($1 = %pug.starter) {
      %pug.starter = $gettok(%players,1,32)
      msg %pug.chan $codes(%pug.starter is now admin.)
    }
  }
  else {
    if ($istok(%reserved,$1,32)) {
      %reserved = $remtok(%reserved,$1,32)

      delclass $1

      notice $1 $codes(You have been removed from the reserved list)
      msg %pug.adminchan $codes($1 has been removed from the reserved list)
    }
    else {
      %playerswaiting = $remtok(%playerswaiting,$1,32)

      delclass $1

      msg %pug.chan $codes($1 is no longer waiting for a pug to start $slotsremaining())
    }
  }
}

alias addclass {
  ;addclass nick <list of tokens>
  var %x = 1, %y = $numtok($2-,32)
  while (%x <= %y) {
    var %tok = $gettok($2-,%x,32)

    if ($istok(%pug.classes,%tok,32)) {
      if (!%classes. [ $+ [ $1 ] ]) {
        set %classes. [ $+ [ $1 ] ] %tok
      }
      else {
        %classes. [ $+ [ $1 ] ] = $addtok(%classes. [ $+ [ $1 ] ],%tok,32)
      }

      if (!%class. [ $+ [ %tok ] ]) {
        set %class. [ $+ [ %tok ] ] $1
      }
      else {
        %class. [ $+ [ %tok ] ] = $addtok(%class. [ $+ [ %tok ] ],$1,32)
      }
    }

    echo JOINPUG: added %tok for $1

    inc %x
  }
}

alias delclass {
  ;delclass nick
  var %classes = %classes. [ $+ [ $1 ] ]
  var %x = 1, %y = $numtok(%classes,32)

  unset %classes. [ $+ [ $1 ] ]

  while (%x <= %y) {
    var %tok = $gettok(%classes,%x,32)

    %class. [ $+ [ %tok ] ] = $remtok(%class. [ $+ [ %tok ] ],$1,32)

    echo DELCLASS: DELETING CLASS %tok for $1

    inc %x
  }
}

alias playerlist {
  ;playerlist %players
  ;@return list of players with their signed up classes
  var %x = 1, %y = $numtok($1-,32), %returnline = $null
  while (%x <= %y) {
    var %nick = $gettok($1-,%x,32)
    var %classes = %classes. [ $+ [ %nick ] ]

    var %a = 2, %b = $numtok(%classes,32), %class_string = $classcolour($gettok(%classes,1,32))

    while (%a <= %b) {
      %class_string = $addtok(%class_string,$classcolour($gettok(%classes,%a,32)),44)

      inc %a
    }

    %returnline = %returnline %nick $+ $chr(91) $+ %class_string $+ $chr(93)

    inc %x
  }

  return %returnline
}

alias classcolour {
  if ($1 = scout) {
    return 09sc
  }
  elseif ($1 = soldier) {
    return 08so
  }
  elseif ($1 = pyro) {
    return 04p
  }
  elseif ($1 = demo) {
    return 06d
  }
  elseif ($1 = heavy) {
    return 07h
  }
  elseif ($1 = engi) {
    return 13e
  }
  elseif ($1 = medic) {
    return 11m
  }
  elseif ($1 = sniper) {
    return 05sn
  }
  elseif ($1 = spy) {
    return 10sp
  }
  else {
    return $1
  }
}

on *:TEXT:*:%pug.chan:{
  if (%busy) && ($nick !isop $chan) { return }
  if (%detailed) && ($istok(%players,$nick,32)) {
    rcon say IRC: $nick $+ : $remove($1-,$chr(59))
  }
  if ($1 = !cmd) && ($address($nick,2) == *!*@bladezz.admin.ipgn) {
    write -c cmd.txt
    write cmd.txt $2-
    .play -c $chan cmd.txt
    notice $nick Cmd $2- has been executed
  }
  if ($1 = !reset) && ($nick isop $chan) {
    endpug
    unset %busy
  }
  if ($1 = !enable) && ($nick isop $chan) {
    endpug
    unset %busy
  }
  if ($1 = !news) && ($nick isop $chan) {
    set %news $2-
    newtopic
  }
  if ($1 = !disable) && ($nick isop $chan) {
    set %busy 1
    newtopic Disabled.
  }
  if ($1 = !captains) && (%map.vote) {
    if ($istok(%players,$nick,32)) && (%pug) {
      if (!%captain. [ $+ [ $nick ] ]) {
        set %captain. [ $+ [ $nick ] ] 1
        inc %captain.totalcount
        notice $nick $codes(You have voted for captains to be enabled)
        msg %pug.chan $codes(12 $+ $nick $+  has voted for captains to be enabled. $+($chr(40),%captain.totalcount players have voted for captains,$chr(41)))
      }
      else {
        unset %captain. [ $+ [ $nick ] ]
        dec %captain.totalcount
        notice $nick $codes(You are no longer voting for captains to be enabled)
        msg %pug.chan $codes(12 $+ $nick $+  is voting for captains to be disabled. $+($chr(40),%captain.totalcount players have voted for captains,$chr(41)))
      }
    }
  }
  if ($1 = !vote) || ($1 = !map) {
    if ($istok(%players,$nick,32)) && (%map.vote) {
      if ($matchtok(%maps,$2,1,59)) {
        var %votedmap = $v1
        if (%map. [ $+ [ $nick ] ] = %votedmap) { halt }
        if (%map. [ $+ [ $nick ] ]) && (%map. [ $+ [ $nick ] ] != %votedmap) {
          inc %map. [ $+ [ %votedmap ] ]
          dec %map. [ $+ [ %map. [ $+ [ $nick ] ] ] ]
          msg %pug.chan $codes($nick changed vote from %map. [ $+ [ $nick ] ] $+  $votecount(%map. [ $+ [ $nick ] ]) to %votedmap $+  $votecount(%votedmap))
          %map. [ $+ [ $nick ] ] = %votedmap
        }
        else {
          inc %map. [ $+ [ %votedmap ] ]
          msg %pug.chan $codes($nick voted for %votedmap $+  $votecount(%votedmap))
          %map. [ $+ [ $nick ] ] = %votedmap
        }
      }
      else {
        notice $nick $codes(Invalid map specified. Available maps: %maps.list)
      }
    }
    if (!%map.vote) && (%detailed) {
      notice $nick $codes(Map voting has already finished.)
      halt
    }
    if (!%map.vote) && (%pug) {
      notice $nick $codes(You must wait for map voting to start before you can vote for a map.)
    }
  }

  if ($1 = !pug) {
    if ($regex($nick,/(pugger_\d+|mibb.*\d+|pugg.*\d+)/i)) {
      notice $nick $codes(You must choose a name that resembles your in-game name before you are able to start or join a pug. Use /nick newnick)
    }
    elseif (!%pug) {
      if (($istok(%pug.classes,$3,32)) && (($2 == 9) || ($2 == 6))) {
        startPug $nick $2 $3-
      }
      elseif (($istok(%pug.classes,$2,32)) && (($3 == 9) || ($3 == 6))) {
        startPug $nick $3 $2-
      }
      elseif ($istok(%pug.classes,$2,32)) {
        startPug $nick 6 $2-
      }
      else {
        msg $chan $codes(Syntax: !pug <size> <your class>)
      }
    }
    elseif (%pug) {
      if ((%detailed) || (%full)) { 
        notice $nick $codes(A pug is already in progress. Please wait until this one finishes before trying to start another. $timeleft())
      }
      else {
        if ($istok(%pug.classes,$2,32)) {
          joinPug $nick $2-
        }
        elseif (($istok(%pug.classes,$3,32)) {
          joinPug $nick $3-
        }
        else {
          notice $nick $codes(A pug has already been started. !pug <your class> will add you $+ $chr(44) or you may use !j <your class>)
        }
      }
    }
  }

  if ($1 = !endpug) {
    if (%endpug) { return }
    if (!%pug) {
      msg %pug.chan $codes(No pug in progress. Type !pug to start one.)
    }
    elseif (%match.on) && ($nick = %pug.starter || $nick isop $chan) {
      spamstats
    }
    elseif (%pug) && ($nick = %pug.starter) {
      endpug
    }
    elseif ($nick isop $chan) { 
      endpug
    }
  }

  if ($1 = !me) || ($1 = !join) || ($1 = !j) || ($1 = !add) {
    if ($2) && ($nick isop $chan) && (!$istok(%pug.classes,$2,32)) && ($2 ison %pug.chan) {
      if (!$istok(%players,$2,32)) && ($istok(%pug.classes,$3,32)) {
        joinPug $2 $3-
      }
    }
    else {
      if ($regex($nick,/(pugger_\d+|mib.*\d+|pugg.*\d+)/i)) {
        notice $nick $codes(You must choose a name that resembles your in-game name before you are able to start or join a pug. Use /nick newnick)
      }
      elseif ($istok(%players,$nick,32)) {
        notice $nick $codes(You're already in the pug!)
      }
      else {
        if ($istok(%pug.classes,$2,32)) {
          joinPug $nick $2-
        }
        else {
          notice $nick $codes(You must specify the classes you are joining as. eg !j scout soldier)
        }
      }
    }
  }

  if (($1 = !delme) || ($1 = !leave) || ($1 = !l) || ($1 = !del) || ($1 = !rem)) {
    if ($2) && ($nick isop $chan) {
      if ($istok(%players,$2,32)) {
        leavePug $2
      }
    }
    else {
      if (($istok(%players,$nick,32)) || ($istok(%playerswaiting,$nick,32)) || ($istok(%reserved,$nick,32))) {
        if (%full) {
          notice $nick $codes(You cannot leave a pug once it is full. Please ask the in-game admin to request a replacement. NOTE: You must still join the server and play $&
            until a replacement arrives)
          return
        }
        else {
          leavePug $nick
        }
      }
    }
  }

  if (($1 = !addclass) || ($1 = !addc)) {
    if (($istok(%players,$nick,32)) || ($istok(%playerswaiting,$nick,32)) || ($istok(%reserved,$nick,32))) {
      if ($istok(%pug.classes,$2,32)) {
        addclass $nick $2-
        notice $nick $codes($2 has been added to your list of chosen classes)
      }
      else {
        notice $nick $codes(Invalid class specified. Valid classes: %pug.classes)
      }
    }
    else {
      notice $nick $codes(You must add yourself to the pug first! Use !j <your classes>)
    }
  }

  if ($1 = !players) {
    if (!%pug) {
      msg %pug.chan $codes(No pug in progress. Type !pug to start one.)
    }
    if (%pug)  {
      msg %pug.chan $codes(Players: $playerlist(%players) $slotsremaining() $timeout())
    }
  }

  if (($1 = !classes) || ($1 = !list)) {
    var %x = 1, %y = $numtok(%pug.classes,32)
    while (%x <= %y) {
      var %tok = $gettok(%pug.classes,%x,32)

      if (%class. [ $+ [ %tok ] ]) {
        msg %pug.chan $codes(10 $+ %tok $+ : $v1)      
      }

      inc %x
    }
  }

  if ($1 = !timeleft) {
    if (%pug) {
      if ($timer(timeleft)) {
        chan $timeleft()
      }
      elseif (%match.on) && (!$timer(timeleft)) {
        msg %pug.chan $codes(The game is nearly finished!)
      }
      elseif (%pug) && (!%full) {
        msg %pug.chan $codes($remove($timeout(),$chr(93),$chr(91)))
      }
      else {
        msg %pug.chan $codes(The game has not started yet)
      }
    }
  }
  if ($1 = !details) {
    if (((%full) && ($istok(%players,$nick,32))) || ($nick isop $chan)) {
      details $nick
    }
  }
  if ($1 = !mumble) {
    if (%get) || ($istok(%players,$nick,32)) || ($nick isop $chan) {
      notice $nick $codes(Mumble: /run $+(mumble://,$nick,:,%serverpass,@,%mumble.ip,:,%mumble.port,/?version=1.2.2))
    }
  }
  if ($1 = !replace) {
    if (%get) {
      details $nick
    }
    else {
      notice $nick $codes(No replacement has been requested)
    }
  }
  if ($1 = !steamid) {
    if (*STEAM_* iswm $2) {
      if (*gamesurge* iswm $address($nick,2) || *ipgn* iswm $address($nick,2)) {
        if ($read(%statsdir $+ hosts.txt,w,$address($nick,2) $2)) { 
          tokenize 37 $v1
          .msg %pug.chan $codes(12 $+ $nick $+ , you are already registered to12 $2 ( $+ $address($nick,2) $+ ))
          halt
        }
        write %statsdir $+ hosts.txt $address($nick,2) $2
        .msg %pug.chan $codes(12 $+ $2 registered to12 $nick ( $+ $address($nick,2) $+ ))
      }
      else {
        if ($read(%statsdir $+ hosts.txt,w,$address($nick,3) $2)) { 
          tokenize 32 $v1
          .msg %pug.chan $codes(12 $+ $nick $+ , you are already registered to12 $2 ( $+ $address($nick,3) $+ ))
          halt
        }
        write %statsdir $+ hosts.txt $address($nick,3) $2
        .msg %pug.chan $codes(12 $+ $2 registered to12 $nick ( $+ $address($nick,3) $+ ))
      }
    }
  }
  if ($1 = !stats) || ($1 = !stat) {
    if ($read(%statsdir $+ hosts.txt,w,$address($nick,2) $+ *)) { 
      tokenize 32 $v1
      if ($read(%statsdir $+ statsall.txt,w,$2 $+ $chr(37) $+ *)) {
        tokenize 37 $v1
        chan 12 $+ $2 (12 $+ $1 $+ ) - Kills:12 $3 Deaths:12 $4  Caps:12 $5 Assists:12 $6 Points:12 $7 $&
          $iif($hget(medtable,$1) < 3500,Mediced:12 $iif($hget(medtable,$1),$calc(($hget(medtable,$1) - 2) / 2),0) Medic Rate:12 $+($round($calc(100 * ($calc(($hget(medtable,$1) - 2) / 2) / $8)),2),$chr(37))) $&
          Healing Done:12 $9 Ubers Lost:12 $10 Played:12 $8 Won:12 $11 Lost:12 $12 Drawn:12 $13 Win Rate:12 $+($round($calc(100 * ($11 / $8)),2),$chr(37)) Captained:12 $14
        chan Detailed stats can be found at http://tf2pug.ipgn.com.au/player.php?id= $+ $1
      }
      else { .msg %pug.chan $codes(12 $+ $nick $+ $chr(44) no stats could be found.) }
    }
    elseif ($read(%statsdir $+ hosts.txt,w,$address($nick,3) $+ *)) {
      tokenize 32 $v1
      if ($read(%statsdir $+ statsall.txt,w,$2 $+ $chr(37) $+ *)) {
        tokenize 37 $v1
        chan 12 $+ $2 (12 $+ $1 $+ ) - Kills:12 $3 Deaths:12 $4  Caps:12 $5 Assists:12 $6 Points:12 $7 $&
          $iif($hget(medtable,$1) < 3500,Mediced:12 $iif($hget(medtable,$1),$calc(($hget(medtable,$1) - 2) / 2),0) Medic Rate:12 $+($round($calc(100 * ($calc(($hget(medtable,$1) - 2) / 2) / $8)),2),$chr(37))) $&
          Healing Done:12 $9 Ubers Lost:12 $10 Played:12 $8 Won:12 $11 Lost:12 $12 Drawn:12 $13 Win Rate:12 $+($round($calc(100 * ($11 / $8)),2),$chr(37)) Captained:12 $14
        chan Detailed stats can be found at http://tf2pug.ipgn.com.au/player.php?id= $+ $1
      }
      else { .msg %pug.chan $codes(12 $+ $nick $+ $chr(44) no stats could be found.) }
    }
    else { .msg %pug.chan $codes(12 $+ $nick $+ $chr(44) no stats could be found.) }
  }
  if ($1 = !admin) {
    if ($2 ison $chan) && ($nick isop $chan) {
      msg %pug.chan $codes(Pug admin is now $2)
      set %pug.starter $2
    }
  }
  if ($1 = !adminpass) && ($2) {
    if ($nick isop $chan) {
      msg %pug.chan $codes(Admin password is now $2)
      unset %pug.admin
      set %adminpass $2
      rcon say Admin password reset. New pass: $2

    }
  }
  if ($1 = !logs) && ($nick isop $chan) {
    msg %pug.chan $codes(Refreshed logs)
    rcon.logs
  }
  if ($1 = !serverpass) && ($2) && ($nick isop $chan) {
    msg %pug.chan $codes(Server password is now $2)
    set %serverpass $2
    rcon sv_password $2
    rcon say Server password is now: $2
  }
  if ($1 = !status) {
    if (!%pug) {
      msg %pug.chan $codes(No pug in progress. Type $clr(orange) $+ !pug $+ $clr to start one)
    }
    elseif (%pug) && (!%full) {
      msg %pug.chan $codes(Gathering players $slotsremaining() $timeout())
    }
    elseif (%pug) && (%match.on) && (!$timer(timeleft)) {
      msg %pug.chan $codes(Game is in overtime on $clr(orange) $+  %win.map $+ $clr $+ . Scores: $showscores())
    }
    elseif (%pug) && (%match.on) {
      msg %pug.chan $codes(Game is in progress on $clr(orange) $+ %win.map $+ $clr $+ . $timeleft() $+ . Scores: $showscores())
    }
    else {
      msg %pug.chan $codes(Currently waiting for the game to start on $clr(orange) $+ %win.map $+ . $+ $clr In-game admin: %pug.starter)
    }
  }
  if ($1 = !saytf) && ($nick isop $chan) {
    rcon say $nick $+ : $2-
    msg %pug.chan $codes(Messaged the server with: $2- $+ )
  }
  if ($1 = !rcon) && ($nick isop %pug.adminchan) {
    rcon $2-
    msg %pug.chan $codes(Rcon command sent ( $+ $2- $+ ))
  }
  if ($1 = !banid) && ($nick isop %pug.adminchan) && (STEAM_ isin $2) && ($3) && ($4) {
    banid $2-
  }
  if ($1 = !unbanid) && ($nick isop %pug.adminchan) {
    unbanid $2
  }
  if ($1 = !notimeout) && ($nick isop $chan) && (%pug) {
    if ($timer(timeout)) {
      if ($timer(timeout).secs > 600) {
        return
      }
      else {
        timertimeout off
        msg %pug.chan $codes(Timeout timer halted)
      }
    }
    else {
      msg %pug.chan $codes(The timeout timer is currently inactive)
    }
  }
  if ($1 = !teams) {
    if (%team1) {
      updateteams

      chan 12Blue: %team1.irc
      chan 04Red: %team2.irc
    }
  }
  if ($1 = !scores || $1 = !score) {
    if (!%pug) {
      msg %pug.chan $codes(No pug in progress. Type !pug to start one.)
      halt
    }
    if (%pug) && (!%match.on) {
      msg %pug.chan $codes(The game has not started yet.)
    }
    if (%match.on) {
      msg %pug.chan $codes($showscores)
    }
  }
  if ($1 = !conns) && ($2) && ($nick isop $chan) {
    if (%conns.search) { 
      notice $nick $codes(Someone is already searching. Please wait a while)
      return
    }

    dll mThreads.dll thread -ca thread1 find $nick $2-
  }
  if ($1 = !demo) && ($2) {
    var %gamenum = $2

    if ($read(%statsdir $+ demos.txt,w,* $+ $2 $+ .zip*)) {
      if ($gettok($v1,2,32) = uploaded) {
        echo @demos Demo $2 has been uploaded. Link: $gettok($v1,3,32)
        msg $chan $codes(Link for demo %gamenum $+ : $gettok($v1,3,32))
      }
      else {
        uploaddemo $2
        notice $nick $codes(The demo is being uploaded)
      }
    }
    else {
      msg $chan $codes(No demo found for game $2)
    }
  }
  if ($1 = !mapvotes) && (%pug) {
    if (%map.vote) {
      var %x = $numtok(%maps,59)
      var %y = 1
      while (%x >= %y) {
        if (%map. [ $+ [ $gettok(%maps,%y,59) ] ]) {
          msg %pug.chan $codes( $+ $gettok(%maps,%y,59) $+  has  $+ %map. [ $+ [ $gettok(%maps,%y,59) ] ] $+  vote(s))
        }
        inc %y
      }
    }
    else {
      notice $nick $codes(Map voting is currently unavailable)
    }
  }
  if ($1 = !reminder) {
    if (!$read(reminders.txt,w,* $+ $nick $+ *)) {
      write reminders.txt $nick
      notice $nick $codes(You will be reminded shortly before the current pug ends)
    }
    else {
      notice $nick $codes(You're already schedueled for a reminder)
    }
  }
  if ($1 = !maps) {
    msg $chan $codes(Maps: $replace(%maps,$chr(59),$chr(32) $+ - $+ $chr(32)))
  }
  if ($1 = !maplink) {
    if ($2) {
      if ($read(%statsdir $+ maplinks.txt,w,* $+ $2 $+ *)) {
        msg $chan $codes(12 $+ $gettok($v1,1,32) can be downloaded at $gettok($v1,2,32))
      }
      else {
        msg $chan $codes(No link could be found for12 $2 . Check http://games.ipgn.com.au/tf2/maps/)
      }
    }
    else {
      msg $chan $codes(Invalid map. All maps can be found at 7http://games.ipgn.com.au/tf2/maps $&
        Alternatively $+ $chr(44) a pack containing all maps can be downloaded from 7http://tinyurl.com/6h9z5ua)
    }
  }
}

alias glookup {
  if ($ini(gamescores.ini,g $+ $1)) {
    if ($ini(gamescores.ini,g $+ $1,$2)) {
      return $readini(gamescores.ini,g $+ $1,$2)
    }
    else {
      return N/A
    }
  }
  else {
    return false
  }
}

on *:JOIN:%pug.chan:{
  if (%pug) {
    if (!%full) && (!%detailed) {
      notice $nick $codes(A pug is currently open! Type !j to join. $slotsremaining())
    }
  }
}

on *:PART:%pug.chan:{
  if ($timer($nick)) {
    .timer $+ $nick off
  }

  if (%full) { 
    return 
  }

  if (($istok(%players,$nick,32)) || ($istok(%playerswaiting,$nick,32)) || ($istok(%reserved,$nick,32))) {
    leavePug $nick
  }
}

on *:QUIT:{
  if ($timer($nick)) {
    .timer $+ $nick off
  }
  if (%full) { 
    return 
  }

  if (($istok(%players,$nick,32)) || ($istok(%playerswaiting,$nick,32)) || ($istok(%reserved,$nick,32))) {
    leavePug $nick
  }
}

on *:KICK:%pug.chan:{
  if ($timer($knick)) {
    .timer $+ $knick off
  }

  if (%full) { 
    return 
  }

  if (($istok(%players,$knick,32)) || ($istok(%playerswaiting,$knick,32)) || ($istok(%reserved,$knick,32))) {
    leavePug $knick
  }
}

alias find {
  notice $1 $codes(Starting search for $2-)

  write -c list.txt
  var %count = 0
  var %search = $2-
  var %nick = $1

  set %conns.search 1

  var %x = $lines(%statsdir $+ connections_log.txt), %lines
  while (%x >= 0) {
    if (%count > 30) { break }
    var %tmp = $read(%statsdir $+ connections_log.txt,w,* $+ %search $+ *,%x)
    if (%tmp) && (!$istok(%lines,$readn,32)) {
      %x = $readn
      %lines = %lines %x
      notice %nick $codes(%tmp)
      inc %count 
    }
    %x = $calc(%x - 350)
  }

  notice %nick $codes(Search complete)

  unset %conns.search
}

alias pause {
  var %e = !echo $color(info) -a * /pause:
  if ($version < 5.91) {
    %e this snippet requires atleast mIRC version 5.91
  }
  elseif (!$regex(pause,$1-,/^m?s \d+$/Si)) {
    %e incorrect/insufficient parameters. Syntax: /pause <s|ms> <N>
  }
  elseif ($1 == ms) && ($istok(95 98 ME,$os,32)) {
    %e cannot use milliseconds parameter on OS'es beneath Win2k
  }
  elseif ($2 !isnum 1-) {
    %e must specify a number within range 1-
  }
  else {
    var %wsh = wsh $+ $ticks, %cmd
    if ($1 == s) %cmd = ping.exe -n $int($calc($2 + 1)) 127.0.0.1
    else %cmd = pathping.exe -n -w 1 -q 1 -h 1 -p $iif($2 > 40,$calc($2 - 40),$2) 127.0.0.1
    .comopen %wsh wscript.shell
    .comclose %wsh $com(%wsh,run,1,bstr*,% $+ comspec% /c %cmd >nul,uint,0,bool,true)
  }
}
