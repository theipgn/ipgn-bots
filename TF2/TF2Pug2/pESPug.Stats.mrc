alias add {
  var %keys = id;name;kill;death;cap;assist;points;games;healing;uberlost;win;loss;draw;captain;damage;ammopack_small;ammopack_medium;tf_ammo_pack;medkit_small;medkit_medium;healing_received;ammopack_large;medkit_large
  var %key = $findtok(%keys,$2,1,59)
  var %sl
  if (!%key) { echo -s INVALID KEY $2 SPECIFIED, SKIPPING | return }
  ;echo -s %keys %key
  if (!$2) {
    if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) { return }
    if ($read(%statsdir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)) { 
      var %oldname = $gettok($read(%statsdir $+ statsall.txt,w,$1 $+ $chr(37) $+ *),2,37)
    }
    else { 
      var %oldname = $sid($1)
    }
    write stats.txt $+($1,$chr(37),%oldname,$str($chr(37) $+ 0,$calc($numtok(%keys,59) - 2))) 
  } 
  if ($2 = captain) {
    if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) {
      %sl = $v1
      write -w $+ $1 $+ $chr(37) $+ * stats.txt $puttok(%sl,$calc($gettok(%sl,%key,37) + $3),%key,37)
    }
    else {
      %sl = $+($1,$chr(37),$sid($1),$str($chr(37) $+ 0,18))
      write stats.txt $puttok(%sl,$3,%key,37)
    }
  }
  elseif ($2 = name) {
    if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) {
      %sl = $v1
      write -l $+ $readn stats.txt $puttok(%sl,$3-,%key,37)
    }
    else {
      write stats.txt $+($1,$chr(37),$3-,$str($chr(37) $+ 0,$calc($numtok(%keys,59) - 2))) 
    }
  }
  else {
    if (%match.on) {
      if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) {
        %sl = $v1
        write -l $+ $readn stats.txt $puttok(%sl,$calc($gettok(%sl,%key,37) + $3),%key,37)
      }
      else { 
        %sl = $+($1,$chr(37),$sid($1),$str($chr(37) $+ 0,$calc($numtok(%keys,59) - 2))) 
        write stats.txt $puttok(%sl,$3,%key,37)
      }
    }
  }
}

alias addweapon {
  var %weapons = tf_projectile_rocket liberty_launcher rocketlauncher_directhit quake_rl shotgun_soldier unique_pickaxe pickaxe paintrain shovel scattergun soda_popper force_a_nature pistol_scout maxgun the_winger bat holy_mackerel flamethrower shotgun_pyro fireaxe backburner axtinguisher flaregun deflect_flare deflect_promode deflect_rocket deflect_sticky taunt_pyro minigun shotgun_hwg fists natascha gloves taunt_heavy shotgun_primary pistol wrench obj_sentrygun obj_sentrygun2 obj_sentrygun3 sniperrifle machina bazaar_bargain smg shahanshah club tf_projectile_arrow compound_bow taunt_sniper tf_projectile_pipe tf_projectile_pipe_remote sword bottle revolver ambassador knife syringegun_medic battleneedle proto_syringe bonesaw blutsauger ubersaw solemn_vow world player fryingpan
  if ($2) && (!$istok(%weapons,$2,32)) { echo @weapons $clr(orange) $+ Weapon $2 detected... ignoring | write ignweapons.txt $2 $playerclass($1) | return }
  var %weap = $calc($findtok(%weapons,$2,1,32) + 1)
  var %wl
  if (!$2) {
    write weapons.txt $+($1,$str($chr(37) $+ 0,$numtok(%weapons,32)))
  }
  if (%match.on) && ($2) {
    if ($read(weapons.txt,w,$1 $+ $chr(37) $+ *)) {
      %wl = $v1
      write -l $+ $readn weapons.txt $puttok(%wl,$calc($gettok(%wl,%weap,37) + 1),%weap,37)
    }
    else {
      %weaponline = $+($1,$str($chr(37) $+ 0,$numtok(%weapons,32)))
      write weapons.txt $puttok(%weaponline,1,%weap,37)
    }
  }
}
