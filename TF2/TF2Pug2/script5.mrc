alias test {

  var %rcon.data = Team "Blue" triggered "pointcaptured" (cp "2") (cpname "Granary_cap_cp3") (numcappers "2") (player1 "[Fv]Greyhunter<329><STEAM_0:0:309237><Blue>") (position1 "-1709 7 -341") (player2 "-eS^jmh<332><STEAM_0:1:101867><Blue>") (position2 "-1556 32 -376")

  if ($regex(%rcon.data,/Team "(Blue|Red)" triggered "pointcaptured" \x28cp "([0-9]+)"\x29 \x28cpname "(.+)"\x29 \x28numcappers "([0-9]+)".+/)) {
    chan $clr($regml(1)) $+ $regml(1) $+ $clr triggered point captured. Control point: $regml(2). CP Name: $replace($regml(3),_,$chr(32)) $+ .

    var %numcappers = $regml(4)
    chan Number of cappers: %numcappers

    %str1 = \x28player(\d) "(.+)<(\d+)><(\S+)><(Blue|Red)>"\x29.+
    var %x = 0
    %str = $str(%str1,%numcappers)
    if ($regex(%rcon.data,%str)) {
      while (%x < %numcappers) {

        chan Capper # $+ $regml($calc(1 + (5 * %x))) $+ : $clr($regml($calc(5 + (5 * %x)))) $+ $regml($calc(2 + (%x * 5))) $+ $clr
        ;          add($regml($key3],"cap","1");
        ;          add($regml($key3],"points","2");

        inc %x   
      }

    }
  }

}
