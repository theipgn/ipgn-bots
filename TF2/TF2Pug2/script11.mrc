alias uploadweapon {

  ;-- Set these variables --
  %hostofphpweaponfile = games.ipgn.com.au
  %locationofphpweaponfile = /tf2/tf2pug/uploadweap.php?password=HeatoN
  ;-------------------------

  %dest = $$1-
  if ((!$file(%dest)) || ($isdir(%dest))) return
  .remove uploadweapon.tmp
  echo @uploadweapon Uploading file %dest $+ ....
  sockopen uploadweapon %hostofphpweaponfile 80
}
On *:sockopen:uploadweapon:{

  var %b = $md5() | write -c uploadweapon.tmp $+(--,%b,$lf,$&
    Content-Disposition: form-data; name="uploaded";,$&
    filename=",$nopath(%dest),",$lf,$lf)
  .copy -a " $+ %dest $+ " uploadweapon.tmp
  write uploadweapon.tmp -- $+ %b

  var %write = sockwrite -tn uploadweapon
  %write POST %locationofphpweaponfile HTTP/1.1
  %write Host: %hostofphpweaponfile
  %write Connection: close
  %write Content-Type: multipart/form-data; boundary= $+ %b
  %write Content-Length: $file(uploadweapon.tmp)
  %write
  .fopen uploadweapon uploadweapon.tmp
}

On *:sockwrite:uploadweapon:{
  if $sock(uploadweapon).sq > 8192 || !$fopen(uploadweapon) { return }
  if $fread(uploadweapon,8192,&a) {
    sockwrite uploadweapon &a
  }
  else .fclose uploadweapon
}

On *:sockread:uploadweapon:{
  var %temp
  sockread %temp
  if (%temp != $null) {
    echo @uploadweapon %temp
  }
  if & 2* iswm %temp {
    echo 3 @uploadweapon SUCCESS!
  }
  elseif & 4* iswm & temp {
    echo 4 @uploadweapon FAILED!
  }
}

On *:sockclose:uploadweapon:cleanuploadweapon
alias cleanuploadweapon {
  .sockclose uploadweapon
  if $fopen(uploadweapon) { .fclose uploadweapon }
  unset %dest
  .remove uploadweapon.tmp
}
