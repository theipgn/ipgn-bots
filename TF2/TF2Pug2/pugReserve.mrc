;Script to replace the voiced users hehe (essentially the same as voicing a user)

on *:TEXT:!*:?,%pug.adminchan:{
  if ($1 = !reserve2) && ($nick isop %pug.chan) {
    if ($2) && ($2 ison %pug.chan) {
      %reserved = $addtok(%reserved,$2,32)
      notice $2 $codes(A spot has been reserved for you in the next pug)

      set %class. [ $+ [ $2 ] ] $3
      inc %class. [ $+ [ $3 ] ] 1

      msg %pug.adminchan $codes(A spot in the next pug has been reserved for $2 by $nick $+($chr(40),$calc(5 - $numtok(%reserved,32)),/,5,$chr(32),reserve spots remaining,$chr(41)))
    }
    elseif ($istok(%reserved,$2,32)) {
      notice $2 $codes(You're already in the reserve list)
    }
    elseif ((!$2) || ($istok(%pug.classes,$2,32))) {
      %reserved = $addtok(%reserved,$nick,32)

      set %class. [ $+ [ $nick ] ] $2
      inc %class. [ $+ [ $2 ] ] 1

      notice $nick $codes(A spot has been reserved for you in the next pug)
      msg %pug.adminchan $codes(A spot in the next pug has been reserved for $nick)
    }
  }
  if ($1 = !delreserve2) && ($nick isop %pug.chan) {
    if ($2) && ($istok(%reserved,$2,32)) && ($2 ison %pug.chan) {
      leavePug $2

      msg %pug.adminchan $codes($2 has been removed from the reserve list by $nick $+($chr(40),$calc(5 - $numtok(%reserved,32)),/,5,$chr(32),reserve spots remaining,$chr(41)))
    }
    elseif ($istok(%reserved,$nick,32)) && (!$2) {
      leavePug $nick
    }
    else {
      msg $nick $codes(Neither you, nor the user you specified is on the reserved list)
    }
  }
  if ($1 = !reserved2) && ($nick isop %pug.chan) {
    msg %pug.adminchan $codes(Spots are being reserved for the following players: %reserved)
  }
}
