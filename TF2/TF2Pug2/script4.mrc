if ($regex(%rcon.data,"(.+)<([0-9]+)><(.+)><(Blue|Red)>" disconnected\)) {
  echo $regml(3) disconnected.

  if ($regml(3) == %pug.admin) {
    %adminpass = new
    rcon say $regml(1) left. Adminpass is now 'new'.
  }
  ;  add($regml(3], "name", $regml(1]);
}

if ($regex(%rcon.data,"(.+)<([0-9]+)><(.+)><>" STEAM USERID validated\)) {
  chan $regml(1) ( $+ $regml(3) $+ ) connected.
  ;  add($regml(3), "name", $regml(1));
  ;  joined($regml(3));
}
if ($regex(%rcon.data,"(.+)<([0-9]+)><(.+)><(Red|Blue)>" say "(.+)"\)) {
  tokenize 32 $regml(5)
  if ($1 = !teams) {
    rcon say Blue: $team1
    rcon say Red: $team2
  }
  if ($1 = !login) {
    if ($2 = %adminpass) {
      if (%pug.admin) { halt }
      else {
        rcon say $regml(1) ( $+ $regml(3) $+ ) logged in.
        chan $regml(1) ( $+ $regml(3) $+ ) logged in.
        %pug.admin = $regml(3)
      }
    }
  }
  if ($regml(3) = %pug.admin) {
    if ($1 = !password) && ($2) {
      rcon sv_password $2; say Password changed to $2
      %serverpass = $2
    }
    if ($1 == !irc) && ($2) {
      rcon say Message sent to IRC: $2-
      chan In-game: $2-
    }
    if ($1 == !get) {
      chan Requesting player! The server details are: connect %rcon.ip $+ : $+ %rcon.port $+ ;password %serverpass
      rcon say Requesting player
    }
    if ($1 == !start) {
      set %match.on 1

      rcon tv_record pug- $+ $date(HH-nn-ddd-dd-m-yyyy) $+ - $+ %map.win $+ ; exec start.cfg
      chan The game is going live! 
    }
    if ($1 == !endpug) {
      endpug
    }
    if ($1 == !k) {
      rcon kick $2-
    }
    if ($1 == !kickid) {
      rcon kickid $2-
    }
  }
}

if (%match.on == 1) {
  if ($regex(%rcon.data,World triggered "Round_Win".+winner.+"(Blue|Red)"\)) {
    chan Round win: $clr($regml(1)) $+ $regml(1) $+ $clr won the round!
  }
  if ($regex(%rcon.data,World triggered "Round_Overtime"\)) {
    chan Round overtime!
  }
  if ($regex(%rcon.data,World triggered "Round_Length".+seconds.+"([0-9]+.[0-9]+)\)) {
    chan Round length: $regml(1)
  }
  if ($regex(%rcon.data,World triggered "Round_Start"\)) {
    chan Round start
  }
  if ($regex(%rcon.data,World triggered "Round_Setup_Begin"\)) {
    chan Round setup begin
  }
  if ($regex(%rcon.data,World triggered "Mini_Round_Win".+winner.+"(Blue|Red)".+"round_([0-9]+)"\)) {
    chan Mini round # $+ $regml(2) win: $clr($regml(1)) $+ $regml(1) $+ $clr
  }
  if ($regex(%rcon.data,World triggered "Mini_Round_Length".+seconds "([0-9]+.[0-9]+)".+\)) {
    chan Mini round length: $regml(1)
  }
  if ($regex(%rcon.data,World triggered "Round_Setup_End"\)) {
    chan Round setup end
  }
  if ($regex(%rcon.data,"(.*)<([0-9]+)><(.*)><(Red|Blue)>" killed "(.*)<([0-9]+)><(.*)><(Red|Blue)>" with "(.*)".+customkill "(.*)"\)) {

    if ($regml(10) = backstab) {
      ; chan 1.5 points:  $regml(1) killed  $regml(5) with  $regml(9) ( $+ $regml(10) $+ )
      ;        add($regml(3),"kill","1");
      ;        add($regml(3),"points","2");
    }
    if ($regml(10) = headshot) {
      ; chan 2 points:  $regml(1) killed  $regml(5) with  $regml(9) ( $+ $regml(10) $+ )
      ;        add($regml(3),"kill","1");
      ;        add($regml(3),"points","2");
    }
    ;      add($regml(7),"death","1");

  }
  if ($regex(%rcon.data,"(.*)<([0-9]+)><(.*)><(Red|Blue)>" killed "(.*)<([0-9]+)><(.*)><(Red|Blue)>" with "(.*)" .att\)) {
    ;      chan 1 point:  $regml(1) killed  $regml(5) with $regml(9)
    ;      add($regml(3),"kill","1");
    ;      add($regml(3),"points","1");
    ;      add($regml(7),"death","1");
  }
  if ($regex(%rcon.data,"(.*)<([0-9]+)><(.*)><(Red|Blue)>" triggered "kill assist" against "(.*)<([0-9]+)><(.*)><(Red|Blue)>"\)) {
    ;      chan 0.5 points:  $regml(1) triggered kill assist against $regml(5)
    ;      add($regml(3),"assist","1");
    ;      add($regml(3),"points","0.5");
  }
  if ($regex(%rcon.data,"(.*)<([0-9]+)><(.*)><(Red|Blue)>" triggered "flagevent" .event "(.*)". .posi\)) {
    if ($regml(5) == captured) {
      chan $clr($regml(4)) $+ $regml(1) $+ $clr captured the intelligence.
      ;        add($regml(3),"cap","1");
      ;        add($regml(3),"points","2");
    }
    if ($regml(5) == defended) {
      chan $clr($regml(4)) $+ $regml(1) $+ $clr defended the intelligence.
      ;        add($regml(3],"points","1");
    }
  }
  if ($regex(%rcon.data,/Team "(Blue|Red)" triggered "pointcaptured" \x28cp "([0-9]+)"\x29 \x28cpname "(.+)"\x29 \x28numcappers "([0-9]+)".+/)) {
    chan $clr($regml(1)) $+ $regml(1) $+ $clr triggered point captured. Control point: $regml(2). CP Name: $replace($regml(3),_,$chr(32)) $+ .

    var %numcappers = $regml(4)
    chan Number of cappers: %numcappers

    %str1 = \x28player(\d) "(.+)<(\d+)><(\S+)><(Blue|Red)>"\x29.+
    var %x = 0
    %str = $str(%str1,%numcappers)
    if ($regex(%rcon.data,%str)) {
      while (%x < %numcappers) {

        chan Capper # $+ $regml($calc(1 + (5 * %x))) $+ : $clr($regml($calc(5 + (5 * %x)))) $+ $regml($calc(2 + (%x * 5))) $+ $clr
        ;          add($regml($key3],"cap","1");
        ;          add($regml($key3],"points","2");

        inc %x   
      }

    }
  }

  if ($regex(%rcon.data,"(.*)<([0-9]+)><(.*)><(Red|Blue)>" changed role to "(.*)"\)) {
    chan $clr($regml(4)) $+ $regml(1) $+ $clr changed role to $clr($regml(4)) $+ $regml(5) $+ $clr
  }
  if ($regex(%rcon.data,"(.*)<([0-9]+)><(.*)><(Red|Blue)>" triggered "domination" against "(.*)<([0-9]+)><(.*)><(Red|Blue)>"\)) {
    ;      chan 1 point: $regml(1) triggered domination against $regml(5)
  }
  if ($regex(%rcon.data,"(.*)<([0-9]+)><(.*)><(Red|Blue)>" triggered "revenge"\)) {
    ;      chan 1 point: $regml(1) triggered revenge
    ;      add($regml(3),"points","1");
  }
  if ($regex(%rcon.data,"(.*)<([0-9]+)><(.*)><(Red|Blue)>" triggered "killedobject"\)) {
    ;      chan 1 point: $regml(1) triggered destruction
    ;      add($regml(3),"points","1");
  }

  if ($regex(%rcon.data,"(.*)<([0-9]+)><(.*)><(Red|Blue)>" triggered "chargedeployed"\)) {
    ;      chan 1 point:  $regml(1) triggered charge deployed
    ;      add($regml(3],"points","1");
  }
  if ($regex(%rcon.data,"(.*)<([0-9]+)><(.*)><(Red|Blue)>" triggered "captureblocked".+cp "([0-9]+)".+.+cpname "#?(\w*)".+\)) {
    ;      chan 1 point: $regml(1) triggered capture blocked CP: $regml(5) CP Name: $regml(6)
    ;      add($regml(3],"points","1");
  }
  if ($regex(%rcon.data,Team "(Blue|Red)" current score "([0-9]+)" with "([0-9]+)" players\)) {
    chan $clr($regml(1)) $+ $regml(1) $+ $clr current score: $regml(2) with $regml(3) players
  }
  if ($regex(%rcon.data,Team "(Blue|Red)" final score "([0-9]+)" with "([0-9]+)" players\)) {
    chan $clr($regml(1)) $+ $regml(1) $+ $clr final score: $regml(2) with  $regml(3) players
  }
  if ($regex(%rcon.data,World triggered "Game_Over" reason "(.*)"\)) {
    chan Game over: $regml(1)
    .timer 1 5 endpug
  }
}
