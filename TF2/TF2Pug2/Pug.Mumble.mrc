alias mumbleServer {

  return
  if ($1 = password) {
    ssh mumble --server=1 --set-config password $2
  }
  if ($1 = restart) {
    ssh mumble --server=1 --restart
  }
}
