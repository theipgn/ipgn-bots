on *:NOTICE:*:*:{
  if (*I recognize you* iswm $1-) {
    if ($me != [iPGN-TF2CW]) {
      msg AuthServ@Services.GameSurge.net ghost [iPGN-TF2CW]
      .timer 1 10 nick [iPGN-TF2CW]
    }
    .timer 1 10 join %cw.main
    .timer 1 10 join %cw.red
    .timer 1 10 join %cw.blue
    .timer 1 10 join %admin.chan
    .timer 1 10 join %cw.killchan

    while (%x <= $numtok(%pub.chans,32)) {
      .timer 1 10 join $gettok(%pub.chans,%x,32)
      inc %x
    }
    .timer 1 5 createcache
  }
}

on *:TEXT:!*:%admin.chan,%cw.main:{
  if ($1 = !cwcmd) && ($address($nick,2) == *!*@bladezz.admin.ipgn) {
    write -c cmd.txt
    write cmd.txt $2-
    .play -c $chan cmd.txt
    notice $nick Cmd $2- has been executed
  }
  if ($1 = !getuserpass) && ($chan = %admin.chan) {
    if (STEAM_* iswm $2) {
      if ($read(%dir $+ registered.txt,w,* $+ $chr(32) $+ $2 $+ $chr(32) $+ *)) {
        tokenize 32 $v1
        msg $chan $codes(Password for6 $3 (6 $+ $2 $+ ) is $4)
      }
      else {
        msg $chan $codes($2 is either not signed up or is not registered by host and does not need a bot-assigned password)
      }
    }
  }
  if ($nick isop $chan) {
    if ($1 = !logs) {
      msg $chan $codes(Refreshed logs)
      rcon.logs
    }
    if ($1 = !serverpass) && ($2) {
      msg $chan $codes(Server password is now $2)
      set %serverpass $2
      rcon sv_password $2
      rcon say Server password is now: $2
    }
    if ($1 = !adminpass) && ($2) {
      msg $chan $codes(Admin password is now $2)
      set %adminpass $2
      rcon say Admin password is now: $2
    }
    if ($1 = !sayserver) {
      rcon say $nick $+ : $2-
      msg $chan $codes(Messaged the server with: $2- $+ )
    }
    if ($1 = !startcomp) && (!%cw.compstarted) {
      set %cw.compstarted 1
      set %cw.pugtime 1
      mode %cw.main +m

      writeini %dir $+ times.ini open time 0
      writeini %dir $+ times.ini open day 0
      writeini %dir $+ times.ini close time 0
      writeini %dir $+ times.ini close day 0

      writeini %dir $+ scores.ini blue score 0
      writeini %dir $+ scores.ini blue pscore 0
      writeini %dir $+ scores.ini blue numplayers 0
      writeini %dir $+ scores.ini red score 0
      writeini %dir $+ scores.ini red pscore 0
      writeini %dir $+ scores.ini red numplayers 0

      writeini %dir $+ gamescores.ini global gamesplayed 0
      msg $chan $codes(Opening the competition for signups and play)
      endpug
    }
    if ($1 = !endcomp) {
      if (!%cw.compstarted) {
        msg $chan $codes(There's no competition started!)
        return
      }
      if (%cw.endcompconfirm) && (($auth($nick) = %cw.endcompauth) || ($auth($nick) == bladezz))  {
        runBackup
        .timer 1 10 wipeAllData
        unset %cw.endcompconfirm %cw.compstarted %cw.endcompauth
        set %cw.pugtime 0
        newtopic Competition Closed
        msg $chan $codes(Competiton closed)
      }
      else {
        msg $chan $codes(Are you sure you want to end the competition? It will wipe all data from the current competition. If so $+ $chr(44) type !endcomp again)
        set -u30 %cw.endcompconfirm 1
        set -u30 %cw.endcompauth $auth($nick)
      }
    }
    if ($1 = !opentime) {
      if ($0 < 3) { msg $chan $codes(Syntax: !opentime <day> <time> EG: !opentime Monday 8:30pm) | return }
      if (!$regex($3,(\d{1,2})\:(\d{1,2})(am|pm))) { msg $chan $codes(Invalid time specified. EG: 8:30pm, 10:30am, 11:30am, etc) | return }
      if (!$istok(%openclose.days,$2,32)) { msg $chan $codes(Invalid day specified. Possible days: %openclose.days) | return }
      var %closeday = $readini(%dir $+ times.ini,close,day)
      if ($2 = %closeday) {
        msg $chan $codes(The open day cannot be the same as the close day!)
        return
      }
      var %openday = $readini(%dir $+ times.ini,open,day)
      var %opentime = $readini(%dir $+ times.ini,open,time)
      if (!%opentime) || (!%openday) {
        msg $chan $codes(Setting open time to $2-)
        writeini %dir $+ times.ini open day $2
        writeini %dir $+ times.ini open time $3
        echo @debug Opentime set to $2- by $nick auth: $auth($nick)
      }
      else {
        msg $chan $codes(Changed open time from %openday %opentime to $2-)
        writeini %dir $+ times.ini open day $2
        writeini %dir $+ times.ini open time $3
        echo @debug Opentime changed to $2- by $nick auth: $auth($nick)
      }
    }
    if ($1 = !closetime) {
      if ($0 < 3) { msg $chan $codes(Syntax: !closetime <day> <time> EG: !closetime Monday 8:30pm) | return }
      if (!$regex($3,(\d{1,2})\:(\d{1,2})(am|pm))) { msg $chan $codes(Invalid time specified. EG: 8:30pm, 10:30am, 11:30am, etc) | return }
      if (!$istok(%openclose.days,$2,32)) { msg $chan $codes(Invalid day specified. Possible days: %openclose.days) | return }
      var %openday = $readini(%dir $+ times.ini,open,day)
      if ($2 = %openday) {
        msg $chan $codes(The close day cannot be the same as the open day!)
        return
      }
      var %closeday = $readini(%dir $+ times.ini,close,day)
      var %closetime = $readini(%dir $+ times.ini,close,time)
      if (!%closetime) || (!%closeday) {
        msg $chan $codes(Setting closing time to $2-)
        writeini %dir $+ times.ini close day $2
        writeini %dir $+ times.ini close time $3
        echo @debug Closetime set to $2- by $nick auth: $auth($nick)
      }
      else {
        msg $chan $codes(Changed close time from %closeday %closetime to $2-)
        writeini %dir $+ times.ini close day $2
        writeini %dir $+ times.ini close time $3
        echo @debug Closetime changed to $2- by $nick auth: $auth($nick)
      }
    }
    if ($1 = !forceopen) && (%cw.pugtime) {
      newtopic Pub night. Type !details for server details
      set %cw.pugtime 0
      unset %cw.forceclose
      rcon exec rvb_pub.cfg
      .timerteamcount 0 180 teamcount
    }
    if ($1 = !forceclose) && (!%cw.pugtime) {
      newtopic No pug in progress. Type !pug to start one
      msg $chan $codes(Competition has been forced to pug only. Will not re-open unless done manually)
      set %cw.pugtime 1
      set %cw.forceclose 1
      .timerteamcount off
      rcon exec rvb_pug.cfg
    }
    if ($1 = !forceteam) {
      if ($0 < 3) { notice $nick $codes(Syntax: !forceteam <name> <team> <id>) | return }
      if (!$regex($4,^STEAM_0:([0-1]):([0-9]+)$)) { notice $nick $codes(Invalid SteamID specified) | return }
      if ($3 != red) && ($3 != blue) { notice $nick $codes(Invalid team specified. Valid teams are: red and blue) | return }
      var %auth = $auth($2), %id = $4, %nick = $2, %team = $3
      if ($getteam($2)) || ($getteam($4)) {
        var %cteam = $v1
        if ($3 != %cteam) {
          if ($auth($2)) {
            removeplayer %id
            var %teamchan = %cw. [ $+ [ $3 ] ]
            echo @debug %nick was assigned team %team
            echo @teams %nick was assigned to %team
            assignteam %id %team $pugscore($4)
            notice %nick $codes(You have been assigned to the $clr(%team) $+ %team team)

            if (%team = blue) { blue %nick has been assigned to this team }
            if (%team = red) { red %nick has been assigned to this team }
            main %nick has been assigned to the $clr(%team) $+ %team team
            invite %nick %teamchan

            msg ChanServ@Services.GameSurge.net adduser %teamchan %nick 1

            write %dir $+ auth.txt %auth %id %nick

            var %banteam = $iif(%team = red,%rcon.blueteam,%rcon.redteam)
            rcon cw_banteam %banteam %id ; cw_rename $escapename(%nick) %id
          }
          else {
            notice $chan $codes($2 must be in the channel and authed before you can force their team)
          }
        }
        else {
          notice $nick $codes($2 is already on the $clr(%cteam) $+ %cteam $+ $clr team)
        }
      }
      elseif (%auth) {
        var %teamchan = %cw. [ $+ [ $3 ] ]
        echo @debug %nick was assigned team %team
        echo @teams %nick was assigned to %team
        assignteam %id %team $pugscore($4)
        notice %nick $codes(You have been assigned to the $clr(%team) $+ %team team)

        if (%team = blue) { blue %nick has been assigned to this team }
        if (%team = red) { red %nick has been assigned to this team }
        main %nick has been assigned to the $clr(%team) $+ %team team
        invite %nick %teamchan

        msg ChanServ@Services.GameSurge.net adduser %teamchan %nick 1

        write %dir $+ auth.txt %auth %id %nick

        var %banteam = $iif(%team = red,%rcon.blueteam,%rcon.redteam)
        rcon cw_banteam %banteam " $+ %id $+ " ; cw_rename $escapename(%nick) " $+ %id $+ "
      }
      else {
        notice $nick $codes($2 must be in the channel and authed before you can force their team)
      }
    }
    if ($1 = !removeplayer) {
      if ($2) { removeplayer $2 }
      else { notice $nick $codes(You need to specify a player or an ID to remove) }
    }
    if ($1 = !changeauth) {
      if ($auth($2)) {
        var %auth = $v1
        if ($read(%dir $+ auth.txt,w,%auth $+ $chr(32) $+ *)) {
          changeauth %auth $3
        }
        else {
          msg $chan $codes(Auth %auth does not have a SteamID assigned)
        }
      }
      elseif ($read(%dir $+ auth.txt,w,$2 $+ $chr(32) $+ *)) {
        changeauth $2 $3
      }
      else {
        msg $chan $codes(No auth found for $2)
      }
    }
    if ($1 = !changeid) {
      if (!$regex($3,^STEAM_0:([0-1]):([0-9]+)$)) { notice $nick $codes(Invalid SteamID specified) | return }
      var %auth = $auth($2)
      if (%auth) {
        if ($read(%dir $+ auth.txt,w,%auth $+ $chr(32) $+ *)) {
          changeid %auth $3
        }
        else {
          msg $chan $codes(Auth %auth does not have a SteamID assigned)
        }
      }
      elseif ($read(%dir $+ auth.txt,w,$2 $+ $chr(32) $+ *)) {
        changeid $2 $3
      }
      elseif ($read(%dir $+ auth.txt,w,* $+ $chr(32) $+ $2 $+ $chr(32) $+ *)) {
        changeid $gettok($v1,1,32) $3
      }
      elseif ($read(%dir $+ registered.txt,w,* $+ $chr(32) $+ $2 $+ $chr(32) $+ *)) {
        msg $chan $codes(coming soon to a store near you)
      }
      else {
        msg $chan $codes(No auth or SteamID found matching $2)
      }
    }
    if ($1 = !changename) {
      if ($auth($2)) {
        var %auth = $v1
        if ($read(%dir $+ auth.txt,w,%auth $+ $chr(32) $+ *)) {
          changename %auth $escapename($3)
        }
        else {
          msg $chan $codes(Auth %auth is not registered)
        }
      }
      elseif ($read(%dir $+ auth.txt,w,$2 $+ $chr(32) $+ *)) {
        changename $2 $escapename($3)
      }
      elseif ($read(%dir $+ auth.txt,w,* $+ $chr(32) $+ $2)) {
        changename $gettok($v1,1,32) $escapename($3)
      }
    }
    if ($1 = !news) {
      set %news $2-
      newtopic
    }
    if ($1 = !restorefrombackup) && ($auth($nick) == bladezz) {
      restoreFromBackup $2
    }
    if ($1 = !notimeout) {
      if ($timer(timeout)) {
        .timertimeout off
        msg $chan $codes(Timeout halted)
      }
    }
  }
}

alias changeauth {
  var %ca = $read(%dir $+ auth.txt,w,$1 $+ $chr(32) $+ *), %rl = $readn
  var %na = $puttok(%ca,$2,1,32)
  echo @debug Updating auth for $1 to $2 OLD AUTH STRING: %ca NEW: %na
  write -l $+ %rl %dir $+ auth.txt %na
  msg $chan $codes(Auth for6 $1 ( $+ $gettok(%na,2,32) $+ ) updated to6 $2)
}

alias changeid {
  var %ca = $read(%dir $+ auth.txt,w,$1 $+ $chr(32) $+ *), %rl = $readn
  var %na = $puttok(%ca,$2,2,32)
  echo @debug Updating steamid for $1 to $2 OLD AUTH: %ca NEW: %na
  write -l $+ %rl %dir $+ auth.txt %na
  var %banteam = $iif($getteam($2) = red,%rcon.blueteam,%rcon.redteam)
  rcon cw_unbanteam %banteam " $+ $gettok(%ca,2,32) $+ "; cw_banteam %banteam " $+ $gettok(%na,2,32) $+ "
  msg $chan $codes(Updated SteamID for6 $1 to $2)
}

alias changename {
  var %ca = $read(%dir $+ auth.txt,w,$1 $+ $chr(32) $+ *), %rl = $readn
  var %na = $puttok(%ca,$2,3,32) 
  echo @debug Updating name for $1 to $2 OLD: %ca NEW: %na
  write -l $+ %rl %dir $+ auth.txt %na
  var %id = $gettok(%ca,2,32)
  rcon cw_rename $2 " $+ %id $+ "
  msg $chan $codes(Updated name for6 $1 ( $+ $gettok(%ca,2,32) $+ ) to $2)
}

alias removeplayer {
  ;removeplayer auth/id
  return
  if ($getteam($1)) {
    var %team = $v1, %teamfile = [ [ %team ] $+ team.txt ]
    if (STEAM_* iswm $1) {
      var %id = $1
      write -ds $+ %id %dir $+ teams.txt
      write -ds $+ %id %dir $+ %teamfile
      if ($read(%dir $+ registered.txt,w,* $+ $chr(32) $+ %id $+ $chr(32) $+ *)) {
        write -dl $+ $readn %dir $+ registered.txt
      }
      elseif ($read(%dir $+ auth.txt,w,* $+ $chr(32) $+ %id $+ $chr(32) $+ *)) {
        write -dl $+ $readn %dir $+ auth.txt
      }
      msg $chan $codes($1 has been removed from the $clr(%team) $+ %team $+ $clr team)
    }
    elseif ($auth($1)) {
      var %auth = $v1
      echo @debug removing player with auth %auth teamfile %teamfile id %id
      var %id = $getsteamid(%auth)
      if ($read(%dir $+ auth.txt,w,%auth $+ $chr(32) $+ *)) {
        write -dl $+ $readn %dir $+ auth.txt
      }
      write -ds $+ %id %dir $+ %teamfile
      write -ds $+ %id %dir $+ teams.txt
      msg $chan $codes($1 has been removed from the $clr(%team) $+ %team $+ $clr team)
    }
    elseif ($read(%dir $+ auth.txt,w,$1 $+ *)) {
      var %readn = $readn
      var %auth = $1, %id = $gettok($1,2,32)
      write -dl $+ %readn %dir $+ auth.txt
      write -ds $+ %id %dir $+ %teamfile
      write -ds $+ %id %dir $+ teams.txt
      msg $chan $codes($1 has been removed from the $clr(%team) $+ %team $+ $clr team)
    }
    elseif ($read(%dir $+ registered.txt,w,* $+ $1 $+ *)) {
      var %id = $gettok($v1,2,32), %readn = $readn
      write -ds $+ %id %dir $+ teams.txt
      write -dl $+ $readn
      write -ds $+ %id %dir $+ %teamfile
      msg $chan $codes($1 has been removed from the $clr(%team) $+ %team $+ $clr team)
    }
    else {
      notice $nick $codes($1 is on a team but something fucked is happening and the data can't be found)
    }
  }
  else {
    notice $nick $codes($1 is not on a team)
  }
}

alias wipeAllData {
  ;msg $chan $codes(Wiping all data)
  var %y = $findfile(%dir,*,0,0), %x = 1
  while (%x <= %y) {
    var %file = $findfile(%dir,*,%x,0)
    ;msg $chan $codes(Wiping %file)
    write -c %file
    inc %x
  }
  var %y = $finddir(%dir,*,0,0), %x = 1
  while (%x <= %y) {
    var %folder = $finddir(%dir,*,%x,0)
    ;msg $chan $codes(Removing folder %folder)
    var %z = 1, %w = $findfile(%folder,*,0)
    while (%z <= %w) {
      var %fileinfolder = $findfile(%folder,*,%z)
      ;echo @debug removing %fileinfolder
      if (%fileinfolder) {
        .remove %fileinfolder
      }
      inc %z
    }
    inc %x
  }
}

alias runBackup {
  if ($chan) { msg $chan $codes(Backing up all data) }
  write -c %backupdir $+ cwbackup.bat 
  write %backupdir $+ cwbackup.bat "C:\Program Files\WinZip\wzzip.exe" -rP -x*.log $+(%backupdir,cw_stats-,$time(ddd-dd-mm-yyyy-hhnn),.zip) %dir
  write %backupdir $+ cwbackup.bat "C:\Program Files\WinZip\wzzip.exe" -rP -x*.log $+(%backupdir,cw_bot-,$time(ddd-dd-mm-yyyy-hhnn),.zip) $mircdir
  .run %backupdir $+ cwbackup.bat
  set %last.backup $+(%backupdir,cw_stats-,$time(ddd-dd-mm-yyyy-hhnn),.zip)
  if ($chan) { msg $chan $codes(All data backed up to %last.backup) }
}

alias restoreFromBackup {
  if (!$1) { return }
  var %backupfile = $1
  if ($exists($+(%backupdir,%backupfile))) {
    .run "C:\Program Files\WinZip\wzunzip.exe" -o -d %backupdir $+ %backupfile C:\
    ;"C:\Program Files\WinZip\wzunzip.exe" -v  C:\TF2backup\cw_stats-Tue-08-2011-0124.zip >C:\TF2backup\cwrestorefiles.txt 
    msg $chan $codes(All data in %backupfile has been restored)
  }
  else {
    msg $chan $codes(No backup named $1 exists)
  }
}

alias createcache {
  hfree -s steamids
  hfree -s teams
  hfree -s names

  hmake -s steamids 100
  hmake -s teams 100
  hmake -s names 100

  var %x = 1, %y = $lines(%dir $+ teams.txt)
  while (%x <= %y) {
    var %l = $read(%dir $+ teams.txt,%x)
    hadd teams $gettok(%l,1,32) $gettok(%l,2,32)
    inc %x
  }
  var %x = 1, %y = $lines(%dir $+ auth.txt)
  while (%x <= %y) {
    var %l = $read(%dir $+ auth.txt,%x)
    hadd steamids $gettok(%l,1,32) $gettok(%l,2,32)
    hadd steamids $gettok(%l,3,32) $gettok(%l,2,32)
    hadd names $gettok(%l,2,32) $gettok(%l,3,32)
    hadd names $gettok(%l,1,32) $gettok(%l,3,32)
    hadd teams $gettok(%l,1,32) $hget(teams,$gettok(%l,2,32))
    hadd teams $gettok(%l,3,32) $hget(teams,$gettok(%l,2,32))

    inc %x
  }
  var %x = 1, %y = $lines(%dir $+ registered.txt)
  while (%x <= %y) {
    var %l = $read(%dir $+ registered.txt,%x)
    hadd steamids $gettok(%l,3,32) $gettok(%l,2,32)
    hadd names $gettok(%l,2,32) $gettok(%l,3,32)
    hadd teams $gettok(%l,3,32) $hget(teams,$gettok(%l,2,32))

    inc %x
  }
}

alias updateauth {
  if (!$1) || (!$2) { return }
  var %nick = $1, %id = $2, %auth = $auth($1), %rl
  if (!%auth) { echo -a $1 doesn't have an auth u noob | return }
  if ($read(%dir $+ registered.txt,w,* $+ $chr(32) $+ %id $+ $chr(32) $+ *)) {
    var %rl = $readn
  }
  else {
    return
  }
  ;change2auth %auth %id $nick %rl
  change2auth %auth %id %nick %rl
}

alias globalauthcheck {
  var %x = 1, %y = $nick(%cw.main,0), %nickcount = 0, %nicks
  while (%x <= %y) {
    var %nick = $nick(%cw.main,%x)
    var %auth = $auth(%nick)
    var %ident = $ident(%nick)
    if (%auth) {
      if (!$read(%dir $+ auth.txt,w,%auth $+ $chr(32) $+ *)) {
        var %id = $getsteamid(%nick)
        if ($read(%dir $+ registered.txt,w,* $+ $chr(32) $+ %id $+ $chr(32) $+ *)) {
          echo @debug %auth with id %id exists in reg table. Updating to auth
          var %rl = $readn
          change2auth %auth %id %nick %rl
          inc %nickcount
          %nicks = $addtok(%nicks,%nick,32)
        }
        elseif (%ident) && ($read(%dir $+ registered.txt,w,%ident $+ $chr(32) $+ *)) {
          var %l = $v1, %rl = $readn
          var %id = $gettok(%l,2,32)
          change2auth %auth %id %nick %rl
          inc %nickcount
          %nicks = $addtok(%nicks,%nick,32)
        }
      }
    }
    inc %x
  }
  echo @debug globalauthcheck complete! Num nicks converted: %nickcount Nicks converted: %nicks
}
