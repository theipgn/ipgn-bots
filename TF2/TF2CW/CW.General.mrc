on *:CONNECT:{
  .timeropenclosetime 0 60 openCloseTimeCheck
  .timerbackupinstant 6:00 1 1 runBackup
  .timerbackup 6:00 0 86400 runBackup
  .timerrecacheinsant 6:30 1 1 createcache
  .timerrecache 6:30 0 86400 createcache
  if (!%cw.pugtime) { .timerteamcount 0 180 teamcount }
  rcon.logs
}

alias clr {
  if ($1 = Blue) {
    return 12
  }
  elseif ($1 = Red) {
    return 04
  }
  elseif ($1 = Green) {
    return 09
  }
  elseif ($1 = yellow) {
    return 08
  }
  elseif ($1 = darkgreen) {
    return 03
  }
  elseif ($1 = orange) {
    return 07
  }
  elseif ($1 = purple) {
    return 06
  }
  elseif ($1 = cyan) {
    return 11
  }
  elseif ($1 = pink) {
    return 13
  }
  else {
    return 
  }
}

alias codes {
  return 7� $1- 7�
}
alias maincodes {
  return 6� $1- 6�
}
alias redcodes {
  return 4� $1- 4�
}
alias bluecodes {
  return 12� $1- 12�
}
alias pinkcodes {
  return 13� $1- 13�
}
alias teamcodes {
  if ($1 = blue) {
    return $bluecodes($2-)
  }
  elseif ($1 = red) {
    return $redcodes($2-)
  }
  else {
    echo @debug INVALID COLOUR SPECIFIED TO teamcodes
    return ERROR: INVALID COLOUR
  }
}

alias main {
  msg %cw.main $maincodes($1-)
}
alias red {
  msg %cw.red $redcodes($1-)
}
alias blue {
  msg %cw.blue $bluecodes($1-)
}
alias pub {
  var %x = 1
  while (%x < $numtok(%pub.chans,32)) {
    msg $gettok(%pub.chans,%x,32) $codes($1-)
    inc %x
  }
}
alias cwchan {
  main $1-
  red $1-
  blue $1-
}
alias msgall {
  main $1-
  red $1-
  blue $1-
  pub $1-
}

alias cwnotice {
  notice %cw.main $maincodes($1-)
  notice %cw.blue $bluecodes($1-)
  notice %cw.red $redcodes($1-)
}

alias newtopic {
  if ($1) {
    set %status $1-
  }
  .topic %cw.main civil war 6:: http://7ipgn.com.au 6:: http://7ozfortress.com 6:: News: %news 6:: Status: %status
  .topic %cw.blue civil war 12:: http://7ipgn.com.au 12:: http://7ozfortress.com 12:: Status: %status 12:: Mumble: %mumble.blue
  .topic %cw.red civil war 4:: http://7ipgn.com.au 4:: http://7ozfortress.com 4:: Status: %status 6:: Mumble: %mumble.red
}


alias ident {
  if (static isin $address($1,2)) {
    return $+(*!,$gettok($gettok($address($1,3),1,64),2,$asc(!)),@,$gettok($v2,2,64))
  }
  elseif (mibbit isin $address($1,3)) || (chatzilla isin $address($1,3)) {
    return $+($1,!*@,$gettok($v2,2,64))
  }
  else {
    return $address($1,3)
  }
}

alias escapename {
  var %name = $1
  ;//echo -a $asc(`) $asc(|) $asc(') $asc(") 44 123 125
  var %sql.escape.chars = 39 34
  var %x = 1
  while (%x <= $numtok(%sql.escape.chars,32)) {
    var %char = $chr($gettok(%sql.escape.chars,%x,32))
    var %name = $replace(%name,%char,\ $+ %char)
    inc %x
  }
  return %name
}

alias autonotify {
  if (!%cw.full) && (%pug) && (!%cw.detailed) {
    if (!$timer(autonotify)) {
      .timerautonotify 0 300 autonotify
    }
    else {
      msgall 6 $+ %cw.main requires more players! $slotsremaining(red) $slotsremaining(blue)
    }
  }
  else {
    if ($timer(autonotify)) {
      .timerautonotify off
    }
  }
}

alias teaminvite {
  ;teaminvite steamid nick
  if ($read(%dir $+ teams.txt,w,$1 $+ *)) {
    var %td = $v1, %tl = $readn
    var %team = $gettok(%td,2,32)
    echo @debug $nick with auth is in %team team
    if (%team = blue) && ($nick !ison %cw.blue) { invite $2 %cw.blue }
    if (%team = red) && ($nick !ison %cw.red) { invite $2 %cw.red }
    return $true
  }
  else {
    return $false
  }
}

alias pugscore {
  if ($1) && ($read(%pugdir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    ;echo -s $calc($hget(medtable,$1) * 1.32 / (($11 / $8) ^ 2))
    ;echo -s $calc($6 / $4)
    ;echo -s $calc($3 / $8)
    ;echo -s $calc($6 / $8)
    return $calc(((200 * ($11 / $8) ^ 1.9) * (($3 + $6) / $4) + ($6 / $8))^1.4  - ($4 / $8))
  }
  else {
    return $rand(400,1000)
  }
}

alias add2stat {
  ;add2stat id nick
  var %keys = id;name;kill;death;cap;assist;points;games;healing;uberlost;win;loss;draw;damage;ammopack_small;ammopack_medium;tf_ammo_pack;medkit_small;medkit_medium;healing_received
  write -w* $+ $1 $+ $chr(37) $+ * %dir $+ statsall.txt $+($1,$chr(37),$2,$str($chr(37) $+ 0,$calc($numtok(%keys,59) - 2)))
  upload stats %dir $+ statsall.txt
}

alias assignteam {
  var %id = $1, %team = $2, %teamfile = [ [ %team ] $+ team.txt ], %pscore = $3
  echo @teams ID: %id has been assigned to the %team team
  write %dir $+ teams.txt %id %team $date(dd-mm-yyyy)
  write %dir $+ %teamfile %id
  upload teams %dir $+ teams.txt
  echo @debug writeini %dir $+ scores.ini %team pscore $calc($readini(%dir $+ scores.ini,%team,pscore) + %pscore)
  writeini %dir $+ scores.ini %team pscore $calc($readini(%dir $+ scores.ini,%team,pscore) + %pscore)
  writeini %dir $+ scores.ini %team numplayers $calc($readini(%dir $+ scores.ini,%team,numplayers) + 1)
}

alias chooseteam {
  var %id = $1
  var %pscore = $pugscore($1)
  var %rr = return red, %rb = return blue

  var %bluescore = $readini(%dir $+ scores.ini,blue,score)
  var %bluepscore = $readini(%dir $+ scores.ini,blue,pscore)
  var %blueplayers = $readini(%dir $+ scores.ini,blue,numplayers)
  var %redscore = $readini(%dir $+ scores.ini,red,score)
  var %redpscore = $readini(%dir $+ scores.ini,red,pscore)
  var %redplayers = $readini(%dir $+ scores.ini,red,numplayers)

  echo @teams New player to assign (ID: %id $+ ) Pug player score: %pscore Blue Cap Score: %bluescore Blue Player Score: %bluepscore Red Cap Score: %redscore Red Player Score: %redpscore

  var %scorediff = $abs($calc(%bluescore - %redscore))
  var %pscorediff = $abs($calc(%bluepscore - %redpscore))
  var %pdiff = $abs($calc(%blueplayers - %redplayers))

  echo @teams Score diff: %scorediff Player Score Diff: %pscorediff Player Diff: %pdiff
  if ((%cwblue.undermanned) || (%cwred.undermanned)) {
    if (%cwblue.undermanned) {
      assignteam %id blue %pscore
      set %cwblue.undermanned 0
      %rb
    }
    else {
      assignteam %id red %pscore
      set %cwred.undermanned 0
      %rr
    }
  }
  elseif (%scorediff >= 6) && (%pdiff < 6) {
    echo @teams Score diff is >= 6 ( $+ %scorediff $+ ). Blue: %bluescore Red: %redscore
    if (%bluescore > %redscore) {
      ;assign to red team
      assignteam %id red %pscore
      %rr
    }
    else {
      ;assign to blue team
      assignteam %id blue %pscore
      %rb
    }
  }
  elseif (%pscorediff > 500) && ($lines(%dir $+ teams.txt) > 16) && (%pdiff < 4) {
    echo @teams P score diff is > 500 ( $+ %pscorediff $+ ). Blue: %bluepscore Red: %redpscore
    if (%bluepscore > %redpscore) {
      if (%bluescore < %redscore) {
        echo @teams assign to blue (because red still has a higher score... even with a lower player score)
        assignteam %id blue %pscore
        %rb
      }
      else {
        echo @teams assign to red team (blue has higher player score and higher cap score)
        assignteam %id red %pscore
        %rr
      }
    }
    else {
      if (%redscore < %bluescore) {
        echo @teams assign to red (because blue has higher cap score even with lower pscore)
        assignteam %id red %pscore
        %rr
      }
      else {
        echo @teams assign to blue team
        assignteam %id blue %pscore
        %rb
      }
    }
  }
  elseif (%pdiff) {
    echo @teams There is a difference in number of players per team. Assigning based on this
    if (%blueplayers > %redplayers) {
      ;assign to red (less players, and no other algorithm matches for assigning)
      assignteam %id red %pscore
      %rr
    }
    else {
      ;assign to blue (see above)
      assignteam %id blue %pscore
      %rb
    }
  }
  else {
    echo @teams Nothing warrants putting %id on a certain team. Randoming it up
    var %team = $iif($rand(1,2) = 1,blue,red)
    assignteam %id %team %pscore
    return %team
  }
}

alias change2auth {
  ;change2auth auth id name readn
  var %auth = $1, %id = $2, %name = $3, %rl = $4
  echo @debug %name with auth %auth has steamid %steamid registered under host $ident(%name) - Converting to auth-based
  var %team = $gettok($read(%dir $+ teams.txt,w,%id $+ $chr(32) $+ *),2,32)
  var %teamchan = %cw. [ $+ [ %team ] ]
  write %dir $+ auth.txt %auth %id %name
  write -dl $+ %rl %dir $+ registered.txt
  msg ChanServ@Services.GameSurge.net adduser %teamchan * $+ %auth 1

  hadd -m steamids %auth %id
  hadd -m steamids %name %id
  hadd -m teams %id %team
  hadd -m teams %auth %team
  hadd -m teams %name %team
  hadd -m names %id %name
  hadd -m names %auth %name

  msg %name $codes(Your SteamID has been converted to GameSurge login based authentication rather than host based authentication)
  msg %name $codes(Make sure to have your GameSurge login setup in your perform!)
  msg %name $codes(Type !invite in %cw.main to be invited to your team's channel and then !uset autoinvite 1 once you're in it)
}

alias addplayer {
  ;addplayer id nick
  var %id = $1, %nick = $2, %auth = $auth($2)
  if (%auth) {
    if ($read(%dir $+ auth.txt,w,%auth $+ *)) {
      var %steamid = $gettok($v1,2,32)
      var %team = $gettok($read(%dir $+ teams.txt,w,%steamid $+ *),2,32)
      notice %nick $codes(You are already registered to SteamID $clr(%team) $+ %steamid $+ . Speak to an admin if you need to change this)
      return
    }
    if ($read(%dir $+ registered.txt,w,* $+ $chr(32) $+ %id $+ $chr(32) $+ *)) {
      var %name = $gettok($v1,3,32), %host = $gettok($v1,1,32)
      if ($ident(%nick) = $gettok($v1,1,32)) {
        change2auth %auth %id %nick $readn
      }
      else {
        notice %nick $codes(This ID has already been registered by $clr(orange) $+ %name $+ . If this is your ID, and your host has changed (dynamic IP), please use $&
          /msg $me !signup %id <your assigned password>)
        msg %nick $codes(Please respond with !signup %id <your assigned password> (only if %id is actually your SteamID))
        msg %nick $codes(In order to avoid this issue in future $+ $chr(44) please make a GameSurge account (http://gamesurge.net/createaccount) and register your SteamID (using !signup <steamid>) once you have logged in.)
        msg %nick $codes(Make sure you add your login string to your perform (if supported by your client) so the bot can recognise you after you leave and rejoin IRC!)
      }
      return
    }
    if ($chooseteam(%id)) {
      var %team = $v1, %teamchan = %cw. [ $+ [ %team ] ]
      echo @debug %nick was assigned team %team
      echo @teams %nick was assigned to %team

      notice %nick $codes(You have been assigned to the $clr(%team) $+ %team team)
      msg %nick $codes(You can now join the $clr(%team) $+ %team channel. Type /join $clr(%team) $+ %teamchan $+ $clr))
      msg %nick $codes(In order to be automatically invited to the channel upon connecting to IRC $+ $chr(44) please type !uset autoinvite 1 once you are in the channel)

      if (%team = blue) { blue %nick has been assigned to this team }
      if (%team = red) { red %nick has been assigned to this team }
      main %nick has been assigned to the $clr(%team) $+ %team team
      invite %nick %teamchan

      msg ChanServ@Services.GameSurge.net adduser %teamchan %nick 1

      hadd -m steamids %auth %id
      hadd -m steamids %nick %id
      hadd -m teams %id %team
      hadd -m teams %nick %team
      hadd -m teams %auth %team
      hadd -m names %auth %name
      hadd -m names %id %name

      write %dir $+ auth.txt %auth %id %nick
      add2stat %id %nick

      var %banteam = $iif(%team = red,%rcon.blueteam,%rcon.redteam)
      rcon cw_banteam %banteam " $+ %id $+ "; cw_rename $escapename(%nick) " $+ %id $+ "
      ozfortressApi
    }
    else {
      echo @debug %nick was not assigned a team. da fuck?
    }
  }
  else {
    if ($read(%dir $+ auth.txt,w,* $+ $chr(32) $+ %id $+ $chr(32) $+ *)) {
      var %name = $gettok($v1,3,32)
      notice %nick $codes(This ID has already been registered by $clr(orange) $+ %name $+ . If this is your SteamID $+ $chr(44) please login to your GameSurge account and use !invite to join your team channel)
      return
    }
    if ($read(%dir $+ registered.txt,w,* $+ $chr(32) $+ %id $+ $chr(32) $+ *)) {
      var %name = $gettok($v1,3,32), %host = $gettok($v1,1,32)
      notice %nick $codes(This SteamID has already been registered by $clr(orange) $+ %name $+ . If this is your SteamID and your host has changed (dynamic IP) $+ $chr(44) please use $&
        /msg $me !signup %id <your assigned password>)
      msg %nick $codes(Please respond with !signup %id <your assigned password> (only if %id is actually your SteamID))
      msg %nick $codes(In order to avoid this issue in future $+ $chr(44) please make a GameSurge account (http://gamesurge.net/createaccount) and register your SteamID (using !signup <steamid>) once you have logged in.)
      msg %nick $codes(Make sure you add your login string to your perform (if supported by your client) so the bot can recognise you after you leave and rejoin IRC!)
    }
    elseif ($ident(%nick)) && ($read(%dir $+ registered.txt,w,$ident(%nick) $+ $chr(32) $+ *)) {
      var %name = $gettok($v1,3,32), %steamid = $gettok($v1,2,32)
      var %team = $gettok($read(%dir $+ teams.txt,w,%steamid $+ *),2,32)
      notice %nick $codes(You are already registered to SteamID $clr(%team) $+ %steamid $+ . Speak to an admin if you need to change this)
    }
    else {
      var %host = $ident(%nick)
      if ($chooseteam(%id)) {
        var %team = $v1, %teamchan = %cw. [ $+ [ %team ] ]
        echo @debug %nick was assigned team %team
        echo @teams %nick was assigned to %team

        notice %nick $codes(You have been assigned to the $clr(%team) $+ %team team)

        if (%team = blue) { blue %nick has been assigned to this team }
        if (%team = red) { red %nick has been assigned to this team }
        main %nick has been assigned to the $clr(%team) $+ %team team
        invite %nick %teamchan

        hadd -m steamids %nick %id
        hadd -m teams %id %team
        hadd -m teams %nick %team
        hadd -m names %id %name

        var %pass = $lower($read(pass.txt))
        write %dir $+ registered.txt %host %id %nick %pass
        add2stat %id %nick

        var %banteam = $iif(%team = red,%rcon.blueteam,%rcon.redteam)
        rcon cw_banteam %banteam " $+ %id $+ "; cw_rename $escapename(%nick) " $+ %id $+ "

        msg %nick $codes(Since you do not have a GameSurge account (http://gamesurge.net/createaccount) $+ $chr(44) your SteamID has been tied to your host. IF YOUR HOST CHANGES $+ $chr(44) YOU WILL NEED TO RE-SIGNUP)
        msg %nick $codes(In the event that this happens $+ $chr(44) you will need to use the password " $+ %pass $+ " (without quotes). Please keep this safe and do not give it out)
        msg %nick $codes(If you need to re-signup to update your host $+ $chr(44) please use /msg $me !signup %id %pass)
        ozfortressApi
      }
      else {
        echo 4 @debug %nick was not assigned a team. da fuck?
      }
    }
  }
}

alias getteam {
  ;$getteam($nick|steamid)
  if ($hget(teams,$1)) {
    echo @debug $1 has team $v1 from hash table
    return $v1
  }
  if (STEAM_* iswm $1) {
    var %team = $gettok($read(%dir $+ teams.txt,w,$1 $+ $chr(32) $+ *),2,32)
    hadd -m teams $1 %team
    return %team
  }
  elseif ($getsteamid($1)) {
    var %id = $v1, %team
    if ($hget(teams,%id)) {
      %team = $v1
    }
    else {
      %team = $gettok($read(%dir $+ teams.txt,w,%id $+ $chr(32) $+ *),2,32)
    }
    hadd -m teams $1 %team
    return %team
  }
  else {
    if ($1 ison %cw.main) || ($1 ison %cw.blue) || ($1 ison %cw.red) {
      var %id = $getsteamid($1)
      if (!%id) { return $false }
      var %team = $gettok($read(%dir $+ teams.txt,w,%id $+ $chr(32) $+ *),2,32)
      hadd -m teams $1 %team
      return %team
    }
    else {
      return $false
    }
  }
}
alias getsteamid {
  if ($hget(steamids,$1)) {
    echo @debug $1 has id $v1 from hash table
    return $v1
  }
  if ($ident($1)) && ($read(%dir $+ registered.txt,w,$ident($1) $+ $chr(32) $+ *)) {
    var %id = $gettok($v1,2,32)
    hadd -m steamids $1 %id
    echo @debug found $1 in registered file (nick supplied, ident: $ident($1) $+ ) adding $1 with steamid %id to steamids table
    return %id
  }
  if ($read(%dir $+ auth.txt,w,$1 $+ $chr(32) $+ *)) {
    var %id = $gettok($v1,2,32)
    hadd -m steamids $1 %id
    echo @debug found $1 in auth file (auth supplied to getsteamid) adding $1 with steamid %id to steamids table
    return %id
  }
  var %auth = $auth($1)
  if ($read(%dir $+ auth.txt,w,%auth $+ $chr(32) $+ *)) {
    var %id = $gettok($v1,2,32)
    hadd -m steamids $1 %id
    hadd -m steamids %auth %id
    echo @debug found $1 in auth file (nick supplied, nick has auth) adding $1 with steamid %id to steamids table
    return %id
  }
  else {
    echo @debug o shit couldn't get ID for $1
    return $null
  }
}
alias getname {
  if ($hget(names,$1)) {
    return $v1
  }
  if (STEAM_* iswm $1) {
    if ($read(%dir $+ auth.txt,w,* $+ $chr(32) $+ $1 $+ $chr(32) $+ *)) {
      var %name = $gettok($v1,3,32)
      hadd -m names $1 %name
      hadd -m names $gettok($v1,1,32) %name
      return %name
    }
    elseif ($read(%dir $+ registered.txt,w,* $+ $chr(32) $+ $1 $+ $chr(32) $+ *)) {
      var %name = $gettok($v1,3,32)
      hadd -m names $1 %name
      return %name
    }
    else {
      return $sid($1)
    }
  }
  var %auth = $auth($1)
  if (%auth) {
    var %name = $gettok($read(%dir $+ auth.txt,w,%auth $+ *),3,32)
    hadd -m names $1 %name
    hadd -m names %auth %name
    return %name
  }
  elseif ($ident($1)) && ($read(%dir $+ registered.txt,w,$ident($1) $+ *)) {
    var %name = $gettok($v1,3,32)
    hadd -m names $1 %name
    return %name
  }
  else {
    return no_name_found
  }
}

alias playerlist {
  var %x = 1, %playerlist
  var %y = $numtok(%players,32)
  while (%x <= %y) {
    ;echo $iif($istok(%blue.players,$gettok(%players,%x,32),32),blue,red)
    %playerlist = $addtok(%playerlist,$clr($iif($istok(%blue.players,$gettok(%players,%x,32),32),blue,red)) $+ $gettok(%players,%x,32) $+ $clr,32) 
    inc %x
  }
  return %playerlist
}

alias slotsremaining {
  ;$slotsremaining(team)
  var %curplayers = $calc(%pug.limit - $numtok(% [ $+ [ $1 ] $+ .players ],32))
  return $+($chr(40),$clr($1),%curplayers,$clr,/,%pug.limit,$chr(32),$clr($1),$1,$clr,$chr(32),slots remaining,$chr(41))
}

alias timeleft {
  ;will return either timeleft in pug/until timeout, or time left in open night
  if (%cw.pugtime) {
    if ($timer(timeout)) {
      return $clr(orange) $+ $round($calc($timer(timeout).secs / 60),0) $+ $clr minutes until timeout
    }
    if ($timer(timeleft)) {
      return Approximately $clr(orange) $+ $round($calc($timer(timeleft).secs / 60),0) $+ $clr minutes remaining
    }
    if (%cw.detailed) && (!%match.on) {
      return The game has not started yet
    }
    if (%match.on) {
      return The game is currently in 'overtime' (may be ages due to payload)
    }
  }
  else {
    return Placeholder for time until open night ends
    var %closeday = $readini(%dir $+ times.ini,close,day)
    var %closetime = $readini(%dir $+ times.ini,close,time)
    var %openday = $readini(%dir $+ times.ini,open,day)
    var %opentime = $readini(%dir $+ times.ini,open,time)
    var %daydiff = $daydiff(%openday,%closeday)
    var %ctime_base = $ctime($time(dd mmm yyyy %opentime))
    var %ctime_limit = $ctime($calc($time(dd) - %daydiff) $time(mmm yyyy) %closetime)
    echo @debug Parameters passed to ctime_limit: $calc($time(dd) - %daydiff) $time(mmm yyyy) %closetime
    echo @debug ctime_base (opentime base): %ctime_base ctime_limit (closetime limit): %ctime_limit
    var %ctime_diff = $abs($calc(%ctime_limit - %ctime_base))
    echo @debug Difference: %ctime_diff (Duration: $duration(%ctime_diff) $+ )
  }
}

alias daydiff {
  var %days = %openclose.days, %x = 1
  var %openday = $1, %closeday = $2
  var %opendayval = $findtok(%days,%openday,1,32), %closedayval = $findtok(%days,%closeday,1,32)
  return $abs($calc(%opendayval - %closedayval))
}

alias startmapvote {
  ;main $codes(Pug $calc(%gamesplayed + 1) is now full. Players: $playerlist())
  newtopic Pug is full. Map voting now in progress.
  %maps.list = $replace(%maps,$chr(59),$chr(32) $+ - $+ $chr(32))
  cwchan Vote for a map using !map <map>. eg. !map cp_well. Available maps: %maps.list
  set %map.vote 1
  .timermapvote 1 60 endmapvote
  .timertimeout off
}
alias votecount {
  if (!%map. [ $+ [ $1 ] ]) { return (0) }
  if (%map. [ $+ [ $1 ] ] < 0) { 
    unset %map. [ $+ [ $1 ] ]
    return (0) 
  }
  else { return ( $+ %map. [ $+ [ $1 ] ] $+ ) }
}
alias endmapvote {
  .timermapvote off
  unset %map.high %map.win %map.count
  var %x = 1
  %map.count = $numtok(%maps,59)
  while (%x <= %map.count) { 
    if (%map. [ $+ [ $gettok(%maps,%x,59) ] ]) {
      if (!%map.high) { 
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59) 
      }
      if (%map. [ $+ [ $gettok(%maps,%x,59) ] ] = %map.high) && ($gettok(%maps,%x,59) != %map.win) {
        %map.equalhigh = %map. [ $+ [ $gettok(%maps,%x,59) ] ]
        %map.equalwin = $gettok(%maps,%x,59)
      }
      if (%map. [ $+ [ $gettok(%maps,%x,59) ] ] > %map.high) {
        %map.high = %map. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %map.win = $gettok(%maps,%x,59)
        unset %map.equalhigh
        unset %map.equalwin
      }
    }
    inc %x
  }
  if (!%map.win) {
    var %randmap = $gettok(%maps,$rand(1,$numtok(%maps,59)),59)
    set %map.win %randmap
    cwchan No map was voted for. Random map chosen: %randmap
  }
  else { 
    if (%map.equalwin) {
      cwchan Map vote is a tie between %map.equalwin ( $+ %map.equalhigh votes) and %map.win ( $+ %map.high votes)
      var %rand = $rand(1,10)
      if (%rand > 5) {
        cwchan  $+ %map.win won the count with %map.high votes!
      }
      else {
        cwchan  $+ %map.equalwin won the count with %map.equalhigh votes!
        set %map.win %map.equalwin
      }
    }
    else {
      cwchan  $+ %map.win won the count with %map.high votes!
    }
  }
  rcon.logs
  rcon sv_password %serverpass ;changelevel %map.win
  .timer 1 1 newtopic In-game on %map.win
  set %win.map %map.win

  if (!$istok(%maps,%pug.lastmap,59)) && (%pug.lastmap) && (%pug.lastmappos) { set %maps $instok(%maps,%pug.lastmap,%pug.lastmappos,59) }

  set %pug.lastmap %map.win
  set %pug.lastmappos $findtok(%maps,%map.win,1,59)
  set %maps $remtok(%maps,%map.win,59)
  unset %map.*
  .timernoendpug 1 600 return
  .timer 1 1 massdetails
  .timer 1 1 maketeams
}

alias massdetails {
  echo @debug Starting mass details
  var %x = 1, %y = %pug.limit
  set %cw.detailed 1
  while (%x <= %y) {
    var %rednick = $gettok(%red.players,%x,32)
    var %bluenick = $gettok(%blue.players,%x,32)
    if (%rednick isop %cw.main) || (%rednick = %pug.starter) {
      notice %rednick $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass $+ ; alias puglogin "say !login %adminpass $+ " // Admin pass: %adminpass)
    }
    else {
      notice %rednick $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass)
    }
    if (%bluenick isop %cw.main) || (%bluenick isop %pug.starter) {
      notice %bluenick $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass $+ ; alias puglogin "say !login %adminpass $+ " // Admin pass: %adminpass)
    }
    else {
      notice %bluenick $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass)
    }
    notice %bluenick $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
    notice %rednick $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))

    notice %rednick $codes(/run $+(mumble://,%rednick,:,red,@,%mumble.ip,:,%mumble.redport,/?version=1.2.2))
    notice %bluenick $codes(/run $+(mumble://,%bluenick,:,blu,@,%mumble.ip,:,%mumble.blueport,/?version=1.2.2))

    inc %x
  }
  echo @debug End mass details
}

alias details {
  ;details nick
  if ($istok(%players,$1,32)) || ($1 isop %cw.main) {
    if ($1 isop %cw.main) || ($1 = %pug.starter) {
      notice $1 $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass $+ ; alias puglogin "say !login %adminpass $+ " // Admin pass: %adminpass)
    }
    else {
      notice $1 $codes(Server details: connect $+(%rcon.ip,:,%rcon.port,;) password %serverpass)
    }
    if ($getteam($1) = red) {
      notice $1 $codes(/run $+(mumble://,$1,:,red,@,%mumble.ip,:,%mumble.redport,/?version=1.2.2))
    }
    else {
      notice $1 $codes(/run $+(mumble://,$1,:,blu,@,%mumble.ip,:,%mumble.blueport,/?version=1.2.2))
    }
    notice $1 $codes(For mIRC users: /run $+(steam://connect/,%rcon.ip,:,%rcon.port,/,%serverpass))
    notice $1 $codes($timeleft())
  }
}

alias spamstats {
  unset %match.on
  .copy -o stats.txt stats_last.txt
  filter -cfftue 3 37 stats.txt stats.txt
  .copy -o stats.txt %dir $+ games\ $+ $+(stats.,%gamesplayed,.txt)
  if (!$read(stats.txt)) { endpug | return }
  if (%endpug) {
    return
  }
  ;ssh restart
  set %endpug 1
  mode %cw.main +m
  ;nextpugreminder
  var %y = $lines(stats.txt)
  var %x = 1, %highdmg.red = 0, %highdmg.redvalue = 0, %highdmg.blue = 0, %highdmg.bluevalue = 0
  var %highmedpack = 0, %highmedpack.value = 0, %highmedpack.small = 0, %highmedpack.medium = 0, %mkgc = pink
  var %highammopack = 0, %highammopack.value = 0, %highammopack.small = 0, %highammopack.medium = 0, %highammopack.large = 0, %apgc = pink
  while (%x <= %y) {
    var %gamestat = $read(stats.txt,%x)
    tokenize 37 %gamestat
    if ($0 > 3) {
      ;id;name;kill;death;cap;assist;points;games;healing;uberlost;win;loss;draw;damage;ammopack_small;ammopack_medium;tf_ammo_pack;medkit_small;medkit_medium;healing_received

      var %ap = $calc($16 + $17 + $18)
      if (%ap > %highammopack.value) {
        %highammopack.value = %ap
        %highammopack = $2
        %highammopack.small = $15
        %highammopack.medium = $16
        %highammopack.large = $17
        ;echo -s AMMO PACK VALUES: %highammopack.small (S) %highammopack.medium (M) %highammopack.large (L)
        %apgc = $iif($istok(%blue.ids,$1,32),blue,red)
      }
      var %mp = $calc($19 + $20)
      if (%mp > %highmedpack.value) {
        %highmedpack.value = %mp
        %highmedpack = $2
        %highmedpack.small = $18
        %highmedpack.medium = $19
        ;echo -s MED PACK VALUES: %highmedpack.small (S) %highmedpack.medium (M)
        %mkgc = $iif($istok(%blue.ids,$1,32),blue,red)
      }
      if ($istok(%blue.ids,$1,32)) { 
        chan 12 $+ $2 - K:12 $3 D:12 $4 A:12 $6 Dmg:12 $14 APs:12 %ap MPs:12 %mp Heal Rcvd:12 $20 Points:12 $7
        blue 12 $+ $2 - K:12 $3 D:12 $4 A:12 $6 Dmg:12 $14 APs:12 %ap MPs:12 %mp Heal Rcvd:12 $20 Points:12 $7
        if ($14 > %highdmg.bluevalue) {
          %highdmg.blue = $2
          %highdmg.bluevalue = $14
        }
      }
      elseif ($istok(%red.ids,$1,32)) { 
        chan 04 $+ $2 - K:04 $3 D:04 $4 A:04 $6 Dmg:4 $14 APs:4 %ap MPs:4 %mp Heal Rcvd:4 $20 Points:04 $7
        red 04 $+ $2 - K:04 $3 D:04 $4 A:04 $6 Dmg:4 $14 APs:4 %ap MPs:4 %mp Heal Rcvd:4 $20 Points:04 $7
        if ($14 > %highdmg.redvalue) {
          %highdmg.red = $2
          %highdmg.redvalue = $14
        }
      }
      else { 
        chan 13 $+ $2 - K:13 $3 D:13 $4 A:13 $6 Dmg:13 $14 APs:13 %ap MPs:13 %mp Heal Rcvd:13 $20 Points:13 $7 
      }

      if ($read(%dir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)) { 
        var %sl = $v1
        var %b = 2, %a = $numtok(%sl,37)
        var %ln = $readn
        while (%b <= %a) {
          if (%b = 2) {
            ;write -l $+ %ln %dir $+ statsall.txt $puttok($read(%dir $+ statsall.txt,%ln),$gettok($read(stats.txt,%x),%b,37),2,37)
            %sl = $puttok(%sl,$2,2,37)
          } 
          else {
            ;write -l $+ %ln %dir $+ statsall.txt $puttok($read(%dir $+ statsall.txt,%ln),$calc($gettok($read(%dir $+ statsall.txt,%ln),%b,37) + $gettok($read(stats.txt,%x),%b,37)),%b,37)
            %sl = $puttok(%sl,$calc($gettok(%sl,%b,37) + $gettok(%gamestat,%b,37)),%b,37)
          }
          inc %b
        }
        write -l $+ %ln %dir $+ statsall.txt %sl
      }
      else { 
        write %dir $+ statsall.txt %gamestat
      }
    }
    inc %x
  }
  upload stats %dir $+ statsall.txt
  ;.timer 1 8 upload chat %dir $+ chat\ $+ %gamesplayed $+ .txt
  ;.timer 1 1 weaponstats
  dll mThreads.dll thread -ca weaponthread weaponstats

  ;chan 4Red Medic Stats - $getname(%prevredmed) $+ : Ubers Used:4 %uber.red Ubers Lost:4 %medic.Red.uberlost Total Healing:4 %medic.Red.healing 
  ;chan 12Blue Medic Stats - $getnamebyid(%prevbluemed) $+ : Ubers Used:12 %uber.blue Ubers Lost:12 %medic.Blue.uberlost Total Healing:12 %medic.Blue.healing

  if ($istok(%blue.ids,%highestkillstreak,32)) { chan 7Highest Kill Streak:12 $getname(%highestkillstreak) with12 %highestkillstreakvalue kills }
  elseif ($istok(%red.ids,%highestkillstreak,32)) { chan 7Highest Kill Streak:4 $getname(%highestkillstreak) with4 %highestkillstreakvalue kills }
  else { chan 7Highest Kill Streak:9 $getname(%highestkillstreak) with9 %highestkillstreakvalue kills }

  chan $clr(orange) $+ Highest Damage per Team:4 %highdmg.red (04 $+ %highdmg.redvalue $+ ) 7/12 %highdmg.blue (12 $+ %highdmg.bluevalue $+ )

  %mkgc = $iif(%mkgc,$ifmatch,pink)
  chan $clr(orange) $+ Medkit Guzzler: $+ $clr(%mkgc) %highmedpack $+  $+($chr(40),$clr(%mkgc),%highmedpack.small,S,$chr(32),$clr(%mkgc),%highmedpack.medium,M,$chr(32),Total:,$chr(32),$clr(%mkgc),%highmedpack.value,,$chr(41))
  %apgc = $iif(%apgc,$ifmatch,pink)
  chan $clr(orange) $+ Ammo Pack Guzzler: $+ $clr(%apgc) %highammopack $+  $+($chr(40),$clr(%apgc),%highammopack.small,S,$chr(32),$clr(%apgc),%highammopack.medium,M,$chr(32),$clr(%apgc),%highammopack.large,L,$chr(32),Total:,$chr(32),$clr(%apgc),%highammopack.value,,$chr(41))

  chan $clr(Orange) $+ Game Number: $+ $clr %gamesplayed
  var %ms = $read(%dir $+ maps.txt,w,%win.map $+ *), %ng = $calc($ini(%dir $+ gamescores.ini,0) - 1)
  chan $clr(orange) $+ %win.map has been played7 $gettok(%ms,2,37) times in7 %ng games ( $+ $clr(orange) $+ $round($calc(($gettok(%ms,2,37) / %ng) * 100),2) $+ % $+ )
  write -c end.txt
  write -c stats.txt
  write -c newhighscores.txt
  ;uploaddemo %gamesplayed
  ;cwnotice $codes(The next pug is about to open! Get your joins ready)
  endpug
}

alias weaponstats {
  echo @weapons DOING WEAPON ADDING SHIT NOW!
  var %y = $lines(weapons.txt)
  var %x = 1
  echo @weapons $clr(green) $+ Adding weapons.txt to %dir $+ weaponsall.txt
  while (%y >= %x) {
    var %weapon_line = $read(weapons.txt,%x), %id = $gettok(%weapon_line,1,37)
    echo @weapons ID: $gettok(%weapon_line,1,37)
    if ($numtok(%weapon_line,37) > 2) {
      if ($read(%dir $+ weaponsall.txt,w,%id $+ $chr(37) $+ *)) {
        var %numweap = $numtok($v1,37)
        var %currentweap = 2, %wl = $v1
        var %ln = $readn
        while (%numweap >= %currentweap) {
          write -l $+ %ln %dir $+ weaponsall.txt $puttok(%wl,$calc($gettok(%wl,%currentweap,37) + $gettok(%weapon_line,%currentweap,37)),%currentweap,37)
          inc %currentweap
        }
      }
      else {
        write %dir $+ weaponsall.txt %weapon_line
      }
    }
    inc %x
  }
  upload weapon %dir $+ weaponsall.txt
  echo @weapons Weapon Adding Complete!
  .copy -o weapons.txt %dir $+ games\ $+ $+(weapons.,%gamesplayed,.txt)
  write -c weapons.txt
}

alias endpug {
  .timermapvote off
  .timertimeleft off
  .timertimeout off
  .timerautonotify off
  .timerreplacecheckred off
  .timerreplacecheckblue off
  rcon tv_stoprecord; sv_password gtfo
  unset %pug %players %player.* %red.players %blue.players %pug.starter %pug.starterid %team* %pug.admin %cw.detailed %cw.full %map.*
  unset %match.on %busy %get %team1 %team2 %red.ids %blue.ids %gameids %replace.red %replace.blue %roundswap %round
  unset %medics %medic.* %uber.red %uber.blue %score.* %killstreak.* %player.* %class.* %left

  ;set %cwblue.undermanned 0
  ;set %cwred.undermanned 0

  var %x = 1, %y = $lines(connected_players.txt)
  while (%y >= %x) {
    rcon kickid $read(connected_players.txt,%x) Pug is over
    inc %x
  }

  hsave steamids %dir $+ steamids.hash
  hsave names %dir $+ names.hash
  hsave teams %dir $+ teams.hash

  write -c connected_players.txt
  write -c joined.txt
  write -c reminders.txt
  .timer 1 5 rcon.logs
  newtopic No pug in progress. Type !pug to start one.
  .timer 1 4 mode %cw.main -m
  .timer 1 4 unset %endpug
  ozfortressApi
  echo @debug PUG ENDED. PREPARE FOR JOIN DEBUG SPAM (LET'S MAKE THIS SHIT FASTER!)
}
alias endpugtimeout {
  cwchan Pug timed out
  endpug
}

alias startpug {
  if (%pug) {
    echo @debug ERROR: PUG IS ALREADY STARTED AND STARTPUG WAS CALLED
    return
  }
  set %pug 1
  set %pug.starter $1
  ;set %pug.limit 2
  %adminpass = $lower($read(pass.txt))
  %serverpass = $lower($read(pass.txt))
  rcon sv_password %serverpass
  newtopic Pug started by $1 $+ . Type !j to play
  cwnotice $+(%pug.limit,v,%pug.limit) pug started by $1 $+ . Type !j to play.
  .timertimeout 1 $calc(20*60) endpugtimeout
  joinpug $1
  autonotify
  echo @debug PUG STARTED
}
alias joinpug {
  ;joinpug $nick id team
  var %id = $2
  if (!%id) { return }
  var %team = $3
  if (%team) {
    if (!%pug) {
      set %pug 1
      set %pug.starter $1
      set %pug.starterid %id
      ;set %pug.limit 2
      %adminpass = $lower($read(pass.txt))
      %serverpass = $lower($read(pass.txt))
      rcon sv_password %serverpass
      newtopic Pug started by $1 $+ . Type !j to play
      cwnotice $+(%pug.limit,v,%pug.limit) pug started by $1 $+ . Type !j to play.
      .timertimeout 1 $calc(20*60) endpugtimeout
      autonotify
      echo @debug PUG STARTED
    }
    if ($numtok(% [ $+ [ %team ] $+ .players ],32) = %pug.limit) {
      notice $1 $codes(The $clr(%team) $+ $upper($left(%team,1)) $+ $mid(%team,2,$len(%team)) $+ $clr team is full)
      return
    }
    %players = $addtok(%players,$1,32)
    % [ $+ [ %team ] $+ .ids ] = $addtok(% [ $+ [ %team ] $+ .ids ],%id,32)
    % [ $+ [ %team ] $+ .players ] = $addtok(% [ $+ [ %team ] $+ .players ],$1,32)
    main $clr(%team) $+ $1 has joined the pug. $slotsremaining(%team)
    %team $clr(%team) $+ $1 has joined the pug. $slotsremaining(%team)
    if (%cw.full) { details $1 }
    if (%replace. [ $+ [ %team ] ]) {
      details $1
      updateteams
      doTeamPlayerNames
      unset %replace. [ $+ [ %team ] ]
    }
    ozfortressApi
  }
  else {
    notice $1 $codes(You can't join a pug without being in a team! Use !signup <steamid>)
  }
  if ($calc($numtok(%red.players,32) + $numtok(%blue.players,32)) = $calc(2 * %pug.limit)) && (!%cw.full) {
    cwchan Pug is now full full! Players: $playerlist()
    set %cw.full 1
    .timertimeout off
    .timer 1 1 startmapvote
    ozfortressApi
  }
}
alias leavepug {
  ;leavepug $nick
  if ($numtok(%players,32) = 1) {
    endpug
    return
  }
  var %id = $getsteamid($1)
  if (!%id) { halt }
  var %team = $getteam(%id)
  if (%team) {
    %players = $remtok(%players,$1,32)
    % [ $+ [ %team ] $+ .ids ] = $remtok(% [ $+ [ %team ] $+ .ids ],%id,32)
    % [ $+ [ %team ] $+ .players ] = $remtok(% [ $+ [ %team ] $+ .players ],$1,32)
    main $clr(%team) $+ $1 has left the pug. $slotsremaining(%team)
    %team $clr(%team) $+ $1 has left the pug. $slotsremaining(%team)
    if (%map. [ $+ [ $1 ] ]) {
      dec %map. [ $+ [ %map. [ $+ [ $1  ] ] ] ]
      unset %map. [ $+ [ $1 ] ]
    }
    if ($1 = %pug.starter) {
      %pug.starter = $gettok(%players,1,32)
      cwchan $clr($getteam(%pug.starter)) $+ %pug.starter is now the in-game admin
    }
    ozfortressApi
  }
}

alias getreplace {
  ;getreplace $nick
  if ($getteam($1)) {
    var %team = $v1
    main Need a $clr(%team) $+ %team $+ $clr replacement for $clr(%team) $+ $1 $+ $clr $+ . Type !details for server details
    %team Replacement needed. Type !details for server details
    set %replace. [ $+ [ %team ] ] 1
    ;.timerreplacecheck $+ %team 0 60 replacecheck %team
  }
}
alias replacecheck {
  ;replacecheck %team
  var %team = $1
  var %teamplayers = $numtok(% [ $+ [ %team ] $+ .players ],32)
  if (%teamplayers < %pug.limit) {
    %team We need another player! Type !j to play
    if (%replace. [ $+ [ %team ] ] > 3) {
      main The $clr(%team) $+ %team $+ $clr needs another player! Type !signup <steamid> to join a team and then !j to play
      pub #cw $clr(%team) $+ %team $+ $clr needs another player! Type !signup <steamid> to join a team and then !j to play
      set %cw [ $+ [ %team ] $+ .undermanned ] 1
      return
    }
    inc %replace. [ $+ [ %team ] ]
  }
  else {
    .timerreplacecheck $+ %team off
  }
}

on *:TEXT:!*:%cw.main,%cw.blue,%cw.red:{
  if (!%cw.compstarted) { return }
  ;//////////////////////////////////////////////////////////////////////////////////////////////
  if ($1 = !signup) {
    if (!$regex($2,^STEAM_0:([0-1]):([0-9]+)$)) { notice $nick $codes(You need to specify a valid SteamID. eg !signup STEAM_0:1:1212121) | return }
    if (mib_* iswm $nick) { notice $nick $codes(You must use an identifiable name before you can register an ID) | return }
    addplayer $2 $nick
    return
  }
  if ($1 = !invite) {
    ;find user's team (if he has registered an id) and invite to respective channel
    var %auth = $auth($nick)
    if (%auth) {
      echo @debug $nick has auth %auth
      if ($read(%dir $+ auth.txt,w,%auth $+ *)) {
        var %ad = $v1
        var %steamid = $gettok(%ad,2,32)
        echo @debug $nick with auth %auth has steamid %steamid
        if (!$teaminvite(%steamid,$nick)) {
          notice $nick $codes(Something has gone amazingly wrong and you are registered but not on a team. WTF? contact an admin asap)
        }
      }
      elseif ($ident($nick)) && ($read(%dir $+ registered.txt,w,$ident($nick) $+ *)) {
        var %l = $v1
        change2auth %auth $gettok(%l,2,32) $nick $readn
        if (!$teaminvite(%steamid,$nick)) {
          notice $nick $codes(Something has gone amazingly wrong and you are registered but not on a team. WTF? contact an admin asap)
        }
      }
      else {
        notice $nick $codes(You have not registered a SteamID. Please use !signup <steamid>)
      }
    }
    elseif ($ident($nick)) && ($read(%dir $+ registered.txt,w,$ident($nick) $+ *)) {
      var %rd = $v1, %rl = $readn
      var %steamid = $gettok(%rd,2,32), %name = $gettok(%rd,3,32)
      echo @debug $nick has steamid %steamid registered under host $gettok(%rd,1,32)
      if (!$teaminvite(%steamid,$nick)) {
        notice $nick $codes(Something has gone amazingly wrong and you are registered but not on a team. WTF? contact an admin asap)
      }
    }
    else {
      msg $nick $codes(You have not registered a SteamID with your current host. If you have previously registered a SteamID you will need to re-register it to assign it to your host.)
      msg $nick $codes(To do so $+ $chr(44) use /msg $me !signup <id> <your assigned password> (where the password is the one that was given when you first signed up))
      msg $nick $codes(In order to avoid this issue $+ $chr(44) please make a GameSurge account (http://gamesurge.net/createaccount) and register your SteamID (using !signup <steamid>) once you have logged in.)
      msg $nick $codes(Make sure you add your login string to your perform (if supported by your client) so the bot can recognise you after you leave and rejoin IRC!)
      msg $nick $codes(If you have NOT registered a SteamID previously $+ $chr(44) please use !signup <your steamid> in the channel)
    }
    return
  }

  ;//////////////////////////////////////////////////////////////////////////
  if ($1 = !join) || ($1 = !j) || ($1 = !add) || ($1 = !pug) {
    if (!%cw.pugtime) {
      if ($getteam($nick)) {
        var %team = $v1
        notice $nick $codes(It's pub night! Jump in the server and kill some people)
        details $nick
      }
      else {
        notice $nick $codes(You need to signup first! Use !signup <steamid>)
      }
      return
    }
    if ($istok(%players,$nick,32)) {
      notice $nick $codes(You're already in the pug)
    }
    else {
      var %id, %table = auth.cache. $+ $cid
      if ($hget(%table,$nick)) {
        %id = $getsteamid($v1)
      }
      else {
        %id = $getsteamid($nick)
      }
      if (!%id) { 
        notice $nick $codes(You need to signup first! Use !signup <steamid>) 
        return 
      }
      var %team = $getteam(%id)
      joinpug $nick %id %team
    }
    return
  }
  if ($1 = !leave) || ($1 = !l) {
    if (!%cw.pugtime) {
      return
    }
    if (!%pug) {
      return
    }
    if ($istok(%players,$nick,32)) {
      leavepug $nick
      if (%cw.full) || (%cw.detailed) {
        set %left $addtok(%left,$nick,32)
        getreplace $nick
      }
    }
    else {
      notice $nick $codes(You're not in the pug)
    }
    return
  }
  if ($1 = !vote) || ($1 = !map) {
    if ($istok(%players,$nick,32)) {
      if (%map.vote) {
        if ($matchtok(%maps,$2,1,59)) {
          var %votedmap = $v1
          if (%map. [ $+ [ $nick ] ] = %votedmap) { halt }
          if (%map. [ $+ [ $nick ] ]) && (%map. [ $+ [ $nick ] ] != %votedmap) {
            inc %map. [ $+ [ %votedmap ] ]
            dec %map. [ $+ [ %map. [ $+ [ $nick ] ] ] ]
            msg $chan $codes($nick changed vote from %map. [ $+ [ $nick ] ] $+  $votecount(%map. [ $+ [ $nick ] ]) to %votedmap $+  $votecount(%votedmap))
            %map. [ $+ [ $nick ] ] = %votedmap
          }
          else {
            inc %map. [ $+ [ %votedmap ] ]
            msg $chan $codes($nick voted for %votedmap $+  $votecount(%votedmap))
            %map. [ $+ [ $nick ] ] = %votedmap
          }
        }
      }
      if (!%map.vote) && (%cw.detailed) {
        notice $nick $codes(Map voting has already finished.)
        halt
      }
      if (!%map.vote) && (%pug) {
        notice $nick $codes(You must wait for map voting to start before you can vote for a map.)
      }
    }
    return
  }
  if ($1 = !endpug) {
    if (!%cw.pugtime) {
      return
    }
    if (!%pug) {
      msg $chan $codes(No pug in progress. Type !pug to start one.)
    }
    elseif (%match.on) && ($nick = %pug.starter || $nick isop $chan) {
      spamstats
    }
    elseif (%pug) && (($nick = %pug.starter) || ($nick isop $chan)) {
      endpug
    }
    return
  }
  ;//////////////////////////////////////////////////////////////////////////////////////
  if ($1 = !status) {
    if (%cw.pugtime) {
      if (!%pug) {
        msg $chan $codes(Status: No pug in progress. Type !pug to start one.)
      }
      elseif (%pug) && (!%cw.full) {
        msg $chan $codes(Waiting for players. Type !j to join. $slotsremaining(red) $slotsremaining(blue) $chr(91) $+ $timeleft() $+ $chr(93))
      }
      elseif (%pug) && (%match.on) && (!$timer(timeleft)) {
        msg $chan $codes(Game is currently in overtime on $clr(orange) $+ %win.map $+ $clr $+ . Scores: $currentscores())
      }
      elseif (%pug) && (%match.on) && ($timer(timeleft)) {
        msg $chan $codes(Game is in progress on $clr(orange) $+ %win.map $+ $clr $+ . $timeleft() $+ . Scores: $currentscores())
      }
      elseif (%pug) && (%cw.full) && (%cw.detailed) {
        msg $chan $codes(Waiting for the game to start on $clr(orange) $+ %win.map $+ $clr)
      }
      else {
        msg $chan $codes(Unsure what other statuses there are :S)
      }
    }
    else {
      msg $chan $codes(It's pub night! Join the server and support your team! $totalscores() $currentplayers() 6$clr(purple) $+ !details for server details)
    }
    return
  }
  if ($1 = !stats) || ($1 = !stat) {
    stat $iif($2,$ifmatch,$nick) $chan
    return
  }
  if ($1 = !compare) {
    comparestat $2 $iif($3,$ifmatch,$nick) $chan
    return
  }
  if ($1 = !scores) || ($1 = !score) {
    if (%pug) {
      msg $chan $codes($iif(%match.on,Scores for current game: $currentscores()) $totalscores())
    }
    else {
      msg $chan $codes($totalscores())
    }
    return
  }
  if ($1 = !details) {
    if (!%cw.pugtime) {
      if ($getteam($nick)) {
        details $nick
      }
      return
    }
    if (%pug) && (%cw.detailed) {
      var %id = $getsteamid($nick)
      if (%id) {
        var %team = $getteam(%id)
        if (%team) {
          if (%replace. [ $+ [ %team ] ]) {
            details $nick
            unset %replace. [ $+ [ %team ] ]
            return
          }
        }
      }
    }
    if ($istok(%players,$nick,32)) || ($nick isop $chan) {
      details $nick

    }
    return
  }
  if ($1 = !players) {
    if (!%cw.pugtime) {
      msg $chan $codes(It's pub night! Join the server and blow some people up $currentplayers $+ . Type 6!details for server details)
      return
    }
    if (%replace.red) {
      msg $chan $codes($playerlist() $slotsremaining(red))
    }
    elseif (%replace.blue) {
      msg $chan $codes($playerlist() $slotsremaining(blue))
    }
    elseif (%pug) {
      msg $chan $codes($playerlist() $iif(!%cw.full,$slotsremaining(red) $slotsremaining(blue) $chr(91) $+ $timeleft() $+ $chr(93),(Full)))
    }
    else {
      msg $chan $codes(No pug in progress. Type !pug to start one)
    }
    return
  }
  if ($1 = !teams) {
    if (%pug) {
      msg $chan $codes(4Red: $iif(%red.players,$ifmatch,None))
      msg $chan $codes(12Blue: $iif(%blue.players,$ifmatch,None))
    }
    elseif (!%cw.pugtime) {
      msg $chan $codes(some code here to show who's on what team in the pub)
    }
    else {
      msg $chan $codes(No game in progress. A full list of players and their teams can be found at http://games.ipgn.com.au/tf2/rvb/teams.php)
    }
    return
  }
  if ($1 = !maps) {
    var %maps.list = $replace(%maps,$chr(59),$chr(32) $+ - $+ $chr(32))
    msg $chan $codes(%maps.list)
  }
  if ($1 = !timeleft) {
    msg $chan $codes($timeleft())
  }
  if ($1 = !help) || ($1 = !info) || ($1 = !cmdlist) || ($1 = !commands) {
    notice $nick $codes(A list of all commands can be found at http://games.ipgn.com.au/tf2/rvb/help.txt For further assistance, please contact an op)
  }
  if ($1 = !clanpage) {
    if ($chan = %cw.red) {
      msg $chan $codes(OZF RED Clan Page: http://ozfortress.com/clan.php?do=viewclan&clanid=844)
    }
    elseif ($chan = %cw.blue) {
      msg $chan $codes(OZF BLU Clan Page: http://ozfortress.com/clan.php?do=viewclan&clanid=845)
    }
    else {
      notice $nick $codes(You must use this command in your team channel!)
    }
  }
}

on *:TEXT:!signup*:?:{
  if (!$regex($2,^STEAM_0:([0-1]):([0-9]+)$)) { notice $nick $codes(You need to specify a valid SteamID. eg !signup STEAM_0:1:1212121) | return }
  if (mib_* iswm $nick) { notice $nick $codes(You must use an identifiable name before you can register an ID) | return }
  var %host = $ident($nick), %id = $2, %upass = $3
  if ($read(%dir $+ registered.txt,w,* $+ $chr(32) $+ %id $+ $chr(32) $+ *)) {
    var %rd = $v1, %rl = $readn
    var %password = $gettok(%rd,4,32)
    if (%upass = %password) {
      var  %auth = $auth($nick)
      echo @debug Checking auth update for $nick : %auth
      if (%auth) {
        ;change2auth %auth %id %nick %rl
        change2auth %auth %id $nick %rl
      }
      else {
        write -l $+ %rl %dir $+ registered.txt %host %id $nick %password
        echo @debug Host $gettok(%rd,1,32) with SteamID %id has been updated to %host
        if ($teaminvite(%id,$nick)) {
          msg $nick $codes(Your SteamID has been updated to your current host and you have been invited to your team channel.)
        }
      }
    }
    elseif (%host) && (%host = $gettok(%rd,1,32)) {
      write -l $+ %rl %dir $+ registered.txt %host %id $nick %password
      if ($teaminvite(%id,$nick)) {
        msg $nick $codes(Your SteamID has been updated to your current host and you have been invited to your team channel.)
      }
    }
    else {
      msg $nick $codes(Invalid password specified for SteamID %id)
    }
  }
  else {
    addplayer $2 $nick
  }
}

alias totalscores {
  var %bluescore = $readini(%dir $+ scores.ini,blue,score)
  var %redscore = $readini(%dir $+ scores.ini,red,score)

  return $+(Total scores: 4Red: %redscore 12Blue: %bluescore)
}
alias currentscores {
  if (%match.on) {
    return 4Red: %score.red 12Blue: %score.blue
  }
}

alias currentplayers {
  var %x = 1, %y = $lines(joined.txt), %red = 0, %blue = 0
  while (%x <= %y) {
    tokenize 32 $read(joined.txt,%x)
    if ($2 = red) {
      inc %red
    }
    else {
      inc %blue
    }
    inc %x
  }
  return $+($chr(40),$clr(red),$calc((%rcon.maxplayers / 2) - %red),$chr(32),red,$clr,$chr(32),slots open,/,$clr(blue),$calc((%rcon.maxplayers / 2) - %blue),$chr(32),blue,$clr,$chr(32),slots open,$chr(41))
}

alias stat {
  ;stat <nick> <chan>
  var %u = $1, %c = $2
  var %id = $getsteamid(%u)
  if (%id) {
    var %team = $getteam(%id)
    if (%team) {
      tokenize 37 $read(%dir $+ statsall.txt,w,%id $+ $chr(37) $+ *)
      var %ap = $calc($15 + $16 + $17)
      var %mp = $calc($18 + $19)
      ;id;name;kill;death;cap;assist;points;games;healing;uberlost;win;loss;draw;damage;ammopack_small;ammopack_medium;tf_ammo_pack;medkit_small;medkit_medium;healing_received
      if (%team = red) {
        msg %c $redcodes(4 $+ $2 - Kills:4 $3 Deaths:4 $4 Assists:4 $6 Captures:4 $5 Damage:4 $14 Points:4 $7 $&
          Healing Done:4 $9 Ubers Lost:4 $10 Healing Received:4 $20 $&
          Played:4 $8 Won:4 $11 Lost:4 $12 Drawn:4 $13 Ammo Packs:4 %ap Medkits:4 %mp)
      }
      else {
        msg %c $bluecodes(12 $+ $2 - Kills:12 $3 Deaths:12 $4 Assists:12 $6 Captures:12 $5 Damage:12 $14 Points:12 $7 $&
          Healing Done:12 $9 Ubers Lost:12 $10 Healing Received:12 $20 $&
          Played:12 $8 Won:12 $11 Lost:12 $12 Drawn:12 $13 Ammo Packs:12 %ap Medkits:12 %mp)
      }
    }
  }
  elseif ($read(%dir $+ statsall.txt,w,* %u $+ *)) {
    var %l = $v1
    var %id = $gettok(%l,1,37)
    var %team = $getteam(%id)
    tokenize 37 %l
    var %ap = $calc($15 + $16 + $17)
    var %mp = $calc($18 + $19)

    if (%team = red) {
      msg %c $redcodes(4 $+ $2 - Kills:4 $3 Deaths:4 $4 Assists:4 $6 Captures:4 $5 Damage:4 $14 Points:4 $7 $&
        Healing Done:4 $9 Ubers Lost:4 $10 Healing Received:4 $20 $&
        Played:4 $8 Won:4 $11 Lost:4 $12 Drawn:4 $13 Ammo Packs:4 %ap Medkits:4 %mp)
    }
    else {
      msg %c $bluecodes(12 $+ $2 - Kills:12 $3 Deaths:12 $4 Assists:12 $6 Captures:12 $5 Damage:12 $14 Points:12 $7 $&
        Healing Done:12 $9 Ubers Lost:12 $10 Healing Received:12 $20 $&
        Played:12 $8 Won:12 $11 Lost:12 $12 Drawn:12 $13 Ammo Packs:12 %ap Medkits:12 %mp)
    }
  }
  else {
    msg %c $codes(No stats could be found for %u)
  }
}

alias statline {
  ;placeholder for future use
}

alias comparestat {
  ;comparestat <nick> <another nick> <chan>
  var %nick = $1, %u = $2, %c = $3
  var %nickid = $getsteamid(%nick), %uid = $getsteamid(%u)
  if (%nickid && %uid) {
    var %nickstat = $read(%dir $+ statsall.txt,w,%nickid $+ $chr(37) $+ *), %ustat = $read(%dir $+ statsall.txt,w,%uid $+ $chr(37) $+ *)
    ;tokenize nickstat and just gettok for all of ustat
    var %kills = $gettok(%ustat,3,37), %deaths = $gettok(%ustat,4,37), %assists = $gettok(%ustat,6,37), %captures = $gettok(%ustat,5,37), %damage = $gettok(%ustat,14,37)
    var %points = $gettok(%ustat,7,37), %healingdone = $gettok(%ustat,9,37), %uberlost = $gettok(%ustat,10,37), %healingrcvd = $gettok(%ustat,20,37), %played = $gettok(%ustat,8,37)
    var %won = $gettok(%ustat,11,37), %lost = $gettok(%ustat,12,37), %drawn = $gettok(%ustat,13,37), %ustatnick = $gettok(%ustat,2,37)
    tokenize 37 %nickstat
    msg %c $maincodes(6 $+ $2 /7 %ustatnick Kills:6 $3 /7 %kills Deaths:6 $4 /7 %deaths Assists:6 $6 /7 %assists Captures:6 $5 /7 %captures $&
      Damage:6 $14 /7 %damage Points:6 $7 /7 %points Healing Done:6 $9 /7 %healingdone Ubers Lost:6 $10 /7 %uberlost Healing Received:6 $20 /7 %healingrcvd $&
      Played:6 $8 /7 %played Won:6 $11 /7 %won Lost:6 $12 /7 %lost Drawn:6 $13 /7 %drawn)
  }
  elseif (%nickid && $read(%dir $+ statsall.txt,w,* $+ %u $+ *)) {
    var %nickstat = $read(%dir $+ statsall.txt,w,%nickid $+ $chr(37) $+ *), %ustat = $v2
    var %kills = $gettok(%ustat,3,37), %deaths = $gettok(%ustat,4,37), %assists = $gettok(%ustat,6,37), %captures = $gettok(%ustat,5,37), %damage = $gettok(%ustat,14,37)
    var %points = $gettok(%ustat,7,37), %healingdone = $gettok(%ustat,9,37), %uberlost = $gettok(%ustat,10,37), %healingrcvd = $gettok(%ustat,20,37), %played = $gettok(%ustat,8,37)
    var %won = $gettok(%ustat,11,37), %lost = $gettok(%ustat,12,37), %drawn = $gettok(%ustat,13,37), %ustatnick = $gettok(%ustat,2,37)
    tokenize 37 %nickstat
    msg %c $maincodes(6 $+ $2 /7 %ustatnick Kills:6 $3 /7 %kills Deaths:6 $4 /7 %deaths Assists:6 $6 /7 %assists Captures:6 $5 /7 %captures $&
      Damage:6 $14 /7 %damage Points:6 $7 /7 %points Healing Done:6 $9 /7 %healingdone Ubers Lost:6 $10 /7 %uberlost Healing Received:6 $20 /7 %healingrcvd $&
      Played:6 $8 /7 %played Won:6 $11 /7 %won Lost:6 $12 /7 %lost Drawn:6 $13 /7 %drawn)
  }
  elseif (%uid && $read(%dir $+ statsall.txt,w,* $+ %nick $+ *)) {
    var %nickstat = $v2, %ustat = $read(%dir $+ statsall.txt,w,%uid $+ $chr(37) $+ *)
    var %kills = $gettok(%ustat,3,37), %deaths = $gettok(%ustat,4,37), %assists = $gettok(%ustat,6,37), %captures = $gettok(%ustat,5,37), %damage = $gettok(%ustat,14,37)
    var %points = $gettok(%ustat,7,37), %healingdone = $gettok(%ustat,9,37), %uberlost = $gettok(%ustat,10,37), %healingrcvd = $gettok(%ustat,20,37), %played = $gettok(%ustat,8,37)
    var %won = $gettok(%ustat,11,37), %lost = $gettok(%ustat,12,37), %drawn = $gettok(%ustat,13,37), %ustatnick = $gettok(%ustat,2,37)
    tokenize 37 %nickstat
    msg %c $maincodes(6 $+ $2 /7 %ustatnick Kills:6 $3 /7 %kills Deaths:6 $4 /7 %deaths Assists:6 $6 /7 %assists Captures:6 $5 /7 %captures $&
      Damage:6 $14 /7 %damage Points:6 $7 /7 %points Healing Done:6 $9 /7 %healingdone Ubers Lost:6 $10 /7 %uberlost Healing Received:6 $20 /7 %healingrcvd $&
      Played:6 $8 /7 %played Won:6 $11 /7 %won Lost:6 $12 /7 %lost Drawn:6 $13 /7 %drawn)
  }
  else {
    msg %c $codes(No stats found for one of the players specified)
  }
}

alias highscore {
  var %statkey = $1
  if ($read(%dir $+ highscores.txt,w,%statkey $+ *)) {
    var %hsl = $v1
    return $gettok(%hsl,3,32)
  }
  else {
    return 0
  }
}

alias newhighscore {
  var %statkey = $1, %id = $2, %statvalue = $3
  write newhighscores.txt %statkey %id %statvalue
  write -w* $+ %statkey $+ * %dir $+ highscores.txt %statkey %id %statvalue
  echo @debug Added new highscore for stat %statkey : %id got %statvalue
}

alias openCloseTimeCheck {
  if (!%cw.pugtime) {
    ;if its not pugtime, it's open time, therefore check for close time
    var %closeday = $readini(%dir $+ times.ini,close,day)
    if ($time(dddd) = %closeday) {
      ;close day, check time
      var %closetime = $readini(%dir $+ times.ini,close,time)
      var %ctime = $ctime
      var %closetimectime = $ctime($time(dd mmm yyyy) %closetime)
      echo @debug Closetime is %closetimectime Current ctime is %ctime
      if (%ctime > %closetimectime) {
        ;it's time to close it to pugs only
        echo @debug Time to close up shop!
        newtopic No pug in progress. Type !pug to start one
        set %cw.pugtime 1
        .timerteamcount off
        rcon exec rvb_pug.cfg
      }
    }
  }
  else {
    if (%cw.forceclose) { return }
    ;pug time, therefore check for open time
    var %openday = $readini(%dir $+ times.ini,open,day)
    if ($time(dddd) = %openday) {
      ;the day is the same as the open day, now calculations for time comparison go!
      var %opentime = $readini(%dir $+ times.ini,open,time)
      var %ctime = $ctime
      var %opentimectime = $ctime($time(dd mmm yyyy) %opentime)
      echo @debug Opentime Ctime is %opentimectime Current ctime is %ctime
      if (%ctime > %opentimectime) {
        ;it's time to open this shit up!
        echo @debug Time to open up and provide
        newtopic Pub night. Type !details for server details
        set %cw.pugtime 0
        .timerteamcount 0 180 teamcount
        rcon exec rvb_pub.cfg
      }
    }
  }
}

on *:NICK:{
  if ($istok(%players,$nick,32)) {
    %players = $reptok(%players,$nick,$newnick,32)
    if ($istok(%red.players,$nick,32)) {
      %red.players = $reptok(%red.players,$nick,$newnick,32)
    }
    if ($istok(%blue.players,$nick,32)) {
      %blue.players = $reptok(%blue.players,$nick,$newnick,32)
    }
    if (%map. [ $+ [ $nick ] ]) {
      set %map. [ $+ [ $newnick ] ] %map. [ $+ [ $nick ] ]
      unset %map. [ $+ [ $nick ] ]
    }
  }
  var %cid = $hget(steamids,$nick)
  if (%cid) {
    if ($read(%dir $+ auth.txt,w,* $+ $chr(32) $+ %cid $+ $chr(32) $+ *)) {
      var %l = $v1, %rl = $readn
      write -l $+ %rl %dir $+ auth.txt $puttok(%l,$newnick,3,32)
    }
    elseif ($read(%dir $+ registered.txt,w,* $+ $chr(32) $+ %cid $+ $chr(32) $+ *)) {
      var %l = $v1, %rl = $readn
      var %new = $puttok(%l,$newnick,3,32)
      %new = $puttok(%new,$ident($newnick),1,32)
      write -l $+ %rl %dir $+ registered.txt %new
    }
    rcon cw_rename $newnick " $+ %cid $+ "
    hadd -m steamids $newnick %cid
    hadd -m teams $newnick $hget(teams,%cid)
    hadd -m names %cid $newnick
    hdel steamids $nick
    hdel teams $nick
  }
}

on *:KICK:%cw.main,%cw.blue,%cw.red:{
  if ($istok(%players,$knick,32)) {
    leavepug $knick
  }
}

on *:PART:%cw.main,%cw.blue,%cw.red:{
  if ($istok(%players,$nick,32)) {
    leavepug $nick
  }
}

on *:QUIT:{
  if ($istok(%players,$nick,32)) && (!%cw.full) {
    leavepug $nick
  }
  var %id = $getsteamid($nick)
  if (%id) {
    if ($hget(steamids,$nick)) {
      hdel steamids $nick
    }
    if ($hget(teams,$nick)) {
      hdel teams $nick
    }
  }
}

on *:JOIN:%cw.main:{
  .timer 1 5 echo @debug CACHING $1 STEAMID $getsteamid($nick) TEAM $getteam($nick)
  ;.timer 1 8 echo @debug CACHING $1 NAME $getname($nick)
  ;echo @debug $nick JOINED CHAN: ID: %cacheid NAME: %cachename
  .timer 1 6 updatenick $1
}

alias updatenick {
  var %auth = $auth($1), %ident = $ident($1)
  if (%auth) {
    var %id = $getsteamid(%auth)
    if (%id) {
      var %l = $read(%dir $+ auth.txt,w,%auth $+ $chr(32) $+ *))
      var %rl = $readn
      var %new = $puttok(%l,$1,3,32)
      rcon cw_rename $1 " $+ %id $+ "
      write -l $+ %rl %dir $+ auth.txt %new
    }
  }
  elseif ($read(%dir $+ registered.txt,w,%ident $+ $chr(32) $+ *)) {
    var %l = $v1, %rl = $readn
    var %new = $puttok(%l,$1,3,32), %id = $gettok(%l,2,32)
    rcon cw_rename $1 " $+ %id $+ "
    write -l $+ %rl %dir $+ registered.txt %new
  }
}

alias regtest {
  noop $regex($1,STEAM_(\d):([0-1]):(\d+))
  return $regml(1) $regml(2) $regml(3)
}
