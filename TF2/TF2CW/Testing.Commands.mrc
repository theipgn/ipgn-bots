alias setupfakedata {
  set %blue.ids 1 2 3 4 5 6 7
  set %blue.players joe1 joe2 joe3 joe4 joe5 joe6 joe7
  set %red.ids 1 2 3 4 5 6 7 8
  set %red.players john1 john2 john3 john4 john5 john6 john7 john8
  set %players %blue.players %red.players
}

alias setupfakestats {
  var %x = 1
  write -c stats.txt
  write -c weapons.txt
  while (%x <= $numtok(%testids,32)) {
    add $gettok(%testids,%x,32) name joe $+ %x
    ;id;name;kill;death;cap;assist;points;games;healing;uberlost;win;loss;draw;damage;ammopack_small;ammopack_medium;tf_ammo_pack;medkit_small;medkit_medium;healing_received
    var %highscore.keys = kill;death;cap;assist;points;healing;uberlost;damage
    var %rand = $rand(1,8)
    echo highscore key: $gettok(%highscore.keys,%rand,59)
    add $gettok(%testids,%x,32) $gettok(%highscore.keys,%rand,59) $rand(10,20)
    addweapon $gettok(%testids,%x,32)
    inc %x
  }
  %blue.ids = $gettok(%testids,1-6,32)
  %red.ids = $gettok(%testids,7-12,32)
}


alias weaponsfordb {
  var %weapons = tf_projectile_rocket liberty_launcher rocketlauncher_directhit quake_rl shotgun_soldier unique_pickaxe pickaxe paintrain shovel scattergun soda_popper force_a_nature pistol_scout maxgun the_winger bat holy_mackerel flamethrower shotgun_pyro fireaxe backburner axtinguisher flaregun deflect_flare deflect_promode deflect_rocket deflect_sticky taunt_pyro minigun shotgun_hwg fists natascha gloves taunt_heavy shotgun_primary pistol wrench obj_sentrygun obj_sentrygun2 obj_sentrygun3 sniperrifle machina bazaar_bargain smg shahanshah club tf_projectile_arrow compound_bow taunt_sniper tf_projectile_pipe tf_projectile_pipe_remote sword bottle revolver ambassador knife syringegun_medic battleneedle proto_syringe bonesaw blutsauger ubersaw solemn_vow world player fryingpan
  var %x = 1
  var %numweapons = $numtok(%weapons,32)
  write -c temp.txt
  while (%numweapons >= %x) {
    write temp.txt $gettok(%weapons,%x,32) int,
    inc %x
  }
  run temp.txt
}

alias weaponsfordb2 {
  var %weapons = tf_projectile_rocket liberty_launcher rocketlauncher_directhit quake_rl shotgun_soldier unique_pickaxe pickaxe paintrain shovel scattergun soda_popper force_a_nature pistol_scout maxgun the_winger bat holy_mackerel flamethrower shotgun_pyro fireaxe backburner axtinguisher flaregun deflect_flare deflect_promode deflect_rocket deflect_sticky taunt_pyro minigun shotgun_hwg fists natascha gloves taunt_heavy shotgun_primary pistol wrench obj_sentrygun obj_sentrygun2 obj_sentrygun3 sniperrifle machina bazaar_bargain smg shahanshah club tf_projectile_arrow compound_bow taunt_sniper tf_projectile_pipe tf_projectile_pipe_remote sword bottle revolver ambassador knife syringegun_medic battleneedle proto_syringe bonesaw blutsauger ubersaw solemn_vow world player fryingpan
  var %x = 1
  var %numweapons = $numtok(%weapons,32)
  var %insline, %insline2
  write -c temp.txt
  while (%numweapons >= %x) {
    %insline2 = $addtok(%insline2,$chr(39) $+ $chr(123) $+ $chr(36) $+ words $+ $chr(91) $+ %x $+ $chr(93) $+ $chr(125) $+ $chr(39) $+ $chr(44),32)
    inc %x
  }
  %x = 1
  while (%numweapons >= %x) {
    %insline = $replace(%weapons,$chr(32),$chr(44) $+ $chr(32))
    inc %x
  }
  echo LINE: INSERT INTO cw_weaponstats (steamid, %insline $+ ) VALUES ( $+ $chr(39) $+ $chr(123) $+ $chr(36) $+ words $+ $chr(91) $+ 0 $+ $chr(93) $+ $chr(125) $+ $chr(39) $+ $chr(44) %insline2 $+ )
}

alias weaponsfordb3 {
  var %weapons = tf_projectile_rocket liberty_launcher rocketlauncher_directhit quake_rl shotgun_soldier unique_pickaxe pickaxe paintrain shovel scattergun soda_popper force_a_nature pistol_scout maxgun the_winger bat holy_mackerel flamethrower shotgun_pyro fireaxe backburner axtinguisher flaregun deflect_flare deflect_promode deflect_rocket deflect_sticky taunt_pyro minigun shotgun_hwg fists natascha gloves taunt_heavy shotgun_primary pistol wrench obj_sentrygun obj_sentrygun2 obj_sentrygun3 sniperrifle machina bazaar_bargain smg shahanshah club tf_projectile_arrow compound_bow taunt_sniper tf_projectile_pipe tf_projectile_pipe_remote sword bottle revolver ambassador knife syringegun_medic battleneedle proto_syringe bonesaw blutsauger ubersaw solemn_vow world player fryingpan
  echo $replace(%weapons,$chr(32),$chr(44) $+ $chr(32))
}

alias populatestatfile {
  var %keys = id;name;kill;death;cap;assist;points;games;healing;uberlost;win;loss;draw;damage;ammopack_small;ammopack_medium;tf_ammo_pack;medkit_small;medkit_medium;healing_received
  var %x = 1, %y = $lines(%dir $+ teams.txt)
  write -c %dir $+ statsall.txt
  while (%x <= %y) {
    var %id = $gettok($read(%dir $+ teams.txt,%x),1,32), %nick
    if ($read(%dir $+ auth.txt,w,* $+ $chr(32) $+ %id $+ $chr(32) $+ *)) {
      %nick = $gettok($v1,3,32)
    }
    elseif ($read(%dir $+ registered.txt,w,* $+ $chr(32) $+ %id $+ $chr(32) $+ *)) {
      %nick = $gettok($v1,3,32)
    }
    else {
      %nick = $sid(%id)
    }
    ;write stats.txt $+($1,$chr(37),$3-,$str($chr(37) $+ 0,$calc($numtok(%keys,59) - 2)))
    write %dir $+ statsall.txt $+(%id,$chr(37),%nick,$str($chr(37) $+ 0,$calc($numtok(%keys,59) - 2)))
    inc %x
  }
}
