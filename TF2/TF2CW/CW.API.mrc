alias ozfortressApi {
  ;http://ozfortress.com/cw.php
  var %sock = api. [ $+ [ $ticks ] ]
  sockopen %sock ozfortress.com 80
}

on *:sockopen:api.*:{

  % [ $+ [ $sockname ] $+ .post_construct ] = $+(match_status=,$iif(%pug,1,0),&players_red=,2,&players_blue=,4,&score_red=,1334,&score_blue=,2222)
  var %postvars, %post_construct
  if (%cw.pugtime) {
    if (%pug) {
      if (%match.on) {
        %postvars = match_status map current_players_blue current_players_red timeleft
        var %match_status, %map, %current_players_blue, %current_players_red, %timeleft
        %post_construct = $+(match_status=,2,&current_players_red=,%pug.limit,&current_players_blue=,%pug.limit,&map=,%win.map,&timeleft=,$timer(timeleft).secs))
        echo @api POST CONSTRUCT: %post_construct
      }
      else {
        %postvars = match_status current_players_blue current_players_red
        var %red = $numtok(%red.players,32)
        var %blue = $numtok(%blue.players,32)
        %post_construct = $+(match_status=,1,&current_players_red=,%red,&current_players_blue=,%blue)
        echo @api POST CONSTRUCT: %post_construct
      }
    }
    else {
      %postvars = match_status
      %post_construct = match_status=0
    }
  }
  else {
    %postvars = match_status current_players_blue current_players_red
    var %red = asdf
    var %blue = asdf2
    %post_construct = $+(match_status=,3,&current_players_red=,%red,&current_players_blue=,%blue)
  }

  var %total_players_red = $readini(%dir $+ scores.ini,red,numplayers)
  var %total_players_blue = $readini(%dir $+ scores.ini,blue,numplayers)
  var %total_score_red = $readini(%dir $+ scores.ini,red,score)
  var %total_score_blue = $readini(%dir $+ scores.ini,blue,score)

  %postvars = %postvars total_players_red total_players_blue score_red score_blue
  %post_construct = $+(%post_construct,&total_players_red=,%total_players_red,&total_players_blue=,%total_players_blue,&score_red=,%total_score_red,&score_blue=,%total_score_blue)

  echo @api POSTVARS: %postvars
  echo @api POST_CONSTRUCT: %post_construct

  % [ $+ [ $sockname ] $+ .post_construct ] = %post_construct

  var %write = sockwrite -tn $sockname
  %write POST /cw.php HTTP/1.1
  %write Host: $+(games.ipgn.com.au,$crlf)
  %write Connection: close
  %write Content-Type: application/x-www-form-urlencoded
  %write Content-Length: $len(% [ $+ [ $sockname ] $+ .post_construct ]) 
  %write
}

on *:sockwrite:api.*:{
  if (!% [ $+ [ $sockname ] $+ .sent ]) {
    sockwrite -n $sockname % [ $+ [ $sockname ] $+ .post_construct ]
    set % [ $+ [ $sockname ] $+ .sent ] 1
  }
}

on *:sockread:api.*:{
  var %api.read
  sockread %api.read
  %api.read = $remove(%api.read,$chr(124))
  echo @api Socket $sockname read: %api.read
}

on *:sockclose:api.*:{
  echo @api Socket $sockname closed
  unset % [ $+ [ $sockname ] $+ .* ]
}

/*
echo "Match Status:" .  $_POST['match_status'];
echo "\ntotal_players_red: " . $_POST['total_players_red'];
echo "\ntotal_players_blue: " . $_POST['total_players_blue'];
echo "\nscore_red: " . $_POST['score_red'];
echo "\nscore_blue: " . $_POST['score_blue'];
echo "\ntimeleft: " . $_POST['timeleft'];
echo "\nmap: " . $_POST['map'];
echo "\ncurrent red: " . $_POST['current_players_red'];
echo "\ncurrent blue: " . $_POST['current_players_blue'];
*/
