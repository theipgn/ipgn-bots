alias challonge {
  if ($1 = getmatchid) {
    openChallongeSocket getmatchid challonge. [ $+ [ $ticks ] ] %challonge_tid $2
  }
  if ($1 = startTournament) {
    openChallongeSocket startTournament challonge. [ $+ [ $ticks ] ]
  }
  if ($1 = result) {
    ;challonge result <round> <matchid> <winner>
    openChallongeSocket result challonge. [ $+ [ $ticks ] $+ [ $rand(1,1000) ] ] %challonge_tid $2 $3 $4
  }
  if ($1 = allresult) {
    openChallongeSocket result challonge. [ $+ [ $ticks ] ] %challonge_tid $1
  }
}

alias openChallongeSocket {
  if ($1 = getmatchid) {
    set % [ $+ [ $2 ] $+ .type ] 1
    set % [ $+ [ $2 ] $+ .tid ] $3
    set % [ $+ [ $2 ] $+ .round ] %ulticomp.round
  }
  if ($1 = startTournament) {
    set % [ $+ [ $2 ] $+ .type ] 2
  }
  if ($1 = result) {
    set % [ $+ [ $2 ] $+ .type ] 3
    set % [ $+ [ $2 ] $+ .tid ] $3
    set % [ $+ [ $2 ] $+ .round ] $4
    set % [ $+ [ $2 ] $+ .matchid ] $5
    set % [ $+ [ $2 ] $+ .winnerseed ] $6
  }
  if ($1 = allresult) {
    set % [ $+ [ $2 ] $+ .type ] 4
    set % [ $+ [ $2 ] $+ .tid ] $3
    set % [ $+ [ $2 ] $+ .round ] $4
  }
  sockopen $2 ultiduo.ipgn.com.au 80
}

on *:sockopen:challonge.*:{
  var %e = sockwrite -n $sockname
  echo @challonge GET $+(/challonge/,$challongeTypeURL(% [ $+ [ $sockname ] $+ .type ],$sockname))
  %e GET $+(/challonge/,$challongeTypeURL(% [ $+ [ $sockname ] $+ .type ],$sockname)) HTTP/1.1
  %e Host: $+(ultiduo.ipgn.com.au,$crlf,$crlf)
  sockwrite $sockname $crlf
}

on *:sockread:challonge.*:{
  if ($sockerr) { echo @challonge SOCKET ERROR }
  var %challonge.data
  sockread %challonge.data
  if (%challonge.data) { echo @challonge %challonge.data }
  if (Auth and pass are valid isin %challonge.data) {
    echo @challonge 7Authorization successful
  }
  if (NEW TOURNAMENT ID isin %challonge.data) {
    set %challonge_tid $gettok(%challonge.data,2,37)
    set %challonge_started 1
  }
  if (ERROR isin %challonge.data) {
    echo @challonge 4ERROR: $gettok(%challonge.data,2,58)
    ;msg %ulticomp.chan $codes(CHALLONGE ERROR: $gettok(%challonge.data,2,58))
  }

}

on *:sockclose:challonge.*:{
  if ($sockerr) { echo @challonge SOCKET ERROR }
  unset % [ $+ [ $sockname ] $+ * ]
}

alias challongeTypeURL {
  if ($1 = 1) {
    return $+(getmatchid.php?auth=ultiduo&pass=,%challonge_pass,&tid=,% [ $+ [ $2 ] $+ .tid ],&round=,% [ $+ [ $2 ] $+ .round ])
  }
  elseif ($1 = 2) {
    return $+(initialize.php?auth=ultiduo&pass=,%challonge_pass,&name=,%challonge_name,&url=,%challonge_url)
  }
  elseif ($1 = 3) {
    return $+(result.php?auth=ultiduo&pass=,%challonge_pass,&tid=,% [ $+ [ $2 ] $+ .tid ],&match=,% [ $+ [ $2 ] $+ .matchid ],&round=,% [ $+ [ $2 ] $+ .round ],&winner=,% [ $+ [ $2 ] $+ .winnerseed ])
  }
  elseif ($1 = 4) {
    return $+(results.php?auth=ultiduo&pass=,%challonge_pass,&tid=,% [ $+ [ $2 ] $+ .tid ],&round=,% [ $+ [ $2 ] $+ .round ])
  }
  else { echo @debug 4CHALLONGE: INVALID TYPE | return }
}
