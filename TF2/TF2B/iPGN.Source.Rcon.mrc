/*
A complete mIRC implementation of the Source RCON Protocol

Not for distribution outside of iPGN without the permission of the author

Copyright 2011 Prithu "bladez" Parker
*/



on *:START:{
  window -e @rcon
}

alias sourceRcon {
  if (!$1) || (!$2) || (!$3) || (!$4) { 
    echo -a Invalid parameters specified. /sourceRcon ip port rcon [chan] command
    return
  }

  echo @rcon sourceRcon params: $1-

  echo @rcon chan: $chan

  var %sock = rcon. [ $+ [ $1 ] $+ _ $+ [ $2 ] ]

  if (!$sock(%sock)) {
    rconSockCleanup %sock

    set % [ $+ [ %sock ] $+ .ip ] $1
    set % [ $+ [ %sock ] $+ .port ] $2
    set % [ $+ [ %sock ] $+ .rcon ] $3

    if ($left($4,1) == $chr(35)) {
      set % [ $+ [ %sock ] $+ .command ] $5-
      set % [ $+ [ %sock ] $+ .chan ] $4
    }
    else {
      set % [ $+ [ %sock ] $+ .command ] $4-
    }

    sockopen %sock $1 $2

    sockmark %sock $1 $+ : $+ $2
  }
  elseif ($sock(%sock)) && (!% [ $+ [ %sock ] $+ .authenticated ]) {
    echo @rcon $time([ddd hh:nn:ss]) %sock was unable to authenticate... Will retry
    sockclose %sock

    set % [ $+ [ %sock ] $+ .ip ] $1
    set % [ $+ [ %sock ] $+ .port ] $2
    set % [ $+ [ %sock ] $+ .rcon ] $3

    if ($left($4,1) == $chr(35)) {
      set % [ $+ [ %sock ] $+ .command ] $5-
      set % [ $+ [ %sock ] $+ .chan ] $4
    }
    else {
      set % [ $+ [ %sock ] $+ .command ] $4-
      unset % [ $+ [ %sock ] $+ .chan ]
    }

    sockopen %sock $1 $2

    sockmark %sock $1 $+ : $+ $2
  }
  else {
    if (% [ $+ [ %sock ] $+ .receiving ]) {
      echo @rcon $time([ddd hh:nn:ss]) %sock is currently receiving data... putting this command into a timer
      .timer 1 2 sourceRcon $1-
    }
    else {
      if (rcon_password isin $1-) {
        rconSockCleanup %sock
        .timer 1 1 sourceRcon $1-
      }
      else {
        if ($left($4,1) == $chr(35)) {
          sendRconCommand exec %sock $5-
          set % [ $+ [ %sock ] $+ .chan ] $4
        }
        else {
          unset % [ $+ [ %sock ] $+ .chan ]
          sendRconCommand exec %sock $4-
        }
      }
    }
  }
  .timer $+ %sock 1 300 rconSockCleanup %sock
}

on *:sockopen:rcon.*:{
  if ($sockerr) {
    return
  }
  sendRconCommand auth $sockname
}

/*
DATA: 10 0 0 1 0 0 0 0 0 0 0 0 0 ------ junk packet on authentication, this is a normal sight
DATA: 10 0 0 1 0 0 0 2 0 0 0 0 0 ------ auth response. successful if the 5th byte (id) is the same as the one we connected with and 9th is 2, -1 if unsuccessful
;first byte(s) in first 4-set is always packet length - max of 4096 chars
*/

on *:sockread:rcon.*:{
  if ($sockerr > 0) { 
    echo @rcon $time([ddd hh:nn:ss]) Socket error in $sockname $+ ! Error: $sock($sockname).wsmsg
    return 
  }
  :nextread
  sockread &data

  if ($sockbr == 0) { return }

  echo @rcon $time([ddd hh:nn:ss]) DATA READ: $bvar(&data,1-312) Actual Length: $bvar(&data,1-4)

  if ($bvar(&data,0) == 1) { goto nextread }

  if ($bvar(&data,5) == -1) || ($bvar(&data,5) == 255) {
    echo @rcon $time([ddd hh:nn:ss]) Invalid RCON password specified for $gettok($sock($sockname).mark,1,58) $+ : $+ $gettok($sock($sockname).mark,2,58) ( $+ % [ $+ [ $sockname ] $+ .rcon ] $+ )
    if (% [ $+ [ $sockname ] $+ .chan ]) {
      msg $v1 $colours(An error occurred when sending an RCON command to $sock($sockname).mark (Invalid RCON password/Banned))
      if ($getserverid($gettok($sock($sockname).mark,1,58), $gettok($sock($sockname).mark,2,58))) {
        var %serverinfo = $v1
        ;type%id , type 2 = ozfortress
        if ($gettok(%serverinfo,1,37) == 2) {
          var %sid = $gettok(%serverinfo,2,37)

          var %rcon = $readini(ozfservers.ini,%sid,rcon)
          var %bookedfor = $readini(ozfservers.ini,%sid,team)
          var %pass = $readini(ozfservers.ini,%sid,password)

          writeini ozfservers.ini %sid outofsync 1

          setHeavyDetails $gettok($sock($sockname).mark,2,58) %rcon %pass $iif(%bookedfor,$ifmatch,ozf_unbooked)
        }
      }
    }
    else {
      if ($getserverid($gettok($sock($sockname).mark,1,58), $gettok($sock($sockname).mark,2,58))) {
        var %serverinfo = $v1
        ;type%id , type 2 = ozfortress
        if ($gettok(%serverinfo,1,37) == 2) {
          var %sid = $gettok(%serverinfo,2,37)
          msg #ozf-help $colours(An error occurred when sending an RCON command to $sock($sockname).mark (Invalid RCON password/Banned))

          var %rcon = $readini(ozfservers.ini,%sid,rcon)
          var %bookedfor = $readini(ozfservers.ini,%sid,team)
          var %pass = $readini(ozfservers.ini,%sid,password)

          setHeavyDetails $gettok($sock($sockname).mark,2,58) %rcon %pass $iif(%bookedfor,$ifmatch,ozf_unbooked)
        }
      }
    }
    if ($sock($sockname)) { rconSockCleanup $sockname }
    return
  }
  ;2 is auth response
  if ($bvar(&data,9) == 2) {
    if (!% [ $+ [ $sockname ] $+ .authenticated ]) {
      set % [ $+ [ $sockname ] $+ .authenticated ] 1
      echo @rcon $time([ddd hh:nn:ss]) received auth response. sending first command

      sendRconCommand exec $sockname % [ $+ [ $sockname ] $+ .command ]
    }
    else {
      ;occurs when a command is sent without being properly authenticated
      echo @rcon $time([ddd hh:nn:ss]) received a response code of 2 after we've already authenticated...
    }
  }

  ;0 is command response
  if ($bvar(&data,9) == 0) {

    if (!% [ $+ [ $sockname ] $+ .junked ]) {
      echo @rcon $time([ddd hh:nn:ss]) received junk packet
      set % [ $+ [ $sockname ] $+ .junked ] 1

      ;when THIS happens (2 packets at once): DATA READ: 10 0 0 0 1 0 0 0 0 0 0 0 0 0 10 0 0 0 1 0 0 0 2 0 0 0 0 0 Actual Length: 10 0 0 0

      if ($bvar(&data,19)) {
        echo @rcon $time([ddd hh:nn:ss]) mirc read double packets
        if ($v1 == -1) || ($v1 == 255) {
          echo @rcon $time([ddd hh:nn:ss]) Invalid RCON password specified.
          if ($sock($sockname)) { sockclose $sockname }
          return
        }
        if ($bvar(&data,23) == 2) {
          set % [ $+ [ $sockname ] $+ .authenticated ] 1
          echo @rcon $time([ddd hh:nn:ss]) received auth response. sending first command

          sendRconCommand exec $sockname % [ $+ [ $sockname ] $+ .command ]
        }
      }
    }
    else {
      echo @rcon $time([ddd hh:nn:ss]) received command response
      ;we know data begins at the 13th byte and ends 2 bytes from the end. Therefore
      var %rconresponse = $bvar(&data,13,$calc($bvar(&data,0) - 2)).text
      ;echo @rcon $time([ddd hh:nn:ss]) TEXT DATA: %rconresponse

      set % [ $+ [ $sockname ] $+ .receiving ] 1

      ;now we loop if there's multiple lines (ascii 10 is line feed) 
      var %y = $numtok(%rconresponse,10)
      if (%y > 1) {
        var %x = 1
        while (%x <= %y) {
          var %responseline = $gettok(%rconresponse,%x,10)

          echo @rcon $time([ddd hh:nn:ss]) Line %x $+ : %responseline
          if (% [ $+ [ $sockname ] $+ .chan ])  && (%x <= 7) {
            msg % [ $+ [ $sockname ] $+ .chan ] $colours(L $+ %x $+ : %responseline)
          }


          ;Total Slots 710, Spectators 0, Proxies 3
          ;Total Slots 506, Spectators 11, Proxies 2
          if ($regex(%responseline,/Total Slots (\d+)\x2C Spectators (\d+)\x2C Proxies (\d+)/i)) {
            var %ip = $gettok($sock($sockname).mark,1,58), %port = $gettok($sock($sockname).mark,2,58)

            var %a = 1, %b = $calc($ini(ipgnrelayservers.ini,0) - 1)
            while (%a <= %b) {
              if ($readini(ipgnrelayservers.ini,%a,booked)) {
                var %ini_ip = $readini(ipgnrelayservers.ini,%a,ip)
                var %ini_port = $readini(ipgnrelayservers.ini,%a,rconport)

                if (%ip == %ini_ip) && (%port == %ini_port) {
                  if (($readini(ipgnrelayservers.ini,%a,numspecs) < $regml(2)) || (!$readini(ipgnrelayservers.ini,%a,numspecs))) {
                    writeini -n ipgnrelayservers.ini %a numspecs $regml(2)
                  }
                  break
                }

              }
              inc %a
            }
          }
          ;Game Time 04:28, Mod "tf", Map "koth_pro_viaduct_rc3", Players 14
          ;Game Time 3:53:22, Mod "tf", Map "cp_snakewater", Players 17
          if ($regex(%responseline,/Game Time (\d+)?:?(\d+):(\d+)\x2C Mod "tf"\x2C Map "(.*)"\x2C Players (\d+)/i)) {
            echo @rcon $time([ddd hh:nn:ss]) game time match
            var %ip = $gettok($sock($sockname).mark,1,58), %port = $gettok($sock($sockname).mark,2,58)

            var %a = 1, %b = $calc($ini(ipgnrelayservers.ini,0) - 1)
            while (%a <= %b) {
              if ($readini(ipgnrelayservers.ini,%a,booked)) {
                var %ini_ip = $readini(ipgnrelayservers.ini,%a,ip)
                var %ini_port = $readini(ipgnrelayservers.ini,%a,rconport)

                if (%ip == %ini_ip) && (%port == %ini_port) {
                  echo @rcon $time([ddd hh:nn:ss]) ip and port match!

                  if ($regml(0) == 4) {
                    writeini -n ipgnrelayservers.ini %a gametime $regml(1) $+ : $+ $regml(2)
                    writeini -n ipgnrelayservers.ini %a map $regml(3)
                    writeini -n ipgnrelayservers.ini %a players $regml(4)
                  }
                  else {
                    writeini -n ipgnrelayservers.ini %a gametime $regml(1) $+ : $+ $regml(2) $+ : $+ $regml(3)
                    writeini -n ipgnrelayservers.ini %a map $regml(4)
                    writeini -n ipgnrelayservers.ini %a players $regml(5)
                  }

                  break
                }
              }
              inc %a
            }
          }

          ;TV_PORT for bookables
          ;sourcetv: port 27016, delay 90.0s
          if ($regex(%responseline,/sourcetv\S\s+port\s+(\d+)\S\s+delay\s+(\d+)\S+/i)) {
            echo @rcon $time([ddd hh:nn:ss]) GOT SOURCETV: PORT $regml(1) DELAY $regml(2)
            var %serverinfo = $getserverid($gettok($sock($sockname).mark,1,58), $gettok($sock($sockname).mark,2,58))
            if (%serverinfo) {
              var %sid = $gettok($v1,2,37)
              var %dbfile = $iif($gettok($v1,1,37) == 1,ipgnservers.ini,ozfservers.ini)

              writeini -n %dbfile %sid tv_port $regml(1)
              writeini -n %dbfile %sid tv_delay $regml(2)
            }
          }

          ;players for bookables
          ;players : 3 (23 max)
          if ($regex(%responseline,/players : (\d+)/i)) {
            echo @rcon $time([ddd hh:nn:ss]) GOT PLAYERS; REGEX: $regml(1)
            var %serverinfo = $getserverid($gettok($sock($sockname).mark,1,58), $gettok($sock($sockname).mark,2,58))
            if (%serverinfo) {
              var %sid = $gettok($v1,2,37)
              var %dbfile = $iif($gettok($v1,1,37) == 1,ipgnservers.ini,ozfservers.ini)

              writeini -n %dbfile %sid last_players $regml(1)

              ;only stv is in the server... so check for auto reset (ozf only)
              if ($gettok(%serverinfo,1,37) == 2) && ($readini(%dbfile,%sid,booked)) {
                if ($regml(1) <= 1) {
                  hinc ozfhash %sid
                  if ($hget(ozfhash,%sid) > 2) {
                    ozfServerReset %sid
                  }
                }
                else {
                  if ($hget(ozfhash,%sid)) {
                    hdel ozfhash %sid
                  }
                }
              }

            }
          }

          inc %x
        }
      }
      else {
        bcopy &response 1 &data 13 $calc($bvar(&data,0) - 2)
        breplace &response 10 0
        echo @rcon $time([ddd hh:nn:ss]) Response: $bvar(&response,1-).text

        if (% [ $+ [ $sockname ] $+ .chan ]) {
          msg $v1 $colours(Server command response: $bvar(&response,1-).text)
        }
      }
      set % [ $+ [ $sockname ] $+ .receiving ] 0
    }
  }

  goto nextread
}

alias -l sendRconCommand {
  ;sendRconCommand <auth/exec> <socket> <command if exec>
  if (!$sock($2)) {
    echo @rcon $time([ddd hh:nn:ss]) 4ERROR: RCON socket $2 died between calling the send and getting to the command (0.000000000000000000000001 seconds) WTF? 
    return
  }

  if ($1 != auth) && ($1 != exec) {
    echo @rcon $time([ddd hh:nn:ss]) 4ERROR: INVALID RCON PARAMETER SPECIFIED
    return
  }
  if ($1 == exec) && (!$3) {
    echo @rcon $time([ddd hh:nn:ss]) 4ERROR: YOU CAN'T SEND AN EXEC PARAMETER WITHOUT A COMMAND
    return
  }

  var %SERVERDATA_EXECCOMMAND = 2, %SERVERDATA_AUTH = 3, %rcon = % [ $+ [ $sockname ] $+ .rcon ], %sock = $2, %reqid

  ;define our local bvars
  bset &conType 1 0
  bset &rconPassword 1 0
  bset &command 1 0
  bset &packetlength 1 0

  ;don't convert request ID or SERVERDATA_* to literal ASCII characters; they are integers and should be treated as such! same goes for packetlength later


  ;don't use sockwrite -b, because it sends a UDP encapsulated packet via TCP stream (STUN) rather than a proper TCP packet, and fucks shit up
  ;sockwrite -b $sockname $calc(%packetlength + 4) &rcon

  if ($1 == auth) {
    ;auth packet stuff

    ;may need to dynamically choose, or increase request id at some point
    set % [ $+ [ $sockname ] $+ .reqid ] 1
    %reqid = 1

    echo @rcon $time([ddd hh:nn:ss]) sending rcon password %rcon to $sock($sockname).mark

    bset &conType 1 %SERVERDATA_AUTH

    var %x = 1, %y = $len(%rcon)
    while (%x <= %y) {
      bset &rconPassword %x $asc($mid(%rcon,%x,1))
      inc %x
    }
    echo @rcon $time([ddd hh:nn:ss]) rconpassword check: $bvar(&rconPassword,1,$bvar(&rconPassword,0)).text

    ;packet length: 4 bytes for id, 4 bytes for request type, n bytes for rcon pass, 1 byte for null between strings, and then 1 null byte for the end, 0 is the length of our null string
    var %packetlength = $calc(4 + 4 + $bvar(&rconPassword,0) + 1 + 0 + 1)
    bset &packetlength 1 %packetlength
  }
  elseif ($1 == exec) {
    var %command = $3-
    echo @rcon $time([ddd hh:nn:ss]) sending rcon command %command to $sock(%sock).mark

    ;we increase the reqid for each command

    inc % [ $+ [ %sock ] $+ .reqid ]

    %reqid = % [ $+ [ %sock ] $+ .reqid ]

    bset &conType 1 %SERVERDATA_EXECCOMMAND

    var %x = 1, %y = $len(%command)
    while (%x <= %y) {
      bset &command %x $asc($mid(%command,%x,1))
      inc %x
    }

    echo @rcon $time([ddd hh:nn:ss]) command check: $bvar(&command,1,$bvar(&command,0)).text

    ;packet length: 4 bytes for id, 4 bytes for request type, n bytes for command, 1 byte for null between strings, and then 1 null byte for the end, 0 is the length of our null string
    var %packetlength = $calc(4 + 4 + $bvar(&command,0) + 1 + 0 + 1)
    bset &packetlength 1 %packetlength
  }
  else {
    echo @rcon $time([ddd hh:nn:ss]) 4I'm not sure how you got this far, but something is horribly wrong
    return
  }

  echo @rcon $time([ddd hh:nn:ss]) packetlength: %packetlength

  bset &reqid 1 %reqid


  ;now we pack it together
  bcopy &rcon 1 &packetlength 1 1
  bcopy &rcon 5 &reqid 1 $bvar(&reqid,0)
  bcopy &rcon 9 &conType 1 $bvar(&conType,0)
  if ($1 = auth) {
    bcopy &rcon 13 &rconPassword 1 $bvar(&rconPassword,0)
  }
  else {
    bcopy &rcon 13 &command 1 $bvar(&command,0)
  }
  ;null byte between strings
  bset &rcon $calc($bvar(&rcon,0) + 1) 0
  ;this position is an emtpy string. but we dont consider it a 'null'
  ;bcopy &rcon $calc($bvar(&rcon,0) + 1) &nullString 1 $bvar(&nullString,0)
  ;ending null byte
  bset &rcon $calc($bvar(&rcon,0) + 1) 0

  echo @rcon $time([ddd hh:nn:ss]) DATA TO SEND: $bvar(&rcon,1,$bvar(&rcon,0))

  sockwrite %sock &rcon
}

on *:sockclose:rcon.*:{
  if ($sockerr) {
    echo @rcon $time([ddd hh:nn:ss]) Socket error in $sockname $+ ! Error: $sock($sockname).wsmsg
  }
  echo @rcon $time([ddd hh:nn:ss]) socket $sockname closed

  rconSockCleanup $sockname
}

alias rconSockCleanup {
  ;cleanup $sockname
  unset % [ $+ [ $1 ] $+ .* ]
  unset % [ $+ [ $1 ] ]
  if ($sock($1)) {
    sockclose $1
  }

  if ($timer($1)) {
    .timer $+ $1 off
  }
}

alias getserverid {
  ;getserverid ip port
  ;@return type%id
  ;type = 1 for ipgn, 2 for ozfortress


  var %x = 1, %y = $calc($ini(ipgnservers.ini,0) - 1)
  while (%x <= %y) {
    var %ini_ip = $readini(ipgnservers.ini,%x,ip)
    var %ini_port = $readini(ipgnservers.ini,%x,port)

    if ($1 == %ini_ip) && ($2 == %ini_port) {  
      return $+(1,$chr(37),%x)
    }

    inc %x
  }

  var %x = 1, %y = $calc($ini(ozfservers.ini,0) - 1)
  while (%x <= %y) {
    var %ini_ip = $readini(ozfservers.ini,%x,ip)
    var %ini_port = $readini(ozfservers.ini,%x,port)

    if ($1 == %ini_ip) && ($2 == %ini_port) {  
      return $+(2,$chr(37),%x)
    }

    inc %x
  }

  return 0
}
