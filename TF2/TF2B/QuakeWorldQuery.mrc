;query command: ����status

alias qwquery { 
  ;qwquery ip port [chan/nick]
  if ((!$1) || (!$2)) { return }

  var %qtick = qwquery. [ $+ [ $ticks ] ]

  set % [ $+ [ %qtick ] $+ .ip ] $1
  set % [ $+ [ %qtick ] $+ .port ] $2
  set % [ $+ [ %qtick ] $+ .recipient ] $iif($3,$3,0)

  var %query = ����status  15

  sockudp -kn %qtick $1 $2 %query
  .timer $+ %qtick -m 1 400 endQuakeQuery $sockname
}

on *:udpread:qwquery.*:{
  var %tmp
  sockread -f %tmp 
  echo @squery QWQUERY: %tmp

  set % [ $+ [ $sockname ] $+ .havedata ] 1

  ;hostname\Sunkings CTF Server TWCreeper\...\map\twctf5
  if (($regex(%tmp,/\\hostname\\(.*?)\\.*\\map\\(.*?)\\/i)) || ($regex(%tmp,/\\hostname\\(.*?)\\.*\\map\\(.*?)/i))) {
    echo @squery Hostname: $regml(1) Map: $regml(2)
    if (% [ $+ [ $sockname ] $+ .recipient ]) {
      msg $v1 3Hostname: $regml(1) 3Map: $regml(2)
    }
  }

  ;1 0 19 29 "Sunking" "ctfb1" 0 4
  if ($regex(%tmp,/(\d+)\s+(\d+|S)\s+(\d+)\s+(\d+)\s+"(.*)"\s+"(.*)"\s+(\d+)\s+(\d+)/)) {
    echo @squery regex player match! Name: $regml(5) Location: $regml(6) Score (or spec): $regml(2)
    if (!% [ $+ [ $sockname ] $+ .players ]) {
      set % [ $+ [ $sockname ] $+ .players ] $+($regml(5),$iif($regml(2) = S,(spec)))
    }
    else {
      % [ $+ [ $sockname ] $+ .players ] = $addtok(% [ $+ [ $sockname ] $+ .players ],$+($regml(5),$iif($regml(2) = S,(spec))),32)
    }
  }

  .timer $+ $sockname -m 1 300 endQuakeQuery $sockname
}

alias endQuakeQuery {
  if (!$1) { return }
  if (% [ $+ [ $1 ] $+ .havedata ]) {
    if (% [ $+ [ $1 ] $+ .recipient ]) {
      msg $v1 3Players: $iif(% [ $+ [ $1 ] $+ .players ],$ifmatch,None)
    }
  }
  unset % [ $+ [ $1 ] $+ .* ]
  sockclose $1
}
