on *:udpread:stvlog:{
  var %data
  :nextread
  sockread %data

  if ($sockbr == 0) {
    return
  }

  echo @stvlog $time(dd/mm/yyyy hh:nn:ss) %data

  goto nextread
}
