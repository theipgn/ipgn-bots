on *:TEXT:!sms*:#ipgn-tf2:{
  if ($1 = !sms) {
    if ($readini(sms.ini,numbers,$2) || $2 isnum) && ($len($3-) <= 160) && ($3) {
      if ($2 isnum) { var %target = $2 }
      else { var %target = $readini(sms.ini,numbers,$2) }
      sms get /httpsend.php $+(usr=admin@ipgn.com.au&pass=caj00n&mess=,$3-,&to=,%target,&from=,$nick)
    }
    else { msg $chan $colours(Syntax: !sms <name|number> <message>) }
  }
  if ($1 = !smsnum) {
    if (($2 !isnum) || ($len($2) != 10)) { msg $chan $colours(Invalid number specified) | return }
    if (!$readini(sms.ini,numbers,$nick)) {
      writeini sms.ini numbers $nick $2
      msg $chan $colours(Added number $2 for $nick)
    }
    else {
      msg $chan $colours(Number already exists for your nick ( $+ $readini(sms.ini,numbers,$nick) $+ ))
    }
  }
}

alias sms {
  write -c sms.txt
  ; Open a new window, where we will output the data
  window -e @sms
  linesep @sms

  ; Set first parameter, which defaults to GET
  var %method = $iif($1,$1,GET)

  ; Set second parameter, which defautls to /
  var %page = $iif($2,$2,/)

  %string = $urlencode_string($3-)

  ; Set socket name
  var %sock = sms $+ $ticks

  ; Open socket and send data to it
  sockopen %sock smsmanager.com.au 80
  sockmark %sock %method %page %string
}

on *:SOCKOPEN:sms*:{
  ; Connection has been made
  aline @sms Connection established...
  aline @sms MODE: $getmark($sockname,1-)

  ; Define command
  var %a = sockwrite -n $sockname


  ; First send GET/POST, next send Host
  %a $getmark($sockname,1-2) $+ $iif($getmark($sockname,1) == GET && %string,$+(?,%string)) HTTP/1.1
  %a Host: smsmanager.com.au

  ; If method is POST, we need to include 2 extra parameters
  if ($getmark($sockname,1) == POST) {
    %a Content-Length: $len(%string)
    %a Content-Type: application/x-www-form-urlencoded
  }

  ; Force connection to close
  %a Connection: close

  ;Just-to-make-sure requests:
  %a Accept: */*
  %a Accept-Charset: *
  %a Accept-Encoding: *
  %a Accept-Language: *
  %a User-Agent: Mozilla/5.0

  %a $crlf

}

on *:SOCKREAD:sms*:{
  sockread %tmp
  echo @sms %tmp
  if ($regex(%tmp,/^success$/)) {
    msg #ipgn-tf2 $colours(Success!)
  }

  write sms.txt %tmp
}
alias getmark {
  ; $getmark(socketname,N)
  ; This alias returns the Nth word from the socketmark from socket socketname
  return $gettok($sock($1).mark,$$2,32)
}

alias urlencode_string {
  ; Encodes a whole string of data in the format name1=data1&name2=data2
  ; Example: $urlencode_string(name1=test&name2=test2)
  ; Returns: name1=%74%65%73%74&name2=%74%65%73%74%32
  var %a = 1, %string = $1, %output
  while ($gettok(%string,%a,38)) {
    tokenize 61 $v1
    if (%a != 1) %output = %output $+ &
    %output = $+(%output,$1,=,$urlencode($2))
    inc %a
  }
  return %output
}
alias urlencode {
  var %a = $regsubex($$1,/([^\w\s])/Sg,$+(%,$base($asc(\t),10,16,2)))
  return $replace(%a,$chr(32),$chr(43))
}
