on *:START:{
  window -e @serverdata
  sockudp -kn serverdata 6002 202.138.3.30 27015
}

on *:TEXT:!*:#ipgn-tf2,#ozf-help:{
  if ($1 = !recent) && ($nick isop $chan) {
    var %demodir = %statsdir $+ demos\, %rf = %demodir $+ recent.txt, %af = %demodir $+ all.txt, %tf = %demodir $+ temp.txt, %tfsorted = %demodir $+ tempsorted.txt
    .copy -o %af %af $+ .tmp
    var %af = %af $+ .tmp

    var %x = 1, %y = $lines(%af), %count = 1
    if ($fopen(alldemos)) { .fclose alldemos }
    .fopen alldemos %af

    if ($fopen(alldemos).err) {
      msg $chan $colours(Error opening demo file)
      .fclose alldemos
      return
    }

    .fseek -l alldemos 1

    if ($2) {
      var %search = $2
      while (!$eof(alldemos)) {
        .fseek -w alldemos * $+ %search $+ *
        var %ld = $fread(alldemos)
        write %tf %ld
        echo @serverdata Writing %ld to %tf
      }

      .fclose alldemos

      filter -cfftue 5 42 %tf %tfsorted

      .fopen sorted %tfsorted

      if ($fopen(tfsorted).err) {
        msg $chan $colours(Error opening sorted file)
        .fclose sorted
        return
      }

      .fseek -l sorted 1

      while (!$eof(sorted)) {
        var %ld = $fread(sorted)
        msg $chan $colours(12 $+ %count $+ : $gettok(%ld,2,42) 12Length: $duration($calc($gettok(%ld,6,42) - $gettok(%ld,5,42))) 12Server: $replace($gettok(%ld,4,42),_,:) 12Log: $gettok(%ld,3,42))

        inc %count
        if (%count > 8) { break }
        inc %x
      }

      .fclose sorted

      .remove %tf
      .remove %tfsorted
    }
    else {
      msg $chan $colours(penis)
    }
  }
}

alias fpos {
  if ($fopen($1)) {
    return $fopen($1).pos
  }
}

alias eof {
  if ($fopen($1)) {
    return $fopen($1).eof
  }
  else {
    return 1
  }
}

alias ferror {
  return $fopen($1).err
}

on *:udpread:serverdata:{
  var %mr.data
  sockread -f %mr.data
  echo @serverdata $time(HH:nn dd/mm/yyyy) %mr.data
  ;STOP_RECORD@demo-asdf-12234-4352-56.zip@holder@202.138.3.404_27030
  if ($istok(%mr.data,START_RECORD,64)) {
    var %demo = $gettok(%mr.data,2,64), %log = in_progress, %server = $gettok(%mr.data,4,64)
    var %demodir = %statsdir $+ demos\, %serverdir = %demodir $+ \ $+ %server $+ \
    if (!$exists(%serverdir)) {
      mkdir %serverdir
    }
    var %dayfile = $time(ddd-dd-mm-yyyy) $+ .txt
    write %demodir $+ recent.txt $+(%dayfile,*,%demo,*,%log,*,%server,*,$ctime)
    write %demodir $+ all.txt $+(%dayfile,*,%demo,*,%log,*,%server,*,$ctime)
    write %serverdir $+ %dayfile $+(%demo,*,%log,*,$ctime)
  }
  if ($istok(%mr.data,STOP_RECORD,64)) {
    var %demo = $gettok(%mr.data,2,64), %log = $gettok(%mr.data,3,64), %server = $gettok(%mr.data,4,64), %prevtime, %ctime = $ctime
    var %demodir = %statsdir $+ demos\, %serverdir = %demodir $+ \ $+ %server $+ \
    var %dayfile = $time(ddd-dd-mm-yyyy) $+ .txt, %rf = %demodir $+ recent.txt, %af = %demodir $+ all.txt, %df = %serverdir $+ %dayfile
    if ($read(%rf,w,* $+ %demo $+ *)) {
      var %l = $readn, %ld = $v1
      write -l $+ %l %rf $reptok(%ld,in_progress,%log,1,42) $+ * $+ %ctime
      write -w* $+ %demo $+ * %af $reptok(%ld,in_progress,%log,1,42) $+ * $+ %ctime
    }
    if ($read(%df,w,* $+ %demo $+ *)) {
      var %l = $readn, %ld = $v1
      %prevtime = $gettok(%ld,3,42)
      write -l $+ $readn %df $reptok(%ld,in_progress,%log,1,42) $+ * $+ %ctime
    }

    echo @serverdata Prevtime: %prevtime Ctime: %ctime Diff: $calc(%ctime - %prevtime)
    if ($getserverid($gettok(%server,1,95), $gettok(%server,2,95))) {
      var %sowner = $gettok($v1,1,37)
      var %dchan = $iif(%sowner == 1,#ipgn-tf2,#ozf-help)

      msg %dchan $colours(12New demo: %demo 12Length: $duration($calc(%ctime - %prevtime)) 12Server: $replace(%server,_,:) 12Log: %log)
    }
    else {
      echo @serverdata Demo is from server not in booking pool. Not displaying anywhere
    }
  }
  if ($istok(%mr.data,CVAR_REPORT,$asc(!))) {
    ;CVAR_REPORT!NAME!ID!IP!CVAR!VALUE
    msg #ozfl CVAR REPORT !! $gettok(%mr.data,2,33) ( $+ $gettok(%mr.data,3,33) $+ ) @ $gettok(%mr.data,4,33) has cvar " $+ $gettok(%mr.data,5,33) $+ " with value " $+ $gettok(%mr.data,6,33) $+ " !!
    write cvarreport.txt %mr.data
  }
  if ($istok(%mr.data,CVAR_REPORT-BAN,$asc(!))) {
    msg #ozfl CVAR BAN !! $gettok(%mr.data,2,33) ( $+ $gettok(%mr.data,3,33) $+ ) @ $gettok(%mr.data,4,33) has been banned for cvar " $+ $gettok(%mr.data,5,33) $+ " with value " $+ $gettok(%mr.data,6,33) $+ " !!
  }
}
