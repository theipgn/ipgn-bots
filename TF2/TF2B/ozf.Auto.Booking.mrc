;Servers by request for #ozf-help
on *:START:{
  hmake ozfhash 30
}

on *:JOIN:#ozf-help:{
  var %hashednick = $hmac(sha1, lucyfire, $lower($nick))
  notice $nick $colours(Demos that are recorded with your booking name will be at http://heavy.ozfortress.com/demos/pub/ $+ %hashednick)
}

on *:TEXT:*:#ozf-help:{
  if (($regex($1-,(thank|\Sty\S|thnx|chur|cheer))) && ($hget(ozfhash,$nick))) {
    msg $chan $gettok(%gratitude_response,$rand(1,$numtok(%gratitude_response,37)),37)
  }

  if ($regex($1-,/(old|older|out of date).*version/)) {
    msg #ozfl $colours(Servers need updating)
  }

  if ($regex($1-,/(unbook)/)) {
    if ($hget(ozfhash,$nick)) {
      var %data = $v1

      if ($address($nick,2) != $gettok(%data,2,59)) {
        msg $chan $colours($address($nick,2) is spoofing as12 $nick $+ . Automatically banning)

        ban -k $chan $nick 2 User spoofing

        return
      }

      ozfServerReset $gettok(%data,1,59)

      return
    }
  }

  if ($regex($1-,/((can|could|may) (.*) (book|have|get|grab|hire|please).*(server|sever|srver|servr|sevre))|((book|get|request)\s+(a\s+)?(\S+)?(\s+)?(server|please))|(server (pl(ease|z|s)\??|thanks|for))|(for scrim)/i)) && ($nick !isop $chan) {
    if (mib_ isin $nick) {
      msg $nick $colours(You must change your name before you can book a server)
      return
    }

    if ($hget(ozfhash,$nick)) {
      var %data = $v1

      if ($address($nick,2) != $gettok(%data,2,59)) {
        msg $chan $colours($address($nick,2) is spoofing as12 $nick $+ . Automatically banning)

        ban -k $chan $nick 2 User spoofing

        return
      }

      var %sid = $gettok(%data,1,59)
      var %type = $readini(ozfservers.ini,%sid,type)
      var %active = $readini(ozfservers.ini,%sid,active)

      var %ip = $readini(ozfservers.ini,%sid,ip)
      var %port = $readini(ozfservers.ini,%sid,port)
      var %rcon = $readini(ozfservers.ini,%sid,rcon)
      var %pass = $readini(ozfservers.ini,%sid,password)

      if (%active) {
        msg $chan $colours(You have already booked a server. Server %sid ( $+ %type $+ ) was given to you. Details have been automatically sent to you again)
        notice $nick $colours(Details for your previous booking: $connectString(%ip,%port,%pass,%rcon))
      }
      else {
        msg $chan $colours(You have already requested a server. Please wait until an administrator accepts or declines the request)
      }

      return
    }

    var %numbooked = $readini(ozfservers.ini,global,numbooked)
    if (%numbooked = $calc($ini(ozfservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | return }

    var %x = 1, %numservers = $calc($ini(ozfservers.ini,0) - 1), %booked, %rtype, %duration = 3, %bookingtime

    if ($regex($1-,/(\sgu\s|g.*united|gamersun|gun)/i)) {
      %rtype = gu
    }
    elseif ($regex($1-,/(hl|highlander)/i)) {
      %rtype = ozfortress
    }
    elseif ($regex($1-,/(ipgn)/i)) {
      %rtype = ipgn
    }


    if ($regex($1-,/(\d)\:?(\d{0,2})(am|pm)?/i)) {
      if (($regml(1) < 12) && ($regml(2) < 59)) {
        echo -s We have a time booking request! $regml(1) $regml(2) $regml(3)
      }
    }

    if ($regex($1-,/(\d+)(\s+|\S+)(hours|hrs|hour)/i)) {
      %duration = $regml(1)
    }

    while (%x <= %numservers) {
      var %type = $readini(ozfservers.ini,%x,type)
      if ((%type = %rtype) || (!%rtype) || (!%type)) {
        if ($readini(ozfservers.ini,%x,booked) == 0) {
          var %serverid = %x
          var %numbooked = $readini(ozfservers.ini,global,numbooked)

          writeini -n ozfservers.ini global numbooked $calc(%numbooked + 1)
          writeini -n ozfservers.ini %x booked 1
          writeini -n ozfservers.ini %x booker $nick
          writeini -n ozfservers.ini %x duration %duration
          writeini -n ozfservers.ini %x team $nick
          writeini -n ozfservers.ini %x bookedat $time(h:nntt dddd)

          %booked = 1

          if (%ozf.AutoAcceptBookings) {
            var %rcon = $lower($read(passwords.txt))
            var %pass = $lower($read(passwords.txt))
            var %currentrcon = $readini(ozfservers.ini,%serverid,defaultrcon)
            var %ip = $readini(ozfservers.ini,%serverid,ip)
            var %port = $readini(ozfservers.ini,%serverid,port)

            writeini -n ozfservers.ini %serverid active 1
            writeini -n ozfservers.ini %serverid password %pass
            writeini -n ozfservers.ini %serverid rcon %rcon

            rconbooking %ip %port %currentrcon say This server has been booked for %duration hours; sv_password %pass ; mr_ipgnbooker $nick ; livelogs_name $nick ; kickall; wait 500; changelevel cp_granary; rcon_password %rcon

            if (%type = ozfortress) {
              setHeavyDetails %port %rcon %pass $nick
            }

            notice $nick $colours(Your booking request has been automatically accepted. Details: $connectString(%ip,%port,%pass,%rcon))
            msg $nick $colours(Your booking request has been automatically accepted. Details: $connectString(%ip,%port,%pass,%rcon))
            msg $chan $colours(Server request by $nick for %duration hour(s) has been accepted. Server %serverid ( $+ %type $+ ) has been automatically chosen)

            .timerresetozf off
            .timerresetozf $+ %serverid 1 $calc(%duration * 3600) ozfServerReset %serverid
            .timerozfautoexpire $+ %serverid off

            if ($2 !isop $chan) {
              .timer 1 120 kickban $chan $nick
            }

            .timerclearozfauto $+ %serverid 1 $calc(%duration * 3600) hdel ozfhash $nick

            ozfAddServerStats BOOKED %serverid $nick $ctime $date(yyyy-mm-dd)
          }
          else {
            notice $chan $colours($nick is requesting a server for %duration hours. Server %x ( $+ %type $+ ) has been automatically chosen. Please wait for an administrator to accept the booking)
            msg $chan $colours($nick is requesting a server for %duration hours. Server %x ( $+ %type $+ ) has been automatically chosen. Please wait for an administrator to accept the booking)

            .timerozfautoexpire $+ %x 1 360 ozfServerRequestExpire $nick
            .timerresetozf $+ %x 1 360 ozfServerReset %x
          }

          hadd -m ozfhash $nick $+(%x,;,$address($nick,2))

          break
        }
      }
      inc %x
    }

    if (!%booked) {
      if (%rtype = gu) {
        msg $chan $colours(All Gamers United servers are in use)
      }
      elseif (%rtype = ipgn) {
        msg $chan $colours(All iPGN servers are in use)
      }
      elseif (%rtype = ozfortress) {
        msg $chan $colours(All ozfortress servers suitable for highlander are currently in use. There may be some iPGN servers available)
      }
      else {
        msg $chan $colours(All servers are currently in use)
      }
    }
  }

  if ($1 = !accept) && (($nick isop $chan) || ($address($nick,2) == *!*@bladezz.admin.ipgn)) {
    if ($2) && ($hget(ozfhash,$2)) {
      var %serverid = $gettok($hget(ozfhash,$2),1,59)
      var %bookingactive = $readini(ozfservers.ini,%serverid,active)

      if (%bookingactive) { return }
      var %rcon = $lower($read(passwords.txt))
      var %pass = $lower($read(passwords.txt))
      var %currentrcon = $readini(ozfservers.ini,%serverid,defaultrcon)
      var %ip = $readini(ozfservers.ini,%serverid,ip)
      var %port = $readini(ozfservers.ini,%serverid,port)
      var %type = $readini(ozfservers.ini,%serverid,type)

      writeini -n ozfservers.ini %serverid active 1
      writeini -n ozfservers.ini %serverid password %pass
      writeini -n ozfservers.ini %serverid rcon %rcon

      rconbooking %ip %port %currentrcon say This server has been booked for %duration hours $+ .; sv_password %pass ; kickall; wait 500; rcon_password %rcon ; changelevel cp_granary; mr_ipgnbooker $2

      if (%type = ozfortress) {
        setHeavyDetails %port %rcon %pass $2
      }

      notice $2 $colours($nick has accepted your request for a server. Details: $connectString(%ip,%port,%pass,%rcon))
      msg $2 $colours($nick has accepted your request for a server. Details: $connectString(%ip,%port,%pass,%rcon))
      msg $chan $colours(Server request by $2 has been accepted. Server %serverid ( $+ %type $+ ) has been granted)

      .timerresetozf off
      .timerresetozf $+ %serverid 1 $calc(%duration * 3600) ozfServerReset %serverid
      .timerozfautoexpire $+ %serverid off

      if ($2 !isop $chan) {
        .timer 1 120 kickban $chan $2
      }

      .timerclearozfauto $+ %serverid 1 $calc(%duration * 3600) hdel ozfhash $2

      ozfAddServerStats BOOKED %serverid $2 $ctime $date(yyyy-mm-dd)
    }
    else {
      msg $chan $colours(No booking or request exists for $2)
    }
  }
  if ($1 = !decline) && (($nick isop $chan) || ($address($nick,2) == *!*@bladezz.admin.ipgn)) {
    if ($hget(ozfhash,$2)) {
      var %serverid = $gettok($v1,1,59)
      notice $2 $colours($nick has declined your request for a server. Speak to said admin for assistance)
      msg $2 $colours($nick has declined your request for a server. Speak to said admin for assistance)
      ozfServerReset %serverid

      .timerozfautoexpire $+ %serverid off

      hdel ozfhash $2
    }
    else {
      msg $chan $colours(No booking or request exists for $2)
    }
  }
  if ($1 = !demos) {
    if (($nick isop $chan) && ($2) && ($len($2) < 40)) {
      var %hashednick = $hmac(sha1, lucyfire, $lower($2))
      notice $nick $colours(Demos for $2 are available at http://heavy.ozfortress.com/demos/pub/ $+ %hashednick)
    }
    else {
      var %hashednick = $hmac(sha1, lucyfire, $lower($nick))
      notice $nick $colours(Demos that are recorded with your booking name will be at http://heavy.ozfortress.com/demos/pub/ $+ %hashednick)
      msg $nick $colours(Demos that are recorded with your booking name will be at http://heavy.ozfortress.com/demos/pub/ $+ %hashednick)
    }
  }
  if ($1 = !toggleautobook) && (($nick isop $chan) || ($address($nick,2) == *!*@bladezz.admin.ipgn)) {
    if (%ozf.AutoAcceptBookings) {
      set %ozf.AutoAcceptBookings 0
      msg $chan $colours(Automatic booking acceptance is now disabled)
    }
    else {
      set %ozf.AutoAcceptBookings 1
      msg $chan $colours(Automatic booking acceptance is now enabled)
    }
  }
}

alias ozfServerRequestExpire {
  ;$0 $nick
  return

  hdel ozfhash $1

  var %x = 1, %y = $ini(ozfemails.ini,emails,0), %sendaddr
  while (%x <= %y) {
    var %name = $ini(ozfemails.ini,emails,%x)

    if ($readini(ozfemails.ini,spammable,%name)) {
      var %address = $readini(ozfemails.ini,emails,%name)
      %sendaddr = $addtok(%sendaddr,%address,59)
    }

    inc %x
  }

  sendmail $+(%sendaddr,$chr(37),Booking request expired,$chr(37),A booking request by $1 expired. Please assist when possible)
}

alias kickban {
  ;kickban chan nick reason
  if ($2 ison $1) {
    if ($2 !isop $1) && ($2 !isvoice $1) && ($address($nick,2) != *!*@bladezz.admin.ipgn) {
      ban -ku5 $1 $2 2 Booking auto kick (no idling) - please rejoin if you need further assistance
    }
  }
}
