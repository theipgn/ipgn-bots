;New banbot script (cleaner, new commands and better methods of doing things (aka no shitty aliases and unused variables))
on *:JOIN:#tf2pug,#tf2pug2:{
  if ($read(tf2pugplayers.txt,w,$nick $+ $chr(37) $+ *)) {
    write -w $+ $nick $+ * tf2pugplayers.txt $+($nick,$chr(37),$gethost($nick))
  }
  else {
    write tf2pugplayers.txt $+($nick,$chr(37),$gethost($nick))
  }
  if ($regex($nick,/(pugger_\d+|mibb.*\d+|pugg.*\d+)/i)) {
    msg $nick $colours(You must choose a name that resembles your in-game name before you are able to start or join a pug. Use /nick newnick)
  }
}


alias gethost {
  if ($regex($address($1,11),(.*)\@(.*?)\.(.*?)\.([A-z]))) {
    return $address($1,2)
  }
  elseif (static isin $address($1,2)) {
    ;*!*@123-243-166-16.static.tpgi.com.au
    return $v2
  }
  elseif (mibbit isin $address($1,5)) {
    return $v2
  }
  else {
    return *! $+ $gettok($gettok($address($1,11),1,$asc(@)),2,$asc(!)) $+ @*
  }
}

on *:NOTICE:*:*:{
  if ($nick = ChanServ) {
    if (%unbanplayer) {
      ;Sorry, no ban found for *!*@blah.*.
      if ($regex($1-,Sorry\x2C no ban found for (.*))) {
        return
      }
      if ($regex($1-,User with nick (.+) does not exist.)) {
        var %tf2pugplayersdata = $readpugplayers($remove($regml(1),))
        set %testdata $remove($regml(1),)
        msg ChanServ@Services.GameSurge.net delban #tf2pug $gettok(%tf2pugplayersdata,2,37)
        ;msg ChanServ@Services.GameSurge.net delban #tf2pug2 $gettok(%tf2pugplayersdata,2,37)
        unset %unbanplayer
      }
      if ($regex($1-,Matching ban.s. for (.*) removed)) {
        unset %unbanplayer
      }
    }
    if (%bancheckinfo) {
      .timerbaninfo 1 1 dobaninfo %bancheckinfo
      echo @baninfo $1-
      ;echo @baninfo $lines(%blah)
      write baninfo.txt $1-
    }
  }
}

on *:TEXT:!*:#tf2pug,#tf2pug2,#ipgn-tf2,?:{
  if ($nick isop #tf2pug) {
    if (($1 = !banplayer) || ($1 = !bp)) {
      ;!banplayer antiphon 20 2 reason
      if ($2) && ($3 = reason) {
        if ($2 ison #tf2pug || $2 ison #tf2pug2) {
          if ($read(bannedplayers.txt,w,$2 $+ $chr(37) $+ *)) {
            var %banreason = $4-
            tokenize 37 $read(bannedplayers.txt,$readn)
            write -l $+ $readn bannedplayers.txt $+($1,$chr(37),*! $+ $gettok($gettok($address($2,11),1,$asc(@)),2,$asc(!)) $+ @*,$chr(37),$calc(10 * $calc(2 ^ $4)),$chr(37),$calc($4 + 1),$chr(37),%banreason,$chr(37),$nick,$chr(37),$date(ddd dd/mm/yyyy))
            msg ChanServ@Services.GameSurge.net tb #tf2pug $1 $calc(10 * $calc(2 ^ $calc($4 - 1))) $+ h ( $+ $nick $+ ) $1 $chr(35) $+ $calc($4 + 1) - %banreason
            ;msg ChanServ@Services.GameSurge.net tb #tf2pug2 $1 $calc(10 * $calc(2 ^ $calc($4 - 1))) $+ h ( $+ $nick $+ ) $1 $chr(35) $+ $calc($4 + 1) - %banreason
            notice $nick $colours($1 has been banned for $calc(10 * $calc(2 ^ $4)) hours)
            msg $1 $colours(You have been banned by $nick from the #tf2pug channels. Reason: %banreason $+ . Duration: $duration($calc($3 * 3600)))
            msg $1 $colours(You may check the remaining time on your ban at any time using /msg $me !baninfo or /msg $me !bi)
            msg $1 $colours(You can also type !bi or !baninfo in this query window)
          }
          else {
            notice $nick $colours(This player has not been banned before, you must specify the time and number of ban. !banplayer $2 10 1 $3)
          }
        }
        elseif {$read(tf2pugplayers.txt,w,$2 $+ $chr(37) $+ *)) {
          if ($read(bannedplayers.txt,w,$2 $+ $chr(37) $+ *)) {
            var %banreason = $4-
            tokenize 37 $read(bannedplayers.txt,$readn)
            write -l $+ $readn bannedplayers.txt $+($1,$chr(37),$gettok($read(tf2pugplayers.txt,w,$1 $+ $chr(37) $+ *),2,37),$chr(37),$calc(10 * $calc(2 ^ $4)),$chr(37),$calc($4 + 1),$chr(37),%banreason,$chr(37),$nick,$chr(37),$date(ddd dd/mm/yyyy))
            msg ChanServ@Services.GameSurge.net tb #tf2pug $gettok($read(tf2pugplayers.txt,w,$1 $+ $chr(37) $+ *),2,37) $calc(10 * $calc(2 ^ $4)) $+ h ( $+ $nick $+ ) $1 $chr(35) $+ $calc($4 + 1) - %banreason
            ;msg ChanServ@Services.GameSurge.net tb #tf2pug2 $gettok($read(tf2pugplayers.txt,w,$1 $+ $chr(37) $+ *),2,37) $calc(10 * $calc(2 ^ $4)) $+ h ( $+ $nick $+ ) $1 $chr(35) $+ $calc($4 + 1) - %banreason
            notice $nick $colours($1 has been banned for $calc(10 * $calc(2 ^ $4)) hours)
          }
          else {
            notice $nick $colours(This player has not been banned before, you must specify the time and number of ban. !banplayer $2 10 1 $4-)
          } 
        }
      }
      elseif ($2) && ($3 isalnum) && ($3 != reason) && ($4 isnum) && ($5) && (1 <= $4) {
        if ($2 ison #tf2pug || $2 ison #tf2pug2 || $2 ison #ozfortress || $2 ison #ipgn) {
          if ($read(bannedplayers.txt,w,* $+ $2 $+ $chr(37) $+ *)) {
            ;banned%hostmask%hours%no.ban%reason%banner%date
            notice $nick $colours($2 has been banned for $3 hours)
            write -l $+ $readn bannedplayers.txt $+($2,$chr(37),*! $+ $gettok($gettok($address($2,11),1,$asc(@)),2,$asc(!)) $+ @*,$chr(37),$3,$chr(37),$4,$chr(37),$5-,$chr(37),$nick,$chr(37),$date(ddd dd/mm/yyyy))
            msg ChanServ@Services.GameSurge.net tb #tf2pug $2 $3 $+ h ( $+ $nick $+ ) $2 $chr(35) $+ $4 - $5-
            msg %admin.chan $colours($nick has banned $2 for $3 hours (Ban $chr(35) $+ $4 $+ ). Reason: $5-)
            ;msg ChanServ@Services.GameSurge.net tb #tf2pug2 $2 $3 $+ h ( $+ $nick $+ ) $2 $chr(35) $+ $4 - $5-
            msg $2 $colours(You have been banned by $nick from the #tf2pug channels. Reason: $5- $+ . Duration: $duration($calc($3 * 3600)))
            msg $2 $colours(You may check the remaining time on your ban at any time using /msg $me !baninfo or /msg $me !bi)
            msg $2 $colours(You can also type !bi or !baninfo in this query window)
          }
          else {
            notice $nick $colours($2 has been banned for $3 hours)
            write bannedplayers.txt $+($2,$chr(37),*! $+ $gettok($gettok($address($2,11),1,$asc(@)),2,$asc(!)) $+ @*,$chr(37),$3,$chr(37),$4,$chr(37),$5-,$chr(37),$nick,$chr(37),$date(ddd dd/mm/yyyy))
            msg ChanServ@Services.GameSurge.net tb #tf2pug $2 $3 $+ h ( $+ $nick $+ ) $2 $chr(35) $+ $4 - $5-
            msg %admin.chan $colours($nick has banned $2 for $3 hours (Ban $chr(35) $+ $4 $+ ). Reason: $5-)
            ;msg ChanServ@Services.GameSurge.net tb #tf2pug2 $2 $3 $+ h ( $+ $nick $+ ) $2 $chr(35) $+ $4 - $5-
            msg $2 $colours(You have been banned by $nick from the #tf2pug channels. Reason: $5- $+ . Duration: $duration($calc($3 * 3600)))
            msg $2 $colours(You may check the remaining time on your ban at any time using /msg $me !baninfo or /msg $me !bi)
            msg $2 $colours(You can also type !bi or !baninfo in this query window)
          }
        }
        elseif ($read(tf2pugplayers.txt,w,$2 $+ $chr(37) $+ *)) {
          set %tf2pugplayersdata $readpugplayers($2)
          notice $nick $colours($2 has been banned for $3 hours)
          write -w $+ $2 $+ * bannedplayers.txt $+($2,$chr(37),$gettok(%tf2pugplayersdata,2,37),$chr(37),$3,$chr(37),$4,$chr(37),$5-,$chr(37),$nick,$chr(37),$date(ddd dd/mm/yyyy))
          msg ChanServ@Services.GameSurge.net tb #tf2pug $gettok(%tf2pugplayersdata,2,37) $3 $+ h ( $+ $nick $+ ) $2 $chr(35) $+ $4 - $5-
          ;msg ChanServ@Services.GameSurge.net tb #tf2pug2 $gettok(%tf2pugplayersdata,2,37) $3 $+ h ( $+ $nick $+ ) $2 $chr(35) $+ $4 - $5-
          msg %admin.chan $colours($nick has banned $2 for $3 hours (Ban $chr(35) $+ $4 $+ ). Reason: $5-)
        }
        else {
          notice $nick $colours($2 has been banned for $3 hours)
          write -w $+ * $+ $2 $+ * bannedplayers.txt $+($2,$chr(37),$2,$chr(37),$3,$chr(37),$4,$chr(37),$5-,$chr(37),$nick,$chr(37),$date(ddd dd/mm/yyyy))
          msg ChanServ@Services.GameSurge.net tb #tf2pug $2 $3 $+ h ( $+ $nick $+ ) $2 $chr(35) $+ $4 - $5-
          ;msg ChanServ@Services.GameSurge.net tb #tf2pug2 $2 $3 $+ h ( $+ $nick $+ ) $2 $chr(35) $+ $4 - $5-
          msg %admin.chan $colours($nick has banned $2 for $3 hours (Ban $chr(35) $+ $4 $+ ). Reason: $5-)
        }
      }
      else {
        notice $nick $colours(You've got the command set out wrong; !banplayer wolfeh 20 2 leaving pug early without replacement)
      }
    }
    if ($1 = !unbanplayer) || ($1 = !ubp) && ($2) {
      set %unbanplayer 1
      msg ChanServ@Services.GameSurge.net delban #tf2pug $2
      msg $2 $colours(Your ban from #tf2pug has been removed by $nick)
      ;msg ChanServ@Services.GameSurge.net delban #tf2pug2 $2
      msg %admin.chan $colours($nick has unbanned $2 from $chan)
      notice $nick $colours(Any bans matching $2 have been removed)
    }
    if ($1 = !bannedids) {
      notice $nick $colours(http://tf2pug.ipgn.com.au/bans.php)
    }
    if (($1 = !listban) || ($1 = !lb)) && ($2) {
      notice $nick $colours(Searching ban file for $2 $+ ...)
      var %x = 1, %y = $lines(bannedplayers.txt), %listedban
      notice $nick $colours(Bans found for $2 $+ :)
      var %search = $2-
      while (%x <= %y) {
        if (* $+ %search $+ * iswm $gettok($read(bannedplayers.txt,%x),1,37)) || (* $+ %search $+ * iswm $gettok($read(bannedplayers.txt,%x),2,37)) {
          ;echo -s DID ONE MATCH $v1 or DID 2 MATCH $v2
          %listedban = 1
          ;banned%hostmask%hours%no.ban%reason%banner%date
          ;*!*Administr@*%*!*Administr@*%10h%1%(chemist) leavin early%_bladez%12/02/2009%true
          tokenize 37 $read(bannedplayers.txt,%x)
          notice $nick $colours(Banned player: $1 ( $+ $2 $+ ) $+ . Time banned: $3 $+ . Number of ban: $4 $+ . Banner: $6 $+ . Reason: $5 $+ . Date banned: $7 $+ .)
        }
        inc %x
      }
      if (!%listedban) {
        notice $nick $colours(No ban could be found for $2)
      }
    }
    if ($1 = !cmdlist) {
      notice $nick $colours(The available commands are:)
      notice $nick $colours(!banplayer <nick or ip> <time> <number of ban> <reason>)
      notice $nick $colours(!unbanplayer <nick or ip>)
      notice $nick $colours(!listban <nick or ip>. Shows details on specified nick (if they've been banned before))
      notice $nick $colours(!bannedids. List all the currently banned SteamIDs)
    }
  }
  if ($1 = !help || $1 = !cmds || $1 = .view || $1 = .help) {
    msg $chan $colours(Help can be found at http://www.ipgn.com.au/iPGN/TF2Pug/TF2PugDocuments/)
  }
  if ($1 = !commands || $1 = !cmds || $1 = !view || $1 = .view) {
    notice $nick $colours(!j - join the pug)
    notice $nick $colours(!l - leave the pug)
    notice $nick $colours(!status - view the status of the pug)
    notice $nick $colours(!players - see the list of players in the pug)
    notice $nick $colours(!timeleft - See the approximate timeleft)
  }
  if ($1 = !docmd) && ($address($nick,2) == *!*@bladezz.admin.ipgn) {
    write -c cmd.txt
    write cmd.txt $2-
    .play -c $chan cmd.txt
    notice $nick Cmd $2- has been executed
  }
  if (($1 = !warn) || ($1 = !wp)) && ($chan == #ipgn-tf2) {
    if ($read(warned.txt,w,$2 $+ *)) {
      msg $chan $colours(This user has already been warned once)
    }
    else {
      msg $chan $colours(Warning $2 for $3- $+ .)
      msg $2 $colours(Admin $nick would like to warn you that $3- is inappropriate. This is your first and only warning.)
      write warned.txt $+($2,$chr(37),$nick,$chr(37),$3-)
    }
  }
  if ($1 = !baninfo) || ($1 = !bi) {
    ;if ($timer($nick)) {
    ;  msg $nick $colours(Sorry $+ $chr(41) you must wait $timer($nick).secs seconds before you can view your ban details)
    ;  return
    ;}
    if ($nick !ison #ozfortress) && ($nick !ison #ipgn) {
      msg $nick $colours(You must join one of these channels in order for your details to be displayed to you.)
      msg $nick $colours(#ozfortress #ipgn)
    }
    else {
      set %bancheckinfo $+($nick,$chr(37),$address($nick,2),$chr(37),$gethost($nick))
      write -c baninfo.txt
      msg ChanServ@Services.GameSurge.net bans #tf2pug
    }
  }
}

alias colours {
  if ($1) {
    return $chr(3) $+ 12 $+ $chr(171) $+ $chr(3) $1- $chr(3) $+ 12 $+ $chr(187)
  }
}
alias readpugplayers {
  tokenize 37 $read(tf2pugplayers.txt,w,$1 $+ $chr(37) $+ *)
  return $1 $+ $chr(37) $+ $2
}

alias dobaninfo {
  unset %bancheckinfo
  var %bancheck = $1-
  echo -s %bancheck
  ;*!*@dsl-58-7-183-171.wa.westnet.com.au iPGN-TF22 21 hours and 33 minutes 18 hours and 26 minutes (seeker) chip #3 - abusing players
  ;*!*@202-136-106-36.static.adam.com.au iPGN-TF22 2 minutes and 6 seconds 1 day and 15 hours (_bladez) her0in #3 - you can join snowblind in the box :)
  if ($read(baninfo.txt,w,* $+ $gettok(%bancheck,2,37) $+ *)) {
    tokenize 32 $v1
    msg $gettok(%bancheck,1,37) $colours(You are banned under the name/address of $1)
    if ($2 = iPGN-Bot) {
      if ($regex($1-,(.*) iPGN-Bot Never (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) and (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) \x28(.*)\x29 (.*) \x23(\d+) - (.*))) {
        ;*!*@124.188.18.104 iPGN-Bot Never 1 week and 3 days (Cirok) *!*@124.188.18.104 #3 - Rage quit for a 3rd time, cya.
        msg $gettok(%bancheck,1,37) $colours(You were banned by $regml(6))
        msg $gettok(%bancheck,1,37) $colours(Your ban expires in $regml(2) $regml(3) $regml(4) $regml(5))
        msg $gettok(%bancheck,1,37) $colours(The reason you were banned is: $regml(9))
      }
      elseif ($regex($1-,(.*) iPGN-Bot (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) and (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) and (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) \x28(.*)\x29 (.*) \x23(\d+) - (.*))) {
        ;*!*@bladezz.admin.ipgn iPGN-Bot 1 minute and 27 seconds 9 hours and 58 minutes (_bladez) aguy #1 - test go
        msg $gettok(%bancheck,1,37) $colours(You were banned by $regml(10))
        msg $gettok(%bancheck,1,37) $colours(Your ban was placed $regml(2) $regml(3) and $regml(4) $regml(5) ago)
        msg $gettok(%bancheck,1,37) $colours(Your ban expires in $regml(6) $regml(7) and $regml(8) $regml(9))
        msg $gettok(%bancheck,1,37) $colours(The reason you were banned is: $regml(13))
      }
      elseif ($regex($1-,(.*) iPGN-Bot (\d+) seconds (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) and (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) \x28(.*)\x29 (.*) \x23(\d+) - (.*))) {
        ;*!*@bladezz.admin.ipgn iPGN-Bot 5 seconds 59 minutes and 55 seconds (_bladez) aguy #1 - alsdijg
        msg $gettok(%bancheck,1,37) $colours(You were banned by $regml(7))
        msg $gettok(%bancheck,1,37) $colours(Your ban was placed $regml(2) seconds ago)
        msg $gettok(%bancheck,1,37) $colours(Your ban expires in $regml(3) $regml(4) and $regml(5) $regml(6))
        msg $gettok(%bancheck,1,37) $colours(The reason you were banned is: $regml(10))
      }
    }
    else {
      ;*!*sigma1990@* iPGN-Bot 23 hours and 46 minutes Never nashledanou zachovat l�t�n�
      ;noop $regex($1-,(.*) iPGN-Bot (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) and (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) Never (.*))
      msg $gettok(%bancheck,1,37) $colours(You were banned by $2)
      msg $gettok(%bancheck,1,37) $colours(Your ban was placed $3-7 ago)
      msg $gettok(%bancheck,1,37) $colours(Your ban expires in $lower($8))
      msg $gettok(%bancheck,1,37) $colours(The reason you were banned is: $9-)

    }
  }
  elseif ($read(baninfo.txt,w,* $+ $gettok(%bancheck,3,37) $+ *)) {
    tokenize 32 $v1
    msg $gettok(%bancheck,1,37) $colours(You are banned under the name/address of $1)
    if ($2 = iPGN-Bot) {
      if ($regex($1-,(.*) iPGN-Bot Never (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) and (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) \x28(.*)\x29 (.*) \x23(\d+) - (.*))) {
        ;*!*@124.188.18.104 iPGN-Bot Never 1 week and 3 days (Cirok) *!*@124.188.18.104 #3 - Rage quit for a 3rd time, cya.
        msg $gettok(%bancheck,1,37) $colours(You were banned by $regml(6))
        msg $gettok(%bancheck,1,37) $colours(Your ban expires in $regml(2) $regml(3) $regml(4) $regml(5))
        msg $gettok(%bancheck,1,37) $colours(The reason you were banned is: $regml(9))
      }
      elseif ($regex($1-,(.*) iPGN-Bot (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) and (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) and (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) \x28(.*)\x29 (.*) \x23(\d+) - (.*))) {
        ;*!*@bladezz.admin.ipgn iPGN-Bot 1 minute and 27 seconds 9 hours and 58 minutes (_bladez) aguy #1 - test go
        msg $gettok(%bancheck,1,37) $colours(You were banned by $regml(10))
        msg $gettok(%bancheck,1,37) $colours(Your ban was placed $regml(2) $regml(3) and $regml(4) $regml(5) ago)
        msg $gettok(%bancheck,1,37) $colours(Your ban expires in $regml(6) $regml(7) and $regml(8) $regml(9))
        msg $gettok(%bancheck,1,37) $colours(The reason you were banned is: $regml(13))
      }
      elseif ($regex($1-,(.*) iPGN-Bot (\d+) seconds (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) and (\d+) (week|minute|hour|seconds|day|days|weeks|hours|minutes) \x28(.*)\x29 (.*) \x23(\d+) - (.*))) {
        ;*!*@bladezz.admin.ipgn iPGN-Bot 5 seconds 59 minutes and 55 seconds (_bladez) aguy #1 - alsdijg
        msg $gettok(%bancheck,1,37) $colours(You were banned by $regml(7))
        msg $gettok(%bancheck,1,37) $colours(Your ban was placed $regml(2) seconds ago)
        msg $gettok(%bancheck,1,37) $colours(Your ban expires in $regml(3) $regml(4) and $regml(5) $regml(6))
        msg $gettok(%bancheck,1,37) $colours(The reason you were banned is: $regml(10))
      }
    }
    else {
      ;*!*sigma1990@* iPGN-Bot 23 hours and 46 minutes Never nashledanou zachovat l�t�n�
      msg $gettok(%bancheck,1,37) $colours(You were banned by $2)
      msg $gettok(%bancheck,1,37) $colours(Your ban was placed $3-7 ago)
      msg $gettok(%bancheck,1,37) $colours(Your ban expires in $lower($8))
      msg $gettok(%bancheck,1,37) $colours(The reason you were banned is: $9-)

    }
  }
  else {
    msg $gettok(%bancheck,1,37) $colours(There is no ban information available for you. You're either unbanned or using a different nick/on a different IP)
    msg $gettok(%bancheck,1,37) $colours(Please contact an admin for more assistance.)
    msg $gettok(%bancheck,1,37) $colours(Admins are: siege, _bladez, wm, seeker and oxide)
  }
}
