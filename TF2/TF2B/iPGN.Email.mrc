on *:START:{
  window -e @mail
}

alias sendmail {
  if ((!$1) || ($numtok($1-,37) < 3)) {
    echo -a Syntax: sendmail to%subject%message
    return
  }

  var %sock = mail. [ $+ [ $ticks ] ]
  set % [ $+ [ %sock ] $+ .server ] mailrelay.primusdatacentre.com.au
  set % [ $+ [ %sock ] $+ .to ] $gettok($1-,1,37)
  set % [ $+ [ %sock ] $+ .from ] teamfortress@ipgn.com.au
  set % [ $+ [ %sock ] $+ .fromname ] iPGN-TF2
  set % [ $+ [ %sock ] $+ .subject ] $gettok($1-,2,37)
  set % [ $+ [ %sock ] $+ .message ] $gettok($1-,3,37)

  sockopen %sock % [ $+ [ %sock ] $+ .server ] 25
}

on *:sockopen:mail.*:{
  if ($sockerr) {
    echo @mail 4Socket error $sock($sockname).wserr on sockopen. Ending transmission!
    sockclose $sockname
    return
  }
  else {
    sockwrite -n $sockname HELO ipgnwin200301.ipgn.com.au
  }
}

on *:sockread:mail.*:{
  var %data

  :nextread
  sockread %data

  if ($sockbr == 0) {
    return
  }
  if (%data) { echo @mail %data }

  if (%data == 250 % [ $+ [ $sockname ] $+ .server ]) {
    set % [ $+ [ $sockname ] $+ .hellod ] 1
    sockwrite -n $sockname MAIL from: % [ $+ [ $sockname ] $+ .from ]
  }

  goto nextread
}

on *:sockwrite:mail.*:{
  ;echo @mail 11Queue empty. Ready for next send
  if (% [ $+ [ $sockname ] $+ .hellod ]) && (!% [ $+ [ $sockname ] $+ .sent ]) {
    set % [ $+ [ $sockname ] $+ .sent ] 1
    ;we've said our hellos, and input our MAIL from header
    ;now we send the rest of the email shit

    echo @mail 7We've hellod but havent sent our mail yet. Sending mail now

    var %sw = sockwrite -n $sockname
    var %x = 1, %y = $numtok(% [ $+ [ $sockname ] $+ .to ],59), %tostring
    while (%x <= %y) {
      %sw RCPT to: $gettok(% [ $+ [ $sockname ] $+ .to ],%x,59)
      echo @mail Adding TO header: RCPT to: $gettok(% [ $+ [ $sockname ] $+ .to ],%x,59)

      %tostring = $addtok(%tostring,$gettok(% [ $+ [ $sockname ] $+ .to ],%x,59),44)

      inc %x
    }

    %tostring = $replace(%tostring,$chr(44),$chr(44) $+ $chr(32))
    ;now we've got all of our TOs, we need to send the data!
    %sw DATA
    echo @mail From: % [ $+ [ $sockname ] $+ .from ]
    %sw From: % [ $+ [ $sockname ] $+ .from ]
    echo @mail To: %tostring
    %sw To: %tostring
    echo @mail Subject: % [ $+ [ $sockname ] $+ .subject ]
    %sw Subject: % [ $+ [ $sockname ] $+ .subject ]
    %sw
    echo @mail Message: % [ $+ [ $sockname ] $+ .message ]
    %sw % [ $+ [ $sockname ] $+ .message ]
    %sw .
    %sw QUIT
  }
}

on *:sockclose:mail.*:{
  echo @mail 4Socket $sockname closed by remote host
  if ($sock($sockname).wserr) {
    echo @mail 9Socket had error $v1
  }

  unset % [ $+ [ $sockname ] $+ .* ]
}
