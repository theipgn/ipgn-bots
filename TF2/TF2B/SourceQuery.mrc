;PUT SOURCE ENGINE QUERY SHIT HERE :D
on *:START:{
  window -e @squery
}
alias sourceQueryTimer {
  echo @squery SOURCE QUERY TIMER ENGAGED!
  if (% [ $+ [ $1 ] $+ .a2s_info ]) && (% [ $+ [ $1 ] $+ .a2s_player ]) {
    msg % [ $+ [ $1 ] ] $colours(Query successful!)
    unset % [ $+ [ $1 ] $+ * ]
    sockclose $1
  }
  elseif (% [ $+ [ $1 ] $+ .a2s_player ]) && (!% [ $+ [ $1 ] $+ .a2s_info ]) {
    sourceQuery $1 % [ $+ [ $1 ] ] % [ $+ [ $1 ] $+ .ip ] % [ $+ [ $1 ] $+ .port ] a2s_info
  }
  elseif (!% [ $+ [ $1 ] $+ .a2s_player ]) && (% [ $+ [ $1 ] $+ .a2s_info ]) {
    sourceQuery $1 % [ $+ [ $1 ] ] % [ $+ [ $1 ] $+ .ip ] % [ $+ [ $1 ] $+ .port ] a2s_player
  }
  else {
    msg % [ $+ [ $1 ] ] $colours(Some sort of error occured and the server could not be queried)
    unset % [ $+ [ $1 ] $+ * ]
    sockclose $1
  }
}

;sourceQuery squery. [ $+ [ $ticks ] ] $chan ip port
alias sourceQuery {
  set % [ $+ [ $1 ] ] $2
  set % [ $+ [ $1 ] $+ .ip ] $3
  set % [ $+ [ $1 ] $+ .port ] $4
  bset -t &a2s_info 1 $str($chr(255),4) $+ TSource Engine Query
  bset &a2s_info $calc($bvar(&a2s_info,0) + 1) 0
  bset -t &a2s_player 1 $str($chr(255),4)
  bset &a2s_player 5 85
  bset -t &a2s_player 6 $str($chr(255),4)
  if ($5) && ($5 == a2s_info) {
    .timer 1 2 sourceQueryTimer $1
    sockudp -k $1 $3 $4 &a2s_info
  }
  elseif ($5) && ($5 == a2s_player) {
    .timer 1 2 sourceQueryTimer $1
    sockudp -k $1 $3 $4 &a2s_player
  }
  else {
    .timer 1 2 sourceQueryTimer $1
    sockudp -k $1 $3 $4 &a2s_info
    sockudp -k $1 $3 $4 &a2s_player
  }
}

on *:udpread:squery.*:{
  var &squery.data, %squery.data
  sockread &squery.data
  var %responsetype = $bvar(&squery.data,5)
  echo @squery RESPONSE TYPE: %responsetype
  if (%responsetype == $hextodec(49)) {
    echo @squery A2S_INFO RESPONSE RECEIVED
    bset &a2s_info 1 $bvar(&squery.data,7-)
    echo @squery $bvar(&a2s_info,1-)
    var %x = 1, %y = $bvar(&a2s_info,0), %z = 1, %hostname, %map, %gametype, %cplayers, %mplayers, %nbots, %stype, %sos, %spass, %svac, %sver
    while (%x <= %y) {
      var %data = $bvar(&a2s_info,%x,100).text
      if (%z == 1) {
        echo @squery HOSTNAME: %data
        %hostname = %data
        %x = $calc(%x + $len(%data) + 1)
      }
      elseif (%z == 2) {
        echo @squery MAP: %data
        %map = %data
        %x = $calc(%x + $len(%data) + 1)
      }
      elseif (%z == 4) {
        echo @squery GAME: %data
        %gametype = %data
        %x = $calc(%x + $len(%data) + 1)
      }
      elseif (%z == 5) { 
        echo @squery NUMBER OF PLAYERS: $bvar(&a2s_info,$calc(%x + 2))
        %cplayers = $bvar(&a2s_info,$calc(%x + 2))
        echo @squery MAX PLAYERS: $bvar(&a2s_info,$calc(%x + 3))
        %mplayers = $bvar(&a2s_info,$calc(%x + 3))
        echo @squery NUMBER OF BOTS: $bvar(&a2s_info,$calc(%x + 4))
        %nbots = $bvar(&a2s_info,$calc(%x + 4))
        inc %x 5
      }
      elseif (%z == 6) {
        echo @squery SERVER TYPE: $bvar(&a2s_info,%x)
        %stype = $iif($chr($bvar(&a2s_info,%x)) = d,Dedicated,$bvar(&a2s_info,%x))
        echo @squery SERVER OS: $bvar(&a2s_info,$calc(%x + 1))
        %sos = $iif($chr($bvar(&a2s_info,$calc(%x + 1))) = l,Linux,Windows)
        inc %x 2
      }
      elseif (%z == 7) {
        echo @squery PASSWORDED: $bvar(&a2s_info,%x)
        %spass = $iif($bvar(&a2s_info,%x),Yes,No)
        echo @squery VAC SECURE: $bvar(&a2s_info,$calc(%x + 1))
        %svac = $iif($bvar(&a2s_info,$calc(%x + 1)),Yes,No)
        inc %x 2
      }
      else {
        %x = $calc(%x + $len(%data) + 1)
        echo @squery LOOP NUM: %z DATA: %data
      }
      ;echo @squery VALUE OF X: %x
      inc %z
      if (%z > 8) { break }
    }
    msg % [ $+ [ $sockname ] ] $colours(12Hostname: %hostname ( $+ %sos %stype $+ ))
    msg % [ $+ [ $sockname ] ] $colours(12Map: %map 12Passworded: %spass 12VAC Secured: %svac)
    msg % [ $+ [ $sockname ] ] $colours(12Players: $+(%cplayers,/,%mplayers) ( $+ %nbots bot(s)))
    set % [ $+ [ $sockname ] $+ .a2s_info ] 1
  }
  if (%responsetype == $hextodec(41)) {
    echo @squery S2C_CHALLENGE RECEIVED
    bset &a2s_player_challenge 1 $bvar(&squery.data,6,9)
    bset -t &a2s_player 1 $str($chr(255),4)
    bset &a2s_player 5 85
    bset &a2s_player 6 $bvar(&a2s_player_challenge,1-)
    echo @squery $bvar(&a2s_player,1-)
    sockudp -k $sockname % [ $+ [ $sockname ] $+ .ip ] % [ $+ [ $sockname ] $+ .port ] &a2s_player
  }
  if (%responsetype == $hextodec(44)) {
    echo @squery RECEIVED a2s_player INFO
    echo @squery a2s_player: $bvar(&squery.data,1-)
    var %num_players = $bvar(&squery.data,6)
    echo @squery NUMBER OF PLAYERS: %num_players
    if (%num_players) {
      bset &a2s_playerinfo 1 $bvar(&squery.data,8-)
      ;breplace &a2s_playerinfo 0 32
      echo @squery $bvar(&a2s_playerinfo,1-)
      echo @squery LENGTH OF RESPONSE: $bvar(&a2s_playerinfo,0)
      var %x = 1
      var %player = 1
      var %y = $bvar(&a2s_playerinfo,0)
      while (%x <= %y) {
        var %name = $bvar(&a2s_playerinfo,%x,100).text
        var %kills = $bvar(&a2s_playerinfo,$calc(%x + $len(%name) + 1))
        echo @squery PLAYER: %player NAME: $iif(%name,$ifmatch,SourceTV (DOESN'T ACTUALLY TAKE UP SLOTS)) KILLS: %kills
        msg % [ $+ [ $sockname ] ] $colours(12Player: %player 12Name: $iif(%name,$ifmatch,SourceTV (DOESN'T ACTUALLY TAKE UP SLOTS)) 12Kills: %kills)
        inc %player
        %x = $calc(%x + $len(%name) + 10)
      }
    }
    set % [ $+ [ $sockname ] $+ .a2s_player ] 1
  }
}

alias hextodec {
  var %x = 1, %y = $len($1), %z = 0, %decvalue = 0
  while (%x <= %y) {
    var %char = $left($right($1,%x),1)
    var %char = $iif(%char isalpha,$asc(%char),%char)
    %decvalue = $calc(%decvalue + (%char * (16 ^ %z)))

    inc %z
    inc %x
  }
  return %decvalue
}
