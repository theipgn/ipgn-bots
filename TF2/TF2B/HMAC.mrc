alias hmac {
  /*
  $hmac(hash function,key,message)
  $hmac() follows RFC2104
  Tested and passed with test cases from RFC2202

  Examples:
  (mIRC supports $md5 and $sha1 by default)

  MD5: $hmac(md5,Jefe,what do ya want for nothing?)
  (Test case 2 of HMAC-MD5 from RFC2202)
  returns: 750c783e6ab0b503eaa86e310a5db738

  SHA-1: $hmac(sha1,Jefe,what do ya want for nothing?)
  (Test vector 2 of HMAC-SHA-1 from RFC2202)
  returns: effcdf6ae5eb2fa2d27416d5f184df9c259a7c79


  Extending:

  $hmac() is extendable with custom cryptographic hash functions
  which support following syntax: $fname(&binvar,1)
  Where the name of the binary variable becomes $1 and
  $2 becomes 1 when using a binary variable based on mIRC's
  default available cryptographic hash functions like $md5 and $sha1.
  $fname is then usable like $md5 or $sha1 in $hmac:
  $hmac(fname,key,message)
  */

  if (!$1) || ($2 == $null) || ($3 == $null) { return }
  bunset &key &h1 &h1_hash &h2

  if ($len($2) > 64) { bset &key 1 $regsubex($($+($,$1,$chr(40),$2,$chr(41)),2),/(..)/g,$base(\1,16,10) $+ $chr(32)) }
  else { bset -t &key 1 $2 }

  var %x = 1
  while (%x <= 64) {
    var %c = $iif($bvar(&key,%x),$v1,0)
    bset &h1 %x $xor(54,%c)
    bset &h2 %x $xor(92,%c)
    inc %x
  }
  bset -t &h1 $calc($bvar(&h1,0)+1) $3
  bset &h1_hash 1 $regsubex($($+($,$1,$chr(40),&h1,$chr(44),1,$chr(41)),2),/(..)/g,$base(\1,16,10) $+ $chr(32))
  bset &h2 $calc($bvar(&h2,0)+1) $bvar(&h1_hash,1-)
  return $($+($,$1,$chr(40),&h2,$chr(44),1,$chr(41)),2)
}
