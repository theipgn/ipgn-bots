alias find {
  ;msg %pug.adminchan $codes(Starting search for $2)
  echo @baninfo $colours(Starting search for $2)
  write -c list.txt
  var %numfiles = $findfile(C:\TF2Pug\connections,*.txt,0) 
  var %count = 0
  var %search = $1

  var %x = $findfile(C:\TF2Pug\connections,*.txt,0)
  while (%x >= 1) {
    write list.txt $findfile(C:\TF2Pug\connections,*.txt,%x) $ctime($remove($findfile(C:\TF2Pug\connections,*.txt,%x),C:\TF2Pug\connections,.txt))
    dec %x
  }
  filter -ffceut 2 32 list.txt list.txt

  var %x = 1
  while (%x <= $lines(list.txt)) {
    if (%count > 10) { break }
    tokenize 32 $read(list.txt,%x)
    var %tmp = $read($1,w,* $+ %search $+ *)
    var %game = $read(list.txt,%x)
    if (%tmp) { 
      ;msg %pug.adminchan $codes(%tmp)
      echo @baninfo $colours(%tmp Game $chr(35) $+ $remove($gettok(C:\TF2Pug\connections\game873.txt,4,92),game,.txt))
      ;echo @baninfo Game $chr(35) $+ $remove($gettok(C:\TF2Pug\connections\game873.txt,4,92),game,.txt)
      inc %count 
    }
    inc %x
  }
  ;msg %pug.adminchan $codes(Search complete)
  echo @baninfo Search complete
}
