on *:CONNECT:{
  ; .timer 1 30 join %pug.chan
}

on *:NOTICE:*:*:{
  if (*I recognize you* iswm $1-) {
    .timer 1 10 join %pug.chan
  }
}
alias codes {
  return 12� $1- 12�
}

alias newtopic {
  if ($1) {
    set %status $1-
  }
  .topic %acfpug.chan 12,0� 1i14,0PGN TF2 Pug! 1,0http://www.12ipgn1.com.au/ 12�1 N14ews: %news 12�1 S14tatus: %status 12�'
}
alias code1 {
  return
}

alias setteams {
  set %full 1
  msg %pug.chan $codes(The game is now full. In-game admin: %acfpug.starter $+ . Players please wait for your details.)
  .timer 1 3 massdetails
}

alias massdetails {
  .timertimeout off
  set %detailed 1
  var %v = %acfpug.limit
  var %u = 1
  while (%u <= %v) {
    %nick = $gettok(%acfplayers,%u,32)
    if (%nick = %acfpug.starter) || (%nick isin %admins.list || $nick isop %acfpug.chan) {
      notice %nick $codes(Server details: connect $+(%acf.ip,:,%acf.port,;) password %serverpass)
    }
    else {
      notice %nick $codes(Server details: connect $+(%acf.ip,:,%acf.port,;) password %serverpass)
    }
    inc %u
  }
}

alias details {
  if (!%busy) && (!%acfmap.vote) {
    if ($1 isin %admins.list) {
      notice $1 $codes(Server details: connect $+(%acf.ip,:,%acf.port,;) password %serverpass)
      halt
    }
    if ($1 = %acfpug.starter) {
      notice $1 $codes(Server details: connect $+(%acf.ip,:,%acf.port,;) password %serverpass)
      halt
    }
    if ($istok(%acfplayers,$1,32)) {
      notice $1 $codes(Server details: connect $+(%acf.ip,:,%acf.port,;) password %serverpass)
      halt
    }
  }
  if (!%busy) && ($1 isin %admins.list) {
    notice $1 $codes(Server details: connect $+(%acf.ip,:,%acf.port,;) password %serverpass)
    halt
  }
}

alias endpug {
  .timer* off
  newtopic No pug in progress. Type !pug or !pug <map> to start one.
  unset %acfpug %acfpug.map %acfpug.mapvote %acfplayers %acfpug.starter %acfpug.admin %acffull %acfdetailed
}

on *:NICK:{
  if ($istok(%acfplayers,$nick,32)) {
    %acfplayers = $reptok(%acfplayers,$nick,$newnick,32)
  }
  if ($nick = %acfpug.starter) {
    %acfpug.starter = $newnick
  }
}

alias timeleft {
  if ($timer(timeleft)) {
    return Approximately $round($calc($timer(timeleft).secs / 60),0) minutes remaining.
  }
}

alias endpugtimeout {
  msg %acfpug.chan $codes(Pug timed out)
  endpug
}

alias startmapvote {
  newtopic Pug is now full. Map voting in progress.
  %acfmaps.list = $replace(%acfmaps,$chr(59),$chr(32) $+ - $+ $chr(32))
  msg %acfpug.chan $codes(Vote for a map using !map <map>. eg. !map cp_well. Available maps: %acfmaps.list)
  set %acfmap.vote 1
  endmapvote
}

alias votecount {
  if (!%acfmap. [ $+ [ $1 ] ]) { return (0) }
  if (%acfmap. [ $+ [ $1 ] ] < 0) { 
    unset %acfmap. [ $+ [ $1 ] ]
    return (0) 
  }
  else { return ( $+ %acfmap. [ $+ [ $1 ] ] $+ ) }
}

alias endmapvote {
  unset %acfmap.high %acfmap.win %acfmap.count
  var %x = 1
  %acfmap.count = $numtok(%maps,59)
  while (%x <= %acfmap.count) { 
    if (%acfmap. [ $+ [ $gettok(%maps,%x,59) ] ]) {
      if (!%acfmap.high) { 
        %acfmap.high = %acfmap. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %acfmap.win = $gettok(%maps,%x,59) 
      }
      if (%acfmap. [ $+ [ $gettok(%maps,%x,59) ] ] > %acfmap.high) {
        %acfmap.high = %acfmap. [ $+ [ $gettok(%maps,%x,59) ] ] 
        %acfmap.win = $gettok(%maps,%x,59)
      }
    }
    inc %x
  }
  msg %acfpug.chan $codes( $+ %acfmap.win won the count with %acfmap.high votes!)
  .timer 1 2 newtopic Pug is now in-game on %acfwin.map.
  set %acfwin.map %acfmap.win
  unset %acfmap.*
  .timer 1 4 setteams
}

alias novotepug {
  set %acfwin.map %acfpug.map
  newtopic Pug is now in-game on %acfpug.map
  .timer 1 2 setteams
}

on *:TEXT:!*:%acfpug.chan:{
  if ($1 = !reset) && ($nick isin %admins.list || $nick isop $chan) {
    endpug
    unset %busy
    halt
  }
  if ($1 = !enable) && ($nick isin %admins.list || $nick isop $chan) {
    endpug
    unset %busy
    halt
  }
  if ($1 = !news) && ($nick isin %admins.list || $nick isop $chan) {
    set %news $2-
    newtopic
  }
  if ($1 = !disable) && ($nick isin %admins.list || $nick isop $chan) {
    set %busy 1
    newtopic Disabled.
  }
  if ($1 = !vote) || ($1 = !map) {
    if ($istok(%acfplayers,$nick,32)) && (%acfmap.vote) {
      if ($istok(%maps,$2,59)) {
        if (%acfmap. [ $+ [ $nick ] ] = $2) { halt }
        if (%acfmap. [ $+ [ $nick ] ]) && (%acfmap. [ $+ [ $nick ] ] != $2) {
          inc %acfmap. [ $+ [ $2 ] ]
          dec %acfmap. [ $+ [ %acfmap. [ $+ [ $nick ] ] ] ]
          msg %acfpug.chan $codes($nick changed vote from %acfmap. [ $+ [ $nick ] ] $+  $votecount(%acfmap. [ $+ [ $nick ] ]) to $2 $+  $votecount($2))
          %acfmap. [ $+ [ $nick ] ] = $2
          halt
        }
        else {
          inc %acfmap. [ $+ [ $2 ] ]
          msg %acfpug.chan $codes($nick voted for $2 $+  $votecount($2))
          %acfmap. [ $+ [ $nick ] ] = $2
        }      
      }
    }
  }
  if (%busy) { halt }
  if ($1 = !pug) && ($nick isop $chan) {
    if (!%pug) && (!$2) {
      set %acfpug.mapvote 1
      set %acfpug.starter $nick
      set %acfpug.limit 12
      %acfplayers = $addtok(%acfplayers,$nick,32)
      newtopic Pug started by $nick $+ . Type !j to play.
      notice %acfpug.chan $codes(%acfpug.limit player pug started by $nick $+ . Type !j to play.)
      msg %acfpug.chan $codes($nick has joined the pug $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)))
      set %pug 1
      halt
    }
    if (!%pug) && ($2 isin %acfmaps) {
      set %acfpug.map $2
      set %acfpug.starter $nick
      set %acfpug.limit 12
      %acfplayers = $addtok(%acfplayers,$nick,32
      newtopic $2 pug started by $nick $+ . Type !j to play.
      notice %acfpug.chan $codes($2 pug started by $nick $+ . Type !j to play.)
      msg %acfpug.chan $codes($nick has joined the pug $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)))
      set %pug 1
      halt
    }
    if (!%pug) && ($2 !isin %maps) {
      %maps.list = $replace(%maps,$chr(59),$chr(32) $+ - $+ $chr(32))
      msg %acfpug.chan $codes( $+ $2 $+  is not a valid map)
      msg %acfpug.chan $codes(You need to specify a map in the maps list: %maps.list)
    }
    if (%pug) {
      halt
    }
  }
  if ($1 = !endpug) {
    if (!%pug) {
      msg %acfpug.chan $codes(No pug in progress. Type !pug or !pug <map> to start one.)
      halt
    }
    if (%pug) && ($nick = %acfpug.starter) {
      endpug
      halt
    }
    if ($nick isin %admins.list || $nick isop $chan) { 
      endpug
      halt
    }
  }
  if ($1 = !me) || ($1 = !join) || ($1 = !j) {
    if ($istok(%acfplayers,$nick,32)) {
      halt
    }
    if (%pug) {
      if (%full) {
        halt 
      }
      %acfplayers = $addtok(%acfplayers,$nick,32)
      msg %acfpug.chan $codes($nick has joined the pug $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)))
      if ($numtok(%acfplayers,32) = %acfpug.limit) {
        set %full 1
        if (%acfpug.mapvote) {
          .timer 1 4 startmapvote
          .timertimeout off
        }
        if (!%acfpug.mapvote) {
          .timer 1 4 novotepug
          .timertimeout off
        }
      }
      halt
    }
  }
  if ($1 = !addmap) && ($2) && ($nick isop $chan) {
    if ($istok(%maps,$2,59)) {
      msg %acfpug.chan $codes($2 is already on the maps list)
      halt
    }
    else {
      msg %acfpug.chan $codes($2 has been added to the maps list)
      %maps = $addtok(%maps,$2,59)
    }
  }
  if ($1 = !removemap) && ($2) && ($nick isop $chan) {
    if (!$istok(%maps,$2,59)) {
      msg %acfpug.chan $codes($2 is not on the maps list)
      halt
    }
    else {
      %maps = $remtok(%maps,$2,59)
      msg %acfpug.chan $codes($2 has been removed from the maps list)
    }
  }
  if ($1 = !delme) || ($1 = !leave) || ($1 = !l) {
    if (!$istok(%acfplayers,$nick,32)) {
      halt
    }
    if (%pug) {
      if (%full) {
        halt 
      }
      if ($nick = %acfpug.starter) {
        if ($numtok(%acfplayers,32) = 1) { 
          endpug
          halt
        }
        %acfplayers = $remtok(%acfplayers,$nick,32)
        %acfpug.starter = $gettok(%acfplayers,1,32)
        msg %acfpug.chan $codes($nick has left the pug $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)))
        msg %acfpug.chan $codes(%acfpug.starter is now admin.)
        halt
      }
      else {      
        %acfplayers = $remtok(%acfplayers,$nick,32)
        msg %acfpug.chan $codes($nick has left the pug $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)))
      }
      halt
    }
    if (!%pug) {
      msg %acfpug.chan $codes(No pug in progress. Type !pug or !pug <map> to start one.)
      halt
    }
  }
  if ($1 = !players) {
    if (!%pug) {
      msg %acfpug.chan $codes(No pug in progress. Type !pug or !pug <map> to start one.)
      halt
    }
    if (%pug)  {
      msg %acfpug.chan $codes(Players: %acfplayers $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)))
      halt
    }
  }
  if ($1 = !details) {
    if (%busy) { halt }
    if (!%full) {
      notice $nick $codes(Pug is not full yet)
      if ($nick isin %admins.list || $nick isop $chan) {
        details $nick
      }
      halt
    }
    if (%full) {
      details $nick
    }
  }

  if ($1 = !status) {
    if (!%pug) {
      msg %acfpug.chan $codes(Status: No pug in progress. Type !pug 6 to start one)
    }
    if (%pug) && (!%full) {
      msg %acfpug.chan $codes(Status: Gathering players)
    }
    if (%pug) && (%full) && (%acfpug.mapvote) {
      msg %acfpug.chan $codes(Status: In-game on %acfwin.map)
    }
    if (%pug) && (%full) && (!%acfpug.mapvote) {
      msg %acfpug.chan $codes(Status: In-game on %acfpug.map)
    }
  }
  if ($1 = !commands) {
    if ($nick isop $chan) {
      notice $nick $codes(!addmap <map> - Adds a map to the map list)
      notice $nick $codes(!removemap <map> - Removes a map from the map list)
      notice $nick $codes(!serverpass <pass> - PM this to the bot)
      notice $nick $codes(!newmap <map> - Forces the pug to a different map)

    }
    else {
      notice $nick $codes(The commands are:)
      notice $nick $codes(!join / !j / !me - Signs you up to the pug)
      notice $nick $codes(!l / !delme / !leave - Removes you from the pug)
      notice $nick $codes(!map <map> - Lets you vote for a map during the map voting)
    }
  }
  if ($1 = !newmap) && ($nick isop $chan || $nick = %acfpug.starter) && ($2) {
    if (%pug) {
      if ($2 isin %maps) {
        if (%acfpug.mapvote) {
          msg %acfpug.chan $codes(Map has been forced to $2, no longer voting when full)
          set %acfpug.map $2
          unset %acfpug.mapvote
        }
        if (!%acfpug.mapvote) {
          msg %acfpug.chan $codes(Map has been changed to $2)
          set %acfpug.map $2
        }
      }
      if ($2 !isin %maps) {
        msg %acfpug.chan $codes( $+ $2 $+  is not a valid map. You must specify a map in the list:)
        msg %acfpug.chan $codes(%maps.list)
      }
    }
    else {
      msg %acfpug.chan $codes(You can't change a map without the pug being started first!)
    }
  }
  if ($1 = !maps) {
    %acfmaps.list = $replace(%acfmaps,$chr(59),$chr(32) $+ - $+ $chr(32))
    msg %acfpug.chan $codes(Maps currently in the list are: %acfmaps.list)
  }
}

on *:TEXT:*:?:{
  if ($nick ison %acfpug.chan) && ($nick isop %acfpug.chan) {
    if ($1 = !serverpass) {
      set %acfserverpass $2
      msg $nick $colours(Server password has been changed to $2)
    }
  }
}

on *:PART:%acfpug.chan:{
  if (!$istok(%acfplayers,$nick,32)) {
    halt
  }
  if (%pug) {
    if (%full) {
      halt 
    }
    if ($nick = %acfpug.starter) {
      if ($numtok(%acfplayers,32) = 1) { 
        endpug
        halt
      }
      if ($numtok(%acfplayers,32) > 1) {
        %acfplayers = $remtok(%acfplayers,$nick,32)
        %acfpug.starter = $gettok(%acfplayers,1,32)
        msg %acfpug.chan $codes($nick has left the pug $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)) $+ . %acfpug.starter is now admin.)
        halt
      }
    }
    else {      
      %acfplayers = $remtok(%acfplayers,$nick,32)
      msg %acfpug.chan $codes($nick has left the pug $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)))
      halt
    }
  }
}


on *:QUIT:{
  if (!$istok(%acfplayers,$nick,32)) {
    halt
  }
  if (%pug) {
    if (%full) {
      halt 
    }
    if ($nick = %acfpug.starter) {
      if ($numtok(%acfplayers,32) = 1) { 
        msg %acfpug.chan $codes($nick has left the pug $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)))
        endpug
        halt
      }
      %acfplayers = $remtok(%acfplayers,$nick,32)
      %acfpug.starter = $gettok(%acfplayers,1,32)
      msg %acfpug.chan $codes($nick has left the pug $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)))
      msg %acfpug.chan $codes(%acfpug.starter is now admin.)
      halt
    }
    else {      
      %acfplayers = $remtok(%acfplayers,$nick,32)
      msg %acfpug.chan $codes($nick has left the pug $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)))
    }
    halt
  }
}

on *:KICK:%acfpug.chan:{
  if (!$istok(%acfplayers,$knick,32)) {
    halt
  }
  if (%pug) {
    if (%full) || (%busy) {
      halt 
    }
    if ($knick = %acfpug.starter) {
      if ($numtok(%acfplayers,32) = 1) { 
        endpug
        halt
      }
      if ($numtok(%acfplayers,32) > 1) {
        %acfplayers = $remtok(%acfplayers,$knick,32)
        %acfpug.starter = $gettok(%acfplayers,1,32)
        msg %acfpug.chan $codes($knick has left the pug $+($chr(40),$numtok(%acfplayers,32),/,%acfpug.limit,$chr(41)) $+ . %acfpug.starter is now admin.)
        halt
      }
    }
    else {      
      %acfplayers = $remtok(%acfplayers,$knick,32)
      msg %acfpug.chan $codes($knick has left the pug $+($chr(40),$calc(%acfpug.limit - $numtok(%acfplayers,32)),/,%acfpug.limit,$chr(32),slots remaining,$chr(41)))
      halt
    }
  }
}
