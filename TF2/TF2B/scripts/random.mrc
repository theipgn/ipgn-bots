on *:START:{
  server 127.0.0.1 11112 tf2:h1tl3r -i [iPGN-TF2]
  ;server -m 127.0.0.1 11112 tf2-ozfslack:h1tl3r -i ipgnbot
  server -m ozfortress.irc.slack.com 6667 ozfortress.qiCrztOt2bpLvEliNBBq -i ipgnbot
}

alias regtest {
  ; sourcetv: port 27016, delay 90.0s
  noop $regex($1-,/sourcetv\S\s+port\s+(\d+)\s+delay\s+(\d+)\S+/i)
  return 1: $regml(1) 2: $regml(2) 3: $regml(3) 4: $regml(4) 5: $regml(5)
}

on *:START:{
  window -e @baninfo
}

on *:CONNECT:{
  unset %get.relay
  ;.timer 0 600 close -m
  .timerpvhud 0 1800 pvhudcheck
}

on *:NOTICE:*:#tf2pug*:{
  if ($address($nick,2) == *!*@iPGN-Bot.bot.ipgn) && (12 player pug started isin $1-) { 
    if ($chan == #tf2pug) {
      msg #ozfortress $colours(A 12 player pug has been started in $clr(blue) $+ $chan $+ $clr by7 $remove($7,.) $+ . $+ $clr Type 12!j in $clr(blue) $+ $chan $+ $clr to play)
      msg #ipgn $colours(A 12 player pug has been started in $clr(blue) $+ $chan $+ $clr by7 $remove($7,.) $+ . $+ $clr Type 12!j in $clr(blue) $+ $chan $+ $clr to play)
    }
    ;if ($chan == #tf2pug2) {
    ;  msg #ozfortress $colours(A 12 player pug has been started in $clr(blue) $+ $chan $+ $clr by $clr(blue) $+ $7 $+ $clr Join up!)
    ;  msg #tf2pug $colours(A 12 player pug has been started in $clr(blue) $+ $chan $+ $clr by $clr(blue) $+ $7 $+ $clr Join up!)
    ;  msg #ipgn $colours(A 12 player pug has been started in $clr(blue) $+ $chan $+ $clr by $clr(blue) $+ $7 $+ $clr Join up!)
    ;  msg $chan 14,0Don't forget to come join us in 1#ipgn 14for fun times
    ;}
  } 
}

on *:TEXT:*:#tf2pug*:{
  if ($address($nick,2) == *!*@iPGN-Bot.bot.ipgn) && (Requesting player isin $1-) {
    if (!%getrelay) {
      set -u300 %getrelay 1
      msg #ozfortress $colours(12 $+ $chan needs another player! The details are: $8 $9 $10)
      msg #ipgn $colours(12 $+ $chan needs another player! The details are: $8 $9 $10)
      msg #nzfortress $colours(12 $+ $chan needs another player! The details are: $8 $9 $10)
      msg #asiafortress $colours(12 $+ $chan needs another player! The details are: $8 $9 $10)
    }
  }
  if ($chan == #tf2pug || $chan == #tf2pug2) &&  (mybrute isin $1-) && ($nick !isop $chan) {
    notice $nick $colours(Don't paste your shitty mybrute crap in this channel, thank you.)
    inc %mybrute. [ $+ [ $nick ] ]
    if (%mybrute. [ $+ [ $nick ] ] >= 6) {
      kick $chan $nick NO
      msg ChanServ@Services.GameSurge.net tb $chan $nick 5h back to your cell
      return
    }
    if (%mybrute. [ $+ [ $nick ] ] >= 3) {
      kick $chan $nick You have been terminated, back to your cell
      msg ChanServ@Services.GameSurge.net tb $chan $nick 30m back to your cell
    }
  }
  if ($chan == #tf2pug || $chan == #tf2pug2) && (live.gotgames isin $1-) && ($nick !isop $chan) {
    if (%gotgames. [ $+ [ $nick ] ] >= 1) {
      kick $chan $nick You have been warned. Contact one of the admins for assistance if need be
      msg ChanServ@Services.GameSurge.net addban $chan $nick Advertising other services. Contact admins for assistance if need be
      return
    }
    notice $nick $colours(We'd like to advise you we do not tolerate the advertisement of this service within our channels. This is your first and only warning.)
    inc %gotgames. [ $+ [ $nick ] ]
  }
  if ($1 = !pug) {
    inc -u60 %pugspam. [ $+ [ $nick ] ]
    if (%pugspam. [ $+ [ $nick ] ] > 3) {
      unset %pugspam. [ $+ [ $nick ] ]
      msg ChanServ@Services.GameSurge.net tb $chan $nick 1h Don't spam !pug (1 hour ban)
    }
  }
  if ($address($nick,2) == *!*@iPGN-Bot.bot.ipgn) {
    if ($chan == #tf2pug && more players needed isin $1-) {
      ;set %ipgn.notify 1
      msg #ozfortress $colours($2 more players needed in $chan)
      ;msg #tf2pug2 $colours($2 more players needed in $chan)
      notice $chan $2 more players needed JOIN!
      ;notice #tf2pug2 $2 more players needed in $chan
    }
  }
  ;Auto message for remaining slots (<=3 slots remaining)
  ;2/12 slots remaining 
  if ($address($nick,2) == *!*@iPGN-Bot.bot.ipgn) {
    if ($chan == #tf2pug && the pug isin $1- && %ipgn.notify == 1) {
      if (3/12 slots remaining isin $1-) {
        msg #ozfortress 3 more players needed in #tf2pug 
        ;msg #tf2pug2 3 more players needed in #tf2pug 
      }
      if (2/12 slots remaining isin $1-) {
        msg #ozfortress 2 more players needed in #tf2pug 
        ;msg #tf2pug2 2 more players needed in #tf2pug 
      }
      if ($chr(40) $+ 1/12 slots remaining isin $1-) {
        msg #ozfortress 1 more player needed in #tf2pug 
        ;msg #ipgn 1 more player needed in #tf2pug 
      }
    }
    if (0/12 slots remaining isin $1-) {
      set %ipgn.notify 0
    }
  }
  if (http://www.oltutasicenter.com isin $1-) {
    ban -k $chan $nick NO ADVERTISING THIS CRAP HERE, THANKS
  }
}

on *:TEXT:!fakeresults:#ultiduo:{
  if ($nick isop $chan) {
    fakeresults
  }
}
alias fakeresults {
  var %x = 1
  var %delay = 0
  var %y = $ini(D:\iPGN-Bots\TF2\TF2Duo\ulticomp_matches.ini,0)
  while (%x <= %y) {
    var %match = $getmatch(%x)
    var %winner = $gettok(%match,1,37)
    if (!$gettok(%match,3,37)) {
      .timer 1 $calc(%delay + 1) msg #ultiduo !result %x $gettok(%match,$rand(1,2),37)
      inc %delay
    }
    inc %x
  }
}

alias getmatch {
  var %matchfile = D:\iPGN-Bots\TF2\TF2Duo\ulticomp_matches.ini
  if ($ini(%matchfile,$1)) {
    var %team1 = $readini(%matchfile,$1,team1)
    var %team2 = $readini(%matchfile,$1,team2)
    var %winner = $readini(%matchfile,$1,winner)
    var %round = $readini(%matchfile,$1,round)
    var %server = $readini(%matchfile,$1,server)
    return $+(%team1,$chr(37),%team2,$chr(37),%winner,$chr(37),%round,$chr(37),%server)
  }
  else {
    return $null
  }
}

alias clr {
  if ($1 = Blue) {
    return 12
  }
  elseif ($1 = Red) {
    return 04
  }
  elseif ($1 = Green) {
    return 09
  }
  elseif ($1 = yellow) {
    return 08
  }
  elseif ($1 = darkgreen) {
    return 03
  }
  elseif ($1 = orange) {
    return 07
  }
  elseif ($1 = purple) {
    return 06
  }
  elseif ($1 = cyan) {
    return 11
  }
  elseif ($1 = pink) {
    return 13
  }
  else {
    return 
  }
}

alias colourcode {
  if (!$2) { return COLOUR ERROR: NO TEXT TO COLOUR }
  if ($1 = Blue) {
    return 12 $+ $2 $+ 
  }
  elseif ($1 = Red) {
    return 04 $+ $2 $+ 
  }
  elseif ($1 = Green) {
    return 09 $+ $2 $+ 
  }
  elseif ($1 = yellow) {
    return 08 $+ $2 $+ 
  }
  elseif ($1 = darkgreen) {
    return 03 $+ $2 $+ 
  }
  elseif ($1 = orange) {
    return 07 $+ $2 $+ 
  }
  elseif ($1 = purple) {
    return 06 $+ $2 $+ 
  }
  elseif ($1 = cyan) {
    return 11 $+ $2 $+ 
  }
  elseif ($1 = pink) {
    return 13 $+ $2 $+ 
  }
  else {
    return COLOUR ERROR: INVALID COLOUR SPECIFIED
  }
}

on *:TEXT:!pvhud*:#:{
  msg $chan $colours(12Latest PVHUD Version: %pvhud.latestversion 12Download: %pvhud.dl)
}

alias pvhudcheck {
  ;http://dl.dropboxusercontent.com/u/1565165/pvhud.html
  sockopen pvhud dl.dropboxusercontent.com 80
}

on *:sockopen:pvhud:{
  if (!$sockerr) {
    var %w = sockwrite -n $sockname
    %w GET /u/1565165/pvhud.html HTTP/1.1
    %w Host: $+(dl.dropboxusercontent.com,$crlf,$crlf)
    %w Connection: Close
    sockwrite $sockname $crlf
  }
}

on *:sockread:pvhud:{
  var %pvhud.data
  sockread %pvhud.data
  if ($sockerr) { echo -s PVHUD SOCKET HAD AN ERROR: $v1 | return }
  ;if (%pvhud.data) { echo @squery %pvhud.data }
  ;<h1 id="cur_version">PVHUD v166</h1>
  ;<h3 id="updated_on">Tue, 26 Jul 2011 20:38:42 -0400</h3>
  ;<h4 id="download"><a id="dl_link" href="http://dl.dropbox.com/u/1565165/pvhud_v166_2011-07-26.zip">Download</a></h4>

  ;<a class="tt" href="http://dl.dropbox.com/u/1565165/pvhud_v248_2012-10-27.zip" id="manual_install" title="v2.48">Download without updater</a>
  ;<a title="v2.51" href="http://dl.dropbox.com/u/1565165/pvhud_v251_2012-11-16.zip" class="tt" id="manual_install">Download without updater</a>
  if ($regex(%pvhud.data,<a title="(.*)" href="(.*)" class="tt"  id="manual_install">)) {
    echo @squery CUR_VERSION: $regml(1) Ver number: $remove($regml(1),v,$chr(46))
    if ($int($remove($regml(1),v,$chr(46))) > %pvhud.latestversion) {
      set %pvhud.latestversion $remove($regml(1),v,$chr(46))
      set %pvhud.dl $regml(2)
      set %pvhud.update 1
      echo @squery THERE IS AN UPDATE!
    }
    else {
      set %pvhud.update 0
    }
  }
  ;<p id="updated_on">Last updated on: Wed, 3 Apr 2013 21:10:49 -0400</p>
  if ($regex(%pvhud.data,<p id="updated_on">(.*?)</p>)) {
    echo @squery PVHUD updated on $regml(1)
    set %pvhud.updatedon $regml(1)
  }
  ;if ($regex(%pvhud.data,<h4 id="download"><a id="dl_link" href="(.*?)">)) {
  ;  echo @squery DL Link: $regml(1)
  ;  set %pvhud.dl $regml(1)
  ;}
}

on *:sockclose:pvhud:{
  if ($sockerr) { echo -s PVHUD SOCKET HAD AN ERROR: $v1 }
  echo @squery pvhud socket closed!
  if (%pvhud.update) {
    msg #ozfortress $colours(PVHUD Update! 12Version: %pvhud.latestversion 12Link: %pvhud.dl)
    msg #tf2pug $colours(PVHUD Update! 12Version: %pvhud.latestversion 12Link: %pvhud.dl)
    .timerpvhudannounce 5 3600 msg #ozfortress $colours(PVHUD Update! 12Version: %pvhud.latestversion 12Link: %pvhud.dl)
    .timerpvhudannounce2 5 3600 msg #tf2pug $colours(PVHUD Update! 12Version: %pvhud.latestversion 12Link: %pvhud.dl)
  }
}

on *:TEXT:*youtu*:#:{
  echo @squery youtube.com link found in $1-
  if ($regex($1-,/(https?:\/\/www\.|www\.)?youtube\.com\/watch\?(.*?)v=([A-Za-z0-9_\-]+)/i)) {
    var %vid = $null

    if ($regml(3)) {
      %vid = $regml(3)
    }
    else {
      %vid = $regml(2)
    }

    echo @squery REGEX MATCH, VID: %vid

    if ($ytcache(%vid)) {
      echo @squery %vid is cached title: $v1
      msg $chan 11Title: $v1 $iif($readini(youtubecache.ini,%vid,nsfw),(NSFW))
    }
    else {
      var %sock = youtube. [ $+ [ $ticks ] ]
      set % [ $+ [ %sock ] $+ .vid ] %vid
      set % [ $+ [ %sock ] $+ .url ] http://www.youtube.com/watch?v= $+ %vid
      set % [ $+ [ %sock ] ] $chan
      echo @squery Getting youtube title for video id %vid url in: $1-
      sockopen %sock www.youtube.com 80
    }
  }
  if ($regex($1-,/(https?:\/\/www\.|www\.)?youtu\.be\/([A-Za-z0-9_\-]+)/i)) {
    var %vid = $null

    if ($regml(2)) {
      %vid = $regml(2)
    }
    else {
      %vid = $regml(1)
    }

    echo @squery REGEX MATCH, VID: %vid

    if ($ytcache(%vid)) {
      echo @squery %vid is cached title: $v1
      msg $chan 11Title: $v1 $iif($readini(youtubecache.ini,%vid,nsfw),(NSFW))
    }
    else {
      var %sock = youtube. [ $+ [ $ticks ] ]
      set % [ $+ [ %sock ] $+ .vid ] %vid
      set % [ $+ [ %sock ] $+ .url ] http://www.youtube.com/watch?v= $+ %vid
      set % [ $+ [ %sock ] ] $chan
      echo @squery Getting youtube title for video id %vid url in: $1-
      sockopen %sock www.youtube.com 80
    }
  }
}

alias ytest {
  var %vid = $1
  var %sock = youtube. [ $+ [ $ticks ] ]
  set % [ $+ [ %sock ] $+ .vid ] %vid
  set % [ $+ [ %sock ] ] $me
  echo @squery Getting youtube title for video id %vid url in: $1-
  sockopen %sock www.youtube.com 80
}

alias ytcache {
  if ($readini(youtubecache.ini,$1,title)) {
    return $v1
  }
  else {
    return $false
  }
}

on *:sockopen:youtube.*:{
  var %write = sockwrite -n $sockname
  if (!% [ $+ [ $sockname ] $+ .newloc ]) {
    %write GET $+(/watch?v=,% [ $+ [ $sockname ] $+ .vid ],&has_verified=1) HTTP/1.1
  }
  else {
    %write GET $+(/,% [ $+ [ $sockname ] $+ .newloc ]) HTTP/1.1
  }
  %write Host: $+(www.youtube.com,$crlf,$crlf)
  ;%write Connection: Close

  %write Accept: */*
  %write Accept-Charset: *
  %write Accept-Encoding: *
  %write Accept-Language: *
  %write User-Agent: Mozilla/5.0

  %write $crlf
}

on *:sockread:youtube.*:{
  var %ytdata
  :nextread
  sockread %ytdata
  if ($sockbr == 0) {
    return
  }
  ;if (%ytdata != $crlf) && (%ytdata) { echo @squery %ytdata }
  if (302 Moved isin %ytdata) || (302 Found isin %ytdata) {
    set % [ $+ [ $sockname ] $+ .moved ] 1
  }
  ;Location: http://www.youtube.com/verify_age?next_url=http%3A//www.youtube.com/watch%3Fv%3Dyuo_ZuTbqP8
  ;Location: http://www.youtube.com/verify_age?next_url=http%3A//www.youtube.com/watch%3Fv%3D3nnqzsH_n8w%26has_verified%3D1
  if (Location: isin %ytdata) && (% [ $+ [ $sockname ] $+ .moved ]) {
    noop $regex($gettok(%ytdata,2,32),http\:(\/\/www\.|\/\/)youtube\.com\/(.*))
    set % [ $+ [ $sockname ] $+ .newloc ] $regml(2)
    if (verify_age isin %ytdata) {
      set % [ $+ [ $sockname ] $+ .nsfw ] 1
    }
    var %sock = $sockname
    sockclose %sock
    sockopen %sock www.youtube.com 80
  }
  if ($regex(%ytdata,/<title>(.*)<\/title>/i)) {
    if (error !isin $regml(1)) {
      var %title = $replace($replace($replace($regml(1),&amp;,and),&quot;,"),&#39;,')
      msg % [ $+ [ $sockname ] ] 11Title: %title $iif(% [ $+ [ $sockname ] $+ .nsfw ],(NSFW))
      writeini -n youtubecache.ini % [ $+ [ $sockname ] $+ .vid ] title %title
      if (% [ $+ [ $sockname ] $+ .nsfw ]) { 
        writeini -n youtubecache.ini % [ $+ [ $sockname ] $+ .vid ] nsfw 1 
      }
    }
    var %sock = $sockname
    unset % [ $+ [ %sock ] $+ .* ]
    unset % [ $+ [ %sock ] ]
    sockclose %sock
    return
  }
  if (<title> isin %ytdata) {
    set % [ $+ [ $sockname ] $+ .titlerecord ] 1
    goto nextread
  }
  if (% [ $+ [ $sockname ] $+ .titlerecord ]) {
    var %title = $replace($replace($replace(%ytdata,&amp;,and),&quot;,"),&#39;,')
    msg % [ $+ [ $sockname ] ] 11Title: %title $iif(% [ $+ [ $sockname ] $+ .nsfw ],(NSFW))
    writeini -n youtubecache.ini % [ $+ [ $sockname ] $+ .vid ] title %title
    if (% [ $+ [ $sockname ] $+ .nsfw ]) { 
      writeini -n youtubecache.ini % [ $+ [ $sockname ] $+ .vid ] nsfw 1 
    }
    var %sock = $sockname
    unset % [ $+ [ %sock ] $+ .* ]
    unset % [ $+ [ %sock ] ]
    sockclose %sock
    return
  }
  if (</title> isin %ytdata) {
    unset % [ $+ [ $sockname ] $+ .titlerecord ]
  }
  if (404 isin %ytdata) {
    unset % [ $+ [ $sockname ] $+ .* ]
    unset % [ $+ [ $sockname ] ]
    sockclose $sockname
    return
  }
  goto nextread
}

on *:sockclose:youtube.*:{
  if ($sockerr) {
    echo @squery $sockname error
  }
  unset % [ $+ [ $sockname ] $+ .* ]
  unset % [ $+ [ $sockname ] ]
  echo @squery $sockname closed! $sock($sockname).wsmsg
}

alias ytreg {
  ;http://youtu.be/yDkT_henhAo
  ;http://www.youtube.com/watch?v=5iWO4NKfWBA
  noop $regex($1-,/(https?:\/\/www\.|www\.)?youtu\.be\/([A-Za-z0-9_\-]+)/i)
  return 1: $regml(1) 2: $regml(2) 3: $regml(3) 4: $regml(4)
}
