;New bot script - notes, bookable server automation and more
on *:start:{
  ;Let's set all da variables :D
  set %defaultrconpassword h1tl3r
  set %server1rconpassword %defaultrconpassword
  set %server2rconpassword %defaultrconpassword
  set %server3rconpassword %defaultrconpassword
  .timer 1 10 join #ipgn-tf2
  .timer 1 10 join #ipgn
  window -e @bookings
}

on *:TEXT:!*:#ipgn-tf2: {
  if ($1 = !acmdlist) {
    notice $nick $colours(Special admin only commands are:)
    notice $nick $colours(!notes <player>. Shows set notes on a specified player // !addnote <player> <note> (Adds a note for a player))
    notice $nick $colours(!book <time> <booked team>. Eg. !book 2 beastin. Books one of the bookable servers and gives out an rcon password.)
    notice $nick $colours(!serverstatus. Specifying a server is optional. Shows status of bookable servers (if server is booked or not). eg !serverstatus 1)
    notice $nick $colours(!resetservers. Sets all passwords/rcon passwords of bookable servers back to default)
    notice $nick $colours(!resetserver <server>. Resets a specific server. EG: !resetserver 1)
    notice $nick $colours(!banid <STEAMID> <nick> <reason>. Eg. !banid STEAM_0:0:12345 bladez king of da woild)
    notice $nick $colours(!timers. Shows timeleft until the booked servers reset)
    notice $nick $colours(!rcon <server> <command>. Sends rcon <command> to specified <server>. Eg !rcon 1 say HAY GUYZ)
  }
  if ($1 = !notes) && ($2) {
    if ($read($2 $+ notes.txt)) {
      var %linesofnotesfile = $lines($2 $+ notes.txt)
      var %currentnoteline = 1
      var %noteguy = $2
      notice $nick $colours(Notes for player $2 $+ :)
      while (%currentnoteline <= %linesofnotesfile) {
        tokenize 37 $read(%noteguy $+ notes.txt,%currentnoteline)
        notice $nick $colours(Note %currentnoteline added by $1 $+ : $2)
        inc %currentnoteline
      }
    }
    else {
      notice $nick $colours(No notes could be found for $2)
    }
  }
  if ($1 = !addnote) && ($2) {
    write $2 $+ notes.txt $+($nick,$chr(37),$3-)
    msg $chan $colours(Note has been added for $2)
  }
  if ($1 = !serverstatus) {
    if (!$2) {
      var %serverstatuslines = $lines(bookableservers.txt)
      var %currentstatusline = 1
      while (%currentstatusline <= %serverstatuslines) {
        ;Booker: <name>. Booked for: <team name>. Booking time: <time of booking (hours)>. RCON password: <rcon pass>
        ;s1%ip%port%Booked%rconpassword%booker%team%time
        tokenize 37 $read(bookableservers.txt,%currentstatusline)
        if ($read(bookableservers.txt,w,S $+ %currentstatusline $+ * $+ Is Booked $+ *)) {
          msg $chan $colours(Bookable $1 $+ . $chr(2) $+ $chr(3) $+ 12 $+ Status $+ : $+ $chr(2) $+ $chr(3) This server is currently booked. $chr(3) $+ 12 $+ $chr(2) $+ Booker: $+ $chr(3) $+ $chr(2) $6 $+ . $chr(3) $+ 12 $+ $chr(2) $+ Booked for: $chr(3) $+ $chr(2) $+ $7 $+ . $chr(3) $+ 12 $+ $chr(2) $+ Booking time: $+ $chr(3) $+ $chr(2) $8 $+ hours. $chr(3) $+ 12 $+ $chr(2) $+ RCON Password: $chr(3) $+ $chr(2) $+ $5 $+ .)
        }
        else {
          msg $chan $colours(Bookable $1 $+ . $chr(3) $+ 12 $+ Status: $+ $chr(3) $4)
        }
        inc %currentstatusline
      }
    }
    if ($2 isnum) {
      if ($read(bookableservers.txt,w,S $+ $2 $+ * $+ Is Booked $+ *,$2)) {
        tokenize 37 $read(bookableservers.txt,$2)
        msg $chan $colours(Bookable $1 $+ . $chr(2) $+ $chr(3) $+ 12 $+ Status $+ : $+ $chr(2) $+ $chr(3) This server is currently booked. $chr(3) $+ 12 $+ $chr(2) $+ Booker: $+ $chr(3) $+ $chr(2) $6 $+ . $chr(3) $+ 12 $+ $chr(2) $+ Booked for: $chr(3) $+ $chr(2) $+ $7 $+ . $chr(3) $+ 12 $+ $chr(2) $+ Booking time: $+ $chr(3) $+ $chr(2) $8 $+ hours. $chr(3) $+ 12 $+ $chr(2) $+ RCON Password: $chr(3) $+ $chr(2) $+ $5 $+ .)
      }
      else {
        tokenize 37 $read(bookableservers.txt,$2)
        msg $chan $colours(Bookable $1 $+ . $chr(3) $+ 12 $+ Status: $+ $chr(3) $4)
      }
    }
  }
  if ($1 = !resetall) {
    ;fileformat = s1%ip%port%status%rconpassword
    msg $chan $colours(All bookable servers have been reset)
    rcon.resetall
  }
  if ($1 = !resetserver) {
    if ($2 isnum) {
      unset % [ $+ server. $+ [ $gettok($read(bookableservers.txt,$2),6,37) ] $+ * ]
      reset server $2
    }
  }
  if ($1 = !book) && ($2 <= 4) {
    if ($read(bookableservers.txt,w,S $+ * $+ $chr(37) $+ * $+ Not currently booked $+ *,1)) {
      var %bookername = $nick
      var %bookingtime = $2
      var %bookingteam = $3-
      var %bookedserver = $readn
      .timerresetserver $+ %bookedserver 1 $calc($2 * 3600) rcon.reset %bookedserver $me
      %server1rconpassword = $lower($read(passwords.txt))
      %server1joinpassword = $lower($read(passwords.txt))
      ;s1%ip%port%Booked%rconpassword%booker%team%time
      tokenize 37 $v1
      msg #ipgn-tf2 $colours(Server %bookedserver has been booked by $nick for %bookingteam $+ . This booking lasts %bookingtime hours.)
      notice $nick $colours(connect $2 $+ : $+ $3 $+ ; password %server1joinpassword $+ ; rcon_password %server1rconpassword)
      rconbooking $2 $3 $5 say This server has been booked for %bookingteam $+ .; sv_password %server1joinpassword ; wait 1000; rcon_password %server1rconpassword ;changelevel cp_granary
      echo @bookings %bookedserver has been booked by $nick for %bookingteam $+ . This booking lasts %bookingtime hours.
      write -l $+ %bookedserver bookableservers.txt $+($1,$chr(37),$2,$chr(37),$3,$chr(37),Is Booked,$chr(37),%server1rconpassword,$chr(37),%bookername,$chr(37),%bookingteam,$chr(37),%bookingtime,$chr(37),%server1joinpassword)
    }
    else {
      msg #ipgn-tf2 $colours(No servers are available)
    }
  }
  if ($1 = !timers) {
    if ($timer(resetserver1) || $timer(resetserver2) || $timer(resetserver3)) {
      if ($timer(resetserver1)) {
        if ($timer(resetserver1).secs >= 3600) {
          msg #ipgn-tf2 $colours(Approximately $round($calc($timer(resetserver1).secs / 3600),1) hours until server 1 resets)
        }
        else {
          msg #ipgn-tf2 $colours(Approximately $round($calc($timer(resetserver1).secs / 60),0) minutes until server 1 resets)
        }
      }
      if ($timer(resetserver2)) {
        if ($timer(resetserver2).secs >= 3600) {
          msg #ipgn-tf2 $colours(Approximately $round($calc($timer(resetserver2).secs / 3600),1) hours until server 2 resets)
        }
        else {
          msg #ipgn-tf2 $colours(Approximately $round($calc($timer(resetserver2).secs / 60),0) minutes until server 2 resets)
        }
      }
      if ($timer(resetserver3)) {
        if ($timer(resetserver3).secs >= 3600) {
          msg #ipgn-tf2 $colours(Approximately $round($calc($timer(resetserver3).secs / 3600),1) hours until server 3 resets)
        }
        else {
          msg #ipgn-tf2 $colours(Approximately $round($calc($timer(resetserver3).secs / 60),0) minutes until server 3 resets)
        }
      }
    }
    else {
      msg #ipgn-tf2 $colours(No servers are currently in use)
    }
  }
  if ($1 = !details) && ($2) {
    if ($read(bookableservers.txt,w,S $+ $2 $+ * $+ Is Booked $+ *)) {
      tokenize 37 $v1
      msg #ipgn-tf2 $colours(Bookable $1 details are: connect $2 $+ : $+ $3 $+ ; password $9 $+ ; rcon_password $5)
    }
    else {
      tokenize 37 $read(bookableservers.txt,$2)
      msg #ipgn-tf2 $colours(Bookable $1 details are: $2 $+ : $+ $3 $+ . RCON Password: $5)
    }
  }
  ;if ($1 = !set) && ($address($nick,2) == *!*@bladezz.admin.ipgn) {
  ;  set $2 $3
  ;}
  if ($1 = !rcon) && ($nick isop #ipgn-tf2) {
    rconbooking $2 $3 $4 $5-
    msg #ipgn-tf2 $colours(Command sent to $2 $+ : $+ $3 with rcon_password $4 ( $+ $5- $+ ))
  }
  if ($1 = !relay) {
    if ($2) {
      if ($read(bookablerelay.txt,w,* $+ Available $+ *)) {
        var %name = $nick
        var %time = infinite
        var %server = $2
        var %relaycommand = tv_relay $2
        msg %admin.chan $colours(Setting up relay for $nick $+ $chr(44) connecting to $2 for %time hours.)
        tokenize 37 $read(bookablerelay.txt,w,* $+ Available $+ *,1)
        write -l $+ $readn bookablerelay.txt $+($1,$chr(37),$2,$chr(37),$3,$chr(37),$4,$chr(37),Booked,$chr(37),%time,$chr(37),%name,$chr(37),%server)
        msg %admin.chan $colours(Relay server $1 $+ : $+ $2 connecting to %server)
        rconbooking $1 $3 $4 %relaycommand
      }
      ;rconbooking 210.50.4.30 27036 %defaultrconpassword tv_relay $2
      ;msg #ipgn-tf2 $colours(SourceTV Relay connecting to $2 $+ ...)
    }
  }
}

/* Relay Server file format:
Booked Format: IP%Connect Port%Rcon Port%Rcon Password%Status%booking time%booking user%server being relayed
$+($1,$chr(37),$2,$chr(37),$3,$chr(37),$4,$chr(37),Booked,$chr(37),%time,$chr(37),%name,$chr(37),%server)
Unbooked format: IP%Connect Port%Rcon Port%Rcon Password%Status
*/

alias rcon.resetall {
  var %y = $lines(bookableservers.txt)
  var %x = 1
  while (%y >= %x) {
    echo -a %x
    tokenize 37 $read(bookableservers.txt,%x)
    ;s1%ip%port%Booked%rconpassword%booker%team%time
    rconbooking $2 $3 $5 exec nopass.cfg; say This server has been reset by $me ; wait 1000 ; rcon_password %defaultrconpassword
    write -l $+ %x bookableservers.txt $+(S $+ %x,$chr(37),$2,$chr(37),$3,$chr(37),Not currently booked,$chr(37),%defaultrconpassword)
    .timerresetserver $+ %x off
    inc %x
  }
}

alias rcon.reset {
  var %resetter = $2
  var %resetserver = $1
  tokenize 37 $read(bookableservers.txt,$1)
  rconbooking $2 $3 $5 exec nopass.cfg; say This server has been reset by %resetter ; wait 1000 ; rcon_password %defaultrconpassword
  msg #ipgn-tf2 $colours(Server %resetserver has been reset by %resetter)
  write -l $+ %resetserver bookableservers.txt $+(S $+ %resetserver,$chr(37),$2,$chr(37),$3,$chr(37),Not currently booked,$chr(37),%defaultrconpassword)
  echo @bookings Bookable S $+ %resetserver has been reset by %resetter
  .timerresetserver $+ %resetserver off
}

alias rconbooking {
  .run -n $shortfn(C:\TF2\SourceRcon.exe) $1 $2 $3 " $+ $4- $+ "
}
