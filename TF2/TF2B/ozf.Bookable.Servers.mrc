on *:CONNECT:{
  .timerozfresetall 06:00 1 1 ozfResetAll
  .timerozfbookingcheck 0 60 ozfCheckPreBookings
  .timerozfserverstatus 0 300 ozfCheckServerStatus
}

on *:TEXT:!*:#ozf-help,#bookings: {
  if ($nick isop $chan) || ($address($nick,2) == *!*@bladezz.admin.ipgn) || (($network != GameSurge) && ($chan == #bookings)) {
    if ($1 = !cmdlist) {
      notice $nick $colours(All commands are available at http://202.138.3.40:6003/?p= $+ %ozf.Web.Server.Password $+ &m=commands)
    }
    if ($1 = !banid) {
      if ((!$2) || (STEAM_* !iswm $2) || (!$3)) { msg $chan $colours(Syntax: !banid <id> <reason>) | return }
      var %ip = $readini(ozfservers.ini,5,ip)
      var %port = $readini(ozfservers.ini,5,port)
      var %currenctrcon = $readini(ozfservers.ini,5,rcon)

      rconbooking %ip %port %currentrcon sm_addban 0 $2 $3-
      msg $chan $colours(Banned12 $2 from 7iPGN $+ $chr(44) 7ozfortress and 7GamersUnited servers)
    }
    if ($1 = !unbanid) {
      if ((!$2) || (STEAM_* !iswm $2)) { msg $chan $colours(Syntax: !unbanid <id>) | return }
      var %ip = $readini(ozfservers.ini,5,ip)
      var %port = $readini(ozfservers.ini,5,port)
      var %currenctrcon = $readini(ozfservers.ini,5,rcon)

      rconbooking %ip %port %currentrcon sm_unban $2
      msg $chan $colours(Unbanned12 $2 from all servers)
    }
    if ($1 = !details) {
      if ($2) && ($2 isnum) && ($2 <= $calc($ini(ozfservers.ini,0) - 1)) {
        if ($readini(ozfservers.ini,$2,booked)) && ($readini(ozfservers.ini,$2,active)) {
          var %currentrcon = $readini(ozfservers.ini,$2,rcon)
          var %ip = $readini(ozfservers.ini,$2,ip)
          var %port = $readini(ozfservers.ini,$2,port)
          var %pass = $readini(ozfservers.ini,$2,password)
          var %type = $readini(ozfservers.ini,$2,type)

          notice $nick $colours(Details for server $2 ( $+ %type $+ ) are: $connectString(%ip,%port,%pass,%currentrcon))
        }
        elseif ($readini(ozfservers.ini,$2,bookingtime)) && (!$readini(ozfservers.ini,$2,active)) {
          var %booker = $readini(ozfservers.ini,$2,booker)
          var %team = $readini(ozfservers.ini,$2,team)
          var %duration = $readini(ozfservers.ini,$2,duration)
          var %bookedat = $readini(ozfservers.ini,$2,bookedat)
          var %booked = $readini(ozfservers.ini,$2,booked)
          var %bookingtime = $readini(ozfservers.ini,$2,bookingtime)
          var %type = $readini(ozfservers.ini,$2,type)

          msg $chan $colours(Server $2 ( $+ %type $+ ) has been pre-booked by %booker for %team at $asctime(%bookingtime,h:nntt dddd) $+ . $&
            This server is not yet active and does not yet have details ( $+ $duration($calc(%bookingtime - $ctime)) remaining until activation))
        }
        else {
          var %currentrcon = $readini(ozfservers.ini,$2,rcon)
          notice $nick $colours(Server $2 is not currently booked. The rcon should be: %currentrcon)
        }
      }
      else {
        notice $nick $colours(ERROR: Incorrect input. !details <server number> or !details relay <server number>)
      }

    }
    if ($1 = !status) {
      if ($2) && ($2 isnum) && ($2 <= $calc($ini(ozfservers.ini,0) - 1)) {
        var %currentrcon = $readini(ozfservers.ini,$2,rcon)
        var %ip = $readini(ozfservers.ini,$2,ip)
        var %port = $readini(ozfservers.ini,$2,port)
        var %booker = $readini(ozfservers.ini,$2,booker)
        var %team = $readini(ozfservers.ini,$2,team)
        var %duration = $readini(ozfservers.ini,$2,duration)
        var %bookedat = $readini(ozfservers.ini,$2,bookedat)
        var %booked = $readini(ozfservers.ini,$2,booked)
        var %bookingtime = $readini(ozfservers.ini,$2,bookingtime)
        var %bookingactive = $readini(ozfservers.ini,$2,active)
        var %type = $readini(ozfservers.ini,$2,type)

        notice $nick $colours($colourcode(orange,Type:) %type $colourcode(orange,IP:) %ip $colourcode(orange,Port:) %port $colourcode(orange,Rcon:) %currentrcon)
        notice $nick $colours($colourcode(orange,Booked:) $iif(%booked,Yes,No) $iif(%booked,$colourcode(orange,Booker:) %booker $colourcode(orange,Booked for:) %team,$nil))
        if (%booked) {
          notice $nick $colours($colourcode(orange,Booked at:) %bookedat $colourcode(orange,Duration:) %duration hours)
        }
        if (%bookingtime) {
          notice $nick $colours($colourcode(orange,Pre-booked for:) $asctime(%bookingtime,h:nntt dddd) $&
            $iif(!%bookingactive,(Not yet active; $duration($calc(%bookingtime - $ctime)) remaining),(Active)))
        }
      }
      if ($2 = relay) {
        if ($3) && ($3 isnum) && ($3 <= $calc($ini(ipgnrelayservers.ini,0) - 1)) {
          var %ip = $readini(ipgnrelayservers.ini,$3,ip)
          var %connectip = $readini(ipgnrelayservers.ini,$3,connectip)
          var %port = $readini(ipgnrelayservers.ini,$3,port)
          var %booked = $readini(ipgnrelayservers.ini,$3,booked)
          var %booker = $readini(ipgnrelayservers.ini,$3,booker)
          var %relay = $readini(ipgnrelayservers.ini,$3,relay)
          var %rconport = $readini(ipgnrelayservers.ini,$3,rconport)
          var %announcemsg = $readini(ipgnrelayservers.ini,$3,announce)
          var %bookingtime = $readini(ipgnrelayservers.ini,$3,bookingtime)
          var %active = $readini(ipgnrelayservers.ini,$3,active)

          msg $chan $colours($colourcode(blue,IP:) %connectip $colourcode(blue,Port:) %port $colourcode(blue,Booked:) $iif(%booked,Yes ( $+ %booker $+ ),No))
          if (%bookingtime) {
            msg $chan $colours($colourcode(blue,Pre-booked for:) $asctime(%bookingtime,h:nntt dddd) $iif(%active,(Active),(Activates in $duration($calc(%bookingtime - $ctime)) $+ )))
          }
          if (%booked) && (%active) {
            msg $chan $colours($colourcode(blue,Connected to:) %relay $iif(%announcemsg,$colourcode(blue,Last announce:) %announcemsg)) 
          }
          if (%booked) && (!%active) {
            msg $chan $colours($colourcode(blue,Target:) %relay $iif(%announcemsg,$colourcode(blue,Announce:) %announcemsg))
          }
        }
        elseif ($3 > $calc($ini(ipgnrelayservers.ini,0) - 1)) {
          notice $nick $colours(There are currently only $calc($ini(ipgnrelayservers.ini,0) - 1) relay servers)
        }
        else {
          notice $nick $colours(ERROR: Incorrect input. !status relay <relay number> (1 - $calc($ini(ipgnrelayservers.ini,0) - 1)))
        }
      }
      else {
        return
      }
    }
    if ($1 = !resetall) {
      ozfResetAll
    }
    if ($1 = !reset) {
      if ($2 = relay) { goto ozfIpgnRelay }
      if ($2 !isnum) { msg $chan $colours(You must specify the number of the server. Eg, !reset 2) | return }
      if ($readini(ozfservers.ini,$2,booked)) {
        ozfServerReset $2
      }
      else {
        msg $chan $colours(Server $2 isn't in use and does not need to be reset.)
      }
    }
    if ($1 = !timebook) {
      if ((!$3) || ($3 >= $calc($ini(ozfservers.ini,0) - 1)) || ($4 !isnum) || (!$5)) {
        msg $chan $colours(Syntax: !timebook <time> <server> <duration> <team/player>)
        return
      }

      if (!$ini(ozfservers.ini,$3)) { msg $chan $colours(Invalid server specified. Please check server IDs using !servers) | return }
      if (!$regex($2,(\d+)\:(\d+)(am|pm))) { msg $chan $colours(Invalid time specified. EG: 8:30pm, 10:30am, 11:30am, etc) | return }
      var %numbooked = $readini(ozfservers.ini,global,numbooked)
      if (%numbooked = $calc($ini(ozfservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | return }
      var %bookingtime = $timeBookingCalculate($2)
      if ($gettok(%bookingtime,1,37) < $ctime) { msg $chan $colours(You can't book a server in the past!) | return }

      if ($readini(ozfservers.ini,$3,booked)) {
        var %booker = $readini(ozfservers.ini,$3,booker)
        var %team = $readini(ozfservers.ini,$3,team)
        var %duration = $readini(ozfservers.ini,$3,duration)
        var %bookedat = $readini(ozfservers.ini,$3,bookedat)
        var %bookingtime = $readini(ozfservers.ini,$3,bookingtime)
        var %bookingactive = $readini(ozfservers.ini,$3,active)
        var %type = $readini(ozfservers.ini,$3,type)        

        if (%bookingtime) && (!%bookingactive) {
          msg $chan $colours(Server $3 ( $+ %type $+ ) was prebooked by %booker at %bookedat and will activate at $asctime(%bookingtime,h:nntt dddd) ( $+ $duration($calc(%bookingtime - $ctime)) $+ ))
        }
        else {
          msg $chan $colours(Server $3 ( $+ %type $+ ) is currently booked for %team $+ . This server was booked by %booker for a duration of %duration hours at %bookedat)
        }
      }
      else {
        var %numbooked = $readini(ozfservers.ini,global,numbooked)
        writeini -n ozfservers.ini global numbooked $calc(%numbooked + 1)
        writeini -n ozfservers.ini $3 booked 1
        writeini -n ozfservers.ini $3 booker $nick
        writeini -n ozfservers.ini $3 duration $4
        writeini -n ozfservers.ini $3 team $5-
        writeini -n ozfservers.ini $3 bookedat $time(h:nntt dddd)
        writeini -n ozfservers.ini $3 bookingtime $gettok(%bookingtime,1,37)

        msg $chan $colours(Server $3 has been booked by $nick for $5- $+ . This booking will last $4 hours and will activate at $2)
      }
    }
    if ($1 = !bookserver) && ($2 isnum) && ($3 isnum) && ($2 <= $calc($ini(ozfservers.ini,0) - 1)) && (1 <= $2) && ($4) {
      var %numbooked = $readini(ozfservers.ini,global,numbooked)
      if (%numbooked = $calc($ini(ozfservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | return }
      if ($readini(ozfservers.ini,$2,booked)) {
        var %booker = $readini(ozfservers.ini,$2,booker)
        var %team = $readini(ozfservers.ini,$2,team)
        var %duration = $readini(ozfservers.ini,$2,duration)
        var %bookedat = $readini(ozfservers.ini,$2,bookedat)
        var %bookingtime = $readini(ozfservers.ini,$2,bookingtime)
        var %bookingactive = $readini(ozfservers.ini,$2,active)
        var %type = $readini(ozfservers.ini,$2,type)

        if (%bookingtime) && (!%bookingactive) {
          msg $chan $colours(Server $2 ( $+ %type $+ ) was prebooked by %booker at %bookedat and will activate at $asctime(%bookingtime,h:nntt dddd) ( $+ $duration($calc(%bookingtime - $ctime)) $+ ))
        }
        else {
          msg $chan $colours(Server $2 ( $+ %type $+ ) is currently booked for %team $+ . This server was booked by %booker for a duration of %duration hours at %bookedat)
        }
      }
      else {
        var %rcon = $lower($read(passwords.txt))
        var %pass = $lower($read(passwords.txt))
        var %currentrcon = $readini(ozfservers.ini,$2,defaultrcon)
        var %ip = $readini(ozfservers.ini,$2,ip)
        var %port = $readini(ozfservers.ini,$2,port)
        var %numbooked = $readini(ozfservers.ini,global,numbooked)
        var %type = $readini(ozfservers.ini,$2,type)

        writeini -n ozfservers.ini global numbooked $calc(%numbooked + 1)
        writeini -n ozfservers.ini $2 booked 1
        writeini -n ozfservers.ini $2 active 1
        writeini -n ozfservers.ini $2 booker $nick
        writeini -n ozfservers.ini $2 duration $3
        writeini -n ozfservers.ini $2 team $4-
        writeini -n ozfservers.ini $2 password %pass
        writeini -n ozfservers.ini $2 rcon %rcon
        writeini -n ozfservers.ini $2 bookedat $time(h:nntt dddd)

        .timerresetozf $+ $2 1 $calc($3 * 3600) ozfServerReset $2

        msg $chan $colours(Server $2 ( $+ %type $+ ) has been booked by $nick for $4 $+ . This booking lasts $3 hours.)
        notice $nick $colours($connectString(%ip,%port,%pass,%rcon))
        if ($4 ison $chan) {
          notice $4 $colours(Details for your booking (Lasts $3 hours): $connectString(%ip,%port,%pass,%rcon))
          msg $4 $colours($nick has booked server $2 ( $+ %type $+ ) for you. Details (Lasts $3 hours): $connectString(%ip,%port,%pass,%rcon))
          if ($4 !isop $chan) { .timer 1 120 kickban $chan $4 }
        }

        rconbooking %ip %port %currentrcon say This server has been booked for $3 hours $+ .; sv_password %pass ; kickall; wait 500; rcon_password %rcon ; changelevel cp_granary; mr_ipgnbooker $4- ; livelogs_name $4-

        if (%type = ozfortress) {
          setHeavyDetails %port %rcon %pass $4
        }

        ozfAddServerStats BOOKED $2 $nick $ctime $date(yyyy-mm-dd)
      }
    }
    if (($1 = !book) && ($3 !isnum)) {
      var %numbooked = $readini(ozfservers.ini,global,numbooked), %typebook
      if (%numbooked == $calc($ini(ozfservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | return }

      if ((!$2) || (!$3) || ($2 !isnum)) {
        msg $chan $colours(You must specify the time the booking should last (hours) and the name of the booking. eg !book 2 myteam)
        return
      }

      var %x = 1, %numservers = $calc($ini(ozfservers.ini,0) - 1)
      while (%x <= %numservers) {
        if ($readini(ozfservers.ini,%x,booked) = 0) {
          var %rcon = $lower($read(passwords.txt))
          var %pass = $lower($read(passwords.txt))
          var %currentrcon = $readini(ozfservers.ini,%x,defaultrcon)
          var %ip = $readini(ozfservers.ini,%x,ip)
          var %port = $readini(ozfservers.ini,%x,port)
          var %numbooked = $readini(ozfservers.ini,global,numbooked)
          var %type = $readini(ozfservers.ini,%x,type)

          writeini -n ozfservers.ini global numbooked $calc(%numbooked + 1)
          writeini -n ozfservers.ini %x booked 1
          writeini -n ozfservers.ini %x active 1
          writeini -n ozfservers.ini %x booker $nick
          writeini -n ozfservers.ini %x duration $2
          writeini -n ozfservers.ini %x team $3-
          writeini -n ozfservers.ini %x password %pass
          writeini -n ozfservers.ini %x rcon %rcon
          writeini -n ozfservers.ini %x bookedat $time(h:nntt dddd)

          .timerresetozf $+ %x 1 $calc($2 * 3600) ozfServerReset %x

          msg $chan $colours(Server %x ( $+ %type $+ ) has been booked by $nick for $3 $+ . This booking lasts $2 hours.)
          notice $nick $colours($connectString(%ip,%port,%pass,%rcon))

          if ($3 ison $chan) {
            notice $3 $colours(Details for your booking (Lasts $2 hours): $connectString(%ip,%port,%pass,%rcon))
            msg $3 $colours($nick has booked server %x ( $+ %type $+ ) for you. Details (Lasts $2 hours): $connectString(%ip,%port,%pass,%rcon))
            if ($3 !isop $chan) { .timer 1 120 kickban $chan $3 }
          }

          rconbooking %ip %port %currentrcon say This server has been booked for $2 hours $+ .; sv_password %pass ; kickall;  wait 500; rcon_password %rcon ; changelevel cp_granary; mr_ipgnbooker $3- ; livelogs_name $3-

          if (%type = ozfortress) {
            setHeavyDetails %port %rcon %pass $3
          }

          ozfAddServerStats BOOKED %x $nick $ctime $date(yyyy-mm-dd)

          break
        }
        else {
          inc %x
        }
      }
    }
    if ($1 = !addbookable) {
      if ((!$2) || (!$3) || (!$4) || (!$regex($2,\b(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}\b))) || ($3 !isnum) || (!$5) || (($6) && ($6 !isnum))) { 
        msg $chan $colours(Usage: !addbookable <ip> <port> <defaultrcon> <type (nz/wa/sg/etc)> <id (optional)>)
        return
      }

      var %x = 1, %sExists = 0, %id_target = $iif($6,$ifmatch,0)
      var %y = $ini(ozfservers.ini,0)
      while (%x <= %y) {
        if ($2 = $readini(ozfservers.ini,%x,ip)) && ($3 = $readini(ozfservers.ini,%x,port)) {
          msg $chan $colours(That server is already on the server list at ID %x)
          %sExists = 1
          break
        }
        inc %x
      }

      if (!%sExists) {
        if (%id_target) {
          if (%id_target < %y) {
            var %x = %y
            while (%id_target < %x) {
              writeini -n ozfservers.ini %x ip $readini(ozfservers.ini,$calc(%x - 1),ip)
              writeini -n ozfservers.ini %x port $readini(ozfservers.ini,$calc(%x - 1),port)
              writeini -n ozfservers.ini %x rcon $readini(ozfservers.ini,$calc(%x - 1),rcon)
              writeini -n ozfservers.ini %x booker $readini(ozfservers.ini,$calc(%x - 1),booker)
              writeini -n ozfservers.ini %x booked $readini(ozfservers.ini,$calc(%x - 1),booked)
              writeini -n ozfservers.ini %x duration $readini(ozfservers.ini,$calc(%x - 1),duration)
              writeini -n ozfservers.ini %x team $readini(ozfservers.ini,$calc(%x - 1),team)
              writeini -n ozfservers.ini %x password $readini(ozfservers.ini,$calc(%x - 1),password)
              writeini -n ozfservers.ini %x bookedat $readini(ozfservers.ini,$calc(%x - 1),bookedat)
              writeini -n ozfservers.ini %x defaultrcon $readini(ozfservers.ini,$calc(%x - 1),defaultrcon)
              writeini -n ozfservers.ini %x bookingtime $readini(ozfservers.ini,$calc(%x - 1),bookingtime)
              writeini -n ozfservers.ini %x active $readini(ozfservers.ini,$calc(%x - 1),active)
              writeini -n ozfservers.ini %x type $readini(ozfservers.ini,$calc(%x - 1),type)

              dec %x
            }
          }
          else {
            %id_target = %y
          }

          writeini -n ozfservers.ini %id_target ip $2
          writeini -n ozfservers.ini %id_target port $3
          writeini -n ozfservers.ini %id_target rcon $4
          writeini -n ozfservers.ini %id_target booker 0
          writeini -n ozfservers.ini %id_target booked 0
          writeini -n ozfservers.ini %id_target duration 0
          writeini -n ozfservers.ini %id_target team 0
          writeini -n ozfservers.ini %id_target password 0
          writeini -n ozfservers.ini %id_target bookedat 0
          writeini -n ozfservers.ini %id_target defaultrcon $4
          writeini -n ozfservers.ini %id_target bookingtime 0
          writeini -n ozfservers.ini %id_target active 0
          writeini -n ozfservers.ini %id_target type $upper($5)
        }
        else {
          writeini -n ozfservers.ini %y ip $2
          writeini -n ozfservers.ini %y port $3
          writeini -n ozfservers.ini %y rcon $4
          writeini -n ozfservers.ini %y booker 0
          writeini -n ozfservers.ini %y booked 0
          writeini -n ozfservers.ini %y duration 0
          writeini -n ozfservers.ini %y team 0
          writeini -n ozfservers.ini %y password 0
          writeini -n ozfservers.ini %y bookedat 0
          writeini -n ozfservers.ini %y defaultrcon $4
          writeini -n ozfservers.ini %y bookingtime 0
          writeini -n ozfservers.ini %y active 0
          writeini -n ozfservers.ini %y type $upper($5)
        }
        msg $chan $colours(Server $iif(%id_target,$ifmatch,%y) ( $+ $upper($5) $+ ) added to the server list.)
      }
    }
    if ($1 = !deletebookable) {
      if (!$2) || ($2 !isnum) || (!$ini(ozfservers.ini,$2)) { 
        msg $chan $colours(Usage: !deletebookable <id>) 
        return 
      }

      var %x = $2
      var %y = $calc($ini(ozfservers.ini,0) - 1)
      while (%x < %y) {
        var %s = $calc(%x + 1)
        writeini -n ozfservers.ini %x ip $readini(ozfservers.ini,%s,ip)
        writeini -n ozfservers.ini %x port $readini(ozfservers.ini,%s,port)
        writeini -n ozfservers.ini %x rcon $readini(ozfservers.ini,%s,rcon)
        writeini -n ozfservers.ini %x booker $readini(ozfservers.ini,%s,booker)
        writeini -n ozfservers.ini %x booked $readini(ozfservers.ini,%s,booked)
        writeini -n ozfservers.ini %x duration $readini(ozfservers.ini,%s,duration)
        writeini -n ozfservers.ini %x team $readini(ozfservers.ini,%s,team)
        writeini -n ozfservers.ini %x password $readini(ozfservers.ini,%s,password)
        writeini -n ozfservers.ini %x bookedat $readini(ozfservers.ini,%s,bookedat)
        writeini -n ozfservers.ini %x defaultrcon $readini(ozfservers.ini,%s,defaultrcon)
        writeini -n ozfservers.ini %x bookingtime $readini(ozfservers.ini,%s,bookingtime)
        writeini -n ozfservers.ini %x active $readini(ozfservers.ini,%s,active)
        writeini -n ozfservers.ini %x type $readini(ozfservers.ini,%s,type)

        inc %x
      }

      remini ozfservers.ini %y

      msg $chan $colours(Removed server $2)
    }
    if (($1 = !servers) || ($1 = !booked)) {
      var %y = $calc($ini(ozfservers.ini,0) - 1)
      var %x = 1, %count = 0
      while (%x <= %y) {
        break

        var %booked = $readini(ozfservers.ini,%x,booked)
        if (%booked) {
          var %currentrcon = $readini(ozfservers.ini,%x,rcon)
          var %defaultrcon = $readini(ozfservers.ini,%x,defaultrcon)
          var %ip = $readini(ozfservers.ini,%x,ip)
          var %port = $readini(ozfservers.ini,%x,port)
          var %booker = $readini(ozfservers.ini,%x,booker)
          var %team = $readini(ozfservers.ini,%x,team)
          var %duration = $readini(ozfservers.ini,%x,duration)
          var %bookedat = $readini(ozfservers.ini,%x,bookedat)
          var %type = $readini(ozfservers.ini,%x,type)

          notice $nick $colours($+(%ip,:,%port) ( $+ %x ( $+ %type $+ )) Status: $iif(%booked,Booked by %booker at %bookedat,Available) Default/Current RCON: $+(%defaultrcon,/,%currentrcon))
          inc %count
          if (%count > 6) {
            break
          }
        }
        inc %x
      }
      notice $nick $colours(The status of all servers can be viewed at http://202.138.3.40:6003/?p= $+ %ozf.Web.Server.Password $+ &m=servers)
    }
    if ($1 = !relay) {
      if (!$2) || ($0 > 2) { msg $chan $colours(Syntax: !relay <tv ip>) | return }
      if (($regex($gettok($2,1,58),\b(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}\b))) || ($regex($gettok($2,1,58),(.*)\.com))) {
        var %numbooked = $readini(ipgnrelayservers.ini,global,numbooked)
        if (%numbooked = $calc($ini(ipgnrelayservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | return }
        var %x = 1, %numrelays = $calc($ini(ipgnrelayservers.ini,0) - 1)
        while (%x <= %numrelays) {
          if ($readini(ipgnrelayservers.ini,%x,booked) = 0) {
            var %currentrcon = $readini(ipgnrelayservers.ini,%x,rcon)
            var %ip = $readini(ipgnrelayservers.ini,%x,ip)
            var %connectip = $readini(ipgnrelayservers.ini,%x,connectip)
            var %port = $readini(ipgnrelayservers.ini,%x,port)
            var %rconport = $readini(ipgnrelayservers.ini,%x,rconport)
            var %numbooked = $readini(ipgnrelayservers,global,numbooked)

            writeini -n ipgnrelayservers.ini global numbooked $calc(%numbooked + 1)
            writeini -n ipgnrelayservers.ini %x booked 1
            writeini -n ipgnrelayservers.ini %x booker $nick
            writeini -n ipgnrelayservers.ini %x duration 4
            writeini -n ipgnrelayservers.ini %x relay $2-
            writeini -n ipgnrelayservers.ini %x bookedat $time(h:nntt dddd)
            writeini -n ipgnrelayservers.ini %x active 1

            .timerresetipgnrelay $+ %x 1 $calc(4 * 3600) ipgnRelayReset %x
            .timerrelayadvert $+ %x 0 960 rconbooking %ip %rconport %currentrcon tv_msg This relay is provided by the iPrimus Gaming Network; wait 300; tv_msg www.ipgn.com.au
            .timerrelaystatus $+ %x 0 30 rconbooking %ip %rconport %currentrcon tv_status

            msg #ipgn-tf2 $colours(Relay server %connectip $+ : $+ %port ( $+ %x $+ ) has been booked by $nick for 4 hours. Connecting to $2-)
            msg $chan $colours(Relay server %connectip $+ : $+ %port ( $+ %x $+ ) has been booked by $nick for 4 hours. Connecting to $2-)

            rconbooking %ip %rconport %currentrcon tv_relay $2-
            break
          }
          else {
            inc %x
          }
        }
      }
      else {
        msg $chan $colours(You must specify a valid IP address)
      }
    }
    if ($1 = !timerelay) {
      if ($0 < 4) { msg $chan $colours(Usage: !timerelay <time> <tv ip> <announce msg>. eg !timerelay 8:30pm $iif($3,$ifmatch,202.138.3.30:27016) OWL5 DIVISION 1 :: JOHN SMITH) | return }
      var %numbooked = $readini(ipgnrelayservers.ini,global,numbooked)
      if (%numbooked = $calc($ini(ipgnrelayservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | return }
      if (!$regex($gettok($3,1,58),\b(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}\b))) && (!$regex($gettok($3,1,58),(.*)\.com)) { msg $chan $colours(Invalid server specified) | return }
      if (!$regex($2,(\d+)\:(\d+)(am|pm))) { msg $chan $colours(Invalid time specified. EG: 8:30pm, 10:30am, 11:30am, etc) | return }
      if (!$4) { msg $chan $colours(You need to specify the announcer message. eg !timerelay 8:30pm $iif($3,$ifmatch,202.138.3.30:27016) OWL5 DIVISION 1 ILIKEMEN) | return }

      var %bookingtime = $timeBookingCalculate($2)
      if ($gettok(%bookingtime,1,37) < $ctime) { msg $chan $colours(You can't book a server in the past!) | return }
      var %x = 1, %numrelays = $calc($ini(ipgnrelayservers.ini,0) - 1)
      while (%x <= %numrelays) {
        if ($readini(ipgnrelayservers.ini,%x,booked) = 0) {
          var %connectip = $readini(ipgnrelayservers.ini,%x,connectip)
          var %port = $readini(ipgnrelayservers.ini,%x,port)
          var %numbooked = $readini(ipgnrelayservers,global,numbooked)

          writeini -n ipgnrelayservers.ini global numbooked $calc(%numbooked + 1)
          writeini -n ipgnrelayservers.ini %x booked 1
          writeini -n ipgnrelayservers.ini %x booker $nick
          writeini -n ipgnrelayservers.ini %x duration 4
          writeini -n ipgnrelayservers.ini %x relay $3
          writeini -n ipgnrelayservers.ini %x bookedat $time(h:nntt dddd)
          writeini -n ipgnrelayservers.ini %x bookingtime $gettok(%bookingtime,1,37)
          writeini -n ipgnrelayservers.ini %x announce $4-

          msg #ipgn-tf2 $colours(Relay server %connectip $+ : $+ %port ( $+ %x $+ ) has been prebooked by $nick $+ . This booking will last 4 hours and will start relaying at $2 $time(dddd))
          msg $chan $colours(Relay server %connectip $+ : $+ %port ( $+ %x $+ ) has been prebooked by $nick $+ . This booking will last 4 hours and will start relaying at $2 $time(dddd))

          ;rconbooking %ip %rconport %currentrcon tv_relay $2-
          break
        }
        else {
          inc %x
        }
      }
    }
    if ($1 = !announce) {
      if ($2) && ($2 isnum) && ($3) {
        if (!$ini(ipgnrelayservers.ini,$2)) { msg $chan $colours(Invalid server specified) | return }
        if ($3 = off) {
          writeini -n ipgnrelayservers.ini $2 announce 0
          msg $chan $colours(Announcing timer for relay $2 has been stopped)
        }
        else {
          writeini -n ipgnrelayservers.ini $2 announce $3-
          if (!$timer(announce)) {
            .timerannounce 0 $calc(8*60) relayAnnounce
          }
          relayAnnounce $2
          msg $chan Now announcing " $+ $3- $+ " for relay $2
        }
      }
    }
    :ozfIpgnRelay
    if ($1 = !reset) && ($2 = relay) {
      if ($3 !isnum) { msg $chan $colours(You must specify the number of the relay. Eg !reset relay 1) | return }
      if ($readini(ipgnrelayservers.ini,$3,booked)) {
        ipgnRelayReset $3
      }
      else {
        if ($3 > $calc($ini(ipgnrelayservers.ini,0) - 1)) { 
          msg $chan $colours(There are currently only $calc($ini(ipgnrelayservers.ini,0) - 1) relay servers) 
        }
        else {
          msg $chan $colours(Relay $3 isn't in use and does not need to be reset.)
        }
      }
    }
    if ($1 = !relayservers) {
      var %x = 1
      while (%x <= $calc($ini(ipgnrelayservers.ini,0) - 1)) {
        var %connectip = $readini(ipgnrelayservers.ini,%x,connectip)
        var %port = $readini(ipgnrelayservers.ini,%x,port)
        var %relay = $readini(ipgnrelayservers.ini,%x,relay)
        var %announce = $readini(ipgnrelayservers.ini,%x,announce)
        var %active = $readini(ipgnrelayservers.ini,%x,active)
        var %bookingtime = $readini(ipgnrelayservers.ini,%x,bookingtime)
        var %booker = $readini(ipgnrelayservers.ini,%x,booker)
        var %booked = $readini(ipgnrelayservers.ini,%x,booked)

        notice $nick $colours($+(%connectip,:,%port) ( $+ %x $+ ) Status:  $iif(!%booked && !%bookingtime,Available) $iif(%booked,Booked by %booker) $&
          $iif(%bookingtime && !%active,Activates in $duration($calc(%bookingtime - $ctime))) $iif(%relay,Relay target: %relay) $iif(%announce,Announce: %announce))

        inc %x
      }
    }
    if ($1 = !rconbookable) {
      if ((exec isin $1-) || (status isin $1-)) {
        msg $chan $colours(One or more of your rcon commands is disallowed)
        return
      }

      if ($ini(ozfservers.ini,$2)) {
        var %currentrcon = $readini(ozfservers.ini,$2,rcon)
        var %ip = $readini(ozfservers.ini,$2,ip)
        var %port = $readini(ozfservers.ini,$2,port)
        rconbooking %ip %port %currentrcon $chan $3-
      }
    }
    if ($1 = !addemail) {
      if (!$2) {
        msg $chan $colours(Syntax: !addemail <email> <yes/no (whether you want to receive booking expiry spam or not)>)
        return
      }

      if ($regex($2,([a-zA-Z0-9\_\-]+)@(.*))) {
        writeini ozfemails.ini emails $nick $2
        if ($3 = yes) {
          writeini ozfemails.ini spammable $nick 1
          msg $chan $colours(Added $nick with email $2 (Spammable: Yes))
        }
        else {
          writeini ozfemails.ini spammable $nick 0
          msg $chan $colours(Added $nick with email $2 (Spammable: No))
        }
      }
      else {
        msg $chan $colours(Invalid e-mail address specified. Format: asdf@example.com)
      }
    }
    if ($1 = !mail) {
      if ((!$2) || (!$3) || (!$4)) {
        msg $chan $colours(Syntax: !mail email/name;email/name;etc Subject: Message)
        return
      }
      else {
        var %mailtos = $2, %subject, %message
        if ($regex($3-,/(.+?):\s+(.*)/)) {
          var %subject = $regml(1), %message = $regml(2)
        }
        else {
          msg $chan $colours(Error parsing command)
          return
        }

        var %x = 1, %y = $numtok(%mailtos,59)
        while (%x <= %y) {
          var %to = $gettok(%mailtos,%x,59)
          if ($readini(ozfemails.ini,emails,%to)) {
            %mailtos = $reptok(%mailtos,%to,$v1,1,59)
          }
          elseif (!$regex(%to,([a-zA-Z0-9\_\-]+)@(.*))) {
            msg $chan $colours(Invalid e-mail/user specified ( $+ %to $+ ). Excluding)
            %mailtos = $remtok(%mailtos,%to,1,59)
          }
          inc %x
        }
        msg $chan $colours(Mailto: %mailtos Subject: %subject Message: %message)
        sendmail $+(%mailtos,$chr(37),%subject,$chr(37),%message)
      }
    }

    if ($1 = !bookingstats) {
      ;check if the logs file is in use
      if ($exists(%statsdir $+ bookings.lock)) {
        notice $nick $colours(Bookings stats is currently busy. Please try again shortly)
        return
      }

      ;now we need to lock the logs file, so it doesn't attempt to get written to while we're using it
      write %statsdir $+ bookings.lock 1

      .fopen bookinglog %statsdir $+ bookings.log

      if ($fopen(bookinglog).err) {
        msg $chan $colours(Error opening booking log file)
        .fclose bookinglog
        return
      }

      var %x = 1

      var %totalbooked = 0, %totalreset = 0, %avgduration = 0, %totalduration = 0, %firstbookingdate

      while (!$eof(bookinglog)) {
        .fseek -l bookinglog %x

        var %line = $fread(bookinglog)

        if (!%firstbookingdate) {
          var %firstbookingdate = $gettok(%line,6,32)
        }

        if ($gettok(%line,3,32) == BOOKED) {
          inc %totalbooked

          var %sip = $gettok(%line,1,32)
          var %sport = $gettok(%line,2,32)

          ;check for corresponding reset
          .fseek -w bookinglog %sip $+ $chr(32) $+ %sport $+ $chr(32) $+ RESET*
          if (!$eof(bookinglog)) {
            inc %totalreset

            var %resetline = $fread(bookinglog)
            if ($gettok(%resetline,6,32) == $gettok(%line,6,32)) {
              ;booked and reset on the same day... good enough for me for now!
              var %duration = $calc($gettok(%resetline,5,32) - $gettok(%line,5,32))

              inc %totalduration %duration
            }
          }
          else {
            .fseek -l bookinglog %x
          }
        }

        inc %x
      }

      .fclose bookinglog

      ;now we clear the lock for other functions
      .remove %statsdir $+ bookings.lock

      msg $chan $colours(Total number of servers booked since %firstbookingdate $+ : %totalbooked)
      msg $chan $colours(Average booking length: $duration($calc(%totalduration / %totalreset)))

    }
  }
}

alias ozfCheckPreBookings {
  var %y = $calc($ini(ozfservers.ini,0) - 1)
  var %x = 1
  while (%x <= %y) {
    var %bookingtime = $readini(ozfservers.ini,%x,bookingtime)
    if (%bookingtime) && (!$readini(ozfservers.ini,%x,active)) {
      if ($ctime > %bookingtime) {
        var %rcon = $lower($read(passwords.txt))
        var %pass = $lower($read(passwords.txt))
        var %currentrcon = $readini(ozfservers.ini,%x,rcon)
        var %ip = $readini(ozfservers.ini,%x,ip)
        var %port = $readini(ozfservers.ini,%x,port)
        var %booker = $readini(ozfservers.ini,%x,booker)
        var %team = $readini(ozfservers.ini,%x,team)
        var %duration = $readini(ozfservers.ini,%x,duration)
        var %bookedat = $readini(ozfservers.ini,%x,bookedat)

        writeini -n ozfservers.ini %x password %pass
        writeini -n ozfservers.ini %x rcon %rcon
        writeini -n ozfservers.ini %x active 1

        .timerresetozf $+ %x 1 $calc(%duration * 3600) ozfServerReset %x

        msg #ozf-help $colours(Server %x has been booked by %booker for %team $+ . This booking lasts %duration hours. Details have also been PMed to %team)
        msg %team $colours(Details for your booking at $asctime(%bookingtime,h:nntt) $+ : $connectString(%ip,%port,%pass,%rcon))
        if (%booker ison #ozf-help) {
          notice %booker $colours($connectString(%ip,%port,%pass,%rcon)) 
        }

        rconbooking %ip %port %currentrcon say This server has been booked for %duration hours $+ .; sv_password %pass ; kickall; wait 500; rcon_password %rcon ; changelevel cp_granary; mr_ipgnbooker %team
        if (%type = ozfortress) {
          setHeavyDetails %port %rcon %pass %team
        }

        ozfAddServerStats BOOKED %x %booker $ctime $date(yyyy-mm-dd)
      }
    }
    inc %x
  }
}

alias ozfServerReset {
  var %currentrcon = $readini(ozfservers.ini,$1,rcon)
  var %ip = $readini(ozfservers.ini,$1,ip)
  var %port = $readini(ozfservers.ini,$1,port)
  var %booker = $readini(ozfservers.ini,$1,booker)
  var %team = $readini(ozfservers.ini,$1,team)
  var %duration = $readini(ozfservers.ini,$1,duration)
  var %bookedat = $readini(ozfservers.ini,$1,bookedat)
  var %rcon = $readini(ozfservers.ini,$1,defaultrcon)
  var %numbooked = $readini(ozfservers.ini,global,numbooked)
  var %type = $readini(ozfservers.ini,$1,type)

  writeini -n ozfservers.ini $1 rcon %rcon
  writeini -n ozfservers.ini global numbooked $calc(%numbooked - 1)

  rconbooking %ip %port %currentrcon say This server has been reset by $iif($nick,$ifmatch,$me) $+ . ; rcon_password %rcon ; mr_ipgnbooker unbooked; livelogs_name default

  if (%type = ozfortress) {
    setHeavyDetails %port %rcon nogo ozf_unbooked
  }

  if ($hget(ozfhash,%booker)) {
    hdel ozfhash %booker
  }

  if ($hget(ozfhash,$1)) {
    hdel ozfhash $1
  }

  if ($hget(ozfhash,%team)) {
    hdel ozfhash %team
  }

  ozfIniReset $1
  .timerresetozf $+ $1 off
  .timerclearozfauto $+ $1 off

  msg #ozf-help $colours(Server $1 ( $+ %type $+ ) booked by %booker for %team at %bookedat for %duration hours has been reset.)

  ozfAddServerStats RESET $1 %booker $ctime $date(yyyy-mm-dd)
}

alias ozfIniReset {
  writeini -n ozfservers.ini $1 booked 0
  writeini -n ozfservers.ini $1 booker 0
  writeini -n ozfservers.ini $1 duration 0
  writeini -n ozfservers.ini $1 team 0
  writeini -n ozfservers.ini $1 password 0
  writeini -n ozfservers.ini $1 bookedat 0
  writeini -n ozfservers.ini $1 bookingtime 0
  writeini -n ozfservers.ini $1 active 0
}

alias ozfResetAll {
  var %x = 1, %numservers = $calc($ini(ozfservers.ini,0) - 1)
  while (%x <= %numservers) {
    if ($readini(ozfservers.ini,%x,booked)) {
      ozfServerReset %x
    }

    inc %x
  }
  writeini -n ozfservers.ini global numbooked 0
}

alias ozfCheckServerStatus {
  var %x = 1, %y = $calc($ini(ozfservers.ini,0) - 1)
  while (%x <= %y) {
    if ($readini(ozfservers.ini,%x,booked) && $readini(ozfservers.ini,%x,active)) {
      var %currentrcon = $readini(ozfservers.ini,%x,rcon)
      var %ip = $readini(ozfservers.ini,%x,ip)
      var %port = $readini(ozfservers.ini,%x,port)

      rconbooking %ip %port %currentrcon status
    }
    inc %x
  }
}

alias setHeavyDetails {
  ;setHeavyDetails %port %rcon %pass $4
  if ((sandvich ison #ozf-help) && (sandvich isop #ozf-help)) || ($address(sandvich,2) == *!*@heavy.ozfortress.com) {
    msg sandvich SET_HEAVY_SERVER_DETAILS $1 $2 $3 $4
  }     
}

alias ozfAddServerStats {
  ;ozfAddServerStats booked/reset serverid booker ctime date

  if ($exists(%statsdir $+ bookings.lock)) {
    .timer 1 1 ozfAddServerStats $1-
    return
  }

  write %statsdir $+ bookings.lock 1

  if ($1 == BOOKED || $1 == RESET) && ($5) {
    var %sid = $2
    var %sip = $readini(ozfservers.ini,%sid,ip)
    var %sport = $readini(ozfservers.ini,%sid,port)

    write %statsdir $+ bookings.log %sip %sport $1 $3 $4 $5
  }

  .remove %statsdir $+ bookings.lock
}
