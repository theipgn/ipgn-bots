;New booking script (for users to ask for a server/stv relay)

on *:TEXT:!*:?:{
  if ($1 = !requestserver) {
    msg $nick $colours(This code is currently deprecated and no longer functions correctly. It will be updated in the near future. Please ask an admin for assistance or visit #ozf-help)
    halt
    if ($2 !isnum) { msg $nick $colours(You may only specify a number for the duration of your booking. eg !requestserver 2 team amazing) | halt }
    if ($2 isnum) && ($2 > 4) { msg $nick $colours(You can only book a server for a maximum of 4 hours) | halt }
    if (%server. [ $+ [ $nick ] ]) {
      msg $nick $colours(You either already have a server booked or a server booking pending. Please wait until the booking has ended or been accepted/declined)
      halt
    }
    if ($2) && ($3) {
      if ($read(bookableservers.txt,w, * $+ Not currently booked $+ *)) {
        set %server. [ $+ [ $nick ] ] 1
        set %server. [ $+ [ $nick ] $+ .time ] $2
        set %server. [ $+ [ $nick ] $+ .team ] $3-
        msg $nick $colours(Waiting for administrator approval...)
        msg %admin.chan $colours($nick is requesting a server for $2 hours. Clan: $3-)
        msg %admin.chan $colours(To accept this booking, type !accept $nick or !decline $nick <reason> to refuse)
      }
      else {
        msg $nick $colours(Sorry, there are currently no servers available)
      }
    }
    else {
      msg $nick $colours(You need to set it out like this: !requestserver <length of booking> <team name>)
      msg $nick $colours(Eg. !requestserver 2 cool kids)
    }
  }
  if ($1 = !requestrelay) {
    msg $nick $colours(This code is currently deprecated and no longer functions correctly. It will be updated in the near future. Please ask an admin for assistance)
    halt
    if ($2 !isnum) { msg $nick $colours(You may only specify a positive integer for the duration of your booking. eg !requestrelay 2 12.34.56.789) | halt }
    if ($2 > 3) {  msg $nick $colours(You can only book a server for a maximum of 3 hours) | halt }
    if (%relay. [ $+ [ $nick ] ]) {
      msg $nick $colours(You either already have a relay booked or a relay booking pending. Please wait until the booking has ended or been accepted/declined)
      halt
    }
    if ($2) && ($3) {
      if ($regex($3,\b(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}\b))) {
        if ($read(bookablerelay.txt,w,* $+ Available $+ *)) {
          set %relay. [ $+ [ $nick ] ] 1
          set %relay. [ $+ [ $nick ] $+ .time ] $2
          set %relay. [ $+ [ $nick ] $+ .server ] $3
          msg $nick $colours(Waiting for administrator approval...)
          msg %admin.chan $colours($nick is requesting a relay server for $2 hours. The server being relayed is: $3-)
          msg %admin.chan $colours(To allow this relay connection, type !accept $nick or !decline $nick <reason> to refuse)
        }
        else {
          msg $nick $colours(Sorry, there are currently no relay servers available)
        }
      }
      else {
        msg $nick $colours(You have entered an invalid format.)
        msg $nick $colours(You need to set it out like this: !requestrelay <length of booking> <server>)
        msg $nick $colours(Eg. !requestrelay 2 210.50.4.30:27016)
      }
    }
    else {
      msg $nick $colours(You need to set it out like this: !requestrelay <length of booking> <server>)
      msg $nick $colours(Eg. !requestrelay 2 210.50.4.30:27016)
    }
  }
}

on *:TEXT:!*:%admin.chan:{
  if ($1 = !accept) && ($2) {
    if (%relay. [ $+ [ $2 ] ] = 1) {
      inc %relay. [ $+ [ $2 ] ]
      msg $2 $colours(Your request has been accepted $+ $chr(44) connecting relay to %relay. [ $+ [ $2 ] $+ .server ] $+ . You will receive the details shortly)
      msg %admin.chan $colours(Relay request for %relay. [ $+ [ $2 ] $+ .server ] accepted.)
      setupRelay $2 %relay. [ $+ [ $2 ] $+ .time ] %relay. [ $+ [ $2 ] $+ .server ]
    }
    elseif (%server. [ $+ [ $2 ] ] = 1) {
      inc %server. [ $+ [ $2 ] ]
      msg $2 $colours(Your server request has been accepted. You will be issued the details shortly)
      msg %admin.chan $colours(Accepted server request for %server. [ $+ [ $2 ] $+ .team ] by $2 $+ .)
      setupServer $2 %server. [ $+ [ $2 ] $+ .time ] %server. [ $+ [ $2 ] $+ .team ]
    }
    else {
      msg %admin.chan $colours(No booking exists for user with nick $2)
    }
  }
  if ($1 = !decline) && ($2) && ($3) {
    if (%relay. [ $+ [ $2 ] ] = 1) {
      msg %admin.chan $colours(Declined relay for %relay. [ $+ [ $2 ] $+ .server ] requested by $2)
      msg $2 $colours(Your relay request has been declined for the following reason: $3- $+ . Please see $nick for further assistance)
      unset %relay. [ $+ [ $2 ] $+ * ]
    }
    elseif (%server. [ $+ [ $2 ] ] = 1) {
      msg %admin.chan $colours(Declined server request by $2)
      msg $2 $colours(Your server request has been declined for the following reason: $3- $+ . Please see $nick for further assistance)
      unset %server. [ $+ [ $2 ] $+ *]
    }
    else { 
      msg %admin.chan $colours(No booking exists for user with nick $2) 
    }
  }
}

alias setupRelay {
  if ($1) && ($2) && ($3) {
    var %name = $1
    var %time = $2
    var %server = $3
    var %relaycommand = tv_relay $3
    msg %admin.chan $colours(Setting up relay for $1 $+ , connecting to $3 for $2 hours.)
    tokenize 37 $read(bookablerelay.txt,w,* $+ Available $+ *,1)
    .timerresetrelay $+ $readn 1 $calc(%time * 3600) reset relay $readn
    write -l $+ $readn bookablerelay.txt $+($1,$chr(37),$2,$chr(37),$3,$chr(37),$4,$chr(37),Booked,$chr(37),%time,$chr(37),%name,$chr(37),%server)
    msg %admin.chan $colours(Relay server $1 $+ : $+ $2 connecting to %server)
    rconbooking $1 $3 $4 %relaycommand
    msg %name $colours(Your relay has been connected. The connected server is: $1 $+ : $+ $2)
  }
  else {
    msg %admin.chan $colours(Error encountered during relay setup: missing token)
  }
}

alias setupServer {
  if ($1) && ($2) && ($3) { 
    var %numbooked = $readini(ipgnservers.ini,global,numbooked)
    if (%numbooked = $calc($ini(ipgnservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | halt }
    if (!$2) || (!$3) || ($2 > 4) {
      msg $nick $colours(You must specify the time the booking should last (maximum of 4 hours) and the name of the booking. eg !book 2 myteam)
      halt
    }
    var %x = 1, %numservers = $calc($ini(ipgnservers.ini,0) - 1)
    while (%x <= %numservers) {
      if ($readini(ipgnservers.ini,%x,booked) = 0) {
        var %rcon = $lower($read(passwords.txt))
        var %pass = $lower($read(passwords.txt))
        var %currentrcon = $readini(ipgnservers.ini,%x,rcon)
        var %ip = $readini(ipgnservers.ini,%x,ip)
        var %port = $readini(ipgnservers.ini,%x,port)
        var %numbooked = $readini(ipgnservers.ini,global,numbooked)
        writeini -n ipgnservers.ini global numbooked $calc(%numbooked + 1)
        writeini -n ipgnservers.ini %x booked 1
        writeini -n ipgnservers.ini %x active 1
        writeini -n ipgnservers.ini %x booker $1
        writeini -n ipgnservers.ini %x duration $2
        writeini -n ipgnservers.ini %x team $3-
        writeini -n ipgnservers.ini %x password %pass
        writeini -n ipgnservers.ini %x rcon %rcon
        writeini -n ipgnservers.ini %x bookedat $time(h:nntt dddd)

        .timerresetipgn $+ %x 1 $calc($2 * 3600) autoBookableReset server %x

        msg $chan $colours(Server %x has been booked by $1 for $3- $+ . This booking lasts $2 hours.)
        msg $1 $colours(An admin has accepted your request for a server.)
        msg $1 $colours($connectString(%ip,%port,%pass,%rcon))

        rconbooking %ip %port %currentrcon say This server has been booked for $3 hours $+ .; sv_password %pass ; wait 1000; rcon_password %rcon ;changelevel cp_granary

        break
      }
      else {
        inc %x
      }
    }
  }
}


/* Relay Server file format:
Booked Format: IP%Connect Port%Rcon Port%Rcon Password%Status%booking time%booking user%server being relayed
$+($1,$chr(37),$2,$chr(37),$3,$chr(37),$4,$chr(37),Booked,$chr(37),%time,$chr(37),%name,$chr(37),%server)
Unbooked format: IP%Connect Port%Rcon Port%Rcon Password%Status
*/

alias autoBookableReset {
  ;reset <servertype> <id> 
  if ($1) && ($2) {
    if ($1 = relay) {
      var %relayserver = $2
      tokenize 37 $read(bookablerelay.txt,$2)
      unset %relay. [ $+ [ $7 ] $+ * ]
      msg %admin.chan $colours(Resetting relay server %relayserver ( $+ $1 $+ : $+ $2 $+ ))
      rconbooking $1 $3 $4 tv_relay 210.50.4.30:27016
      msg %admin.chan $colours(Relay server $1 $+ : $+ $2 has been reset)
    }
    elseif ($1 = server) {
      unset % [ $+ server. $+ [ $gettok($read(bookableservers.txt,$2),6,37) ] $+ * ]
      ipgnServerReset $2
    }
    else {
      msg %admin.chan $colours(Error encountered during reset: No known server type " $+ $1 $+ ")
      halt
    }
  }
  else {
    msg %admin.chan $colours(Error encountered during reset: Missing token)
  }
}
