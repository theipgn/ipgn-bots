;Server bookings, including relays

on *:CONNECT:{
  .timeripgnbookingcheck 0 60 ipgnCheckPreBookings
  .timeripgnrelaybookingcheck 0 60 ipgnCheckRelayPreBookings
}

on *:NOTICE:*:*:{
  if (*I recognize you* iswm $1-) {
    ;if ($me != [iPGN-TF2B]) {
    ;  msg AuthServ@Services.GameSurge.net ghost [iPGN-TF2B]
    ;  .timer 1 10 nick [iPGN-TF2B]
    ;}
    .timer 1 10 join #ipgn-tf2
    .timer 1 10 join #ipgn
    .timer 1 10 join #tf2pug
    .timer 1 10 join #tf2pug2
    .timer 1 10 join #ozfortress
    .timer 1 10 join #nzfortress
    .timer 1 10 join #asiafortress
    .timer 1 10 join #ozf-help
  }
}

on *:TEXT:!stv:#: {
  var %x = 1, %y = $calc($ini(ipgnrelayservers.ini,0) - 1)

  while (%x <= %y) {
    var %announcemsg = $readini(ipgnrelayservers.ini,%x,announce)
    var %map = $readini(ipgnrelayservers.ini,%x,map)

    if ((%announcemsg) || (%map)) {
      var %ip = $readini(ipgnrelayservers.ini,%x,connectip)
      var %port = $readini(ipgnrelayservers.ini,%x,port)
      var %playersig = $readini(ipgnrelayservers.ini,%x,players)
      var %numspecs = $readini(ipgnrelayservers.ini,%x,numspecs)

      msg $chan $colours($iif(%announcemsg,12Match: $ifmatch) 12STV Relay: $connectString(%ip,%port) (07 $+ %numspecs specs (highest)) $iif(%map,12Map: $ifmatch (07 $+ %playersig players in-game)))
    }

    inc %x
  }
}

alias relayAnnounce {
  var %a, %b, %y, %x

  %y = $chan(0)
  %x = 1

  if ($1) {
    %a = $1
    %b = $1
  }
  else {
    %a = $calc($ini(ipgnrelayservers.ini,0) - 1)
    %b = 1
  }

  while (%b <= %a) {
    var %ip = $readini(ipgnrelayservers.ini,%b,connectip)
    var %port = $readini(ipgnrelayservers.ini,%b,port)
    var %announcemsg = $readini(ipgnrelayservers.ini,%b,announce)
    var %map = $readini(ipgnrelayservers.ini,%b,map)
    var %playersig = $readini(ipgnrelayservers.ini,%b,players)
    var %numspecs = $readini(ipgnrelayservers.ini,%b,numspecs)

    ;echo Checking relay %b

    if ((!%announcemsg) || (!%playersig)) {
      inc %b
      continue
    }

    while (%x <= %y) {
      if ($chan(%x) != #ipgn-tf2) && ($chan(%x) != #ozf-help) && ($chan(%x) != #ozfl) && ($chan(%x) != #tf2pug2) && ($chan(%x) != #ultiduo) && ($chan(%x) != #tf2pug.kills) {
        msg $chan(%x) $colours($colourcode(blue,Match:) %announcemsg $colourcode(blue,STV Relay:) $connectString(%ip,%port) (07 $+ %numspecs specs (highest)) $&
          $iif(%map,$colourcode(blue,Map:) %map (07 $+ %playersig players in-game)))
      }

      inc %x
    }

    inc %b
  }
}


on *:TEXT:!*:#ipgn-tf2: {
  if ($1 = !acmdlist) {
    notice $nick $colours(Special admin only commands are:)
    notice $nick $colours(!book <time> <booked team>. Eg. !book 2 beastin. Books one of the bookable servers and gives out an rcon password.)
    notice $nick $colours(!bookserver <server number> <time> <team>. eg !book 2 3 yomumma
    notice $nick $colours(!status <server>. Specifying a server is optional. Shows status of bookable servers (if server is booked or not). eg !serverstatus 1)
    notice $nick $colours(!details <server> - gives you the details for specified server)
    notice $nick $colours(!resetall Sets all passwords/rcon passwords of bookable servers back to default)
    notice $nick $colours(!reset <server>. Resets a specific server. EG: !resetserver 1)
    notice $nick $colours(!banid <STEAMID> <nick> <reason>. Eg. !banid STEAM_0:0:12345 bladez king of da woild)
    notice $nick $colours(!rcon <server> <command>. Sends rcon <command> to specified <server>. Eg !rcon 1 say HAY GUYZ)
  }
  if ($1 = !query) {
    if ($2) && (!3) && ($regex($gettok($2,1,58),\b(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}\b))) {
      sourceQuery squery. [ $+ [ $ticks ] ] $chan $gettok($2,1,58) $gettok($2,2,58)
    }
    elseif ($2) && ($3) && ($regex($2,\b(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}\b))) {
      sourceQuery squery. [ $+ [ $ticks ] ] $chan $2 $3
    }
    else {
      msg $chan $colours(Invalid server address specified)
    }
  }
  if ($1 = !announce) {
    if ($2) && ($2 isnum) && ($3) {
      if (!$ini(ipgnrelayservers.ini,$2)) { msg $chan $colours(Invalid server specified) | return }
      if ($3 = off) {
        writeini -n ipgnrelayservers.ini $2 announce 0
        msg $chan $colours(Announcing timer for relay $2 has been stopped)
      }
      else {
        writeini -n ipgnrelayservers.ini $2 announce $3-
        if (!$timer(announce)) {
          .timerannounce 0 $calc(8*60) relayAnnounce
        }
        relayAnnounce $2
        msg $chan Now announcing " $+ $3- $+ " for relay $2
      }
    }
  }
  if ($1 = !massmsg) {
    var %x = $chan(0)
    var %y = 1
    while (%y <= %x) {
      if ($chan(%y) != #ipgn-tf2) && ($chan(%y) != #ozf-help) {
        msg $chan(%y) $colours($2-)
      }
      inc %y
    }
  }
  if ($1 = !status) {
    if ($2 = relay) {
      if ($3) && ($3 isnum) && ($3 <= $calc($ini(ipgnrelayservers.ini,0) - 1)) {
        var %ip = $readini(ipgnrelayservers.ini,$3,ip)
        var %connectip = $readini(ipgnrelayservers.ini,$3,connectip)
        var %port = $readini(ipgnrelayservers.ini,$3,port)
        var %booked = $readini(ipgnrelayservers.ini,$3,booked)
        var %booker = $readini(ipgnrelayservers.ini,$3,booker)
        var %relay = $readini(ipgnrelayservers.ini,$3,relay)
        var %rconport = $readini(ipgnrelayservers.ini,$3,rconport)
        var %announcemsg = $readini(ipgnrelayservers.ini,$3,announce)
        var %bookingtime = $readini(ipgnrelayservers.ini,$3,bookingtime)
        var %active = $readini(ipgnrelayservers.ini,$3,active)

        msg $chan $colours($colourcode(blue,IP:) %connectip $colourcode(blue,Port:) %port $colourcode(blue,Booked:) $iif(%booked,Yes ( $+ %booker $+ ),No))
        if (%bookingtime) {
          msg $chan $colours($colourcode(blue,Pre-booked for:) $asctime(%bookingtime,h:nntt dddd) $iif(%active,(Active),(Activates in $duration($calc(%bookingtime - $ctime)) $+ )))
        }
        if (%booked) && (%active) {
          msg $chan $colours($colourcode(blue,Connected to:) %relay $iif(%announcemsg,$colourcode(blue,Last announce:) %announcemsg)) 
        }
        if (%booked) && (!%active) {
          msg $chan $colours($colourcode(blue,Target:) %relay $iif(%announcemsg,$colourcode(blue,Announce:) %announcemsg))
        }
      }
      elseif ($3 > $calc($ini(ipgnrelayservers.ini,0) - 1)) {
        notice $nick $colours(There are currently only $calc($ini(ipgnrelayservers.ini,0) - 1) relay servers)
      }
      else {
        notice $nick $colours(ERROR: Incorrect input. !status relay <relay number> (1 - $calc($ini(ipgnrelayservers.ini,0) - 1)))
      }
    }
    if ($2) && ($2 isnum) && ($2 <= $calc($ini(ipgnservers.ini,0) - 1)) {
      var %currentrcon = $readini(ipgnservers.ini,$2,rcon)
      var %ip = $readini(ipgnservers.ini,$2,ip)
      var %port = $readini(ipgnservers.ini,$2,port)
      var %booker = $readini(ipgnservers.ini,$2,booker)
      var %team = $readini(ipgnservers.ini,$2,team)
      var %duration = $readini(ipgnservers.ini,$2,duration)
      var %bookedat = $readini(ipgnservers.ini,$2,bookedat)
      var %booked = $readini(ipgnservers.ini,$2,booked)
      var %bookingtime = $readini(ipgnservers.ini,$2,bookingtime)
      var %bookingactive = $readini(ipgnservers.ini,$2,active)

      msg $chan $colours($colourcode(orange,IP:) %ip $colourcode(orange,Port:) %port $colourcode(orange,Rcon:) %currentrcon)
      msg $chan $colours($colourcode(orange,Booked:) $iif(%booked,Yes,No) $iif(%booked,$colourcode(orange,Booker:) %booker $colourcode(orange,Booked for:) %team,$nil))
      if (%booked) {
        msg $chan $colours($colourcode(orange,Booked at:) %bookedat $colourcode(orange,Duration:) %duration hours)
      }
      if (%bookingtime) {
        msg $chan $colours($colourcode(orange,Pre-booked for:) $asctime(%bookingtime,h:nntt dddd) $&
          $iif(!%bookingactive,(Not yet active; $duration($calc(%bookingtime - $ctime)) remaining),(Active)))
      }
    }
  }
  if ($1 = !resetall) {
    :resetall
    var %x = 1, %numservers = $calc($ini(ipgnservers.ini,0) - 1)
    writeini -n ipgnservers.ini global numbooked 0
    while (%x <= %numservers) {
      ipgnServerReset %x
      inc %x
    }
    writeini -n ipgnservers.ini global numbooked 0
    msg $chan $colours(All servers have been reset)
  }
  if ($1 = !reset) {
    if ($2 = relay) {
      if ($3 !isnum) { msg $chan $colours(You must specify the number of the relay. Eg !reset relay 1) | return }
      if ($readini(ipgnrelayservers.ini,$3,booked)) {
        ipgnRelayReset $3
      }
      else {
        if ($3 > $calc($ini(ipgnrelayservers.ini,0) - 1)) { 
          msg $chan $colours(There are currently only $calc($ini(ipgnrelayservers.ini,0) - 1) relay servers) 
        }
        else {
          msg $chan $colours(Relay $3 isn't in use and does not need to be reset.)
        }
      }
    }
    else {
      if ($2 !isnum) { msg $chan $colours(You must specify the number of the server. Eg, !reset 2) | return }
      if ($readini(ipgnservers.ini,$2,booked)) {
        ipgnServerReset $2
      }
      else {
        msg $chan $colours(Server $2 isn't in use and does not need to be reset.)
      }
    }
  }
  ;!timebook <time> <server> <duration> <team>
  if ($1 = !timebook) && ($3) && ($3 <= $calc($ini(ipgnservers.ini,0) - 1)) && ($3 >= 1) && ($4 isnum) && ($5) {
    var %numbooked = $readini(ipgnservers.ini,global,numbooked)
    if (%numbooked = $calc($ini(ipgnservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | return }
    if (!$ini(ipgnservers.ini,$3)) { msg $chan $colours(Invalid server specified. Please check server IDs using !servers) | return }
    if (!$regex($2,(\d+)\:(\d+)(am|pm))) { msg $chan $colours(Invalid time specified. EG: 8:30pm, 10:30am, 11:30am, etc) | return }
    var %bookingtime = $timeBookingCalculate($2)
    if ($gettok(%bookingtime,1,37) < $ctime) { msg $chan $colours(You can't book a server in the past!) | return }
    if ($readini(ipgnservers.ini,$3,booked)) {
      var %booker = $readini(ipgnservers.ini,$3,booker)
      var %team = $readini(ipgnservers.ini,$3,team)
      var %duration = $readini(ipgnservers.ini,$3,duration)
      var %bookedat = $readini(ipgnservers.ini,$3,bookedat)
      var %bookingtime = $readini(ipgnservers.ini,$3,bookingtime)
      var %bookingactive = $readini(ipgnservers.ini,$3,active)
      if (%bookingtime) && (!%bookingactive) {
        msg $chan $colours(Server $3 was prebooked by %booker at %bookedat and will activate at $asctime(%bookingtime,h:nntt dddd) ( $+ $duration($calc(%bookingtime - $ctime)) $+ ))
      }
      else {
        msg $chan $colours(Server $3 is currently booked for %team $+ . This server was booked by %booker for a duration of %duration hours at %bookedat)
      }
    }
    else {
      var %numbooked = $readini(ipgnservers.ini,global,numbooked)
      writeini -n ipgnservers.ini global numbooked $calc(%numbooked + 1)
      writeini -n ipgnservers.ini $3 booked 1
      writeini -n ipgnservers.ini $3 booker $nick
      writeini -n ipgnservers.ini $3 duration $4
      writeini -n ipgnservers.ini $3 team $5-
      writeini -n ipgnservers.ini $3 bookedat $time(h:nntt dddd)
      writeini -n ipgnservers.ini $3 bookingtime $gettok(%bookingtime,1,37)

      msg $chan $colours(Server $3 has been booked by $nick for $5- $+ . This booking will last $4 hours and will activate at $2 on $time(dddd))
    }
  }
  if ($1 = !bookserver) && ($2 isnum) && ($3 isnum) && ($2 <= $calc($ini(ipgnservers.ini,0) - 1)) && (1 <= $2) && ($4) {
    var %numbooked = $readini(ipgnservers.ini,global,numbooked)
    if (%numbooked = $calc($ini(ipgnservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | return }
    if (!$ini(ipgnservers.ini,$2)) { msg $chan $codes(Invalid server specified. Please check server IDs using !servers) | return }
    if ($readini(ipgnservers.ini,$2,booked)) {
      var %booker = $readini(ipgnservers.ini,$2,booker)
      var %team = $readini(ipgnservers.ini,$2,team)
      var %duration = $readini(ipgnservers.ini,$2,duration)
      var %bookedat = $readini(ipgnservers.ini,$2,bookedat)
      var %bookingtime = $readini(ipgnservers.ini,$2,bookingtime)
      var %bookingactive = $readini(ipgnservers.ini,$2,active)
      if (%bookingtime) && (!%bookingactive) {
        msg $chan $colours(Server $2 was prebooked by %booker at %bookedat and will activate at $asctime(%bookingtime,h:nntt dddd) ( $+ $duration($calc(%bookingtime - $ctime)) $+ ))
      }
      else {
        msg $chan $colours(Server $2 is currently booked for %team $+ . This server was booked by %booker for a duration of %duration hours at %bookedat)
      }
    }
    else {
      var %rcon = $lower($read(passwords.txt))
      var %pass = $lower($read(passwords.txt))
      var %currentrcon = $readini(ipgnservers.ini,$2,rcon)
      var %ip = $readini(ipgnservers.ini,$2,ip)
      var %connectip = $readini(ipgnservers.ini,$2,connectip)
      var %port = $readini(ipgnservers.ini,$2,port)
      var %numbooked = $readini(ipgnservers.ini,global,numbooked)
      writeini -n ipgnservers.ini global numbooked $calc(%numbooked + 1)
      writeini -n ipgnservers.ini $2 booked 1
      writeini -n ipgnservers.ini $2 active 1
      writeini -n ipgnservers.ini $2 booker $nick
      writeini -n ipgnservers.ini $2 duration $3
      writeini -n ipgnservers.ini $2 team $4-
      writeini -n ipgnservers.ini $2 password %pass
      writeini -n ipgnservers.ini $2 rcon %rcon
      writeini -n ipgnservers.ini $2 bookedat $time(h:nntt dddd)

      .timerresetipgn $+ $2 1 $calc($3 * 3600) ipgnServerReset $2

      msg $chan $colours(Server $2 has been booked by $nick for $4- $+ . This booking lasts $3 hours.)
      notice $nick $colours($connectString(%connectip,%port,%pass,%rcon))

      if (($4 ison #ozf-help) || ($4 ison #ozfortress)) && ($4 != $nick) {
        msg $4 $colours(Server %x has been booked by $nick for you. This booking lasts $3 hours. Details: $connectString(%connectip,%port,%pass,%rcon))
        notice $4 $colours(Server %x has been booked by $nick for you. This booking lasts $3 hours. Details: $connectString(%connectip,%port,%pass,%rcon))
      }

      rconbooking %ip %port %currentrcon say This server has been booked for $3 hours $+ .; sv_password %pass ; mr_ipgnbooker $4- ; livelogs_name $4- ; rcon_password %rcon
    }
  }
  if ($1 = !book) && ($2 isnum) && (1 <= $2 <= 5) && ($3 !isnum) {
    var %numbooked = $readini(ipgnservers.ini,global,numbooked)
    if (%numbooked = $calc($ini(ipgnservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | return }
    if (!$2) || (!$3) || ($2 > 10) {
      msg $chan $colours(You must specify the time the booking should last (maximum of 4 hours) and the name of the booking. eg !book 2 myteam)
      return
    }
    var %x = 1, %numservers = $calc($ini(ipgnservers.ini,0) - 1)
    while (%x <= %numservers) {
      if ($readini(ipgnservers.ini,%x,booked) = 0) {
        var %rcon = $lower($read(passwords.txt))
        var %pass = $lower($read(passwords.txt))
        var %currentrcon = $readini(ipgnservers.ini,%x,rcon)
        var %ip = $readini(ipgnservers.ini,%x,ip)
        var %connectip = $readini(ipgnservers.ini,%x,connectip)
        var %port = $readini(ipgnservers.ini,%x,port)
        var %numbooked = $readini(ipgnservers.ini,global,numbooked)
        writeini -n ipgnservers.ini global numbooked $calc(%numbooked + 1)
        writeini -n ipgnservers.ini %x booked 1
        writeini -n ipgnservers.ini %x active 1
        writeini -n ipgnservers.ini %x booker $nick
        writeini -n ipgnservers.ini %x duration $2
        writeini -n ipgnservers.ini %x team $3-
        writeini -n ipgnservers.ini %x password %pass
        writeini -n ipgnservers.ini %x rcon %rcon
        writeini -n ipgnservers.ini %x bookedat $time(h:nntt dddd)

        .timerresetipgn $+ %x 1 $calc($2 * 3600) ipgnServerReset %x

        msg $chan $colours(Server %x has been booked by $nick for $3 $+ . This booking lasts $2 hours.)
        notice $nick $colours($connectString(%ip,%port,%pass,%rcon))

        if (($3 ison #ozf-help) || ($3 ison #ozfortress)) && ($3 != $nick) {
          msg $3 $colours(Server %x has been booked by $nick for you. This booking lasts $2 hours. Details: $connectString(%connectip,%port,%pass,%rcon))
          notice $3 $colours(Server %x has been booked by $nick for you. This booking lasts $2 hours. Details: $connectString(%connectip,%port,%pass,%rcon))
        }

        rconbooking %ip %port %currentrcon say This server has been booked for $3 hours.; sv_password %pass ; servercfgfile common_bookable.cfg; exec common_bookable.cfg; wait 1000; mr_ipgnbooker $3- ;rcon_password %rcon 

        break
      }
      else {
        inc %x
      }
    }
  }
  if ($1 = !details) {
    if ($2 = relay) {
      if ($3) && ($3 isnum) && ($3 <= $calc($ini(ipgnrelayservers.ini,0) - 1)) {
        if ($readini(ipgnrelayservers.ini,$3,booked)) {
          var %connectip = $readini(ipgnservers.ini,%x,connectip)
          var %port = $readini(ipgnrelayservers.ini,$3,port)

          notice $nick $colours(Details for relay $3 are: $connectString(%connectip,%port))
        }
      }
      else {
        notice $nick $colours(ERROR: Incorrect input. !details relay <relay number>)
      }
    }
    elseif ($2) && ($2 isnum) && ($2 <= $calc($ini(ipgnservers.ini,0) - 1)) {
      if ($readini(ipgnservers.ini,$2,booked)) && ($readini(ipgnservers.ini,$2,active)) {
        var %currentrcon = $readini(ipgnservers.ini,$2,rcon)
        var %connectip = $readini(ipgnservers.ini,$2,connectip)
        var %port = $readini(ipgnservers.ini,$2,port)
        var %pass = $readini(ipgnservers.ini,$2,password)

        notice $nick $colours(Details for server $2 are: $connectString(%connectip,%port,%pass,%currentrcon))
      }
      elseif ($readini(ipgnservers.ini,$2,bookingtime)) && (!$readini(ipgnservers.ini,$2,active)) {
        var %booker = $readini(ipgnservers.ini,$2,booker)
        var %team = $readini(ipgnservers.ini,$2,team)
        var %duration = $readini(ipgnservers.ini,$2,duration)
        var %bookedat = $readini(ipgnservers.ini,$2,bookedat)
        var %booked = $readini(ipgnservers.ini,$2,booked)
        var %bookingtime = $readini(ipgnservers.ini,$2,bookingtime)

        msg $chan $colours(Server $2 has been pre-booked by %booker for %team at $asctime(%bookingtime,h:nntt dddd) $+ . $&
          This server is not yet active and does not have details ( $+ $duration($calc(%bookingtime - $ctime)) remaining))
      }
      else {
        var %currentrcon = $readini(ipgnservers.ini,$2,rcon)
        var %connectip = $readini(ipgnservers.ini,%x,connectip)
        var %port = $readini(ipgnservers.ini,$2,port)
        var %pass = $readini(ipgnservers.ini,$2,password)
        notice $nick $colours(Server $2 is not currently booked. Details are: $connectString(%connectip,%port) $+ ; rcon_password %currentrcon)
      }
    }
    else {
      notice $nick $colours(ERROR: Incorrect input. !details <server number> or !details relay <server number>)
    }
  }
  if ($1 = !relay) {
    if (($regex($gettok($2,1,58),\b(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}\b))) || ($regex($gettok($2,1,58),(.*)\.com))) && (!$3) {
      var %numbooked = $readini(ipgnrelayservers.ini,global,numbooked)
      if (%numbooked = $calc($ini(ipgnrelayservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | return }
      var %x = 1, %numrelays = $calc($ini(ipgnrelayservers.ini,0) - 1)
      while (%x <= %numrelays) {
        if ($readini(ipgnrelayservers.ini,%x,booked) = 0) {
          var %currentrcon = $readini(ipgnrelayservers.ini,%x,rcon)
          var %ip = $readini(ipgnrelayservers.ini,%x,ip)
          var %connectip = $readini(ipgnrelayservers.ini,%x,connectip)
          var %port = $readini(ipgnrelayservers.ini,%x,port)
          var %rconport = $readini(ipgnrelayservers.ini,%x,rconport)
          var %numbooked = $readini(ipgnrelayservers,global,numbooked)
          writeini -n ipgnrelayservers.ini global numbooked $calc(%numbooked + 1)
          writeini -n ipgnrelayservers.ini %x booked 1
          writeini -n ipgnrelayservers.ini %x booker $nick
          writeini -n ipgnrelayservers.ini %x duration 4
          writeini -n ipgnrelayservers.ini %x relay $2-
          writeini -n ipgnrelayservers.ini %x bookedat $time(h:nntt dddd)
          writeini -n ipgnrelayservers.ini %x active 1

          .timerresetipgnrelay $+ %x 1 $calc(4 * 3600) ipgnRelayReset %x
          .timerrelayadvert $+ %x 0 960 rconbooking %ip %rconport %currentrcon tv_msg This relay is provided by the iPrimus Gaming Network; wait 300; tv_msg www.ipgn.com.au
          .timerrelaystatus $+ %x 0 30 rconbooking %ip %rconport %currentrcon tv_status

          msg $chan $colours(Relay server %connectip $+ : $+ %port ( $+ %x $+ ) has been booked by $nick for 4 hours. Connecting to $2-)

          rconbooking %ip %rconport %currentrcon tv_relay $2-
          break
        }
        else {
          inc %x
        }
      }
    }
    else {
      msg $chan $colours(You must specify a valid IP address)
    }
  }
  if ($1 = !timerelay) {
    if ($0 < 4) { msg $chan $colours(Usage: !timerelay <time> <tv ip> <announce msg>. eg !timerelay 8:30pm $3 OWL5 DIVISION 1 :: JOHN SMITH) | return }
    var %numbooked = $readini(ipgnrelayservers.ini,global,numbooked)
    if (%numbooked = $calc($ini(ipgnrelayservers.ini,0) - 1)) { msg $chan $colours(All servers are currently in use.) | return }
    if (!$regex($gettok($3,1,58),\b(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}\b))) && (!$regex($gettok($3,1,58),(.*)\.com)) { msg $chan $colours(Invalid server specified) | return }
    if (!$regex($2,(\d+)\:(\d+)(am|pm))) { msg $chan $colours(Invalid time specified. EG: 8:30pm, 10:30am, 11:30am, etc) | return }
    if (!$4) { msg $chan $colours(You need to specify the announcer message. eg !timerelay 8:30pm $3 OWL5 DIVISION 1 ILIKEMEN) | return }
    var %bookingtime = $timeBookingCalculate($2)
    if ($gettok(%bookingtime,1,37) < $ctime) { msg $chan $colours(You can't book a server in the past!) | return }
    var %x = 1, %numrelays = $calc($ini(ipgnrelayservers.ini,0) - 1)
    while (%x <= %numrelays) {
      if ($readini(ipgnrelayservers.ini,%x,booked) = 0) {
        var %connectip = $readini(ipgnrelayservers.ini,%x,connectip)
        var %port = $readini(ipgnrelayservers.ini,%x,port)
        var %numbooked = $readini(ipgnrelayservers,global,numbooked)
        writeini -n ipgnrelayservers.ini global numbooked $calc(%numbooked + 1)
        writeini -n ipgnrelayservers.ini %x booked 1
        writeini -n ipgnrelayservers.ini %x booker $nick
        writeini -n ipgnrelayservers.ini %x duration 4
        writeini -n ipgnrelayservers.ini %x relay $3
        writeini -n ipgnrelayservers.ini %x bookedat $time(h:nntt dddd)
        writeini -n ipgnrelayservers.ini %x bookingtime $gettok(%bookingtime,1,37)
        writeini -n ipgnrelayservers.ini %x announce $4-

        msg $chan $colours(Relay server %connectip $+ : $+ %port ( $+ %x $+ ) has been prebooked by $nick $+ . This booking will last 4 hours and will start relaying at $2 $time(dddd))

        ;rconbooking %ip %rconport %currentrcon tv_relay $2-
        break
      }
      else {
        inc %x
      }
    }
  }
  if ($1 = !redirect) {
    if ($0 < 3) { msg $chan $colours(Syntax: !redirect <relay id> <tv ip:port>) | return }
    if (!$regex($gettok($3,1,58),\b(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}\b))) && (!$regex($gettok($3,1,58),(.*)\.com)) { msg $chan $colours(Invalid server specified) | return }
    if ($ini(ipgnrelayservers.ini,2)) {
      var %currentrcon = $readini(ipgnrelayservers.ini,$2,rcon)
      var %ip = $readini(ipgnrelayservers.ini,$2,ip)
      var %connectip = $readini(ipgnrelayservers.ini,$2,connectip)
      var %port = $readini(ipgnrelayservers.ini,$2,port)
      var %rconport = $readini(ipgnrelayservers.ini,$2,rconport)
      var %booked = $readini(ipgnrelayservers.ini,$2,booked)
      if (!%booked) {
        msg $chan $colours(Relay server %connectip $+ : $+ %port ( $+ $2 $+ ) is not booked. Use !relay <target>)
        return
      }

      writeini -n ipgnrelayservers.ini $2 relay $3

      msg $chan $colours(Relay server %connectip $+ : $+ %port ( $+ $2 $+ ) has been redirected to $3)

      rconbooking %ip %rconport %currentrcon tv_relay $2-
    }
  }
  if ($1 = !rcon) {
    sourceRcon $2 $3 $4 $chan $5-
    ;rconbooking $2-
    ;msg $chan $colours(RCon command ( $+ $5- $+ ) has been sent to $2 $+ : $+ $3 using rcon password $4 $+ .)
  }
  if ($1 = !rconbookable) {
    if ((exec isin $1-) || (tv_status isin $1-)) {
      msg $chan $colours(Command unavailable)
      return
    }

    if ($ini(ipgnservers.ini,$2)) {
      var %currentrcon = $readini(ipgnservers.ini,$2,rcon)
      var %ip = $readini(ipgnservers.ini,$2,ip)
      var %port = $readini(ipgnservers.ini,$2,port)
      sourceRcon %ip %port %currentrcon $chan $3-
      ;msg $chan $colours(SENT!)
    }
  }
  if ($1 = !servers) {
    var %y = $calc($ini(ipgnservers.ini,0) - 1)
    var %x = 1
    while (%x <= %y) {
      var %currentrcon = $readini(ipgnservers.ini,%x,rcon)
      var %ip = $readini(ipgnservers.ini,%x,ip)
      var %connectip = $readini(ipgnservers.ini,%x,connectip)
      var %port = $readini(ipgnservers.ini,%x,port)
      var %booker = $readini(ipgnservers.ini,%x,booker)
      var %team = $readini(ipgnservers.ini,%x,team)
      var %duration = $readini(ipgnservers.ini,%x,duration)
      var %bookedat = $readini(ipgnservers.ini,%x,bookedat)
      var %booked = $readini(ipgnservers.ini,%x,booked)
      notice $nick $colours($+(%connectip,:,%port) ( $+ %x $+ ) Status: $iif(%booked,Booked by %booker at %bookedat,Available))
      inc %x
    }
  }
  if ($1 = !relayservers) {
    var %x = 1
    while (%x <= $calc($ini(ipgnrelayservers.ini,0) - 1)) {
      var %connectip = $readini(ipgnrelayservers.ini,%x,connectip)
      var %port = $readini(ipgnrelayservers.ini,%x,port)
      var %relay = $readini(ipgnrelayservers.ini,%x,relay)
      var %announce = $readini(ipgnrelayservers.ini,%x,announce)
      var %active = $readini(ipgnrelayservers.ini,%x,active)
      var %bookingtime = $readini(ipgnrelayservers.ini,%x,bookingtime)
      var %booker = $readini(ipgnrelayservers.ini,%x,booker)
      var %booked = $readini(ipgnrelayservers.ini,%x,booked)

      notice $nick $colours($+(%connectip,:,%port) ( $+ %x $+ ) Status:  $iif(!%booked && !%bookingtime,Available) $iif(%booked,Booked by %booker) $&
        $iif(%bookingtime && !%active,Activates in $duration($calc(%bookingtime - $ctime))) $iif(%relay,Relay target: %relay) $iif(%announce,Announce: %announce))

      inc %x
    }
  }
}

alias ipgnServerReset {
  var %currentrcon = $readini(ipgnservers.ini,$1,rcon)
  var %ip = $readini(ipgnservers.ini,$1,ip)
  var %port = $readini(ipgnservers.ini,$1,port)
  var %booker = $readini(ipgnservers.ini,$1,booker)
  var %team = $readini(ipgnservers.ini,$1,team)
  var %duration = $readini(ipgnservers.ini,$1,duration)
  var %bookedat = $readini(ipgnservers.ini,$1,bookedat)
  var %numbooked = $readini(ipgnservers.ini,global,numbooked)
  var %defaultrcon = $readini(ipgnservers.ini,$1,defaultrcon)

  writeini -n ipgnservers.ini $1 rcon %defaultrcon
  writeini -n ipgnservers.ini global numbooked $calc(%numbooked - 1)

  rconbooking %ip %port %currentrcon say The booking time for this server has expired or an admin has forced a reset; rcon_password %defaultrcon ; sv_password jimmybob
  ;do stuff here
  ipgnIniReset $1
  .timerresetipgn $+ $1 off

  msg %admin.chan $colours(Server $1 booked by %booker for %team at %bookedat for %duration hours has been reset.)
}

alias ipgnRelayReset {
  var %ip = $readini(ipgnrelayservers.ini,$1,ip)
  var %port = $readini(ipgnrelayservers.ini,$1,port)
  var %rconport = $readini(ipgnrelayservers.ini,$1,rconport)
  var %relay = $readini(ipgnrelayservers.ini,$1,relay)
  var %booker = $readini(ipgnrelayservers.ini,$1,booker)
  var %duration = $readini(ipgnrelayservers.ini,$1,duration)
  var %bookedat = $readini(ipgnrelayservers.ini,$1,bookedat)
  var %numbooked = $readini(ipgnrelayservers.ini,global,numbooked)
  var %rcon = $readini(ipgnrelayservers.ini,$1,rcon)
  var %numspecs = $readini(ipgnrelayservers.ini,$1,numspecs)

  writeini -n ipgnrelayservers.ini global numbooked $calc(%numbooked - 1)

  rconbooking %ip %rconport %rcon tv_relay 202.138.3.30:27016

  ipgnRelayIniReset $1

  .timerresetipgnrelay $+ $1 off
  .timerrelayadvert $+ $1 off
  .timerrelaystatus $+ $1 off

  if ($chan) && ($chan != %admin.chan) { msg $chan $colours(Relay server %ip $+ : $+ %port ( $+ $1 $+ ) connected to %relay by %booker at %bookedat has been reset. Highest recorded spectators: %numspecs) }
  msg %admin.chan $colours(Relay server %ip $+ : $+ %port ( $+ $1 $+ ) connected to %relay by %booker at %bookedat has been reset. Highest recorded spectators: %numspecs)
}

alias ipgnIniReset {
  writeini -n ipgnservers.ini $1 booked 0
  writeini -n ipgnservers.ini $1 booker 0
  writeini -n ipgnservers.ini $1 duration 0
  writeini -n ipgnservers.ini $1 team 0
  writeini -n ipgnservers.ini $1 password 0
  writeini -n ipgnservers.ini $1 bookedat 0
  writeini -n ipgnservers.ini $1 bookingtime 0
  writeini -n ipgnservers.ini $1 active 0
}

alias ipgnRelayIniReset {
  writeini -n ipgnrelayservers.ini $1 booked 0
  writeini -n ipgnrelayservers.ini $1 booker 0
  writeini -n ipgnrelayservers.ini $1 duration 0
  writeini -n ipgnrelayservers.ini $1 relay 0
  writeini -n ipgnrelayservers.ini $1 bookedat 0
  writeini -n ipgnrelayservers.ini $1 announce 0
  writeini -n ipgnrelayservers.ini $1 bookingtime 0
  writeini -n ipgnrelayservers.ini $1 active 0
  writeini -n ipgnrelayservers.ini $1 gametime 0
  writeini -n ipgnrelayservers.ini $1 map 0
  writeini -n ipgnrelayservers.ini $1 players 0
  writeini -n ipgnrelayservers.ini $1 numspecs 0
}

alias timeBookingCalculate {
  var %time = $1
  var %ctime_bookingtime = $ctime($time(mmmm d yyyy) %time)
  var %time_to_booking = $calc(%ctime_bookingtime - $ctime)
  var %time_to_booking_legible = $gettok($duration(%time_to_booking),1-2,32)
  return $+(%ctime_bookingtime,$chr(37),%time_to_booking_legible)
}

alias ipgnCheckPreBookings {
  var %y = $calc($ini(ipgnservers.ini,0) - 1)
  var %x = 1
  while (%x <= %y) {
    var %bookingtime = $readini(ipgnservers.ini,%x,bookingtime)
    if (%bookingtime) && (!$readini(ipgnservers.ini,%x,active)) {
      if ($ctime > %bookingtime) {
        var %rcon = $lower($read(passwords.txt))
        var %pass = $lower($read(passwords.txt))
        var %currentrcon = $readini(ipgnservers.ini,%x,rcon)
        var %ip = $readini(ipgnservers.ini,%x,ip)
        var %port = $readini(ipgnservers.ini,%x,port)
        var %booker = $readini(ipgnservers.ini,%x,booker)
        var %team = $readini(ipgnservers.ini,%x,team)
        var %duration = $readini(ipgnservers.ini,%x,duration)
        var %bookedat = $readini(ipgnservers.ini,%x,bookedat)

        writeini -n ipgnservers.ini %x password %pass
        writeini -n ipgnservers.ini %x rcon %rcon
        writeini -n ipgnservers.ini %x active 1

        .timerresetipgn $+ %x 1 $calc(%duration * 3600) ipgnServerReset %x

        msg #ipgn-tf2 $colours(Server %x booked by %booker for %team at %bookedat has been activated. This booking lasts %duration hours.)
        if (%booker ison #ipgn-tf2) {
          notice %booker $colours($connectString(%ip,%port,%pass,%rcon)) 
        }
        else {
          msg #ipgn-tf2 $colours(%booker is absent. Please give these details to %team $+ : $connectString(%ip,%port,%pass,%rcon))
          notice #ipgn-tf2 $colours(%booker is absent. Please give these details to %team $+ : $connectString(%ip,%port,%pass,%rcon))
        }

        rconbooking %ip %port %currentrcon say This server has been booked for %duration hours.; sv_password %pass ;servercfgfile common_bookable.cfg; exec common_bookable; wait 1000; mr_ipgnbooker %team ; rcon_password %rcon
      }
    }
    inc %x
  }
}
alias ipgnCheckRelayPreBookings {
  var %y = $calc($ini(ipgnrelayservers.ini,0) - 1)
  var %x = 1
  while (%x <= %y) {
    var %bookingtime = $readini(ipgnrelayservers.ini,%x,bookingtime)
    if (%bookingtime) && (!$readini(ipgnrelayservers.ini,%x,active)) {
      if ($ctime > %bookingtime) {
        var %currentrcon = $readini(ipgnrelayservers.ini,%x,rcon)
        var %connectip = $readini(ipgnrelayservers.ini,%x,connectip)
        var %ip = $readini(ipgnrelayservers.ini,%x,ip)
        var %port = $readini(ipgnrelayservers.ini,%x,port)
        var %rconport = $readini(ipgnrelayservers.ini,%x,rconport)
        var %booker = $readini(ipgnrelayservers.ini,%x,booker)
        var %relay = $readini(ipgnrelayservers.ini,%x,relay)
        var %duration = $readini(ipgnrelayservers.ini,%x,duration)
        var %bookedat = $readini(ipgnrelayservers.ini,%x,bookedat)

        writeini -n ipgnrelayservers.ini %x active 1

        .timerresetipgnrelay $+ %x 1 $calc(4 * 3600) ipgnRelayReset %x
        .timerrelayadvert $+ %x 0 960 rconbooking %ip %rconport %currentrcon tv_msg This relay is provided by the iPrimus Gaming Network; wait 300; tv_msg www.ipgn.com.au
        if (!$timer(announce)) {
          .timerannounce 0 $calc(8*60) relayAnnounce
        }
        .timerrelaystatus $+ %x 0 30 rconbooking %ip %rconport %currentrcon tv_status

        relayAnnounce %x

        msg #ipgn-tf2 $colours(Relay server $+(%connectip,:,%port) ( $+ %x $+ ) booked by %booker for %duration hours has been activated. Connecting to %relay)

        rconbooking %ip %rconport %currentrcon tv_relay %relay
      }
    }
    inc %x
  }
}

alias connectString {
  if ($1) && ($2) && (!$3) {
    return connect $1 $+ : $+ $2
  }
  elseif ($1) && ($2) && ($3) && (!$4) {
    return connect $1 $+ : $+ $2 $+ ; password $3
  }
  elseif ($1) && ($2) && ($3) && ($4) {
    return connect $1 $+ : $+ $2 $+ ; password $3 $+ ; rcon_password $4
  }
  else {
    return NA
  }
}

alias rconbooking {
  sourceRcon $1-
}

alias timecheck {
  noop $regex($1,(\d+)\:(\d+)(am|pm))
  echo -a $regml(1) $regml(2) $regml(3)
}
