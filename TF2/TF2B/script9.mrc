;New booking script (for users to ask for a server/stv relay)

on *:TEXT:?:{
  if ($1 = !requestserver) {
    if ($3 > 3) { msg $nick $colours(You can only book a server for a maximum of 3 hours) | halt }
    if ($2) && ($3) {
      if ($read(bookableservers.txt,w, * $+ Not currently booked $+ *)) {
        msg %admin.chan $colours($nick is requesting a server for $2 hours. Clan: $3-)
        msg %admin.chan $colours(To accept this booking, type !accepts $nick)
      }
      else {
        msg $nick $colours(Sorry, there are currently no servers available)
      }
    }
    else {
      msg $nick $colours(You need to set it out like this: !requestserver <length of booking> <team name>)
      msg $nick $colours(Eg. !requestserver 2 cool kids)
    }
  }
  if ($1 = !requestrelay) {
    if ($3 > 3) {  msg $nick $colours(You can only book a server for a maximum of 3 hours) | halt }
    if ($2) && ($3) {
      if ($read(bookablerelay.txt,w,* $+ Available $+ *)) {
        msg %admin.chan $colours($nick is requesting a relay server for $2 hours. The server being relayed is: $3-)
        msg %admin.chan $colours(To allow this relay connection, type !acceptr $nick)
      }
      else {
        msg $nick $colours(Sorry, there are currently no relay servers available)
      }
    }
    else {
      msg $nick $colours(You need to set it out like this: !requestrelay <length of booking> <server>)
      msg $nick $colours(Eg. !requestrelay 2 210.50.4.30:27016)
    }
  }
}

on *:TEXT:%admin.chan:{
  if ($1 = !accept) && ($2) {
    if (%relay. [ $+ [ $2 ] ]) {
      msg $2 $colours(Your request has been accepted, connecting relay to %relay. [ $+ [ $2 ] $+ .server] $+ . You will received the details shortly)
      msg %admin.chan $colours(Relay request for %relay. [ $+ [ $2 ] $+ .server ] accepted. Setting up relay...)
      setupRelay $2 %relay. [ $+ [ $2 ] $+ .time ] %relay. [ $+ [ $2 ] $+ .server ]
    }
    elseif (%server. [ $+ [ $2 ] ]) {
      msg $2 $colours(Your server request has been accepted. You will be issued the details shortly)
      msg %admin.chan $colours(Accepted server request for %server. [ $+ [ $2 ] .team ] by $2 $+ . Setting up server...)
      setupServer $2 %server. [ $+ [ $2 ] $+ .time ] %server. [ $+ [ $2 ] .team ]
    }
    else {
      msg %admin.chan $colours(No booking exists for user with nick $2)
    }
  }
  if ($1 = !decline) && ($2) && ($3) {
    if (%relay. [ $+ [ $2 ] ]) {
      msg %admin.chan $colours(Declined relay for %relay. [ $+ [ $2 ] $+ .server ] requested by $2)
      msg $2 $colours(Your relay request has been declined for the following reason: $3- $+ . Please see $nick for further assistance)
      unset %relay. [ $+ [ $2 ] $+ * ]
    }
    elseif (%server. [ $+ [ $2 ] ]) {
      msg %admin.chan $colours(Declined server request by $2)
      msg $2 $colours(Your server request has been declined for the following reason: $3- $+ . Please see $nick for further assistance)
      unset %server. [ $+ [ $2 ] $+ *]
    }
    else { 
      msg %admin.chan $colours(No booking exists for user with nick $2) 
    }
  }
}

alias setupRelay {
  if ($1) && ($2) && ($3) {
    var %name = $1
    var %time = $2
    var %server = $3
    var %relaycommand = tv_relay $3
    msg %admin.chan $colours(Setting up relay for $1 $+ , connecting to $3 for $2 hours.
    tokenize 37 $read(bookablerelay.txt,w,* $+ Available $+ *,1)
    .timer 1 $calc(%time * 3600) reset relay $readn
    write -l $+ $readn $+($1,$chr(37),$2,$chr(37),$3,$chr(37),$4,$chr(37),Booked,$chr(37),%time,$chr(37),%name,$chr(37),%server)
    msg %admin.chan $colours(Relay server $1 $+ : $+ $2 connecting to %server)
    rconbooking $1 $3 $4 %relaycommand
    msg %admin.chan $colours(Relay server $1 $+ : $+ $2 connected to %server)
    msg %name $colours(Your relay has been connected. The connected server is: $1 $+ : $+ $2)
  }
  else {
    msg %admin.chan $colours(Error encountered during relay setup: missing token)
  }
}

alias setupServer {
  if ($1) && ($2) && ($3) {
    var %bookername = $1
    var %bookingtime = $2
    var %bookingteam = $3-
    if ($read(bookableservers.txt,w,S* $+ $chr(37) $+ Not currently booked $+ *,1)) {
      msg %admin.chan $colours(Setting up server for %bookername)
      var %bookedserver = $readn
      .timerresetserver $+ %bookedserver 1 $calc($2 * 3600) rcon.reset %bookedserver $me
      %server1rconpassword = $lower($read(passwords.txt))
      %server1joinpassword = $lower($read(passwords.txt))
      ;s1%ip%port%Booked%rconpassword%booker%team%time
      tokenize 37 $v1
      msg #ipgn-tf2 $colours(Server %bookedserver has been booked by $nick for %bookingteam $+ . This booking lasts %bookingtime hours.)
      notice $nick $colours(connect $2 $+ : $+ $3 $+ ; password %server1joinpassword $+ ; rcon_password %server1rconpassword)
      rconbooking $2 $3 $5 say This server has been booked for %bookingteam $+ .; sv_password %server1joinpassword ; wait 1000; rcon_password %server1rconpassword ;changelevel cp_granary
      echo @bookings %bookedserver has been booked by $nick for %bookingteam $+ . This booking lasts %bookingtime hours.
      write -l $+ %bookedserver bookableservers.txt $+($1,$chr(37),$2,$chr(37),$3,$chr(37),Is Booked,$chr(37),%server1rconpassword,$chr(37),%bookername,$chr(37),%bookingteam,$chr(37),%bookingtime,$chr(37),%server1joinpassword)
    }
  }
  else {
    msg %admin.chan $colours(Error encounted during server setup: missing token)
  }
}


/* Relay Server file format:
Booked Format: IP%Connect Port%Rcon Port%Rcon Password%Status%booking time%booking user%server being relayed
$+($1,$chr(37),$2,$chr(37),$3,$chr(37),$4,$chr(37),Booked,$chr(37),%time,$chr(37),%name,$chr(37),%server)
Unbooked format: IP%Connect Port%Rcon Port%Rcon Password%Status
*/

alias reset {
  ;reset <servertype> <line number> 
  if ($1) && ($2) && ($3) {
    if ($2 = relay) {
      var %relayserver = $3
      tokenize 37 $read(bookablerelay.txt,$3)
      msg %admin.chan $colours(Resetting relay server %relayserver ( $+ $1 $+ : $+ $2 $+ ))
      rconbooking $1 $3 $4 tv_relay 210.50.4.30:27016
      write -l $+ %relayserver $+($1,$chr(37),$2,$chr(37),$3,$chr(37),$4,$chr(37),Available)
      msg %admin.chan $colours(Relay server $1 $+ : $+ $2 has been reset)
    }
    elseif ($2 = server) {
      rcon.reset $3 $me
    }
    else {
      msg %admin.chan $colours(Error encountered during reset: No known server type " $+ $2 $+ ")
      halt
    }
  }
}
