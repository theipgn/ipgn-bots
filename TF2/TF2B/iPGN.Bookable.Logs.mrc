on *:start:{
  bookingLog.stopall
}

alias bookingLog.setup {


}

alias bookingLog.start {
  sockclose bookinglog. [ $+ [ $1 ] ]
  window -e @bookinglog. [ $+ [ $1 ] ]
  var %currentrcon = $readini(ipgnservers.ini,$1,rcon)
  var %ip = $readini(ipgnservers.ini,$1,ip)
  var %port = $readini(ipgnservers.ini,$1,port)
  sockudp -kn bookinglog. [ $+ [ $1  ] ] $2 %ip %port
  rconbooking %ip %port %currentrcon logaddress_add $ip $+ : $+ $2
}

alias bookingLog.stop {
  rconbooking $2 $3 $4 logaddress_delall
  sockclose $1
}

alias bookingLog.stopall {
  return
}

alias logchan {
  echo #ipgn-test $1 $2-
  .timer 1 90 msg $1 $colours($2-)
}

alias clr {
  if ($1 = Blue) {
    return 12
  }
  elseif ($1 = Red) {
    return 04
  }
  elseif ($1 = Green) {
    return 09
  }
  elseif ($1 = yellow) {
    return 08
  }
  elseif ($1 = darkgreen) {
    return 03
  }
  elseif ($1 = orange) {
    return 07
  }
  elseif ($1 = purple) {
    return 06
  }
  elseif ($1 = cyan) {
    return 11
  }
  else {
    return 
  }
}

alias sid {
  return $remove($1,STEAM_,:)
}

alias replacenames {
  if ($1 = tf_projectile_rocket) {
    return rocket launcher
  }
  elseif ($1 = tf_projectile_pipe) {
    return pipe bomb
  }
  elseif ($1 = tf_projectile_pipe_remote) {
    return sticky bomb
  }
  elseif ($1 = tf_projectile_arrow) {
    return bow and arrow
  }
  elseif ($1 = OBJ_SENTRYGUN) {
    return L1 sentry gun
  }
  elseif ($1 = OBJ_TELEPORTER_EXIT) {
    return teleporter exit
  }
  elseif ($1 = OBJ_TELEPORTER_ENTRANCE) {
    return teleporter entrance
  }
  elseif ($1 = OBJ_DISPENSER) {
    return dispenser
  }
  elseif ($1 = obj_sentrygun2) {
    return L2 sentry gun
  }
  elseif ($1 = obj_sentrygun3) {
    return L3 sentry gun
  }
  elseif ($1 = Granary_cap_red_cp2) {
    ;return Control Point: 2 (Red). CP Name: Granary Red CP2 (Red House)
    return Granary Red CP2 ( $+ $clr(red) $+ Red House $+ $clr $+ )
  }
  elseif ($1 = Granary_cap_red_cp1) {
    ;return Control Point: 1 (Red). CP Name: Granary Red CP1 ( $+ $clr(red) $+ Red Base $+ $clr $+ )
    return Granary Red CP1 ( $+ $clr(red) $+ Red Base $+ $clr $+ )
  }
  elseif ($1 = Granary_cap_cp3) {
    ;return Control Point: 3 (CENTER). CP Name: Granary CP3 (Center)
    return Granary CP3 ( $+ $clr(purple) $+ Middle $+ $clr $+ )
  }
  elseif ($1 = Granary_cap_blue_cp2) {
    ;return Control Point: 2 (Blue). CP Name: Granary Blue CP2 ( $+ $clr(blue) $+ Blue House $+ $clr $+ )
    return Granary Blue CP2 ( $+ $clr(blue) $+ Blue House $+ $clr $+ )
  }
  elseif ($1 = Granary_cap_blue_cp1) {
    ;return Control Point: 1 (Blue). CP Name: Granary Blue CP1 ( $+ $clr(blue) Blue Base $+ $clr $+ )
    return Granary Blue CP1 ( $+ $clr(blue) $+ Blue Base $+ $clr $+ )
  }
  elseif ($1 = #Well_cap_center) {
    return Well CP3 ( $+ $clr(purple) $+ Middle $+ $clr $+ )
  }
  elseif ($1 = #Well_cap_blue_two) {
    return Well Blue CP2 ( $+ $clr(blue) $+ Blue Warehouse $+ $clr $+ )
  }
  elseif ($1 = #Well_cap_blue_rocket) {
    return Well Blue CP1 ( $+ $clr(blue) $+ Blue Base $+ $clr $+ )
  }
  elseif ($1 = #Well_cap_red_two) {
    return Well Red CP2 ( $+ $clr(red) $+ Red Warehouse $+ $clr $+ )
  }
  elseif ($1 = #Well_cap_red_rocket) {
    return Well Red CP1 ( $+ $clr(red) $+ Red Base $+ $clr $+ )
  }
  elseif ($1 = #Badlands_cap_cp3) {
    return Badlands CP3 ( $+ $clr(purple) $+ Middle $+ $clr $+ )
  }
  elseif ($1 = #Badlands_cap_blue_cp2) {
    return Badlands Blue CP2 ( $+ $clr(blue) $+ Blue Spire $+ $clr $+ )
  }
  elseif ($1 = #Badlands_cap_blue_cp1) {
    return Badlands Blue CP1 ( $+ $clr(blue) $+ Blue Warehouse $+ $clr $+ )
  }
  elseif ($1 = #Badlands_cap_red_cp2) {
    return Badlands Red CP2 ( $+ $clr(red) $+ Red Spire $+ $clr $+ )
  }
  elseif ($1 = #Badlands_cap_red_cp1) {
    return Badlands Red CP1 ( $+ $clr(red) $+ Red Warehouse $+ $clr $+ )
  }
  else {
    return $replace($1-,_,$chr(32))
  }
}

on *:udpread:bookinglog.*:{
  if ($sockerr > 0) {
    return
  }
  :nextread
  var %bookinglog.data
  sockread -f %bookinglog.data
  if ($sockbr == 0) {
    return
  }
  if (%bookinglog.data == $null) {
    goto nextread
  }
  else {
    if (*"*from*"*"* iswm %bookinglog.data) {
      %bookinglog.data = $null
    }
    :continue
    if (*Rcon:* iswm %bookinglog.data) {
      %bookinglog.data = $null
    }
    if (Bad Rcon: rcon* iswm %bookinglog.data) {
      %bookinglog.data = $null     
    }
    if ($regex(%bookinglog.data,"(.+)<(\d+)><(.+)><(Blue|Red)>" disconnected \x28reason "(.*)"\x29)) {
      ;"ch3x<23><STEAM_0:0:20730909><Blue>" disconnected (reason "ch3x timed out")
      left $regml(3)
    }
    if ($regex(%bookinglog.data,"(.+)<(\d+)><(.+)><>" connected. address "(.+)")) {
      ;nothing here atm
    }
    if ($regex(%bookinglog.data,"(.+)<(\d+)><(.+)><>" STEAM USERID validated)) {
      write connected_players.txt $regml(3)
      joined $regml(3) $regml(1)
      ;Try adding user to stats with ign
      add $regml(3) name $remove($regml(1),$chr(39),$chr(44),$chr(45),$chr(59),$chr(124),$chr(47),$chr(92))
      addweapon $regml(3)

    }
    if ($regex(%bookinglog.data,"(.+)<(\d+)><(.+)><(Red|Blue)>" (say|say_team) "(.+)")) {
      ;do something
    }
    if ($regex(%bookinglog.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" changed role to "(.*)")) {
      ;playerclass $regml(3) $regml(5)
    }
    if (% [ $+ [ $sockname ] $+ .on ]) {
      if ($regex(%bookinglog.data,World triggered "Round_Win".+winner.+"(Blue|Red)")) {
        logchan % [ $+ [ $sockname ] ] Round win: $clr($regml(1)) $+ $regml(1) $+ $clr won the round!
        killstreak endround
      }
      if ($regex(%bookinglog.data,World triggered "Round_Overtime")) {
        logchan % [ $+ [ $sockname ] ] Round overtime!
      }
      if ($regex(%bookinglog.data,World triggered "Round_Length".+seconds.+"(\d+.\d+))) {
        var %rl_min = $round($calc($regml(1) / 60),2)
        logchan % [ $+ [ $sockname ] ] Round length: $clr(orange) $+ $gettok(%rl_min,1,46) $+ $clr minutes and $clr(orange) $+ $round($calc(($gettok(%rl_min,2,46) / 100) * 60),0) $+ $clr seconds
      }
      if ($regex(%bookinglog.data,World triggered "Round_Start")) {
        logchan % [ $+ [ $sockname ] ] Round start
      }
      if ($regex(%bookinglog.data,World triggered "Round_Setup_Begin")) {
        logchan % [ $+ [ $sockname ] ] Round setup begin
      }
      if ($regex(%bookinglog.data,World triggered "Mini_Round_Win" \x28winner "(Blue|Red)"\x29 \x28round "round_(\d+)"\x29)) {
        logchan % [ $+ [ $sockname ] ] Mini round $chr(35) $+ $regml(2) win: $clr($regml(1)) $+ $regml(1) $+ $clr
      }
      if ($regex(%bookinglog.data,World triggered "Mini_Round_Length" \x28seconds "(\d+.\d+)"\x29)) {
        logchan % [ $+ [ $sockname ] ] Mini round length: $regml(1)
      }
      if ($regex(%bookinglog.data,World triggered "Round_Setup_End")) {
        logchan % [ $+ [ $sockname ] ] Round setup end
      }
      ;Format: "%s<%i><%s><%s>" triggered "medic_death" against "%s<%i><%s><%s>" (healing "%d") (ubercharge "%s")
      ;healing is the amount the Medic healed in that life
      ;ubercharge (1/0) is whether they died with a full charge
      if ($regex(%bookinglog.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "medic_death" against "(.*)<(\d+)><(.*)><(Red|Blue)>" \x28healing "(.*)"\x29 \x28ubercharge "(.*)"\x29)) {
        ;echo @medic %bookinglog.data
        add $regml(7) healing $regml(9)
        add $regml(7) points $round($calc($regml(9) / 500),2)
        medcalc $regml(8) $regml(9) $regml(10)
        if ($regml(10) = 1) {
          logchan % [ $+ [ $sockname ] ] $clr($regml(8)) $+ $regml(5) $+ $clr(orange) $kdratio($regml(7)) $+ $clr healed a total of $clr($regml(8)) $+ $regml(9) $+ $clr and died with uber! $read(medphrase.txt)
          add $regml(7) uberlost 1
        }
        if ($regml(10) = 0) {
          logchan % [ $+ [ $sockname ] ] $clr($regml(8)) $+ $regml(5) $+ $clr(orange) $kdratio($regml(7)) $+ $clr died and did NOT lose uber after healing a total of $clr($regml(8)) $+ $regml(9)
        }
      }
      ;Format: "%s<%i><%s><%s>" committed suicide with "world" (customkill "%s") (attacker_position "%d %d %d")
      ;07/03/2010 - 14:04:16: "Hypnos<20><STEAM_0:0:24915059><Red>" committed suicide with "world" (customkill "train") (attacker_position "568 397 -511")
      if ($regex(%bookinglog.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" committed suicide with "(.*)" \x28customkill "(.*?)"\x29)) {
        add $regml(3) deaths 1
      }
      if ($regex(%bookinglog.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" killed "(.*)<(\d+)><(.*)><(Red|Blue)>" with "(.*)" \x28customkill "(\w+)"\x29)) {
        killstreak $regml(3) $regml(7)
        if ($regml(10) = backstab) {
          add $regml(7) death 1
          add $regml(3) kill 1
          addweapon $regml(3) $regml(9)
          add $regml(3) points 2
        }
        if ($regml(10) = headshot) {
          add $regml(7) death 1
          add $regml(3) kill 1
          addweapon $regml(3) $regml(9)
          add $regml(3) points 2
        }
        if ($regml(10) = feign_death) {
          ;add $regml(3) kill 1
          ;add $regml(3) points 1
        }
      }
      if ($regex(%bookinglog.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" killed "(.*)<(\d+)><(.*)><(Red|Blue)>" with "(.*)" \x28att.+)) {
        ;inc %killstreak. [ $+ [ $regml(3) ] ]
        ;if (%killstreak. [ $+ [ $regml(7) ] ]) { unset %killstreak. [ $+ [ $regml(7) ] ] }
        killstreak $regml(3) $regml(7)
        add $regml(3) kill 1
        add $regml(3) points 1
        add $regml(7) death 1
        addweapon $regml(3) $regml(9)
      }
      if ($regex(%bookinglog.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "kill assist" against "(.*)<(\d+)><(.*)><(Red|Blue)>")) {
        ;killchan $clr($regml(4)) $+ $regml(1) $+ $clr(orange) $kdratio($regml(3)) $+ $clr assisted $clr($regml(4)) $+ %killer $+ $clr(orange) $kdratio(%killerid) $+ $clr in killing $clr($regml(8)) $+ $regml(5) $+ $clr(orange) $kdratio($regml(7)) $+ $clr
        add $regml(3) assist 1
        add $regml(3) points 0.5
      }
      if ($regex(%bookinglog.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "flagevent" .event "(.*)". .posi)) {
        if ($regml(5) = captured) {
          if ($regml(4) = blue) {
            logchan % [ $+ [ $sockname ] ] $clr($regml(4)) $+ $regml(1) $+ $clr captured the $clr(red) $+ Red $+ $clr intelligence.
          }
          elseif ($regml(4) = red) {
            logchan % [ $+ [ $sockname ] ] $clr($regml(4)) $+ $regml(1) $+ $clr captured the $clr(blue) $+ Blue $+ $clr intelligence.
          }
          add $regml(3) cap 1
          add $regml(3) points 2
        }
        if ($regml(5) = defended) {
          logchan % [ $+ [ $sockname ] ] $clr($regml(4)) $+ $regml(1) $+ $clr defended the $clr($regml(4)) $+ $regml(4) $+ $clr intelligence.
          add $regml(3) points 1
        }
      }
      if ($regex(%bookinglog.data,/Team "(Blue|Red)" triggered "pointcaptured" \x28cp "(\d+)"\x29 \x28cpname "(.+)"\x29 \x28numcappers "(\d+)".+/)) {
        logchan % [ $+ [ $sockname ] ] $clr($regml(1)) $+ $regml(1) $+ $clr triggered point captured. Control point: $regml(2) $+ . CP Name: $replacenames($regml(3)) $+ .
        var %numcappers = $regml(4)
        logchan % [ $+ [ $sockname ] ] Number of cappers: %numcappers
        var %str1 = \x28player(\d) "(.+)<(\d+)><(\S+)><(Blue|Red)>"\x29.+
        var %x = 0
        var %str = $str(%str1,%numcappers)
        if ($regex(%bookinglog.data,%str)) {
          while (%x < %numcappers) {
            logchan % [ $+ [ $sockname ] ] Capper $chr(35) $+ $regml($calc(1 + (5 * %x))) $+ : $clr($regml($calc(5 + (5 * %x)))) $+ $regml($calc(2 + (%x * 5))) $+ $clr $kdratio($regml($calc(4 + (%x * 5))))
            add $regml($calc(4 + (5 * %x))) cap 1
            add $regml($calc(4 + (5 * %x))) points 2
            inc %x   
          }
        }
      }
      if ($regex(%bookinglog.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "revenge" against "(.*)<(\d+)><(.*)><(Red|Blue)>")) {
        add $regml(3) points 1
      }
      ;"dcup<109><STEAM_0:0:15236776><Red>" triggered "killedobject" (object "OBJ_SENTRYGUN") (weapon "tf_projectile_pipe") (objectowner "NsS. oLiVz<101><STEAM_0:1:15674014><Blue>") (attacker_position "551 2559 216")
      if ($regex(%bookinglog.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "killedobject" .object "(.*)". .weapon "(.*)". .objectowner "(.*)<(\d+)><(.*)><(Blue|Red)>)") {
        add $regml(3) points 1
      }
      if ($regex(%bookinglog.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "chargedeployed")) {
        inc %uber. [ $+ [ $regml(4) ] ]
        add $regml(3) points 1
      }
      ;"pvtx<103><STEAM_0:1:7540588><Red>" triggered "captureblocked" (cp "1") (cpname "Control Point B") (position "-2143 2284 156")
      if ($regex(%bookinglog.data,"(.*)<(\d+)><(.*)><(Red|Blue)>" triggered "captureblocked".+cp "(\d+)".+.+cpname "#?(\w*)".+)) {
        ;killchan $clr($regml(4)) $+ $regml(1) $+ $clr blocked CP $+ $regml(5) $replacenames($regml(6))
        add $regml(3) points 1
      }
      if ($regex(%bookinglog.data,Team "(Blue|Red)" current score "(\d+)" with "(\d+)" players)) {
        logchan % [ $+ [ $sockname ] ] $clr($regml(1)) $+ $regml(1) $+ $clr current score: $clr($regml(1)) $+ $regml(2) $+ $clr with $clr($regml(1)) $+ $regml(3) $+ $clr players
        set %score. [ $+ [ $regml(1) ] ] $regml(2)
      }
      if ($regex(%bookinglog.data,Team "(Blue|Red)" final score "(\d+)" with "(\d+)" players)) {
        logchan % [ $+ [ $sockname ] ] $clr($regml(1)) $+ $regml(1) $+ $clr final score: $clr($regml(1)) $+ $regml(2) $+ $clr with $clr($regml(1)) $+ $regml(3) $+ $clr players
      }
      if ($regex(%bookinglog.data,World triggered "Game_Over" reason "(.*)")) {
        logchan % [ $+ [ $sockname ] ] Game over: $clr(green) $+ $regml(1) $+ $clr
      }
    }
    :serversay
    if (%bookinglog.data != $null) {
      echo @ [ $+ [ $sockname ] ] %bookinglog.data
      .timerlogs off
      .timerlogs 1 1200 rcon.logs
      unset %bookinglog.data
    }
    goto nextread
  }
}

alias medcalc {
  ;medcalc colour healing uber
  if (!%medic. [ $+ [ $1 $+ .healing ] ]) && (%medic. [ $+ [ $1 $+ .healing ] ] != 0) {
    %medic. [ $+ [ $1 $+ .healing ] ] = $2
  }
  else {
    %medic. [ $+ [ $1 $+ .healing ] ] = $calc(%medic. [ $+ [ $1 $+ .healing ] ] + $2)
  }
  if (!%medic. [ $+ [ $1 $+ .uberlost ] ]) && (%medic. [ $+ [ $1 $+ .uberlost ] ] != 0) {
    %medic. [ $+ [ $1 $+ .uberlost ] ] = $3
  }
  else {
    %medic. [ $+ [ $1 $+ .uberlost ] ] = $calc(%medic. [ $+ [ $1 $+ .uberlost ] ] + $3)
  }
}

alias kdratio {
  return
  ;return $+($clr(orange),$chr(91),$gettok($read(stats.txt,w,$1 $+ *),3,37),$chr(47),$gettok($read(stats.txt,w,$1 $+ *),4,37),$chr(93),$clr)
}

alias getnamebyid {
  if ($read(stats.txt,w,$1 $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    return $2
  }
  elseif ($read(%statsdir $+ statsall.txt,w,$1 $+ $chr(37) $+ *)) {
    tokenize 37 $v1
    return $2
  }
  else {
    return $sid($1)
  }
}

alias left {
  if (%team1) { return }
  if ($read(joined.txt,w,$1 $+ $chr(61) $+ *)) && (!%match.on) { 
    write -dw $+ $1 $+ =* joined.txt 
    %playerids = $remtok(%playerids,$1,44)
  }
}

alias killstreak {
  return
}
