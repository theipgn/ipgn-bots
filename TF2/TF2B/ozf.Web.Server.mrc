on *:START:{
  set %ipgn.Web.Version ipgnBookableServer/1.0.2
  set %ozf.Web.Server.Password fuckingleakers
  window -e @webserver
}

alias webServerRandomPassword {
  var %pass = $lower($read(passwords.txt))
  set %ozf.Web.Server.Password %pass

  webServerLog New password generated. Today's random password: %pass
}

alias -l webServerLog {
  var %ts = $time([(ddd)hh:nn:ss])
  echo @webserver %ts $1-
  write %statsdir $+ $+(weblogs\,$date(dd-mm-yyyy),.log) %ts $1-
}

on *:CONNECT:{
  if (!$sock(ozf.Servers.Listen)) {
    socklisten -d 202.138.3.40 ozf.Servers.Listen 6003
  }

  .timerrandomwebpass 03:00 0 86400 webServerRandomPassword
}

on *:socklisten:ozf.Servers.Listen:{
  webServerLog Connection on $sock($sockname).addr $sock($sockname).port

  set -u5 %web.server.sockincr 1
  var %tick = $calc($ticks + %web.server.sockincr)
  var %sock = ozf.Servers.Data. [ $+ [ %tick ] ]
  inc %web.server.sockincr

  if (!$sock(%sock)) {
    sockaccept %sock
    var %ip = $sock(%sock).ip

    webServerLog Accepted connection at %tick on %sock from %ip $+ : $+ $sock(%sock).port

    if ($read(web_bannedips.txt,w,%ip)) {
      sockclose %sock
      webServerLog %ip is banned. closed connection
    }
  }
  else {
    webServerLog %sock is already in use. Not accepting connection
  }

  ;listen for http headers
}

on *:sockopen:ozf.Servers.Data.*:{
  webServerLog sockopen event fired
}

on *:sockread:ozf.Servers.Data.*:{
  var %data.read
  :nextread
  sockread %data.read

  if ($sockbr == 0) { return }

  if (%data.read) { webServerLog CLIENT: %data.read }
  var %valid = 0, %invalidlogin = 0
  if (favicon isin %data.read) { goto nextread }

  if (GET isin %data.read) && (favicon !isin %data.read) {
    webServerLog GET data: %data.read
    ;reqloc $gettok(%data.read,2,32)
    noop $regex(%data.read,\/\?(.*?)HTTP\S+)

    webServerLog Params; $regml(1)

    var %x = 1, %y = $numtok($regml(1),38), %args = $regml(1)
    while (%x <= %y) {
      ;p=asdf&page=onetwothree&dicks=vaginas

      var %param = $gettok(%args,%x,38)
      var %paramkey = $gettok(%param,1,61)
      var %paramvalue = $gettok(%param,2,61)

      webServerLog Loop %x Param: %param Key: %paramkey Value: %paramvalue

      if ($lower(%paramkey) == p) {
        if (%paramvalue == %ozf.Web.Server.Password) {
          if (%y == 1) {
            ;valid login and login is the only param specified. serve servers page as default
            set % [ $+ [ $sockname ] $+ .page ] servers
            %valid = 1
          }
        }
        else {
          %invalidlogin = 1
          set % [ $+ [ $sockname ] $+ .forbidden ] 1

          webServerLog Unauthorised access attempt from $sock($sockname).ip using password %paramvalue
          msg #ozf-help Unauthorised web access attempt from $sock($sockname).ip using password %paramvalue
        }
      }
      elseif ($lower(%paramkey) == m) {
        ;here we'll have the module (or page) handling
        if ($lower(%paramvalue) == servers) {
          set % [ $+ [ $sockname ] $+ .page ] servers
          %valid = 1
        }
        elseif ($lower(%paramvalue) == commands) {
          set % [ $+ [ $sockname ] $+ .page ] comlist
          %valid = 1
        }
        else {
          ;invalid module
          %valid = 0
        }
      }
      else {
        webServerLog Invalid param %param specified. Ignoring
      }

      inc %x
    }
  }
  var %w = sockwrite -n $sockname

  if (%data.read == $null) {
    webServerLog End of request headers. Send generic server response
    if (%invalidlogin) {
      %w HTTP/1.1 403 Forbidden
      set % [ $+ [ $sockname ] $+ .forbidden ] 1
    }
    elseif ((%valid) || (% [ $+ [ $sockname ] $+ .page ])) {
      %w HTTP/1.1 200 OK
      set % [ $+ [ $sockname ] $+ .valid ] 1
    }
    elseif (!%valid) && (!%invalidlogin) {
      %w HTTP/1.1 403 Forbidden
      set % [ $+ [ $sockname ] $+ .forbidden ] 1
    }
    else {
      %w HTTP/1.1 404 Not Found
      set % [ $+ [ $sockname ] $+ .valid ] 0
    }
    %w Content-Type: text/html
    %w Server: %ipgn.Web.Version
    %w Connection: close
    %w

    webServerSendData $sockname % [ $+ [ $sockname ] $+ .page ]
    return
  }

  goto nextread
}

on *:sockwrite:ozf.Servers.Data.*:{
  if ($sockerr) {
    webServerLog Socket error in $sockname $+ ! Error: $sock($sockname).wsmsg
    sockclose $sockname
    return
  }
}

alias webServerSendData {
  ;webssd sockname page
  var %sock = $1, %page = $2

  webServerLog sendData sock: %sock page: %page
  webServerLog Valid: % [ $+ [ %sock ] $+ .valid ] Forbidden: % [ $+ [ %sock ] $+ .forbidden ]

  var %w = sockwrite -nt %sock

  if (% [ $+ [ %sock ] $+ .forbidden ]) {
    %w <html><head><title>403 Forbidden</title></head><body>
    %w <br><br>Unauthorized<br><br><br><br><br> %ipgn.Web.Version
    %w </body></html>
    unset % [ $+ [ %sock ] $+ .forbidden ]
  }
  elseif ((!% [ $+ [ %sock ] $+ .valid ]) || (!%page)) {
    %w <html><head><title>404 Not Found</title></head><body>
    %w <br><br>Invalid location specified. <br><br><br><br><br> %ipgn.Web.Version
    %w </body></html>
  }
  else {
    ;std hmtl constructors
    %w <html>
    %w <head>

    if (%page == servers) {
      %w <title>ozfortress server status</title>
      %w </head>
      %w <body>
      var %y = $calc($ini(ozfservers.ini,0) - 1)
      %w Number of servers booked: $readini(ozfservers.ini,global,numbooked) $+ / $+ %y <br><br><br>
      var %x = 1
      while (%x <= %y) {
        var %currentrcon = $readini(ozfservers.ini,%x,rcon)
        var %defaultrcon = $readini(ozfservers.ini,%x,defaultrcon)
        var %ip = $readini(ozfservers.ini,%x,ip)
        var %port = $readini(ozfservers.ini,%x,port)
        var %booker = $readini(ozfservers.ini,%x,booker)
        var %booked = $readini(ozfservers.ini,%x,booked)
        var %team = $readini(ozfservers.ini,%x,team)
        var %duration = $readini(ozfservers.ini,%x,duration)
        var %bookedat = $readini(ozfservers.ini,%x,bookedat)
        var %type = $readini(ozfservers.ini,%x,type)
        var %last_players = $readini(ozfservers.ini,%x,last_players)
        var %tv_port = $readini(ozfservers.ini,%x,tv_port)
        var %tv_delay = $readini(ozfservers.ini,%x,tv_delay)

        %w <b>Server:</b> %x ( $+ %type $+ ) &nbsp;&nbsp;&nbsp;<b>Status:</b> $iif(%booked,Booked,Available) &nbsp;&nbsp;&nbsp;<b>IP:Port</b> %ip $+ : $+ %port &nbsp;&nbsp;&nbsp;<b>Default RCON:</b> %defaultrcon <br> 
        if (%booked) {
          %w &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Booker:</b> %booker ( $+ $gettok($hget(ozfhash,%booker),2,59) $+ ) &nbsp;&nbsp;&nbsp;<b>Booked at:</b> %bookedat &nbsp;&nbsp;&nbsp;<b>Booked for:</b> %team <br> 
          %w &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Current RCON:</b> %currentrcon &nbsp;&nbsp;&nbsp; <br><br>
          %w &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Players (10 mins past):</b> %last_players &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>STV Port:</b> %tv_port ( $+ %tv_delay $+ s delay) <br>
        }
        %w ----------------------------------------------------------------------------------------------------------
        %w <br>

        ;webServerLog sent data for server %x ######### current socket queue size: $sock(%sock).sq

        inc %x
      }
      webServerLog Server status sent, num servers shown: %x
    }
    elseif (%page == comlist) {
      %w <title>#ozf-help command list</title>
      %w </head>
      %w <body>
      %w <b>Commands are as follows:</b><br><br>

      var %x = 1, %y = $ini(ozfcommands.ini,0)
      while (%x <= %y) {
        if ($ini(ozfcommands.ini,%x)) {
          %w <b><i> $+ $readini(ozfcommands.ini,%x,command) $+ </b></i> - $readini(ozfcommands.ini,%x,desc) <br>
        }

        inc %x
      }

      webServerLog Command list sent
    }
    else {
      %w <title>404 Not Found</title></head><body>
      %w <br><br>Invalid location specified.  asdf<br><br><br><br><br> %ipgn.Web.Version
    }

    ;end std html constructors
    %w </body>
    %w </html>
    %w
  }

  unset % [ $+ [ $sockname ] $+ .valid ]
  unset % [ $+ [ $sockname ] $+ .page ]

  if (!$sock(%sock).sq) {
    sockclose %sock
  }
  else {
    .timerwebsock $+ %sock -m 0 200 webServerSocketClose %sock
  }
}

on *:sockclose:ozf.Servers.Data.*:{
  if ($sock($sockname).wserr) {
    webServerLog Socket $sockname had error $v1 ( $+ $sock($sockname).wsmsg $+ )
  }
  else {
    webServerLog Sock $sockname closed by client
  }
}

alias webServerSocketClose {
  ;close %sock
  var %sock = $1
  if ($sock(%sock).sq) { return }

  sockclose %sock
  .timerwebsock $+ %sock off
}
