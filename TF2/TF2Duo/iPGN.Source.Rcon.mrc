/*
A complete mIRC implementation of the Source RCON Protocol

Not for distribution outside of iPGN without the permission of the author

Copyright 2011 Prithu "bladez" Parker
*/

on *:START:{
  window -e @sourcercon
}

on *:INPUT:@sourcercon:{
  if ($left($1,1) != $chr(47)) { 
    rcon $1-
  }
}

alias sourceRcon {
  if (!$1) || (!$2) || (!$3) || (!$4) { 
    echo @sourcercon $time([(ddd)HH:nn:ss]) RCON: Invalid parameters specified. sourceRcon ip port rcon command
    echo @sourcercon $time([(ddd)HH:nn:ss]) RCON: Specified params: $1-
    return
  }

  echo @sourcercon $time([(ddd)HH:nn:ss]) sourceRcon params: $1-

  var %sock = rcon. [ $+ [ $1 ] $+ _ $+ [ $2 ] ]

  if ($timer(%sock $+ error)) {
    echo @sourcercon $time([(ddd)HH:nn:ss]) 4Socket had a connection error previously. Writing arguments to queue
    write %sock $+ .queue $1-
    return
  }

  if (% [ $+ [ %sock ] $+ .waiting ]) {
    if (!% [ $+ [ %sock ] $+ .waittimer ]) {
      set -u2 % [ $+ [ %sock ] $+ .waittimer ] 1
    }

    echo @sourcercon $time([(ddd)HH:nn:ss]) 7Waiting for current rcon commands to finish...
    .timer -m 1 $calc(% [ $+ [ %sock ] $+ .waittimer ] * 100) sourceRcon $1-

    inc % [ $+ [ %sock ] $+ .waittimer ]
    echo @sourcercon $time([(ddd)HH:nn:ss]) 7Wait timer is now $calc(% [ $+ [ %sock ] $+ .waittimer ] * 100) milliseconds
    return
  }

  if (!$sock(%sock)) {
    rconSockCleanup %sock

    set % [ $+ [ %sock ] $+ .ip ] $1
    set % [ $+ [ %sock ] $+ .port ] $2
    set % [ $+ [ %sock ] $+ .rcon ] $3

    set % [ $+ [ %sock ] $+ .command ] $4-

    sockopen %sock $1 $2

    sockmark %sock $1 $+ : $+ $2

    if (!$timer(%sock $+ queuecheck)) {
      .timer $+ %sock $+ queuecheck 1 3 rconQueueCheck %sock
    }

  }
  elseif ($sock(%sock)) && (!% [ $+ [ %sock ] $+ .authenticated ]) && (% [ $+ [ %sock ] $+ .junked ]) {
    echo @sourcercon $time([(ddd)HH:nn:ss]) %sock was unable to authenticate... Will retry
    rconSockCleanup %sock

    set % [ $+ [ %sock ] $+ .ip ] $1
    set % [ $+ [ %sock ] $+ .port ] $2
    set % [ $+ [ %sock ] $+ .rcon ] $3

    set % [ $+ [ %sock ] $+ .command ] $4-

    sockopen %sock $1 $2

    sockmark %sock $1 $+ : $+ $2

    if (!$timer(%sock $+ queuecheck)) {
      .timer $+ %sock $+ queuecheck 1 3 rconQueueCheck %sock
    }

  }
  else {
    set % [ $+ [ %sock ] $+ .command ] $4-
    sendRconCommand exec %sock $4-
  }

  set % [ $+ [ %sock ] $+ .waiting ] 1
}

on *:sockopen:rcon.*:{
  if ($sockerr > 0) {
    echo @sourcercon $time([(ddd)HH:nn:ss]) $sockname unable to connect with server $sock($sockname).mark $+ . Error: $sock($sockname).wserr ( $+ $sock($sockname).wsmsg $+ )
    rconSockCleanup $sockname
    return
  }
  sendRconCommand auth $sockname
}

/*
DATA: 10 0 0 1 0 0 0 0 0 0 0 0 0 ------ junk packet on authentication, this is a normal sight
DATA: 10 0 0 1 0 0 0 2 0 0 0 0 0 ------ auth response. successful if the 5th byte (id) is the same as the one we connected with and 9th is 2, -1 if unsuccessful
;first byte(s) in first 4-set is always packet length - max of 4096 chars
*/

on *:sockread:rcon.*:{
  if ($sockerr > 0) { 
    echo @sourcercon $time([(ddd)HH:nn:ss]) SOCKET ERROR $sockerr
    return 
  }
  :nextread
  sockread &data

  if ($sockbr == 0) { return }

  echo @sourcercon $time([(ddd)HH:nn:ss]) DATA READ: $bvar(&data,1-312) Actual Length: $bvar(&data,1-4)

  if ($bvar(&data,0) == 1) { goto nextread }

  if (($bvar(&data,5) == -1) || (($bvar(&data,5) == 255) && ($bvar(&data,6) == 255))) {
    echo @sourcercon $time([(ddd)HH:nn:ss]) Invalid RCON password specified.

    ;msg %pug.adminchan $codes(An error occurred when sending an RCON command to $sock($sockname).mark (Invalid RCON password))

    if ($sock($sockname)) { rconSockCleanup $sockname }
    return
  }
  ;2 is auth response
  if ($bvar(&data,9) == 2) {
    if (!% [ $+ [ $sockname ] $+ .authenticated ]) {
      set % [ $+ [ $sockname ] $+ .authenticated ] 1
      echo @sourcercon $time([(ddd)HH:nn:ss]) received auth response. sending first command

      sendRconCommand exec $sockname % [ $+ [ $sockname ] $+ .command ]
    }
    else {
      ;occurs when a command is sent without being properly authenticated
      echo @sourcercon $time([(ddd)HH:nn:ss]) received a response code of 2 after we've already authenticated...
    }
  }

  ;0 is command response
  if ($bvar(&data,9) == 0) {
    if (!% [ $+ [ $sockname ] $+ .junked ]) {
      echo @sourcercon $time([(ddd)HH:nn:ss]) received junk packet
      set % [ $+ [ $sockname ] $+ .junked ] 1

      ;when THIS happens (2 packets at once): DATA READ: 10 0 0 0 1 0 0 0 0 0 0 0 0 0 10 0 0 0 1 0 0 0 2 0 0 0 0 0 Actual Length: 10 0 0 0

      if ($bvar(&data,19)) {
        echo @sourcercon $time([(ddd)HH:nn:ss]) mirc read double packets
        if ($v1 == -1) || ($v1 == 255) {
          echo @sourcercon $time([(ddd)HH:nn:ss]) Invalid RCON password specified.

          ;msg %pug.adminchan $codes(An error occurred when sending an RCON command to $sock($sockname).mark (Invalid RCON password))

          if ($sock($sockname)) { sockclose $sockname }
          return
        }
        if ($bvar(&data,23) == 2) {
          set % [ $+ [ $sockname ] $+ .authenticated ] 1
          echo @sourcercon $time([(ddd)HH:nn:ss]) received auth response. sending first command

          sendRconCommand exec $sockname % [ $+ [ $sockname ] $+ .command ]
        }
      }
    }
    else {
      echo @sourcercon $time([(ddd)HH:nn:ss]) received command response
      ;we know data begins at the 13th byte and ends 2 bytes from the end. Therefore
      var %rconresponse = $bvar(&data,13,$calc($bvar(&data,0) - 2)).text

      ;now we loop if there's multiple lines (ascii 10 is line feed) 
      var %y = $numtok(%rconresponse,10)
      if (%y > 1) {
        var %x = 1
        while (%x <= %y) {
          var %responseline = $gettok(%rconresponse,%x,10)

          echo @sourcercon $time([(ddd)HH:nn:ss]) Line %x $+ : %responseline
          if (%x > 8) {
            ;var %responselinedebug = $replace(%responseline,$chr(32),*)
            ;echo @sourcercon debugline: %responselinedebug
            ;# 3 "shooter [i love radiant]" STEAM_0:0:23881410 08:31 101 0 active 124.169.217.92:27005
            if ($regex(%responseline,/#\s+(\d+)\s+"(.*)"\s+(.*?)\s+\S+\s+\S+\s+\S+\s+active\s+(.*):.*/i)) {
              ;echo @sourcercon STATUS RESPONSE: uid $regml(1) name: $regml(2) sid: $regml(3) ip: $regml(4)
              joined $regml(3) $regml(2)
            }
          }
          inc %x
        }
      }
      else {
        bcopy &response 1 &data 13 $calc($bvar(&data,0) - 2)
        breplace &response 10 0
        echo @sourcercon $time([(ddd)HH:nn:ss]) Response: $bvar(&response,1-).text
      }
    }
  }

  goto nextread
}

alias sendRconCommand {
  ;sendRconCommand <auth/exec> <socket> <command if exec>

  var %sock = $2

  if ($sock(%sock).sq > 0) {
    echo 11 @sourcercon $time([(ddd)HH:nn:ss]) %sock is currently sending. sendqueue: $sock(%sock).sq $+ . Waiting to send next command, params: $1-
    .timer -m 1 100 sendRconCommand $1-
    return
  }

  set % [ $+ [ %sock ] $+ .sending ] 1

  if ($1 != auth) && ($1 != exec) {
    echo @sourcercon $time([(ddd)HH:nn:ss]) 4ERROR: INVALID RCON PARAMETER SPECIFIED
    return
  }
  if ($1 == exec) && (!$3) {
    echo @sourcercon $time([(ddd)HH:nn:ss]) 4ERROR: YOU CAN'T SEND AN EXEC PARAMETER WITHOUT A COMMAND
    return
  }

  var %SERVERDATA_EXECCOMMAND = 2, %SERVERDATA_AUTH = 3, %rcon = % [ $+ [ $sockname ] $+ .rcon ], %reqid

  ;define our local bvars
  bset &conType 1 0
  bset &rconPassword 1 0
  bset &command 1 0
  bset &packetlength 1 0

  ;don't convert request ID or SERVERDATA_* to literal ASCII characters; they are integers and should be treated as such! same goes for packetlength later


  ;don't use sockwrite -b, because it sends a UDP encapsulated packet via TCP stream (STUN) rather than a proper TCP packet, and fucks shit up
  ;sockwrite -b $sockname $calc(%packetlength + 4) &rcon

  if ($1 == auth) {
    ;auth packet stuff

    ;may need to dynamically choose, or increase request id at some point
    set % [ $+ [ $sockname ] $+ .reqid ] 1
    %reqid = 1

    echo @sourcercon $time([(ddd)HH:nn:ss]) sending rcon password %rcon to $sock($sockname).mark

    bset &conType 1 %SERVERDATA_AUTH

    var %x = 1, %y = $len(%rcon)
    while (%x <= %y) {
      bset &rconPassword %x $asc($mid(%rcon,%x,1))
      inc %x
    }
    echo @sourcercon $time([(ddd)HH:nn:ss]) rconpassword check: $bvar(&rconPassword,1,$bvar(&rconPassword,0)).text

    ;packet length: 4 bytes for id, 4 bytes for request type, n bytes for rcon pass, 1 byte for null between strings, and then 1 null byte for the end, 0 is the length of our null string
    var %packetlength = $calc(4 + 4 + $bvar(&rconPassword,0) + 1 + 0 + 1)
    bset &packetlength 1 %packetlength
  }
  elseif ($1 == exec) {
    var %command = $3-
    echo @sourcercon $time([(ddd)HH:nn:ss]) sending rcon command %command to $sock(%sock).mark

    ;we increase the reqid for each command

    inc % [ $+ [ %sock ] $+ .reqid ]

    %reqid = % [ $+ [ %sock ] $+ .reqid ]
    ;reset the request ID back to 1 if it gets bigger than 255

    if (%reqid > 255) {
      %reqid = 1
      set % [ $+ [ %sock ] $+ .reqid ] 1
    }
    bset &conType 1 %SERVERDATA_EXECCOMMAND

    var %x = 1, %y = $len(%command)
    while (%x <= %y) {
      ;echo @sourcercon adding $mid(%command,%x,1) to &command at pos %x
      bset &command %x $asc($mid(%command,%x,1))
      inc %x
    }

    echo @sourcercon $time([(ddd)HH:nn:ss]) RCON: Command Send Check: $bvar(&command,1-).text

    ;packet length: 4 bytes for id, 4 bytes for request type, n bytes for command, 1 byte for null between strings, and then 1 null byte for the end, 0 is the length of our null string
    var %packetlength = $calc(4 + 4 + $bvar(&command,0) + 1 + 0 + 1)
    bset &packetlength 1 %packetlength
    if (%packetlength > 255) {
      echo @sourcercon $time([(ddd)HH:nn:ss]) 4PACKET LENGTH IS TOO LONG. NOT SENDING COMMAND
      unset % [ $+ [ %sock ] $+ .waiting ]
      return
    }
  }
  else {
    echo @sourcercon $time([(ddd)HH:nn:ss]) 4I'm not sure how you got this far, but something is horribly wrong
    return
  }

  echo @sourcercon $time([(ddd)HH:nn:ss]) packetlength: %packetlength

  bset &reqid 1 %reqid

  ;now we pack it together
  bcopy &rcon 1 &packetlength 1 $bvar(&packetlength,0)
  bcopy &rcon 5 &reqid 1 $bvar(&reqid,0)
  bcopy &rcon 9 &conType 1 $bvar(&conType,0)
  if ($1 = auth) {
    bcopy &rcon 13 &rconPassword 1 $bvar(&rconPassword,0)
  }
  else {
    bcopy &rcon 13 &command 1 $bvar(&command,0)
  }
  ;null byte between strings
  bset &rcon $calc($bvar(&rcon,0) + 1) 0
  ;this position is an emtpy string. but we dont consider it a 'null'
  ;bcopy &rcon $calc($bvar(&rcon,0) + 1) &nullString 1 $bvar(&nullString,0)
  ;ending null byte
  bset &rcon $calc($bvar(&rcon,0) + 1) 0

  echo @sourcercon $time([(ddd)HH:nn:ss]) DATA TO SEND: $bvar(&rcon,1,$bvar(&rcon,0))

  if (!$sock($2)) {
    echo @sourcercon $time([(ddd)HH:nn:ss]) 4ERROR: The RCON socket died between starting the send command and writing to the socket. Attempting to reconnect
    sourceRcon % [ $+ [ %sock ] $+ .ip ] % [ $+ [ %sock ] $+ .port ] % [ $+ [ %sock ] $+ .rcon ] % [ $+ [ %sock ] $+ .command ]
    return
  }

  sockwrite %sock &rcon
}

on *:sockwrite:rcon.*:{
  if ($sockerr) {
    echo @sourcercon $time([(ddd)HH:nn:ss]) $sockname had socket error $sock($sockname).wserr ( $+ $sock($sockname).wsmsg $+ )
  }
  echo @sourcercon $time([(ddd)HH:nn:ss]) 11Send queue empty. Ready for next command
  unset % [ $+ [ $sockname ] $+ .waiting ]
  unset % [ $+ [ $sockname ] $+ .sending ]
}

on *:sockclose:rcon.*:{
  if ($sockerr) {
    echo @sourcercon $time([(ddd)HH:nn:ss]) $sockname had socket error $sock($sockname).wserr ( $+ $sock($sockname).wsmsg $+ )
  }
  echo @sourcercon $time([(ddd)HH:nn:ss]) socket $sockname closed by server

  var %ip, %port, %rcon, %command

  if ((% [ $+ [ $sockname ] $+ .waiting ]) || (% [ $+ [ $sockname ] $+ .sending ])) {
    echo @sourcercon $time([(ddd)HH:nn:ss]) 4Socket had error while sending. Will attempt to resend
    %ip = % [ $+ [ $sockname ] $+ .ip ]
    %port = % [ $+ [ $sockname ] $+ .port ]
    %rcon = % [ $+ [ $sockname ] $+ .rcon ]
    %command = % [ $+ [ $sockname ] $+ .command ]
  }

  rconSockCleanup $sockname

  if (%ip) {
    sourceRcon %ip %port %rcon %command
  }
}

alias rconSockCleanup {
  ;cleanup $sockname
  echo @sourcercon $time([(ddd)HH:nn:ss]) 9Cleaning up socket $1
  var %error

  if ($sock($1)) {
    if (!$sock($1).sq) && (!$sock($1).rq) {
      echo @sourcercon $time([(ddd)HH:nn:ss]) 9Manually closing socket during cleanup
      sockclose $1
    }
    else {
      echo @sourcercon $time([(ddd)HH:nn:ss]) 9Socket is still in sending or receiving queue. Not cleaning up
      if ($sock($1).wserr) {
        %error = $v1
        echo @sourcercon $time([(ddd)HH:nn:ss]) 9Socket error. Therefore cleanup in 3 seconds
        .timer $+ $1 1 3 rconSockCleanup $1
      }
      return
    }
  }

  unset % [ $+ [ $1 ] $+ .* ]
  unset % [ $+ [ $1 ] ]

  if ($timer($1)) {
    .timer $+ $1 off
  }

  if (%error) {
    echo @sourcercon $time([(ddd)HH:nn:ss]) 9Socket had error $v1 $+ . Creating timer to prevent instant re-creation of the socket
    .timer $+ $1 $+ error 1 5 return
  }
}

alias rconQueueCheck {
  if ($sock($1)) {
    if ($read($1 $+ .queue)) {
      var %x = 1, %y = $lines($1 $+ .queue)
      while (%x <= %y) {
        var %line = $read($1 $+ .queue,%x)

        echo @sourcercon $time([(ddd)HH:nn:ss]) Found %line in queue. Executing

        sourceRcon %line

        inc %x
      }
      write -c $1 $+ .queue
    }
  }
}

alias testspam {
  rcon say testing1
  rcon say testing2
  rcon say testing3
  rcon say testing4
  rcon say testing5
  timer
}
