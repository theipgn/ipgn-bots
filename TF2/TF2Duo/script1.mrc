alias generateseeds {
  var %y = $calc(%ulticomp.teams - $ini(ulticomp.ini,0))
  var %x = 1
  while (%x <= %ulticomp.teams) {
    addteam $lower($read(passwords.txt)) $lower($read(passwords.txt))
    inc %x
  }
}

alias fakeresults {
  var %x = 1
  var %y = $ini(ulticomp_matches.ini,0)
  while (%x <= %y) {
    var %match = $getmatch(%x)
    var %winner = $gettok(%match,1,37)
    if (!$gettok(%match,3,37)) {
      msg %ulticomp.chan $codes(Result for match %x (Round: $gettok(%match,4,37) $+ ) ( $+ $gettok($getteam($gettok(%match,1,37)),1,37) vs $gettok $gettok($getteam($gettok(%match,2,37)),1,37) $+ ) $&
        entered. Winner: $gettok($getteam(%winner),1,37) (Seed: %winner $+ ))

      writeini ulticomp_matches.ini %x winner %winner
      writeini ulticomp.ini $iif(%winner = $gettok(%match,1,37),$gettok(%match,2,37),$gettok(%match,1,37)) eliminated $iif(%winner = $gettok(%match,1,37),$gettok(%match,1,37),$gettok(%match,2,37))
      writeini ulticomp_servers.ini $gettok(%match,5,37) inuse 0
      servercheck
    }
    inc %x
  }
  roundcalc
}
