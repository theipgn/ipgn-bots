/*
//ipgn GET /forum/newthread.php do=newthread&f=69
//ipgn POST /forum/newthread.php f=69&securitytoken= $+ %sectoken $+ &subject=ropfltestetetet&message=jkwbkewjfbwkjfbwkjefbwkje1231231egdfedfg&do=postthread&sbutton=Submit+New+Thread

//ipgn GET /forum/newreply.php do=newreply&noquote=1&p=936620
//ipgn POST /forum/newreply.php securitytoken= $+ %sectoken $+ &do=postreply&t=96070&message=testingwdup&sbutton=Submit+Reply
*/

on *:START:{
  window -e @forum
}

alias posttest {
  forumControl GET /newreply.php do=newreply&noquote=1&p= $+ $1
  .timer 1 3 forumControl POST /newreply.php do=postreply&p= $+ $1 $+ $&
    &message=This is another test $+ $crlf $+ with multiple $+ $crlf $+ lines $+ &sbutton=Submit+Reply
}

alias newthreadtest {
  forumControl GET /newthread.php do=newthread&f=14
  .timer 1 3 forumControl POST /newthread.php f=1576&subject=This is a test2 $+ &message=This is a test $+ $crlf $+ with multiple $+ $crlf $+ lines $&
    $+ &do=postthread&sbutton=Submit+New+Thread
}

alias forumpost {
  if ($timer(forumwait)) {
    .timer 1 $calc($timer(forumwait).secs + 2) forumpost $1-
    return
  }

  .timerforumwait 1 8 return

  if ($1 = endround) {
    if (%ulticomp.forumthread) {
      var %x = 1, %y = $ini(ulticomp_matches.ini,0), %forumpost = Round $calc(%ulticomp.round - 1) match results:

      while (%x <= %y) {
        var %round = $readini(ulticomp_matches.ini,%x,round)

        if (%round == $calc(%ulticomp.round - 1)) {
          var %team2 = $readini(ulticomp_matches.ini,%x,team2)
          var %team1ts = $strip($teamstring($readini(ulticomp_matches.ini,%x,team1)))

          echo @forum MATCH %x IS IN PREV ROUND

          if (%team2 != bye) {
            echo @forum NO BYE

            var %team2ts = $strip($teamstring(%team2))

            var %winnerts
            if ($readini(ulticomp_matches.ini,%x,winner) == %team2) {
              %winnerts = %team2ts
            }
            else {
              %winnerts = %team1ts
            }

            echo @forum Adding Match %x $+ : %team1ts [b]vs[/b] %team2ts $+ . [b][u]Winner:[/b][/u] %winnerts to post

            %forumpost = %forumpost $+ $crlf $+ Match %x $+ : %team1ts [b]vs[/b] %team2ts $+ . [b][u]Winner:[/b][/u] %winnerts

            echo @forum Current forumpost: %forumpost
          }
          else {
            %forumpost = %forumpost $+ $crlf $+ Match %x $+ : %team1ts had a BYE
          }
        }

        inc %x
      }

      echo @forum FORUM POST: %forumpost
      ;http://ozfortress.com/showpost.php?p=794665&postcount=2
      forumControl GET /newreply.php do=newreply&noquote=1&p= $+ %ulticomp.forumthread

      .timer 1 3 forumControl POST /newreply.php do=postreply&p= $+ %ulticomp.forumthread $&
        $+ &message= $+ %forumpost $&
        $+ &sbutton=Submit+Reply
    }
    else {
      ; create thread
      forumControl GET /newthread.php do=newthread&f= $+ %ulticomp.forumid

      var %x = 1, %y = $ini(ulticomp_matches.ini,0), %forumpost = Round $calc(%ulticomp.round - 1) match results:

      while (%x <= %y) {
        var %round = $readini(ulticomp_matches.ini,%x,round)

        if (%round == $calc(%ulticomp.round - 1)) {
          var %team2 = $readini(ulticomp_matches.ini,%x,team2)
          var %team1ts = $strip($teamstring($readini(ulticomp_matches.ini,%x,team1)))

          echo @forum MATCH %x IS IN PREV ROUND

          if (%team2 != bye) {
            echo @forum NO BYE

            var %team2ts = $strip($teamstring(%team2))

            var %winnerts
            if ($readini(ulticomp_matches.ini,%x,winner) == %team2) {
              %winnerts = %team2ts
            }
            else {
              %winnerts = %team1ts
            }

            echo @forum Adding Match %x $+ : %team1ts [b]vs[/b] %team2ts $+ . [b][u]Winner:[/b][/u] %winnerts to post

            %forumpost = %forumpost $+ $crlf $+ Match %x $+ : %team1ts [b]vs[/b] %team2ts $+ . [b][u]Winner:[/b][/u] %winnerts

            echo @forum Current forumpost: %forumpost
          }
          else {
            %forumpost = %forumpost $+ $crlf $+ Match %x $+ : %team1ts had a BYE
          }
        }

        inc %x
      }

      echo @forum FORUM POST: %forumpost

      .timer 1 3 forumControl POST /newthread.php f= $+ %ulticomp.forumid $+ &subject= $+ %challonge_url $&
        $+ &message= $+ %forumpost $&
        $+ &do=postthread&sbutton=Submit+New+Thread
    }
  }

  if ($1 = startcomp) {
    ;create thread
    forumControl GET /newthread.php do=newthread&f= $+ %ulticomp.forumid

    var %forumpost = %challonge_name seeds:

    ;http://images.challonge.com/ultiduo6.png
    var %x = 1, %y = $ini(ulticomp.ini,0)
    while (%x <= %y) {
      var %player1 = $remove($readini(ulticomp.ini,%x,teammate),$chr(34))
      var %player2 = $remove($readini(ulticomp.ini,%x,teamcaptain),$chr(34))

      %forumpost = %forumpost $+ $crlf $+ %x $+ : %player1 / %player2

      inc %x
    }


    echo @forum FORUM POST: %forumpost

    .timer 1 3 forumControl POST /newthread.php f= $+ %ulticomp.forumid $+ &subject= $+ %challonge_url $&
      $+ &message= $+ [center][size=7] $+ %challonge_name $+ [/size][/center] $+ $crlf $+ $crlf $+ $crlf $+ Challonge ladder: [url]http://images.challonge.com/ $+ %challonge_url $+ .png[/url] $+ $crlf $&
      $+ Realtime match results: [url]http://ultiduo.ipgn.com.au/matches/[/url] $+ $crlf $+ Realtime seed status: [url]http://ultiduo.ipgn.com.au/teams/[/url] $+ $crlf $+ $crlf $&
      $+ STV: [url=steam://connect/202.138.3.13:27035/]badger.ipgn.com.au:27035[/url] $+ $crlf $+ $crlf $+ $crlf $&
      $+ %forumpost $&
      $+ &do=postthread&sbutton=Submit+New+Thread
  }

}

alias forumControl {
  ;this is all for now  
  var %sock = forum. $+ $ticks

  set % [ $+ [ %sock ] $+ .cookiefile ] ozf_cookie.txt

  set % [ $+ [ %sock ] $+ .message ] $3-

  sockopen %sock ozfortress.com 80

  sockmark %sock $1-2
}

on *:sockopen:forum.*:{
  var %string

  var %method = $getmark($sockname,1)
  var %page = $getmark($sockname,2)

  var %w = sockwrite -n $sockname

  if (%method == GET) {
    %string = $urlencode_string($getmark($sockname,3-))
    %w GET $+(%page,?,%string) HTTP/1.1
  }
  elseif (%method == POST) {
    ;%string = $urlencode_string($+(securitytoken=,%sectoken,&,% [ $+ [ $sockname ] $+ .message ]))
    %string = $+(securitytoken=,%sectoken,&,% [ $+ [ $sockname ] $+ .message ])
    echo @forum POST STRING: %string
    %w POST %page HTTP/1.1
  }
  else {
    ;unsupported
    sockclose $sockname
  }

  %w Host: ozfortress.com

  ;needa send auth cookies and shiz
  var %cookiefile = % [ $+ [ $sockname ] $+ .cookiefile ]

  if ($exists(%cookiefile)) {
    var %x = 1, %y = $lines(%cookiefile), %cookieline
    while (%x <= %y) {
      ;cookies must be separated with semicolon
      if ($read(%cookiefile,%x)) {
        %cookieline = $addtok(%cookieline,$v1,59)
      }

      inc %x
    }
    %w Cookie: %cookieline
  }

  ;if it's a post, need content length and type
  if (%method == POST) {
    %w Content-Length: $len(%string)
    %w Content-Type: application/x-www-form-urlencoded
  }

  %w Connection: close

  %w Accept: */*
  %w Accept-Charset: *
  %w Accept-Encoding: *
  %w Accept-Language: *
  %w User-Agent: Mozilla/5.0

  if (%method == POST) {
    %w
    %w %string
  }
  else {
    %w
  }
}

on *:sockread:forum.*:{
  var %data

  :nextread
  sockread %data

  if ($sockbr == 0) {
    return
  }

  if (%data) && ($getmark($sockname,1) == POST) { echo @forum %data }

  ;var SECURITYTOKEN = "1325744007-ab6a697a46af5c10daa1a4e1f818b196785d502f";
  if ($regex(%data,var SECURITYTOKEN = "(.*?)";)) {
    set % [ $+ [ $sockname ] $+ .sectoken ] $regml(1)
    set %sectoken $regml(1)
    echo @forum got security token $regml(1)
  }

  ;if we get new cookies to set:
  if ($regex(%data,/^Set-Cookie: (.+?)=(.+?);/i)) {
    var %cookiefile = % [ $+ [ $sockname ] $+ .cookiefile ]
    echo @forum Recieved Cookie: $regml(1) data: $regml(2)
    if (*lastview* !iswm $regml(1)) {
      if ($read(%cookiefile, w, $regml(1) $+ =*)) {
        write -l $+ $readn %cookiefile $+($regml(1),=,$regml(2))
      }
      else {
        write %cookiefile $+($regml(1),=,$regml(2))
      }
    }
  }

  if ($regex(%data,/Location: .+showthread.php\?p=(\d+)(&posted=1)?#post(\d+)/)) {
    var %postid = $regml(1)
    echo @forum Post id: $regml(1)

    set %ulticomp.forumthread %postid
  }

  if (</html> isin %data) {

  }

  goto nextread
}

on *:sockclose:forum.*:{
  unset % [ $+ [ $sockname ] $+ .cookiefile ]
}

alias getmark {
  ; $getmark(socketname,N)
  ; This alias returns the Nth word from the socketmark from socket socketname
  return $gettok($sock($1).mark,$$2,32)
}

alias urlencode_string {
  ; Encodes a whole string of data in the format name1=data1&name2=data2
  ; Example: $urlencode_string(name1=test&name2=test2)
  ; Returns: name1=%74%65%73%74&name2=%74%65%73%74%32
  var %a = 1, %string = $1, %output
  while ($gettok(%string,%a,38)) {
    tokenize 61 $v1
    if (%a != 1) %output = %output $+ &
    %output = $+(%output,$1,=,$urlencode($2))
    inc %a
  }
  return %output
}

alias urlencode {
  var %a = $regsubex($$1,/([^\w\s])/Sg,$+(%,$base($asc(\t),10,16,2)))
  return $replace(%a,$chr(32),$chr(43))
}
