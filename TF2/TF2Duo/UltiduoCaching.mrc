alias cacheteams {
  ;@return player%captain%eliminated?%match ids

  if ($hget(teams)) {
    hfree teams
  }

  hmake teams 100

  var %x = 1, %y = $ini(ulticomp.ini,0)

  while (%x <= %y) {
    var %teammate = $readini(ulticomp.ini,%x,teammate)
    var %teamcaptain = $readini(ulticomp.ini,%x,teamcaptain)
    var %eliminated = $readini(ulticomp.ini,%x,eliminated)
    var %matches = $readini(ulticomp.ini,%x,matches) 

    hadd teams %x $+(%teammate,$chr(37),%teamcaptain,$chr(37),%eliminated,$chr(37),%matches)

    inc %x
  }

  echo @debug Finished caching teams
}

alias cachematches {
  ;@return team1%team2%winner%round%server

  if ($hget(matches)) {
    hfree matches
  }

  hmake matches 100

  var %x = 1, %y = $ini(ulticomp_matches.ini,0)

  while (%x <= %y) {
    var %team1 = $readini(ulticomp_matches.ini,%x,team1)
    var %team2 = $readini(ulticomp_matches.ini,%x,team2)
    var %winner = $readini(ulticomp_matches.ini,%x,winner)
    var %round = $readini(ulticomp_matches.ini,%x,round)
    var %server = $readini(ulticomp_matches.ini,%x,server)

    hadd matches %x $+(%team1,$chr(37),%team2,$chr(37),%winner,$chr(37),%round,$chr(37),%server)

    inc %x
  }

  echo @debug Finished caching matches
}

alias cacheservers {
  ;@return ip%port%rcon%password%name%matchid on server

  if ($hget(servers)) {
    hfree servers
  }

  hmake servers 100

  var %x = 1, %y = $ini(ulticomp_matches.ini,0)

  while (%x <= %y) {
    var %ip = $readini(ulticomp_servers.ini,%x,ip)
    var %port = $readini(ulticomp_servers.ini,%x,port)
    var %rcon = $readini(ulticomp_servers.ini,%x,rcon)
    var %password = $readini(ulticomp_servers.ini,%x,password)
    var %name = $readini(ulticomp_servers.ini,%x,name)
    var %inuse = $readini(ulticomp_servers.ini,%x,inuse)

    hadd servers %x $+(%ip,$chr(37),%port,$chr(37),%rcon,$chr(37),%password,$chr(37),%name,$chr(37),%inuse)

    inc %x
  }

  echo @debug Finished caching servers
}
