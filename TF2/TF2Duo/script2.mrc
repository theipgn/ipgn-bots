on *:TEXT:*:#ozf-help:{
  if ($nick !isop $chan) { return }
  if ($1 = !hotserverpass) {
    writeini ulticomp_servers.ini %hotserverid password $2
    msg $nick $codes(Hot server password changed to $2)
  }
  if ($1 = !udaddserver) {
    if ((!$2) || (!$3) || (!$4) || (!$5)) {
      msg $chan $codes(Usage: !addserver <ip> <port> <rcon> <server name>)
      return
    }
    var %x = 1, %sExists = 0
    var %y = $ini(ulticomp_servers.ini,0)
    while (%x <= %y) {
      if ($2 = $gettok($getserver(%x),1,37)) && ($3 = $gettok($getserver(%x),2,37)) {
        msg %ulticomp.chan $codes(That server is already on the server list)
        %sExists = 1
        break
      }
      inc %x
    }
    if (!%sExists) {
      writeini ulticomp_servers.ini $calc(%y + 1) ip $2
      writeini ulticomp_servers.ini $calc(%y + 1) port $3
      writeini ulticomp_servers.ini $calc(%y + 1) rcon $4
      writeini ulticomp_servers.ini $calc(%y + 1) password 0
      writeini ulticomp_servers.ini $calc(%y + 1) name $5-
      writeini ulticomp_servers.ini $calc(%y + 1) inuse 0
      msg $chan $codes(Server added to the server list.)
    }
  }
  if ($1 = !udservers) {
    var %x = 1
    var %y = $ini(ulticomp_servers.ini,0)
    while (%x <= %y) {
      var %server = $getserver(%x)
      echo @debug Server Listing: %server
      notice $nick $codes(Server $clr(num,%x) ( $+ $gettok(%server,5,37) $+ ): $gettok(%server,1,37) $+ : $+ $gettok(%server,2,37) RCON: $gettok(%server,3,37) $&
        $iif($gettok(%server,4,37),Password: $gettok(%server,4,37),$null) Match in progress: $&
        $iif($gettok(%server,6,37),Match $clr(num,$gettok(%server,6,37)) ( $+ $teamstring($gettok($getmatch($gettok(%server,6,37)),1,37)) $&
        vs $teamstring($gettok($getmatch($gettok(%server,6,37)),2,37)) $+ ),None))
      inc %x
    }
  }
  if ($1 = !uddeleteserver) {
    if (!$2) { msg $chan $codes(You need to specify a server ID to remove. Eg !deleteserver $clr(num,2)) | halt }
    var %server = $getserver($2)
    if (!%server) { msg $chan $codes(Server does not exist) | halt }
    if ($gettok(%server,6,37)) { msg $chan $codes(That server is currently in use you cannot delete it!) | halt }
    var %x = $2
    var %y = $ini(ulticomp_servers.ini,0)
    while (%x < %y) {
      writeini ulticomp_servers.ini %x ip $readini(ulticomp_servers.ini,$calc(%x + 1),ip)
      writeini ulticomp_servers.ini %x port $readini(ulticomp_servers.ini,$calc(%x + 1),port)
      writeini ulticomp_servers.ini %x rcon $readini(ulticomp_servers.ini,$calc(%x + 1),rcon)
      writeini ulticomp_servers.ini %x password $readini(ulticomp_servers.ini,$calc(%x + 1),password)
      writeini ulticomp_servers.ini %x name $readini(ulticomp_servers.ini,$calc(%x + 1),name)
      writeini ulticomp_servers.ini %x inuse $readini(ulticomp_servers.ini,$calc(%x + 1),inuse)
      inc %x
    }
    remini ulticomp_servers.ini %y
    msg $chan $codes(Server ID $clr(num,$2) ( $+ $gettok(%server,5,37) $+ ) has been removed.)
  }
  if ($1 = !seed) {
    if (%ulticomp.started) { msg $chan $codes(Competition has already started, you can no longer modify seeds) | halt }
    if (!$2) || (!$3) { msg $chan $codes(You need to specify the teams old position and new position. Eg !seed 4 2, would move seed $clr(snum,4) to seed $clr(snum,2)) | halt }
    if ($3 > %ulticomp.seedthreshold) { msg $chan $codes(You can't seed teams below seed $clr(num,%ulticomp.seedthreshold)) | halt }
    if ($2 == $3) { msg $chan $codes(You can't really reseed someone at the same position they're already in...) | halt }
    if ($getteam($2)) && ($getteam($3)) {
      moveseed $2 $3 
    }
    upload teams ulticomp.ini
  }

  if ($1 = !challongedet) {
    set %challonge_name $2
    set %challonge_url $2

    msg $chan $codes(Set challonge_name to $2)
  }
}
