on *:START:{
  window -e @upload
}

alias upload {
  :error
  if ($error) {
    echo -a ERORR IN UPLOAD: $error

    reseterror

    .timer 1 2 upload $1-
    return
  }

  set -u5 %uploadticker 1

  var %sock = upload. [ $+ [ $calc($ticks + %uploadticker) ] ]

  if ($sock(%sock)) {
    .timer 1 2 upload $1-
    return
  }

  echo UPLOAD SOCKET: %sock

  inc %uploadticker

  ;-- Set these variables --
  set % [ $+ [ %sock ]  $+ .hostofphpfile ] ultiduo.ipgn.com.au
  set % [ $+ [ %sock ] $+ .locationofphpfile ] $+(/upload $+ $1 $+ .php?password=HeatoN)
  ;-------------------------
  if (!$2) { msg %pug.adminchan $codes(Upload error: no file specified. alias call: upload $1-) }
  var %dest = $2-

  set % [ $+ [ %sock ] $+ .file ] %dest

  if ((!$file(%dest)) || ($isdir(%dest))) return

  echo @upload Uploading file %dest $+ ....
  sockopen %sock % [ $+ [ %sock ]  $+ .hostofphpfile ] 80
}
on *:sockopen:upload.*:{
  var %upfile = $+($sockname,.tmp)
  var %b = $md5() | write -c %upfile $+(--,%b,$lf,$&
    Content-Disposition: form-data; name="uploaded";,$&
    filename=",$nopath(% [ $+ [ $sockname ] $+ .file ]),",$lf,$lf)
  .copy -a " $+ % [ $+ [ $sockname ] $+ .file ] $+ " %upfile

  write %upfile -- $+ %b

  var %write = sockwrite -tn $sockname
  %write POST % [ $+ [ $sockname ] $+ .locationofphpfile ] HTTP/1.1
  %write Host: % [ $+ [ $sockname ]  $+ .hostofphpfile ]
  %write Connection: close
  %write Content-Type: multipart/form-data; boundary= $+ %b
  %write Content-Length: $file(%upfile)
  %write
  .fopen $sockname %upfile
}

on *:sockwrite:upload.*:{
  if (($sock($sockname).sq > 8192) || (!$fopen($sockname))) { 
    return 
  }

  if ($fread($sockname,8192,&a)) {
    sockwrite $sockname &a
  }
  else {
    .fclose $sockname
  }
}

on *:sockread:upload.*:{
  var %temp
  sockread %temp

  if (%temp) {
    echo @upload %temp
  }

  if (& 2* iswm %temp) {
    echo 3 @upload SUCCESS!
  }
  elseif (& 4* iswm & temp) {
    echo 4 -a FAILED!
  }
}

on *:sockclose:upload.*:cleanupload $sockname

alias cleanupload {
  .sockclose $1
  if ($fopen($1)) { 
    .fclose $1
  }

  unset % [ $+ [ $1 ] $+ .* ]

  .remove $1 $+ .tmp
}
