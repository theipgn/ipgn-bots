on *:START:{
  window -e @debug
  hmake signed 200
}

on *:NOTICE:*:*:{
  if (*I recognize you* iswm $1-) {
    .timer 1 10 join #ultiduo
    .timer 1 10 join #ozf-help
    .timer 1 10 join #tf2pug
    .timer 1 10 join #ozfortress
    .timer 1 10 join #ipgn
  }
}

on *:CONNECT:{
  .timer 1 10 join #ultiduo
  .timer 1 10 join #ozf-help
  .timer 1 10 join #tf2pug
  .timer 1 10 join #ozfortress
  .timer 1 10 join #ipgn
}

alias msgpubchannels {
  msg #ozfortress $1-
  msg #ipgn $1-
  msg #tf2pug $1-
}

alias rcon {
  .run -n $shortfn(D:\iPGN-Bots\TF2\SourceRcon.exe) $1 $2 $3 " $+ $4- $+ "
  ;sourceRcon $1 $2 $3 $4-
}

alias codes {
  return 7 $+ $chr(171) $+  $1- 7 $+ $chr(187)
}

alias clr {
  if (!$2) && ($2 != 0) { return COLOUR ERROR: NO TEXT TO COLOUR }
  if ($1 = str) {
    return 12 $+ $2 $+ 
  }
  elseif ($1 = num) {
    return 04 $+ $2 $+ 
  }
  elseif ($1 = g) {
    return 09 $+ $2 $+ 
  }
  elseif ($1 = y) {
    return 08 $+ $2 $+ 
  }
  elseif ($1 = dg) {
    return 03 $+ $2 $+ 
  }
  elseif ($1 = name) {
    return 07 $+ $2 $+ 
  }
  elseif ($1 = pu) {
    return 06 $+ $2 $+ 
  }
  elseif ($1 = cy) {
    return 11 $+ $2 $+ 
  }
  elseif ($1 = snum) {
    return 13 $+ $2 $+ 
  }
  else {
    return COLOUR ERROR: INVALID COLOUR SPECIFIED
  }
}

alias newtopic {
  if ($1) {
    set %ulticomp.status $1-
  }
  topic %ulticomp.chan 0,1http://12ozfortress0.com 7::0 http://12ultiduo0.12ipgn0.com.au 7::0 %ulticomp.type 7::0 %ulticomp.status
}

alias randomizeseeds {
  var %x = $round($calc((%ulticomp.teams / 2) + 1),0)
  var %y = $ini(ulticomp.ini,0)
  while (%x <= %y) {
    var %xteam = $getteam(%x)
    var %randseed = $round($rand($calc(%ulticomp.seedthreshold + 1),%y),0)
    var %randseedteam = $getteam(%randseed)
    writeini ulticomp.ini %randseed teammate $gettok(%xteam,1,37)
    writeini ulticomp.ini %randseed teamcaptain $gettok(%xteam,2,37)
    writeini ulticomp.ini %randseed eliminated $gettok(%xteam,3,37)
    writeini ulticomp.ini %randseed matches $gettok(%xteam,4,37)

    writeini ulticomp.ini %x teammate $gettok(%randseedteam,1,37)
    writeini ulticomp.ini %x teamcaptain $gettok(%randseedteam,2,37)
    writeini ulticomp.ini %x eliminated $gettok(%randseedteam,3,37)
    writeini ulticomp.ini %x matches $gettok(%randseedteam,4,37)
    inc %x
  }
}

alias createdraws {
  write -c teampriorities.txt
  ;if (%y >= 2^x) && (%y <= 2^(x + 1))
  if (%ulticomp.round == 1) {
    var %y = $ini(ulticomp.ini,0)
    var %a = 1
    var %b = $int($calc($log(%ulticomp.teams) / $log(2)))
    while (%a <= %b) {
      echo @debug Draw upper limit: $calc(2 ^ (%a + 1))
      echo @debug Draw lower limit: $calc(2 ^ %a)
      if (%y >= $calc(2 ^ %a)) && (%y <= $calc(2 ^ (%a + 1))) {
        var %numbyes = $calc(2 ^ (%a + 1) - %y)
        var %num_games = $calc((%y - %numbyes)/2)
        echo @debug NUMBER OF BYES: %numbyes
        echo @debug NUMBER OF GAMES: %num_games
        set %ulticomp.maxgamespr %num_games
        set %ulticomp.maxgamesbyespr $calc(%num_games + %numbyes)

        var %x = 1, %byeteams
        while (%x <= %numbyes) {
          %byeteams = $addtok(%byeteams,%x,32)
          echo @debug BYE FOR %x
          creatematch %x bye %ulticomp.round
          inc %x
        }
        var %games = 1
        %x = $calc(%numbyes + 1)
        echo @debug INITIAL TEAM FOR MATCHING: %x
        var %x2 = 0
        while (%games <= %num_games) {
          creatematch %x $calc(%y - %x2) %ulticomp.round
          inc %games
          inc %x
          inc %x2
        }
        break
      }
      inc %a
    }
  }
  else {
    var %y = $ini(ulticomp_matches.ini,0)
    var %winning_seeds, %x = 1 
    while (%x <= %y) {
      var %match = $getmatch(%x)
      if ($gettok(%match,4,37) = $calc(%ulticomp.round - 1)) && ($gettok(%match,3,37)) {
        %winning_seeds = $addtok(%winning_seeds,$gettok(%match,3,37),32)
      }
      inc %x
    }
    var %numseeds = $numtok(%winning_seeds,32), %x = 1, %x2 = 0
    var %num_games = $calc(%numseeds / 2)
    set %ulticomp.maxgamespr %num_games
    while (%x <= %num_games) {
      creatematch $gettok(%winning_seeds,%x,32) $gettok(%winning_seeds,$calc(%numseeds - %x2),32) %ulticomp.round
      inc %x
      inc %x2
    }
  }

  var %x = 1
  var %y = $ini(ulticomp_matches.ini,0)
  while (%x <= %y) {
    var %match = $getmatch(%x)
    if ($gettok(%match,4,37) = %ulticomp.round) && ($gettok(%match,2,37) != bye) {
      write teampriorities.txt $+(%x,$chr(32),$matchpriority(%match))
      var %match = $getmatch(%x)
      msg %ulticomp.chan $codes(Match $clr(num,%x) (Round: $clr(num,$gettok(%match,4,37)) $+ ): $teamstring($gettok(%match,1,37)) $&
        $iif($gettok(%match,2,37) != bye,vs $teamstring($gettok(%match,2,37)),(Bye)))
    }
    inc %x
  }

  if (%ulticomp.round > 1) {
    filter -ffceut 2 32 teampriorities.txt sortedpriority.txt
    if (%ulticomp.maxgamespr > 4) { %y = $calc(%ulticomp.maxgamespr / 4) }
    else { %y = $lines(sortedpriority.txt) }
    %x = 1
    set %ulticomp.hottys
    unset %hotty.*
    while (%x <= %y) {
      var %line = $read(sortedpriority.txt,%x)
      echo @debug Adding HOTTY $gettok(%line,1,32) with priority $gettok(%line,2,32)
      set %ulticomp.hottys $addtok(%ulticomp.hottys,$gettok(%line,1,32),32)
      inc %x
    }
    echo @debug THE HOTTYS: %ulticomp.hottys
  }

  unset %ulticomp.roundpause

  if (%ulticomp.maxgamespr == 1) && (%ulticomp.round > 1) {
    var %finalmatch = $getmatch(%ulticomp.hottys)
    newtopic This is the final round! The match will begin in 3 minutes. STV: 202.138.3.13:27035
    msgpubchannels $codes(The final %ulticomp.type match will begin in $clr(num,3) minutes. The grand final match is between $teamstring($gettok(%finalmatch,1,37)) and $&
      $teamstring($gettok(%finalmatch,2,37)) $+ . Who will be the grand champions? Jump on the STV at 202.138.3.13:27035 to find out!)
  }
  else {
    newtopic Round %ulticomp.round in progress. Use !status and !matches to see the progress of the round
    msgpubchannels $codes(%ulticomp.type round $clr(num,%ulticomp.round) has started. Join %ulticomp.chan for match-by-match results, alternatively check http://ultiduo.ipgn.com.au)
  }

  servercheck
  .timerupload 1 1 upload matches ulticomp_matches.ini
  upload teams ulticomp.ini
  if (!%challonge_started) { .timer 1 3 challonge startTournament }
  ;.timer 1 20 challonge getmatchid %ulticomp.round
  ;  .timer 1 10 fakeresults
}

alias creatematch {
  var %matches = $ini(ulticomp_matches.ini,0)
  writeini ulticomp_matches.ini $calc(%matches + 1) team1 $1
  writeini ulticomp_matches.ini $calc(%matches + 1) team2 $2
  writeini ulticomp_matches.ini $calc(%matches + 1) winner $iif($2 = bye,$1,0)
  writeini ulticomp_matches.ini $calc(%matches + 1) round $3
  writeini ulticomp_matches.ini $calc(%matches + 1) server 0
  writeini ulticomp.ini $1 matches $addtok($remtok($readini(ulticomp.ini,$1,matches),0,32),$calc(%matches + 1),32)
  if ($2 != bye) { writeini ulticomp.ini $2 matches $addtok($remtok($readini(ulticomp.ini,$2,matches),0,32),$calc(%matches + 1),32)) }
}

alias moveseed {
  var %seed_target_details = $getteam($2)
  if ($1 > $2) {
    var %seed_position = $1
    var %seed_target = $2
    var %seed_position_details = $getteam($1)
    var %x = $calc(%seed_position - 1)
    while (%x >= %seed_target) {
      var %xteam = $getteam(%x)
      writeini ulticomp.ini $calc(%x + 1) teammate $gettok(%xteam,1,37)
      writeini ulticomp.ini $calc(%x + 1) teamcaptain $gettok(%xteam,2,37)
      writeini ulticomp.ini $calc(%x + 1) eliminated $gettok(%xteam,3,37)
      writeini ulticomp.ini $calc(%x + 1) matches $gettok(%xteam,4,37)

      adjustsigned %x $calc(%x + 1)

      dec %x
    }
    writeini ulticomp.ini %seed_target teammate $gettok(%seed_position_details,1,37)
    writeini ulticomp.ini %seed_target teamcaptain $gettok(%seed_position_details,2,37)
    writeini ulticomp.ini %seed_target eliminated $gettok(%seed_position_details,3,37)
    writeini ulticomp.ini %seed_target matches $gettok(%seed_position_details,4,37)
    adjustsigned %seed_position %seed_target
  }
  elseif ($1 < $2) {
    var %seed_position = $1
    var %seed_target = $2
    var %seed_position_details = $getteam($1)
    var %x = $calc(%seed_position + 1)
    while (%x <= %seed_target) {
      var %xteam = $getteam(%x)
      writeini ulticomp.ini $calc(%x - 1) teammate $gettok(%xteam,1,37)
      writeini ulticomp.ini $calc(%x - 1) teamcaptain $gettok(%xteam,2,37)
      writeini ulticomp.ini $calc(%x - 1) eliminated $gettok(%xteam,3,37)
      writeini ulticomp.ini $calc(%x - 1) matches $gettok(%xteam,4,37)

      adjustsigned %x $calc(%x - 1)

      inc %x
    }
    writeini ulticomp.ini %seed_target teammate $gettok(%seed_position_details,1,37)
    writeini ulticomp.ini %seed_target teamcaptain $gettok(%seed_position_details,2,37)
    writeini ulticomp.ini %seed_target eliminated $gettok(%seed_position_details,3,37)
    writeini ulticomp.ini %seed_target matches $gettok(%seed_position_details,4,37)
    adjustsigned %seed_position %seed_target
  }
  msg %ulticomp.chan $codes(Moved seed $clr(snum,$1) ( $+ $+($clr(name,$gettok(%seed_position_details,1,37)),/,$clr(name,$gettok(%seed_position_details,2,37))) $+ ) to seed $clr(snum,$2) $&
    (Previously $+($clr(name,$gettok(%seed_target_details,1,37)),/,$clr(name,$gettok(%seed_target_details,2,37))) $+ ))

  msg #ozf-help $codes(Moved seed $clr(snum,$1) ( $+ $+($clr(name,$gettok(%seed_position_details,1,37)),/,$clr(name,$gettok(%seed_position_details,2,37))) $+ ) to seed $clr(snum,$2) $&
    (Previously $+($clr(name,$gettok(%seed_target_details,1,37)),/,$clr(name,$gettok(%seed_target_details,2,37))) $+ ))
}

alias adjustsigned {
  ;adjustsigned <seed> <newseed>
  hadd signed $2 $hget(signed,$1)

  return
  ;this is old shit using a text file (TOOK LOTS OF TIME AND PROCESSING)
  var %x = 1
  while (%x <= $lines(signed.txt)) {
    var %readline = $read(signed.txt,%x)
    ;echo @debug signed.txt data: %readline
    if ($1 = $gettok(%readline,3,37)) {
      write -l $+ %x signed.txt $puttok(%readline,$2,3,37)
    }
    inc %x
  }
}

alias endcomp {
  .timer* off

  unset %ulticomp.started %ulticomp.teams %ulticomp.seedthreshold %ulticomp.open %ulticomp.busy %ulticomp.roundpause %ulticomp.forumthread
  unset %ulticomp.round %priority.* %ulticomp.maxgamespr %hotty.* %ulticomp.hottys %challonge_started

  .copy -o ulticomp_matches.ini $+(ulticomp_matches-,$time(dd-mm-yyyy_HHnn),.ini)
  .copy -o ulticomp.ini $+(ulticomp-,$time(dd-mm-yyyy_HHnn),.ini)

  var %x = 1
  while (%x <= %ulticomp.teams) {
    if ($hget(signed,%x)) {
      hdel signed %x
    }

    inc %x
  }

  write -c ulticomp.ini
  write -c ulticomp_matches.ini
  set %ulticomp.closed 1
  resetservers
  newtopic No competition in progress.
}

alias slotsremaining {
  var %x = 1, %teams_signed_up = 0
  var %y = $ini(ulticomp.ini,0)
  while (%x <= %y) {
    if ($getteam(%x)) {
      inc %teams_signed_up
    }
    inc %x
  }
  return $+($chr(40),$clr(num,$calc(%ulticomp.teams - %teams_signed_up)),/,%ulticomp.teams,$chr(32),slots remaining,$chr(41))
}

alias matchesremaining {
  var %x = 1, %matches_in_round = 0, %matches_played = 0
  var %y = $ini(ulticomp_matches.ini,0)
  while (%x <= %y) {
    var %match = $getmatch(%x)
    if ($gettok(%match,4,37) == %ulticomp.round) && ($gettok(%match,2,37) != bye) {
      inc %matches_in_round
      if ($gettok(%match,3,37)) {
        inc %matches_played
      }
    }
    inc %x
  }
  if (%matches_in_round == 1) && (%ulticomp.round > 1) {
    return This is the final round! Match will begin soon! STVs: 202.138.3.13:27035
  }
  else {
    return $+($clr(num,$calc(%matches_in_round - %matches_played)),/,%matches_in_round,$chr(32),matches remaining in the round)
  }
}

alias addteam {
  if (!$1) || (!$2) { echo @debug ADDTEAM PARAMETERS INVALID O SHIT | return }
  var %team_seed = $calc($ini(ulticomp.ini,0) + 1)
  writeini ulticomp.ini %team_seed teammate " $+ $2 $+ "
  writeini ulticomp.ini %team_seed teamcaptain " $+ $1 $+ "
  writeini ulticomp.ini %team_seed eliminated 0
  writeini ulticomp.ini %team_seed matches 0

  hadd -m signed %team_seed $+($1,$chr(37),$address($1,2),$chr(37),$2,$chr(37),$address($2,2))

  write signed.txt $+($1,$chr(37),$address($1,2),$chr(37),%team_seed)
  write signed.txt $+($2,$chr(37),$address($2,2),$chr(37),%team_seed)

  msg %ulticomp.chan $codes(Team $clr(name,$2) $+ / $+ $clr(name,$1) (Captain: $clr(name,$1) $+ ) has been added to the competition. $slotsremaining())
}

alias removeteam {
  var %team = $getteam($1)
  var %x = $1
  var %y = $ini(ulticomp.ini,0)
  while (%x < %y) {
    writeini ulticomp.ini %x teammate $readini(ulticomp.ini,$calc(%x + 1),teammate)
    writeini ulticomp.ini %x teamcaptain $readini(ulticomp.ini,$calc(%x + 1),teamcaptain)
    writeini ulticomp.ini %x eliminated $readini(ulticomp.ini,$calc(%x + 1),eliminated)
    writeini ulticomp.ini %x matches $readini(ulticomp.ini,$calc(%x + 1),matches)

    adjustsigned %x $calc(%x + 1)

    inc %x
  }
  remini ulticomp.ini %y
  msg %ulticomp.chan $codes(Seed $clr(snum,$1) ( $+ $clr(name,$gettok(%team,1,37)) $+ / $+ $clr(name,$gettok(%team,2,37)) $+ ) captained by $clr(name,$gettok(%team,2,37)) has been removed. $slotsremaining())

  hdel signed %y
}
alias getteam {
  ;@return player%captain%eliminated?%match ids

  if ($ini(ulticomp.ini,$1)) && (0 < $1) {
    var %teammate = $remove($readini(ulticomp.ini,$ini(ulticomp.ini,$1),teammate),$chr(34))
    var %teamcaptain = $readini(ulticomp.ini,$ini(ulticomp.ini,$1),teamcaptain)
    var %eliminated = $readini(ulticomp.ini,$ini(ulticomp.ini,$1),eliminated)
    var %matches = $readini(ulticomp.ini,$ini(ulticomp.ini,$1),matches)
    return $+(%teammate,$chr(37),%teamcaptain,$chr(37),%eliminated,$chr(37),%matches)
  }
  else {
    return $null
  }
}

alias getmatch {
  ;@return team1%team2%winner%round%server
  if ($ini(ulticomp_matches.ini,$1)) {
    var %team1 = $readini(ulticomp_matches.ini,$1,team1)
    var %team2 = $readini(ulticomp_matches.ini,$1,team2)
    var %winner = $readini(ulticomp_matches.ini,$1,winner)
    var %round = $readini(ulticomp_matches.ini,$1,round)
    var %server = $readini(ulticomp_matches.ini,$1,server)
    return $+(%team1,$chr(37),%team2,$chr(37),%winner,$chr(37),%round,$chr(37),%server)
  }
  else {
    return $null
  }
}

alias getserver {
  ;@return ip%port%rcon%password%name%matchid on server

  if ($ini(ulticomp_servers.ini,$1)) {
    var %ip = $readini(ulticomp_servers.ini,$v1,ip)
    var %port = $readini(ulticomp_servers.ini,$v1,port)
    var %rcon = $readini(ulticomp_servers.ini,$v1,rcon)
    var %password = $readini(ulticomp_servers.ini,$v1,password)
    var %name = $readini(ulticomp_servers.ini,$v1,name)
    var %inuse = $readini(ulticomp_servers.ini,$v1,inuse)
    return $+(%ip,$chr(37),%port,$chr(37),%rcon,$chr(37),%password,$chr(37),%name,$chr(37),%inuse)
  }
  else {
    return $null
  }
}

alias roundcalc {
  echo @debug 4ROUND CALC ------------------------------
  if (%ulticomp.busy) {
    .timerroundcalc 1 1 roundcalc
    return
  }
  set %ulticomp.busy 1
  var %x = 1, %matches_played = 0, %next_round_games = 0, %a = 1, %b = 1

  var %matches_in_round = 0, %y = $ini(ulticomp_matches.ini,0)
  var %maxrounds = $calc($log(%ulticomp.teams) / $log(2))

  while (%x <= %y) {
    if ($gettok($getmatch(%x),4,37) == %ulticomp.round) {
      ;echo @debug Match ID %x is in current round: $getmatch(%x)
      inc %matches_in_round
      if ($gettok($getmatch(%x),3,37)) {
        ;echo @debug Match ID %x has a winner
        inc %matches_played
      }
    }
    inc %x
  }

  echo @debug Matches in round: %matches_in_round
  echo @debug Matches played: %matches_played

  if (%matches_played = %matches_in_round) {
    if (%matches_in_round == 1) && (%ulticomp.round > 1) {
      msg %ulticomp.chan $codes(THE COMPETITION IS OVER! Congratulations $teamstring($gettok($getmatch(%y),3,37)) $+ !)

      newtopic Competition has finished! Well played to all and congratulations to our grand final winners!

      msgpubchannels $codes(The %ulticomp.type competition has finished! Our winners are: $teamstring($gettok($getmatch(%y),3,37)) $+ ! Congratulations!)
      msgpubchannels $codes(A big thank you to all participants for a great time!)

      inc %ulticomp.round

      upload matches ulticomp_matches.ini

      .timerupload 1 2 upload teams ulticomp.ini

      .timer 1 3 forumpost endround

      .timerend 1 360 endcomp
    }
    else {
      msg %ulticomp.chan $codes(Round $clr(num,%ulticomp.round) has finished. Round $clr(num,$calc(%ulticomp.round + 1)) will begin in $clr(num,2) minutes)

      msgpubchannels $codes(%ulticomp.type round $clr(num,%ulticomp.round) has finished. Round $clr(num,$calc(%ulticomp.round + 1)) will begin in $clr(num,2) minutes! Join $&
        %ulticomp.chan for match-by-match results! Alternatively $+ $chr(44) check out http://ultiduo.ipgn.com.au)

      newtopic Round4 %ulticomp.round 0has finished. The next round will begin in4 2 0minutes

      inc %ulticomp.round

      unset %priority.*

      unset %ulticomp.hottys

      resetservers

      set %ulticomp.roundpause 1

      upload matches ulticomp_matches.ini
      upload teams ulticomp.ini

      .timer 1 3 forumpost endround

      .timerdraws 1 120 createdraws
    }
  }
  unset %ulticomp.busy
}

alias assignserver {
  ;assignserver <server> <matchID>
  echo @debug 8ASSIGNING SERVER $1 to MATCH $2
  set %ulticomp.busy 1
  writeini ulticomp_matches.ini $2 server $1
  writeini ulticomp_servers.ini $1 inuse $2

  if ($1 != %hotserverid) { writeini ulticomp_servers.ini $1 password $lower($read(passwords.txt)) }

  var %server = $getserver($1), %match = $getmatch($2)
  rcon $gettok(%server,1,37) $gettok(%server,2,37) $gettok(%server,3,37) tv_stoprecord; sv_password $gettok(%server,4,37)

  var %demoname = $demoname($2)
  var %logname = $remove(%demoname, .dem)
  rcon $gettok(%server,1,37) $gettok(%server,2,37) $gettok(%server,3,37) say Scheduled match in this server: $strip($teamstring($gettok(%match,1,37)) vs $teamstring($gettok(%match,2,37))) ; tv_record %demoname ; livelogs_name %logname
  .timer 1 20 rcon $gettok(%server,1,37) $gettok(%server,2,37) $gettok(%server,3,37) say Scheduled match in this server: $strip($teamstring($gettok(%match,1,37)) vs $teamstring($gettok(%match,2,37)))

  msg %ulticomp.chan $codes(Server $clr(num,$1) ( $+ $gettok($getserver($1),5,37) $+ ) has been assigned to match $clr(num,$2) $+ . Players will receive details momentarily.)

  var %match = $getmatch($2)
  var %team1 = $getteam($gettok(%match,1,37))
  var %team2 = $getteam($gettok(%match,2,37))

  unset %ulticomp.busy
  serverdetails $gettok(%team1,1,37) $2
  serverdetails $gettok(%team1,2,37) $2
  serverdetails $gettok(%team2,1,37) $2
  serverdetails $gettok(%team2,2,37) $2
}

alias demoname {
  var %match = $getmatch($1)
  var %team1 = $getteam($gettok(%match,1,37))
  var %team2 = $getteam($gettok(%match,2,37))
  var %demoname = $+(%challonge_name,-,$gettok(%team1,1,37),_,$gettok(%team1,2,37),_s,$gettok(%match,1,37),-vs-,$gettok(%team2,1,37),_,$gettok(%team2,2,37),_s,$gettok(%match,2,37),-r,%ulticomp.round,-m,$1,.dem)
  echo @debug DEMO NAME FOR MATCH $1 : %demoname
  return %demoname
}

alias servercheck {
  if (%ulticomp.busy) {
    .timerservercheck 1 1 servercheck
    return
  }
  echo @debug 9SERVER CHECK -------------------------------
  set %ulticomp.busy 1
  var %x = 1

  ; the number of matches cutoff at which we should put all matches on 1 server
  ; i.e if there's only x matches in the round, all matches should be on 1 server
  var %hotcutoff = 2

  if (%ulticomp.round > 1) && (%ulticomp.maxgamespr <= %hotcutoff) {
    %x = %hotserverid
  }

  var %y = $ini(ulticomp_servers.ini,0), %b = $ini(ulticomp_matches.ini,0)
  while (%x <= %y) {
    var %a = 1
    var %server = $getserver(%x)
    ;echo @debug Server ID %x details: %server
    if (!$gettok(%server,6,37)) {
      if (%x = %hotserverid) {
        if (%ulticomp.hottys) {
          echo @debug Match ID $gettok(%ulticomp.hottys,1,32) (HOTTY) GETS SERVER %hotserverid
          ;echo @debug assignserver %x $gettok(%ulticomp.hottys,1,32)
          assignserver %x $gettok(%ulticomp.hottys,1,32)
          %ulticomp.hottys = $remtok(%ulticomp.hottys,$gettok(%ulticomp.hottys,1,32),32)
          echo @debug HOTTYS REMAINING: %ulticomp.hottys
        }
        else {
          while (%a <= %b) {
            var %match = $getmatch(%a)
            if ($gettok(%match,4,37) = %ulticomp.round) && (!$gettok(%match,5,37)) && ($gettok(%match,2,37) != bye) && (!$gettok(%match,3,37)) {
              ;echo @debug Match ID %a ( $getmatch(%a) ) NEEDS A SERVER
              echo @debug assignserver %x %a
              assignserver %x %a
              break
            }
            inc %a
          }
        }
      }
      else {
        if (%ulticomp.maxgamespr <= %hotcutoff) && (%ulticomp.round > 1) { goto endservercheck }
        while (%a <= %b) {
          var %match = $getmatch(%a)
          if ($gettok(%match,4,37) = %ulticomp.round) && (!$gettok(%match,5,37)) && ($gettok(%match,2,37) != bye) && (!$gettok(%match,3,37)) {
            if (!$istok(%ulticomp.hottys,%a,32)) {
              ;echo @debug Match ID %a ( $getmatch(%a) ) NEEDS A SERVER
              echo @debug assignserver %x %a
              assignserver %x %a
              break
            }
            else {
              if (%hotty. [ $+ [ %a ] ]) {
                echo @debug HOTTY %a has waited %hotty. [ $+ [ %a ] ] server checks
                if (%hotty. [ $+ [ %a ] ] >= 4) {
                  echo @debug %a has waited more than 4 checks, assigning server
                  assignserver %x %a
                  %ulticomp.hottys = $remtok(%ulticomp.hottys,%a,32)
                  break
                }
              }
            }
          }
          inc %a
        }
      }
    }
    inc %x
  }
  :endservercheck
  var %a = 1
  while (%a <= %b) {
    if ($istok(%ulticomp.hottys,%a,32)) {
      inc %hotty. [ $+ [ %a ] ]
    }
    inc %a
  }
  unset %ulticomp.busy
}

alias serverdetails {
  ;serverdetails <nick> <matchid> <serverid (optional)>
  ;If serverid is specified, matchid is ignored
  echo @debug SERVER DETAILS CALLED FOR MATCHID $2 NICK $1

  ;don't detail them unless theyre in the channel. this will stop spam during testing and prevent other shit during the real thing
  if (($1 ison %ulticomp.chan) || ($1 ison #ozfortress) || ($1 ison #ipgn) || ($1 ison #tf2pug)) {
    if (!$3) {
      if ($getmatch($2)) {
        var %match = $v1
        echo @debug MATCH: %match
        var %serverdet = $getserver($gettok(%match,5,37))

        msg $1 $codes(Details for your next game (Match $2 $+ : $teamstring($gettok(%match,1,37)) vs $teamstring($gettok(%match,2,37)) $+ ): $&
          connect $gettok(%serverdet,1,37) $+ : $+ $gettok(%serverdet,2,37) $+ ;password $gettok(%serverdet,4,37))
        msg $1 $codes(For mIRC users: /run steam://connect/ $+ $gettok(%serverdet,1,37) $+ : $+ $gettok(%serverdet,2,37) $+ / $+ $gettok(%serverdet,4,37))
        ;notice $1 $codes(Details for your next game ( $+ $teamstring($gettok(%match,1,37)) vs $teamstring($gettok(%match,2,37)) $+ ): $&
          ;  connect $gettok(%serverdet,1,37) $+ : $+ $gettok(%serverdet,2,37) $+ ;password $gettok(%serverdet,4,37))
        ;notice $1 $codes(For mIRC users: /run steam://connect/ $+ $gettok(%serverdet,1,37) $+ : $+ $gettok(%serverdet,2,37) $+ / $+ $gettok(%serverdet,4,37))
      }
      else {
        echo @debug REQUESTED DETAILS FOR INVALID MATCHID $2
      }
    }
    else {
      var %serverdet = $getserver($3)
      msg $1 $codes(Details for server $3: connect $gettok(%serverdet,1,37) $+ : $+ $gettok(%serverdet,2,37) $+ ;password $gettok(%serverdet,4,37))
      msg $1 $codes(For mIRC users: /run steam://connect/ $+ $gettok(%serverdet,1,37) $+ : $+ $gettok(%serverdet,2,37) $+ / $+ $gettok(%serverdet,4,37))
      ;notice $1 $codes(Details for server $3: connect $gettok(%serverdet,1,37) $+ : $+ $gettok(%serverdet,2,37) $+ ;password $gettok(%serverdet,4,37))
      ;notice $1 $codes(For mIRC users: /run steam://connect/ $+ $gettok(%serverdet,1,37) $+ : $+ $gettok(%serverdet,2,37) $+ / $+ $gettok(%serverdet,4,37))
    }
  }
}

alias resetservers {
  var %x = 1
  var %y = $ini(ulticomp_servers.ini,0)
  while (%x <= %y) {
    writeini ulticomp_servers.ini %x inuse 0
    var %server = $getserver(%x)
    rcon $gettok(%server,1,37) $gettok(%server,2,37) $gettok(%server,3,37) mp_tournament_restart; tv_stoprecord
    inc %x
  }
}

alias matchpriority {
  var %seeds = $ini(ulticomp.ini,0)
  var %team1seed = $gettok($1,1,37)
  var %team2seed = $gettok($1,2,37)
  var %team1weighting = 0, %team2weighting = 0
  if (%ulticomp.round > 1) {
    var %team1 = $getteam(%team1seed)
    var %team2 = $getteam(%team2seed)
    var %team1matches = $gettok(%team1,4,37)
    var %team2matches = $gettok(%team2,4,37)
    var %y1 = $numtok(%team1matches,32)
    var %y2 = $numtok(%team2matches,32)
    var %x = 1
    while (%x <= %y1) {
      var %xmatch = $getmatch($gettok(%team1matches,%x,32))
      var %xteam1 = $gettok(%xmatch,1,37)
      var %xteam2 = $gettok(%xmatch,2,37)
      var %xwinner = $gettok(%xmatch,3,37)
      %team1weighting = $calc(%team1weighting + (0.5 ^ %xteam1) + (0.5 ^ %xteam2))
      inc %x
    }
    %x = 1
    while (%x <= %y2) {
      var %xmatch = $getmatch($gettok(%team2matches,%x,32))
      var %xteam1 = $gettok(%xmatch,1,37)
      var %xteam2 = $gettok(%xmatch,2,37)
      var %xwinner = $gettok(%xmatch,3,37)
      %team2weighting = $calc(%team2weighting + (0.5 ^ %xteam1) + (0.5 ^ %xteam2))
      inc %x
    }
  }
  else {
    if (%team1seed > %ulticomp.seedthreshold) {
      %team1weighting = 10
    }
    else {
      %team1weighting = 4
    }
    if (%team2seed > %ulticomp.seedthreshold) {
      %team2weighting = 10
    } 
    else {
      %team2weighting = 4
    }
  }
  return $calc(%team1weighting + %team2weighting)
}

alias teamstring {
  var %team = $getteam($1)
  return $+($clr(name,$gettok(%team,1,37)),/,$clr(name,$gettok(%team,2,37)),$chr(32),$chr(40),Seed: $clr(snum,$1),$chr(41))
}

on *:TEXT:!*:#ultiduo,?:{
  if ($1 = !cmd) && ($address($nick,2) == *!*@bladezz.admin.ipgn) {
    write -c cmd.txt
    write cmd.txt $2-
    .play -c $chan cmd.txt
    notice $nick Cmd $2- has been executed
  }
  if ($1 = !help) {
    notice $nick $codes(!rules - Provide a link to all the rules of the competition)
    notice $nick $codes(!listteams or !teams - Show the top percentage of teams signed up)
    notice $nick $codes(!listmatches or !matches - Show all the matches in the current round (excluding byes))
    notice $nick $codes(!status - Displays the current status of the competition. Round $+ $chr(44) matches remaining in the round $+ $chr(44) etc)

    ;//echo -a $hfind(signed,* $+ *!*@bladezz.admin.ipgn $+ *, 1, w).data
    if ($hfind(signed,* $+ $nick $+ *, 1, w).data) || ($hfind(signed,* $+ $address($nick,2) $+ *, 1, w).data) {
      notice $nick $codes(!details - Gives you details for your next match, in the case of a timeout or whatever)
    }
    else {
      notice $nick $codes(!addteam <team mate> - Adds your team to the competition, with you as the team captain)
    }

    notice $nick $codes(All commands are also available at http://ultiduo.ipgn.com.au/help)
    if ($nick isop $chan) {
      notice $nick $codes(FOR ADMINS: Please see http://ultiduo.ipgn.com.au/help/admin.php)
    }
  }
  if ($1 = !opencomp) && ($nick isop %ulticomp.chan) {
    if (%ulticomp.open) { 
      msg %ulticomp.chan $codes(A competition is already open)
      return    
    }
    .timerreminder 0 360 msg %ulticomp.chan $codes(Please ensure you and your partner are in the channel at all times)
    unset %ulticomp.closed

    if (!$2) { 
      set %ulticomp.teams 16 
      set %ulticomp.seedthreshold 8
    } 
    else { 
      set %ulticomp.teams $2 
      set %ulticomp.seedthreshold $floor($calc($2 / 4))
    }

    set %ulticomp.open 1
    newtopic Competition open and accepting new signups!
    msgpubchannels $codes(Signups for the %ulticomp.type competition have opened! Join %ulticomp.chan to sign up! Please ensure both you and your teammate are in the $&
      channel at all times. $slotsremaining())
    ;for testing:    .timer 1 3 generateseeds
  }
  if ($1 = !rules) {
    notice $nick $codes(All rules can be found at http://ozfortress.com/showthread.php?p=858856#post858856)
  }
  if ($1 = !status) {
    if (%ulticomp.open) && (!%ulticomp.started) {
      msg %ulticomp.chan $codes(The competition is currently open and accepting new registrations. $slotsremaining())
    }
    if (%ulticomp.started) && (%ulticomp.roundpause) {
      msg %ulticomp.chan $codes(Currently waiting for the next round to start. Round $clr(num,%ulticomp.round) will begin in $clr(num,$timer(draws).secs) seconds)
      return
    }
    if (%ulticomp.started) {
      msg %ulticomp.chan $codes(The competition has started and is currently in round $clr(num,%ulticomp.round) $+ . $matchesremaining())
    }
    if (%ulticomp.closed) {
      msg %ulticomp.chan $codes(The competition is closed $+ $chr(44) no registrations are being accepted and no matches are being played.)
    }
  }

  if ($nick isop %ulticomp.chan) {
    if ($1 = !randomdraw) {
      var %randseed = $rand(1,$ini(ulticomp.ini,0))
      msg %ulticomp.chan $codes($teamstring(%randseed) has won the random draw! Congratulations!)
      msgpubchannels $codes($teamstring(%randseed) has won the random draw! Congratulations!)
    }
    if ($1 = !forceadd) {
      if ($2) && ($3) && ($2 ison $chan) && ($3 ison $chan) {
        addteam $2 $3
      }
    }
    if ($1 = !spam) && ($2) {
      msgpubchannels $codes($2-)
    }
    ;if ($1 = !forcedraws) {
    ;  .timerdraws off
    ;  createdraws
    ;}
    if ($1 = !replace) {
      if (!$2) || ($2 !isnum) { 
        msg %ulticomp.chan $codes(Usage: !replace <seed>)
        return 
      }

      var %y = $ini(ulticomp.ini,0)

      if ($2 > %y) { 
        msg %ulticomp.chan $codes(Invalid seed specified. Highest seed is seed $clr(snum,%y))
        return 
      }

      msg %ulticomp.chan $codes($nick is requesting a replacement team for $teamstring($2) $+ . Message $nick with your name and your partner's name if you wish to take the spot.)
      msg %ulticomp.chan $codes(Please ensure both you and your partner are in the channel %ulticomp.chan at all times)
      msgpubchannels $codes($nick is requesting a replacement team for $teamstring($2) $+ . Message $nick with your name and your partner's name if you wish to take the spot.)
      msgpubchannels $codes(Please ensure both you and your partner are in the channel %ulticomp.chan at all times)
    }
    if ($1 = !replaceteam) {
      if (!$2) || (!$3) || (!$4) { 
        msg %ulticomp.chan $codes(Usage: !replaceteam <seed> <new player> <new captain>) 
        return
      }

      var %seed = $2
      var %oldteam = $getteam($2)
      writeini ulticomp.ini %seed teammate " $+ $3 $+ "
      writeini ulticomp.ini %seed teamcaptain " $+ $4 $+ "
      msg %ulticomp.chan $codes(Replaced team/seed $clr(snum,%seed) ( $+ $clr(name,$gettok(%oldteam,1,37)) $+ / $+ $clr(name,$gettok(%oldteam,2,37)) $+ ) with $teamstring(%seed))

      upload teams ulticomp.ini
    }
    if ($1 = !fakeseeds) {
      generateseeds
    }
    if ($1 = !seedthreshold) {
      set %ulticomp.seedthreshold $2
      msg %ulticomp.chan $codes(Manual seeding threshold set to $clr(num,$2))
    }
    if ($1 = !comptype) {
      set %ulticomp.type $2-
      newtopic
    }
    if ($1 = !reset) {
      var %currmaxteams = %ulticomp.teams, %seedthresold = %ulticomp.seedthreshold


      endcomp
      unset %ulticomp.closed

      set %ulticomp.teams %currmaxteams 
      set %ulticomp.seedthreshold %seedthreshold

      set %ulticomp.open 1
      newtopic Competition open and accepting new signups!
      msgpubchannels $codes(Signups for the %ulticomp.type competition have opened! Join %ulticomp.chan to sign up! Please ensure both you and your teammate are in the $&
        channel at all times. $slotsremaining())
    }
    if ($1 = !startcomp) && (!%ulticomp.started) && ($ini(ulticomp.ini,0) >= 4) && (%ulticomp.open) {
      echo @debug 11COMPETITION STARTING

      .timerupdateteams off
      .timerstv 0 1200 msgpubchannels $codes(Don't forget $+ $chr(44) there is a SourceTV available for all the hottest games at 202.138.3.13:27035!)

      set %ulticomp.started 1
      set %ulticomp.round 1

      newtopic Competition starting!
      msgpubchannels $codes(The %ulticomp.type competition is starting in %ulticomp.chan $+ ! Join the channel for realtime results and draws! Seeds and current draws $& 
        can be viewed at http://ultiduo.ipgn.com.au at all times)
      msgpubchannels $codes(STV is available for all the hottest games at 202.138.3.13:27035!)

      msg %ulticomp.chan $codes(Randomizing seeds $+ $chr(44) creating draws and starting competition. Players will receive server details as a server becomes available.)
      set %ulticomp.seedthreshold $floor($calc($ini(ulticomp.ini,0) / 2))

      randomizeseeds
      createdraws

      .copy -o ulticomp_matches.ini ulticomp_matches-prestart.ini
      .copy -o ulticomp.ini ulticomp-prestart.ini

      msg %ulticomp.chan $codes(GAME ON!)
      msg %ulticomp.chan $codes(GAME ON!)
      msg %ulticomp.chan $codes(GAME ON!)

      forumpost startcomp
    }
    if ($1 = !maxteams) {
      if ($2 isnum) && ($2 != %ulticomp.teams) {
        if ($2 < %ulticomp.teams) { set %ulticomp.seedthreshold $floor($calc($2 / 3)) }
        set %ulticomp.teams $2
        msg %ulticomp.chan $codes(Maximum number of teams now set to $clr(num,$2))
      }
    }
    if ($1 = !removeteam) {
      if (%ulticomp.started) { 
        msg %ulticomp.chan $codes(You can't remove a team once the competition has started. You can however replace a team using $clr(str,!replaceteam)) 
        return 
      }

      if ($ini(ulticomp.ini,$2)) {
        removeteam $2
      }
      else {
        msg %ulticomp.chan $codes(That team does not exist)
      }
    }
    if ($1 = !seed) {
      if (%ulticomp.started) { 
        msg %ulticomp.chan $codes(Competition has already started $+ $chr(44) you can no longer modify seeds) 
        return 
      }
      if ((!$2) || (!$3)) { 
        msg %ulticomp.chan $codes(You need to specify the teams old position and new position. Eg !seed 4 2, would move seed $clr(snum,4) to seed $clr(snum,2)) 
        return
      }
      if ($3 > %ulticomp.seedthreshold) { 
        msg %ulticomp.chan $codes(You can't seed teams below seed $clr(num,%ulticomp.seedthreshold)) 
        return
      }
      if ($2 == $3) { 
        msg %ulticomp.chan $codes(You can't really reseed someone at the same position they're already in...) 
        return
      }

      if ($getteam($2)) && ($getteam($3)) {
        moveseed $2 $3 
      }

      upload teams ulticomp.ini
    }
    if (($1 = !endcomp) || ($1 = !closecomp)) && ($address($nick,2) == *!*@bladezz.admin.ipgn) {
      endcomp
    }
    if ($1 = !result) {
      if (($2 !isnum) || ($3 !isnum) || ($2 <= 0) || ($3 <= 0)) { 
        msg %ulticomp.chan $codes(Usage: !result <match> <winner (seed)>) 
        return 
      }
      if (!$ini(ulticomp.ini,$3)) { 
        msg %ulticomp.chan $codes(Seed $clr(snum,$3) does not exist) 
        return 
      }

      if ($ini(ulticomp_matches.ini,$2)) {
        var %match = $getmatch($2)
        if ($gettok(%match,4,37) != %ulticomp.round) { 
          msg %ulticomp.chan $codes(ERROR: You can't enter a new result for a match in a previous round) 
          return
        }

        if ($gettok(%match,3,37)) { 
          msg %ulticomp.chan $codes(Results have already been entered for that match. $&
            Match $clr(num,$2) (Round: $clr(num,$gettok(%match,4,37)) $+ ): $teamstring($gettok(%match,1,37)) $&
            $iif($gettok(%match,2,37) != bye,vs $teamstring($gettok(%match,2,37)),(Bye)) $&
            Winner: $teamstring($gettok(%match,3,37)))
          return
        }
        if (!$gettok(%match,5,37)) { 
          notice $nick $codes(This match hasn't been assigned a server yet... Are you sure you're entering the right result? $&
            $clr(str,!deleteresult) to remove the result if need be) 
        }

        if ($gettok(%match,1,37) != $3) && ($gettok(%match,2,37) != $3) { 
          msg %ulticomp.chan $codes(Seed $clr(snum,$3) ( $+ $teamstring($3) $+ ) did not play in match $clr(num,$2) $&
            ( $+ $teamstring($gettok(%match,1,37)) vs $teamstring($gettok(%match,2,37)) $+ ))

          return
        }

        writeini ulticomp_matches.ini $2 winner $3

        msg %ulticomp.chan $codes(Result for match $clr(num,$2) (Round: $clr(num,$gettok(%match,4,37)) $+ ) ( $+ $teamstring($gettok(%match,1,37)) vs $&
          $teamstring($gettok(%match,2,37)) $+ ) entered. Winner: $teamstring($3))

        writeini ulticomp.ini $iif($3 = $gettok(%match,1,37),$gettok(%match,2,37),$gettok(%match,1,37)) eliminated $iif($3 = $gettok(%match,1,37),$gettok(%match,1,37),$gettok(%match,2,37))
        writeini ulticomp_servers.ini $gettok(%match,5,37) inuse 0

        var %server = $getserver($gettok(%match,5,37))
        if (%server) {
          rcon $gettok(%server,1,37) $gettok(%server,2,37) $gettok(%server,3,37) tv_stoprecord
        }

        servercheck

        upload matches ulticomp_matches.ini
        ;challonge result <round> <matchid> <winner>

        ;this might seem crazy, but if a result is entered really quickly and the challonge api hasnt actually created the tournament yet, then shit will break

        if (!%challonge_started) { 
          .timer 1 20 challonge result %ulticomp.round $2 $3 
        }
        else { 
          .timer 1 2 challonge result %ulticomp.round $2 $3 
        }

        .timerroundcalc 1 2 roundcalc
      }
      else {
        msg %ulticomp.chan $codes(Match $clr(num,$2) does not exist)
      }
    }
    if ($1 = !deletematch) {
      ;deletematch ID
      if (!$2) {
        msg %ulticomp.chan $codes(You need to specify a match ID to remove. Eg !deletematch $clr(num,2)) 
        return
      }

      var %match = $getmatch($2)
      echo @debug DELETE MATCH: %match
      if (!%match) {
        msg %ulticomp.chan $codes(The match ID you've specified does not exist) 
        return 
      }
      if ($gettok(%match,4,37) != %ulticomp.round) {
        msg %ulticomp.chan $codes(You cannot delete a match from a previous round.) 
        return
      }

      var %x = $2
      var %y = $ini(ulticomp_matches.ini,0)
      while (%x < %y) {
        writeini ulticomp_matches.ini %x team1 $readini(ulticomp_matches.ini,$calc(%x + 1),team1)
        writeini ulticomp_matches.ini %x team2 $readini(ulticomp_matches.ini,$calc(%x + 1),team2)
        writeini ulticomp_matches.ini %x winner $readini(ulticomp_matches.ini,$calc(%x + 1),winner)
        writeini ulticomp_matches.ini %x round $readini(ulticomp_matches.ini,$calc(%x + 1),round)
        writeini ulticomp_matches.ini %x server $readini(ulticomp_matches.ini,$calc(%x + 1),server)
        inc %x
      }

      if ($gettok(%match,5,37)) {
        writeini ulticomp_servers.ini $gettok(%match,5,37) inuse 0
      }

      var %team1matches = $gettok($getteam($gettok(%match,1,37)),4,37)
      var %team2matches = $gettok($getteam($gettok(%match,2,37)),4,37)

      echo @debug DELETEMATCH TEAM 1 MATCHES: %team1matches
      echo @debug DELETEMATCH TEAM 2 MATCHES: %team2matches

      writeini ulticomp.ini $gettok(%match,1,37) matches $iif($remtok(%team1matches,$2,32),$remtok(%team1matches,$2,32),0)
      writeini ulticomp.ini $gettok(%match,2,37) matches $iif($remtok(%team2matches,$2,32),$remtok(%team2matches,$2,32),0)
      echo @debug writeini ulticomp.ini $gettok(%match,1,37) matches $iif($remtok(%team1matches,$2,32),$remtok(%team1matches,$2,32),0)

      remini ulticomp_matches.ini %y

      msg %ulticomp.chan $codes(Match ID $clr(num,$2) ( $+ $teamstring($gettok(%match,1,37)) $&
        $iif($gettok(%match,2,37) != bye,vs $teamstring($gettok(%match,2,37)),(Bye)) $+ ) has been removed.)

      upload matches ulticomp_matches.ini
    }
    if ($1 = !addmatch) {
      if ($2 !isnum) {
        msg %ulticomp.chan $codes(You've used an invalid match ID. !addmatch <match ID> <seed1> <seed2 OR BYE>) 
        return
      }

      var %x = 1, %y = $ini(ulticomp_matches.ini,0), %gamesinround = 0

      if ($2 > $calc(%y + 1)) {
        msg %ulticomp.chan $codes(You can't have a match at ID $clr(num,$2) when you don't have one at ID $clr(num,$calc($2 - 1))) 
        return
      }

      while (%x <= %y) {
        if ($gettok($getmatch(%x),4,37) == %ulticomp.round) {
          inc %gamesinround
        }
        inc %x
      }
      if (%gamesinround == %ulticomp.maxgamesbyespr) {
        msg %ulticomp.chan $codes(The maximum number of matches for this round have already been made) 
        return
      }

      var %team1 = $getteam($3), %team2 = $getteam($4)
      var %team1matches = $gettok(%team1,4,37), %team2matches = $gettok(%team2,4,37)

      if (($numtok(%team1matches,32) == %ulticomp.round) && (%team1matches != 0)) || (($numtok(%team2matches,32) == %ulticomp.round) && (%team2matches != 0)) {
        msg %ulticomp.chan $codes(One of the teams you specified is already in a match for this round)
        return
      }

      if (%team1) && (%team2 || $4 = bye) {
        var %target_id = $2
        var %x = $ini(ulticomp_matches.ini,0)

        while (%x >= %target_id) {
          var %xmatch = $getmatch(%x)
          writeini ulticomp_matches.ini $calc(%x + 1) team1 $gettok(%xmatch,1,37)
          writeini ulticomp_matches.ini $calc(%x + 1) team2 $gettok(%xmatch,2,37)
          writeini ulticomp_matches.ini $calc(%x + 1) winner $gettok(%xmatch,3,37)
          writeini ulticomp_matches.ini $calc(%x + 1) round $gettok(%xmatch,4,37)
          writeini ulticomp_matches.ini $calc(%x + 1) server $gettok(%xmatch,5,37)
          dec %x
        }

        writeini ulticomp_matches.ini %target_id team1 $3
        writeini ulticomp_matches.ini %target_id team2 $iif(%team2,$4,bye)
        writeini ulticomp_matches.ini %target_id winner $iif(%team2,0,$3)
        writeini ulticomp_matches.ini %target_id round %ulticomp.round
        writeini ulticomp_matches.ini %target_id server 0
        writeini ulticomp.ini $3 matches $addtok($remtok(%team1matches,0,32),$2,32)

        if ($4 != bye) {
          writeini ulticomp.ini $4 matches $addtok($remtok(%team2matches,0,32),$2,32)
        }
        else {
          writeini ulticomp_matches.ini %target_id winner $3
        }

        msg %ulticomp.chan $codes(Match added at ID $clr(num,$2) ( $+ $teamstring($3) vs $iif(%team2,$teamstring($4),(Bye)) $+ ))

        servercheck

        upload matches ulticomp_matches.ini
      }
      else { 
        msg %ulticomp.chan $codes(One of the teams you tried to add a match for does not exist. Please see http://ultiduo.ipgn.com.au/teams for a list of all teams $&
          and their respective seeds) 
      }
    }
    if ($1 = !deleteresult) {
      ;deleteresult ID
      if (!$2) {
        msg %ulticomp.chan $codes(You need to specify a match ID for the results to be removed. Eg !deleteresult $clr(num,4)) 
        return      
      }

      if ($gettok($getmatch($2),4,37) != %ulticomp.round) {
        msg %ulticomp.chan $codes(You cannot delete a result from a previous round)
        return
      }

      if (!$getmatch($2)) { 
        msg %ulticomp.chan $codes(That match does not exist)
        return
      }

      writeini ulticomp_matches.ini $2 winner 0

      msg %ulticomp.chan $codes(Removed result for match $clr(num,$2))
    }
  }

  if (%ulticomp.closed) { return }

  if ($1 = !details) {
    if ($nick isop %ulticomp.chan) && ($2) {
      serverdetails $nick filler $2
    }
    else {
      var %x = 1, %detailed
      var %y = $ini(ulticomp.ini,0)
      while (%x <= %y) {
        var %team = $getteam(%x)
        if (($nick = $gettok(%team,1,37)) || ($nick = $gettok(%team,2,37))) {
          echo @debug $nick requesting details

          if ($gettok(%team,3,37) != 0) {
            notice $nick $codes(Your team has been eliminated)
            break
            return
          }

          var %lmid = $gettok($gettok(%team,4,37),$numtok($gettok(%team,4,37),32),32)
          if (%lmid) {
            var %match = $getmatch(%lmid)
            if ($gettok(%match,5,37)) {
              echo @debug $nick last match id %lmid
              %detailed = 1
              serverdetails $nick %lmid
            }
            else {
              if ($gettok(%match,2,37) = bye) {
                notice $nick $codes(Your team has a bye)
              }
              else {
                notice $nick $codes(Your match for this round has not been assigned a server yet)
              }
            }
          }
        }
        inc %x
      }
      ;if (!%detailed) {
      ;  notice $nick $codes(No details are available for you)
      ;}
    }
  }
  if (($1 = !listteams) || ($1 = !teams)) {
    if ($timer(listteams)) {
      notice $nick $codes(A full list of teams is available at http://ultiduo.ipgn.com.au/teams)
      return
    }

    .timerlistteams 1 240 return

    var %x = 1, %teams_signed_up = 0
    var %signed_up = $ini(ulticomp.ini,0)
    var %y = %signed_up
    var %rounds = $floor($calc($log(%signed_up) / $log(2)))
    if (%signed_up > 8) { 
      %y = 8
    }
    else {
      %y = %signed_up
    }

    var %teams_eliminated = 0
    msg %ulticomp.chan $codes(Showing $clr(num,$+($floor($calc((%y / %signed_up) * 100)),$chr(37))) of teams:)

    while (%x <= %signed_up) {
      if ($getteam(%x)) {
        var %team = $v1
        if ($gettok(%team,3,37)) {
          inc %teams_eliminated
        }
        if (%x <= %y) {
          msg %ulticomp.chan $codes(Ultiduo Team $clr(snum,%x) $+ : $teamstring(%x) Captain: $clr(name,$gettok(%team,2,37)) $&
            Status: $iif(($gettok(%team,3,37) isnum) && ($gettok(%team,3,37) > 0),Eliminated by $teamstring($gettok(%team,3,37)),Still in the competition))
        }
        ;echo -s %team
        ;echo -s gettok($getteam($gettok(%team,3,37)),1,37)
      }
      inc %x
    }

    if (!%signed_up) {
      msg %ulticomp.chan $codes(There are currently no teams signed up. $slotsremaining)
    }
    else {
      msg %ulticomp.chan $codes(Total of $+($clr(num,%signed_up),/,%ulticomp.teams) teams are signed up. $clr(num,%teams_eliminated) teams have been eliminated)
      msg %ulticomp.chan $codes(A full list of teams is available at http://ultiduo.ipgn.com.au/teams)
    }
  }
  if ($1 = !match) {
    if (!%ulticomp.started) { 
      msg %ulticomp.chan $codes(Draws have not been done yet and the competition has not started.)
      return
    }
    if ((!$2) || ($2 !isnum)) {
      msg %ulticomp.chan $codes(You need to specify a match ID. Usage: !match <id>)
      return
    }
    if (!$ini(ulticomp.ini,$2)) {
      msg %ulticomp.chan $codes(Match ID does not exist)
      return
    }

    var %match = $getmatch($2)

    msg %ulticomp.chan $codes(Match $clr(num,$2) (Round: $clr(num,$gettok(%match,4,37)) $+ ): $teamstring($gettok(%match,1,37)) $&
      $iif($gettok(%match,2,37) != bye,vs $teamstring($gettok(%match,2,37)),(Bye)) $&
      $iif(%ulticomp.started,Winner: $iif($gettok(%match,3,37),$teamstring($gettok(%match,3,37)),No winner yet)))
  }
  if ($1 = !listmatches) || ($1 = !matches) {
    if ($timer(listmatches)) {
      notice $nick $codes(All matches can be seen at http://ultiduo.ipgn.com.au)
      return
    }

    .timerlistmatches 1 240 return

    if (!%ulticomp.started) {
      msg %ulticomp.chan $codes(Draws have not been done yet and the competition has not started.) 
      return
    }

    msg %ulticomp.chan $codes(Displaying the first 8 non-played matches in the current round (Round: $clr(num,%ulticomp.round) $+ ) (byes are not shown))
    var %x = 1, %matches_played = 0, %matches_progress = 0, %msgcount = 0
    var %y = $ini(ulticomp_matches.ini,0)
    while (%x <= %y) {
      if ($getmatch(%x)) {
        var %match = $v1

        if ($gettok(%match,4,37) = %ulticomp.round) && ($gettok(%match,2,37) != bye) {
          if (%ulticomp.maxgamesbyespr > 8) {
            if (%msgcount < 8) {
              if (!$gettok(%match,3,37)) {
                msg %ulticomp.chan $codes(Match $clr(num,%x) (Round: $clr(num,$gettok(%match,4,37)) $+ ): $&
                  $teamstring($gettok(%match,1,37)) vs $teamstring($gettok(%match,2,37)))

                inc %msgcount
              }
            }
          }
          else {
            msg %ulticomp.chan $codes(Match $clr(num,%x) (Round: $clr(num,$gettok(%match,4,37)) $+ ): $teamstring($gettok(%match,1,37)) $&
              $iif($gettok(%match,2,37) != bye,vs $teamstring($gettok(%match,2,37)),(Bye)) $&
              $iif(%ulticomp.started,Winner: $iif($gettok(%match,3,37),$teamstring($gettok(%match,3,37)),No winner yet)))
          }

          if ($gettok(%match,3,37)) { 
            inc %matches_played
          }
          else {
            inc %matches_progress
          }
        }
      }
      inc %x
    }
    if (!%matches_played) && (!%matches_progress) {
      msg %ulticomp.chan $codes(There are currently no matches that have been played and there are no matches currently in progress in this round)
    }
    elseif (%matches_played) && (!%matches_progress) {
      msg %ulticomp.chan $codes(Total of $clr(num,%matches_played) matches have been completed. No matches are currently in progress)
    }
    elseif (!%matches_played) && (%matches_progress) {
      msg %ulticomp.chan $codes(Total of $clr(num,%matches_progress) matches in progress)
    }
    else {
      msg %ulticomp.chan $codes(Total of $clr(num,%matches_played) matches have been played. $clr(num,%matches_progress) matches are in progress)
    }
    msg %ulticomp.chan $codes(All matches can be seen at http://ultiduo.ipgn.com.au)
  }
  if ($1 = !stvip) && ($2) && ($2 isnum) {
    if ($2 = %hotserverid) {
      msg $chan $codes(STV: 202.138.3.13:27035)
      return
    }

    var %server = $getserver($2)

    if (!%server) {
      msg $chan $codes(Invalid server)
      return
    }

    msg $chan $codes(STV IP for server $clr(num,$2) $+ : connect $+($gettok(%server,1,37),:,$calc($gettok(%server,2,37) + 1)))
  }
  if ($1 = !addteam) && (($nick isvoice %ulticomp.chan) || ($nick isop %ulticomp.chan)) {
    ;.whois $2
    ;.whois $nick
    if (%ulticomp.started) {
      notice $nick $codes(Sorry $+ $chr(44) the competition has already started. If a team must withdraw $+ $chr(44) you may be able to take their place)
      return
    }
    if ($ini(ulticomp.ini,0) == %ulticomp.teams) {
      msg %ulticomp.chan $codes(Sorry $+ $chr(44) the competition is currently full. Should positions open $+ $chr(44) it will be visible in this channel.)
      return
    }
    if ((!$2) || ($3)) {
      notice $nick $codes(You need to specify the other player in your team Eg. !addteam $clr(name,bladez))
      return
    }
    if ($2 !ison %ulticomp.chan) {
      notice $nick $codes(Invalid team member specification. Please ensure your team mate is in the channel. eg !addteam $clr(name,bladez)) 
      return
    }
    if ($2 == $nick) {
      notice $nick $codes(You can't have a team with just yourself in it) 
      return
    }
    if (%ulticomp.team [ $+ [ $nick ] ]) {
      notice $nick $codes(You've already tried to register a team and are still waiting for admin approval)
      return
    }
    if ($hfind(signed,* $+ $nick $+ *, 1, w).data) || ($hfind(signed,* $+ $address($nick,2) $+ *, 1, w).data) {
      var %readline = $v1
      notice $nick $codes(You are already registered in a team ( $+ $teamstring($gettok(%readline,3,37)) $+ ))
      return
    }
    if ($hfind(signed,* $+ $2 $+ *, 1, w).data) || ($hfind(signed,* $+ $gettok($address($2,2),2,$asc(@)) $+ *, 1, w).data) {
      var %seed = $v1
      notice $nick $codes(Your team mate is already registered in a team ( $+ $teamstring(%seed) $+ ))
      return
    }

    addteam $nick $2

    upload teams ulticomp.ini
  }
}

on *:NICK:{
  if (%ulticomp.open) {
    if ($hfind(signed,* $+ $nick $+ *, 1, w).data) {
      var %seed = $v1
      var %sdata = $hget(signed,%seed)

      echo @debug $nick changed his name to $newnick

      var %nickpos = $findtok(%sdata,$nick,1,37)
      if (%nickpos == 3) {
        writeini ulticomp.ini %seed teammate " $+ $newnick $+ "
      }
      elseif (%nickpos == 1) {
        writeini ulticomp.ini %seed teamcaptain " $+ $newnick $+ "
      }
      else {
        echo @debug something went wrong and couldn't get pos of $nick in $hget(signed,%seed)
      }

      hadd signed %seed $replace(%sdata,$nick,$newnick)
    }
  }
}
